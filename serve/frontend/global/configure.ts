import { $, $set, $xhr } from './dollar';
import { showModal } from './modal';
import { formatTz, loadingSpinner } from './utils';

const initConfigure = (container: HTMLDivElement, onChangeCallback?: () => void): void => {
    const allInputs = Array.from(container.querySelectorAll('[name]')) as (HTMLSelectElement | HTMLInputElement)[];
    const onChange = function (this: HTMLSelectElement | HTMLInputElement) {
        for (const input of allInputs) {
            input.disabled = true;
        }
        const value = (this.tagName === 'INPUT') ? `${!!(this as HTMLInputElement).checked}` : (this as HTMLSelectElement).value;
        if (onChangeCallback) { onChangeCallback(); }
        $xhr('/api/configure', {
            f: encodeURIComponent(this.name) + '=' + encodeURIComponent(value),
            e: msg => alert('设置失败：' + msg),
            t: html => {
                container.innerHTML = html;
                initConfigure(container); // We must have already called onChangeCallback so there's no point in calling it again.
            }
        });
    };
    for (const select of allInputs) {
        select.addEventListener('change', onChange);
    }

    const deviceTz = Math.round(-(new Date).getTimezoneOffset() / 60);
    const currentTz = parseInt((container.querySelector('#tz-select') as HTMLSelectElement).value);
    if (deviceTz !== currentTz) {
        const warningEl = container.querySelector('#tz-warning') as HTMLElement;
        warningEl.style.display = 'block';
        warningEl.appendChild(document.createTextNode(formatTz(deviceTz)));
    }

    const openThemeSelector = () => {
        const subContainer = $('div', undefined, loadingSpinner(false));
        showModal('配色风格', subContainer, undefined, 'xl');    
        $xhr('/api/themes', {
            e: () => { $set(container, '加载失败 (,,#ﾟДﾟ)'); },
            t: html => {
                subContainer.innerHTML = html;
                const iframes = Array.from(subContainer.querySelectorAll('iframe'));
                const checkbox = subContainer.querySelector('#swc-color-preview') as HTMLInputElement;
                const text = subContainer.querySelector('#swc-color-text') as HTMLSpanElement;
                let dark = document.documentElement.getAttribute('data-bs-theme') === 'dark';
                checkbox.checked = dark;
                const iframeLoad = ({ currentTarget } : { currentTarget: unknown }) => {
                    (currentTarget as HTMLIFrameElement).contentDocument!.documentElement.setAttribute('data-bs-theme', dark ? 'dark' : 'light');
                };
                const render = () => {
                    $set(text, dark ? '深' : '浅');
                    for (const iframe of iframes) {
                        try { iframeLoad({ currentTarget: iframe }); } catch (e) {}
                    }
                };
                for (const iframe of iframes) {
                    iframe.addEventListener('load', iframeLoad);
                }
                checkbox.addEventListener('change', () => {
                    dark = checkbox.checked;
                    render();
                });
                render();
            }
        });
    };
    const el = container.querySelector('#theme-entry') as HTMLInputElement;
    el.addEventListener('click', openThemeSelector);
    el.addEventListener('keypress', openThemeSelector);
};

export const openMazoSettings = (highlight?: string) => {
    const container = $('div', undefined, loadingSpinner(false));
    let changed = false;
    showModal('界面选项', container, () => {
        if (changed) { location.reload(); return false; }
        return true;
    });
    $xhr('/api/configure', {
        e: () => { $set(container, '加载失败 (,,#ﾟДﾟ)'); },
        t: html => {
            container.innerHTML = html;
            initConfigure(container, () => { changed = true; });
            if (!highlight) { return; }
            const select = container.querySelector(`#${highlight}-select`);
            if (!select) { return; }
            (select.parentNode!.parentNode as HTMLElement).classList.add('highlight-all');
        }
    });
};