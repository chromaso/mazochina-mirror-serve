import * as elements from 'typed-xhtml';

import { HtmlTmpl } from '../utils/tsx_utils';
import { BaseThreadTarget, RenderThreadRetVal } from '../shared/thread_table';
import { tagList } from './tag';

// Migrated from threads_table_lite.inc.pug.
export const threadTableLite: HtmlTmpl<{
    threads: RenderThreadRetVal<BaseThreadTarget>
}> = (ctx, { threads }) => {
    const { mazoLoginUser, render } = ctx.state;
    let blockedCount = 0;
    return (
        <>
            <table id="thread-table-main" class="table mb-0">
                <tbody>
                    {threads.map(thread => {
                        const starIcon = (thread.star !== undefined) ? (thread.star ? 'favorite' : 'block') : undefined;
                        const block = thread.star === false;
                        if (block) { blockedCount++; }
                        return (
                            <tr id="thread-row" class={block ? 'd-none opacity-50' : ''}>
                                <td class="align-middle px-0">
                                    <span class="read-mark ms-sm-1">
                                        {
                                            ((thread.unread === -4) ? (
                                                <>
                                                    <span
                                                        title="此主题全部楼层已读"
                                                        role="button"
                                                        class="material-icons v-low text-secondary"
                                                    >
                                                        chat_bubble
                                                    </span>
                                                    {starIcon ? (
                                                        <span class="material-icons fav-icon v-low text-secondary">
                                                            {starIcon}
                                                        </span>
                                                    ) : null}
                                                </>
                                            ) : (thread.unread === -3) ? (
                                                <>
                                                    <span
                                                        title="此主题读过，但有新回复未读…"
                                                        role="button"
                                                        class="material-icons text-primary"
                                                    >
                                                        mark_chat_unread
                                                    </span>
                                                    {starIcon ? (
                                                        <span class="material-icons fav-icon text-primary">
                                                            {starIcon}
                                                        </span>
                                                    ) : null}
                                                </>
                                            ) : (thread.unread === -2) ? (
                                                <>
                                                    <span
                                                        title="此主题很久没更新"
                                                        role="button"
                                                        class="material-icons v-low text-primary"
                                                    >
                                                        chat_bubble
                                                    </span>
                                                    {starIcon ? (
                                                        <span class="material-icons fav-icon v-low text-primary">
                                                            {starIcon}
                                                        </span>
                                                    ) : null}
                                                </>
                                            ) : (thread.unread === -1) ? (
                                                <>
                                                    <span
                                                        title="此主题完全未读"
                                                        role="button"
                                                        class="material-icons text-primary"
                                                    >
                                                        mark_chat_unread
                                                    </span>
                                                    {starIcon ? (
                                                        <span class="material-icons fav-icon text-primary">
                                                            {starIcon}
                                                        </span>
                                                    ) : null}
                                                </>
                                            ) : (
                                                <span
                                                    title="登入后你可收藏主题及记录已读状态"
                                                    role="button"
                                                    class="material-icons text-primary"
                                                >
                                                    mark_chat_unread
                                                </span>
                                            ))
                                        }
                                    </span>
                                </td>
                                <td class="align-middle">
                                    <a href={mazoLoginUser ? '/post/t' + thread.thread_id : thread.firstLink} class="ui-link">
                                        {render.contentConv(thread.thread_title)}
                                    </a>
                                    {thread.official ? (
                                        <> <span class="material-icons align-middle user-select-none" style='font-size: 100%' title="公告">verified</span></>
                                    ) : null}
                                    {thread.tags ? tagList(ctx, { list: thread.tags, tagStyle: threads.tagStyle }) : null}
                                    <small class="d-block mt-1">
                                        <span class="badge bg-secondary">{render.staticConv(thread.forumName)}</span>
                                        {' '}
                                        {render.renderDate(thread.lastTime)}
                                    </small>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
                {blockedCount ? (
                    <tfoot>
                        <tr class="opacity-50">
                            <td colspan="3" class="text-center">
                                另有 <a href="javascript:void(0)">{blockedCount} 个主题</a>已被你屏蔽
                            </td>
                        </tr>
                    </tfoot>
                ) : null}
            </table>
            <script type='text/javascript'>{mazoLoginUser ? elements.safeHtml(`initThreadListPage();`) : '/*NOT_LOGGED_IN*initThreadListPage()*/'}</script>
        </>
    );
};

// Migrated from threads_table.inc.pug.
export const threadTable: HtmlTmpl<{
    forumId?: number,
    threads: RenderThreadRetVal<BaseThreadTarget>
    showReadState: boolean
}> = (ctx, self) =>  {
    const { render } = ctx.state;
    let blockedCount = 0;
    return (
        <>
            <table id="thread-table-main" class="table">
                <thead>
                    {self.showReadState ? <th class="px-0" /> : null}
                    <th>主题</th>
                    <th class="text-nowrap text-end">回复</th>
                    <th class="d-none d-sm-table-cell">最后发表</th>
                </thead>
                <tbody>
                    {self.threads.map(thread => {
                        const lastTime = render.renderDate(thread.lastTime);
                        const starIcon = (thread.star !== undefined) ? (thread.star ? 'favorite' : 'block') : undefined;
                        let classes: string | undefined;
                        if (self.showReadState && (thread.star === false)) { 
                            blockedCount++;
                            classes = 'd-none opacity-50';
                        } else if (thread.isSpam) {
                            classes = 'opacity-50';
                        }
                        return (
                            <tr class={classes ?? ''}>
                                {self.showReadState ? (
                                    <td class="align-middle text-center px-0">
                                        <span class="read-mark ms-sm-1">
                                            {
                                                ((thread.unread === -4) ? (
                                                    <>
                                                        <span
                                                            title="此主题全部楼层已读"
                                                            role="button"
                                                            class="material-icons v-low text-secondary"
                                                        >
                                                            chat_bubble
                                                        </span>
                                                        {starIcon ? (
                                                            <span class="material-icons fav-icon v-low text-secondary">
                                                                {starIcon}
                                                            </span>
                                                        ) : null}
                                                    </>
                                                ) : (thread.unread === -3) ? (
                                                    <>
                                                        <span
                                                            title="此主题读过，但有新回复未读…"
                                                            role="button"
                                                            class="material-icons text-primary"
                                                        >
                                                            mark_chat_unread
                                                        </span>
                                                        {starIcon ? (
                                                            <span class="material-icons fav-icon text-primary">
                                                                {starIcon}
                                                            </span>
                                                        ) : null}
                                                    </>
                                                ) : (thread.unread === -2) ? (
                                                    <>
                                                        <span
                                                            title="此主题很久没更新"
                                                            role="button"
                                                            class="material-icons v-low text-primary"
                                                        >
                                                            chat_bubble
                                                        </span>
                                                        {starIcon ? (
                                                            <span class="material-icons fav-icon v-low text-primary">
                                                                {starIcon}
                                                            </span>
                                                        ) : null}
                                                    </>
                                                ) : (thread.unread === -1) ? (
                                                    <>
                                                        <span
                                                            title="此主题完全未读"
                                                            role="button"
                                                            class="material-icons text-primary"
                                                        >
                                                            mark_chat_unread
                                                        </span>
                                                        {starIcon ? (
                                                            <span class="material-icons fav-icon text-primary">
                                                                {starIcon}
                                                            </span>
                                                        ) : null}
                                                    </>
                                                ) : (
                                                    <span
                                                        title="登入后你可收藏主题及记录已读状态"
                                                        role="button"
                                                        class="material-icons text-primary"
                                                    >
                                                        mark_chat_unread
                                                    </span>
                                                ))
                                            }
                                        </span>
                                        {/* Author page may inject elements using JS here. See `initPinnedThreads()` */}
                                    </td>
                                ) : null}
                                <td class="align-middle">
                                    <a href={thread.firstLink} class="ui-link">
                                        {render.contentConv(thread.thread_title)}
                                    </a>
                                    {thread.official ? (
                                        <> <span class="material-icons align-middle user-select-none" style='font-size: 100%' title="公告">verified</span></>
                                    ) : null}
                                    {thread.tags ? tagList(ctx, { list: thread.tags, tagStyle: self.threads.tagStyle }) : null}
                                    <small class="d-block mt-1">
                                        {!self.forumId ? (
                                            <><span class="badge bg-secondary">{render.staticConv(thread.forumName)}</span> </>
                                        ) : null}
                                        <span class="d-none d-sm-inline-block">
                                            <a href={'/author/' + thread.ownerId} class="ui-link">{thread.firstUser}</a> @ {render.renderDate(thread.firstTime)}
                                        </span>
                                        <span class="d-sm-none">
                                            最后发表：
                                            <a href={thread.lastLink} class="ui-link">
                                                {thread.lastUser} @ {lastTime}
                                            </a>
                                        </span>
                                    </small>
                                </td>
                                <td class="align-middle text-nowrap text-end">
                                    {thread.count - 1}
                                </td>
                                <td class="d-none d-sm-table-cell align-middle">
                                    <a href={thread.lastLink} class="ui-link">
                                        {thread.lastUser}
                                        <br />
                                        {lastTime}
                                    </a>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
                {blockedCount ? (
                    <tfoot>
                        <tr class="opacity-50">
                            <td colspan="4" class="text-center">
                                另有 <a href="javascript:void(0)">{blockedCount} 个主题</a>已被你屏蔽
                            </td>
                        </tr>
                    </tfoot>
                ) : null}
            </table>
            <script type='text/javascript'>{(self.showReadState && ctx.state.mazoLoginUser) ? elements.safeHtml(`initThreadListPage();`) : '/*NOT_LOGGED_IN*initThreadListPage()*/'}</script>
        </>
    );
};