import * as campaign from './campaign';
import * as dash from './dash';
import * as moderation from './moderation';

const global = window as any;

// Initialize all components of the admin dashboard (regular section).
// Usages: admin.tsx (adminDash).
global['initAdminDash'] = dash.initAdminDash;

// Initialize all components of the admin dashboard (core section).
// Usages: admin.tsx (adminDash).
global['initAdminDashCore'] = dash.initAdminDashCore;

// Manage a post or user.
// Usages: author.tsx (author_id), threads.tsx (thread_id_page).
global['mazomirrorAdmin'] = moderation.mazomirrorAdmin;

// Manage a thread.
// Usages: threads.tsx (thread_id_page).
global['initThreadAdmin'] = moderation.initThreadAdmin;

// Campaign admin page: show review content.
// Usages: campaign_backends.tsx (adminContest).
global['mmcaShowContentModal'] = campaign.mmcaShowContentModal;

// Campaign admin page: edit score/review.
// Usages: campaign_backends.tsx (adminContest).
global['mmcaShowEditModal'] = campaign.mmcaShowEditModal;

// Campaign admin page: Initialize chart-drawing features.
// Note that the method will add two more methods to window.
// Usages: campaign_backends.tsx (adminContest).
global['mmcaInitCharts'] = campaign.mmcaInitCharts;