import { parseBBCode } from '../../common/bbcode';
import { query as sql, withTransaction } from '../db';
import { CONV_SC as toSc } from '../chinese';
import { F18_CONSTS } from '../shared/tags';
import { appendSumInPlace, ExtraInformation, ExtraInformationLegacy, legacyRowCompat, renderSortOrderForFields, SCORE_COMPONENTS, TEMP_EXTERNAL_WORKS, TITLES_OF_COMPONENTS } from '../shared/contest_backend';
import { rowsToMap, rowsToMultiMap } from '../../common/logic_utils';
import * as sStats from 'simple-statistics';
import { promises as fs } from 'fs';
import assert = require('assert');
import path = require('path');
import { Campaign192Json, META_THREAD_ID, REVIEW_CAMPAIGN_TAG_ID as TAG_ID } from '../shared/contest_result';
import { fromRoot } from '../path';

type UserLite = { user_id: number, user_name: string };

const normalize = (val: string): string => toSc(val.toLowerCase());

const globalRefTime = Math.round(Date.now() / 1000);

const campaignDir = fromRoot('static', 'campaign');

const postSingle = (threadId: number, userId: number, refTime: number, postTitle: string, postContent: string): Promise<number> =>
    withTransaction(async query => {
        const insertedLog = (await query('INSERT INTO local_post_log (rev_id, post_id, updated_at, title, content, flags) VALUES (nextval(\'local_rev_id\'), nextval(\'local_post_id\'), $1, $2, $3, 16) RETURNING rev_id, post_id', [refTime, postTitle, postContent])).rows[0];
        const postId = insertedLog.post_id;
        const parsed = parseBBCode(postId, postContent, { lax: true });
        const normalizedTextContent = normalize(postTitle + '\n\ufffc\n' + parsed!.text);
        await query('INSERT INTO posts (post_id, thread_id, title, user_id, content, posted_date, most_recent_update, content_sc, flags, rev_id) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, 16, $9)', [postId, threadId, postTitle, userId, parsed.html, refTime, -globalRefTime, normalizedTextContent, insertedLog.rev_id]);
        return postId;
    });

const main = async () => {
    const generatePosts = process.argv.some(v => v.includes('post'));
    console.log(`Generate posts? ${generatePosts}.`);

    const panelMembers = rowsToMap((await sql<UserLite>('SELECT user_id, user_name FROM users WHERE flags & 16 <> 0 OR user_id = 25646')).rows, 'user_id');
    // Note: asserting every eligible thread have at least a review.
    const threadRows = rowsToMap((await sql<{ thread_id: number, thread_title: string }>(`SELECT thread_id, thread_title FROM thread_targets WHERE forum_id = 18 AND last_page_number > 0 AND successive_fail >= 0 AND EXISTS (SELECT tagass_id FROM tag_association WHERE tag_association.thread_id = thread_targets.thread_id AND tag_id = $1 AND tag_score > ${F18_CONSTS.CONSTS.SCORE_MULTIPLIER}) ORDER BY thread_id ASC`, [TAG_ID])).rows, 'thread_id');
    const allReviews = (await sql<{ rev_id: number, thread_id: number, user_id: number, content: string, review_at: number, extra_information: ExtraInformationLegacy }>('SELECT DISTINCT ON (thread_id, user_id) rev_id, thread_id, user_id, content, review_at, extra_information FROM contest_panel_reviews WHERE thread_id = ANY($1::INT[]) AND user_id = ANY($2::INT[]) ORDER BY thread_id, user_id, rev_id DESC', [[...threadRows.keys(), META_THREAD_ID, ...TEMP_EXTERNAL_WORKS.keys()], Array.from(panelMembers.keys())])).rows.map(legacyRowCompat);

    // For all reviews and forewords, generate posts.
    if (generatePosts) {
        for (const review of allReviews) {
            const user = panelMembers.get(review.user_id);
            assert.ok(user);
            if (!review.content.trim()) { continue; }
            let title: string;
            if (review.thread_id === META_THREAD_ID) {
                title = `评委 ${user.user_name} 的前言`;
            } else if (review.thread_id < 0) {
                continue;
            } else {
                const thread = threadRows.get(review.thread_id);
                assert.ok(thread);
                title = `评委 ${user.user_name} 对《${thread.thread_title}》的评语`;
            }
            const postId = await postSingle(review.thread_id, review.user_id, Math.abs(review.review_at), title, review.content);
            const newExtraInfo: ExtraInformation = { ...(review.scores ? { subscores1: review.scores } : {}), post: postId };
            console.log(`Created post ${postId} for ${title}`);
            await sql('UPDATE contest_panel_reviews SET extra_information = $1 WHERE rev_id = $2', [JSON.stringify(newExtraInfo), review.rev_id]);
        }
    }

    const readerVotes = rowsToMap((await sql<{ thread_id: number, v1: number[] | null, v2: number[] | null }>('SELECT thread_id, ARRAY_AGG(user_id ORDER BY vote_at ASC) FILTER (WHERE vote = 2) AS v2, ARRAY_AGG(user_id ORDER BY vote_at ASC) FILTER (WHERE vote = 1) AS v1 FROM contest_votes WHERE vote > 0 GROUP BY thread_id')).rows, 'thread_id');

    type Normalizer = (score: number) => (number | null);
    const normalizersByUserAndComponent: Map<number, Normalizer[]> = new Map();
    for (const [userId, rows] of rowsToMultiMap(allReviews, 'user_id')) {
        normalizersByUserAndComponent.set(userId, SCORE_COMPONENTS.map((max, index) => {
            const scores = rows.map(row => (row.thread_id === META_THREAD_ID) ? undefined : row.scores?.[index]).filter((_: number | null | undefined): _ is number => !!_);
            if (!scores.length) {
                throw new Error(`评委 ${userId} 未给任何作品评分`);
            }
            const mean = sStats.mean(scores);
            const sd = sStats.standardDeviation(scores); // Can be zero.
            return score => (score === mean) ? 0 : (((score - mean) / sd) * max);
        }));
    }

    const totalScores = appendSumInPlace(SCORE_COMPONENTS.slice());

    const allEntries = Array.from(rowsToMultiMap(allReviews, 'thread_id')).filter(([threadId]) => threadId !== META_THREAD_ID).map(([threadId, reviews]) => {
        const aggregationRaw = TITLES_OF_COMPONENTS.map(_ => [] as number[]);
        const aggregationStd = TITLES_OF_COMPONENTS.map(_ => [] as number[]);

        const userIds: number[] = [];
        let reviewTextCnt = 0;
        for (const review of reviews) {
            const scores = review.scores;
            const normalizers = normalizersByUserAndComponent.get(review.user_id);
            if (!normalizers) { throw new Error(`未找到 ${review.user_id}`); }
            if (scores) {
                if ((scores.length !== SCORE_COMPONENTS.length) || scores.some((v, i) => !v || (v > SCORE_COMPONENTS[i]))) {
                    throw new Error(`${review.user_id}@${review.thread_id} 不合法：[${scores.join(',')}]`);
                }
                appendSumInPlace(scores.slice()).forEach((v, i) => aggregationRaw[i].push(v));
                appendSumInPlace(scores.map((single, index) => normalizers[index](single)!)).forEach((v, i) => aggregationStd[i].push(v / totalScores[i]));
                userIds.push(review.user_id);
            }
            if (review.content) {
                reviewTextCnt++;
            }
        }
        const vote = readerVotes.get(threadId);
        return {
            thread: threadId,
            scores: aggregationRaw.map((dim, index) => ({
                avg: sStats.mean(dim),
                z: sStats.mean(aggregationStd[index]),
                sd: sStats.standardDeviation(dim)
            })),
            reviews: reviewTextCnt,
            votes: appendSumInPlace([(vote?.v1?.length ?? 0) + (vote?.v2?.length ?? 0), vote?.v2?.length ?? 0]),
            voters: [vote?.v1 ?? [], vote?.v2 ?? []],
            scorers: userIds
        } as const;
    });

    const rankings = renderSortOrderForFields({
        scores: TITLES_OF_COMPONENTS.map(() => ({ avg: 0 })),
        votes: [0, 0, 0]
    }, allEntries);

    const rankString = (rankStr: string): string | null => {
        const rank = parseInt(rankStr);
        if (rank === 1) { return '第一名'; }
        if (rank === 2) { return '第二名'; }
        if (rank <= allEntries.length / 10) { return '前 10%'; }
        if (rank <= allEntries.length / 4) { return '前 25%'; }
        if (rank <= allEntries.length / 2) { return '前 50%'; }
        return null;
    };

    const external = (negativeId: number) => {
        const work = TEMP_EXTERNAL_WORKS.get(negativeId)!;
        return { url: `https://www.pixiv.net/novel/show.php?id=${-negativeId}`, date: Math.round(Date.parse(work[1]) / 1000), title: work[0] };
    };

    const outputJson: Campaign192Json = Object.fromEntries(allEntries.map((thread, threadIndex) => [
        thread.thread,
        {
            scores: thread.scores.map((component, componentIndex) => ({
                ...component,
                rank: rankString(rankings[threadIndex].scores[componentIndex].avg)
            })),
            reviews: thread.reviews,
            scorers: thread.scorers,
            ...(thread.thread < 0) ? external(thread.thread) : {
                votes: thread.votes.map((count, index) => ({ count, rank: rankString(rankings[threadIndex].votes[index]) })),
                voters: thread.voters
            }
        }
    ]));
    await fs.writeFile(path.join(campaignDir, '192.json'), JSON.stringify(outputJson, (k, v) => (typeof v === 'number') ? Number(v.toFixed(4)) : v));
};

main().catch(e => { console.error(e); process.exit(-1); });