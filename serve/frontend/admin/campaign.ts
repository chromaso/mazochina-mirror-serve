/// <reference types="google.visualization" />

import { $id, $, $set, $xhr, showModal } from '../common-import';

export const mmcaShowContentModal = (threadId: number, userId: number) => {
    var ph = $('div', undefined, '加载中…');
    showModal('评语全文', ph);
    $xhr('/api/contest/thread/' + threadId + '/user/' + userId, {
        e: e => $set(ph, $('div', 'alert alert-danger', '错误：' + e)),
        j: j => $set(ph, $('div',{ innerHTML: j.html }))
    });
};

export const mmcaShowEditModal = (threadId: number, userId: number) => {
    showModal('代为填入评分/评语', $('form', { method: 'post' }, [
        $('input', { name: 'override-thread', type: 'hidden', 'value': threadId }),
        $('input', { name: 'override-user', type: 'hidden', 'value': userId }),
        $('div', 'form-group row', [
            $('label', { className: 'col-sm-2 col-form-label', htmlFor: 'input-panel-review-score1' }, '色情度'),
            $('div', 'col-sm-3',
                $('input', { id: 'input-panel-review-score1', className: 'form-control', name: 'override-score', type: 'number', min: '0', max: '100', value: '0' })
            ),
            $('div', 'col-sm-7', $('small', 'text-muted', '满分 100 分；填写 0 视作暂不评分'))
        ]),
        $('div', 'form-group row', [
            $('label', { className: 'col-sm-2 col-form-label', htmlFor: 'input-panel-review-score2' }, '文学性'),
            $('div', 'col-sm-3',
                $('input', { id: 'input-panel-review-score2', className: 'form-control', name: 'override-score', type: 'number', min: '0', max: '80', value: '0' })
            ),
            $('div', 'col-sm-7', $('small', 'text-muted', '满分 80 分；填写 0 视作暂不评分'))
        ]),
        $('div', 'form-group row', [
            $('label', { className: 'col-sm-2 col-form-label', htmlFor: 'input-panel-review-score3' }, '合题性'),
            $('div', 'col-sm-3',
                $('input', { id: 'input-panel-review-score3', className: 'form-control', name: 'override-score', type: 'number', min: '0', max: '80', value: '0' })
            ),
            $('div', 'col-sm-7', $('small', 'text-muted', '满分 80 分；填写 0 视作暂不评分'))
        ]),
        $('div', 'form-group row', [
            $('label', { className: 'col-sm-2 col-form-label', htmlFor: 'input-panel-review-score4' }, '创意加分'),
            $('div', 'col-sm-3',
                $('input', { id: 'input-panel-review-score4', className: 'form-control', name: 'override-score', type: 'number', min: '0', max: '40', value: '0' })
            ),
            $('div', 'col-sm-7', $('small', 'text-muted', '满分 40 分；填写 0 视作暂不评分'))
        ]),
        $('div', 'form-group row', [
            $('label', { className: 'col-sm-2 col-form-label', htmlFor: 'input-panel-review-comments' }, '评语'),
            $('div', 'col-sm-10',
                $('textarea', { id:  'input-panel-review-comments', className: 'form-control', name: 'override-content', placeholder: '（非必填；原先若有评语，将会被清除，若要保留原先的评语请将其手动复制至此处）' })
            )
        ]),
        $('input', { className: 'btn btn-primary',  type: 'submit', value: '提交' }),
    ]));
};

const mmcaInitCheckboxes = () => {
    const listenCheckbox = (id: string, handler: (checked: boolean) => void): void => {
        var el = $id(id) as HTMLInputElement;
        el.addEventListener('change', e => {
            handler((e.currentTarget as typeof el).checked);
        });
        handler(el.checked);
    };
    listenCheckbox('tg-fluid', checked => {
        $id('root-container')!.className = checked ? 'container-fluid my-5' : 'container my-5';
    });
    listenCheckbox('tg-steps-dod', checked =>
        $id('main-tbody')!.classList[checked ? 'add' : 'remove']('show-dod')
    );
    listenCheckbox('tg-steps-std', checked =>
        $id('main-tbody')!.classList[checked ? 'add' : 'remove']('show-std')
    );
    listenCheckbox('tg-ranking', checked =>
        $id('main-tbody')!.classList[checked ? 'add' : 'remove']('show-rank')
    );
};

export const mmcaInitCharts = (metrics: [string, number[]][], corrTuples: [number, number, number][], components: string[]) => {
    const titles = Array.from(document.querySelectorAll('.s-thread-title')).map(t => t.textContent);

    mmcaInitCheckboxes(); // Piggyback this irrelevant thing.

    google.charts.load('current', {'packages': ['corechart']});

    const ch = $id('s-colaxh') as HTMLSelectElement;
    const cv = $id('s-colaxv') as HTMLSelectElement;
    const ct = $id('scatter-target')!;
    const drawScatter = (hIndex: number, vIndex: number) => {
        const data = new google.visualization.DataTable();
        data.addColumn('number', metrics[hIndex][0]);
        data.addColumn('number', metrics[vIndex][0]);
        data.addColumn({ type: 'string', label: '作品标题', role: 'tooltip' });
        data.addRows(titles.map((title, i) => 
            [metrics[hIndex][1][i], metrics[vIndex][1][i], title]
        ));

        (new google.visualization.ScatterChart(ct)).draw(data, {
            height: 600,
            legend: 'none',
            hAxis: { title: metrics[hIndex][0] },
            vAxis: { title: metrics[vIndex][0] },
            aggregationTarget: 'category',
            chartArea: {
                left: 72,
                right: 16,
                bottom: 48,
                top: 16
            },
            dataOpacity: 0.5
        });
    }
    ch.innerHTML = '';
    metrics.forEach((metric, index) => {
        if (!metric) { return; }
        ch.appendChild($('option', { value: String(index) }, metric[0]));
    });
    ch.value = '';
    const chChanged = () => {
        const hIndex = parseInt(ch.value);
        cv.innerHTML = '';
        corrTuples.map(tuple => {
            if (typeof tuple[2] !== 'number') { return; }
            if (tuple[0] === hIndex) { return [tuple[1], tuple[2]]; }
            if (tuple[1] === hIndex) { return [tuple[0], tuple[2]]; }
        })
            .filter(<T>(tuple: T): tuple is NonNullable<T> => !!tuple)
            .sort((a, b) => a[0] - b[0])
            .forEach(p =>
                cv.appendChild($('option', { value: p[0] }, metrics[p[0]][0] + '（r=' + p[1] + '）'))
            );
        cv.value = '';
    };
    ch.addEventListener('change', chChanged);
    const cvChanged = () => {
        var hIndex = parseInt(ch.value);
        var vIndex = parseInt(cv.value);
        if (isNaN(hIndex) || isNaN(vIndex)) { return; }
        google.charts.setOnLoadCallback(() => drawScatter(hIndex, vIndex));
    };
    cv.addEventListener('change', cvChanged);

    const drawers = { 
        scores: (div: HTMLDivElement, offset: number) => {
            const data = new google.visualization.DataTable();
            data.addColumn('string', '作品标题');
            components.map((title, index) => {
                data.addColumn({ type: 'number', label: title, role: (index === components.length - 1) ? 'annotation' : 'data' });
            });
            data.addRows(
                metrics[offset + 8][1].map((v, i) => [v, i]).sort((a, b) => (b[0] - a[0]) || (a[1] - b[1])).map(t => // An array of [_, threadIndex] tuple, sorted.
                    [titles[t[1]] as any].concat([0, 2, 4, 6, 8].map(j => metrics[offset + j][1][t[1]]))
                )
            );
            (new google.visualization.BarChart(div)).draw(data, {
                title: metrics[offset + 8][0],
                isStacked: true,
                height: titles.length * 28 + 100,
                chartArea: { height: titles.length * 28, left: '30%' },
                vAxis: { textStyle: { fontSize: 14 } },
                legend: { position: 'top' }
            });
        },
        votes: (div: HTMLDivElement) => {
            const data = new google.visualization.DataTable();
            data.addColumn('string', '作品标题');
            data.addColumn('number', '👍');
            data.addColumn('number', '🥇');
            data.addColumn({ type: 'number', label: '👍+🥇', role: 'annotation' });
            data.addRows(
                metrics[6][1].map((v, i) => [v, i]).sort((a, b) => (b[0] - a[0]) || (a[1] - b[1])).map(t => // An array of [_, threadIndex] tuple, sorted.
                    [titles[t[1]] as any].concat([2, 4, 6].map(j => metrics[j][1][t[1]]))
                )
            );
            const chart = new google.visualization.BarChart(div);
            chart.draw(data, {
                title: '读者投票',
                isStacked: true,
                height: titles.length * 28 + 100,
                chartArea: { height: titles.length * 28, left: '30%' },
                vAxis: { textStyle: { fontSize: 14 } },
                legend: { position: 'top' }
            });
        }
    } as const;

    (window as any)['mmcsDrawScat'] = (h: number, v: number) => {             
        ch.value = String(h);
        chChanged();
        cv.value = String(v);
        cvChanged();
        ct.scrollIntoView();
    }

    (window as any)['mmcsDraw'] = (chart: keyof typeof drawers, offset: number) => {
        const div = $('div', undefined, '图表加载中…');
        showModal('排名', div);
        let parent: HTMLElement = div;
        while (parent = parent.parentNode as HTMLElement) {
            if (parent.classList.contains('modal-dialog')) {
                parent.classList.add('modal-xl');
                break;
            }
        }
        google.charts.setOnLoadCallback(() => drawers[chart](div, offset));
    };
};