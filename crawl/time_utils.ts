export const now = (): number => Math.floor(Date.now() / 1000);

export const timeout = (timeoutMs: number): Promise<void> => new Promise(res => setTimeout(res, timeoutMs));

export const dateStrToTsInS = (date: string): number => Math.floor(Date.parse(date.trim() + ' UTC') / 1000);