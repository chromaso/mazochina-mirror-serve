import * as phin from 'phin';
import { read } from 'read-last-lines';
import assert = require('assert');
import { QueryResult } from 'pg';

import { ADMIN_DASH_TIMESERIES_CHUNK_COUNT, ADMIN_DASH_TIMESERIES_CHUNK_LENGTH, THRESHOLD_FOR_LOCAL } from '../../common/consts';
import { rowsToMap, rowsToMultiMap } from '../../common/logic_utils';
import { BACKOFFICE_SERVER } from '../conf';
import { query } from '../db';
import { MazoKoaContext } from '../utils/logic_utils';
import { ROOT_USER } from '../shared/login';
import { allTagsById } from '../shared/tags';
import { LOGIN_REQUIRED, PageImpl, RpcImpl, error, finalizePage } from '../utils/render_utils';

import * as templates from '../templates/admin';

const LOG_FILE_PREFIX = '/var/www/.pm2/logs/userve-';
const LOG_FILES = ['error', 'out'];
const LOG_FILE_SUFFIX = '.log';

const checkAdminAccess = (ctx: MazoKoaContext) => {
    const loginUser = ctx.state.mazoLoginUser;
    if (!(loginUser?.admin || loginUser?.confirmed)) {
        throw error(403, '你没有管理权限。');
    }
}

export const adminDash: PageImpl = async ctx => {
    const loginUser = ctx.state.mazoLoginUser;
    if (!loginUser) { throw error(403, LOGIN_REQUIRED); }
    if (!(loginUser.admin || loginUser.confirmed)) { throw error(403, '你没有管理权限。'); }

    return finalizePage(ctx, {
        title: 'Dashboard',
        nav: false,
        deps: ['ADMIN_JS', 'CHARTS']
    }, templates.adminDash(ctx));
};

// Read a log file.
export const apiAdminDashLog_file: RpcImpl = async ctx => {
    if (ctx.state.mazoLoginUser?.id !== ROOT_USER) {
        throw error(403, 'Root only.');
    }

    const file = ctx.params.file;
    if (file.includes('-')) {
        const response = await phin({ url: BACKOFFICE_SERVER + 'log?file=' + ctx.params.file, method: 'GET', timeout: 15000, parse: 'string' });
        ctx.status = response.statusCode || 200;
        ctx.body = response.body;
        return;
    }
    if (!LOG_FILES.includes(file)) { throw error(404, 'No such log file.'); }
    try {
        ctx.body = await read(LOG_FILE_PREFIX + file + LOG_FILE_SUFFIX, 250);
    } catch (e) {
        ctx.body = 'Failed to read log: ' + e;
    }
};

// Read a user list.
export const apiAdminDashUsers: RpcImpl = async ctx => {
    checkAdminAccess(ctx);

    const users = (await query<{ user_id: number, user_name: string }>('SELECT user_id, user_name FROM users WHERE flags & 4 <> 0')).rows;
    const [ops, ops15] = (await Promise.all([
        query<{ admin_user_id: number, cnt: number }>('SELECT admin_user_id, COUNT(*)::INT AS cnt FROM local_admin_log GROUP BY admin_user_id'),
        query<{ admin_user_id: number, cnt: number }>('SELECT admin_user_id, COUNT(*)::INT AS cnt FROM local_admin_log WHERE operation_date > $1 GROUP BY admin_user_id', [ctx.refDate - 15 * 86400]),
    ])).map(res => rowsToMap(res.rows, 'admin_user_id'));
    ctx.body = users.map(item => ({
        id: item.user_id,
        name: item.user_name,
        op: ops.get(item.user_id)?.cnt ?? 0,
        op15: ops15.get(item.user_id)?.cnt ?? 0
    })).sort((a, b) => b.op15 - a.op15);
};

// Read stats over chunks of 8 hours, over the last 15 days (thus, a total of 45 chunks).
export const apiAdminDashTimeseries_stat: RpcImpl = async ctx => {
    checkAdminAccess(ctx);

    const earliestDate = ctx.refDate - ADMIN_DASH_TIMESERIES_CHUNK_COUNT * ADMIN_DASH_TIMESERIES_CHUNK_LENGTH;

    const doQuery = async (tableName: string, fieldName: string, whereClause?: string): Promise<{ chunk: number, cnt: number }[]> => (await query(`SELECT ($3 - ${fieldName}) / $2 AS chunk, COUNT(*)::INT AS cnt FROM ${tableName} WHERE ${fieldName} > $1${whereClause ? ' AND ' + whereClause : ''} GROUP BY chunk`, [earliestDate, ADMIN_DASH_TIMESERIES_CHUNK_LENGTH, ctx.refDate])).rows;

    let lists: Awaited<ReturnType<typeof doQuery>>[];
    switch (ctx.params.stat) {
        case 'user':
            lists = await Promise.all([doQuery('users', 'registered_at'), doQuery('user_tombstone', 'registered_at'), doQuery('user_tombstone', 'merged_at')]);
            break;
        case 'thread':
            lists = await Promise.all([doQuery('thread_targets', 'firstpost_date', `thread_id > ${THRESHOLD_FOR_LOCAL}`), doQuery('thread_targets', 'firstpost_date', `thread_id <= ${THRESHOLD_FOR_LOCAL}`)]);
            break;
        case 'post':
            lists = await Promise.all([doQuery('posts', 'posted_date', `post_id > ${THRESHOLD_FOR_LOCAL}`), doQuery('posts', 'posted_date', `post_id <= ${THRESHOLD_FOR_LOCAL}`)]);
            break;
        case 'fav':
            lists = await Promise.all([doQuery('thread_stars', 'star_at')]);
            break;
        default:
            throw error(404, 'No such stat.');
    }

    const output = new Array(ADMIN_DASH_TIMESERIES_CHUNK_COUNT);
    lists.forEach((list, index) => {
        for (const { chunk, cnt } of list) {
            if (!output[chunk]) {
                output[chunk] = new Array(lists.length);
                output[chunk].fill(0);
            }
            output[chunk][index] = cnt;
        }
    });

    ctx.body = output;
};

export const apiAdminDashExttimeseries_stat: RpcImpl = async ctx => {
    if (ctx.state.mazoLoginUser?.id !== ROOT_USER) {
        throw error(403, 'Root only.');
    }

    const CYCLE_LENGTH = 259200;
    const firstCycle = Math.floor(1633000000 / CYCLE_LENGTH);
    const lastCycle = Math.floor(Date.now() / 1000 / CYCLE_LENGTH + 0.5);

    const mergeIntoOthers = <T extends string, V extends number | string>(list: QueryResult<{ [key in T]: V } & { cnt: number, cycle: number }>, mergeOnKey: T): [V[], number[][]] => {
        const countsByKey = new Map<V, number>();
        for (const row of list.rows) {
            countsByKey.set(row[mergeOnKey], (countsByKey.get(row[mergeOnKey]) ?? 0) + row.cnt);
        }
        // The top 6 keys. Others will be merged into "other".
        const top6 = Array.from(countsByKey.entries()).sort((a, b) => b[1] - a[1]).slice(0, 6).map(pairs => pairs[0]);
        const byCycle: Map<number, ({ cnt: number } & { [key in T]: V })[]> = rowsToMultiMap(list.rows, 'cycle');

        const data: number[][] = [];
        for (let cycle = firstCycle; cycle <= lastCycle; cycle++) {
            const outData = new Array<number>(7);
            outData.fill(0);
            for (const row of (byCycle.get(cycle) ?? [])) {
                const index = top6.indexOf(row[mergeOnKey]);
                if (index < 0) {
                    outData[0] += row.cnt;
                } else {
                    assert.strictEqual(outData[index + 1], 0);
                    outData[index + 1] = row.cnt;
                }
            }
            data.push(outData);
        }

        return [top6, data];
    };

    let lists: [any[], number[][]];
    switch (ctx.params.stat) {
        case 'user':
            lists = mergeIntoOthers(
                await query<{ cnt: number, cycle: number, mail_suffix: string }>(`SELECT SPLIT_PART(email, '@', 2) AS mail_suffix, COUNT(*)::INT AS cnt, (CASE WHEN user_id <= ${THRESHOLD_FOR_LOCAL} THEN (SELECT registered_at FROM user_tombstone WHERE user_tombstone.new_user_id = users.user_id) ELSE registered_at END) / ${CYCLE_LENGTH} AS cycle FROM users WHERE password_h IS NOT NULL GROUP BY mail_suffix, cycle`),
                'mail_suffix'
            );
            break;
        case 'thread':
            lists = mergeIntoOthers(
                await query<{ cnt: number, cycle: number, forum_id: number }>(`SELECT COUNT(*)::INT AS cnt, (firstpost_date / ${CYCLE_LENGTH}) AS cycle, forum_id FROM thread_targets WHERE thread_id > ${THRESHOLD_FOR_LOCAL} GROUP BY cycle, forum_id`),
                'forum_id'
            );
            break;
        case 'post':
            lists = mergeIntoOthers(
                await query<{ cnt: number, cycle: number, forum_id: number }>(`SELECT COUNT(*)::INT AS cnt, (posted_date / ${CYCLE_LENGTH}) AS cycle, (SELECT forum_id FROM thread_targets WHERE thread_targets.thread_id = posts.thread_id) FROM posts WHERE post_id > ${THRESHOLD_FOR_LOCAL} GROUP BY cycle, forum_id`),
                'forum_id'
            );
            break;
        case 'fav':
            lists = mergeIntoOthers(
                await query<{ cnt: number, cycle: number, forum_id: number }>(`SELECT COUNT(*)::INT AS cnt, (star_at / ${CYCLE_LENGTH}) AS cycle, (SELECT forum_id FROM thread_targets WHERE thread_targets.thread_id = thread_stars.thread_id) FROM thread_stars GROUP BY cycle, forum_id`),
                'forum_id'
            );
            break;
        default:
            throw error(404, 'No such stat.');
    }

    ctx.body = {
        header: lists[0],
        data: lists[1],
        first: firstCycle,
        interval: CYCLE_LENGTH
    };
};

export const adminGuide: PageImpl = async ctx => {
    checkAdminAccess(ctx);

    return finalizePage(ctx, {
        title: ctx.state.render.staticConv('管理操作指南'),
        nav: true
    }, templates.adminGuide(ctx));
};

export const adminTagAudit: PageImpl = async ctx => {
    checkAdminAccess(ctx);

    const tagOps = (await query<{ user_id: number, vote: -1 | 0 | 1, date: number, thread_id: number, tag_id: number }>('WITH tv AS (SELECT tagass_id, user_id, COALESCE(NULLIF(updated_at, 0), voted_at) AS date, SIGN(vote) AS vote FROM tag_votes ORDER BY COALESCE(NULLIF(updated_at, 0), voted_at) DESC LIMIT 400) SELECT tv.*, thread_id, tag_id FROM tv LEFT JOIN tag_association USING (tagass_id) ORDER BY date DESC')).rows;
    const userIds = new Set(tagOps.map(op => op.user_id));
    const threads = rowsToMap((await query<{ thread_id: number, thread_title: string, owner_user_id: number }>('SELECT thread_id, thread_title, owner_user_id FROM thread_targets WHERE thread_id = ANY($1::INT[])', [Array.from(new Set(tagOps.map(op => op.thread_id)))])).rows, 'thread_id');
    threads.forEach(thread => userIds.add(thread.owner_user_id));
    const users = rowsToMap((await query<{ user_id: number, user_name: string }>('SELECT user_id, user_name FROM users WHERE user_id = ANY($1::INT[])', [Array.from(userIds)])).rows, 'user_id');

    const tags = await allTagsById();
    return finalizePage(ctx, {
        title: '标签操作审计',
        nav: false
    }, templates.adminTagAudit(ctx, tagOps.map(op => ({
        vote: op.vote,
        date: op.date,
        thread: op.thread_id,
        title: threads.get(op.thread_id)?.thread_title ?? '[UNKNOWN]',
        owner: users.get(threads.get(op.thread_id)?.owner_user_id ?? 0)!,
        user: users.get(op.user_id)!,
        tag: tags.get(op.tag_id)!
    }))));
};