export function toSc(text: string): string;
export function containsCjk(text: string): boolean;