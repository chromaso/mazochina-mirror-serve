import { promises as fs } from 'fs';
import { sep } from 'path';

import { parse, print, types } from 'recast';

import { OCC_TC, OCC_SC } from '../chinese';
import { fromRoot } from '../path';

// Comment to add to each function.
const COMMENT = 'TRANSFORMED BY AUTOCC';

const doSingleFile = (content: string): string => {
    const ast = parse(content);

    types.visit(ast, {
        visitFunction(path) {
            for (let current = path.parent; current !== null; current = current.parent) {
                // Not root function, then we don't care.
                if (types.namedTypes.Function.check(current.node)) { this.traverse(path); return; }
            }
            // This is a top-level ("root") function.
            const firstParam = path.node.params[0];
            if (!(types.namedTypes.Identifier.check(firstParam) && (firstParam.name === 'ctx'))) {
                throw new Error(`Template's first param ${firstParam.loc?.start.line}:${firstParam.loc?.start.column} is not ctx!`);
            }
            if (!path.node.comments) { path.node.comments = []; }
            path.node.comments.unshift(types.builders.commentBlock(COMMENT));
            this.traverse(path);
        },

        visitLiteral(path) {
            const node = path.node;
            if (typeof node.value !== 'string') { this.traverse(path); return; }

            let inAFunction = false;
            for (let current = path.parent; current !== null; current = current.parent) {
                if (types.namedTypes.Function.check(current.node)) {
                    inAFunction = true;
                    break;
                }
            }
            
            const str = node.value;
            const tc = OCC_TC.convertSync(str);
            const sc = OCC_SC.convertSync(str);
            let out;
            if (tc === sc) {
                out = types.builders.stringLiteral(str);
            } else {
                if (!inAFunction) {
                    throw new Error(`String literal ${node.loc?.start.line}:${node.loc?.start.column} outside a function: ${JSON.stringify(node.value)}!`);
                }
                out = types.builders.conditionalExpression(
                    types.builders.memberExpression(
                        types.builders.identifier('ctx'),
                        types.builders.identifier('tc')
                    ),
                    types.builders.stringLiteral(tc),
                    types.builders.stringLiteral(sc)
                );
            }
            path.replace(out);
    
            return false;
        }
    });

    return print(ast).code;
};

const main = async () =>
    Promise.all(
        (await fs.readdir(fromRoot('templates'), { withFileTypes: true }))
            .filter(file => file.isFile() && file.name.endsWith('.js'))
            .map(file => file.path + sep + file.name)
            .map(async path => {
                try {
                    const content = await fs.readFile(path, 'utf8');
                    if (content.includes(COMMENT)) {
                        throw new Error('File already transformed. Should not do again.');
                    }
                    await fs.writeFile(path, doSingleFile(content))
                } catch (e) {
                    console.error(`Error in processing ${path}!`);
                    throw e;
                }
            })
    );

if (require.main === module) {
    main().catch(e => {
        console.error(e);
        process.exit(-1);
    });
}