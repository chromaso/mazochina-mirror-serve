import { SPAM_MANUAL, SPAM_THRESHOLD, THRESHOLD_FOR_LOCAL } from '../../common/consts';
import { unawaited } from '../../common/logic_utils';
import { query as sql, withTransaction } from '../db';
import { AdminLogDAO, prepareAdminLogTableRenderParams } from '../shared/moderation';
import { updateThreadTargetsRow } from '../shared/thread';
import { normalize, parseInt32, sendReplyNotifications, updateForumTimestamp } from '../utils/logic_utils';
import { forumsCache } from '../shared/forum';
import { LOGIN_REQUIRED, PageImpl, error, finalizePage, redirectWithDelay } from '../utils/render_utils';
import * as type4Notification from '../utils/type_4_notifications';

import * as templates from '../templates/moderation';

export const moderation_page: PageImpl = async (ctx) => {
    if (!ctx.state.mazoLoginUser) { throw error(403, LOGIN_REQUIRED); }

    const all = 'all' in ctx.query;
    let page = 1;
    let maxPage = 1;
    let isSelf = false;
    let userName: string | undefined;
    let adminOps: AdminLogDAO[];

    const userId = parseInt32(ctx.query.u as string);
    const adminId = parseInt32(ctx.query.a as string);
    if ((userId > 0) !== (adminId > 0)) { // When both params exist, just pretend neither exists.
        const targetUserRow = (await sql(`SELECT user_name FROM users WHERE user_id = $1`, [(userId > 0) ? userId : adminId])).rows[0];
        if (!targetUserRow) {
            throw error(404, '不存在这样的用户～');
        }
        userName = targetUserRow.user_name;
    }

    if (userId > 0) {
        isSelf = userId === ctx.state.mazoLoginUser.id;
        adminOps = (await sql(`SELECT admin_log_id, operation_date, admin_user_id, target_id, target_type, note, extra_information, flags FROM local_admin_log WHERE flags ${all ? '& 2 = 0' : '& 3 = 0'} AND ((target_type = 'thread' AND (SELECT owner_user_id FROM thread_targets WHERE thread_id = target_id) = $1) OR (target_type = 'post' AND (SELECT user_id FROM posts WHERE post_id = target_id) = $1) OR (target_type = 'user' AND target_id = $1)) ORDER BY admin_log_id DESC`, [userId])).rows;
    } else {
        page = parseInt32(ctx.params.page);
        if (isNaN(page) || page <= 0) { page = 1; }
        let whereClause = all ? '' : ' WHERE flags & 1 = 0';
        if (adminId > 0) { whereClause += (all ? ' WHERE' : ' AND') + ` admin_user_id = ${adminId}`; }
        adminOps = (await sql(`SELECT admin_log_id, operation_date, admin_user_id, target_id, target_type, note, extra_information, flags FROM local_admin_log${whereClause} ORDER BY admin_log_id DESC LIMIT 20 OFFSET $1`, [(page - 1) * 20])).rows;
        maxPage = Math.ceil((await sql('SELECT COUNT(admin_log_id)::INT AS cnt FROM local_admin_log' + whereClause)).rows[0].cnt / 20);
    }

    if (!adminOps.length) {
        throw error(404, (userId > 0) ? '这个用户从未被处理哦～' : '没有更多记录了～');
    }

    return finalizePage(ctx, {
        title: ctx.state.render.staticConv('管理操作记录'),
        nav: true
    }, templates.moderation(ctx, {
        ...(await prepareAdminLogTableRenderParams(adminOps)),
        pagination: {
            currentPage: page,
            maxPage,
            linkExtra: ctx.querystring ? '?' + ctx.querystring : '',
            baseLink: '/moderation'
        },
        userName,
        authorId: userId,
        adminId,
        isSelf,
        all
    }));
};

export const postAdmin: PageImpl = async ctx => {
    const loginUser = ctx.state.mazoLoginUser;
    if (!loginUser) {
        throw error(403, LOGIN_REQUIRED);
    }
    if (!(loginUser.admin || loginUser.confirmed)) { 
        throw error(403, '你没有管理权限。');
    }

    const id = ctx.request.body.integer('id');
    const type = ctx.request.body.string('type');
    if (isNaN(id)) { throw error(400, 'ID 非法。'); }

    let note = ctx.request.body.string('note');
    let extra = null;
    let targetUserIds: number[] | undefined;

    if (!loginUser.admin) { // Non-admin users (confirmed users) have limited privileges per day.
        const adminLogCount = (await sql<{ cnt: number }, [number, number]>(`SELECT COUNT(*)::INT AS cnt FROM local_admin_log WHERE admin_user_id = $1 AND operation_date > $2`, [loginUser.id, ctx.refDate - 86400])).rows[0];
        if (adminLogCount!.cnt >= 5) {
            throw error(429, '当前每位资深用户每 24 小时仅能执行 5 个管理操作。若想成为无限权限管理员，请联系站长。');
        }
    }

    let responseBody: string | undefined;
    let hideByDefault = false; // Log will be written anyway, but a type-2 notification may be skipped.
    let notAttachedToOwner = false; // Log will not be shown in per-user log list.

    switch (type) {
        case 'user': {
            const targetStatus = ctx.request.body.integer('target'); // 1 for unblocking, 0 for blocking
            if (isNaN(targetStatus)) { throw error(400, '非法目标状态。'); }
            const user = (await sql(`UPDATE users SET flags = flags ${targetStatus ? '& (~ 2::SMALLINT)' : '| 2::SMALLINT'} WHERE user_id = $1 RETURNING user_id, user_name`, [id])).rows[0];
            if (!user) { throw error(404, '用户不存在。'); }
            if (targetStatus) {
                extra = { do: 'free' };
            }
            targetUserIds = [id];
            responseBody = '用户 ' + user.user_name + (targetStatus ? ' 已被解封' : ' 已被全局永久禁言');
            break;
        }
        case 'post': {
            if (id <= THRESHOLD_FOR_LOCAL) { throw error(400, '不可以删除在源站（而非镜像）发表的回帖～'); }
            const post = (await sql<{ thread_id: number, user_id: number, flags: number }>('SELECT thread_id, user_id, flags FROM posts WHERE post_id = $1', [id])).rows[0];
            if (!post) { throw error(404, '文章不存在。'); }
            if (post.flags & 4) { throw error(400, 'IaSt 帖子暂时不支持一键删除，请手动检查数据库～'); }
            const firstOfThread = (await sql('SELECT post_id FROM posts WHERE thread_id = $1 ORDER BY posted_date ASC, post_id ASC LIMIT 1', [post.thread_id])).rows[0];
            if (firstOfThread.post_id === id) {
                throw error(400, '不可以删除主题中的首个帖子。若要直接删除整个主题，请使用下方的主题移动/删除功能。');
            }
            await sql('UPDATE posts SET most_recent_update = -ABS($1) WHERE post_id = $2', [ctx.refDate, id]);
            unawaited(sendReplyNotifications(id, [], ctx.refDate), 'delete reply-to notification (admin)');
            unawaited(type4Notification.onDeletingPost({
                thread: post.thread_id,
                post: id
            }), 'deleting subs after post deletion (admin)');

            unawaited(updateThreadTargetsRow(sql, post.thread_id), 'updating thread date (admin delete post)');
            targetUserIds = [post.user_id];
            responseBody = '帖子已被删除';
            break;
        }
        case 'thread': {
            const targetForum = ctx.request.body.integer('target');
            if (isNaN(targetForum)) { throw error(400, '非法移动目标。'); }
            const threadNow = (await sql<{ forum_id: number, thread_title: string, spam_likeness: number, successive_fail: number, owner_user_id: number, firstpost_date: number, post_count: number }>('SELECT forum_id, thread_title, spam_likeness, successive_fail, owner_user_id, firstpost_date, post_count FROM thread_targets WHERE thread_id = $1', [id])).rows[0];
            if (!threadNow) { throw error(404, '主题不存在。'); }
            extra = { from: threadNow.forum_id, to: targetForum };
            let outText: string;
            switch (targetForum) {
                case -4: {
                    if (id <= THRESHOLD_FOR_LOCAL) {
                        throw error(400, '在源站（而非镜像）发表的主题不可做此操作～'); // But the reverse is allowed.
                    }
                    const targetThreadId = ctx.request.body.integer('merge');
                    if (isNaN(targetThreadId)) {
                        throw error(400, '未指定合并对象！');
                    }
                    const targetThread = (await sql<{ thread_title: string, firstpost_date: number, successive_fail: number }>('SELECT thread_title, firstpost_date, successive_fail from thread_targets WHERE thread_id = $1', [targetThreadId])).rows[0];
                    if (!targetThread) {
                        throw error(404, `未找到 ID 为 ${targetThreadId} 的主题。`);
                    }
                    if (targetThread.successive_fail < 0) {
                        throw error(404, `ID 为 ${targetThreadId} 的主题已被删除。`);
                    }
                    if (targetThread.firstpost_date > threadNow.firstpost_date) {
                        throw error(404, `ID 为 ${targetThreadId} 的主题发布时间比此主题还晚。不可以将较旧的主题并入较新的主题。`);
                    }
                    const precheckPosts = (await sql<{ post_id: number, flags: number }>('SELECT flags FROM posts WHERE thread_id = $1', [id])).rows;
                    if (precheckPosts.some(post => post.flags !== 0)) { throw error(400, '有文章 flags 不为 0'); }
                    const posts = await withTransaction(async query => {
                        await query('UPDATE thread_targets SET successive_fail = -1000, flags = flags | 16 WHERE thread_id = $1', [id]);
                        const postList = (await query<{ post_id: number }>('UPDATE posts SET thread_id = $1 WHERE thread_id = $2 RETURNING post_id', [targetThreadId, id])).rows.map(row => row.post_id);
                        if (postList.length !== precheckPosts.length) {
                            throw error(500, 'Race condition：出现了新的文章；请重试。');
                        }
                        updateThreadTargetsRow(query, targetThreadId);
                        return postList;
                    });
                  
                    extra = { thread: targetThreadId, posts };
                    outText = `合并入「${targetThread.thread_title}」`;
                    break;
                }
                case -3:
                    if ((threadNow.spam_likeness >= SPAM_THRESHOLD) || (threadNow.successive_fail < 0)) {
                        throw error(400, '该主题已经被标记为广告或删除，不可重复操作～');
                    }
                    await sql('UPDATE thread_targets SET spam_likeness = $1 WHERE thread_id = $2', [SPAM_THRESHOLD, id]);
                    outText = '标记为广告并半隐藏';
                    break;
                case -2:
                    if (threadNow.spam_likeness < SPAM_THRESHOLD) {
                        throw error(400, '该主题并未被隐藏～');
                    }
                    if ((threadNow.spam_likeness < SPAM_MANUAL) && (threadNow.spam_likeness !== SPAM_THRESHOLD)) {
                        hideByDefault = true; // Correcting a wrong auto-spam-mark.
                    }
                    if (threadNow.successive_fail < 0) {
                        throw error(400, '该主题已经被删除，不可执行此操作～');
                    }
                    await sql('UPDATE thread_targets SET spam_likeness = -1000 WHERE thread_id = $1', [id]);
                    outText = '取消隐藏';
                    break;
                case -1:
                    if ((threadNow.spam_likeness === SPAM_MANUAL) || (threadNow.successive_fail < 0)) {
                        throw error(400, '该主题已经被隐藏或删除，不可重复操作～');
                    }
                    await sql('UPDATE thread_targets SET spam_likeness = $1 WHERE thread_id = $2', [SPAM_MANUAL, id]);
                    outText = '隐藏';
                    break;
                case 0:
                    if (threadNow.successive_fail < 0) {
                        throw error(400, '该主题已经被删除，不可重复操作～');
                    }
                    if (id <= THRESHOLD_FOR_LOCAL) {
                        throw error(400, '在源站（而非镜像）发表的主题只可隐藏不可删除～');
                    }
                    await sql('UPDATE thread_targets SET successive_fail = -1000 WHERE thread_id = $1 ', [id]);
                    outText = '删除';
                    break;
                default: {
                    const forum = (await forumsCache).get(targetForum);
                    if (!forum) { throw error(404, '目标板块不存在。'); }
                    const postIds = ctx.request.body.string('posts');
                    if (postIds) { // Splitting some posts to a different thread.
                        notAttachedToOwner = true;

                        const posts = postIds.split(',').map(postId => parseInt32(postId.trim()));
                        if (!posts.length || posts.some(item => isNaN(item))) { throw error(400, '未列出要拆分的文章。'); }
                        if (posts.some(post => post <= THRESHOLD_FOR_LOCAL)) { throw error(400, '无法拆分源站文章。'); }
                        const verifiedPosts = (await sql<{ post_id: number, user_id: number, flags: number, posted_date: number }>('SELECT post_id, user_id, flags, posted_date FROM posts WHERE post_id = ANY($1::INT[]) AND thread_id = $2 ORDER BY posted_date ASC, post_id ASC', [posts, id])).rows;
                        if (verifiedPosts.length !== posts.length) { throw error(404, '部分文章未在主题中出现。'); }
                        if (verifiedPosts.some(post => post.flags !== 0)) { throw error(400, '有文章 flags 不为 0'); }
                        if (posts.includes((await sql<{ pmin: number }>('SELECT MIN(post_id) AS pmin FROM posts WHERE thread_id = $1 AND most_recent_update >= 0', [id])).rows[0].pmin)) {
                            throw error(400, '不能拆分主楼！');
                        }

                        const threadTitle = ctx.request.body.string('title') || `自「${threadNow.thread_title}」拆分的离题内容`;
                        const lastPost = verifiedPosts[verifiedPosts.length - 1];
                        const newThread = (await sql<{ thread_id: number }>('INSERT INTO thread_targets (thread_id, forum_id, thread_title, last_page_number, firstpost_date, lastpost_date, sortpost_date, lastpost_id, lastpost_user_id, post_count, thread_title_sc, owner_user_id, flags) VALUES (nextval(\'local_thread_id\'), $1, $2, 1073741824, $3, $4, $4, $5, $6, $7, $8, $9, 8) RETURNING thread_id', [targetForum, threadTitle, verifiedPosts[0].posted_date, lastPost.posted_date, lastPost.post_id, lastPost.user_id, verifiedPosts.length, normalize(threadTitle), verifiedPosts[0].user_id])).rows[0];
                        await sql('UPDATE posts SET thread_id = $3 WHERE post_id = ANY($1::INT[]) AND thread_id = $2', [posts, id, newThread.thread_id]);

                        extra = { thread: newThread.thread_id, posts };
                        targetUserIds = Array.from(new Set(verifiedPosts.map(post => post.user_id)));
                        outText = `拆分至 /thread/${newThread.thread_id}`
                    } else { // Moving the entire thread.
                        if (threadNow.forum_id === targetForum) { throw error(400, '不可以移动至同一板块～'); }
                        const moved = (await sql('UPDATE thread_targets SET forum_id = $1 WHERE thread_id = $2 RETURNING lastpost_date', [targetForum, id])).rows[0];
                        unawaited(updateForumTimestamp(targetForum, moved.lastpost_date), `updateForumTimestamp`);
                        outText = `移动至 "${forum.forum_name}"`;
                    }
                }
            }
            targetUserIds ||= [threadNow.owner_user_id];
            responseBody = '主题已被' + outText;
            break;
        }
        default:
            throw error(400, '未指定操作类型～')
    }

    const adminLog = (await sql('INSERT INTO local_admin_log (operation_date, admin_user_id, target_id, target_type, note, extra_information, flags) VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING admin_log_id', [ctx.refDate, loginUser.id, id, type, note, extra, (hideByDefault ? 0x1 : 0x0) | (notAttachedToOwner ? 0x2 : 0x0)])).rows[0];

    if (targetUserIds && !hideByDefault) {
        targetUserIds.forEach(targetUserId => 
            unawaited(sql('INSERT INTO notifications (user_id, notification_date, message_type, primary_target_id) VALUES ($1, $2, 2, $3)', [targetUserId, ctx.refDate, adminLog.admin_log_id]), 'notification for admin operation')
        );
    }

    return redirectWithDelay(ctx, {
        message: responseBody,
        targetText: '先前的页面',
        targetHref: ctx.get('referrer') || ('/' + type + '/' + id),
        numberOfSeconds: 1
    });
};