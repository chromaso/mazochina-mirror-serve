import { $, $id, $set, $xhr, getThreadIdFromUri, loadingSpinner, showModal } from "../common-import";
import { multiTabModal } from "./modal";
import { contestCampaignTagConfirmationDialog } from "./tags_campaign";
import { CATS_CAMPAIGN, TagListModel, initializeSearcher, loadTagListModel, requireChildren } from "./tags_common";

type PostApiThreadTagResponse = { full: boolean, some: boolean, warn: boolean, name: string };

const allAnchorsInHeader = (): HTMLAnchorElement[] => Array.from($id('tag-container')!.getElementsByClassName('mm-tag-anchor')) as HTMLAnchorElement[];

const tagIdFromAnchor = (anchor: HTMLAnchorElement): string => anchor.pathname.split('/').pop()!;

const insertNewTagAnchorBadge = (child: HTMLElement) => {
    const searchInput = $id('tag-searcher-trigger')!;
    searchInput.parentNode!.insertBefore(child, searchInput); // Insert before the "+" button.
};

const createNewTagAnchorBadge = ({ full, some, warn, name }: PostApiThreadTagResponse, tagId: string) => {
    if (!some) { throw Error('Tag gone'); }
    // LINT.IfChange(tag_badges)
    return $('span', 'position-relative d-inline-block', [
        $('a', { className: 'mm-tag-anchor badge rounded-pill bg-secondary me-2' + (full ? '' : ' opacity-50'), href: `/tag/${tagId}` }, name, showThreadTagPopFromHeaderAnchor).also(anchor => {
            anchor.setAttribute('data-toggle', 'dropdown');
            if (warn) {
                anchor.appendChild($('span', 'material-icons align-middle', 'report_problem')).also(span => { span.style.fontSize = '85%'; });
            }
        }),
        $('div', 'dropdown-menu')
    ]);
    // LINT.ThenChange(../templates/thread.tsx:tag_badges)
};

const initSearchForNewTag = (model: TagListModel, input: HTMLInputElement) => {
    const allSelections = () => {
        const selected: { [tagId: string]: true } = {};
        for (const anchor of allAnchorsInHeader()) {
            const idStr = tagIdFromAnchor(anchor);
            if (idStr) {
                selected[idStr] = true;
            }
        }
        return selected;
    };

    const suggestions: string[] = [];
    const updateSuggestions = () => {
        suggestions.splice(0, suggestions.length, ...model.recommend(allSelections(), document.getElementById('thread-title')!.textContent || '', document.querySelector('.mm-post > .card-body')!.textContent, {
            _prop_defaultWarningBonus: 0,
            _prop_nonArbitraryPenalty: 0.025
        }));
    };
    updateSuggestions();

    const addTag = (tagId: string, skipContestConfirmation?: boolean): void => {
        if (allSelections()[tagId]) { return; }

        if ((model.tagMapById[tagId]._prop_category === CATS_CAMPAIGN) && !skipContestConfirmation) {
            contestCampaignTagConfirmationDialog(confirmed => {
                if (confirmed) { addTag(tagId, true); }
            });
            return;
        }

        const name = model.tagMapById[tagId]._prop_names[0];
        const tempBadge = $('a', { className: 'mm-tag-anchor badge rounded-pill bg-secondary me-2 opacity-50', href: `/tag/${tagId}` }, loadingSpinner(true, name), e => e.preventDefault());
        insertNewTagAnchorBadge(tempBadge);

        updateSuggestions();

        $xhr(`/api/thread/${getThreadIdFromUri()}/tag/${tagId}`, {
            f: 'vote=1',
            e: e => {
                window.alert('添加标签失败啦，请刷新试试？错误信息：' + e);
                tempBadge.parentNode!.removeChild(tempBadge);
                updateSuggestions();
            },
            j: (response: PostApiThreadTagResponse) => {
                if (!response.some) {
                    window.alert('该标签已被较多用户反对，需要收到更多赞成票才会显示。');
                    tempBadge.parentNode!.removeChild(tempBadge);
                    return;
                }
                tempBadge.parentNode!.replaceChild(createNewTagAnchorBadge(response, tagId), tempBadge);
            }
        });
    };

    initializeSearcher(input, model, suggestions, tagId => {
        const tag = model.tagMapById[tagId];
        if (tag._prop_flags & 0x2) {
            requireChildren(tag, addTag);
        } else { addTag(tagId); }
    }, true);

    setTimeout(() => input.click()); // Trigger the dropdown to show.
};

const tagAttaching = () => {
    const container = $id('tag-searcher-parent')!;
    const input = container.firstElementChild as HTMLInputElement;

    container.previousElementSibling!.classList.add('d-none'); // The + button.
    container.style.setProperty('display', 'inline-block', 'important');

    const hideInput = () => {
        container.previousElementSibling!.classList.remove('d-none');
        container.style.removeProperty('display');
    };
    setTimeout(() => input.focus());

    let closingTimeout: ReturnType<typeof setTimeout> | undefined;
    input.addEventListener('blur', () => {
        closingTimeout = setTimeout(hideInput, 250);
    });
    input.addEventListener('focus', () => clearTimeout(closingTimeout));

    loadTagListModel(false, res => {
        if (typeof res === 'string') { window.alert('标签列表加载失败了：' + res); hideInput(); return; }
        initSearchForNewTag(res, input);
    });
};

const showThreadTagPopUpImpl = (
    anchor: HTMLElement,
    tagId: string,
    onSuccess: (response: PostApiThreadTagResponse, vote: '-1' | '0' | '1') => void
) => {
    const dropdown = anchor.nextElementSibling as HTMLElement;
    $set(dropdown, $('div', 'px-2 text-center', loadingSpinner(false)));

    const reload = () => {
        $xhr(`/api/thread/${getThreadIdFromUri()}/tag/${tagId}`, {
            t: html => {
                const fragment = $('div', { innerHTML: html });
                const newDropdown = fragment.querySelector('.dropdown-menu')!;
                const buttons = Array.from(fragment.getElementsByTagName('button'));
                const vote = (e: MouseEvent) => {
                    const voteStr = (e.currentTarget as HTMLButtonElement).name.substring(5) as ('-1' | '0' | '1'); // First 5 chars always "vote-".
                    for (const button of buttons) {
                        button.disabled = true;
                    }
                    $xhr(`/api/thread/${getThreadIdFromUri()}/tag/${tagId}`, {
                        f: 'vote=' + voteStr,
                        e: e => window.alert('投票失败啦！或许刷新试试？错误信息：' + e),
                        j: res => { onSuccess(res, voteStr); reload(); }
                    });
                };
                for (const button of buttons) {
                    button.addEventListener('click', vote);
                }
                $set(dropdown, Array.from(newDropdown.children) as HTMLElement[]);
            },
            e: error => $set(dropdown, $('div', 'px-2 text-center', error))
        });
    };

    reload();
};

const updateAnchorAccodingToResponse = (anchor: HTMLAnchorElement, { full, some }: PostApiThreadTagResponse) => {
    if (full) {
        anchor.style.opacity = '1';
    } else if (some) {
        anchor.style.opacity = '0.5';
    } else {
        const parent = anchor.parentNode!; // The inline-block wrapper.
        parent.parentNode!.removeChild(parent);
    }
};

const showThreadTagPopFromHeaderAnchor = (e: MouseEvent) => {
    e.preventDefault();

    const anchor = e.currentTarget as HTMLAnchorElement;
    showThreadTagPopUpImpl(anchor, tagIdFromAnchor(anchor), response => updateAnchorAccodingToResponse(anchor, response));
};

const showThreadTagPopFromModalCandidateList = (onEditDone: (name: string) => void) => (e: Event) => {
    const button = e.currentTarget as HTMLElement;
    const id = button.getAttribute('data-id')!;
    showThreadTagPopUpImpl(button, id, (response, vote) => {
        const currentAnchors = allAnchorsInHeader();
        const anchor = currentAnchors.find(single => tagIdFromAnchor(single) === id);
        if (anchor) {
            updateAnchorAccodingToResponse(anchor, response);
        } else if (response.some) {
            insertNewTagAnchorBadge(createNewTagAnchorBadge(response, id));
        }
        const materialIconElement = button.firstElementChild!;
        // LINT.IfChange(material_cb)
        materialIconElement.replaceChild(document.createTextNode(response.full ? 'check_box' : (response.some ? 'indeterminate_check_box' : 'check_box_outline_blank')), materialIconElement.firstChild!);
        // LINT.ThenChange(../templates/tag.tsx:material_cb)
        const iVoted = !!parseInt(vote);
        materialIconElement.classList[iVoted ? 'add' : 'remove']('text-primary');
        onEditDone(response.name);
    });
};

// LINT.IfChange(tab_ids)
const thread_only_LOG_TAB = 'thread-tag-logs';
const thread_only_BROWSE_TAB = 'thread-add-tags';
// LINT.ThenChange(../templates/tag.tsx:tab_ids)

export const initThreadTags = () => {
    for (const anchor of allAnchorsInHeader()) {
        anchor.addEventListener('click', showThreadTagPopFromHeaderAnchor);
        anchor.setAttribute('data-toggle', 'dropdown');
        anchor.parentNode!.appendChild($('div', 'dropdown-menu'));
    }

    const searchButton = $id('tag-searcher-trigger') as HTMLAnchorElement;
    searchButton.href = 'javascript:void(0)';
    searchButton.addEventListener('click', tagAttaching);

    const popButton = $id('tag-searcher-pop') as HTMLAnchorElement;
    popButton.addEventListener('click', () => {
        const container = $('div', undefined, loadingSpinner(false));
        showModal('此小说的标签', container);
        $xhr('/api/thread/' + getThreadIdFromUri() + '/tags', {
            t: text => {
                let changeTagNames: string[] = [];
                const onTagButtonClick = showThreadTagPopFromModalCandidateList(name => { if (!changeTagNames.includes(name)) { changeTagNames.push(`「${name}」`); } });
                multiTabModal(container, text, [
                    { _prop_label: '修改标签', _prop_id_in_html: thread_only_BROWSE_TAB },
                    { _prop_label: '标签操作记录', _prop_id_in_html: thread_only_LOG_TAB }
                ], tab => {
                    if (tab.id === thread_only_BROWSE_TAB) {
                        for (const element of Array.from(tab.getElementsByClassName('mm-tag-fcbox'))) {
                            element.addEventListener('click', onTagButtonClick);
                            element.setAttribute('data-toggle', 'dropdown');
                            element.parentNode!.appendChild($('div', 'dropdown-menu'));
                        }
                    } else if ((tab.id === thread_only_LOG_TAB) && changeTagNames.length) {
                        if (!tab.firstElementChild!.classList.contains('alert-secondary')) {
                            tab.insertBefore($('div', 'alert alert-secondary', `你刚才对${changeTagNames.join('、')}标签的操作暂未显示于此；你需要关闭此弹窗重新打开，才能在记录中看到它们。`), tab.firstChild);
                        }
                    }
                });
            },
            e: () => container.replaceChild($('div', 'alert alert-danger', '加载失败啦 °(°ˊДˋ°) °'), container.firstChild!)
        });
    });
};
