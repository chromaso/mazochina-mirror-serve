import * as elements from 'typed-xhtml';
import { HtmlTmpl } from '../utils/tsx_utils';

// Migrated from admin_password.inc.pug.
export const adminPassword: HtmlTmpl<{
    updated: { user_id: number, user_name: string, password: string } | null,
    nearMatches:  { user_id: number, user_name: string, has_password: boolean, email: string }[] | null
}> = (ctx, self) => (
    <div class="container">
        {self.updated ? (
            <div class="alert alert-info">
                <a href={'/author/' + self.updated.user_id}>
                    {self.updated.user_name}
                </a>
                {' 的密码已被更改至 '}
                {self.updated.password}
            </div>
        ) : null}
        {self.nearMatches ? (
            <div class="alert alert-warning">
                <p>没有完全符合条件的用户。以下为近似结果：</p>
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">用户名</th>
                            <th scope="col">密码？</th>
                            <th scope="col">邮箱</th>
                        </tr>
                    </thead>
                    <tbody>
                        {self.nearMatches.map(user => (
                            <tr>
                                <th scope="row">{user.user_id}</th>
                                <td>{user.user_name}</td>
                                <td>{user.has_password ? 'Y' : 'N'}</td>
                                <td>{user.email}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        ) : null}
        <form method="post">
            <div class="form-group row">
                <label for="user-name" class="col-4 col-form-label">
                    用户名
                </label>
                <div class="col-8">
                    <input
                        id="user-name"
                        name="username"
                        type="text"
                        class="form-control"
                    />
                </div>
            </div>
            <div class="form-group row">
                <label for="email" class="col-4 col-form-label">
                    邮箱
                </label>
                <div class="col-8">
                    <input
                        id="email"
                        name="email"
                        type="email"
                        class="form-control"
                    />
                </div>
            </div>
            <div class="form-group row">
                <label for="password" class="col-4 col-form-label">
                    新密码
                </label>
                <div class="col-8">
                    <input
                        id="password"
                        name="password"
                        type="text"
                        class="form-control"
                    />
                </div>
            </div>
            <div class="form-group text-center">
                <button id="submit-button" type="submit" class="btn btn-primary">
                    修改
                </button>
            </div>
        </form>
    </div>
);