import * as elements from 'typed-xhtml';
import { HtmlTmpl } from '../utils/tsx_utils';
import { ConfigPanel, ThemeName, themeToUrl, ALL_THEMES, DEFAULT_THEMES } from '../shared/options';

// Migrated from options.pug.
export const configPanel: HtmlTmpl<ConfigPanel> = (ctx, self) => {
    const loginUser = ctx.state.mazoLoginUser;
    return (
        <div>
            <div class="form-group row">
                <label for="theme-select" class="col-4 col-form-label">
                    风格
                </label>
                <div class="col-8">
                    <input id="theme-entry" type="text" class="form-control" value={self.selectedTheme} readonly />
                </div>
            </div>
            <div class="form-group row">
                <label for="tz-select" class="col-4 col-form-label">
                    时区
                </label>
                <div class="col-8">
                    <select id="tz-select" name="tz" class="form-control">
                        {self.tzs.map((tz, index) => (
                            <option value={String(index - 12)} selected={index - 12 === self.selectedTz}>
                                {tz}
                            </option>     
                        ))}
                    </select>
                    <div id="tz-warning" class="invalid-feedback">
                        你设备的时区为 
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label for="date-format-select" class="col-4 col-form-label">
                    日期格式
                </label>
                <div class="col-8">
                    <select id="date-format-select" name="tfmt" class="form-control">
                        {self.tfmtPreviews.map((preview, index) => (
                            <option value={String(index)} selected={index === self.selectedTfmt}>
                                {preview.join('，')}
                            </option>
                        ))}
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="lf-select" class="col-4 col-form-label">
                    主题列表默认过滤
                    <span class="mazomirror-tip" data-title="这里设置的是默认过滤选项，浏览时可以在列表底部临时切换">
                        <span class="material-icons std-icon">help</span>
                    </span>
                </label>
                <div class="col-8">
                    <select id="lf-select" name="lf" class="form-control">
                        <option value="-1" selected={self.lf === -1}>
                            完全不显示广告帖 - 完全不显示被半隐藏或隐藏的主题
                        </option>
                        <option value="0" selected={self.lf === 0}>
                            显示半隐藏广告帖 -
                            显示被半隐藏的广告帖（半透明格式），完全不显示被隐藏的帖子
                        </option>
                        <option value="1" selected={self.lf === 1}>
                            显示全部主题 -
                            显示所有主题，即使是被隐藏或半隐藏的广告帖、重复帖和违规帖（半透明格式）
                        </option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="forum-page-select" class="col-4 col-form-label">
                    主题列表页
                </label>
                <div class="col-8">
                    <select id="forum-page-select" name="fp" class="form-control">
                        {self.forumPc.map(fp => (
                            <option value={String(fp)} selected={fp === self.selectedForumPc}>
                                每页 {fp} 个主题
                            </option>    
                        ))}
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="thread-page-select" class="col-4 col-form-label">
                    主题内容页
                </label>
                <div class="col-8">
                    <select id="thread-page-select" name="tp" class="form-control">
                        {self.threadPc.map(tp => (
                            <option value={String(tp)} selected={tp === self.selectedThreadPc}>
                                每页 {tp} 篇文章
                            </option>
                        ))}
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="cc-select" class="col-4 col-form-label">
                    语言
                </label>
                <div class="col-8">
                    <select id="cc-select" name="cc" class="form-control">
                        {Object.entries(self.ccs).map(([val, display]) => (
                            <option value={`"${val}"`} selected={val === self.selectedCc}>
                                {display}
                            </option>
                        ))}
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="tags-settings-link" class="col-4 col-form-label">
                    标签
                </label>
                <div class="col-8">
                    <a id="tags-settings-link" class="btn btn-link" href="/tags/settings">
                        标签功能相关设置
                    </a>
                </div>
            </div>
            {loginUser ? (
                 <div class="form-group row">
                    <label for="hc-select" class="col-4 col-form-label">
                        用户信息卡
                    </label>
                    <div class="col-8">
                        <select id="hc-select" name="hc" class="form-control">
                            <option value="0" selected={self.hc === 0}>
                                不显示
                            </option>
                            <option value="1" selected={self.hc === 1}>
                                点击时显示
                            </option>
                            <option value="2" selected={self.hc === 2}>
                                鼠标悬停时显示
                            </option>
                        </select>
                    </div>
                </div>
            ) : null}       
            {loginUser ? (
                <div class="form-group">
                    <div class="form-check form-switch">
                        <input
                            id="sync-check"
                            type="checkbox"
                            name="sync"
                            checked={self.sync}
                            class="form-check-input"
                        />
                        <label for="sync-check" class="form-check-label">
                            在不同设备间同步以上设置
                        </label>
                    </div>
                </div>
            ) : null}
        </div>
    );
};

export const preview: HtmlTmpl<{ selectedTheme: ThemeName }> = (ctx, self) => (<>
    <div class="mb-3">
        <label for="swc-color-preview">
            <span class="material-icons std-icon">light_mode</span>
        </label>
        <div class="form-check form-switch d-inline-block align-middle ms-1 me-0">
            <input class="form-check-input" type="checkbox" role="switch" id="swc-color-preview" />
        </div>
        <label for="swc-color-preview">
            <span class="material-icons std-icon">dark_mode</span>
        </label>
        （显示各风格在<span id="swc-color-text">浅</span>色模式下的预览）
    </div>
    <form action="/theme" method="post" class="row gy-4 text-center justify-content-center">
        {Array.from(ALL_THEMES).sort().map(theme => (
            <div class="d-inline-block col-auto">
                <label for={'theme-click-' + theme} style="width: 216px; height: 190px" class="overflow-hidden">
                    <div style="transform: scale(0.4); transform-origin: top left">
                        <iframe src={'/theme/' + theme} width="540" height="475" />
                    </div>
                </label>
                <br />
                <input id={'theme-click-' + theme} type="submit" class="btn btn-primary" name="themeaction" value={((theme === self.selectedTheme) ? '当前使用' : '使用') + ' ' + theme} disabled={theme === self.selectedTheme} />
                {DEFAULT_THEMES.includes(theme) ? <span class="material-icons std-icon ms-1">verified</span> : null}
            </div>
        ))}
    </form>
</>);

export const previewTheme: HtmlTmpl<{
    theme: ThemeName
}> = (ctx, { theme }) => 
    elements.safeHtml(`<!DOCTYPE html><html lang="zh">${
        <head>
            <meta name='viewport' content='width=device-width, initial-scale=1' />
            <link rel='stylesheet' type='text/css' href={themeToUrl(theme)} />
            <link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/icon?family=Material+Icons' />
            <style type="text/css">{elements.safeHtml(`a.badge { text-decoration: none; } .ui-link, .breadcrumb-item a { text-decoration: none; } .btn > .material-icons { font-size: 120%; vertical-align: middle; }`)}</style>
        </head>
    }${
        <body>
            <nav class="navbar bg-body-tertiary">
                <div class="container position-relative flex-nowrap" style="min-width: 0">
                    <a href="javascript:void(0)" class="navbar-brand">M系镜像</a>
                    <span class="text-nowrap"><a role="button" href="javascript:void(0)" title="搜索" class="btn btn-link btn-sm"><span class="material-icons">search</span></a><a class="btn btn-link btn-sm" role="button" href="javascript:void(0)" title="通知"><span class="material-icons">notifications</span></a></span>
                </div>
            </nav>
            <div class="container my-4">
                <nav class="mb-4"><ol class="breadcrumb"><li class="breadcrumb-item"><a href="javascript:void(0)">首页</a></li><li class="breadcrumb-item active">灌水区</li></ol></nav>
                <div class="d-flex justify-content-between align-items-center mb-4"><h1 class="my-0">灌水区</h1><form action="/post/f22" method="post" class="d-inline"><button type="submit" class="btn btn-primary"><span class="material-icons me-0">post_add</span></button></form></div>
                <table class="table">
                    <thead><tr><th>主题</th><th class="text-nowrap text-end">回复</th></tr></thead>
                    <tbody>
                        <tr><td class="align-middle"><a href="javascript:void(0)" class="ui-link">沉沦</a><small class="d-block mt-1"><span>最后发表：<a href="javascript:void(0)" class="ui-link">chromaso @ <time>01-30</time></a></span></small></td><td class="align-middle text-nowrap text-end">10</td></tr>
                    </tbody>
                </table>
                <ul class="pagination justify-content-center d-sm-none"><li class="page-item"><a href="javascript:void(0)" class="page-link">1</a></li><li class="page-item active"><a href="javascript:void(0)" class="page-link">2</a></li><li class="page-item"><a href="javascript:void(0)" class="page-link">3</a></li><li class="page-item disabled" style="position: relative;"><a href="javascript:void(0)" class="page-link">…</a></li><li class="page-item"><a href="javascript:void(0)" class="page-link">100</a></li></ul>
                <div class="text-center mt-3"><button class="btn btn-primary"><span class="material-icons me-1">post_add</span>发表主题</button><button  class="btn btn-secondary ms-3"><span class="material-icons">filter_list</span></button></div>
                <div class="alert alert-secondary my-3">这是 {theme} 风格的效果预览；上方为主题列表页样式。下方为主题内容页样式。</div>
                <h1 id="thread-title" class="mb-3">沉沦</h1>
                <h4 id="tag-container" class="mb-4"><span class="position-relative d-inline-block"><a href="/tag/8" class="badge rounded-pill bg-secondary me-2">短篇</a><div class="dropdown-menu"></div></span><span class="position-relative d-inline-block"><a href="/tag/15" class="badge rounded-pill bg-secondary me-2">原创</a><div class="dropdown-menu"></div></span><span class="position-relative d-inline-block"><a href="/tag/108" class="badge rounded-pill bg-secondary me-2">足控</a><div class="dropdown-menu"></div></span><span class="position-relative d-inline-block"><a href="/tag/115" class="badge rounded-pill bg-secondary me-2">踩踏</a></span></h4>
                <div class="card my-4"><div class="card-header d-flex justify-content-between"><div class="rounded-circle bg-info bg-gradient" style="width: 48px; height: 48px" /><div class="flex-shrink-1 flex-grow-1 ms-1 align-self-start"><a href="javascript:void(0)" class="ui-link">郁达夫</a><div class="text-muted">沉沦（五）</div></div><span class="badge bg-secondary ms-1 align-self-center">仅镜像</span></div><div class="card-body">秋天又到了。浩浩的苍空，一天一天的高起来。他的旅馆旁边的稻田，都带起黄金色来。朝夕的凉风，同刀也似的刺到人的心骨里去，大约秋冬的佳日，来也不远了。<br />一礼拜前的有一天午后，他拿了一本 Wordsworth 的诗集，在田塍路上逍遥漫步了半天。从那一天以后，他的循环性的忧郁症，尚未离他的身过。前几天在路上遇着的那两个女学生，常在他在风气纯良，不与市井小人同处，清闲雅淡的地方，过日子正如做梦一样。他到了Ｎ市之后，转瞬之间，已经有半年多了。</div><div class="card-footer d-flex justify-content-between align-items-center"><span class="text-muted">1 世纪前</span><span class="flex-shrink-0 text-end"><button type="submit" class="btn btn-secondary btn-sm"><span class="material-icons me-1">format_quote</span>引用</button><button class="btn btn-secondary btn-sm ms-2"><span class="material-icons">more_horiz</span></button></span></div></div>
            </div>
        </body>
    }</html>`);