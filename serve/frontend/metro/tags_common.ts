import { $, $set, $xhr, closeDropdownMenuIfSourceInside, showModal, tipper } from '../common-import';

const CATS_FIRST_ARBITRARY = 4; // This needs to be consistent with the backend value.
export const CATS_CAMPAIGN = 8; // This needs to be consistent with the backend value.

const DEBUG = false;

// FIXME: Temporary fix only! Long term solution is for tipper to support HTML!
const htmlToText = (innerHTML: string) => innerHTML.includes('<') ? $('div', { innerHTML }).textContent! : innerHTML;

// https://en.wikipedia.org/wiki/Longest_common_substring#Dynamic_programming
const longestCommonSubstrings = (str1: string, str2: string): string[] => {
    const L: number[][] = [];
    for (let i = 0; i <= str1.length; i++) {
        L[i] = [];
    }
    let z = 0;
    let ret: string[] = [];
    for (let i = 0; i < str1.length; i++) {
        for (let j = 0; j < str2.length; j++) {
            if (str1[i] === str2[j]) {
                const Lij = (i && j) ? L[i - 1][j - 1] + 1 : 1;
                L[i][j] = Lij;
                if (Lij < z) { continue; }
                const substr = str1.substring(i - Lij + 1, i + 1);
                if (Lij > z) {
                    z = Lij;
                    ret = [substr];
                } else {
                    ret.push(substr);
                }
            } else { L[i][j] = 0; }
        }
    }
    return ret;
};

const highlight = (text: string, keywordLowerCase: string): (string | HTMLElement)[] => {
    const index = text.toLowerCase().indexOf(keywordLowerCase);
    if (index < 0) { return [text]; }
    return [text.substring(0, index), $('b', undefined, [text.substring(index, index + keywordLowerCase.length)]), text.substring(index + keywordLowerCase.length)];
};

type TagWiring = [number, [string, ...string[]], number, string, number[] | null];

type Tag = Readonly<{
    _prop_id: number;
    _prop_names: [string, ...string[]];
    _prop_flags: number;
    _prop_category: number;
    _prop_descHtml: string;
    _prop_children: Tag[];
    _prop_parents: Tag[];
    _prop_basicScore: number;
}>;

type RecommendationTweakParams = {
    _prop_nonArbitraryPenalty: number; // for >=1, not showing them at all.
    _prop_defaultWarningBonus: number;
};

export type TagListModel = {
    tagMapById: Record<string, Tag>;
    recommend: (selectedTagIds: Readonly<Record<string, unknown>>, title: string, content: string | null, tweakParams: RecommendationTweakParams) => string[];
};

const tagListModel = (tags: Readonly<TagWiring[]>, exclCampaign: boolean): TagListModel => {
    const tagMapById: { [tagId: string]: Tag } = {};
    for (let index = 0; index < tags.length; index++) {
        const tag = tags[index];
        const flagAndCat = tag[2];
        const cat = flagAndCat >> 3;
        if (exclCampaign && (cat === CATS_CAMPAIGN)) { continue; }
        tagMapById[tag[0]] = Object.freeze({
            _prop_id: tag[0],
            _prop_names: tag[1],
            _prop_flags: flagAndCat & 0x7,
            _prop_category: flagAndCat >> 3,
            _prop_descHtml: tag[3],
            _prop_children: [],
            _prop_parents: [],
            _prop_basicScore: 1 - index / tags.length + 0.25 * Math.random()
        });
    }
    for (const tag of tags) {
        tag[4]?.forEach(childId => {
            const parent = tagMapById[tag[0]];
            const child = tagMapById[childId];
            parent._prop_children.push(child);
            child._prop_parents.push(parent);
        });
    }

    return {
        tagMapById,
        recommend: (selectedTagIds, title, content, tweakParams) => {
            const scoreByTagIdStr: [number, string][] = [];
            const normalizedTitle = title.toLowerCase();
            const normalizedContent = (content || '').toLowerCase();
            for (const tagId of Object.keys(tagMapById)) {
                if (selectedTagIds.hasOwnProperty(tagId)) { continue; }
                const tag = tagMapById[tagId];
                // Basic score (0 - 1, linear, depending on the order in the list), plus a 0.25 random.
                let score = tag._prop_basicScore;
                if (DEBUG) {
                    console.debug(`Basic score for ${tag._prop_names[0]} ${score}`);
                }
                if ((tag._prop_category < CATS_FIRST_ARBITRARY)) {
                    if (tweakParams._prop_nonArbitraryPenalty >= 1) {
                        continue;
                    }
                    score -= tweakParams._prop_nonArbitraryPenalty;
                    if (DEBUG) {
                        console.debug(`Non-arbitrary penalty ${tweakParams._prop_nonArbitraryPenalty}`);
                    }
                }
                if (tag._prop_flags & 0x1) {
                    score += tweakParams._prop_defaultWarningBonus;
                    if (DEBUG) {
                        console.debug(`Default-warning bonus ${tweakParams._prop_defaultWarningBonus}`);
                    }
                }
                // Content keyword matching.
                let titleMatchCount = 0;
                let contentMatchCount = 0;
                for (const name of tag._prop_names) {
                    const lengthMultiplier = Math.log(name.length + 0.025) * Math.E;
                    const normalizedName = name.toLowerCase();
                    titleMatchCount += lengthMultiplier * (normalizedTitle.split(normalizedName).length - 1);
                    contentMatchCount += lengthMultiplier * (normalizedContent.split(normalizedName).length - 1);
                }
                // e.g. for 2-char name appearing once in title, 3.39; for 3-char name appearing twice in content 1.95.
                const keywordSubtotal = Math.log(1 + titleMatchCount * 15 + contentMatchCount);
                if (DEBUG) {
                    console.debug(`Keyword subtotal ${keywordSubtotal} (${titleMatchCount} + ${contentMatchCount})`);
                }
                score += keywordSubtotal;
                // If any parent/ancestors is already selected, recommend. If a sibling at any level is selected, slightly recommend.
                const parentOrSiblingScores: number[] = [];
                const doParents = (tags: Tag[], bonus: number) => {
                    for (const parentTag of tags) {
                        if (selectedTagIds.hasOwnProperty(parentTag._prop_id)) {
                            parentOrSiblingScores.push(bonus);
                        } else {
                            doParents(parentTag._prop_parents, bonus * 0.75);
                            for (const sibling of parentTag._prop_children) {
                                if (selectedTagIds.hasOwnProperty(sibling._prop_id)) {
                                    parentOrSiblingScores.push(bonus * 0.5);
                                }
                            }
                        }
                    }
                };
                doParents(tag._prop_parents, 1.25);
                const parentSubtotal = parentOrSiblingScores.sort((a, b) => b - a).reduce((a, c, i) => a + c / Math.pow(Math.E, i), 0);
                if (DEBUG) {
                    console.debug(`Parent subtotal ${parentSubtotal}`);
                }
                score += parentSubtotal;
                // If any child/descendant is already selected, do not recommend.
                const childScores: number[] = [];
                const doChildren = (tags: Tag[], bonus: number) => {
                    for (const childTag of tags) {
                        if (selectedTagIds.hasOwnProperty(childTag._prop_id)) {
                            childScores.push(bonus);
                        } else {
                            doChildren(childTag._prop_children, bonus * 0.75);
                        }
                    }
                };
                doChildren(tag._prop_children, 0.5);
                const childSubtotal = childScores.sort((a, b) => b - a).reduce((a, c, i) => a + c / Math.pow(Math.E, i), 0);
                if (DEBUG) {
                    console.debug(`Child subtotal minus ${childSubtotal}`);
                }
                score -= childSubtotal;
                if (DEBUG) {
                    console.debug(`Final score for ${tag._prop_names[0]} ${score}`);
                }
                scoreByTagIdStr.push([score, tagId]);
            }
            return scoreByTagIdStr.sort((a, b) => b[0] - a[0]).map(pair => pair[1]);
        }
    };
};

export const loadTagListModel = (exclCampaign: boolean, callback: (modelOrError: TagListModel | string) => void) =>
    $xhr('/api/tags', {
        j: res => callback(tagListModel(res, exclCampaign)),
        e: callback,
        c: true
    });

export const initializeSearcher = (input: HTMLInputElement, model: TagListModel, suggestions: string[], onSelectTag: (tagId: string) => void, showListOnEmpty: boolean) => {
    const dropdown = input.nextElementSibling as HTMLDivElement;

    const selectOption = (e: Event) => {
        e.preventDefault(); // Don't actually change location.hash.
        const element = e.currentTarget as HTMLAnchorElement;
        element.parentNode?.removeChild(element);
        onSelectTag(element.hash.split('-').pop()!);
        input.value = '';
        setTimeout(() => input.focus());
    };
    let keyboardSelectionTarget: HTMLAnchorElement | undefined;

    let lastParameters = '';
    const onSearchChange = () => {
        const text = input.value.trim().substring(0, 16).toLowerCase();
        const parameters = text + '|' + suggestions.join(',');
        if (parameters === lastParameters) { return; }
        lastParameters = parameters;

        keyboardSelectionTarget = undefined;
        let searchResults: { _prop_tag: Tag, _prop_alias?: string }[];
        if (!text.length) {
            if (showListOnEmpty) {
                searchResults = suggestions.map(id => ({ _prop_tag: model.tagMapById[id] })).slice(0, 5);
            } else {
                $set(dropdown, $('a', {
                    className: 'dropdown-item disabled'
                }, '输入文字以搜索…'));
                return;
            }
        } else {
            const searchSuggestions: { _prop_tag: Tag, _prop_index: number, _prop_score: number, _prop_alias: string | undefined }[] = [];
            suggestions.forEach((tagId, index) => {
                const tag = model.tagMapById[tagId];
                let maxAliasScore = 0;
                let maxAliasScoreFor: string | undefined;
                for (const name of tag._prop_names) {
                    let aliasScore = 0;
                    const normalizedName = name.toLowerCase();
                    const exactMatchIndex = normalizedName.indexOf(text);
                    if (exactMatchIndex >= 0) {
                        aliasScore = (exactMatchIndex ? 10000 : 15000) - // Prefer a match at the start. 
                                     (normalizedName.length - text.length); // Prefer "exacter" match (i.e. extra part shorter).
                    } else {
                        const commonSubstrings = longestCommonSubstrings(normalizedName, text);
                        if (!commonSubstrings.length) { continue; }
                        // De-dup the substrings.
                        const substringsCount: Record<string, number> = {};
                        for (const substring of commonSubstrings) {
                            if (substringsCount.hasOwnProperty(substring)) {
                                substringsCount[substring]++;
                            } else {
                                substringsCount[substring] = 1;
                            }
                        }
                        Object.keys(substringsCount).forEach(k => {
                            aliasScore += 10 * Math.log(1 + substringsCount[k]);
                        });
                        aliasScore += 100 * commonSubstrings[0].length - normalizedName.length / 2;  // Prefer "exacter" match (i.e. extra part shorter).
                    }
                    if (aliasScore > maxAliasScore) {
                        maxAliasScore = aliasScore;
                        maxAliasScoreFor = name;
                    }
                }
                if (maxAliasScoreFor) {
                    searchSuggestions.push({
                        _prop_tag: tag,
                        _prop_index: index,
                        _prop_score: maxAliasScore,
                        _prop_alias: (maxAliasScoreFor === tag._prop_names[0]) ? undefined : maxAliasScoreFor
                    })
                }
            });
            searchResults = searchSuggestions.sort((a, b) => (b._prop_score - a._prop_score) || (a._prop_index - b._prop_index)).slice(0, 5);
        }

        if (!searchResults.length) {
            $set(dropdown, $('a', {
                className: 'dropdown-item disabled'
            }, '未找到匹配的标签'));
        } else {
            $set(dropdown, []);
            for (const result of searchResults) {
                const body = result._prop_alias ? [
                    result._prop_tag._prop_names[0],
                    $('i', undefined, ['（别名：', ...highlight(result._prop_alias, text), '）'])
                ] : highlight(result._prop_tag._prop_names[0], text);
                if (result._prop_tag._prop_descHtml) {
                    body.push(tipper(htmlToText(result._prop_tag._prop_descHtml)).also(tip => tip.classList.add('ms-1')));
                }
                dropdown.appendChild($('a', {
                    className: 'dropdown-item',
                    href: '#tag-sel-' + result._prop_tag._prop_id
                }, body, selectOption));
            }
            keyboardSelectionTarget = dropdown.firstElementChild as HTMLAnchorElement;
            keyboardSelectionTarget.classList.add('active');
        }
    };
    input.addEventListener('focus', onSearchChange);
    input.addEventListener('change', onSearchChange);
    input.addEventListener('input', () => { onSearchChange(); input.click(); });
    input.addEventListener('keydown', e => {
        switch (e.keyCode) {
            case 0x28: // down
                if (keyboardSelectionTarget) {
                    keyboardSelectionTarget.classList.remove('active');
                    keyboardSelectionTarget = (keyboardSelectionTarget.nextElementSibling ?? keyboardSelectionTarget.parentNode?.firstElementChild) as (HTMLAnchorElement | undefined);
                    keyboardSelectionTarget?.classList.add('active');
                }
                break;
            case 0x26: // up
                if (keyboardSelectionTarget) {
                    keyboardSelectionTarget.classList.remove('active');
                    keyboardSelectionTarget = (keyboardSelectionTarget.previousElementSibling ?? keyboardSelectionTarget.parentNode?.lastElementChild) as (HTMLAnchorElement | undefined);
                    keyboardSelectionTarget?.classList.add('active');
                }
                break;
            case 0x1B: // esc
                closeDropdownMenuIfSourceInside(dropdown);
                break;
            case 0x0D: // enter
                if (keyboardSelectionTarget) {
                    const target = keyboardSelectionTarget;
                    keyboardSelectionTarget = undefined;
                    target.click();
                }
                break;
            default:
                return;
        }
        e.preventDefault();
    });
    onSearchChange();
};

export const displayTag = (tag: Tag): ((button: HTMLButtonElement) => void) => {
    let hint = htmlToText(tag._prop_descHtml);
    if (tag._prop_names.length > 1) {
        hint += '（别名：' + tag._prop_names.slice(1).join('、') + '）';
    }
    return button => {
        button.appendChild(document.createTextNode(tag._prop_names[0]));
        if (!hint) { return; }
        button.classList.add('mazomirror-tip');
        button.setAttribute('data-title', hint);
        button.appendChild($('span', 'material-icons ms-1 opacity-75', 'info')).also(e => { e.style.fontSize = '100%'; });
    };
};

export const requireChildren = (tag: Tag, addTagCallback: (tagId: string) => void) => {
    const modalCloser = showModal(`标签「${tag._prop_names[0]}」需要更具体指定`, [$('div', undefined, [
        $('p', 'mb-2', `标签「${tag._prop_names[0]}」太过笼统，仅为归纳整理所用。请从它的以下子标签中选择具体情境：`),
        $('div', undefined, tag._prop_children.map(child =>
            $('div', 'btn-group btn-group-sm me-1 mt-1', [
                $('button', { type: 'button', className: 'btn btn-primary' }).also(displayTag(child)),
                $('button', { type: 'button', className: 'btn btn-primary' }, '+')
            ], () => {
                addTagCallback(String(child._prop_id));
                modalCloser();
            })
        ))
    ])]);
};
