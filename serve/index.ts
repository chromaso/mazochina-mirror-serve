import Koa = require('koa');
import Router = require('koa-router');
import createError = require('http-errors');

import { IS_DEV_MODE as DEV_MODE } from './conf';
console.log(`Environment: ${DEV_MODE ? 'DEV' : 'PROD'}`);

import { bgTasks } from './bg_task';
import { unawaited } from '../common/logic_utils';
import { query as sql } from './db';
import { bodyParser, MazoContext, MazoKoaContext, MazoState, parseInt32 } from './utils/logic_utils';
import { forumsCache, listForums } from './shared/forum';
import * as options from './shared/options';
import { processReadStatus } from './utils/read_status';
import { error, LOGIN_REQUIRED, redirectWithDelay, redirectWithoutDelay, renderErrorToCtxBody } from './utils/render_utils';
import { attachCurrentLogin } from './shared/login';
import { initStatic } from './utils/tsx_utils';

import adminImpl = require('./endpoints/admin');
import attachmentImpl = require('./endpoints/attachment');
import authorImpl = require('./endpoints/author');
import avatarImpl = require('./endpoints/avatar');
import achievementsImpl = require('./endpoints/achievements');
import contestBackendImpl = require('./endpoints/contest_backend');
import contestResultImpl = require('./endpoints/contest_result');
import contestVoteImpl = require('./endpoints/contest_vote');
import favoriteImpl = require('./endpoints/favorite');
import forumImpl = require('./endpoints/forum');
import iastImpl = require('./endpoints/iast');
import indexImpl = require('./endpoints/index');
import loginImpl = require('./endpoints/login');
import moderationImpl = require('./endpoints/moderation');
import notificationImpl = require('./endpoints/notification');
import optionsImpl = require('./endpoints/options');
import postImpl = require('./endpoints/post');
import pmImpl = require('./endpoints/pm');
import questionnaireImpl = require('./endpoints/questionnaire');
import rootImpl = require('./endpoints/root');
import searchImpl = require('./endpoints/search');
import tagImpl = require('./endpoints/tag');
import threadImpl = require('./endpoints/thread');

const app = new Koa<MazoState, MazoKoaContext>();
app.proxy = true;

// Overriding the default handler. Basically just copied over.
app.context.onerror = function (err: createError.HttpError) {
    if (null == err) { return; }
    if (!(err instanceof Error)) { err = createError(500, `${err}`); }

    const userId = this.state.mazoLoginUser?.id ?? this.state.mazoGuest?.id ?? 0;

    let headerSent = false;
    if (this.headerSent || !this.writable) {
        headerSent = err.headerSent = true;
    }
    if (!err.expose) { // So it will be logged by Koa's default app.onerror.
        err.stack += `\n[${this.method} ${this.path}][U${userId}]`; // For better logging.
    }
    this.app.emit('error', err, this);
    if (headerSent) { return; }

    const { res } = this;
    res.getHeaderNames().forEach((name: string) => res.removeHeader(name));

    this.set(err.headers || {});
    this.type = 'text';

    let statusCode = err.status || err.statusCode;
    if ('ENOENT' === (err as NodeJS.ErrnoException).code) { statusCode = 404; }
    if ('number' !== typeof statusCode) { statusCode = 500; }

    let msg = err.expose ? err.message : `Unknown error @ ${Date.now()}`;
    if (err.expose && this.state.userConfig && !this.path.startsWith('/api/')) {
        try {
            renderErrorToCtxBody(this, err.message || `${err}`);
            if ((typeof this.body !== 'string') || !this.body.startsWith('<!DOCTYPE')) { throw new Error('Body format wrong.'); }
            msg = this.body;
            this.type = 'html';
        } catch (e) {
            console.error('Failed to generate an error page!', e, err);
        }
    } else if (msg === LOGIN_REQUIRED) {
        msg = '你尚未登入，此功能需要登入后才可使用';
        if (this.state.render) { // It's possible that .render is not assigned yet.
            msg = this.state.render.staticConv(msg);
        }
    }

    this.status = err.status = statusCode;
    this.length = Buffer.byteLength(msg);
    res.end(msg);
};

// koa-bodyparser has grown too heavy and troublesome to use. Parse manually instead.
app.use(bodyParser);

app.use(attachCurrentLogin);

app.use(async (ctx, next) => {
    if (`${ctx.headers['user-agent'] || ''}`.length > 400) {
        // Several of our logics stores UA. To be safe, check its length first.
        ctx.throw(413, 'Your user agent string is too long.');
    }

    const refDate = Math.floor(Date.now() / 1000);
    ctx.refDate = refDate;
    const configs = options.currentConfiguration(ctx);
    ctx.tc = configs.userConfig.cc.startsWith('tc');
    Object.assign(ctx.state, configs);
    ctx.state.render = options.renderHelpers(ctx);

    if (('notification' in ctx.query) && ctx.state.mazoLoginUser) {
        const notificationId = parseInt32(`${ctx.query.notification || ''}`);
        if (!isNaN(notificationId)) {
            unawaited(sql('UPDATE notifications SET dismissed = $1 WHERE notification_id = $2 AND user_id = $3', [refDate, notificationId, ctx.state.mazoLoginUser.id]), 'mark notification read');
        }
    }
    await processReadStatus(ctx); // Await as it MIGHT affect the rendering.

    await next();
});

const router = new Router<MazoState, MazoContext>();

router.get('/', indexImpl.home);
router.get('/achievement', achievementsImpl.achievement);
router.get('/admin/contest', contestBackendImpl.adminContest);
router.get('/admin/dash', adminImpl.adminDash);
router.get('/admin/guide', adminImpl.adminGuide);
router.get('/admin/password', rootImpl.adminPassword);
router.get('/admin/questionnaire', questionnaireImpl.adminQuestionnaire);
router.get('/admin/tag/audit', adminImpl.adminTagAudit);
router.get('/api/admin/dash/log/:file', adminImpl.apiAdminDashLog_file);
router.get('/api/admin/dash/timeseries/:stat', adminImpl.apiAdminDashTimeseries_stat);
router.get('/api/admin/dash/exttimeseries/:stat', adminImpl.apiAdminDashExttimeseries_stat);
router.get('/api/admin/dash/users', adminImpl.apiAdminDashUsers);
router.get('/api/author/:id', authorImpl.apiAuthor_id);
router.get('/api/author/:id/threads', authorImpl.apiAuthor_id_threads);
router.get('/api/author/:id/achievements', achievementsImpl.apiAuthor_id_achievements);
router.get('/api/collections', favoriteImpl.apiCollections);
router.get('/api/configure', optionsImpl.apiConfigure);
router.get('/api/contest/thread/:id/user/:userId?', contestBackendImpl.apiContestThread_id_user_id);
router.get('/api/forums', async ctx => { ctx.body = await listForums(); });
router.get('/api/iast/:id/:key', iastImpl.apiIast_id_key);
router.get('/api/notifications/v2', notificationImpl.apiNotifications_v2);
router.get('/api/pm/:id', pmImpl.apiPm_id);
router.get('/api/read/:id', threadImpl.apiRead_id);
router.get('/api/review/thread/:id', contestResultImpl.apiReviewThread_id);
router.get('/api/tags', tagImpl.apiTags);
router.get('/api/themes', optionsImpl.apiThemes);
router.get('/api/trivia', indexImpl.apiTrivia);
router.get('/api/thread/:id', threadImpl.apiThread_id);
router.get('/api/thread/:id/tag/:tagId', tagImpl.apiThread_id_tag);
router.get('/api/thread/:id/tags', tagImpl.apiThread_id_tags);
router.get('/api/thread/:id/vote', contestVoteImpl.apiThread_id_vote);
router.get('/api/thread/:id/votes', contestVoteImpl.apiThread_id_votes);
router.get('/api/voters/:id', contestResultImpl.apiVoters);
router.get('/api/votes', contestVoteImpl.apiVotes);
router.get('/author/:id', authorImpl.author_id);
router.get('/author/:id/favs/:page?', favoriteImpl.author_id_favs);
router.get('/collection/:id/:page?', favoriteImpl.collection_id_page);
router.get('/confirmation', authorImpl.confirmation);
router.get('/contest/panel', contestBackendImpl.contestPanel);
router.get('/forum/:id/:page?', forumImpl.forum_id_page);
router.get('/logout', loginImpl.logout);
router.get('/merge', loginImpl.merge);
router.get('/moderation/:page?', moderationImpl.moderation_page);
router.get('/modmail', ctx => redirectWithoutDelay(ctx, '/thread/1073744822'));
router.get('/theme/:theme', optionsImpl.theme_theme);
router.get('/pm', pmImpl.pm);
router.get('/pm/:id', pmImpl.pm_id);
router.get('/post/:id', threadImpl.post_id);
router.get('/query', searchImpl.query);
router.get('/questionnaire', questionnaireImpl.questionnaire);
router.get('/register', loginImpl.register);
router.get('/rules', ctx => redirectWithoutDelay(ctx, '/thread/1073751822'));
router.get('/search/:id?/:page?', searchImpl.search_id_page);
router.get('/tag/:id', tagImpl.tag_id);
router.get('/tags', tagImpl.tags);
router.get('/tags/settings', tagImpl.tagsSettings);
router.get('/thread/:id/:page?', threadImpl.thread_id_page);
router.get('/thread/:id/heading/:heading', threadImpl.thread_id_heading_heading);

router.post('/admin', moderationImpl.postAdmin);
router.post('/admin/contest', contestBackendImpl.postAdminContest);
router.post('/admin/password', rootImpl.postAdminPassword);
router.post('/api/author/:id/achievements', achievementsImpl.postApiAuthor_id_achievements);
router.post('/api/avatar', avatarImpl.postApiAvatar);
router.post('/api/configure', optionsImpl.postApiConfigure);
router.post('/api/iast/:id', iastImpl.postApiIast_id);
router.post('/api/invalidate/thread/:id/:page?', threadImpl.postApiInvalidate_thread_id);
router.post('/api/notifications', notificationImpl.postApiNotifications);
router.post('/api/read', async ctx => { 
    const cookieHeader = ctx.cookies.response.getHeader('set-cookie');
    if (!cookieHeader) { throw error(500, 'No mazomirror_rp found.'); }
    for (const single of (Array.isArray(cookieHeader) ? cookieHeader : [cookieHeader])) {
        const matched = (single as string).match(/mazomirror_rp=([a-z0-9]+);/);
        if (matched) {
            ctx.body = matched[1];
            return;
        }
    }
    throw error(500, 'No mazomirror_rp found.');
});
router.post('/api/star/:id', favoriteImpl.postApiStar_id);
router.post('/api/tags/settings', tagImpl.postApiTagsSettings);
router.post('/api/thread/:id/tag/:tagId', tagImpl.postApiThread_id_tag);
router.post('/api/thread/:id/vote', contestVoteImpl.postApiThread_id_vote);
router.post('/author/:id', authorImpl.postAuthor_id);
router.post('/collection/:id', favoriteImpl.postCollection_id);
router.post('/confirmation', authorImpl.postConfirmation);
router.post('/contest/panel', contestBackendImpl.postContestPanel);
router.post('/edit/:id', postImpl.postEdit_id);
router.post('/login', loginImpl.postLogin);
router.post('/merge', loginImpl.postMerge);
router.post('/pm/:id', pmImpl.postPm_id);
router.post('/post/:parent', postImpl.postPost_parent);
router.post('/questionnaire', questionnaireImpl.postQuestionnaire);
router.post('/register', loginImpl.postRegister);
router.post('/theme', optionsImpl.postTheme);
router.post('/tag/:id', tagImpl.postTag_id);
router.post('/thread/:id/tag', tagImpl.postThread_id_tag);
router.post('/user/security', loginImpl.postUserSecurity);

router.put('/api/attachment', attachmentImpl.putApiAttachment);

app.use(router.routes());

if (DEV_MODE) {
    app.use(require('koa-static')(__dirname));
}

Promise.all([
    initStatic(),
    forumsCache,
]).then(() => {
    const port = 3000;
    const server = app.listen(port, DEV_MODE ? '0.0.0.0' : '127.0.0.1');
    console.log(`Server started on port ${port}.`);

    // Background tasks to be in the same process for the simpler deployments.
    const bgTaskAborter = bgTasks();

    let closing = false;
    process.on('SIGINT', () => {
        if (closing) { return; }
        closing = true;
        bgTaskAborter.abort();
        server.close(() => console.log('Gracefully stopped'));
    });

    if (process.send) {
        process.send('ready'); // PM2 graceful start
    }
});