import { SPAM_THRESHOLD, THRESHOLD_FOR_LOCAL } from '../../common/consts';
import { query as sql } from '../db';
import { HtmlSegmentImpl, PageImpl, error, finalizePage, finalizeSegment } from '../utils/render_utils';
import { formatVisitorId } from '../shared/login';
import { tagPref } from '../shared/tags';
import { THREAD_RENDER_COLS, renderThreads } from '../shared/thread_table';
import { getTrivia } from '../shared/trivia';

import * as templates from '../templates/index';
import * as triviaTemplates from '../templates/trivia';

export const home: PageImpl = async ctx => {
    const forumResults = (await sql('SELECT forum_targets.forum_id, forum_name, parent_forum_id, thread_count.num_of_threads, display_order, most_recent_update_include_children FROM forum_targets LEFT JOIN (SELECT forum_id, COUNT(thread_id)::INT AS num_of_threads FROM thread_targets GROUP BY forum_id) AS thread_count USING (forum_id) ORDER BY forum_targets.display_order ASC')).rows;
    const filtered = forumResults.filter(row => row.parent_forum_id === 0);
    const forumGroups: templates.ForumRendering[][] = [];
    let lastGroup: templates.ForumRendering[];
    filtered.map((row, index) => {
        const newGroup = (index === 0) || (row.display_order - filtered[index - 1].display_order > 10);
        if (newGroup) { lastGroup = []; forumGroups.push(lastGroup); }
        lastGroup.push({
            count: row.num_of_threads || 0,
            name: row.forum_name,
            link: '/forum/' + row.forum_id,
            childForums: forumResults.filter(srow => srow.parent_forum_id === row.forum_id).map(srow => ({
                name: srow.forum_name,
                link: '/forum/' + srow.forum_id
            })),
            lastPost: row.most_recent_update_include_children
        });
    });

    const threadIdResults = await sql(`SELECT ${THREAD_RENDER_COLS.join(', ')} FROM thread_targets WHERE last_page_number > 0 AND successive_fail >= 0 AND spam_likeness < ${SPAM_THRESHOLD} ORDER BY sortpost_date DESC LIMIT 10`);

    return finalizePage(ctx, {
        title: ctx.state.render.staticConv('首页'),
        canonicalPath: '/',
        nav: true
    }, templates.index(ctx, {
        localOnlyUser: ctx.state.mazoLoginUser ? (ctx.state.mazoLoginUser.id > THRESHOLD_FOR_LOCAL) : false,
        forumGroups,
        currentTz: ctx.state.userConfig.tz,
        hostname: ctx.hostname,
        threads: await renderThreads(ctx, threadIdResults.rows, await tagPref(ctx), ctx.state.mazoLoginUser?.id),
        triviaStats: getTrivia()
    }));
};

export const apiTrivia: HtmlSegmentImpl = async ctx => {
    const trivia = getTrivia();
    if (!trivia) { throw error(400, 'Trivia 未准备好。'); }
    return finalizeSegment(ctx, triviaTemplates.trivia(ctx, trivia.postRank));
};