import { SPAM_THRESHOLD } from '../../common/consts';
import { query as sql } from '../db';
import { tagPref } from '../shared/tags';
import { parseInt32 } from '../utils/logic_utils';
import { forumsCache } from '../shared/forum';
import { PageImpl, error, finalizePage } from '../utils/render_utils';
import { THREAD_RENDER_COLS, renderThreads } from '../shared/thread_table';

import * as templates from '../templates/forum';

export const forum_id_page: PageImpl = async ctx => {
    let includeSpam: number = NaN;
    if ('all' in ctx.query) {
        includeSpam = parseInt(String(ctx.query.all));
    }
    if (isNaN(includeSpam)) {
        includeSpam = ctx.state.userConfig.lf;
    }
    const allowedSortings = ['sort', 'last', 'first'] as const;
    const sorting: 'sort' | 'last' | 'first' = allowedSortings.includes(ctx.query.sort as any) ? ctx.query.sort as any : allowedSortings[0];

    const forumId = parseInt32(ctx.params.id);
    let page = parseInt32(ctx.params.page);
    if (isNaN(forumId)) { throw error(400, '不要乱输入网址嘛┭┮﹏┭┮'); }
    if (isNaN(page) || page <= 0) { page = 1; }
    const forumName = forumId ? (await forumsCache).get(forumId)?.forum_name : '所有主题';
    if (!forumName) { throw error(404, '才没有这样的板块哟！'); }

    const spamLikenessPredicate = ((includeSpam > 0) ? '' : ` AND spam_likeness ${(includeSpam < 0) ? '<' : '<='} ${SPAM_THRESHOLD}`);
    const threadIdRequestParams = [ctx.state.userConfig.fp, (page - 1) * ctx.state.userConfig.fp];
    if (forumId) { threadIdRequestParams.push(forumId); }
    const threadIdRequest = sql(`SELECT ${THREAD_RENDER_COLS.join(', ')} FROM thread_targets WHERE` + (forumId ? ' forum_id = $3 AND' : '') + ' last_page_number > 0 AND successive_fail >= 0' + spamLikenessPredicate + ` ORDER BY ${sorting}post_date DESC LIMIT $1 OFFSET $2`, threadIdRequestParams);
    const countTotal = sql('SELECT COUNT(thread_id)::INT AS thread_count FROM thread_targets WHERE' + (forumId ? ' forum_id = $1 AND' : '') + ' last_page_number > 0 AND successive_fail >= 0' + spamLikenessPredicate, forumId ? [forumId] : []);
    const childForums = forumId ? sql('SELECT forum_targets.forum_id, forum_name, thread_count.num_of_threads, most_recent_update_include_children FROM forum_targets LEFT JOIN (SELECT forum_id, COUNT(thread_id)::INT AS num_of_threads FROM thread_targets GROUP BY forum_id) AS thread_count USING (forum_id) WHERE parent_forum_id = $1 ORDER BY display_order ASC', [forumId]) : undefined;

    const threads = (await threadIdRequest).rows;
    if (!threads.length) {
        throw error(400, '页码不对哦～');
    }

    const total = (await countTotal).rows[0].thread_count;
    const maxPage = Math.ceil(total / ctx.state.userConfig.fp);

    return finalizePage(ctx, {
        title: ctx.state.render.staticConv(forumName),
        nav: true,
        canonicalPath: '/forum/' + forumId + ((page > 1) ? '/' + page : '')
    }, templates.forum(ctx, {
        title: forumName,
        forumId,
        childForums: childForums ? (await childForums).rows.map(row => ({
            count: row.num_of_threads || 0,
            name: row.forum_name,
            link: '/forum/' + row.forum_id,
            lastPost: row.most_recent_update_include_children
        })) : [],
        pagination: {
            currentPage: page,
            linkExtra: ctx.querystring ? '?' + ctx.querystring : '',
            baseLink: '/forum/' + forumId,
            maxPage
        },
        postLink: forumId ? '/post/f' + forumId : undefined,
        includeSpam,
        sorting,
        threads: await renderThreads(ctx, threads, await tagPref(ctx), ctx.state.mazoLoginUser?.id),
        showReadState: !!ctx.state.mazoLoginUser
    }));
};
