import { $, $id, $set, $xhr, loadingSpinner } from "../common-import";
import { TagListModel, initializeSearcher, loadTagListModel } from "./tags_common";

const findFieldGroup = (source: HTMLElement) => {
    let parent: HTMLElement['parentNode'] = source;
    while (parent instanceof HTMLElement) {
        if (parent.tagName === 'FIELDSET') {
            return parent;
        }
        parent = parent.parentNode;
    }
    return null;
};

export const initTagSettingsPage = (warningTagListDivId: string, alertDivId: string) => {
    const container = $id(warningTagListDivId)!;
    const alertDiv = $id(alertDivId)!;

    const getExistingOnes = (): HTMLButtonElement[] => Array.from(container.getElementsByClassName('mm-tag-remove') as HTMLCollectionOf<HTMLButtonElement>);

    const saveSettings = (f: string, additionalCallbackSuccess: () => void, additionalCallbackFail: () => void) => $xhr('/api/tags/settings', {
        f,
        e: e => {
            alertDiv.className = 'alert alert-danger';
            $set(alertDiv, ['设置保存失败，建议刷新试试：', e]);
            additionalCallbackFail();
        },
        t: text => {
            alertDiv.className = 'alert alert-success';
            $set(alertDiv, text);
            additionalCallbackSuccess();
        }
    });

    const onRemoveTag = (e: MouseEvent) => {
        const button = (e.currentTarget as HTMLButtonElement);
        const btnGroup = button.parentNode as HTMLElement;

        btnGroup.removeChild(btnGroup.lastElementChild!);
        $set(btnGroup.firstElementChild as HTMLElement, loadingSpinner(true, '移除中'));

        const tagId = button.name;
        saveSettings('warn=-' + tagId, () => container.removeChild(btnGroup), () => {
            $set(btnGroup.firstElementChild as HTMLElement, '移除失败');
            btnGroup.firstElementChild!.className = 'btn btn-danger';
        });
    };

    const addTag = (tagId: string, tagName: string, onDomChange: () => void) => {
        // LINT.IfChange(tag_chip)
        const btnGroup = $('span', 'btn-group btn-group-sm m-1', [
            $('button', { className: 'btn btn-secondary', type: 'button' }, loadingSpinner(true, tagName)),
            $('button', { className: 'btn btn-secondary mm-tag-remove', type: 'button', name: tagId, disabled: true }, '×', onRemoveTag)
        ]);
        container.appendChild(btnGroup);
        onDomChange();

        saveSettings('warn=' + tagId, () => {
            $set(btnGroup.firstElementChild as HTMLButtonElement, tagName);
            (btnGroup.lastElementChild as HTMLButtonElement).disabled = false;
            onDomChange();
        }, () => { container.removeChild(btnGroup); onDomChange(); });
        // LINT.ThenChange(../templates/tags.tsx:tag_chip)
    };

    for (const single of getExistingOnes()) {
        single.addEventListener('click', onRemoveTag);
    }

    const searchInput = container.parentNode!.parentNode!.querySelector('input')!;

    const startTagSearching = (model: TagListModel) => {
        const suggestions: string[] = [];
        const updateSuggestions = () => {
            const selected: Record<string, true> = {};
            for (const button of getExistingOnes()) {
                const idStr = button.name;
                if (idStr) {
                    selected[idStr] = true;
                }
            }
            suggestions.splice(0, suggestions.length, ...model.recommend(selected, '', '', {
                _prop_defaultWarningBonus: 0.5,
                _prop_nonArbitraryPenalty: 0.75
            }));
        };
        updateSuggestions();
        initializeSearcher(searchInput, model, suggestions, tagId => {
            if (getExistingOnes().some(el => el.name === tagId)) { return; }
            addTag(tagId, model.tagMapById[tagId]._prop_names[0], updateSuggestions);
        }, true);
    };

    searchInput.addEventListener('focus', () => loadTagListModel(true, res => {
        if (typeof res === 'string') { window.alert('标签列表加载失败了：' + res); searchInput.blur(); } else { startTagSearching(res); }
    }));

    const onCheckboxOrRadioCheck = (e: Event) => {
        const input = e.currentTarget as HTMLInputElement;
        const checkbox = input.type.toLowerCase() === 'checkbox';
        if (checkbox) {
            input.disabled = true;
            const unlock = () => { input.disabled = false; };
            saveSettings(input.name + '=' + (input.checked ? '1' : '-1'), unlock, unlock);
        } else {
            const fg = findFieldGroup(input)!;
            const inputsInGroup = Array.from(fg.getElementsByTagName('input'));
            for (const input of inputsInGroup) {
                input.disabled = true;
            }
            const unlock = () => inputsInGroup.forEach(input => { input.disabled = false; });
            saveSettings(input.name + '=' + input.value, unlock, () => {
                unlock();
                input.checked = false;
            });
        }
    };
    for (const check of Array.from(document.querySelectorAll<HTMLInputElement>('input[id^=tf-]'))) {
        check.addEventListener('click', onCheckboxOrRadioCheck)
    }
};