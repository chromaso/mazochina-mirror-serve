import { $, $set, $xhr } from './dollar';
import { showModal } from './modal';
import { closeDropdownMenuIfSourceInside, positionPop } from './pop';
import { reportIfNeeded } from './read_status_track';
import { debugLog, getThreadIdFromUri, loadingSpinner } from './utils';

const tipper = (text: string) => $('span', 'mazomirror-tip', [$('span', 'material-icons align-middle', ['info']).also(icon => { icon.style.fontSize = '100%'; })]).also(tip => tip.setAttribute('data-title', text));

export const sideEffect = () => {
    window['肉便器'] = [$xhr, $, showModal, debugLog, $set, getThreadIdFromUri, reportIfNeeded, closeDropdownMenuIfSourceInside, tipper, loadingSpinner, positionPop];
};