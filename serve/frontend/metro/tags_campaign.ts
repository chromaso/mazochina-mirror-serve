import { $, showModal } from "../common-import";

export const contestCampaignTagConfirmationDialog = (callback: (confirm: boolean) => void) => {
    let selected = false;
    const btnClick = (e: MouseEvent) => {
        const button = e.currentTarget as HTMLButtonElement;
        if (button.classList.contains('btn-primary')) {
            callback(true);
        } else if (button.classList.contains('btn-secondary')) {
            callback(false);
        } else {
            return;
        }
        selected = true;
        closeModal();
    };
    const closeModal = showModal('确认报名参加征文活动吗？', $('div', void 0, [
        $('p', void 0, '请确认你符合以下参赛条件：'),
        $('ul', void 0, [
            $('li', void 0, '作品为完全原创，且全文将于此主题帖内发布；'),
            $('li', void 0, [
                '作品符合叁孙杯「恶女」题材征文活动的主题及其它要求（详请参见',
                $('a', { 'href': '/thread/1073748496', target: '_blank' }, '活动详情帖'),
                '）;'
            ]),
            $('li', void 0, '会积极回应活动举办者的联络。'),
        ]),
        $('button', { className: 'btn btn-primary d-block w-100', type: 'button' }, '确认此小说符合参赛要求，报名参赛', btnClick),
        $('button', { className: 'btn btn-secondary d-block w-100 mt-2', type: 'button' }, '暂不将此小说报名参赛', btnClick)
    ]), () => {
        if (!selected) {
            window.alert('请作出选择！');
        }
        return selected;
    });
};

export const initCampaignSignupInitial = () => {
    const checkbox = document.getElementById('mm-tag-campaign-tag') as HTMLInputElement;
    checkbox.addEventListener('change', function() {
        if (!checkbox.checked) {
            return;
        }
        contestCampaignTagConfirmationDialog(confirm => { if (!confirm) { checkbox.checked = false; } });
    });
};