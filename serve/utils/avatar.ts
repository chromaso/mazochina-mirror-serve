import { createHash } from 'crypto';
import { safeHtml } from 'typed-xhtml';

import { escapeHtml } from '../../common/dependencies';
import { HtmlTmpl } from './tsx_utils';
import { unawaited } from '../../common/logic_utils';
import { query } from '../db';
import { attachmentIdToName } from '../shared/attachment';

const dogHead = (id: number): string => '/static/a240/' + (((id % 14) * 521 + 233) & 1023) + '.png';

const textColor = (id: number): string => `hsla(${(id % 180) << 1}, 60%, 85%, 0.85)`;

const bgColor = (id: number): string => `hsl(${((id + 15) % 180) << 1}, 100%, 20%)`;

const defaultAvatar = (userId: number, userName: string, hide: boolean): SafeString => {
    return safeHtml(`<div class="mm-def-ava" style="background: ${bgColor(userId)}; color: ${textColor(userId)}${hide ? '; visibility: hidden' : ''}"><img src="${dogHead(userId)}" /><div>${escapeHtml(userName.charAt(0).toUpperCase())}<small style="font-size: 40%">${escapeHtml(userName.charAt(1))}</small></div></div>`);
};

// Caller must ensure timestamp is nonzero. The timestamp is used to invalidate cache.
export const thirdPartyAvatarUrl = (email: string, timestamp: number, large?: boolean): string => 'https://cn.cravatar.com/avatar/' + createHash('md5').update(email.toLowerCase()).digest('hex') + '?s=' + (large ? '240' : '120') + ((timestamp > 0) ? '.0' + timestamp : '') + '&d=404';

export const firstPartyAvatarUrl = (avatarId: number, large?: boolean) => `/static/avatars/${attachmentIdToName(avatarId)}-${large ? '240' : '120'}.webp`;

const avatarUrlForArchive = (email: string, timestamp: number): string => 'https://cn.cravatar.com/avatar/' + createHash('md5').update(email.toLowerCase()).digest('hex') + '?s=480' + ((timestamp > 0) ? '.0' + timestamp : '') + '&d=blank';

export const dualAvatar = (userId: number, userName: string, userEmail: string | null, avatarSmallInt: number, large?: boolean) => {
    const enableAvatar = !!(userEmail && avatarSmallInt);
    const firstParty = avatarSmallInt < -1;
    return safeHtml(
        '<div class="mm-dual-ava border">' +
            defaultAvatar(userId, userName, enableAvatar) +
            // src not escaped as they're constructed above and we know they're safe.
            (enableAvatar ? `<img class="mm-img-ava" src=${firstParty ? firstPartyAvatarUrl(-avatarSmallInt, large) : thirdPartyAvatarUrl(userEmail, avatarSmallInt, large)} onerror="this.previousElementSibling.style.visibility = 'visible';this.style.display='none';"${firstParty ? '' : ' referrerpolicy="no-referrer"'} />` : '') +
        '</div>'
    );
};

const AVATAR_CK_POPUP_FREQ = 0.03;

export const maybeShowAvatarCkPopup: HtmlTmpl<void> = ctx => {
    const userInfo = ctx.state.mazoLoginUser;
    if (!userInfo) { return ''; }
    if (userInfo.avatarSmallInt < -1) { return ''; } // First-party avatar; no checking needed.
    if (userInfo.avatarSkipck >= ctx.refDate) { return ''; }
    if (Math.random() >= AVATAR_CK_POPUP_FREQ) { return ''; }
    unawaited(query('UPDATE users SET avatar_skipck = $1 WHERE user_id = $2', [ctx.refDate + 3600 * 12, userInfo.id]), 'avatar_skipck_p12');
    return safeHtml(`<script type="text/javascript">avatarChecker(${JSON.stringify(avatarUrlForArchive(userInfo.email, userInfo.avatarSmallInt))}, ${!!userInfo.avatarSmallInt});</script>`);
};
