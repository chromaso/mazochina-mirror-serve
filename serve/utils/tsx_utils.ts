import { createHash } from 'crypto';
import { promises as fs } from 'fs';

import { safeHtml } from 'typed-xhtml';

import { MazoKoaContext } from './logic_utils';
import { fromRoot } from '../path';

const safeCSS = Symbol('safeCSS');

export type SafeStringPossiblyWithStyle = SafeString & {
    [safeCSS]?: SafeString;
};

export const cssOf = (fragment: SafeStringPossiblyWithStyle): SafeString | undefined => fragment[safeCSS];

export const styled = (css: string, html: SafeStringPossiblyWithStyle): SafeStringPossiblyWithStyle => {
    html[safeCSS] = safeHtml(css);
    return html;
};

export type HtmlTmpl<Params> = (ctx: MazoKoaContext, params: Params) => SafeStringPossiblyWithStyle;

export const staticHashes: string[] = [];

export const initStatic = async () => {
    const hashes = await Promise.all([
        'bundle-cn.js', // 0
        'metro-cn.js', // 1
        'styles.css', // 2
        'admin-cn.js' // 3
    ].map(fn => fs.readFile(fromRoot('static', fn)).then(buf => createHash('md5').update(buf).digest('hex').substr(4, 8))));
    staticHashes.splice(0, staticHashes.length, ...hashes);
};