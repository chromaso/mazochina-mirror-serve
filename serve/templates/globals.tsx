import * as elements from 'typed-xhtml';
import { HtmlTmpl, staticHashes } from '../utils/tsx_utils';
import { IS_DEV_MODE } from '../conf';
import { dualAvatar, maybeShowAvatarCkPopup } from '../utils/avatar';
import { formatVisitorId } from '../shared/login';
import { hiddenGreet } from '../utils/rand_greet';

// Migrated from global_nav.pug.
export const nav: HtmlTmpl<void> = ctx => {
    const { render } = ctx.state;
    const loginUser = ctx.state.mazoLoginUser;
    const P = ctx.path.startsWith('/thread/1073750003');
    return (
        <>
            <nav class="navbar fixed-top bg-body-tertiary">
                <div class="container position-relative flex-nowrap" style="min-width: 0">
                    <a href="/" class="navbar-brand">
                        {IS_DEV_MODE ? (
                            <span class="text-danger mazomirror-tip" data-title="此环境仅供临时测试用，任何数据都为测试数据且随时可能被删除">镜像测试环境 ⚠️</span>
                        ) : (
                            <>
                                {P ? <span title="PAPILIO">P</span> : 'M'}系镜像
                            </>
                        )}
                    </a>
                    <span class="text-nowrap">
                        <a role="button" href="#search" title="搜索" class="btn btn-link btn-sm">
                            <span class="material-icons">search</span>
                        </a>
                        {loginUser ? (
                            <>
                                <a class="btn btn-link btn-sm noti-icon-base" role="button" href={`/author/${loginUser.id}/favs`} title='收藏'>
                                    <span id="fav-none" class="material-icons">
                                        favorite_border
                                    </span>
                                    <span id="fav-active" class="material-icons d-none">
                                        favorite
                                    </span>
                                </a>
                                <a class="btn btn-link btn-sm noti-icon-base" role="button" href="#notifications" title="通知">
                                    <span id="noti-none" class="material-icons">
                                        notifications_none
                                    </span>
                                    <span id="noti-active" class="material-icons d-none">
                                        notifications
                                    </span>
                                </a>
                                <a class="btn btn-link btn-sm noti-icon-base" role="button" href="#pmessages" title="私信">
                                    <span id="pm-none" class="material-icons">
                                        mail_outline
                                    </span>
                                    <span id="pm-active" class="material-icons d-none">
                                        mark_email_unread
                                    </span>
                                </a>
                                <a class="d-inline-block align-middle ms-2" style="height: 20px" role="button" href="#menu">
                                    <div class="mm-36-ava" style="margin-top: -8px">
                                        {dualAvatar(loginUser.id, loginUser.name, loginUser.email, loginUser.avatarSmallInt)}
                                    </div>
                                </a>
                            </>
                        ) : (
                            <>
                                <a class="btn btn-secondary btn-sm ms-2" role="button" href="#login">
                                    登入
                                </a>
                                <a class="btn btn-secondary btn-sm ms-2" role="button" href="#menu">
                                    <span class="material-icons">more_horiz</span>
                                </a>
                            </>
                        )}
                    </span>
                    <div id="search" class="card shadow">
                        <div class="card-header">搜索</div>
                        <div class="card-body">
                            <form method="get" action="/query" class="d-flex flex-nowrap">
                                <div class="input-group">
                                    <input class="form-control" name="query" placeholder="根据标题搜索主题" />
                                    <input type="hidden" name="type" value="thread" />
                                    <button class="btn btn-primary">搜索全站</button>
                                </div>
                                <a href="/search" class="btn btn-secondary flex-shrink-0 ms-2">更多</a>
                            </form>
                        </div>
                    </div>
                    <div id="menu" class="card shadow">
                        <div class="card-header">
                            {loginUser ? '已登入为 ' + loginUser.name : (formatVisitorId(ctx.state.mazoGuest?.id) + '，你未登入')}
                        </div>
                        <div class="list-group list-group-flush">
                            {loginUser ? <a href={'/author/' + loginUser.id + '/'} class="list-group-item list-group-item-action">
                                <span class="material-icons me-1">person</span>个人页面
                            </a> : <a href="#login" class="list-group-item list-group-item-action">
                                <span class="material-icons me-1">login</span>登入或注册
                            </a>}
                            <a href="javascript:void(0)" class="list-group-item list-group-item-action" id="light-dark-mode-switch">
                                <span class="material-icons me-1">brightness_medium</span>
                                <span>未知</span>
                            </a>
                            <a href="javascript:openMazoSettings()" class="list-group-item list-group-item-action">
                                <span class="material-icons me-1">settings</span>界面选项
                            </a>
                            {loginUser ? <a href="/logout" class="list-group-item list-group-item-action">
                                <span class="material-icons me-1">logout</span>登出
                            </a> : null}
                        </div>
                    </div>
                    <div id="login" class="card shadow">
                        <div class="card-body">
                            <form method="post" action="/login">
                                <div class="alert alert-primary">请使用你在镜像注册的密码喔～</div>
                                <div class="form-group">
                                    <input
                                        id="login-form-username"
                                        name="username"
                                        type="text"
                                        placeholder="用户名"
                                        class="form-control"
                                    />
                                </div>
                                <div class="form-group">
                                    <input
                                        id="login-form-password"
                                        name="password"
                                        type="password"
                                        placeholder="密码"
                                        class="form-control"
                                    />
                                </div>
                                <div class="row justify-content-between">
                                    <div class="col-auto">
                                        <a href="/register" class="btn btn-secondary">注册</a>
                                    </div>
                                    <div class="col-auto">
                                        <button type="submit" class="btn btn-primary">登入</button>
                                    </div>
                                </div>
                            </form>
                            <p class="mt-2 mb-0 small">
                                若忘记密码或需要其它帮助，请<a href="/modmail">联系镜像管理员</a>。
                            </p>
                        </div>
                    </div>
                    {loginUser ? (<>
                        <div id="favorites" class="nfc-menu-host card shadow">
                            <div class="card-header d-flex justify-content-between">
                                <div class="nav nav-tabs card-header-tabs">
                                    <li class="nav-item">
                                        <a href="#favorites" class="nav-link active">订阅的回复</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#collections" class="nav-link">收藏夹列表</a>
                                    </li>
                                </div>
                                <a href="javascript:void(0)" id="fav-dism-b" title="全部标为已读" class="d-none"><span class="material-icons align-middle" style="font-size: 100%">done_all</span></a>
                            </div>
                            <div id="fav-card-inner" class="card-body">
                                <div class="text-center">
                                    <div class="spinner-border" />
                                    <div class="mt-1">订阅加载中</div>
                                </div>
                            </div>
                        </div>
                        <div id="collections" class="nfc-menu-host card shadow">
                            <div class="card-header">
                                <div class="nav nav-tabs card-header-tabs">
                                    <li class="nav-item">
                                        <a href="#favorites" class="nav-link">订阅的回复</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#collections" class="nav-link active">收藏夹列表</a>
                                    </li>
                                </div>
                            </div>
                            <div id="coll-card-inner" class="card-body">
                                <div class="text-center">
                                    <div class="spinner-border" />
                                    <div class="mt-1">收藏夹列表加载中</div>
                                </div>
                            </div>
                        </div>
                        <div id="notifications" class="nfc-menu-host card shadow">
                            <div class="card-header d-flex justify-content-between">
                                <span>提醒</span>
                                <a href="javascript:void(0)" id="noti-dism-b" title="全部标为已读" class="d-none"><span class="material-icons align-middle" style="font-size: 100%">done_all</span></a>
                            </div>
                            <div id="noti-card-inner" class="card-body">
                                <div class="text-center">
                                    <div class="spinner-border" />
                                    <div class="mt-1">提醒加载中</div>
                                </div>
                            </div>
                        </div>
                        <div id="pmessages" class="nfc-menu-host card shadow">
                            <div class="card-header d-flex justify-content-between">
                                <span>私信</span>
                                <a href="/pm">»</a>
                            </div>
                            <div id="pm-card-inner" class="card-body">
                                <div class="text-center">
                                    <div class="spinner-border" />
                                    <div class="mt-1">私信加载中</div>
                                </div>
                            </div>
                        </div>
                    </>) : null}
                </div>
                {ctx.state.mazoLoginUser ? <>
                    <script type='text/javascript' src={'/static/metro-' + render.locale + '.js?v=' + staticHashes[1]} />
                    <script type='text/javascript'>{elements.safeHtml(`initHovercard(${ctx.state.userConfig.hc});`)}</script>
                    {maybeShowAvatarCkPopup(ctx)}
                </> : null}
            </nav>
            <nav class="navbar justify-content-center">
                <div class="navbar-brand text-center">{hiddenGreet(ctx)}</div>
            </nav>
        </>
    );
}