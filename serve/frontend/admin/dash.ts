/// <reference types="google.visualization" />

import { $, $id, $set, $xhr, tipper } from '../common-import';
import { ADMIN_DASH_TIMESERIES_CHUNK_COUNT, ADMIN_DASH_TIMESERIES_CHUNK_LENGTH } from '../../../common/consts';

const tabbed = <T>(dataAttr: string, render: (data: T) => HTMLElement, requestSender: (tagName: string, errorHandler: (error: string) => void, load: (response: T) => void) => void) => {
    const btns = Array.from(document.querySelectorAll(`[data-${dataAttr}]`));
    const cached: { [key: string]: T } = {};
    const contentEl = $id(dataAttr + '-target')!;
    let current: string | undefined;
    const load = (key: string, newContent?: T) => {
        if (newContent) { cached[key] = newContent; }
        if (key !== current) { return; }
        $set(contentEl, render(cached[key]));
    }
    const loadLog = (e: Event) => {
        e.preventDefault();
        const btn = e.currentTarget as HTMLElement;
        const tagName = btn.getAttribute('data-' + dataAttr)!;
        current = tagName;
        btns.forEach(b => b.classList[(btn === b) ? 'add' : 'remove']('active'));
        if (cached[tagName]) {
            load(tagName);
            return;
        }
        $set(contentEl, '正在加载…');
        requestSender(tagName, e => {
            if (current === tagName) {
                $set(contentEl, $('div', undefined, ['加载错误：', e]).also(it => { it.style.color = 'red'; }))
            }
        }, t => load(tagName, t));
    };
    btns.forEach(btn => btn.addEventListener('click', loadLog));
};

const adminDashForLog = () =>
    tabbed<string>(
        'lfile',
        text => $('pre', { className: 'small overflow-auto', 'style': 'max-height: 400px' }, $('code', undefined, [text])),
        (tagName, e, load) => 
            $xhr('/api/admin/dash/log/' + tagName, {
                e,
                t: load
            })
    );

const adminDashForCoreGraphs = (forums: { id: number, name: string }[]) => {
    tabbed<google.visualization.DataTable>(
        'sstat',
        dataTable => {
            const host = $('div');
            setTimeout(() => {
                const chart = new google.visualization.AreaChart(host);
                chart.draw(dataTable, {
                    isStacked: true,
                    height: 600,
                    explorer: {},
                    theme: 'maximized'
                });
            });
            return host;
        },
        (tagName, e, load) => 
            $xhr('/api/admin/dash/exttimeseries/' + tagName, {
                e,
                j: (json: { header: (string | number)[], data: number[][], first: number, interval: number }) => {
                    const output: any[][] = [
                        ['时段', '其它', ...((tagName === 'user') ? 
                            json.header :
                            json.header.map(forumId => forums.find(f => f.id === forumId)?.name ?? `ID=${forumId}`)
                        )]
                    ];
                    for (let i = 2; i < json.data.length; i++) {
                        const sections = json.data.slice(i - 2, i + 1);
                        output.push([
                            new Date(((json.first + (i - 0.5)) * json.interval) * 1000),
                            ...sections.reduce((p, c) => c.map((v, i) => v + p[i]))
                        ]);
                    }
                    load(google.visualization.arrayToDataTable(output));
                }
            })
    );
};

const adminDashForAdmins = (onFinish: () => void) => {
    const target = $id('card-admins')!;
    $set(target, '正在加载…');
    $xhr('/api/admin/dash/users', {
        j: (result: { id: number, name: string, op: number, op15: number }[]) => {
            $set(target, $('table', 'table table-sm', [
                $('thead', undefined, $('tr', undefined, [
                    $('th', { scope: 'col' }, '用户'),
                    $('th', { scope: 'col' }, ['总操作', tipper('有史以来的管理操作')]),
                    $('th', { scope: 'col' }, ['最近', tipper('最近 15 天内的管理操作')]),
                ])),
                $('tbody', undefined, result.map(row => $('tr', undefined, [
                    $('td', undefined, $('a', { href: '/author/' + row.id }, [row.name])),
                    $('td', row.op ? '' : 'text-muted', row.op ? $('a', { href: '/moderation?a=' + row.id }, String(row.op)) : String(row.op)),
                    $('td', row.op15 ? '' : 'text-muted', String(row.op15)),
                ])))
            ]));
            onFinish();
        },
        e: e => {
            $set(target, $('div', undefined, ['加载错误：', e]).also(it => { it.style.color = 'red'; }));
            onFinish();
        }
    });
};

const timeSeriesGroupings: Readonly<{ [title: string]: number }> = { '每 8 小时': 1, '每日': 3, '每 3 日': 9 };

const adminDashForTimeSeries = (stat: string, headers: [string, string][]): ((onFinish: () => void) => void) => onFinish => {
    const target = $id(`card-${stat}s`)!;
    $set(target, '正在加载…');
    let data: Readonly<(number[] | null)[]> | undefined;
    let grouping = 3;
    let tableProper: Node | undefined;
    const date0 = Date.now();
    const renderTable = () => {
        const outputArray: { title: string, desc: string, values: number[] }[] = [];
        for (let i = 0; i < ADMIN_DASH_TIMESERIES_CHUNK_COUNT / grouping; i++) {
            const values = headers.map(_ => 0);
            outputArray.push({
                title: Math.round((i * grouping * ADMIN_DASH_TIMESERIES_CHUNK_LENGTH) / 8640) / 10 + ' 天前',
                desc: `${new Date(date0 - (i + 1) * grouping * ADMIN_DASH_TIMESERIES_CHUNK_LENGTH * 1000)} 至 ${new Date(date0 - i * grouping * ADMIN_DASH_TIMESERIES_CHUNK_LENGTH * 1000)}`,
                values
            });
        }
        data!.forEach((array, index) => {
            if (!array) { return; }
            const targetIndex = Math.floor(index / grouping);
            outputArray[targetIndex].values = outputArray[targetIndex].values.map((v, i) => v + array[i]);
        });
        const oldTableProper = tableProper!;
        target.replaceChild(tableProper = $('table', 'table table-sm mt-2', [
            $('thead', undefined, $('tr', undefined, [
                $('th', { scope: 'col' }, '时段'),
                ...headers.map(header => $('th', { scope: 'col' }, [header[0], tipper(header[1])]))
            ])),
            $('tbody', undefined, outputArray.map(row => $('tr', undefined, [
                $('th', { scope: 'row' }, [row.title, tipper(row.desc)]),
                ...row.values.map(number => $('td', undefined, String(number)))
            ])))
        ]), oldTableProper);
    };
    const drawChart = () => {
        const additionalBody = $('div', 'card-body pb-0');
        target.parentNode!.insertBefore(additionalBody, target);

        const dataTable = new google.visualization.DataTable();
        dataTable.addColumn('date', '时段');
        for (const header of headers) {
            dataTable.addColumn('number', header[0]);
        }
        // dataTable.addColumn({ type: 'string', role: 'tooltip' });
        for (let i = ADMIN_DASH_TIMESERIES_CHUNK_COUNT; i >= 3; i--) {
            const sections = data!.slice(i - 3, i);
            dataTable.addRow([
                new Date(date0 - (i + 1.5) * ADMIN_DASH_TIMESERIES_CHUNK_LENGTH * 1000),
                ...headers.map((_, index) => sections.reduce((p, c) => p + (c?.[index] ?? 0), 0)),
                // `${new Date(date0 - i * grouping * ADMIN_DASH_TIMESERIES_CHUNK_LENGTH * 1000)} 至 ${new Date(date0 - (i - 3) * grouping * ADMIN_DASH_TIMESERIES_CHUNK_LENGTH * 1000)}`
            ]);
        }
        setTimeout(() => {
            const chart = new google.visualization.LineChart(additionalBody);
            chart.draw(dataTable, {
                title: 'Per day',
                legend: { position: 'bottom' },
                theme: 'maximized'
            });
        });
    };
    const initRender = () => {
        $set(target, $('select', 'form-control', Object.keys(timeSeriesGroupings).map(k => {
            const value = timeSeriesGroupings[k];
            return $('option', { 'selected': (value === grouping), value: String(value) }, [k]);
        }), {
            'change': e => {
                grouping = parseInt((e.currentTarget as HTMLSelectElement).value);
                renderTable();
            }
        }));
        target.appendChild(tableProper = document.createTextNode('…'));
        renderTable();
        try {
            google.charts.setOnLoadCallback(drawChart);
        } catch (e) {
            // Do nothing.
        }
    };
    target.style.height = '320px';
    target.style.overflow = 'auto';
    $xhr('/api/admin/dash/timeseries/' + stat, {
        j: result => {
            data = result;
            if (result.length !== ADMIN_DASH_TIMESERIES_CHUNK_COUNT) { $set(target, $('div', undefined, '数据总量不对劲…').also(it => { it.style.color = 'red'; })); return; }
            setTimeout(initRender);
            onFinish();
        },
        e: e => {
            $set(target, $('div', undefined, ['加载错误：', e]).also(it => { it.style.color = 'red'; }));
            onFinish();
        }
    });
};

export const initAdminDashCore = () => {
    adminDashForLog();

    $xhr('/api/forums', {
        j: adminDashForCoreGraphs,
        e: e => $set($id('sstat-target')!, '无法加载论坛列表：' + e)
    });
};

export const initAdminDash = () => {
    try {
        google.charts.load('current', { 'packages': ['corechart'] });
    } catch (e) {
        // Do nothing.
    }

    const tasks = [
        adminDashForAdmins,
        adminDashForTimeSeries('user', [['注册（未绑）', '在此时段内在镜像注册的用户（之后从未绑定其在源站的 ID）'], ['注册（已绑定）', '在此时段内在镜像注册的用户（之后绑定了其在源站的 ID）'], ['绑定', '在此时段内绑定了其在源站的 ID（注册时间不限）']]),
        adminDashForTimeSeries('thread', [['镜像', '在此时段内在镜像新发布的主题帖'], ['源站', '在此时段内在源站新发布的主题帖（仅限已被镜像收录的；但镜像不一定是在此时段内收录的，可能延后）']]),
        adminDashForTimeSeries('post', [['镜像', '在此时段内在镜像新发布的文章（主题/回复）'], ['源站', '在此时段内在源站新发布的文章（仅限已被镜像收录的；但镜像不一定是在此时段内收录的，可能延后）']]),
        adminDashForTimeSeries('fav', [['收藏', '在此时段内用户的收藏操作次数（仅限每个主题帖第一次被某用户收藏；之后添加进其它收藏夹，或是取消收藏重新收藏，皆不计数）']])
    ];
    const doNextTask = () => {
        if (!tasks.length) { return; }
        const task = tasks.splice(Math.floor(Math.random() * tasks.length), 1)[0];
        task(doNextTask);
    };
    doNextTask();
};
