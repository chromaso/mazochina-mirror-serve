import { SPAM_THRESHOLD } from '../../common/consts';
import { rowsToMap, unawaited } from '../../common/logic_utils';
import { query } from '../db';
import { allDescedentTags, allTagsById, F18_CONSTS } from './tags';
import { error } from '../utils/render_utils';

type RawDataResult = Readonly<{
    desc: string, // String description of progress.
} & (
    { type: 'author' | 'thread' | 'thread-me', entries: number[]} |
    { type: 'tagass', entries: { thread_id: number, tag_id: number }[]} |
    { type: 'achievement', entries: string[] } |
    { type?: never, entries: (string | number)[] }
)>;

type Checker = (userId: number, refDate: number) => Promise<RawDataResult>;

const makeTagChecker = (tagId: number, tagNameScUpper: string): Checker => async userId => {
    const tag = (await allTagsById()).get(tagId);
    if (!tag || !(tag.primary_name.toUpperCase().includes(tagNameScUpper) || tag.aliases?.some(alias => alias.toUpperCase().includes(tagNameScUpper)))) {
        throw new Error(`Tag ${tagId} not found! This should never happen.`);
    }
    const tagTree = allDescedentTags(tag);
    const data = await query<{ thread_id: number }>(`SELECT thread_id FROM thread_targets WHERE forum_id = 18 AND owner_user_id = $1 AND last_page_number > 0 AND successive_fail >= 0 AND spam_likeness < $2 AND EXISTS (SELECT tagass_id FROM tag_association WHERE tag_association.thread_id = thread_targets.thread_id AND tag_id = ANY($3::INT[]) AND tag_score > ${F18_CONSTS.CONSTS.SCORE_MULTIPLIER})`, [userId, SPAM_THRESHOLD, Array.from(tagTree)]);
    const threads = data.rows.map(row => row.thread_id);
    return {
        desc: `发布过 ${threads.length} 篇带「${tagNameScUpper}」标签（或其子标签）的小说`,
        entries: threads,
        type: 'thread'
    } as const;
};

const FOOTNOTE_TZ = 1 << 1;
const FOOTNOTE_REP = 1 << 2;
const FOOTNOTE_F18 = 1 << 3;
const FOOTNOTE_SPAM = 1 << 4;
const FOOTNOTE_FIRSTTAG = 1 << 5;
const FOOTNOTE_TAGV_EXCL = 1 << 6;
const FOOTNOTE_WC_INACCURATE = 1 << 7;
const FOOTNOTE_EXCL_META = 1 << 8;
const FOOTNOTE_EXCL_OWNER = 1 << 9;

export const achivementFootnotes = (conf: typeof ACHIEVEMENTS[number]): string[] => {
    const flags = conf.footnotes;
    const retVal = [];
    if (flags & FOOTNOTE_TZ) {
        retVal.push('日期的计算以按照 UTC 而非当地时区。');
    }
    if (flags & FOOTNOTE_REP) {
        retVal.push('若某主题内有回复可见内容，则整个主题下全部回复不计入在内。');
    }
    if (flags & FOOTNOTE_F18) {
        retVal.push('「小说」指在小说文字区内的主题帖。');
    }
    if (flags & FOOTNOTE_SPAM) {
        retVal.push('若主题被删除、隐藏或半隐藏，则不计入在内。');
    }
    if (flags & FOOTNOTE_FIRSTTAG) {
        retVal.push('作为首个对该主题添加该标签的用户才计入在内。该标签最终计分大于 1，才计入在内。');
    }
    if (flags & FOOTNOTE_TAGV_EXCL) {
        retVal.push('若仅是对未被任何人👍的标签进行👎，则不计入在内。');
    }
    if (flags & FOOTNOTE_WC_INACCURATE) {
        retVal.push('空格不计入字数。部分帖子的字数统计不准确，存在些许误差。');
    }
    if (flags & FOOTNOTE_EXCL_META) {
        retVal.push('此成就本身（及其它「二阶成就」）不计入在内。');
    }
    if (flags & FOOTNOTE_EXCL_OWNER) {
        retVal.push('（楼主）自己的回复不计入在内。');
    }
    return retVal;
};

export type AchievementConf = Readonly<{
    name: string,
    descPrefix: string,
    l3CountThreshold: number,
    descSuffix: string,
    checker: Checker,
    postId: number,
    epoch: number,
    matIcon: string,
    footnotes: number
}>;

const makeAchievement = (name: string, descPrefix: string, l3CountThreshold: number, descSuffix: string, checker: Checker, conf: { postId: number, epoch: number, matIcon: string }, footnotes: number): AchievementConf => Object.freeze({
    name, descPrefix, l3CountThreshold, descSuffix, checker, ...conf, footnotes
});

const META_POST_ID = 1073843639;

export const ACHIEVEMENTS: Readonly<AchievementConf[]> = Object.freeze([
    makeAchievement('笔耕不辍', '在', 6, '个年份发布过小说', async userId => {
        const data = await query<{ yr: number }>('SELECT DISTINCT EXTRACT(YEAR FROM TO_TIMESTAMP(firstpost_date)) AS yr FROM thread_targets WHERE forum_id = 18 AND last_page_number > 0 AND successive_fail >= 0 AND owner_user_id = $1 AND spam_likeness < $2', [userId, SPAM_THRESHOLD]);
        const years = data.rows.map(row => row.yr).sort((a, b) => a - b);
        return {
            desc: years.length ? `在 ${years.join('、')} 年发布过小说` : '从未发布过小说',
            entries: years
        };
    }, { postId: 1073843627, epoch: 1731000000, matIcon: 'history_edu' }, FOOTNOTE_TZ),
    makeAchievement('千客万来', '获得', 333, '个不同用户回复', async userId => {
        const data = await query<{ user_id: number }>('SELECT DISTINCT user_id FROM posts WHERE most_recent_update >= 0 AND thread_id IN (SELECT thread_id FROM thread_targets WHERE last_page_number > 0 AND successive_fail >= 0 AND owner_user_id = $1 AND spam_likeness < $2 AND NOT EXISTS (SELECT post_id FROM posts WHERE posts.thread_id = thread_targets.thread_id AND posts.flags & 2 <> 0)) EXCEPT SELECT $1 AS user_id', [userId, SPAM_THRESHOLD]);
        const users = data.rows.map(row => row.user_id);
        return {
            desc: `主题帖共获得 ${users.length} 个不同用户的回复`,
            entries: users,
            type: 'author'
        };
    }, { postId: 1073843626, epoch: 1731000000, matIcon: 'groups' }, FOOTNOTE_REP | FOOTNOTE_SPAM | FOOTNOTE_EXCL_OWNER),
    makeAchievement('最佳读者', '在', 168, '篇他人的小说下回复过', async userId => {
        const data = await query<{ thread_id: number }>('SELECT thread_id FROM thread_targets WHERE last_page_number > 0 AND successive_fail >= 0 AND owner_user_id <> $1 AND spam_likeness < $2 AND forum_id = 18 AND EXISTS (SELECT post_id FROM posts WHERE posts.thread_id = thread_targets.thread_id AND posts.user_id = $1 AND most_recent_update >= 0)', [userId, SPAM_THRESHOLD]);
        const threads = data.rows.map(row => row.thread_id);
        return {
            desc: `在 ${threads.length} 篇他人的小说下发过回复`,
            entries: threads,
            type: 'thread-me'
        };
    }, { postId: 1073843627, epoch: 1731000000, matIcon: 'reply_all' }, FOOTNOTE_F18 | FOOTNOTE_SPAM),
    makeAchievement('常驻嘉宾', '在', 399, '天发过帖', async userId => {
        const data = await query<{ datestr: string }>('SELECT DISTINCT TO_CHAR(TO_TIMESTAMP(posted_date), \'YYYY-MM-DD\') AS datestr FROM posts WHERE most_recent_update >= 0 AND user_id = $1 AND EXISTS (SELECT thread_id FROM thread_targets WHERE last_page_number > 0 AND successive_fail >= 0 AND spam_likeness < $2 AND thread_targets.thread_id = posts.thread_id)', [userId, SPAM_THRESHOLD]);
        const dates = data.rows.map(row => row.datestr);
        return {
            desc: `在 ${dates.length} 天发过帖`,
            entries: dates
        };
    }, { postId: 1073843626, epoch: 1731000000, matIcon: 'calendar_month' }, FOOTNOTE_TZ | FOOTNOTE_SPAM),
    makeAchievement('惨遭放置', '发布', 15, '个几乎无人回复的主题帖', async (userId, refDate) => {
        const data = await query<{ thread_id: number }>('SELECT thread_id FROM thread_targets WHERE last_page_number > 0 AND successive_fail >= 0 AND owner_user_id = $1 AND spam_likeness < $2 AND (SELECT COUNT(DISTINCT user_id) FROM posts WHERE posts.thread_id = thread_targets.thread_id) < 3 AND firstpost_date < $3', [userId, SPAM_THRESHOLD, refDate - 60 * 86400]);
        const threads = data.rows.map(row => row.thread_id);
        return {
            desc: `有 ${threads.length} 个主题帖的回复人数小于二（发布满两个月才计入）`,
            entries: threads,
            type: 'thread'
        };
    }, { postId: 1073843626, epoch: 1731000000, matIcon: 'self_improvement' }, FOOTNOTE_SPAM | FOOTNOTE_EXCL_OWNER),
    makeAchievement('青翠欲滴', '发布', 6, '篇 NTR 主题小说', makeTagChecker(82, 'NTR'), { postId: 1073843628, epoch: 1731000000, matIcon: 'group_add' }, FOOTNOTE_F18 | FOOTNOTE_SPAM),
    makeAchievement('榨死方休', '发布', 6, '篇榨精主题小说', makeTagChecker(96, '榨精'),  { postId: 1073843628, epoch: 1731000000, matIcon: 'local_drink' }, FOOTNOTE_F18 | FOOTNOTE_SPAM),
    makeAchievement('血流成河', '发布', 6, '篇血腥主题小说', makeTagChecker(129, '血腥'), { postId: 1073843628, epoch: 1731000000, matIcon: 'healing' }, FOOTNOTE_F18 | FOOTNOTE_SPAM),
    makeAchievement('人间便器', '发布', 6, '篇厕奴主题小说', makeTagChecker(143, '厕奴'), { postId: 1073843628, epoch: 1731000000, matIcon: 'wc' }, FOOTNOTE_F18 | FOOTNOTE_SPAM),
    makeAchievement('空白支票', '发布', 6, '篇贡奴主题小说', makeTagChecker(142, '贡奴'), { postId: 1073843628, epoch: 1731000000, matIcon: 'local_atm' }, FOOTNOTE_F18 | FOOTNOTE_SPAM),
    makeAchievement('雌雄莫辨', '发布', 6, '篇跨性别主题小说', makeTagChecker(80, '非二元性别'), { postId: 1073843628, epoch: 1732000000, matIcon: 'transgender' }, FOOTNOTE_F18 | FOOTNOTE_SPAM),
    makeAchievement('马可波罗', '发布', 6, '篇翻译作品', makeTagChecker(18, '翻译'), { postId: 1073843628, epoch: 1732000000, matIcon: 'translate' }, FOOTNOTE_F18 | FOOTNOTE_SPAM),
    makeAchievement('人型沙发', '在', 120, '个主题帖里成为首个回复者', async userId => {
        const data = await query<{ thread_id: number }>('SELECT thread_id FROM posts p_sel INNER JOIN thread_targets USING (thread_id) WHERE p_sel.user_id = $1 AND p_sel.most_recent_update > 0 AND thread_targets.last_page_number > 0 AND thread_targets.successive_fail >= 0 AND thread_targets.owner_user_id <> $1 AND thread_targets.spam_likeness < $2 AND NOT EXISTS (SELECT post_id FROM posts p_ref wHERE p_ref.thread_id = thread_targets.thread_id AND p_ref.posted_date < p_sel.posted_date AND p_ref.user_id <> thread_targets.owner_user_id)', [userId, SPAM_THRESHOLD]);
        const threads = data.rows.map(row => row.thread_id);
        return {
            desc: `在 ${threads.length} 篇他人的主题帖里，成为第一个回复者`,
            entries: threads,
            type: 'thread'
        };
    }, { postId: 1073843629, epoch: 1732000000, matIcon: 'chair' }, FOOTNOTE_SPAM | FOOTNOTE_EXCL_OWNER),
    makeAchievement('人型脚凳', '在', 333, '个主题帖里列身前五个回复', async userId => {
        const data = await query<{ thread_id: number }>('SELECT thread_id FROM posts p_sel INNER JOIN thread_targets USING (thread_id) WHERE p_sel.user_id = $1 AND p_sel.most_recent_update > 0 AND thread_targets.last_page_number > 0 AND thread_targets.successive_fail >= 0 AND thread_targets.owner_user_id <> $1 AND thread_targets.spam_likeness < $2 AND (SELECT COUNT(post_id) FROM posts p_ref wHERE p_ref.thread_id = thread_targets.thread_id AND p_ref.posted_date < p_sel.posted_date) <= 5', [userId, SPAM_THRESHOLD]);
        const threads = data.rows.map(row => row.thread_id);
        return {
            desc: `在 ${threads.length} 篇他人的主题帖里，列身前五个回复`,
            entries: threads,
            type: 'thread'
        };
    }, { postId: 1073843629, epoch: 1732000000, matIcon: 'chair_alt' }, FOOTNOTE_SPAM),
    makeAchievement('图书馆员', '给小说共计添加', 300, '个有效标签', async userId => {
        const data = (await query<{ thread_id: number, tag_id: number }>(`SELECT thread_id, tag_id FROM tag_votes tv INNER JOIN tag_association USING (tagass_id) WHERE tag_score > ${F18_CONSTS.CONSTS.SCORE_MULTIPLIER} AND tv.user_id = $1 AND vote > 0 AND NOT EXISTS (SELECT * FROM tag_votes tv_ref WHERE tv_ref.tagass_id = tv.tagass_id AND tv_ref.voted_at < tv.voted_at) AND EXISTS (SELECT * FROM thread_targets WHERE thread_targets.thread_id = tag_association.thread_id AND last_page_number > 0 AND successive_fail >= 0 AND spam_likeness < $2 AND forum_id = 18)`, [userId, SPAM_THRESHOLD])).rows;
        return {
            desc: `共为小说添加 ${data.length} 个实际生效的标签`,
            entries: data,
            type: 'tagass'
        };
    }, { postId: 1073843629, epoch: 1732000000, matIcon: 'style' }, FOOTNOTE_F18 | FOOTNOTE_SPAM | FOOTNOTE_FIRSTTAG),
    makeAchievement('禁书目录', '对', 150, '篇小说进行过标签操作', async userId => {
        const data = await query<{ thread_id: number }>(`SELECT DISTINCT(thread_id) FROM tag_votes tv INNER JOIN tag_association USING (tagass_id) WHERE tv.user_id = $1 AND ((vote > 0) OR (vote < 0 AND EXISTS (SELECT * FROM tag_votes tv_ref WHERE tv_ref.tagass_id = tv.tagass_id AND tv_ref.voted_at < tv.voted_at AND tv_ref.vote > 0))) AND EXISTS (SELECT * FROM thread_targets WHERE thread_targets.thread_id = tag_association.thread_id AND last_page_number > 0 AND successive_fail >= 0 AND spam_likeness < $2 AND forum_id = 18)`, [userId, SPAM_THRESHOLD]);
        const threads = data.rows.map(row => row.thread_id);
        return {
            desc: `为 ${threads.length} 篇小说执行了标签操作`,
            entries: threads,
            type: 'thread'
        };
    }, { postId: 1073843629, epoch: 1732000000, matIcon: 'local_library' }, FOOTNOTE_F18 | FOOTNOTE_SPAM | FOOTNOTE_TAGV_EXCL),
    makeAchievement('应援团长', '在', 66, '个主题帖里留下较长回复', async userId => {
        const data = await query<{ thread_id: number }>(`SELECT DISTINCT(thread_id) FROM posts INNER JOIN thread_targets USING (thread_id) WHERE posts.user_id = $1 AND thread_targets.last_page_number > 0 AND thread_targets.successive_fail >= 0 AND thread_targets.owner_user_id <> $1 AND thread_targets.spam_likeness < $2 AND posts.most_recent_update >= 0 AND LENGTH(REGEXP_REPLACE(SUBSTRING(content_sc FROM POSITION('\ufffc' IN content_sc) + 1), '\\s+', '', 'g')) >= 100`, [userId, SPAM_THRESHOLD]);
        const threads = data.rows.map(row => row.thread_id);
        return {
            desc: `在 ${threads.length} 个他人的主题帖里留下 100 字以上回复`,
            entries: threads,
            type: 'thread-me'
        };
    }, { postId: 1073843629, epoch: 1732000000, matIcon: 'comment' }, FOOTNOTE_SPAM | FOOTNOTE_WC_INACCURATE),
    makeAchievement('基友遍地', '与', 48, '个用户互相回复过', async userId => {
        const data = await query<{ user_id: number }>(`(SELECT DISTINCT user_id FROM posts WHERE most_recent_update >= 0 AND thread_id IN (SELECT thread_id FROM thread_targets WHERE last_page_number > 0 AND successive_fail >= 0 AND owner_user_id = $1 AND spam_likeness < $2)) INTERSECT (SELECT DISTINCT owner_user_id AS user_id FROM thread_targets INNER JOIN posts USING (thread_id) WHERE posts.user_id = $1 AND posts.most_recent_update >= 0) EXCEPT SELECT $1 AS user_id`, [userId, SPAM_THRESHOLD]);
        const users = data.rows.map(row => row.user_id);
        return {
            desc: `与 ${users.length} 个用户互相在对方的主题帖里回帖`,
            entries: users,
            type: 'author'
        };
    }, { postId: 1073843626, epoch: 1732000000, matIcon: 'connect_without_contact' }, FOOTNOTE_SPAM),
    makeAchievement('勋章狂热', '获得', 18, '个成就', async userId => {
        const data = (await query<{ name_sc: string }>('SELECT DISTINCT name_sc FROM user_achievements WHERE user_id = $1 AND post_id <> $2', [userId, META_POST_ID])).rows.map(row => row.name_sc);
        return {
            desc: `共获得 ${data.length} 个不同成就`,
            entries: data,
            type: 'achievement'
        };
    }, { postId: META_POST_ID, epoch: 1732000000, matIcon: 'emoji_events' }, FOOTNOTE_EXCL_META),
    makeAchievement('集邮大师', '获得', 6, '个金色成就', async userId => {
        const data = (await query<{ name_sc: string }>('SELECT name_sc FROM user_achievements WHERE user_id = $1 AND post_id <> $2 AND lvl = 3', [userId, META_POST_ID])).rows.map(row => row.name_sc);
        return {
            desc: `共获得 ${data.length} 个金色成就`,
            entries: data,
            type: 'achievement'
        };
    }, { postId: META_POST_ID, epoch: 1732000000, matIcon: 'auto_awesome' }, FOOTNOTE_EXCL_META)
]);

export const achivementNameIconLookUp = (name: string): string | undefined => ACHIEVEMENTS.find(a => a.name === name)?.matIcon;

export type PSingle = Readonly<{ // Programatically managed.
    desc: string, // Description of count.
    count: number, // Count.
    granted: [number, number, number]
}>;

export type MSingle = Readonly<{ // Manually managed.
    postId: number,
    name: string,
    level: number
    granted: number
}>;

const updateAchievementsImpl = async (userId: number, refDate: number): Promise<{
    p: PSingle[],
    m: MSingle[],
    currentTitle: string | null,
    cnt: number // Total number of achievements (i.e. number of rows in table).
}> => {
    const userRow = (await query<{ has_password: boolean; label: string | null; }, [number]>('SELECT label, password_h IS NOT NULL AS has_password FROM users WHERE user_id = $1', [userId])).rows[0];
    if (!userRow) { throw error(404, '该用户不存在。'); } 
    if (!userRow.has_password) { throw error(404, '成就系统不适用于未在镜像注册的用户。'); }

    const rows = (await query<{ granted: number, name_sc: string, lvl: number , post_id: number }, [number]>('SELECT name_sc, lvl, granted, post_id FROM user_achievements WHERE user_id = $1', [userId])).rows;
    const labelCandidates = new Set(rows.filter(row => row.lvl === 3).map(row => row.name_sc));
    const existingAchievements = rowsToMap(rows.map(row => Object.assign(row, { pk: row.name_sc + row.lvl })), 'pk');

    let cnt = 0;

    const dataForEach = await Promise.all(ACHIEVEMENTS.map(async ({ name, l3CountThreshold, checker, postId }) => {
        const grantedDate = refDate;
        const checkerResults = await checker(userId, refDate);
        // 0 when < 1/3; 1 when in [1/3, 2/3); 2 when in [2/3, 1), 3 when > 1.
        const actualProgress = Math.floor(Math.min(Math.max(checkerResults.entries.length * 3 / l3CountThreshold, 0), 3));
        const acquiredDates = [0, 0, 0] as [number, number, number]; // Bronze, silver, gold.
        for (let level = 3; level > 0; level--) {
            const existing = existingAchievements.get(name + level);
            if (actualProgress >= level) {
                cnt++;
                if (existing) {
                    acquiredDates[level - 1] = existing.granted;
                } else {
                    await query('INSERT INTO user_achievements (user_id, name_sc, lvl, post_id, granted) VALUES ($1, $2, $3, $4, $5) ON CONFLICT DO NOTHING', [userId, name, level, postId, grantedDate]);
                    acquiredDates[level - 1] = grantedDate;
                    if (level === 3) { labelCandidates.add(name); }
                }
            } else if (existing) { // Ideally we just skip the `if` check and delete regardless, but let's save some DB load.
                await query('DELETE FROM user_achievements WHERE user_id = $1 AND name_sc = $2 AND lvl = $3', [userId, name, level]);
                if (level === 3) { labelCandidates.delete(name); }
            }
            existingAchievements.delete(name + level);
        }
        return { desc: checkerResults.desc, count: checkerResults.entries.length, granted: acquiredDates };
    }));

    let currentTitle = userRow.label;
    if (currentTitle && !labelCandidates.has(currentTitle)) {
        unawaited(query('UPDATE users SET label = NULL WHERE user_id = $1', [userId]), 'removing user label');
        currentTitle = null;
    }

    cnt += existingAchievements.size;
    return {    
        // All remaining entries still in existingAchievements now are manually-managed.
        m: Array.from(existingAchievements.values()).map(entry => ({
            postId: entry.post_id,
            name: entry.name_sc,
            level: entry.lvl,
            granted: entry.granted
        })),
        // Programactially-managed ones.
        p: dataForEach,
        currentTitle,
        cnt
    };
};

export const updateAchievements = async (
    userId: number,
    requesterUserId: number
): Promise<{
    p: PSingle[],
    m: MSingle[],
    currentTitle: string | null
}> => {
    const startMillis = Date.now();
    const startSec = Math.floor(startMillis / 1000);
    const results = await updateAchievementsImpl(userId, startSec);
    unawaited(query('INSERT INTO stats.achi_ppl (updated_at, user_id, requester, count, duration) VALUES ($1, $2, $3, $4, $5)', [startSec, userId, requesterUserId, results.cnt, Date.now() - startMillis]), 'logging achi-ppl');
    return results;
};