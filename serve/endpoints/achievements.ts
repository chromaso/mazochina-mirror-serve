import { query as sql } from '../db';
import { HtmlSegmentImpl, LOGIN_REQUIRED, PageImpl, error, finalizePage, finalizeSegment } from '../utils/render_utils';
import { ACHIEVEMENTS, achivementFootnotes, updateAchievements } from '../shared/achievements';
import { parseInt32 } from '../utils/logic_utils';

import * as templates from '../templates/achievement';
import { escapeHtml } from '../../common/dependencies';
import { safeHtml } from 'typed-xhtml';
import { rowsToMultiMap } from '../../common/logic_utils';
import { allTagsById } from '../shared/tags';

export const postApiAuthor_id_achievements: HtmlSegmentImpl = async ctx => {
    const userId = parseInt32(ctx.params.id);
    if (isNaN(userId)) { throw error(400, '又在乱改网址了吗……'); }
    const loginUser = ctx.state.mazoLoginUser;
    if (!loginUser) { throw error(403, LOGIN_REQUIRED); }

    return finalizeSegment(ctx, templates.achievements(ctx, {
        entries: await updateAchievements(userId, loginUser.id),
        isSelf: (userId === loginUser.id) && !('preview' in ctx.query),
        userId
    }));
};

export const apiAuthor_id_achievements: HtmlSegmentImpl = async ctx => {
    const userId = parseInt32(ctx.params.id);
    if (isNaN(userId)) { throw error(400, '又在乱改网址了吗……'); }
    const loginUser = ctx.state.mazoLoginUser;
    if (!loginUser) { throw error(403, LOGIN_REQUIRED); }

    const name = String(ctx.query['name']);
    const confEntry = ACHIEVEMENTS.find(conf => conf.name === name);
    if (!confEntry) { throw error(404, '没有这个成就。'); }
    
    const { staticConv, contentConv } = ctx.state.render;

    const checker = confEntry.checker;
    const results = await checker(userId, ctx.refDate);
    let items: SafeString[];
    if (results.type === 'author') {
        const authors = await sql('SELECT user_id, user_name FROM users WHERE user_id = ANY($1::INT[])', [results.entries]);
        items = authors.rows.map(row => safeHtml(`<a href="/author/${row.user_id}">${escapeHtml(row.user_name)}</a>`));
    } else if ((results.type === 'thread') || (results.type === 'thread-me')) {
        const suffix = (results.type === 'thread-me') ? '?show=' + userId : '';
        const threads = await sql('SELECT thread_id, thread_title FROM thread_targets WHERE thread_id = ANY($1::INT[])', [results.entries]);
        items = threads.rows.map(row => safeHtml(`<a href="/thread/${row.thread_id}${suffix}">${escapeHtml(contentConv(row.thread_title))}</a>`));
    } else if (results.type === 'tagass') {
        const allTags = await allTagsById();
        const threadIds = new Set(results.entries.map(single => single.thread_id));
        const tagsAssThraed = rowsToMultiMap(results.entries, 'thread_id');
        items = (await sql<{ thread_id: number, thread_title: string }>('SELECT thread_id, thread_title FROM thread_targets WHERE thread_id = ANY($1::INT[])', [Array.from(threadIds)])).rows.map(row => 
            safeHtml(`<a href="/thread/${row.thread_id}">${escapeHtml(contentConv(row.thread_title))}</a>：<ul>${tagsAssThraed.get(row.thread_id)!.map(({ tag_id }) => `<li><a href="/tag/${tag_id}">${escapeHtml(staticConv(allTags.get(tag_id)!.primary_name))}</a></li>`).join('')}</ul>`)
        );
    } else if (results.type === 'achievement') {
        items = results.entries.map(nameSc => safeHtml(`<a href="/achievement?name=${encodeURIComponent(nameSc)}">${staticConv(nameSc)}</a>`));
    } else {
        items = results.entries.map(single => safeHtml(escapeHtml(String(single))));
    }

    return finalizeSegment(ctx, templates.achievementQualificationList(ctx, {
        desc: results.desc,
        items,
        footnotes: achivementFootnotes(confEntry)
    }));
};

export const achievement: PageImpl = async ctx => {
    const name = String(ctx.query['name'] || '');
    if (name.length < 1 || name.length > 10) { throw error(400, '成就名未指定。'); }
    const list = (await sql<{ user_id: number, max_lvl: number, user_name: string }>(`SELECT user_id, max_lvl, user_name FROM (SELECT user_id, MAX(lvl) AS max_lvl FROM user_achievements WHERE name_sc = $1 GROUP BY user_id) AS achi LEFT JOIN users USING (user_id) ORDER BY max_lvl DESC, user_name ASC`, [name])).rows;

    if (!list.length) { throw error(404, '成就不存在或尚无任何人获得。'); }

    const conf = ACHIEVEMENTS.find(conf => conf.name === name);

    return finalizePage(ctx, {
        title: `成就「${ctx.state.render.staticConv(name)}」`,
        canonicalPath: '/achievement?name=' + encodeURIComponent(name),
        nav: true
    }, templates.achievementGranteeList(ctx, { name, list, conf }));
};