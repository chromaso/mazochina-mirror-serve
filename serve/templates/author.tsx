import * as elements from 'typed-xhtml';
import { HtmlTmpl } from '../utils/tsx_utils';
import { BaseThreadTarget, RenderThreadRetVal } from '../shared/thread_table';
import { threadTable } from './thread_table';
import { CONFIRMATION_ADDITIONALS, CONFIRMATION_CONSTS, RATE_LIMIT_CONSTS } from '../shared/author';
import { thirdPartyAvatarUrl, dualAvatar, firstPartyAvatarUrl } from '../utils/avatar';
import { achievementInHovercard } from './achievement';

// Migrated from author.pug.
export const author: HtmlTmpl<{
    userId: number,
    userName: string,
    email: string | null,
    profileText: string[] | null,
    avatarSmallInt: number,
    lastLocalAvatar: number, // If not isSelf, always 0; if isSelf: first-party avatar ID, or zero.
    isLocal: boolean,
    isOriginal: boolean,
    confirmed: boolean,
    label: string | null,
    registeredAt: number,
    localRegisterDate: number,
    violationCount: number,
    blocked: boolean,
    threadCount: number,
    postCount: number,
    collectionCount: number,
    favCount: number,
    threads: RenderThreadRetVal<BaseThreadTarget>,
    pinnedThreadsCount: number,
    isSelf: boolean,
    canAdminUser: boolean
}> = (ctx, self) => {
    const { render } = ctx.state;
    return (
        <div class="container">
            {(ctx.state.mazoLoginUser!.id === self.userId) ? (
                <div class="alert alert-info">{self.isSelf ? <>
                    你正在查看自己的个人页面。<a href="?preview">点此</a>预览其它已登入用户查看此页面时的效果。
                </> : <>
                    你正在预览其它已登入用户查看你的个人页面时的效果。此页面对于未登入用户及搜索引擎等皆不可见。
                </>}</div>
            ) : null}
            <div class="d-flex align-items-center mb-3 flex-wrap justify-content-center">
                <div id="profile-avatar-container" class="position-relative flex-shrink-0" style="width: 120px; height: 120px">
                    {dualAvatar(self.userId, self.userName, self.email, self.avatarSmallInt, true)}
                    {self.isSelf ? (
                        <a href={`javascript:editAvatarPopup(${self.avatarSmallInt}, '${self.email}', '${thirdPartyAvatarUrl(self.email!, self.avatarSmallInt, true)}', '${self.lastLocalAvatar ? firstPartyAvatarUrl(self.lastLocalAvatar, true) : ''}')`} class="btn btn-secondary btn-sm m-2 position-absolute end-0 bottom-0">
                            <span class="material-icons">settings</span> 头像
                        </a>
                    ) : null}
                </div>
                <h1 class="mx-3 flex-grow-1 text-break">
                    {self.userName}
                    {self.label ? (
                        <span class="badge bg-secondary ms-2">
                            {render.staticConv(self.label)}
                        </span>
                    ) : null}
                </h1>
                {(self.isLocal && !self.isSelf) ? (
                    <a href={'/pm/' + self.userId} class="btn btn-primary">
                        <span class="material-icons me-1">mail</span>私信
                    </a>
                ) : null}
            </div>
            <div class="row">
                {(self.isSelf || self.profileText) ? (
                    <div class="col-md">
                        <div class="card mb-4">
                            <div class="card-header">自我介绍</div>
                            {self.isSelf ? (
                                <div id="edit-introduction" class="card-body d-none">
                                    <form method="post">
                                        <div class="form-group">
                                            <textarea
                                                name="introduction"
                                                placeholder="输入个人简介（仅登录用户可见，留空以删除）"
                                                class="form-control">
                                                {self.profileText ? self.profileText.join('\n') : ''}
                                            </textarea>
                                        </div>
                                        <div class="form-group mb-0">
                                            <a class="btn btn-secondary" href="javascript:cancelEditIntro()">
                                                取消
                                            </a>
                                            <input type="submit" class="btn btn-primary ms-2" name="profile" value="提交" />
                                        </div>
                                    </form>
                                </div>
                            ) : null}
                            <div class="card-body">
                                {self.profileText ? (
                                    <>
                                        <p class="card-text">
                                            {self.profileText.map(profilePara => [render.contentConv(profilePara), <br />])}
                                        </p>
                                        {(self.isSelf) ? (
                                            <a class="btn btn-primary btn-sm" href="javascript:editIntro()">
                                                <span class="material-icons">edit</span>
                                            </a>
                                        ) : null}
                                    </>
                                ) : (
                                    <a class="btn btn-sm btn-primary" href="javascript:editIntro()">
                                        撰写自我介绍
                                    </a>
                                )}
                            </div>
                        </div>
                    </div>
                ) : null}
                {self.isSelf ? (
                    // Must be after creating the elements.
                    <script type="text/javascript">{elements.safeHtml(`initProfileEdit();`)}</script>
                ) : null}
                <div class="col-md">
                    <table class="table table-sm">
                        <tr>
                            <td>用户类型</td>
                            <td>
                                {self.isLocal ? (
                                    <span class="badge bg-primary">镜像作者</span>
                                ) : null}
                                {(self.isLocal && self.isOriginal) ? ' ': ''}
                                {self.isOriginal ? (
                                    <span class="badge bg-primary">源站作者</span>
                                ) : null}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                资深用户（<a href="/confirmation">?</a>）
                            </td>
                            <td>{self.confirmed ? '是' : '否'}</td>
                        </tr>
                        <tr>
                            <td>注册于（{self.isOriginal ? '源站' : '镜像'}）</td>
                            {self.registeredAt ? (
                                <td>{render.renderDate(self.registeredAt)}</td>
                            ) : (
                                <td>未知</td>
                            )}
                        </tr>
                        {self.isOriginal && self.isLocal ? (
                            <tr>
                                <td>注册于（镜像）</td>
                                {self.localRegisterDate ? (
                                    <td>{render.renderDate(self.localRegisterDate)}</td>
                                ) : (
                                    <td>未知</td>
                                )}
                            </tr>
                        ) : null}
                        <tr>
                            <td>被管理员处理</td>
                            {(self.violationCount > 0) ? (
                                <td>
                                    <a href={'/moderation?u=' + self.userId}>{self.violationCount} 次</a>
                                </td>
                            ) : (
                                <td>从未</td>
                            )}
                        </tr>
                        {self.isSelf ? (
                            <tr>
                                <td>
                                    邮箱 <span class="badge bg-secondary">仅自己可见</span>
                                </td>
                                <td>
                                    {self.email}
                                    <form action="/user/security" method="post">
                                        <button class="btn btn-secondary btn-sm" type="submit">
                                            修改邮箱或密码
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        ) : null}
                    </table>
                </div>
            </div>
            <div class="m-n1">
                <a class="btn btn-primary m-1" href={'/query?type=thread&uid=' + self.userId}>
                    发布的主题（{self.threadCount}）
                </a>
                <a class="btn btn-secondary m-1" href={'/query?type=post&uid=' + self.userId}>
                    发布的文章（{self.postCount}）
                </a>
                {self.isLocal ? (
                    <>
                        <a class="btn btn-secondary m-1" href={'/author/' + self.userId + '/favs'}>
                            {self.collectionCount} 个收藏夹（含 {self.favCount} 个主题）
                        </a>
                    </>
                ) : null}
                {self.canAdminUser ? (
                    <a class="btn btn-secondary m-1" href={'javascript:mazomirrorAdmin("user", ' + self.userId + ', ' + (self.blocked ? 1 : 0) + ')'}>
                        {self.blocked ? '解封' : '封禁'}
                    </a>
                ) : null}
            </div>
            {self.blocked ? (
                <div class="alert alert-secondary my-4">
                    该用户已被镜像管理员封禁（<a href={'/moderation?u=' + self.userId}>查看封禁理由</a>）
                </div>
            ) : null}
            <h3 class="mt-4 mb-3">成就</h3>
            {self.isLocal ? (
                <div id="per-user-achivement-tgt">
                    <div class="spinner-border" />
                    <script type="text/javascript">{elements.safeHtml('loadAchisInAuthor();')}</script>
                </div>
            ) : <div>成就系统只适用于在镜像注册过的用户。该用户未在镜像注册过。</div>}
            {self.threads.length ? (
                <>
                    <h3 class="mt-4 mb-3 d-flex justify-content-between align-items-center">
                        <span>{self.userName} 发布的主题</span>
                        {self.isSelf ? <a class="btn btn-secondary" href="javascript:pinnedThreadsMgmt()"><span class="material-icons">settings</span></a> : null}
                    </h3>
                    <div id="author-page-thread-table">
                        {threadTable(ctx, {
                            threads: self.threads,
                            showReadState: true
                        })}
                    </div>
                    {(self.threadCount > self.threads.length) ? (
                        <div class="text-center my-4">
                            <a class="btn btn-primary m-1" href={'/query?type=thread&uid=' + self.userId}>
                                全部（{self.threadCount}）
                            </a>
                        </div>
                    ) : null}
                    <script type="text/javascript">{elements.safeHtml(`initPinnedThreads(${self.pinnedThreadsCount});`)}</script>
                </>
            ) : null}
        </div>
    );
};

export const authorHovercard: HtmlTmpl<{
    id: number,
    name: string,
    avatar: SafeString,
    isLocal: boolean,
    profileText: string | undefined,
    achievements: [string, 1 | 2 | 3][],
    threadCount: number,
    postCount: number
}> = (ctx, self) => {
    const nonEmptyLines = (self.profileText?.split('\n') ?? []).filter(line => line.trim());
    return (
        <>
            <div class="d-flex overflow-hidden position-relative align-items-start">
                <a class="flex-shrink-0 mm-87-ava mb-2" href={`/author/${self.id}/`}>{self.avatar}</a>
                <div class="flex-grow-1 ms-3">
                    <a href={`/author/${self.id}/`} class="text-nowrap lead fw-bold ui-link">{self.name}</a>
                    {nonEmptyLines.length ? <p>{(nonEmptyLines[0].length > 42) ? (nonEmptyLines[0].substring(0, 40) + '…') : nonEmptyLines[0]}{(nonEmptyLines.length > 1) ? <><br />…</> : null}</p> : null}
                    {self.isLocal ? null : <p class="text-muted">该用户未在镜像注册</p>}
                </div>
            </div>
            {self.achievements.length ? (
                <a href={`/author/${self.id}/`}>
                    {self.achievements.slice(0, (self.achievements.length > 5) ? 4 : 5).map(achievement => achievementInHovercard(ctx, achievement))}
                    {(self.achievements.length > 5) ? <span class="badge rounded-pill bg-transparent border text-reset mazomirror-tip" data-title={`另还有 ${self.achievements.length - 4} 个成就`}>+ {self.achievements.length - 5}</span> : null}
                </a>
            ) : null}
            <div class="d-flex text-center mt-2">
                <div class="flex-grow-1">
                    <a class="btn btn-link" href={'/query?type=thread&uid=' + self.id}>
                        {self.threadCount} 主题
                    </a>
                </div>
                <div class="flex-grow-1">                    
                    <a class="btn btn-link" href={'/query?type=post&uid=' + self.id}>
                        {self.postCount} 文章
                    </a>
                </div>
                <div class="flex-grow-1">                    
                    {self.isLocal ? (
                        <a href={'/pm/' + self.id} class="btn btn-primary">
                            <span class="material-icons me-1">mail</span>私信
                        </a>
                    ) : (
                        <button class="btn btn-link disabled">
                            无法私信
                        </button>
                    )}
                </div>
            </div>
        </>
    );
};

// Migrated from user_disambiguation.pug.
export const userDisambiguation: HtmlTmpl<{
    username: string,
    users: {
        link: string,
        id: number,
        isOriginal: boolean,
        isLocal: boolean,
        name: string
    }[]
}> = (ctx, { username, users }) => (
    <div class="container">
        <div class="card">
            <div class="card-header">消歧义页</div>
            <div class="card-body">
                <p class="card-text">
                    有多名用户使用「{username}」为用户名，请选择你具体所指：
                </p>
                <div class="list-group">
                    {users.map(user => (
                        <a href={user.link} class="list-group-item list-group-item-action">
                            ID 为 {user.id} 的
                            {user.isLocal ? '镜像用户' : null}
                            {user.isLocal && user.isOriginal ? '暨' : null}
                            {user.isOriginal ? '源站用户' : null}「
                            {user.name}」
                        </a>
                    ))}
                </div>
            </div>
        </div>
    </div>
);

// Migrated from user_confirmation.pug.
export const userConfirmation: HtmlTmpl<{
    threadValid: boolean,
    firstPostValid: boolean,
    postCountValid: boolean,
    threads: {
        thread_id: number,
        thread_title: string,
        days: number,
        posted_date: number,
        num_repliers: number,
        valid: number
    }[],
    mirrorPostCount: number,
    firstMirrorPost: {
        post_id: number,
        title: string,
        posted_date: number
    }
}> = (ctx, self) => {
    const { render } = ctx.state;
    return (
        <div class="container">
            {(self.threadValid && self.firstPostValid && self.postCountValid) ? (
                <div class="card" style='margin: 0 auto; max-width: 480px'>
                    <div class="card-header">操作成功</div>
                    <div class="card-body">
                        <p class="card-text">你已成为「资深用户」。</p>
                        <a id="primary-link" href="/" class="btn btn-primary">
                            跳转至首页
                        </a>
                    </div>
                </div>
            ) : (
                <>
                    {!self.threadValid ? (
                        self.threads.length ? (
                            <>
                                <p>
                                    你的主题帖尚未达到「资深用户」的标准。下面为你最接近标准的几个主题帖，供参考。
                                </p>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>主题</th>
                                            <th>发布于</th>
                                            <th>回复人数</th>
                                            <th />
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {self.threads.map(thread => (
                                            <tr>
                                                <td>
                                                    <a href={'/thread/' + thread.thread_id}>
                                                        {thread.thread_title}
                                                    </a>
                                                </td>
                                                <td>
                                                    {render.renderDate(thread.posted_date)}（{thread.days} 天）
                                                </td>
                                                <td>{thread.num_repliers}</td>
                                                <td>
                                                    {(thread.valid === 2) ? (
                                                        <span class="material-icons">done_all</span>
                                                    ) : (thread.valid === 1) ? (
                                                        <span class="material-icons">done</span>
                                                    ) : null}
                                                </td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </table>
                            </>
                        ) : (
                            <p>你从未发布过主题帖，因此无法达到「资深用户」的标准。</p>
                        )
                    ) : null}
                    {!self.postCountValid ? (
                        <p>
                            你在镜像发帖数量（{self.mirrorPostCount}）少于 {CONFIRMATION_ADDITIONALS.postCount}，因此无法达到「资深用户」的标准。
                        </p>
                    ) : null}
                    {!self.firstPostValid ? (
                        !self.firstMirrorPost ? (
                            <p>你从未在镜像发帖，因此无法达到「资深用户」的标准。</p>
                        ) : (
                            <p>
                                你在镜像发的第一个帖子 
                                {' '}
                                <a href={'/post/' + self.firstMirrorPost.post_id}>
                                    {render.contentConv(self.firstMirrorPost.title)}
                                </a>
                                {' '}
                                发布于 {render.renderDate(self.firstMirrorPost.posted_date)}，尚未满 {CONFIRMATION_ADDITIONALS.daysSinceMirror} 天，因此无法达到「资深用户」的标准。
                            </p>
                        )
                    ) : null}
                    <p>
                        <a id="primary-link" class="btn btn-primary" href="/confirmation">返回</a>
                    </p>
                </>
            )}
        </div>
    );
};

// Migrated from user_rules.pug.
export const userRules: HtmlTmpl<{
    preview: {
        threads: number,
        posts: number,
        pms: number,
        group: number
    },
    confirmed: boolean
}> = (ctx, self) => (
    <div class="container">
        <h3>关于发帖/私信频率限制</h3>
        <p>由于本站出现了滥发广告的用户，每 24 小时内发帖/私信数量限制如下：</p>
        <table class="table">
            <thead>
                <tr>
                    <th>用户组别</th>
                    <th>发帖：Soft limit</th>
                    <th>发帖：hard limit</th>
                    <th>发送新的私信</th>
                </tr>
            </thead>
            <tbody>
                {[
                    '注册未满 24 小时的用户',
                    '注册未满 72 小时的用户',
                    '注册满 72 小时用户',
                    '资深用户',
                ].map((text, index) => (
                    <tr>
                        <td>
                            {(self.preview.group === index) ? <b>{text}（你）</b> : text}
                        </td>
                        <td>{RATE_LIMIT_CONSTS[index][0]}</td>
                        <td>{RATE_LIMIT_CONSTS[index][1]}</td>
                        <td>{RATE_LIMIT_CONSTS[index][2]}</td>
                    </tr>
                ))}
            </tbody>
        </table>
        <p>关于发帖数量的计算：</p>
        <ul>
            <li>
                连续 24 小时内发帖数量达到 soft limit 者，需要输入 CAPTCHA
                才可发帖；发帖数量达到 hard limit 者，暂时不可继续发帖。
            </li>
            <li>
                每个回帖按照 1 个发帖计算，而每个主题帖按照 5
                个发帖计算。已删除或隐藏的主题帖和回帖也会计算在内。
            </li>
            <li>
                你最近 24 小时发了主题 <b>{self.preview.threads}</b> 个，回帖 <b>{self.preview.posts - self.preview.threads}</b> 个，故发帖数计算作 {self.preview.threads}×5+{self.preview.posts - self.preview.threads} = <b>{self.preview.threads * 4 + self.preview.posts}</b>。
            </li>
        </ul>
        <p>关于「发送新的私信」的计算：</p>
        <ul>
            <li>仅当向从未与你有过私信来往的用户首次发送私信时才计入。</li>
            <li>
                你最近 24 小时共向 {self.preview.pms} 个陌生人发送私信。
            </li>
        </ul>
        <h3>关于「资深用户」</h3>
        <p>「资深用户」共有以下几点效果：</p>
        <ul>
            <li>即上一节所述，发帖频率限制较宽松；</li>
            <li>仅有资深用户可以使用「回复可见」功能；</li>
            <li>资深用户每日可上传的图片数量和每个图片的文件大小限制均较宽松；</li>
            <li>资深用户可行使管理权限，隐藏或移动论坛中的任何主题。</li>
        </ul>
        <p>成为「资深用户」，你需要同时满足三项条件：</p>
        <ul>
            <li>
                从你<i>在镜像</i>首次发帖后，至少已过去 {CONFIRMATION_ADDITIONALS.daysSinceMirror} 天。
            </li>
            <li>
                你<i>在镜像</i>总发帖数量（主题帖或回帖）达到 {CONFIRMATION_ADDITIONALS.postCount}。
            </li>
            <li>
                满足以下两个条件之一（在源站或镜像皆可）：
                <ul>
                    <li>
                        发布过一个得到过至少 {CONFIRMATION_CONSTS[0][0]} 个不同用户的回复的主题（且那个主题需要已经发布满 {CONFIRMATION_CONSTS[0][1]} 天）；
                    </li>
                    <li>
                        发布过两个得到过至少 {CONFIRMATION_CONSTS[1][0]} 个不同用户的回复的主题（且那两个主题需要已经发布满 {CONFIRMATION_CONSTS[1][1]} 天）。
                    </li>
                </ul>
            </li>
        </ul>
        {self.confirmed ? (
            <p>
                你已经是资深用户了。请注意，若你长期不活跃，则可能会被取消资深用户状态。
            </p>
        ) : (
            <form method="post">
                如果你已经满足上述条件，你仍需要手动点击
                <button class="btn btn-sm btn-primary mx-1">
                    我确认不会滥发广告或滥用管理权限
                </button>
                完成确认，方可成为「资深用户」。请注意，若你长期不活跃，则可能会被取消资深用户状态。
            </form>
        )}
    </div>
);