import { query } from './db';
import { crawlIndexPage } from './crawl';
import throttle from './global_throttle';

export const doNext = async () => {
    const lock = await throttle();
    let result: Awaited<ReturnType<typeof crawlIndexPage>>;
    try {
        result = await crawlIndexPage();
    } catch (e) {
        lock.fail();
        throw e;
    }
    lock.success();
    await Promise.all(result.forums.map(forum =>
        query('UPDATE forum_targets SET dirty_most_recent_update = COALESCE(NULLIF($1, most_recent_update_include_children), 0) WHERE dirty_most_recent_update = 0 AND forum_id = $2', [forum.lastPost, forum.id])
    )); // make sure previous SQLs are done before changing dirty_until_page again
};