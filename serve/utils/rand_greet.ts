import { promises as fs } from 'fs';
import { fromRoot } from '../path';
import { MazoKoaContext } from './logic_utils';

const makeRandGreet = (fileName: string): (ctx: MazoKoaContext) => string => {
    const candidates: string[] = ['服务器刚刚启动'];
    fs.readFile(fromRoot('conf', fileName), 'utf8').then(str =>
        candidates.splice(0, 1, ...str.split('\n').filter(_ => _))
    ).catch(e => console.error(`Failed makeRandGreet(${fileName})!`, e));
    return ctx => ctx.state.render.staticConv(candidates[(Math.random() * candidates.length) | 0]);
};

export const homeGreet = makeRandGreet('home.txt');
export const hiddenGreet = makeRandGreet('hidden.txt');

