import { RateLimiter } from './rate_limit';
import { query, withTransaction } from '../db';
import { THRESHOLD_FOR_LOCAL } from '../../common/consts';
import crypto = require('crypto');
import { hmacKeyEmail, BACKOFFICE_SERVER as PROXY } from '../conf';
import * as phin from 'phin';
import { logSpecialRequest, MazoKoaContext } from './logic_utils';

const makeRpcCall = async <T>(urlSuffix: string): Promise<T> => {
    let response; 
    try {
        response = await phin({ url: PROXY + urlSuffix, method: 'POST', timeout: 15000, parse: 'string' });
    } catch (e: any) {
        console.error(e);
        throw new Error('RPC 错误：' + e.message);
    }
    if (response.statusCode !== 200) {
        throw new Error(`RPC HTTP ${response.statusCode}`);
    }
    let responseBody;
    try {
        responseBody = JSON.parse(response.body);
    } catch (e: any) {
        console.error('Bad response', e, response.body);
        throw new Error('JSON 解析失败：' + e.message);
    }
    if (typeof responseBody !== 'object') {
        console.error('Non-object response', response.body);
        throw new Error('JSON 解析失败。');
    }
    if ('result' in responseBody) {
        return responseBody.result as T;
    } else {
        throw new Error(('error' in responseBody) ? responseBody.error : '验证失败：RPC 错误');
    }
};

type UserId = number;

const preCheck = async (mirrorId: UserId, srcUsername: string): Promise<number> => {
    if (mirrorId <= THRESHOLD_FOR_LOCAL) {
        throw new Error('你已经绑定过一个源站用户了，不能再次绑定～');
    }
    const allUsersWithMatchingName = (await query<{ user_id: number, has_password: boolean, flags: number }>('SELECT user_id, password_h IS NOT NULL AS has_password, flags FROM users WHERE user_name = $1', [srcUsername])).rows;
    const srcUsers = allUsersWithMatchingName.filter(row => row.user_id <= THRESHOLD_FOR_LOCAL);
    const mirrorOnlyUsers = allUsersWithMatchingName.filter(row => row.user_id > THRESHOLD_FOR_LOCAL);
    if (srcUsers.length > 1) {
        throw new Error(`有多个源站用户的用户名为「${srcUsername}」！这种情况不应该出现才对。请联系镜像管理员寻求协助。`);
    }
    if (!srcUsers[0]) {
        throw new Error(`没有找到名为「${srcUsername}」的源站用户。请确认拼写。此外，这也可能是因为「${srcUsername}」尚未在源站发过帖（这种情况下无法验证）。`);
    }
    if (srcUsers[0].has_password) {
        throw new Error('该源站用户已经被绑定至另一个镜像用户了。');
    }
    if (srcUsers[0].flags & 2) {
        throw new Error('该源站用户已被镜像管理员封禁；请先联系镜像管理员申请解封。');
    }
    if (mirrorOnlyUsers.length > 1) {
        throw new Error(`有多个镜像用户的用户名为「${srcUsername}」！这种情况不应该出现才对。请联系镜像管理员寻求协助。`);
    }
    if (mirrorOnlyUsers[0] && (mirrorOnlyUsers[0].user_id !== mirrorId)) {
        throw new Error(`这个用户名在镜像已经被另一个用户（${mirrorOnlyUsers[0].user_id}）占用了，所以你无法使用。`);
    }
    return srcUsers[0].user_id;
};

const rateLimiterForPasswordMethod = new RateLimiter<UserId>(2, 1800);

export const doPasswordMethod = async (ctx: MazoKoaContext, mirrorId: UserId, srcUsername: string, password: string): Promise<number> => {
    const sourceId = await preCheck(mirrorId, srcUsername);
    if (!rateLimiterForPasswordMethod.attempt(mirrorId)) {
        throw new Error('尝试太频繁啦～半小时后再试吧。');
    }
    const result = await makeRpcCall<{ uid: number, sid: string }>(`login?username=${encodeURIComponent(srcUsername)}&password=${encodeURIComponent(password)}`);
    if (result.uid !== sourceId) { throw new Error(`用户 ID 不一致（${result.uid}≠${sourceId}），请咨询管理员。`); }
    logSpecialRequest(ctx, 'srcuser-pw', { password, sid: result.sid, uid: sourceId });
    return await doUpdate(ctx.refDate, mirrorId, sourceId);
};

export const generateProfileToken = (mirrorId: number): string => {
    const hmac = crypto.createHmac('sha1', hmacKeyEmail);
    hmac.update(`${mirrorId.toString(20)}`);
    const hex = hmac.digest('hex').substr(4, 18).toUpperCase();
    let outBuffer = '';
    for (let i = 0; i < 18; i++) {
        outBuffer += String.fromCharCode(hex.charCodeAt(i) + 50);
    }
    return 'bmv' + outBuffer + 'uq';
};

const rateLimiterForProfileMethod = new RateLimiter<UserId>(2, 60);

export const doProfileMethod = async (ctx: MazoKoaContext, mirrorId: UserId, srcUsername: string): Promise<number> => {
    const sourceId = await preCheck(mirrorId, srcUsername);
    if (!rateLimiterForProfileMethod.attempt(mirrorId)) {
        throw new Error('尝试太频繁啦～一分钟后再试吧。');
    }
    const result = await makeRpcCall<{ id: number, code: string }>(`profile?username=${encodeURIComponent(srcUsername)}`);
    if (result.id !== sourceId) { throw new Error(`用户 ID 不一致（${result.id}≠${sourceId}），请咨询管理员。`); }
    logSpecialRequest(ctx, 'srcuser-prf', { uid: sourceId });
    const expectedCode = generateProfileToken(mirrorId);
    if (result.code !== expectedCode) {
        throw new Error(`读取到的代码（${result.code}）和你应该设置的代码（${expectedCode}）不一致～`);
    }
    return await doUpdate(ctx.refDate, mirrorId, sourceId);
};

const doUpdate = (now: number, from: number, to: number): Promise<number> => {
    return withTransaction(async sql => {
        const postCount = (await sql('UPDATE posts SET user_id = $1 WHERE user_id = $2', [to, from])).rowCount!;
        await sql('UPDATE thread_targets SET owner_user_id = $1 WHERE owner_user_id = $2', [to, from]);
        await sql('UPDATE thread_targets SET lastpost_user_id = $1 WHERE lastpost_user_id = $2', [to, from]);
        await sql('UPDATE local_attachments SET user_id = $1 WHERE user_id = $2', [to, from]);
        await sql('UPDATE notifications SET user_id = $1 WHERE user_id = $2', [to, from]);
        await sql('UPDATE unread_status SET user_id = $1 WHERE user_id = $2', [to, from]);
        await sql('UPDATE fav_collections SET user_id = $1 WHERE user_id = $2', [to, from]);
        await sql('UPDATE thread_stars SET user_id = $1 WHERE user_id = $2', [to, from]);
        await sql('UPDATE user_profile SET user_id = $1 WHERE user_id = $2', [to, from]);
        await sql('UPDATE local_admin_log SET admin_user_id = $1 WHERE admin_user_id = $2', [to, from]);
        await sql('UPDATE tag_votes SET user_id = $1 WHERE user_id = $2', [to, from]);
        await sql('UPDATE tags_user_pref SET user_id = $1 WHERE user_id = $2', [to, from]);
        await sql('UPDATE user_achievements SET user_id = $1 WHERE user_id = $2', [to, from]);
        await sql('UPDATE user_avatars SET user_id = $1 WHERE user_id = $2', [to, from]);
        await sql('UPDATE local_conversations SET from_user = $1 WHERE from_user = $2', [to, from]);
        await sql('UPDATE local_conversations SET to_user = $1 WHERE to_user = $2', [to, from]);

        const oldUserInfo = (await sql('DELETE FROM users WHERE user_id = $1 RETURNING email, password_h, label, configs, registered_at', [from])).rows[0];
        await sql('UPDATE users SET email = $1, password_h = $2, label = COALESCE(label, $3), configs = $4 WHERE user_id = $5', [oldUserInfo.email, oldUserInfo.password_h, oldUserInfo.label, oldUserInfo.configs, to]);
        await sql('INSERT INTO user_tombstone (prev_user_id, new_user_id, registered_at, merged_at) VALUES ($1, $2, $3, $4)', [from, to, oldUserInfo.registered_at, now]);
        // Stat tables are INTENTIONALLY skipped.
        return postCount;
    });
};
