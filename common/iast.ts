export const MAX_LENGTH_PER_INPUT = 30; // Up to 30 characters no matter what type it is.

export const RAW_IDENTIFIER_PATTERN = /^[^\s\\\/|!=&[\]()（）]{1,25}$/;
export const ESCAPED_IDENTIFIER_PATTERN = /^(?:[a-z0-9\-_\\~]){1,150}$/; // Worst case: any Chinese character escaped to become 6.

// Partially reverse escapeForIast to get something barely readable. See escapeForIast.
export const unescapeForIast = (text: string) => text.replace(/\\u[0-9a-f]{4}/g, codeSeq => String.fromCharCode(parseInt(codeSeq.substring(2), 16)));

export type FunctionName = 'FN' | 'GN' | 'LC';

export type IastInput = {
    type: 
        'cnm' | // Chinese full name.
        'any' | // Freetext.
        string[]; // Array of multiple choice items.
    default?: string; // Default value.
    uname?: true; // Defaults to be current (reader) user name. Fallback to `default` if not logged in.
    priv?: true;
}

export type IastDependencyRequirement = {
    text?: ('any' | FunctionName)[]; // For [ref] calls.
    options?: string[]; // For [if] calls.
};

const singleCharDecode = (str: string): string => String.fromCharCode(parseInt(str, 36));

// Using some encoding, otherwise the SC -> TC converter will mess things up (TC bundle not recognizing SC surnames, reporting error).
const chineseFamilyName1 = 'kemmtnisgg7stookfcrxxvd0go8gmsivki1lpghkdtul4kgnfo5smlui4p5jkn5i2zsltroiu09gswg4vfimq5fq3ro3vkc9qypsk3rlkfwllfskceiulglxpu7ggyq6jqaprwefepuofqe3gl2tn2fo9lzskf0jechlbtcvleyn5cflnhr0pvnk3dnozhqyrotiqesl5mfessxtomsm5i1gnelidwipjlazskxo2ulengkyu4efqnsl1i1rvixfevl9xlxxtddlesi7tvdak5vim0l7qfh0rwarx2viyk1ziouky2g34l9zk3htpylkap9bi2xu58fu2ffpml7lrdpt2i1vfryo9suwxq57fm5u06n5fi78latpc2fffmcmgltkj7skis1bib7vgwlhbkn9q0bio4sd7of5nz1ijqlmqrp3ny9k52poypbzmlbggsrcpg37pufg5orw9p0qtwznh7o9xl5zn44u49kc2jdclrsto3r1wilpgh7kiti88ux9fnxr5vtu5p8htqjkr1n4ok8fqdggglgedgl4ki7i2pkj3tnykletmdr8jit6kj8ggesbqg3tjense1sd8gllubag75nnjjdmffcgkkokzleolw5k7fpu9skspnrn5xi3vkf9igqpung2wr2yiq1oe8hs4nh2obqhnekdko5ig2imhhslqsl4g3krnshrgi8wsf2sm4k7jklasldgueq6xrkxgxnjqlo86sf3i2vk0monwso2g53ggjptdfk7pryq8tsn1ijto59fitp1fflefgwqjiggui69oaig58gini3cqxvkt6fofkk7gf5i3biokgkwijvty8fzbkltpxirnwtfajfwk5qlg9fm7ia9fhcpglfrxgp8qbeit1gaxtpvksqryxvczgmri23ucsp6tsodrh9tznucuk3gqctso7rapgn6qh3nhjqarrt4q4ptkru8ot4uog4riysnmtq0u3rvilq3wt2altbgx8rswrucvisvioqi5u33h4kluzqh2upbtzfn6qpdigkiiilvgqpzehg7k76tlorsroowl74u11si2tq5qxnuq9gxai5jtjoit3q27s7esjuuaak7dsnok9ui3yfpioiphttq8bve0revsgzl5eunlr9ko93sj6qfhtxrovokxuj7ojbdpoapxmrg6sxsjq2p8yqgq'.match(/.{3}/g)!.map(singleCharDecode);

const chineseFamilyName2 = 'feyi3cfl0mogoa7kdkl5zto3ggni3vgl4ui4gl4ivmi2vk1zhlbfqnffgk3dj85i49i6hsf3g30i1lnevn57sbtsd1r5rtm0rncq57l74tq5ggni3ygl4ucskflk3di6hsj6g30i23s7msatr5rtj4rg8q57'.match(/.{6}/g)!.map(num => singleCharDecode(num.substring(0, 3)) + singleCharDecode(num.substring(3, 6)));

// seed must be uniformly distributed numbers between [0, 1) (just like Math.random()).
// serial must be a positive integer.
export const randomName = (seed: number, serial: number): string =>
    chineseFamilyName1[Math.floor(Math.pow(seed, 1.5) * 200)] + '二三四五六七八九一'.charAt(serial % 9);

export const legalChineseName = (name: string): [string, string] => {
    if (name.length > 4) { throw RangeError('姓名不可超过四个字'); }
    if (name.length < 2) { throw RangeError('姓名不可短于两个字'); }
    const illegals = name.match(/[^\u4E00-\u9FA5\u3400-\u4DB5]/g);
    if (illegals) {
        throw RangeError(`${illegals.join('、')}并非合法汉字`);
    }
    if (name.length >= 3) {
        const doubleFnAttempt = chineseFamilyName2.indexOf(name.substr(0, 2));
        if (doubleFnAttempt >= 0) {
            return [chineseFamilyName2[doubleFnAttempt], name.substr(2)];
        }
    }
    const firstFnAttempt = chineseFamilyName1.indexOf(name.charAt(0));
    if (firstFnAttempt < 0) {
        throw RangeError(`${name.charAt(0)}并非已知的汉语姓氏，请尝试使用更常见的姓氏`);
    }
    return [chineseFamilyName1[firstFnAttempt], name.substr(1)];
};

export const expressionRuntime: Record<FunctionName, (input: string) => string> = {
    'FN': fullName => legalChineseName(fullName)[0],
    'GN': fullName => legalChineseName(fullName)[1],
    'LC': input => input.charAt(input.length - 1)
};

const expressionCallMatch = /^([A-Z]{2})(\(|（)(.*)(\)|）)$/;

// Can be used on either raw form or escaped form.
export const parseRefExpression = (expr: string): [string] | [string, FunctionName] => {
    const matchCall = expr.match(expressionCallMatch);
    if (matchCall) {
        if (!expressionRuntime.hasOwnProperty(matchCall[1])) {
            throw new RangeError(`「${matchCall[1]}」不是合法的函数名`);
        }
        return [matchCall[3], matchCall[1] as FunctionName];
    }
    return [expr];
}

// If must be a DNF expression, using `=` as separator, `&&` as and, `||` as or.
// e.g. a=1&&c=2||a=2&&c=1
// Return a list of disjuncts, each containing a list of conjuncts, where each conjunct is a [key, value, reverse], where reverse is a boolean, false when ==, true when !=.
// Can be used on either raw form or escaped form.
export const parseIfExpression = (expr: string): [string, string, boolean][][] => {
    if (!expr) { throw new Error(`表达式为空`); }
    return expr.split(/\|\|?/).map(disjunct => disjunct.split(/&&?/).map(item => {
        const match = item.match(/!?==?/);
        if (!match) {
            throw new Error(`表达式「${item}」不是 key=value 的形式`);
        }
        const keyPart = item.substring(0, match.index);
        const valuePart = item.substring(match.index! + match[0].length);
        return [keyPart, valuePart, match[0].charAt(0) === '!'];
    }));
}