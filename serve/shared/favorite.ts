// Note those two dummy ones are in the DB as well for convenience.
// LINT.IfChange
export const MY_THREADS_DUMMY_COLLECTION = { fc_id: 0, default_star_type: 0x10 | 0x6, title: '我发布的主题' };
export const BLOCKED_THREADS_DUMMY_COLLECTION = { fc_id: -1, default_star_type: 0x10 | 0x8, title: '我屏蔽的主题' };
// LINT.ThenChange(../frontend/fav_in_thread.ts, ../frontend/fav_in_list.ts)