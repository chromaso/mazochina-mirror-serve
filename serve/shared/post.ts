import { LinksParsedFromBBCode, parseBBCode } from '../../common/bbcode';
import { escapeHtml } from '../../common/dependencies';
import { query } from '../db';
import { attachmentNameToId } from './attachment';

const parseDate = (date: string) => Math.floor(Date.parse(date.trim()) / 1000);

export const processContent = (content: string, dateFormatter?: (dateInSeconds: number) => SafeString) => {
    return content.replace(/<img class="smilies" src=".\/images\/smilies\/icon_/g, '<img class="smilies" src="/static/smilies/icon_').replace(/<blockquote>/g, '<blockquote class="card p-3 d-block">').replace(/<cite><a href="\.\/memberlist\.php\?mode=viewprofile&amp;u=(\d+)">([^<>]+)<\/a> wrote: <a href="\.\/viewtopic\.php\?p=\d+#p\d+" data-post-id="(\d+)" onclick="if\(document\.getElementById\(hash\.substr\(1\)\)\)href=hash">↑<\/a><div class="responsive-hide">([^<>]+)<\/div><\/cite>/g, (str, userId, userName, postId, date) => '<cite><a class="text-reset" href="/author/' + userId + '">' + userName + '</a>：<a href="/post/' + postId + '">↑</a><span class="text-muted float-end d-none d-sm-inline">' + (dateFormatter ? dateFormatter(parseDate(date)) : date) + '</span></cite>');
};

export const processContentSc = (content: string, dateFormatter?: (dateInSeconds: number) => SafeString): string => {
    const separatorIndex = content.indexOf('\ufffc'); // Non-ancient remote, or any local.
    return (separatorIndex >= 0) ? content.substr(separatorIndex + 1) : processContent(content, dateFormatter);
};

export const processContentScForQuote = (content: string, opt_bbcode?: string): string => {
    let quotableText = opt_bbcode;
    if (!quotableText) {
        const separatorIndex = content.indexOf('\ufffc');
        quotableText = (separatorIndex >= 0) ? content.substr(separatorIndex + 1) : escapeHtml(content);
    }
    return quotableText.trim();
};

interface LinksParsedFromBBCodeWireFormat {
    // Each of the fields should never be an empty array - either it's non-empty or the field doesn't exist.
    a?: number[]; // Attachment IDs.
    t?: string[]; // To hashes.
    r?: number[]; // Replying to post IDs.
}

export const xrefsToWireFormat = ({ links, quotes }: ReturnType<typeof parseBBCode>): LinksParsedFromBBCodeWireFormat => {
    const retVal: LinksParsedFromBBCodeWireFormat = {};
    if (links.attachments.length) {
        retVal.a = links.attachments.map(attachmentNameToId);
    }
    if (links.toLinks.size) {
        retVal.t = Array.from(links.toLinks);
    }
    if (quotes.length) {
        retVal.r = Array.from(new Set<number>(quotes.map(quote => quote.postId).filter((_): _ is number => !!_)));
    }
    return retVal;
};

export const validateLinkTargetHeadings = async (parsedLinks: LinksParsedFromBBCode, threadId: number, postId: number): Promise<void> => {
    if (!parsedLinks.toLinks.size) { return; }
    for (const { post_id, link_id } of (await query<{ post_id: number, link_id: string }>('SELECT post_id, JSONB_ARRAY_ELEMENTS(xrefs -> \'t\') AS link_id FROM posts WHERE thread_id = $1 AND most_recent_update >= 0 AND post_id <> $2', [threadId, postId])).rows) {
        if (parsedLinks.toLinks.has(link_id)) {
            throw new Error(`链接跳转目标名称 ${link_id} 已被同一主题内另一篇文章（${post_id}）占用`);
        }
    }
};