import * as elements from 'typed-xhtml';
import { HtmlTmpl, styled } from '../utils/tsx_utils';
import { formatVisitorId } from '../shared/login';
import { adminLogTable } from './moderation';
import { pagination, PaginationParams } from './pagination';

// Migrated from thread.pug.
export const thread: HtmlTmpl<{
    threadId: number,
    title: string,
    parent: { link: string, name: string },
    official: boolean,
    remoteForumName: string | undefined,
    tags: { id: number, name: string, full: boolean, warn: boolean }[] | undefined,
    warnings: string[],
    originalThread: { id: number, title: string } | undefined,
    authorOnly: string | null,
    pagination: PaginationParams,
    contestResultHtml: SafeString | null,
    isMyThread: boolean,
    readToDate: number,
    iHaveReplied: boolean,
    starStatus: number,
    iastSelections: Record<string, unknown> | undefined,
    showReplyWarning: boolean,
    isSpam: boolean,
    canAdminThread: boolean,
    campaign: boolean,
    posts: ({
        id: number,
        date: number,
        edited: number | null,
        authorId: number,
        authorName: string,
        authorLabel: string | null,
        authorAvatar: SafeString,
        title: string,
        content: string,
        isHidden: boolean,
        replyLink: string,
        authorOnlyLink: string,
        isLocal: boolean,
        originalLink: string | null // Null if isLocal.
    })[],
    isLocal: boolean,
    poll: string | null, // Null if isLocal.
    originalLink: string, // Meaningless unless isLocal.
    originalReplyLink: string // Meaningless unless isLocal.
}> = (ctx, self) => {
    const { render, mazoLoginUser } = ctx.state;
    return styled(`
        blockquote:has(> cite) { max-height: 160px; overflow: hidden; position: relative; opacity: 0.75; }
        blockquote > cite { display: block; }
        .card-body img { max-width: 100%; }
        .post-date-wrap { cursor: pointer; }
        .post-date-wrap span + span { display: none; }
        .post-date-wrap span.d-none + span { display: inline; }
        .quick-host .btn-text { display: none; }
        .quick-host:hover .btn-text { display: inline; }
        body.no-side-buttons #rep-quick, body.no-side-buttons #fav-quick { display: none; }
        #rep-quick, #fav-quick { border-top-right-radius: 0; border-bottom-right-radius: 0; }
        #rep-quick:not(:hover) { border-bottom-left-radius: 0; }
        #fav-quick:not(:hover) { border-top-left-radius: 0; }
        .mm-post { scroll-margin-top: 72px; }
        .mm-tag-fcbox { border: 0 none; padding: 0; background: none; }
        #ic-suggestion-card:not(.d-none) + div { display: none; }`,
        <div class="container">
            <nav>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="/">首页</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href={self.parent.link}>{render.staticConv(self.parent.name)}</a>
                    </li>
                    <li class="breadcrumb-item active flex-grow-1 d-inline-flex">
                        {render.contentConv(self.title)}
                        {(self.isLocal || self.remoteForumName) ? (
                            <span
                                data-title={
                                    self.remoteForumName
                                        ? `此主题在源站位于「${render.staticConv(self.remoteForumName)}」板块，仅在镜像站被移动至此板块`
                                        : '此主题从镜像发布，仅存在于镜像站，不存在于源站。'
                                }
                                class="ms-auto mazomirror-tip align-self-end"
                            >
                                <span class="material-icons std-icon">error</span>
                            </span>
                        ) : null}
                    </li>
                </ol>
            </nav>
            <h1 id="thread-title" class={self.tags ? 'mt-4 mb-3' : 'my-4'}>
                {render.contentConv(self.title)}
            </h1>
            {self.tags ? (
                <h4 id="tag-container" class="mb-4">
                    {self.tags.map(tag => (
                        // LINT.IfChange(tag_badges)
                        <span class="position-relative d-inline-block">
                            <a href={`/tag/${tag.id}`} class={"mm-tag-anchor badge rounded-pill bg-secondary me-2" + (tag.full ? '' : ' opacity-50')}>
                                {render.staticConv(tag.name)}
                                {tag.warn ? (
                                    <span class="material-icons align-middle" style="font-size: 85%">
                                        report_problem
                                    </span>
                                ) : null}
                            </a>
                        </span>
                        // LINT.ThenChange(../frontend/metro/tags_in_threads.ts:tag_badges)
                    ))}
                    {/* The href will be overridden by JS if the user has logged in */}
                    <a
                        id="tag-searcher-trigger"
                        href="javascript:window.alert('你需要登入后才可以执行此操作！')"
                        class="badge rounded-pill bg-secondary opacity-75"
                    >
                        <span class="material-icons align-middle" style="font-size: 85%">
                            add
                        </span>
                        {!self.tags.length ? '添加标签' : null}
                    </a>
                    {mazoLoginUser ? (
                        <>
                            <div id="tag-searcher-parent" class="position-relative d-none">
                                <input
                                    type="text"
                                    placeholder="搜索标签……"
                                    data-toggle="dropdown"
                                    class="form-control form-control-sm w-auto"
                                />
                                <div class="dropdown-menu">
                                    <a href="javascript:void(0)" class="dropdown-item disabled">
                                        加载中…
                                    </a>
                                </div>
                            </div>
                            <a id="tag-searcher-pop" href="javascript:void(0)" class="badge rounded-pill bg-secondary opacity-25 ms-2">
                                <span class="material-icons align-middle" style="font-size: 85%">
                                    open_in_new
                                </span>
                            </a>
                        </>
                    ) : null}
                </h4>
            ) : null}
            {self.warnings.map(warning => (
                <div class="alert alert-warning">{render.staticConv(warning)}</div>
            ))}
            {self.official ? (
                <div class="alert alert-info">
                    <span class="material-icons std-icon">verified</span> 这是本站（M系镜像）的规则/指引的一部分。
                </div>
            ) : null}
            {self.poll ? (
                <div class="alert alert-info">
                    该主题正在进行投票：<b>{render.contentConv(self.poll)}</b>，若须参与投票，请<a href={self.originalLink} class="alert-link">跳转回源站</a> 。
                </div>
            ) : null}
            {self.originalThread ? (
                <div class="alert alert-info">
                    本主题由 <a href={'/thread/' + self.originalThread.id}>{render.contentConv(self.originalThread.title)}</a> 拆分而来。
                </div>
            ) : null}
            {self.authorOnly ? (
                <div class="alert alert-primary">
                    只显示<b>{self.authorOnly}</b>的文章。
                    <a href={self.pagination.baseLink} class="alert-link float-end">
                        显示全部
                    </a>
                </div>
            ) : null}
            {self.contestResultHtml}
            {self.posts.map(post => (
                <div
                    id={'p' + post.id}
                    data-date={post.date.toString(36)}
                    class="card my-4 mm-post"
                >
                    <div class="card-header d-flex justify-content-between">
                        <a class="mm-post-ava flex-shrink-0 me-2 align-self-start" href={'/author/' + post.authorId}>{post.authorAvatar}</a>
                        <div class="flex-shrink-1 flex-grow-1 ms-1 align-self-start">
                            <a href={'/author/' + post.authorId} class="ui-link">{post.authorName}</a>
                            {post.authorLabel ? (
                                <span class="badge bg-secondary user-select-none ms-1">
                                    {render.staticConv(post.authorLabel)}
                                </span>
                            ) : null}
                            <div class="text-muted">
                                {render.contentConv(post.title)}
                            </div>
                        </div>
                        {post.isLocal ? (
                            <span class="badge bg-secondary ms-1 mazomirror-tip align-self-center" data-title="此帖从镜像发布，仅存在于镜像站，不存在于源站。">
                                仅镜像
                            </span>
                        ) : null}
                    </div>
                    <div class="card-body">
                        {post.isHidden ? (
                            self.iHaveReplied ? (
                                <div class="alert alert-info">
                                    下列内容回复可见。您已回复。
                                </div>
                            ) : (mazoLoginUser?.admin) ? (
                                <div class="alert alert-info">
                                    下列内容回复可见。您是管理员因此可以直接查看。
                                </div>
                            ) : (
                                <div class="alert alert-info mb-0">
                                    本文章的内容回复此主题可见。您尚未回复。
                                </div>
                            )
                        ) : null}
                        {(self.iHaveReplied || !post.isHidden || (mazoLoginUser?.admin))
                            ? elements.safeHtml(render.contentConv(post.content))
                            : null}
                    </div>
                    <div class="card-footer d-flex justify-content-between align-items-center">
                        <span class="text-muted post-date-wrap">
                            <span class="d-none">
                                发布于 {render.renderDate(post.date)}
                                {post.edited ? (
                                    <>
                                        ，编辑于 {render.renderDate(post.edited)}
                                    </>
                                ) : null}
                            </span>
                            {post.edited ? (
                                <span class="fst-italic">
                                    {render.renderDate(post.edited, true)[0]}
                                </span>
                            ) : (
                                <span>{render.renderDate(post.date, true)[0]}</span>
                            )}
                        </span>
                        <span class="flex-shrink-0 text-end">
                            {(mazoLoginUser?.id === post.authorId) ? (
                                <form
                                    action={'/edit/' + post.id}
                                    method="post"
                                    class="d-inline"
                                >
                                    <button
                                        type="submit"
                                        class="btn btn-outline-primary btn-sm"
                                    >
                                        <span class="material-icons me-1">edit</span>编辑
                                    </button>
                                </form>
                            ) : (
                                <form
                                    action={post.replyLink}
                                    method="post"
                                    class="d-inline"
                                >
                                    <button type="submit" class="btn btn-secondary btn-sm">
                                        <span class="material-icons me-1">format_quote</span>
                                        引用
                                    </button>
                                </form>
                            )}
                            <span class="position-relative d-inline-block">
                                <button
                                    data-toggle="dropdown"
                                    class="btn btn-secondary btn-sm ms-2"
                                >
                                    <span class="material-icons">more_horiz</span>
                                </button>
                                <div class="dropdown-menu">
                                    {!self.authorOnly ? (
                                        <a href={post.authorOnlyLink} class="dropdown-item">
                                            只看该作者
                                        </a>
                                    ) : null}
                                    {(mazoLoginUser?.admin && post.isLocal) ? (
                                        <a
                                            href={
                                                'javascript:mazomirrorAdmin("post", ' + post.id + ')'
                                            }
                                            class="dropdown-item"
                                        >
                                            以管理员身份删除
                                        </a>
                                    ) : null}
                                    {!post.isLocal ? (
                                        <a href={post.originalLink!} class="dropdown-item">
                                            回源站查看
                                        </a>
                                    ) : null}
                                </div>
                            </span>
                        </span>
                    </div>
                </div>
            ))}
            <div class="quick-host position-fixed d-none d-lg-block end-0 bottom-50">
                <button id="rep-quick" class="btn btn-primary">
                    <span class="material-icons">post_add</span>
                    <span class="btn-text ms-1">回复</span>
                </button>
            </div>
            <div class="quick-host position-fixed d-none d-lg-block end-0 top-50">
                <button
                    id="fav-quick"
                    data-toggle="dropdown"
                    class="btn btn-secondary"
                >
                    <span class="material-icons">
                        {mazoLoginUser ? 'pending' : 'favorite_border'}
                    </span>
                    <span class="btn-text ms-1">收藏/订阅</span>
                </button>
                <div class="dropdown-menu text-center">
                    {!mazoLoginUser ? (
                        <div class="px-2">登入用户可以收藏或订阅主题～</div>
                    ) : null}
                </div>
            </div>
            {pagination(ctx, self.pagination)}
            {self.campaign ? (
                <div id="campaign-vote-card-placeholder" class="mb-4">
                    <div class="card">
                        <div class="card-header">征文比赛投票</div>
                        <div class="card-body">加载中…</div>
                    </div>
                </div>
            ) : null}
            <div class="text-center mb-4">
                <form
                    action={'/post/t' + self.threadId}
                    method="post"
                    class="d-inline"
                >
                    <button
                        id="rep-button"
                        type="submit"
                        class="btn btn-primary mb-2"
                    >
                        <span class="material-icons me-1">post_add</span>回复
                    </button>
                </form>
                <span class="position-relative">
                    <button
                        id="fav-button"
                        data-toggle="dropdown"
                        class="btn btn-secondary ms-2 mb-2"
                    >
                        <span class="material-icons me-1">
                            {mazoLoginUser ? 'pending' : 'favorite_border'}
                        </span>
                        收藏/订阅
                    </button>
                    <div class="dropdown-menu text-center">
                        {mazoLoginUser ? (
                            <div class="px-2">
                                <div class="spinner-border" />
                                <div class="mt-1">加载中</div>
                            </div>
                        ) : (
                            <div class="px-2">登入用户可以收藏或订阅主题～</div>
                        )}
                    </div>
                </span>
                {!self.isLocal ? (
                    <span class="position-relative">
                        <button
                            data-toggle="dropdown"
                            class="btn btn-secondary ms-2 mb-2"
                        >
                            源站
                        </button>
                        <div class="dropdown-menu">
                            <a href={self.originalLink} class="dropdown-item">
                                回源站查看
                            </a>
                            <a href={self.originalReplyLink} class="dropdown-item">
                                在源站回复
                            </a>
                            {mazoLoginUser ? (
                                <a
                                    href="javascript:invalidate(window.alert)"
                                    class="dropdown-item"
                                >
                                    从源站同步
                                </a>
                            ) : null}
                        </div>
                    </span>
                ) : null}
                {mazoLoginUser ? (
                    <span class="position-relative d-inline-block">
                        <button
                            data-toggle="dropdown"
                            class="btn btn-secondary ms-2 mb-2"
                        >
                            <span class="material-icons">more_horiz</span>
                        </button>
                        <div class="dropdown-menu">
                            <button class="dropdown-item" id="block-thread-btn">
                                加载中…
                            </button>
                            <a href="javascript:openThreadStats()" class="dropdown-item">
                                记录与统计
                            </a>
                            {self.canAdminThread ? (
                                <a
                                    href={`javascript:initThreadAdmin(${self.isSpam}, ${self.isLocal && mazoLoginUser!.admin})`}
                                    class="dropdown-item"
                                >
                                    管理操作
                                </a>
                            ) : null}
                        </div>
                    </span>
                ) : null}
            </div>
            {mazoLoginUser ? (
                <>
                    {self.showReplyWarning ? (
                        <div class="alert alert-warning mb-4">
                            本主题没有任何回复可见的内容。发布无意义回复将会导致你被永久封禁。
                        </div>
                    ) : null}
                    <form
                        action={'/post/t' + self.threadId}
                        method="post"
                        class="text-center mb-5"
                    >
                        <div class="input-group">
                            <textarea
                                id="main-textarea"
                                name="content"
                                placeholder="快速回复…"
                                rows="2"
                                style='max-height: 120px; min-height: 40px;'
                                class="form-control"
                            />
                            <input
                                id="qrp-form-btn"
                                type="submit"
                                name="submitpost"
                                value="发布"
                                class="btn btn-secondary"
                            />
                        </div>
                    </form>
                </>
            ) : null}
            <script type="text/javascript">{elements.safeHtml(`threadPageInit();`)}</script>
            <script type="text/javascript">{self.iastSelections ? elements.safeHtml( `iastInitilize(${JSON.stringify(self.iastSelections)}, ${self.isMyThread});`) : '/*NO_IAST*iastInitilize()*/'}</script>
            <script type="text/javascript">{elements.safeHtml(`markThread(${self.readToDate}, ${self.posts[0].date});`)}</script>
            <script type="text/javascript">{mazoLoginUser ? elements.safeHtml(`initThreadFav(${self.starStatus}, ${self.isMyThread}, ${self.isLocal});`) : '/*NOT_LOGGED_IN*initThreadFav()*/'}</script>
            <script type="text/javascript">{(mazoLoginUser && self.tags) ? elements.safeHtml(`initThreadTags();`) : '/*NO_TAGS*initThreadTags()*/'}</script>
            <script type="text/javascript">{self.campaign ? elements.safeHtml(`initThreadCampaign();`) : '/*NO_CAMPAIGN*initThreadCampaign()*/'}</script>
        </div>
    );
}

// Migrated from thread_stats.pug.
export const threadStats: HtmlTmpl<{
    readerStats: {
        weekly: Readonly<[number, number]>,
        monthly: Readonly<[number, number]>,
        yearly: Readonly<[number, number]>,
        limitedPermission: boolean,
        recentReads: { user_id: number, user_name: string }[] | undefined,
        favs: number,
        publicFavs: { fc_id: number, title: string, user_name: string }[]
    },
    adminStats: Parameters<typeof adminLogTable>[1] | null
}> = (ctx, { readerStats: self, adminStats }) => (
    <>
        <div id="thread-reader-logs">
            <h4>阅读</h4>
            <table class="table table-sm">
                <thead>
                    <tr>
                        <th />
                        <th scope="col">注册用户</th>
                        <th scope="col">未登入访客</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">最近一周</th>
                        <td>{self.weekly[0]}</td>
                        <td>{self.weekly[1]}</td>
                    </tr>
                    <tr>
                        <th scope="row">最近一月</th>
                        <td>{self.monthly[0]}</td>
                        <td>
                            {self.limitedPermission ? (
                                <div class="text-muted">[REDACTED]</div>
                            ) : (
                                self.monthly[1]
                            )}
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">最近一年</th>
                        <td>
                            {self.limitedPermission ? (
                                <div class="text-muted">[REDACTED]</div>
                            ) : (
                                self.yearly[0]
                            )}
                        </td>
                        <td>
                            {self.limitedPermission ? (
                                <div class="text-muted">[REDACTED]</div>
                            ) : (
                                self.yearly[1]
                            )}
                        </td>
                    </tr>
                </tbody>
            </table>
            <p class="mb-0">最近十个读者</p>
            {self.limitedPermission ? (
                <div class="text-muted">[REDACTED]</div>
            ) : (
                <div>
                    {self.recentReads?.map((reader, index) => 
                        <>
                            {(index === 0) ? null : ' '}
                            {(reader.user_id < 0) ? (
                                <span class="badge bg-secondary opacity-75">
                                    {formatVisitorId(reader.user_id)}
                                </span>
                            ) : (
                                <a class="badge bg-secondary" href={'/author/' + reader.user_id}>
                                    {reader.user_name}
                                </a>
                            )}
                        </>
                    )}
                </div>
            )}
            <h4 class="mt-3">收藏</h4>
            <p> 
                {'此主题共被 '}
                {self.limitedPermission ? (
                    <span class="text-muted">[REDACTED]</span>
                ) : (
                    self.favs
                )}
                {' 人收藏'}
                {self.publicFavs.length ? (<>，以下为包含此主题的所有公开收藏夹（{self.publicFavs.length}）：</>) : null}
            </p>
            {self.publicFavs.length ? (
                <div class="list-group">
                    {self.publicFavs.map(fav => (
                        <a class="list-group-item list-group-item-action" href={'/collection/' + fav.fc_id}>
                            {fav.title}
                            <div class="small">由 {fav.user_name} 创建</div>
                        </a>
                    ))}
                </div>
            ) : null}
        </div>
        <div id="thread-admin-logs">
            {adminStats ? (
                adminLogTable(ctx, adminStats)
            ) : (
                <div class="alert alert-info">此主题从未被管理员操作过</div>
            )}
        </div>
    </>
);