import { Types } from '../types';

export const $set: Types['$set'] = (parent, children) => {
    while (parent.lastChild) { parent.removeChild(parent.lastChild); }
    const single = (child: HTMLElement | string) => parent.appendChild((child instanceof HTMLElement) ? child : document.createTextNode(child));
    if (children instanceof Array) {
        children.forEach(single);
    } else {
        single(children);
    }
};

export const $: Types['$'] = (tagName, props, children, eventListeners) => {
    const el = document.createElement(tagName);
    if (props) {
        if (typeof props === 'string') {
            el.className = props;
        } else {
            Object.keys(props).forEach(k => (el as any)[k] = (props as any)[k]);
        }
    }
    if (children) {
        $set(el, children);
    }
    if (eventListeners) {
        if (typeof eventListeners === 'function') {
            el.addEventListener('click', eventListeners as any);
        } else {
            Object.keys(eventListeners).forEach(k => el.addEventListener(k, (eventListeners as any)[k]));
        }
    }
    return el;
};


const XHR_CACHE: { [url: string]: string | (Parameters<typeof $xhr>[1])[] } = {};

const serveResult = (resultText: string, callback: Parameters<typeof $xhr>[1]) => {
    if (callback.t) {
        (window.queueMicrotask || setTimeout)(() => callback.t(resultText));
        return;
    }
    let parsed;
    try {
        parsed = JSON.parse(resultText);
    } catch (e) {
        callback.e('无法解析服务器的回应');
        return;
    }
    (window.queueMicrotask || setTimeout)(() => callback.j(parsed)); // Prevent errors or delays in a callback from inferferring w/ other callbacks.
};

export const $xhr: Types['$xhr'] = (path, params) => {
    const cache = !!params.c;

    if (cache) {
        const cached = XHR_CACHE[path];
        if (cached instanceof Array) { // Request in flight.
            cached.push(params);
            return;
        }
        if (cached) { // Cached version available.
            serveResult(cached, params);
            return;
        }
        XHR_CACHE[path] = [params];
        // Fallthrough to send request.
    }

    const handleFail = (error: string) => {
        if (cache) {
            const cached = XHR_CACHE[path];
            if (cached instanceof Array) {
                cached.forEach(single => single.e(error));
                delete XHR_CACHE[path];
            } else {
                console.error('[WTF] No cb!');
            }
        } else {
            params.e(error);
        }
    };

    const xhr = new XMLHttpRequest();
    const post = (params.f !== undefined);
    xhr.open(post ? 'POST' : 'GET', path);
    if (typeof params.f === 'string') {
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    }
    xhr.onload = () => {
        if (xhr.status !== 200) {
            handleFail(`HTTP ${xhr.status}：` + xhr.responseText);
        } else {
            if (cache) {
                const cached = XHR_CACHE[path];
                if (cached instanceof Array) {
                    XHR_CACHE[path] = xhr.responseText;
                    cached.forEach(single => serveResult(xhr.responseText, single));
                } else {
                    console.error('[WTF] No cb!');
                }
            } else {
                serveResult(xhr.responseText, params);
            }
        }
    };
    xhr.onerror = () => handleFail('网络链接失败');
    xhr.ontimeout = () => handleFail('网络超时');
    xhr.send(params.f || null);
};

export const updateChildAt = (parent: Element, index: number, newChild?: Node): Node | undefined => {
    const oldChild = parent.childNodes[index];
    if (oldChild) {
        if (newChild) {
            parent.replaceChild(newChild, oldChild);
        } else {
            parent.removeChild(oldChild);
        }
    } else if (newChild) {
        parent.appendChild(newChild);
    }
    return oldChild;
};

export const sideEffect = () => {
    HTMLElement.prototype.also = function (this, block) {
        block(this!);
        return this!;
    }    
};