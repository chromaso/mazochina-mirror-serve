import sharp = require("sharp");

// Input must be a square image.
export const imageToSignatureBuffer = async (imageFile: sharp.Sharp): Promise<Buffer> => {
    const buffer = await imageFile.clone().removeAlpha().resize(12, null).raw().toBuffer();
    if (buffer.length !== 432) { throw new Error(`Unexpected buffer size ${buffer.length}`); }
    const downsized = Buffer.allocUnsafe(165);
    let dIndex = -1;
    let bitBuffer = 0;
    let bitBufferLen = 0;
    const write = (value: number, bitLength: number) => {
        bitBuffer <<= bitLength;
        bitBuffer |= value;
        bitBufferLen += bitLength;
    };
    const flush = () => {
        while (bitBufferLen >= 8) {
            bitBufferLen -= 8;
            downsized[++dIndex] = bitBuffer >> bitBufferLen;
            bitBuffer &= (1 << bitBufferLen) - 1;
        }
    };
    for (let i = 0; i < 432; ) {
        const pixel1 = ((buffer[i] >> 5) << 6) | ((buffer[i + 1] >> 5) << 3) | ((buffer[i + 2] >> 5)); // 9 bits RGB.
        i += 3;
        write(pixel1, 9);
        const pixel2 = ((buffer[i] >> 5) << 5) | ((buffer[i + 1] >> 5) << 2) | ((buffer[i + 2] >> 6)); // 8 bits RGB.
        i += 3;
        write(pixel2, 8);
        flush();
        const pixel3 = ((buffer[i] >> 5) << 6) | ((buffer[i + 1] >> 5) << 3) | ((buffer[i + 2] >> 5)); // 9 bits RGB.
        i += 3;
        write(pixel3, 9);
        flush();
    }
    if ((bitBufferLen !== 0) || (bitBuffer !== 0)) { throw new Error('Bit buffer not empty.'); }
    if (dIndex !== 155) { throw new Error('Bit buffer not empty.'); }
    const alphaBuffer = await imageFile.clone().ensureAlpha().extractChannel('alpha').resize(6, null).raw().toBuffer();
    if (alphaBuffer.length !== 36) { throw new Error(`Unexpected buffer size ${buffer.length}`); }
    for (let i = 0; i < 36; i += 4) {
        downsized[++dIndex] = ((alphaBuffer[i] >> 6) << 6) + ((alphaBuffer[i + 1] >> 6) << 4) + ((alphaBuffer[i + 2] >> 6) << 2) + (alphaBuffer[i + 3] >> 6);
    }
    if (dIndex + 1 !== downsized.length) { throw new Error(`Unexpected downsample index ${dIndex}`); }
    return downsized;
};

// "Inverse" function of the one above, constructing a thumbnail from a signature. This is a slow operation as it was never intended to be run online.
export const signatureToImage = async (signatureBuffer: Buffer): Promise<sharp.Sharp> => {
    if (signatureBuffer.length !== 165) { throw new Error('Wrong size?'); }
    const rgbBuffer = Buffer.allocUnsafe(432);
    let oIndex = -1;
    for (let i = 0; i < 156; i += 13) {
        let rowBigInt = (signatureBuffer.readBigUInt64BE(i) << BigInt(40)) | BigInt(signatureBuffer.readUIntBE(i + 8, 5)); 
        // if (!i) { console.log(signatureBuffer.readBigUInt64BE(i), signatureBuffer.readUIntBE(i + 8, 5), rowBigInt); }
        const segments: number[] = [];
        for (let j = 3; j >= 0; j--) {
            segments[j] = Number(rowBigInt & BigInt(0x3ffffff));
            rowBigInt >>= BigInt(26);
        }
        if (rowBigInt) { throw new Error('Remaining part?'); }
        for (let j = 0; j < 4; j++) {
            const pixel1 = segments[j] >> 17;
            const pixel2 = (segments[j] >> 9) & 0xFF;
            const pixel3 = segments[j] & 0x1FF;
            // if ((!i) && (!j)) { console.log(`DP1 ${pixel1} ${pixel2} ${pixel3}`); }
    
            rgbBuffer[++oIndex] = (((pixel1 & 0x1C0) >> 6) << 5) | (1 << 4);
            rgbBuffer[++oIndex] = (((pixel1 & 0x38) >> 3) << 5) | (1 << 4);
            rgbBuffer[++oIndex] = ((pixel1 & 0x7) << 5) | (1 << 4);
    
            rgbBuffer[++oIndex] = (((pixel2 & 0xE0) >> 5) << 5) | (1 << 4);
            rgbBuffer[++oIndex] = (((pixel2 & 0x1C) >> 2) << 5) | (1 << 4);
            rgbBuffer[++oIndex] = ((pixel2 & 0x3) << 6) | (1 << 5);

            rgbBuffer[++oIndex] = (((pixel3 & 0x1C0) >> 6) << 5) | (1 << 4);
            rgbBuffer[++oIndex] = (((pixel3 & 0x38) >> 3) << 5) | (1 << 4);
            rgbBuffer[++oIndex] = ((pixel3 & 0x7) << 5) | (1 << 4);
        }
    }
    if (oIndex + 1 !== rgbBuffer.length) { throw new Error(`Unexpected rgb index ${oIndex}`); }

    const alphaBuffer = Buffer.alloc(36);
    oIndex = -1;
    for (let i = 156; i < 165; i++) {
        const value = signatureBuffer[i];
        alphaBuffer[++oIndex] = (value & 0xC0) | (1 << 5);
        alphaBuffer[++oIndex] = ((value & 0x30) << 2) | (1 << 5);
        alphaBuffer[++oIndex] = ((value & 0xC) << 4) | (1 << 5);
        alphaBuffer[++oIndex] = ((value & 0x3) << 6) | (1 << 5);
    }
    if (oIndex + 1 !== alphaBuffer.length) { throw new Error(`Unexpected rgb index ${oIndex}`); }

    const scaledAlphaBuffer = await sharp(alphaBuffer, { raw: { channels: 1, width: 6, height: 6 } }).resize(12, 12, { kernel: 'nearest' }).toBuffer({ resolveWithObject: true });

    return sharp(rgbBuffer, { raw: { width: 12, height: 12, channels: 3 } }).joinChannel(scaledAlphaBuffer.data, { raw: scaledAlphaBuffer.info });
};
