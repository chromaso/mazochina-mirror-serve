import { SPAM_THRESHOLD } from './consts';

export const rowsToMap = <Row, Key extends keyof Row>(rows: Readonly<Iterable<Row>>, key: Key): Map<Row[Key], Row> => {
    const retVal = new Map<Row[Key], Row>();
    for (const row of rows) {
        retVal.set(row[key], row);
    }
    return retVal;
};

export const rowsToMultiMap = <Row, Key extends keyof Row>(rows: Readonly<Iterable<Row>>, key: Key): Map<Row[Key], Row[]> => {
    const retVal = new Map<Row[Key], Row[]>();
    for (const row of rows) {
        const group = row[key];
        const existing = retVal.get(group);
        if (existing) { existing.push(row); } else { retVal.set(group, [row]); }
    }
    return retVal;
};

export const unawaited = (promise: Promise<unknown>, label: string): void => {
    promise.catch(e => console.error(`Failing async ${label}`, e));
};

// Returns false for a normal thread, where sortpost_date would always equal lastpost_date.
// Returns true for a "special" thread: a thread with warning banner, a hidden thread, or a thread with rep2see enabled.
export const threadNeedsSpecialSortDateCalc = async (thread: { thread_id: number; flags: number; spam_likeness: number; }, txn: ((sqlText: string, params: any[]) => Promise<{ rows: any[] }>)): Promise<boolean> => {
    if ((thread.spam_likeness >= SPAM_THRESHOLD) || (thread.flags & 2)) { return true; }
    // Need to check is rep2see is enabled.
    const postFlags = (await txn('SELECT BIT_OR(flags) AS all_flags FROM posts WHERE thread_id = $1 AND posts.most_recent_update >= 0', [thread.thread_id])).rows[0];
    if (!postFlags) { console.log('Should never happen: no posts under thread ' + thread.thread_id); return true; }
    return !!(postFlags.all_flags & 2);
};
