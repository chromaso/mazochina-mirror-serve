
import { parseBBCode } from '../../common/bbcode';
import { LENGTH_THRESHOLD_FOR_UPDATING_SORTPOST_DATE, THRESHOLD_FOR_LOCAL } from '../../common/consts';
import { threadNeedsSpecialSortDateCalc, unawaited } from '../../common/logic_utils';
import { query as sql, withTransaction } from '../db';
import { postHtmlToBBCode } from '../../common/html2bbcode2';
import * as attachments from '../shared/attachment';
import { RATE_LIMIT_CONSTS, rateLimitGroup, rateLimitTest } from '../shared/author';
import { checkIastItemsValidity, escapeForIast } from '../shared/iast';
import * as login from '../shared/login';
import { allTagsByCategory, allTagsById, F18_CONSTS, voteForThread } from '../shared/tags';
import { updateThreadTargetsRow } from '../shared/thread';
import { createCaptchaInfo, validateCaptcha } from '../utils/captcha';
import { myFirstPostInThread, normalize, parseInt32, sendReplyNotifications, updateForumTimestamp } from '../utils/logic_utils';
import { forumsCache } from '../shared/forum';
import { checkQuoteItemValidity, getReplyToBBCode } from '../utils/quote_reply';
import { RateLimiter } from '../utils/rate_limit';
import { error, finalizePage, LOGIN_REQUIRED, PageImpl, redirectWithDelay } from '../utils/render_utils';
import * as type4Notification from '../utils/type_4_notifications';
import { originalRemoteUrl, originalReplyUrl } from '../utils/src_post';

import * as templates from '../templates/post';
import { MY_THREADS_DUMMY_COLLECTION } from '../shared/favorite';
import { xrefsToWireFormat, validateLinkTargetHeadings } from '../shared/post';

type RateLimitDecision = 'PASS' | 'REJECT' | 'CAPTCHA';

// For enforcing the 2-second windowed rate limit.
const DEDUP_RATE_LIMITER = new RateLimiter<number>(1, 2);

// For enforcing the 24-hour windowed rate limit.
// Returns false: okay to process directly. Returns true: CAPTCHA required. Throws when hard limit exceeded.
const enforcePostRateLimit = async (user: login.UserInfo, ask: 1 | 5): Promise<RateLimitDecision> => {
    const date = Math.floor(Date.now() / 1000);
    const tested = await rateLimitTest(date, user.id);
    const myLimits = RATE_LIMIT_CONSTS[rateLimitGroup(date, user)];
    const count = tested.posts + tested.threads * 4 + ask;
    if (count > myLimits[1]) { return 'REJECT'; }
    return (count <= myLimits[0]) ? 'PASS' : 'CAPTCHA';
};

const isJunkPost = (text: string) => {
    if (text.length > 20) { return false; }
    const actual = text.replace(/[,，.。?？!！]/g, '');
    return ((/^\d*$/).test(actual) || (/^(试)*(回复)+(可见)?(看图)?\d*/).test(actual));
};

const hasPassword = async (userId: number): Promise<boolean> => (await sql('SELECT password_h IS NOT NULL AS has_password FROM users WHERE user_id = $1', [userId])).rows[0]?.has_password ?? true;

// Reply without a blockquote will always be considered an "update from op". Reply with a blockquote will be considered an "update from op" if the proper length is higher than this.
const LENGTH_THRESHOLD_FOR_FORCE_NOTIFICATION = 500;

export const postPost_parent: PageImpl = async ctx => {
    const userInfo = ctx.state.mazoLoginUser;
    if (!userInfo) { throw error(403, LOGIN_REQUIRED); }
    if (userInfo.blocked) { throw error(403, LOGIN_REQUIRED); }

    const parent = ctx.params.parent || '';
    const parentType = parent.charAt(0);
    let parentId = parseInt32(parent.substr(1));
    if (isNaN(parentId)) { throw error(400, '你在做什么？嗯？'); }

    let canUseHidden = false;
    let canUseInteractive = parentType === 'f';
    let post: { post_id: number, title: string, content_sc: string, user_id: number, user_name: string, thread_id: number, rev_id: number, flags: number } | undefined
    let thread: { thread_id: number, thread_title: string, forum_id: number, owner_user_id: number, flags: number, spam_likeness: number } | undefined;
    let forum: { forum_id: number, forum_name: string };
    switch (parentType) {
        case 'p':
            post = (await sql('SELECT post_id, title, content_sc, posts.user_id, users.user_name, thread_id, rev_id, posts.flags FROM posts LEFT JOIN users USING (user_id) WHERE post_id = $1 AND (posts.most_recent_update >= 0 OR posts.flags & 16 <> 0)', [parentId])).rows[0];
            if (!post) { throw error(404, '嗯？没有这样的文章哟～'); }
            parentId = post.thread_id;
            // fallthrough
        case 't':
            thread = (await sql('SELECT thread_id, thread_title, forum_id, owner_user_id, flags, spam_likeness FROM thread_targets WHERE thread_id = $1 AND last_page_number > 0 AND successive_fail >= 0', [parentId])).rows[0];
            if (!thread) { throw error(404, '这个主题……是不存在的呢(；′⌒`)'); }
            parentId = thread.forum_id;
            canUseHidden = userInfo.confirmed && !!(await myFirstPostInThread(userInfo, thread.thread_id)) && (userInfo.id === thread.owner_user_id);
            canUseInteractive = userInfo.id === thread.owner_user_id;
            // fallthrough
        case 'f':
            forum = (await forumsCache).get(parentId)!;
            if (!forum) { throw error(404, '没有这样的板块呐～'); }
            break;
        default:
            throw error(400, '都报错了就别乱改啦！');
    }

    let f18Tags: Awaited<ReturnType<typeof allTagsByCategory>> | undefined;
    let selectedF18Tags: Set<number> | undefined;

    const pastPostCount = (await sql<{ cnt: number }>('SELECT COUNT(post_id)::INT as cnt FROM posts WHERE user_id = $1', [userInfo.id])).rows[0];
    const formWarning = pastPostCount.cnt ? undefined : '初次发帖前，建议先阅读本论坛<a href="/rules">章程与规则</a>！' ;
    let replyToRemoteUrl: string | undefined;
    if (thread) { // p or t
        if (!(await hasPassword(post ? post.user_id : thread.owner_user_id))) {
            replyToRemoteUrl = originalReplyUrl(forum.forum_id, thread.thread_id, post?.post_id);
        }
    } else if (forum.forum_id === 18) { // f18（「小說文字區」）
        f18Tags = await allTagsByCategory();
        selectedF18Tags = new Set();
    }

    const defaultPostTitle = (post ? 'Re: ' + post.title : (thread ? 'Re: ' + thread.thread_title : ''));
    const postTitle = ctx.request.body.string('title') || defaultPostTitle;
    const postContent = ctx.request.body.string('content');
    const hide = !!ctx.request.body.string('hide');

    const errors = [];

    let junkWarningForThread: boolean = false;
    let junkWarningForPost: boolean = false;
    let rateLimitError: Awaited<ReturnType<typeof createCaptchaInfo>> | boolean = false;
    if (ctx.request.body.string('submitpost')) { // Okay to go!
        if (f18Tags) {
            const tagLength = ctx.request.body.integer('tag-group-' + F18_CONSTS.CATS.LENGTH);
            const tagForSource = ctx.request.body.integer('tag-group-' + F18_CONSTS.CATS.SOURCE);
            if (f18Tags.get(F18_CONSTS.CATS.LENGTH)!.some(tag => tag.tag_id === tagLength)) {
                selectedF18Tags!.add(tagLength);
            } else {
                errors.push('你未选择小说' + F18_CONSTS.CAT_TITLES[F18_CONSTS.CATS.LENGTH]);
            }
            if (f18Tags.get(F18_CONSTS.CATS.SOURCE)!.some(tag => tag.tag_id === tagForSource)) {
                selectedF18Tags!.add(tagForSource);
            } else {
                errors.push('你未选择作品'  + F18_CONSTS.CAT_TITLES[F18_CONSTS.CATS.SOURCE]);
            }
            const allTags = await allTagsById();
            for (const tagId of ctx.request.body.integerArray('tag-general')) {
                const tag = allTags.get(tagId);
                if (!tag || (tag.category < F18_CONSTS.CATS._FIRST_ARBITRARY)) { errors.push(`标签 ${tagId} 不在候选列表中`); continue; }
                if (tag.flags & 0x2) { errors.push(`标签 ${tagId} 不在候选列表中`); continue; }
                if ((tag.flags & 0x4) && (tagId !== F18_CONSTS.OPEN_CAMPAIGN_TAG_ID)) { errors.push(`标签 ${tagId} 当前不可用`); continue; }
                selectedF18Tags!.add(tagId);
            }
        }

        if (!DEDUP_RATE_LIMITER.attempt(userInfo.id)) {
            errors.push('你操作得太频繁啦～请勿在五秒内连续点击发布～');
        } else {
            const postId = (await sql('SELECT nextval(\'local_post_id\') AS id')).rows[0].id;
            if (!postTitle) { errors.push('未输入标题'); }
            if ((postTitle.length > 55) && (postTitle !== defaultPostTitle)) { errors.push('标题不可超过 55 个字'); }

            if (hide && !canUseHidden) { errors.push('回复可见功能尚未启用'); }
            let parsed: ReturnType<typeof parseBBCode>;
            let properTextLength = 0x7fffffff;
            try {
                parsed = parseBBCode(postId, postContent, canUseInteractive ? { iast: { escapeIdentifier: escapeForIast } } : undefined);
                if (parsed.iastItems) {
                    await checkIastItemsValidity(parsed.iastItems, thread ? thread.thread_id : 0, 0);
                }
                if (thread) {
                    await validateLinkTargetHeadings(parsed.links, thread.thread_id, 0);
                }
                properTextLength = parsed.text.length;
                if (parsed.quotes.length) {
                    properTextLength = await checkQuoteItemValidity(ctx.state, parsed, post);
                }
                if (properTextLength < 3) {
                    errors.push('正文长度太短');
                }
            } catch (e) {
                errors.push((e as Error).message);
            }

            if (!errors.length) {
                const myLastPost = (await sql<{ post_id: number, thread_id: number, content: string }>('SELECT post_id, thread_id, content FROM posts WHERE user_id = $1 ORDER BY post_id DESC LIMIT 1', [userInfo.id])).rows[0];
                if (myLastPost && (myLastPost.content === parsed!.html) && (!thread || (thread.thread_id === myLastPost.thread_id))) {
                    return redirectWithDelay(ctx, {
                        message: '您刚刚发布过相同内容的帖子',
                        targetText: '您发布的相同内容的帖子',
                        targetHref: '/post/' + myLastPost.post_id
                    });
                }

                const newThread = !thread;
                const rateLimitResult = await enforcePostRateLimit(userInfo, newThread ? 5 : 1);
                if (rateLimitResult === 'REJECT') {
                    rateLimitError = true;
                } else if (rateLimitResult === 'CAPTCHA') {
                    let captchaPassed = false;
                    const ref = ctx.request.body.string('ref');
                    if (ref) {
                        const captcha = ctx.request.body.string('captcha');
                        if (!captcha) { 
                            errors.push('你没有填写验证码哦～');
                        } else {
                            try {
                                validateCaptcha(ref, captcha);
                                captchaPassed = true;
                            } catch (e) {
                                errors.push('验证码输入错误（' + (e as Error).message + '）');
                            }
                        }
                    }
                    if (!captchaPassed) { rateLimitError = await createCaptchaInfo(); }
                }
                junkWarningForThread = !!(thread && (thread.flags & 0x2));
                junkWarningForPost = isJunkPost(normalize(parsed!.text.replace(/\[引用]/g, '')));
                const forceByPassJunkWarning = ctx.request.body.string('junk') === 'no';
                const antiJunkPassed = forceByPassJunkWarning || !(junkWarningForThread || junkWarningForPost);
                if (antiJunkPassed && !rateLimitError) {
                    const flags = (hide ? 0x2 : 0x0) | (parsed!.iastItems ? 0x4 : 0x0);
                    const normalizedTextContent = normalize(postTitle + '\n\ufffc\n' + parsed!.text);
                    const now = ctx.refDate;
                    const postedPostId = await withTransaction(async query => {
                        const insertedLog = (await query('INSERT INTO local_post_log (rev_id, post_id, updated_at, title, content, flags, user_id) VALUES (nextval(\'local_rev_id\'), $1, $2, $3, $4, $5, $6) RETURNING rev_id', [postId, now, postTitle, postContent, flags, userInfo.id])).rows[0];
                        if (!thread) {
                            thread = (await query<{ thread_id: number, thread_title: string, forum_id: number, owner_user_id: number, flags: number, spam_likeness: number }>('INSERT INTO thread_targets (thread_id, forum_id, thread_title, last_page_number, firstpost_date, lastpost_date, sortpost_date, lastpost_id, lastpost_user_id, post_count, thread_title_sc, owner_user_id) VALUES (nextval(\'local_thread_id\'), $1, $2, 1073741824, $3, $3, $3, $6, $5, 1, $4, $5) RETURNING thread_id, thread_title, forum_id, owner_user_id, flags, spam_likeness', [forum.forum_id, postTitle, now, normalize(postTitle), userInfo.id, postId])).rows[0];
                        } else {
                            const updateSortPostDate = (parsed.text.replace(/\s+/g, '').length >= LENGTH_THRESHOLD_FOR_UPDATING_SORTPOST_DATE) || !(await threadNeedsSpecialSortDateCalc(thread, query));
                            await query(`UPDATE thread_targets SET lastpost_date = $1${updateSortPostDate ? ', sortpost_date = $1' : ''}, lastpost_id = $3, lastpost_user_id = $4, post_count = (SELECT COUNT(post_id) FROM posts WHERE thread_id = $2 AND posts.most_recent_update >= 0) + 1 WHERE thread_id = $2`, [now, thread.thread_id, postId, userInfo.id]);
                        }
                        await query('INSERT INTO posts (post_id, thread_id, title, user_id, content, posted_date, most_recent_update, content_sc, flags, rev_id, xrefs) VALUES ($1, $2, $3, $4, $5, $6, $6, $7, $8, $9, $10)', [postId, thread.thread_id, postTitle, userInfo.id, parsed.html, now, normalizedTextContent, flags, insertedLog.rev_id, xrefsToWireFormat(parsed)]);
                        if (parsed.iastItems) {
                            await query('INSERT INTO iast_config (thread_id, post_id, iast_inputs, iast_outputs) VALUES ($1, $2, $3, $4)', [thread.thread_id, postId, parsed.iastItems.inputs, parsed.iastItems.usages]);
                        }
                        await attachments.updateAttachmentsToPosts(parsed.links.attachments, postId, query);
                        return postId;
                    });

                    if (selectedF18Tags) {
                        unawaited(Promise.all(Array.from(selectedF18Tags).map(tagId => voteForThread(thread!, tagId, userInfo, -1, 1, false))), `new tags to thread ${thread!.thread_id}`);
                    }
                    unawaited(updateForumTimestamp(forum.forum_id, now), 'forum timestamps');
                    unawaited(sendReplyNotifications(postedPostId, parsed!.replyToUsers, now), 'reply-to notification');
                    unawaited(
                        newThread ? sql(`INSERT INTO thread_stars (user_id, thread_id, star_type, star_at, fc_ids) VALUES ($1, $2, 6, -1, ARRAY[${MY_THREADS_DUMMY_COLLECTION.fc_id}])`, [userInfo.id, thread!.thread_id]) : type4Notification.onNewPost({
                            thread: thread!.thread_id,
                            post: postedPostId,
                            postDate: now,
                            isFromOp: userInfo.id === thread!.owner_user_id,
                            isActualReply: (properTextLength > LENGTH_THRESHOLD_FOR_FORCE_NOTIFICATION) || !parsed!.replyToUsers.length,
                            skipUsers: [userInfo.id].concat(parsed!.replyToUsers.filter(id => id !== userInfo.id)) // Those users are going to receive a type-3 notification anyway, there's no point in flooding them with another type-4 notification.
                        }),
                        'post-posting subscription'
                    );

                    const entityType = (parentType === 'f') ? '主题' : '文章';
                    return redirectWithDelay(ctx, {
                        message: entityType + '已成功发布',
                        targetText: '您刚刚发布的' + entityType,
                        targetHref: '/post/' + postedPostId,
                        numberOfSeconds: 1
                    });
                }
            }
        }
    }

    const pageTitle = ctx.state.render.staticConv(thread ? '发表回复' : '发表主题');
    return finalizePage(ctx, {
        title: pageTitle,
        nav: true,
        deps: ['SCEDITOR']
    }, templates.post(ctx, {
        pageTitle,
        thread,
        forum,
        f18Tags,
        selectedF18Tags,
        postTitle: postTitle,
        postContent: postContent || (post ? `[quote=${/\s|=/.test(post.user_name) ? `"${post.user_name}"` : post.user_name} author=${post.user_id} post=${post.post_id}]` + await getReplyToBBCode(ctx.state, post) + '[/quote]\n' : ''),
        formError: errors.length ? errors.join('；') : null,
        formWarning,
        replyToRemoteUrl,
        rateLimitError, // true: hard-fail. false: pass. otherwise a captcha tuple.
        junkWarning: (junkWarningForThread || junkWarningForPost) ? [junkWarningForThread, junkWarningForPost] : null,
        canDelete: false,
        canUseHidden,
        postHidden: hide
    }));
};

export const postEdit_id: PageImpl = async ctx => {
    const postId = parseInt32(ctx.params.id);

    const isDelete = ctx.request.body.string('delete') === 'delete';
    const userInfo = ctx.state.mazoLoginUser;
    if (!userInfo) { throw error(403, LOGIN_REQUIRED); }

    const post = (await sql<{ title: string, content: string, thread_id: number, user_id: number, rev_id: number, posted_date: number, flags: number }>('SELECT title, content, thread_id, user_id, rev_id, posted_date, flags FROM posts WHERE post_id = $1 AND posts.most_recent_update >= 0', [postId])).rows[0];
    if (!post) { throw error(404, '嗯？没有这样的文章哟～'); }

    const thread = (await sql<{ thread_id: number, thread_title: string, forum_id: number, owner_user_id: number, spam_likeness: number, flags: number }>('SELECT thread_id, thread_title, forum_id, owner_user_id, spam_likeness, flags FROM thread_targets WHERE thread_id = $1 AND last_page_number > 0 AND successive_fail >= 0', [post.thread_id])).rows[0];
    if (!thread) { throw error(404, '这个主题……是不存在的呢(；′⌒`)'); }
    const forum = (await forumsCache).get(thread.forum_id);
    if (!forum) { throw error(404, '没有这样的板块呐～'); }

    if (post.user_id !== userInfo.id) {
        throw error(403, '这不是你的文章啦！');
    }
    const myFirstPostId = await myFirstPostInThread(userInfo, thread.thread_id);
    const canUseHidden = (myFirstPostId !== postId) && userInfo.confirmed && (userInfo.id === thread.owner_user_id);
    const canUseInteractive = (userInfo.id === thread.owner_user_id) && (postId > THRESHOLD_FOR_LOCAL);

    const survivingPostsDesc = (await sql('SELECT post_id, posted_date FROM posts WHERE thread_id = $1 AND posts.most_recent_update >= 0 ORDER BY posted_date DESC LIMIT 2', [thread.thread_id])).rows;
    const canDelete = (survivingPostsDesc.length > 0) && (survivingPostsDesc.shift().post_id === postId);
    if (isDelete) {
        if (!canDelete) {
            throw error(403, '本文章不是所在主题的最后一篇文章，所以没办法删除呢┭┮﹏┭┮');
        }
        const redirectSpec = await withTransaction(async query => {
            await query('UPDATE posts SET most_recent_update = -ABS($1) WHERE post_id = $2 RETURNING posted_date', [ctx.refDate, postId]);
            await query('DELETE FROM iast_config WHERE post_id = $1', [postId]);
            if (!survivingPostsDesc.length) { // No more surviving posts.
                await query('UPDATE thread_targets SET successive_fail = -1000, post_count = 0 WHERE thread_id = $1 AND NOT EXISTS (SELECT post_id FROM posts WHERE posts.thread_id = thread_targets.thread_id AND posts.most_recent_update >= 0)', [thread.thread_id]);
                return {
                    message: '主题已删除～',
                    targetText: '论坛',
                    targetHref: '/forum/' + forum.forum_id
                };
            } else {
                await updateThreadTargetsRow(query, post.thread_id);
                return {
                    message: '文章已删除～',
                    targetText: '主题',
                    targetHref: '/thread/' + thread.thread_id
                };
            }
        });
        // Delete all the reply-to notifications if they're not read yet.
        unawaited(sendReplyNotifications(postId, [], ctx.refDate), 'delete reply-to notification'); 
        unawaited(type4Notification.onDeletingPost({
            thread: thread.thread_id,
            post: postId
        }), 'deleting subs after post deletion');
        return redirectWithDelay(ctx, redirectSpec);
    }

    let formWarning: string | undefined;
    let lastRevContent: string;
    const isLocal = post.rev_id > THRESHOLD_FOR_LOCAL;
    const lastRev = (await sql('SELECT title, updated_at, content FROM local_post_log WHERE rev_id = $1', [post.rev_id])).rows[0];
    if (isLocal) {
        if (!lastRev) {
            throw error(404, '不知道为什么没有找到之前的版本。');
        }
        lastRevContent = lastRev.content;
        if (postId <= THRESHOLD_FOR_LOCAL) {
            formWarning = '此帖子目前显示在镜像编辑过的版本。此帖子在源站的更新永不会自动同步至镜像。若需重新开启自动从源站同步至镜像的功能，请<a href="/modmail">联系管理员</a>。';
        }
    } else {
        lastRevContent = lastRev?.content ?? postHtmlToBBCode(postId, post.content);
        formWarning = '你正在试图编辑你在源站发布的帖子。一旦提交编辑后，此帖子在<a href="' + originalRemoteUrl(thread.forum_id, thread.thread_id) + '">源站</a>的更新将不会再自动同步至镜像（除非联系管理员手动恢复）。';
    }
    const postTitle = ctx.request.body.string('title');
    const postContent = ctx.request.body.string('content');
    const errors = [];
    if (ctx.request.body.string('submitpost')) { // Okay to go!
        if (!postTitle) { errors.push('未输入标题'); }
        if (postTitle.length > 55) { errors.push('标题不可超过 55 个字'); }
        const hide = ctx.request.body.string('hide');
        if (hide && !canUseHidden) { errors.push('回复可见功能尚未启用'); }
        let parsed: ReturnType<typeof parseBBCode>;
        let properTextLength = 0x7fffffff;
        try {
            parsed = parseBBCode(postId, postContent, canUseInteractive ? { iast: { escapeIdentifier: escapeForIast } } : undefined);
            if (parsed.iastItems) {
                await checkIastItemsValidity(parsed.iastItems, thread.thread_id, postId);
            }
            await validateLinkTargetHeadings(parsed.links, thread.thread_id, postId);
            properTextLength = parsed.text.length;
            if (parsed.quotes.length) {
                properTextLength = await checkQuoteItemValidity(ctx.state, parsed);
            }
            if (properTextLength < 3) {
                errors.push('正文长度太短');
            }
        } catch (e) {
            errors.push((e as Error).message);
        }
        const firstOfThread = (await sql('SELECT post_id FROM posts WHERE thread_id = $1 ORDER BY posted_date ASC, post_id ASC LIMIT 1', [thread.thread_id])).rows[0];
        if (!errors.length) {
            const flags = (hide ? 0x2 : 0x0) | (parsed!.iastItems ? 0x4 : 0x0);
            const normalizedTextContent = normalize(postTitle + '\n\ufffc\n' + parsed!.text);
            await withTransaction(async query => {
                const revId = (await query('INSERT INTO local_post_log (rev_id, post_id, updated_at, title, content, flags, user_id) VALUES (nextval(\'local_rev_id\'), $1, $2, $3, $4, $5, $6) RETURNING rev_id', [postId, ctx.refDate, postTitle, postContent, flags, userInfo.id])).rows[0].rev_id;
                await query('UPDATE posts SET title = $1, content = $2, most_recent_update = $3, content_sc = $4, flags = $5, rev_id = $6, xrefs = $7 WHERE post_id = $8', [postTitle, parsed.html, ctx.refDate, normalizedTextContent, flags, revId, xrefsToWireFormat(parsed), postId]);
                if (parsed.iastItems) {
                    await query('INSERT INTO iast_config (thread_id, post_id, iast_inputs, iast_outputs) VALUES ($1, $2, $3, $4) ON CONFLICT (post_id) DO UPDATE SET iast_inputs = EXCLUDED.iast_inputs, iast_outputs = EXCLUDED.iast_outputs', [thread.thread_id, postId, parsed.iastItems.inputs, parsed.iastItems.usages]);
                } else {
                    await query('DELETE FROM iast_config WHERE post_id = $1', [postId]);
                }
                await attachments.updateAttachmentsToPosts(parsed.links.attachments, postId, query);
                if (firstOfThread.post_id === postId) {
                    await query('UPDATE thread_targets SET thread_title = $1, thread_title_sc = $2 WHERE thread_id = $3', [postTitle, normalize(postTitle), thread.thread_id]);
                }
            });
            unawaited(type4Notification.onUpdatingPostConvertingBetweenType3AndType4({
                thread: thread.thread_id,
                post: postId,
                isFromOp: userInfo.id === thread.owner_user_id,
                isActualReply: (properTextLength > LENGTH_THRESHOLD_FOR_FORCE_NOTIFICATION) || !parsed!.replyToUsers.length,
                postDate: post.posted_date
            }, sendReplyNotifications(postId, parsed!.replyToUsers.filter(id => id !== userInfo.id), ctx.refDate)), 'reply-to notification');
            return redirectWithDelay(ctx, {
                message: '文章已成功修改',
                targetText: '您刚刚修改好的文章',
                targetHref: '/post/' + postId,
                numberOfSeconds: 1
            });
        }
    }

    const pageTitle = ctx.state.render.staticConv('编辑文章');
    return finalizePage(ctx, {
        title: pageTitle,
        nav: true,
        deps: ['SCEDITOR']
    }, templates.post(ctx, {
        pageTitle,
        thread,
        forum,
        postTitle: postTitle || post.title,
        postContent: postContent || lastRevContent,
        formError: errors.length ? errors.join('；') : null,
        formWarning,
        rateLimitError: false,
        junkWarning: null,
        canDelete,
        canUseHidden,
        postHidden: !!(post.flags & 2),
        replyToRemoteUrl: undefined,
        f18Tags: undefined,
        selectedF18Tags: undefined
    }));
};