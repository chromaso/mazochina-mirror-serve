import { query as sql } from '../db';
import { rowsToMap } from '../../common/logic_utils';

export const fetchType5Notifications = async (activeUserId: number, t5ThreadIds: number[]): Promise<Map<number, { userName: string, userId: number, postTitle: string | undefined, unreadMsgCount: number, postTitleRead: boolean } | undefined>> => {
    const t5Threads = (await sql<{ thread_id: number, from_user: number, to_user: number }>('SELECT thread_id, from_user, to_user FROM local_conversations WHERE thread_id = ANY($1::int[])', [t5ThreadIds])).rows;

    const froms: number[] = [];
    const tos: number[] = [];

    const threadToUserKVPs = t5Threads.map(thread => {
        if (thread.from_user === activeUserId) {
            froms.push(thread.thread_id);
            return [thread.thread_id, thread.to_user] as const;
        }
        if (thread.to_user === activeUserId) {
            tos.push(thread.thread_id);
            return [thread.thread_id, thread.from_user] as const;
        }
        return [thread.thread_id, 0] as const;
    });

    const paramGroups = [
        [froms, 8],
        [tos, 0]
    ];

    const userInfo = rowsToMap((await sql<{ user_id: number, user_name: string }>('SELECT user_id, user_name FROM users WHERE user_id = ANY($1::int[])', [
        threadToUserKVPs.map(item => item[1])
    ])).rows, 'user_id');

    const lastPmWithTitle = rowsToMap(([] as { thread_id: number, title: string, post_id: number }[]).concat(...(await Promise.all(paramGroups.map(params =>
        sql('SELECT thread_id, title, post_id FROM local_pms pms WHERE thread_id = ANY($1::int[]) AND flags & 8 = $2 AND title <> \'\' AND NOT EXISTS (SELECT post_id FROM local_pms ref WHERE ref.thread_id = pms.thread_id AND ref.flags & 8 = $2 AND title <> \'\' AND ref.post_id > pms.post_id)', params)
    ))).map(result => result.rows)), 'thread_id');

    const unreadCountOfEachThread = rowsToMap(([] as { thread_id: number, cnt: number, min_unread: number }[]).concat(...(await Promise.all(paramGroups.map(params =>
        sql('WITH rs AS (SELECT thread_id, up_to_date FROM unread_status WHERE user_id = $3 AND thread_id = ANY($1::int[])) SELECT thread_id, COUNT(post_id)::INT AS cnt, MIN(post_id) as min_unread FROM local_pms WHERE thread_id = ANY($1::int[]) AND flags & 8 = $2 AND posted_date > COALESCE((SELECT up_to_date FROM rs WHERE rs.thread_id = local_pms.thread_id), 0) GROUP BY thread_id', [params[0], params[1], activeUserId])
    ))).map(result => result.rows)), 'thread_id');

    return new Map(threadToUserKVPs.map(([threadId, userId]) => {
        const user = userInfo.get(userId);
        if (!user) { return [threadId, undefined]; }
        const lastPm = lastPmWithTitle.get(threadId);
        const unread = unreadCountOfEachThread.get(threadId);

        return [threadId, {
            userId: userId,
            userName: user.user_name,
            postTitle: lastPm ? lastPm.title : undefined,
            postTitleRead: (unread && lastPm) ? lastPm.post_id >= unread.min_unread : false,
            unreadMsgCount: unread ? unread.cnt : 0
        }];
    }));
};