import { Types } from '../types';
import { $ } from './dollar';

// A stripped-down version of bootstrap modal, that only animates when showing, not when hiding.
export const showModal: Types['showModal'] = (title, body, beforeClose, size) => {
    document.body.className = 'modal-open';
    const backdrop = $('div', 'modal-backdrop fade');
    let modal: HTMLElement;
    const closeModal = () => {
        if ((!beforeClose) || beforeClose()) {
            document.body.className = '';
            document.body.removeChild(backdrop);
            document.body.removeChild(modal);
        }
    };
    modal = $('div', 'modal fade',
        $('div', 'modal-dialog modal-dialogmodal-dialog-scrollable' + (size ? ' modal-' + size : ''),
            $('div', 'modal-content', [
                $('div', 'modal-header', [
                    $('h5', 'modal-title', title),
                    $('button', 'btn-close', undefined, closeModal)
                ]),
                $('div', 'modal-body', body)
            ])
        )
    );
    modal.style.display = 'block';
    document.body.appendChild(backdrop);
    document.body.append(modal);
    setTimeout(() => {
        backdrop.classList.add('show');
        modal.classList.add('show');
    });
    return closeModal;
};