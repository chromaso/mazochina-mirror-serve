import { THRESHOLD_FOR_LOCAL } from '../../common/consts';
import { rowsToMap } from '../../common/logic_utils';
import { query as sql } from '../db';
import { AdminLogDAO } from '../shared/moderation';
import * as notificationTemplates from '../templates/notification';
import { fetchType5Notifications } from '../utils/type_5_notifications';
import { parseInt32 } from '../utils/logic_utils';
import { forumsCache } from '../shared/forum';
import { error, finalizeSegment, HtmlSegmentImpl, LOGIN_REQUIRED, RpcImpl } from '../utils/render_utils';
import { fetchType4Notifications } from '../utils/type_4_notifications';

export const apiNotifications_v2: HtmlSegmentImpl = async ctx => {
    const userInfo = ctx.state.mazoLoginUser;
    if (!userInfo) { throw error(403, LOGIN_REQUIRED); }
    let pageToken = parseInt32(`${ctx.query.token || ''}`);
    if (isNaN(pageToken) || pageToken <= 0) { pageToken = THRESHOLD_FOR_LOCAL; }

    const [notifications, t4Notifications, t5Notifications] = (await Promise.all(['<', '=', '>'].map(sym => sql<{ notification_id: number, notification_date: number, message_type: 2 | 3 | 4 | 5, primary_target_id: number, dismissed: number }>(`SELECT notification_id, notification_date, message_type, primary_target_id, dismissed FROM notifications WHERE user_id = $1 AND notification_id < $2 AND message_type ${sym} 4 ORDER BY notification_date DESC LIMIT 50`, [userInfo.id, pageToken])))).map(res => res.rows);
    // Post being replied.
    const postIds = new Set(notifications.filter(_ => _.message_type === 3).map(_ => _.primary_target_id).filter(_ => _));
    const adminOpIds = notifications.filter(_ => _.message_type === 2).map(_ => _.primary_target_id);
    const adminOps = adminOpIds.length ? (await sql<{ admin_log_id: number, target_id: number, target_type: AdminLogDAO['target_type'], note: string, extra_information: any }>('SELECT admin_log_id, target_id, target_type, note, extra_information FROM local_admin_log WHERE admin_log_id = ANY($1::INT[])', [adminOpIds])).rows : undefined;

    let threadIds: number[] = [];
    let threads: Map<number, { thread_title: string, thread_id: number, forum_id: number, owner_user_id: number, flags: number }> | undefined;
    let adminThreadSplitsAndMerges: Map<number, any> | undefined;
    if (adminOps) {
        adminOps.filter(row => row.target_type === 'post').forEach(row => postIds.add(row.target_id));

        const threadAdminOps = adminOps.filter(row => row.target_type === 'thread');
        threadIds = threadAdminOps.map(row => row.target_id);
        const threadSplitsAndMerges = threadAdminOps.filter(row => row.extra_information?.thread);
        if (threadSplitsAndMerges.length) {
            threadIds = threadIds.concat(threadSplitsAndMerges.map(row => row.extra_information.thread));
            const allPostIds = new Set<number>();
            for (const split of threadSplitsAndMerges) {
                for (const post of split.extra_information.posts) {
                    allPostIds.add(post);
                }
            }
            const splitPosts = (await sql<{ post_id: number, thread_id: number, not_deleted: boolean }>('SELECT post_id, thread_id, most_recent_update >= 0 AS not_deleted FROM posts WHERE user_id = $1 AND post_id = ANY($2::INT[]) ORDER BY posted_date ASC, post_id ASC', [userInfo.id, Array.from(allPostIds)])).rows;
            adminThreadSplitsAndMerges = new Map(threadSplitsAndMerges.map(adminOpRow => {
                const count = splitPosts.filter(post => adminOpRow.extra_information.posts.includes(post.post_id)).length; // Including those deleted and those no longer in the target thread (possibly another split).
                const firstPost = splitPosts.filter(post => (post.thread_id === adminOpRow.extra_information.thread) && post.not_deleted);
                return [
                    adminOpRow.admin_log_id,
                    { count, link: firstPost.length ? `/post/${firstPost[0].post_id}` : `/thread/${adminOpRow.extra_information.thread}` }
                ];
            }));
        }
    }

    const t4ThreadIds = t4Notifications.map(item => item.primary_target_id);
    threadIds = threadIds.concat(t4ThreadIds);
    if (threadIds.length) {
        threads = rowsToMap((await sql('SELECT thread_title, thread_id, forum_id, owner_user_id, flags FROM thread_targets WHERE thread_id = ANY($1::INT[])', [threadIds])).rows, 'thread_id');
    }
    const t4Threads = t4ThreadIds.length ? fetchType4Notifications(userInfo.id, t4ThreadIds, threads!) : undefined;

    const t5ThreadIds = t5Notifications.map(item => item.primary_target_id);
    const t5Threads = t5ThreadIds.length ? fetchType5Notifications(userInfo.id, t5ThreadIds) : undefined;

    const posts = postIds.size ? rowsToMap((await sql('SELECT title, thread_id, post_id, posts.user_id, users.user_name FROM posts LEFT JOIN users USING (user_id) WHERE post_id = ANY($1::INT[])', [Array.from(postIds)])).rows, 'post_id') : undefined;

    return finalizeSegment(ctx, notificationTemplates.notifications(ctx, {
        notifications,
        t4Notifications,
        t5Notifications,
        t5Threads: t5Threads ? (await t5Threads) : undefined,
        adminLogs: adminOps ? rowsToMap(adminOps, 'admin_log_id') : undefined,
        adminThreadSplitsAndMerges,
        posts,
        threads,
        t4Threads: t4Threads ? (await t4Threads) : undefined,
        forums: await forumsCache
    }));
};

export const postApiNotifications: RpcImpl = async ctx => {
    const userInfo = ctx.state.mazoLoginUser;
    if (!userInfo) { throw error(403, 'Not logged in.'); }
    const ids = ctx.request.body.integerArray('id');
    if (ids.some(isNaN)) { throw error(400, 'Notification ID not specified.'); }
    await sql('UPDATE notifications SET dismissed = $1 WHERE notification_id = ANY($2::INT[]) AND user_id = $3 AND dismissed = 0', [ctx.refDate, ids, userInfo.id]);
    ctx.body = `Marked ${ids.join(', ')} as read`;
};
