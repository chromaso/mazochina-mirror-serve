import * as elements from 'typed-xhtml';
import { HtmlTmpl } from '../utils/tsx_utils';
import { AdminLogDAO } from '../shared/moderation';
import { forumsCache } from '../shared/forum';
import { moveActionPostAcrossThread } from './moderation';

type Notification = {
    notification_id: number,
    dismissed: number,
    message_type: 2 | 3 | 4 | 5,
    primary_target_id: number,
    notification_date: number
};

type SingleAdminLog = Pick<AdminLogDAO, 'target_id' | 'target_type' | 'extra_information' | 'admin_log_id' | 'note'>;

// Migrated from notifications.pug.
export const notifications: HtmlTmpl<{
    notifications: Notification[],
    t4Notifications: Notification[],
    t5Notifications: Notification[],
    adminLogs: Map<number, SingleAdminLog> | undefined,
    adminThreadSplitsAndMerges: Map<number, { count: number, link: string }> | undefined,
    posts: Map<number, { thread_id: number, title: string, user_id: number, user_name: string }> | undefined,
    threads: Map<number, { thread_title: string, flags: number }> | undefined,
    t4Threads: Map<number, [number, number] | undefined> | undefined,
    t5Threads: Map<number, { userId: number, userName: string, postTitleRead: boolean, postTitle: string | undefined, unreadMsgCount: number } | undefined> | undefined,
    forums: Awaited<typeof forumsCache>
}> = (ctx, self) => {
    const { render } = ctx.state;

    const single = (notification: Notification) => {
        const messageType = notification.message_type;
        const hrefSuffix = notification.dismissed ? '' : ('?notification=' + notification.notification_id);

        const renderType2MovePostAcrossThread = (log: SingleAdminLog, threadSplit: { count: number; link: string; } | undefined) => (
            <>
                你在主题
                {' '}
                <a href={'/thread/' + log.target_id + hrefSuffix}>
                    {render.contentConv(self.threads?.get(log.target_id)?.thread_title ?? '[UNKNOWN]')}
                </a>
                {' '}
                下的
                {' '}
                {threadSplit ? (
                    <>
                        {threadSplit.count} 篇文章已被管理员{moveActionPostAcrossThread(ctx, { fromThread: log.target_id, toThread: log.extra_information.thread, threads: self.threads! })}至
                        {' '}
                        <a href={threadSplit.link + hrefSuffix}>
                            {render.contentConv(self.threads?.get(log.extra_information.thread)?.thread_title ?? '[UNKNOWN]')}
                        </a>
                    </>
                ) : '一些文章已被管理员移动至另一主题'}
            </>
        );

        const renderType2 = (notification: Notification) => {
            const log = self.adminLogs?.get(notification.primary_target_id);
            if (!log) { return '未知管理员操作。' }
            const targetType = log.target_type;

            return <>
                {
                    ((targetType === 'user') ? (
                        <>
                            你已被
                            <a href={'/moderation?u=' + log.target_id}>
                                {(log.extra_information?.do === 'free')  ? '解除封禁' : '封禁'}
                            </a>
                        </>
                    ) : (targetType === 'post') ? (
                        (post => (
                            <>
                                你的
                                {post ? <>文章 <a href={'/thread/' + post.thread_id + hrefSuffix}>{render.contentConv(post.title)}</a> </> : '一篇文章'}
                                已被管理员删除
                            </>
                        ))(self.posts?.get(log.target_id))
                    ) : (targetType === 'thread') ? (
                        log.extra_information.thread ? (
                            renderType2MovePostAcrossThread(log, self.adminThreadSplitsAndMerges?.get(log.admin_log_id))
                        ) : (
                            <>
                                你的主题
                                {' '}
                                <a href={'/thread/' + log.target_id + hrefSuffix}>
                                    {render.contentConv(self.threads?.get(log.target_id)?.thread_title ?? '[UNKNOWN]')}
                                </a>
                                {' '}
                                已被管理员从
                                {' '}
                                <a href={'/forum/' + log.extra_information.from + hrefSuffix}>
                                    {render.staticConv(self.forums.get(log.extra_information.from)!.forum_name)}
                                </a>
                                {' '}
                                {
                                    ((log.extra_information.to === -3) ? (
                                        '半隐藏'
                                    ) : (log.extra_information.to === -2) ? (
                                        '取消隐藏'
                                    ) : (log.extra_information.to === -1) ? (
                                        '隐藏'
                                    ) : (log.extra_information.to === 0) ? (
                                        '删除'
                                    ) : (
                                        <>
                                            {' '}
                                            移动至
                                            {' '}
                                            <a href={'/forum/' + log.extra_information.to + hrefSuffix}>
                                                {render.staticConv(self.forums.get(log.extra_information.to)!.forum_name)}
                                            </a>
                                        </>
                                    ))
                                }
                            </>
                        )
                    ) : (
                        '未知操作'
                    ))
                }
                。操作理由：{render.contentConv(log.note)}
            </>
        };
        
        const renderType3 = (notification: Notification) => {
            const postItem = self.posts?.get(notification.primary_target_id);
            if (!postItem) { return '你的文章曾被回复' }
            return (
                <>
                    <a href={'/author/' + postItem.user_id + hrefSuffix}>
                        {postItem.user_name}
                    </a>
                    {' '}
                    在
                    {' '}
                    <a href={'/post/' + notification.primary_target_id + hrefSuffix}>
                        {render.contentConv(postItem.title)}
                    </a>
                    {' '}
                    中回复了你
                </>
            );
        };
        
        const renderType4 = (notification: Notification) => {
            const t4ThreadData = self.t4Threads?.get(notification.primary_target_id);
            const t4ThreadTitle = self.threads?.get(notification.primary_target_id)?.thread_title ?? '[UNKNOWN]';
            const t4ThreadLink = '/post/t' + notification.primary_target_id + hrefSuffix;
            return (
                <>
                    {!t4ThreadData ? (
                        <>
                            你曾订阅的主题
                            {' '}
                            <a href={t4ThreadLink}>{render.contentConv(t4ThreadTitle)}</a>
                            {' '}
                            曾有新回复
                        </>
                    ) : (t4ThreadData[0] === -1) ? (
                        (t4ThreadData[1] === 0) ? (
                            <>
                                你订阅的主题
                                {' '}
                                <a href={t4ThreadLink}>{render.contentConv(t4ThreadTitle)}</a>
                                {' '}
                                楼主曾有新的回复，你现已阅读
                            </>
                        ) : (
                            <>
                                你订阅的主题
                                {' '}
                                <a href={t4ThreadLink}>{render.contentConv(t4ThreadTitle)}</a>
                                {' '}
                                有 {t4ThreadData[1]} 个来自楼主的新回复
                            </>
                        )
                    ) : (t4ThreadData[0] === 0) ? (
                        <>
                            你订阅的主题
                            {' '}
                            <a href={t4ThreadLink}>{render.contentConv(t4ThreadTitle)}</a>
                            {' '}
                            曾有新的回复，你现已阅读
                        </>
                    ) : (
                        <>
                            你订阅的主题
                            {' '}
                            <a href={t4ThreadLink}>{render.contentConv(t4ThreadTitle)}</a>
                            {' '}
                            有 {t4ThreadData[0]} 个新回复
                            {(t4ThreadData[1] !== 0) ? (<>（其中 {t4ThreadData[1]} 个来自楼主）</>) : null}
                        </>
                    )}
                </>
            );
        };
        
        const renderType5 = (notification: Notification) => {
            const t5ThreadData = self.t5Threads?.get(notification.primary_target_id);
            return (
                <>
                    {t5ThreadData ? (
                        t5ThreadData.unreadMsgCount ? (
                            <>
                                你收到了来自
                                {' '}
                                <a href={'/author/' + t5ThreadData.userId}>
                                    {t5ThreadData.userName}
                                </a>
                                {' '}
                                的
                                {' '}
                                <a href={'/pm/' + t5ThreadData.userId}>
                                    {t5ThreadData.postTitleRead ? `「${render.contentConv(t5ThreadData.postTitle!)}」等` : ''}
                                    {' '}
                                    {t5ThreadData.unreadMsgCount} 条未读私信
                                </a>
                            </>
                        ) : (
                            <>
                                你曾收到来自
                                {' '}
                                <a href={'/author/' + t5ThreadData.userId}>
                                    {t5ThreadData.userName}
                                </a>
                                {' '}
                                的
                                {' '}
                                <a href={'/pm/' + t5ThreadData.userId}>
                                    {t5ThreadData.postTitle ? `「${render.contentConv(t5ThreadData.postTitle)}」等` : ''}
                                    私信
                                </a>
                            </>
                        )
                    ) : (
                        '你曾有新的私信'
                    )}
                </>
            );
        };
    
        return (
            <li
                id={'nt-' + notification.notification_id}
                class={'list-group-item ' + (notification.dismissed ? ' dismissed' : '')}
            >
                {
                    (messageType === 2) ? (
                        renderType2(notification)
                    ) : (messageType === 3) ? (
                        renderType3(notification)
                    ) : (messageType === 4) ? (
                        renderType4(notification)
                    ) : messageType === 5 ? (
                        renderType5(notification)
                    ) : (
                        '未知事件'
                    )
                }
                <small class="float-end text-muted mt-1 ps-2">
                    {render.renderDate(notification.notification_date)}
                </small>
            </li>
        );
    };

    return (
        <>
            <div id="fav-card-inner" class="card-body">
                {self.t4Notifications.length ? (
                    <ul class="list-group">
                        {self.t4Notifications.map(single)}
                    </ul>
                ) : (
                    '当前暂未任何新回复'
                )}
            </div>
            <div id="noti-card-inner" class="card-body">
                {self.notifications.length ? (
                    <ul class="list-group">
                        {self.notifications.map(single)}
                    </ul>
                ) : (
                    '当前没有任何提醒'
                )}
            </div>
            <div id="pm-card-inner" class="card-body">
                {self.t5Notifications.length ? (
                    <ul class="list-group">
                        {self.t5Notifications.map(single)}
                    </ul>
                ) : (
                    '当前暂未任何私信'
                )}
            </div>
        </>
    );
}
