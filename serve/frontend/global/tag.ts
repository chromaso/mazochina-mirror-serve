import { $xhr } from "./dollar";

export const initTagPageSwitch = (tagId: number) => {
    const switchEl = document.getElementById('tag-warning-switch') as HTMLInputElement;
    switchEl.disabled = false;
    switchEl.addEventListener('click', () => {
        switchEl.disabled = true;
        $xhr('/api/tags/settings', {
            f: 'warn=' + (switchEl.checked ? '' : '-') + tagId,
            t: _ => { switchEl.disabled = false; },
            e: e => window.alert('保存设置失败：' + e)
        });
    });
};