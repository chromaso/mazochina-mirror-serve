import { $, $id, $xhr, debugLog, reportIfNeeded } from '../common-import';

const DATA_KEY = 'data-last';
const ADD = 'add';
const REMOVE = 'remove';
const CLASSNAME_D_NONE = 'd-none';
const CLASSNAME_ACTIVE = 'active';
const CLASSLIST = 'classList';

export const loadMmPm = (conversationId: number): void => {
    const hrButtons = document.querySelectorAll('.hr-button');
    const hrbForm = hrButtons[0];
    const hrbHistory = hrButtons[1];
    const hr = hrbForm.parentNode as HTMLElement;
    let hrStatus = 0; // -1 for history only; 1 for form only.
    const topHalf = hr.previousElementSibling!;
    const bottomHalf = hr.nextElementSibling!;

    const setStatus = (nextVal: number) => {
        topHalf[CLASSLIST][(nextVal > 0) ? ADD : REMOVE](CLASSNAME_D_NONE);
        hrbForm[CLASSLIST][(nextVal > 0) ? ADD : REMOVE](CLASSNAME_D_NONE);
        hrbHistory[CLASSLIST][(nextVal > 0) ? ADD : REMOVE](CLASSNAME_ACTIVE);
        hr[CLASSLIST][(nextVal > 0) ? REMOVE : ADD]('mt-2');

        bottomHalf[CLASSLIST][(nextVal < 0) ? ADD : REMOVE](CLASSNAME_D_NONE);
        hrbHistory[CLASSLIST][(nextVal < 0) ? ADD : REMOVE](CLASSNAME_D_NONE);
        hrbForm[CLASSLIST][(nextVal < 0) ? ADD : REMOVE](CLASSNAME_ACTIVE);
        hr[CLASSLIST][(nextVal < 0) ? REMOVE : ADD]('mb-2');

        hrStatus = nextVal;
    };
    hrbForm.addEventListener('click', () => setStatus(hrStatus + 1));
    hrbHistory.addEventListener('click', () => setStatus(hrStatus - 1));

    const container = $id('conversation-' + conversationId)!;
    const loadingSpinner = $id('mm-pm-loading') as HTMLElement;
    const [fetchNext, loadMoreButtonEventListener] = [(anchor: HTMLElement, lastPostId?: number) => {
        const scrollRef = lastPostId ? anchor.nextElementSibling as HTMLElement : undefined;

        anchor.parentNode!.removeChild(anchor);
        container.insertBefore(loadingSpinner, container.firstChild);
        $xhr('/api/pm/' + conversationId + (lastPostId ? '?last=' + lastPostId : ''), {
            t: (html: string) => {
                const prev = $('div');
                prev.innerHTML = html;

                const savedScroll = scrollRef ? scrollRef.getBoundingClientRect().top : 0;
                container.replaceChild(prev, loadingSpinner);
                if (prev.children[0].hasAttribute(DATA_KEY)) {
                    prev.children[0].addEventListener('click', loadMoreButtonEventListener);
                }
                const latestInCurrrentGroup = prev.lastElementChild!.getAttribute('data-date');
                const scrollAdjustor = scrollRef ? (() => topHalf.scrollBy(0, scrollRef.getBoundingClientRect().top - savedScroll)) : (() => prev.lastElementChild!.scrollIntoView());
                setTimeout(() => {
                    scrollAdjustor();
                    if (latestInCurrrentGroup && !lastPostId) {
                        const date = parseInt(latestInCurrrentGroup, 36);
                        debugLog(`Marking PM ${conversationId} to ${date}`);
                        reportIfNeeded(conversationId, date);
                    }
                });
            },
            e: error => container.replaceChild($('div', 'alert alert-danger my-4', ['过往对话加载失败啦：', error]), loadingSpinner)
        });
    }, (e: Event) =>
        fetchNext(e.currentTarget as HTMLElement, parseInt((e.currentTarget as HTMLElement).getAttribute(DATA_KEY)!))
    ];
    fetchNext(loadingSpinner);
};