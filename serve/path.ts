import { join } from 'path';

export const fromRoot = (...segments: string[]) => join(__dirname, ...segments);