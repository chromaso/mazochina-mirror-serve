import * as elements from 'typed-xhtml';
import { HtmlTmpl } from '../utils/tsx_utils';
import { forumsCache } from '../shared/forum';
import { pagination, PaginationParams } from './pagination';
import { AdminLogDAO } from '../shared/moderation';

type GlobalMaps = {
    users: Map<number, { user_name: string }>,
    posts: Map<number, { user_id: number, title: string, thread_id: number, content: string }>,
    threads: Map<number, { owner_user_id: number, thread_title: string, flags: number }>,
    forums: Awaited<typeof forumsCache>
};

type SingleLogAnd<T extends keyof GlobalMaps> = { log: AdminLogDAO } & Pick<GlobalMaps, T>;

const userOpRow: HtmlTmpl<SingleLogAnd<'users'>> = (ctx, { log, users }) => (
    <>
        <td>
            {(log.extra_information?.do === 'free') ? '解除封禁' : '封禁用户'}
        </td>
        <td>
            <a href={'/author/' + log.target_id}>
                {users.get(log.target_id)?.user_name ?? '[UNKNOWN]'}
            </a>
        </td>
    </>
);

const postOpRow: HtmlTmpl<SingleLogAnd<'users' | 'posts'>> = (ctx, { log, posts, users }) => {
    const { render } = ctx.state;
    const postItem = posts.get(log.target_id)!;
    return (
        <>
            <td>删除文章</td>
            <td>
                <a href={'/author/' + postItem.user_id}>
                    {users.get(postItem.user_id)?.user_name ?? '[UNKNOWN]'}
                </a>
                {' 发布的 '}
                <a href={'/thread/' + postItem.thread_id}>
                    {render.contentConv(postItem.title)}
                </a>
                ，内容为「
                {render.contentConv(postItem.content.substring(0, 16))}
                {(postItem.content.length > 16) ? (
                    <>…」（共 {postItem.content.length} 字）</>
                ) : '」'}
            </td>
        </>
    );
};

export const moveActionPostAcrossThread: HtmlTmpl<{
    fromThread: number, toThread: number, threads: Map<number, { flags: number }>
}> = (ctx, { fromThread, toThread, threads }): string => {
    if ((threads.get(fromThread)?.flags ?? 0) & 0x10) { // This check must come before the "else if" part, as the merge target thread could be one created from an earlier split operation.
        return '合并';
    }
    if ((threads.get(toThread)?.flags ?? 0) & 0x8) {
        return '拆分';
    } 
    console.error(`Should never happen: post move from ${fromThread} to ${toThread} not merge nor split.`);
    return '移动';
};

const threadOpRow: HtmlTmpl<SingleLogAnd<'users' | 'threads' | 'forums'>> = (ctx, { log, users, threads, forums }) => {
    const { render } = ctx.state;
    const { to } = log.extra_information;
    const thread = threads.get(log.target_id)!;

    return (
        <>
            <td>
                {log.extra_information.thread
                    ? moveActionPostAcrossThread(ctx, { fromThread: log.target_id, toThread: log.extra_information.thread, threads })
                    : ((to === -3) ? (
                        <>半隐藏（<a href="/thread/1073745138">?</a>）</>
                    ) : (to === -2) ? (
                        '显示'
                    ) : (to === -1) ? (
                        '隐藏'
                    ) : (to === 0) ? (
                        '删除'
                    ) : (
                        '移动'
                    ))}
                主题
            </td>
            <td>
                {log.extra_information.from ? (
                    <>
                        <a href={'/forum/' + log.extra_information.from}>
                            {render.contentConv(forums.get(log.extra_information.from)?.forum_name ?? '[UNKNOWN]')}
                        </a>
                        {' 下 '}
                    </>
                ) : null}
                {(!(log.flags & 0x2)) ? (
                    <>
                        <a href={'/author/' + thread.owner_user_id}>
                            {users.get(thread.owner_user_id)?.user_name ?? '[UNKNOWN]'}
                        </a>
                        {' 发布的 '}
                    </>
                ) : null}
                <a href={'/thread/' + log.target_id}>
                    {render.contentConv(thread.thread_title)}
                </a>
                {log.extra_information.to > 0 ? (
                    <>
                        {' 移动至 '}
                        <a href={'/forum/' + log.extra_information.to}>
                            {render.contentConv(forums.get(log.extra_information.to)?.forum_name ?? '[UNKNOWN]')}
                        </a>
                    </>
                ) : log.extra_information.thread ? (
                    <>
                        {' '}
                        中的 {log.extra_information.posts.length} 篇文章被{moveActionPostAcrossThread(ctx, { fromThread: log.target_id, toThread: log.extra_information.thread, threads })}至
                        {' '}
                        <a href={'/thread/' + log.extra_information.thread}>
                            {render.contentConv(
                                threads.get(log.extra_information.thread)?.thread_title ?? '[UNKNOWN]',
                            )}
                        </a>
                    </>
                ) : null}
            </td>
        </>
    );
};

// Migrated from admin_log_table.inc.pug.
export const adminLogTable: HtmlTmpl<{
    logs: AdminLogDAO[]
} & GlobalMaps> = (ctx, self) => (
    <table class="table">
        <thead>
            <th>操作</th>
            <th>对象</th>
            <th>管理员</th>
            <th>理由</th>
            <th>时间</th>
        </thead>
        <tbody>
            {self.logs.map(log => (
                <tr>
                    {
                        ((log.target_type === 'user') ? (
                            userOpRow(ctx, { log, users: self.users })
                        ) : (log.target_type === 'post') ? (
                            postOpRow(ctx, { log, users: self.users, posts: self.posts })
                        ) : (log.target_type === 'thread') ? (
                            threadOpRow(ctx, { log, users: self.users, threads: self.threads, forums: self.forums })
                        ) : (
                            <>
                                <td>未知操作</td>
                                <td>
                                    {log.target_id}（{log.target_type}）
                                </td>
                            </>
                        ))
                    }
                    <td>
                        <a href={'/moderation?a=' + log.admin_user_id}>
                            {self.users.get(log.admin_user_id)?.user_name ?? '[UNKNOWN]'}
                        </a>
                    </td>
                    <td>{ctx.state.render.contentConv(log.note)}</td>
                    <td>{ctx.state.render.renderDate(log.operation_date)}</td>
                </tr>
            ))}
        </tbody>
    </table>
);

// Migrated from admin_log.pug.
export const moderation: HtmlTmpl<{
    logs: AdminLogDAO[],
    authorId: number, // Can be zero or NaN for all users.
    userName: string | undefined,
    adminId: number, // Can be zero or NaN for all admins.
    pagination: PaginationParams,
    all: boolean,
    isSelf: boolean
} & GlobalMaps> = (ctx, self) => (
    <div class="container">
        <h1>管理操作记录</h1>
        <p class="lead">
            {(self.authorId > 0) ? (
                self.isSelf ? (
                    '此处列出站长或资深用户对你的内容进行的管理操作记录。'
                ) : (
                    <>此处列出站长或资深用户对 <a href={'/author/' + self.authorId}>{self.userName}</a> 的内容进行的操作记录。</>
                )
            ) : (
                <>
                    此处列出
                    {(self.adminId > 0) ? (
                        <> <a href={'/author/' + self.adminId}>{self.userName}</a> </>
                    ) : null}
                    进行的管理操作记录。由程序自动判定为广告并隐藏者不在此列。
                </>
            )}
            请参见<a href="/rules">板规</a>；若有任何疑问可以<a href="/modmail">联系站长咨询或申诉</a>。
        </p>
        {(self.isSelf && ctx.state.mazoLoginUser!.blocked) ? (
            <div class="alert alert-warning my-4">
                你已被封禁，但你可<a href="/modmail">联系站长</a>要求解封。首次被封禁者皆可很快被解封。
            </div>
        ) : null}
        <div class="my-4">
            {adminLogTable(ctx, self)}
        </div>
        {(self.pagination.maxPage > 1) ? pagination(ctx, self.pagination) : null}
        <div class="text-center">
            <div class="form-check form-check-inline">
                <input
                    id="hide-spam"
                    type="checkbox"
                    checked={!self.all}
                    class="form-check-input"
                />
                <label for="hide-spam" class="form-check-label">
                    隐藏将<i>被系统误标记为广告帖的主题</i>手动恢复显示的操作
                </label>
            </div>
        </div>
        <script type="text/javascript">{elements.safeHtml(`document.getElementById("hide-spam").addEventListener("change",function(){var e=window.location,a=e.search,l="all="+(this.checked?0:1),n=a?a.indexOf("all=")>=0?a.replace(/all=\d+/,l):a+"&"+l:"?"+l;e.replace(e.pathname+("?all=0"===n?"":n))});`)}</script>
    </div>
);