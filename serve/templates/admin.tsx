import * as elements from 'typed-xhtml';
import { HtmlTmpl, styled } from '../utils/tsx_utils';
import { TagLite } from '../shared/tags';

const coreAdminSection: HtmlTmpl<void> = ctx => (
    <>
        <h1 class="mb-4">Core section</h1>
        <div class="card mb-3">
            <div class="card-header">
                <ul class="nav nav-tabs card-header-tabs">
                    <li class="nav-item">
                        <a href="#" data-lfile="error" class="nav-link">
                            stderr
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" data-lfile="out" class="nav-link">
                            stdout
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" data-lfile="ncrawl-error" class="nav-link">
                            crawl-err
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" data-lfile="ncrawl-out" class="nav-link">
                            crawl-out
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" data-lfile="proxy-error" class="nav-link">
                            proxy-err
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" data-lfile="proxy-out" class="nav-link">
                            proxy-out
                        </a>
                    </li>
                </ul>
            </div>
            <div id="lfile-target" class="card-body">
                选择一标签加载服务器进程日志
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                <ul class="nav nav-tabs card-header-tabs">
                    <li class="nav-item">
                        <a href="#" data-sstat="user" class="nav-link">
                            用户注册
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" data-sstat="thread" class="nav-link">
                            主题（thread）
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" data-sstat="post" class="nav-link">
                            文章（post）
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" data-sstat="fav" class="nav-link">
                            收藏
                        </a>
                    </li>
                </ul>
            </div>
            <div id="sstat-target" class="card-body">
                选择一标签加载图表
            </div>
        </div>
        <script type="text/javascript">{elements.safeHtml(`initAdminDashCore();`)}</script>
    </>
);

// Migrated from admin_dash.pug.
export const adminDash: HtmlTmpl<void> = ctx => (
    <div class="container">
        <h1 class="mb-4">Dashboard</h1>
        <div class="row">
            <div class="col-sm-8 col-lg-9 row">
                <div class="col-sm-6 mb-4">
                    <div class="card">
                        <div class="card-header">用户</div>
                        <div id="card-users" class="card-body">
                            等待加载…
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 mb-4">
                    <div class="card">
                        <div class="card-header">新主题（thread）</div>
                        <div id="card-threads" class="card-body">
                            等待加载…
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 mb-4">
                    <div class="card">
                        <div class="card-header">新文章（post）</div>
                        <div id="card-posts" class="card-body">
                            等待加载…
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 mb-4">
                    <div class="card">
                        <div class="card-header">新收藏</div>
                        <div id="card-favs" class="card-body">
                            等待加载…
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-lg-3 mb-4">
                <div class="card">
                    <div class="card-header">资深用户</div>
                    <div id="card-admins" class="card-body">
                        等待加载…
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">{elements.safeHtml(`initAdminDash();`)}</script>
        {(ctx.state.mazoLoginUser?.admin) ? coreAdminSection(ctx) : null}
    </div>
);

// Migrated from admin_guide.pug.
export const adminGuide: HtmlTmpl<void> = ctx => 
    styled(`#guide button { margin: 0 4px; }`,
        <div id="guide" class="container">
            <h1 class="mb-4">管理操作指南</h1>
            <div class="alert alert-warning">
                请阅读以下内容了解操作标准，并克制使用管理权限。滥用者将被永久裭夺相关用户等级/身份。
            </div>
            <div class="alert alert-warning">
                所有管理操作只适用于明确违反<a href="/rules">板规</a>
                的情况。单纯的离题/存在争议/不够友好/性癖不符/意见不合，或被他人单方面指控侵权/违法等，不能成为执行操作的理由。
            </div>
            <div>
                <h2 class="mt-4 mb-3">主题帖管理操作</h2>点击主题帖底部的
                <button class="btn btn-secondary btn-sm">管理操作</button>
                即可进行操作。
                <div>
                    <h4 class="mt-4 mb-3">半隐藏/隐藏主题帖</h4>
                    <p class="fst-italic">
                        关于隐藏：默认设置下，被隐藏的主题帖将无法被看到，而半隐藏的主题帖将半透明显示。绝大多数用户使用的是默认设置。但用户可以于主题列表页底部
                        <button class="btn btn-secondary btn-sm">
                            <span class="material-icons">filter_list</span>
                        </button>
                        临时切换这项设置，或在「
                        <span class="list-group-item-action d-inline-block w-auto">
                            <span class="material-icons me-1">settings</span>选项
                        </span>
                        」菜单的「主题列表默认过滤」修改设置。
                    </p>
                    <ul>
                        <li>
                            纯粹的广告帖应当全隐藏操作；带有一部分分享或解说的广告/推广帖子可以半隐藏操作。对于具体的判定标准，请参见
                            <a href="/rules">板规</a>。
                        </li>
                        <li>
                            对于长期在本站免费分享原创作品的用户可以适当宽容，对于新注册、从未有过任何贡献的用户应当更加严厉。
                        </li>
                    </ul>
                </div>
                <div>
                    <h4 class="mt-4 mb-3">移动主题帖</h4>
                    <ul>
                        <li>
                            将帖子移动至其它板块时，除了参考各板块标题，也请参照各板块已有的内容，来看是否类似。
                        </li>
                        <li>
                            小说文字区有一些较旧主题的标题或主楼可能是求助/交流格式，但下面楼层很快有贴出全文。这种情况可以让它留在小说文字区。
                        </li>
                        <li>
                            灌水区并不是垃圾桶或厨房水槽。若有更合适的板块，可以优先考虑移至对应板块，而不是移至灌水区。
                        </li>
                    </ul>
                    <h4 class="mt-4 mb-3">拆分主题帖</h4>
                    <p>
                        此功能目前没有方便易用的操作方式，需要站长手动执行。有亟需拆分的主题帖，请通知站长。
                    </p>
                </div>
                <div class="opacity-50">
                    <h4 class="mt-4 mb-3">
                        删除主题帖
                        <span class="badge bg-warning ms-2 mazomirror-tip" data-title="此功能较少被正常使用，为避免滥用，已暂停向资深用户开放；确有需要使用此权限者可联系站长">
                            已暂停
                        </span>
                    </h4>
                    <p>
                        被删除的主题帖将完全无法再被看到。执行删除主题帖时尽量谨慎。仅以下情况适合使用：
                    </p>
                    <ul>
                        <li>
                            发帖人发布明显的广告帖已经被处理过一次后，再（无论以同一用户，还是另开小号）重新发布的一模一样的广告。
                        </li>
                        <li>
                            发帖人将一模一样的主题帖同时发至多个板块。这种情况下，可以只保留其在最相关的板块下的一份，将其它直接删除（若还没有人回复）。但若其中的有主题帖已经得到他人回复，或可变通处理。
                        </li>
                    </ul>
                    <p class="fst-italic">
                        仅有发布在镜像的主题帖可以删除。在源站发布的主题帖无法删除，只能隐藏处理。
                    </p>
                </div>
            </div>
            <div class="opacity-50">
                <h2 class="mt-4 mb-3">
                    用户封禁操作
                    <span class="badge bg-warning ms-2 mazomirror-tip" data-title="此功能较少被正常使用，为避免滥用，已暂停向资深用户开放；确有需要使用此权限者可联系站长">
                        已暂停
                    </span>
                </h2>
                在作者个人资料页面点击
                <button class="btn btn-secondary btn-sm">封禁</button>按钮即可操作。
                <div />
                <p>仅限以下三种情况之一，可以封禁用户：</p>
                <ul>
                    <li>该用户注册后，除了广告之外，再没有发布过任何内容；</li>
                    <li>
                        该用户屡次被管理员执行隐藏/删除等操作后，依然进行一模一样的违规（例如将被删除的帖子一模一样地重新发出来）；
                    </li>
                    <li>有清晰而明确的证据表明该用户为其它已被封禁用户的小号/马甲。</li>
                </ul>
                <p class="fst-italic">
                    封禁仅可阻止用户在镜像发帖，无法阻止其在源站发帖。但被封禁后其在源站新发的主题帖，在镜像皆会自动隐藏。因此对于源站的广告机器人等情况，在镜像进行封禁依然有意义。
                </p>
            </div>
            <div class="opacity-50">
                <h2 class="mt-4 mb-3">
                    删除回帖操作
                    <span class="badge bg-warning ms-2 mazomirror-tip" data-title="此功能较少被正常使用，为避免滥用，已暂停向资深用户开放；确有需要使用此权限者可联系站长">
                        已暂停
                    </span>
                </h2>
                在回帖楼层右下角
                <button class="btn btn-secondary btn-sm">
                    <span class="material-icons">more_horiz</span>
                </button>
                按钮中选择「以管理员身份删除」即可操作。操作标准请参见下列例子：
                <table class="table">
                    <tbody>
                        <tr>
                            <th scope="row">无意义回复</th>
                            <td>
                                文字本身有含义，即使可能是复制粘贴的，无独到观点，也建议不作删除，例如以下例子：
                                <ul>
                                    <li>楼主牛逼</li>
                                    <li>感谢作者</li>
                                    <li>好人一生平安</li>
                                </ul>
                            </td>
                            <td>
                                文字本身毫无意义的，建议删除，例如以下例子：
                                <ul>
                                    <li>11111</li>
                                    <li>gooooo</li>
                                    <li>好asdf</li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">广告宣传</th>
                            <td>
                                有一定上下文，不是和主题帖完全无关、并非直接而单纯营利的，建议不作删除，例如以下例子：
                                <ul>
                                    <li>喜欢这类文章的可以到我的XXX论坛浏览，有很多类似帖子</li>
                                    <li>
                                        喜欢我上面楼层发的图片的话，请到我的
                                        <a href="javascript:void(0)">XXX主页</a>打赏支持
                                    </li>
                                    <li>
                                        楼主如果想要这类东西可以到我的
                                        <a href="javascript:void(0)">店铺</a>购买
                                    </li>
                                </ul>
                            </td>
                            <td>
                                单纯广告的，建议删除，例如以下例子：
                                <ul>
                                    <li>
                                        <a href="javascript:void(0)">{'http://example.top/?tg_id=foo'}</a>这里面什么你想要黄片的都有
                                    </li>
                                    <li>调教视频合计100部，加我QQ337845818购买</li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">不友善</th>
                            <td>
                                较为激烈的讨论，只要回复中包含有意义的内容，哪怕包含一些不友好内容，也建议不作删除，例如以下例子：
                                <ul>
                                    <li>楼主玩的这种东西真的恶心</li>
                                    <li>被洗脑的小将又破防了</li>
                                    <li>写的什么垃圾玩意儿</li>
                                </ul>
                            </td>
                            <td>
                                没有任何逻辑和上下文的单纯人身攻击，建议删除，例如以下例子：
                                <ul>
                                    <li>纯傻逼，滚</li>
                                    <li>你妈是被狗肏烂了才生出你这么个杂种玩意儿吧</li>
                                </ul>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <p class="fst-italic">
                    仅有发布在镜像的回帖可以删除。在源站发布的回帖无法删除。
                </p>
            </div>
            <h2 class="mt-4 mb-3">Dashboard</h2>
            <p>
                为方便群策群力、互相监督，镜像向各位开放了 <a href="/admin/dash">数据统计 dashboard</a> 以供参考。
            </p>
            <h2 class="mt-4 mb-3">反馈</h2>
            <p>
                有任何意见或建议，可以在<a href="/forum/1073741824">镜像板务/反馈区</a>发帖。
            </p>
        </div>
    );

export const adminTagAudit: HtmlTmpl<{
    vote: -1 | 0 | 1,
    date: number,
    thread: number,
    title: string,
    owner: { user_id: number, user_name: string },
    user: { user_id: number, user_name: string },
    tag: TagLite
}[]> = (ctx, rows) => (
    <div class="container">
        <h1 class="mb-4">标签操作（最近 {rows.length} 个）</h1>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">主题</th>
                    <th scope="col">主题作者</th>
                    <th scope="col">标签操作者</th>
                    <th scope="col">日期</th>
                    <th scope="col" colspan="2">标签投票</th>
                </tr>
            </thead>
            <tbody>
                {rows.map(row => (
                    <tr>
                        <td><a href={'/thread/' + row.thread}>{ctx.state.render.contentConv(row.title)}</a></td>
                        <td><a href={'/author/' + row.owner.user_id}>{row.owner.user_name}</a></td>
                        <td><a href={'/author/' + row.user.user_id}>{row.user.user_name}</a></td>
                        <td>{ctx.state.render.renderDate(row.date)}</td>
                        <td><a href={'/tag/' + row.tag.tag_id}>{ctx.state.render.staticConv(row.tag.primary_name)}</a></td>
                        <td>
                            {(row.vote > 0) ? (
                                <span class="material-icons align-middle">thumb_up_alt</span>
                            ) : (row.vote  < 0) ? (
                                <span class="material-icons align-middle">thumb_down_alt</span>
                            ) : '已撤回'}
                        </td>
                    </tr>
                ))}
            </tbody>
        </table>
    </div>
);
