import { query as sql } from '../db';
import { LOGIN_REQUIRED, PageImpl, error, finalizePage } from '../utils/render_utils';
import * as login from '../shared/login';

import * as templates from '../templates/root';
import { logSpecialRequest } from '../utils/logic_utils';

export const adminPassword: PageImpl = async ctx => {
    const loginUser = ctx.state.mazoLoginUser;
    if (!loginUser) { throw error(403, LOGIN_REQUIRED); }
    if (loginUser.id !== login.ROOT_USER) { throw error(403, '仅站长可使用此功能。'); }

    return finalizePage(ctx, { 
        title: ctx.state.render.staticConv('密码重设'),
        nav: false
    }, templates.adminPassword(ctx, { nearMatches: null, updated: null }));
};

export const postAdminPassword: PageImpl = async ctx => {
    const loginUser = ctx.state.mazoLoginUser;
    if (!loginUser) { throw error(403, LOGIN_REQUIRED); }
    if (loginUser.id !== login.ROOT_USER) { throw error(403, '仅站长可使用此功能。'); }

    const username = ctx.request.body.string('username');
    const email = ctx.request.body.string('email').toLowerCase();
    const password = ctx.request.body.string('password');

    if (!username || !email || !password) { throw error(400, '部分字段未填写～'); }
    const tryExactMatch = (await sql('UPDATE users SET password_h = SHA224(CONVERT_TO($1 || \'-O-\' || $2, \'UTF8\')) WHERE password_h IS NOT NULL AND email = $2 AND LOWER(user_name) = LOWER($3) RETURNING user_id', [password, email, username])).rows[0];

    let updated: { user_id: number, user_name: string, password: string } | null = null;
    let nearMatches: { user_id: number, user_name: string, has_password: boolean, email: string }[] | null = null;
    if (tryExactMatch) {
        logSpecialRequest(ctx, 'admin-pwchg', { user: tryExactMatch.user_id });
        updated = {
            user_id: tryExactMatch.user_id,
            user_name: username,
            password
        };
    } else {
        nearMatches = (await sql('SELECT user_id, user_name, password_h IS NOT NULL AS has_password, email FROM users WHERE LOWER(user_name) = LOWER($1) OR email = $2', [username, email])).rows;
    }

    return finalizePage(ctx, { 
        title: ctx.state.render.staticConv('密码重设'),
        nav: false
    }, templates.adminPassword(ctx, { nearMatches, updated }));
};
