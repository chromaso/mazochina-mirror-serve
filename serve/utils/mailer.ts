import { inspect } from 'util';

import mailjet = require('node-mailjet');
import brevo = require('@getbrevo/brevo');

import { escapeHtml } from '../../common/dependencies';
import { mailjetKey, sendInBlueKey } from '../conf';
import { RateLimiter } from './rate_limit';

type Sender = (from: string, title: string, text: string, html: string, destination: string, destinationName: string) => Promise<unknown>;

const mailJetClient = mailjet.connect('9503ff270d40487d8f8c912ddda1a780', mailjetKey);

export const cleanupMailjetContacts = async () => {
  const result = await mailJetClient.get('contact', { 'version': 'v3' }).request({ Limit: 10 });
  const contactIds = result.body.Data.map(single => (single as any).ID);
  console.log(`[Mailjet cleanup] Contacts ${contactIds.join(', ')}`);
  for (const contact of contactIds) {
    await mailJetClient.delete('contact', { 'version': 'v4' }).id(contact).request();
  }
  console.log(`[Mailjet cleanup] Done with ${contactIds.length} contacts.`);
};

export const sendMailViaMailjet: Sender = (from, title, text, html, destination, destinationName): Promise<unknown> => {
  if (Math.random() < 0.125) {
    // Not using it now because of https://github.com/mailjet/mailjet-apiv3-nodejs/issues/282.
    // unawaited(cleanupMailjetContacts(), 'mailjet cleanup');
  }
  return mailJetClient.post('send', { 'version': 'v3.1' }).request({
    'Messages': [{
      'From': {
        'Email': from + '@mirror.chromaso.net',
        'Name': 'M系镜像（M）'
      },
      'To': [
        {
          'Email': destination,
          'Name': destinationName
        }
      ],
      'Subject': title,
      'TextPart': text,
      'HTMLPart': html
    }]
  });
};

const sendInBlueClient = new brevo.TransactionalEmailsApi();
sendInBlueClient.setApiKey(brevo.TransactionalEmailsApiApiKeys.apiKey, sendInBlueKey);

export const sendMailViaSIB: Sender = (from, title, text, html, destination, destinationName): Promise<unknown> => {
  const sendSmtpEmail = new brevo.SendSmtpEmail();
  sendSmtpEmail.subject = title;
  sendSmtpEmail.textContent = text;
  sendSmtpEmail.htmlContent = html;
  sendSmtpEmail.sender = { name: 'M系镜像（B）', email: from + '@mirror.chromaso.net' };
  sendSmtpEmail.to = [{ email: destination, name: destinationName }];
  return sendInBlueClient.sendTransacEmail(sendSmtpEmail);
};

type EmailParams = {
  email: string;
  userName: string;
  code: string;
  conv: (text: string) => string;
};

type EmailTemplate = (params: EmailParams, sender: (from: string, title: string, text: string, html: string, destination: string, destinationName: string) => Promise<unknown>) => Promise<unknown>;

const registrationHtmlTemplate = (userName: string, code: string, conv: (text: string) => string): string => `${escapeHtml(userName)}：<br />${conv('欢迎来到 M 系镜像！<br /><br />你的注册验证码是：<br /><b>')}${code}${conv('</b><br />请将此验证码填写至注册表单中。<br /><br />以下事项还请了解：<ul><li>请阅读本站<a href="https://mirror.chromaso.net/rules">章程与规则</a>。</li><li>若有任何故障和错误，可以直接<a href="https://mirror.chromaso.net/modmail">联系管理员</a>。</li><li>若有功能上的建议，可以在 <a href="https://mirror.chromaso.net/thread/1073741842">FAQ 帖子</a>下回复。</li><li>镜像在功能上还将继续完善，请多多支持！在镜像上积极发帖或是帮助宣传的话我将感激不尽。</li></ul>祝你在新家玩得愉快！')}`;

export const TEMPLATE_REGISTER: EmailTemplate = ({ email, userName, code, conv }, sender): Promise<unknown> => 
  sender('registration', conv('M系镜像：注册验证'), conv('注册验证码是 ') + code, registrationHtmlTemplate(userName, code, conv), email, userName);

const updateHtmlTemplate = (userName: string, code: string, conv: (text: string) => string): string => `${escapeHtml(userName)}：<br />${conv('你正在试图修改你在 M 系镜像的绑定邮箱或密码，你的验证码是：<br /><b>')}${code}${conv('</b><br />请将此验证码填写至先前页面的表单中。<br /><br />若有任何疑问，请 <a href="https://mirror.chromaso.net/modmail">联系管理员</a>。')}`;

export const TEMPLATE_UPDATE: EmailTemplate = ({ email, userName, code, conv }, sender): Promise<unknown> =>
  sender('registration', conv('M系镜像：用户信息修改'), conv('你正在试图修改绑定邮箱或密码，验证码是 ') + code, updateHtmlTemplate(userName, code, conv), email, userName);

let serial = (Date.now() / 600000) | 0;

// Return true if sent.
export const sendMail = async (params: EmailParams, template: EmailTemplate): Promise<boolean> => {
  const traceId = ++serial;
  console.log(`[Mailer ${traceId}] Attempting to send to ${params.email}`);
  const implementation = (Math.random() > 0.65) ? sendMailViaMailjet : sendMailViaSIB;
  try {
    const result = await template(params, implementation);
    console.log(`[Mailer ${traceId}] ${implementation.name} to ${params.email} OK w/ response ${inspect(result).substr(0, 256)}`);
    return true;
  } catch (e) {
    console.log(`[Mailer ${traceId}] Failed via ${implementation.name}`, e);
    throw e;
  }
};

export const registrationAttemptCounterByEmail = new RateLimiter<string>(1, 60);