import { QueryResult } from 'pg';

import { SPAM_MANUAL, SPAM_THRESHOLD, THRESHOLD_FOR_LOCAL } from '../../common/consts';
import { rowsToMap } from '../../common/logic_utils';
import { redoBBCodeOfThread } from '../utils/bbcode_offline';
import { query as sql } from '../db';
import { allContestResults, META_THREAD_ID as CAMPAIGN_META_THREAD_ID, CAMPAIGN_REVIEW_DISPLAY_ENABLED, loadContest192Json, REVIEW_CAMPAIGN_TAG_ID } from '../shared/contest_result';
import { escapeForIast } from '../shared/iast';
import { AdminLogDAO, prepareAdminLogTableRenderParams } from '../shared/moderation';
import { processContent } from '../shared/post';
import { allTagsById, F18_CONSTS, tagPref } from '../shared/tags';
import { getIastDefaultOptions, logSpecialRequest, myFirstPostInThread, parseInt32, TREAT_AS_READ_BEFORE } from '../utils/logic_utils';
import { forumsCache } from '../shared/forum';
import { error, finalizePage, finalizeSegment, HtmlSegmentImpl, LOGIN_REQUIRED, PageImpl, processLocalContent, redirectWithDelay, redirectWithoutDelay, RpcImpl } from '../utils/render_utils';
import { originalRemoteUrl, originalReplyUrl } from '../utils/src_post';

import * as contestResultTemplates from '../templates/contest_result';
import * as templates from '../templates/thread';
import { dualAvatar } from '../utils/avatar';

export const thread_id_page: PageImpl = async ctx => {
    const threadId = parseInt32(ctx.params.id);
    let page = parseInt32(ctx.params.page);
    if (isNaN(threadId)) { throw error(400, '不要乱输入网址嘛┭┮﹏┭┮'); }
    if (isNaN(page) || page <= 0) { page = 1; }
    const thread = (await sql<{ forum_id: number, remote_forum_id: number, thread_title: string, poll_title: string, owner_user_id: number, successive_fail: number, spam_likeness: number, flags: number }>('SELECT forum_id, remote_forum_id, thread_title, poll_title, owner_user_id, successive_fail, spam_likeness, flags FROM thread_targets WHERE thread_id = $1', [threadId])).rows[0];
    if (!thread) { throw error(404, '这个主题……是不存在的呢(；′⌒`)'); }

    const userInfo = ctx.state.mazoLoginUser;
    const warnings: string[] = [];
    const canAdminThread = userInfo ? (userInfo.admin || userInfo.confirmed) : false;
    if (thread.successive_fail < 0) {
        if (thread.flags & 0x10) {
            const extras = (await sql('SELECT extra_information FROM local_admin_log WHERE target_type = \'thread\' AND target_id = $1 AND (extra_information ? \'thread\') ORDER BY admin_log_id DESC LIMIT 1', [threadId])).rows[0];
            if (!extras) { throw error(500, '此主题已被合并入另一主题，但由于未知错误未能找到其合并至的主题。'); }
            const newThreadId = extras.extra_information.thread;
            let targetHref = '/thread/' + newThreadId;
            const firstPostId = (extras.extra_information.posts as number[]).sort((a, b) => a - b)[0];
            const firstPost = firstPostId ? (await sql<{ thread_id: number, most_recent_update: number }>('SELECT thread_id, most_recent_update FROM posts WHERE post_id = $1', [firstPostId])).rows[0] : undefined;
            if (firstPost && (firstPost.most_recent_update >= 0) && (firstPost.thread_id === newThreadId)) { // The first post being moved still exists, and is still in the target thread! This should be the case most of the time.
                targetHref = '/post/' + firstPostId;
            }
            return redirectWithDelay(ctx, {
                targetHref,
                targetText: '合并至的主题',
                message: '此主题已被合并入另一主题'
            });
        }
    
        if (userInfo?.admin) {
            warnings.push('这个主题已经被删除，不会公开显示。');
        } else {
            throw error(404, '这个主题……已经被删除了！');
        }
    }
    if (thread.spam_likeness > SPAM_THRESHOLD) {
        const manuallyHidden = thread.spam_likeness === SPAM_MANUAL;
        if (manuallyHidden && !ctx.state.mazoLoginUser) {
            throw error(404, '这个主题……已经被隐藏了！');
        }
        warnings.push(manuallyHidden ? '这个主题已经被管理员隐藏。' : `这个主题被自动判定为广告${canAdminThread ? `（置信度：${thread.spam_likeness}）` : ''}并隐藏。`);
    } else if (thread.spam_likeness === SPAM_THRESHOLD) {
        warnings.push('这个主题被管理员标注为广告并半隐藏。');
    }

    let postsRequest: Promise<QueryResult<{ post_id: number, title: string, user_id: number, user_name: string, content: string, flags: number, posted_date: number, rev_id: number, most_recent_update: number }>>, countTotal: Promise<QueryResult<{ post_count: number }>>;
    const authorId = parseInt32(`${ctx.query.show || ''}`);
    if (!isNaN(authorId)) {
        postsRequest = sql('SELECT post_id, title, user_id, user_name, content, flags, posted_date, rev_id, most_recent_update FROM posts WHERE thread_id = $1 AND posts.user_id = $2 AND posts.most_recent_update >= 0 ORDER BY posted_date ASC, post_id ASC LIMIT $3 OFFSET $4', [threadId, authorId, ctx.state.userConfig.tp, (page - 1) * ctx.state.userConfig.tp]);
        countTotal = sql('SELECT COUNT(post_id)::INT as post_count FROM posts WHERE thread_id = $1 AND user_id = $2 AND posts.most_recent_update >= 0', [threadId, authorId]);
    } else {
        postsRequest = sql('SELECT post_id, title, user_id, user_name, content, flags, posted_date, rev_id, most_recent_update FROM posts WHERE thread_id = $1 AND posts.most_recent_update >= 0 ORDER BY posted_date ASC, post_id ASC LIMIT $2 OFFSET $3', [threadId, ctx.state.userConfig.tp, (page - 1) * ctx.state.userConfig.tp]);
        countTotal = sql('SELECT COUNT(post_id)::INT as post_count FROM posts WHERE thread_id = $1 AND posts.most_recent_update >= 0', [threadId]);
    }
    const posts = (await postsRequest).rows;
    if (!posts.length) { throw error(400, '似乎没有任何在这里的文章呢～'); }

    const authorInfoRequest = sql<{ user_id: number, user_name: string, email: string | null, avatar: number, label: string | null }>('SELECT user_id, user_name, email, avatar, label FROM users WHERE user_id = ANY($1::INT[])', [Array.from(new Set(posts.map(post => post.user_id)))]);

    const total = (await countTotal).rows[0].post_count;
    const maxPage = Math.ceil(total / ctx.state.userConfig.tp);

    const userOrVisitorId = userInfo ? userInfo.id : ctx.state.mazoGuest?.id;
    const readStatusRequest = userOrVisitorId? sql<{ up_to_date: number }>('SELECT up_to_date FROM unread_status WHERE user_id = $1 AND thread_id = $2', [userOrVisitorId, threadId]) : null;
    const starStatusRequest = userInfo ? sql<{ star_type: number }>('SELECT star_type, fc_ids FROM thread_stars WHERE user_id = $1 AND thread_id = $2', [userInfo.id, threadId]) : null;

    const parentForum = (await forumsCache).get(thread.forum_id);
    const iHaveReplied = posts.some(post => (post.flags & 2)) ? !!(await myFirstPostInThread(userInfo, threadId)) : false;
    const authorInfos = new Map((await authorInfoRequest).rows.map(row => [row.user_id, row]));

    let iastSelections: Record<string, unknown> | undefined;
    if (posts.some(post => (post.flags & 4))) {
        const [defaultValues, myIastValues] = await Promise.all([
            getIastDefaultOptions(threadId, userInfo?.name),
            userInfo ? sql<{ choices: { [key: string]: string } }, [number, number]>('SELECT choices FROM iast_choices WHERE thread_id = $1 AND user_id = $2', [threadId, userInfo.id]).then(({ rows }) => rows[0]) : undefined
        ]);
        const iastSelectionOutput: { [key: string]: [string, string?] } = {};
        Object.keys(defaultValues).forEach(key => {
            const meta = defaultValues[key];
            const iHaveSelected = myIastValues?.choices.hasOwnProperty(key);
            if (typeof meta[2] === 'string') { // type=cnm || type=text
                if (iHaveSelected) {
                    const value = myIastValues!.choices[key]; // Use the user input as-is, without conversion.
                    iastSelectionOutput[key] = [escapeForIast(value), value];
                } else {
                    const converted = ctx.state.render.contentConv(meta[0]); // contentConv on both explicit and implicit defaults.
                    iastSelectionOutput[key] = meta[1] ? [escapeForIast(meta[0]), converted] : ['', converted];
                }
            } else { // type=select
                iastSelectionOutput[key] = [iHaveSelected ? myIastValues!.choices[key] : meta[0]]; // Could be empty string ''.
            }
        });
        iastSelections = iastSelectionOutput;
    }

    let remoteForumName: string | undefined;
    if ((threadId <= THRESHOLD_FOR_LOCAL) && (thread.forum_id !== thread.remote_forum_id)) {
        const remoteForum = (await forumsCache).get(thread.remote_forum_id);
        remoteForumName = remoteForum ? remoteForum.forum_name : '[UNKNOWN]';
    }

    let originalThread: { id: number, title: string } | undefined;
    if (thread.flags & 0x8) {
        const originalThreadId = (await sql('SELECT target_id FROM local_admin_log WHERE target_type = \'thread\' AND extra_information @> JSONB_BUILD_OBJECT(\'thread\', $1::INT)', [threadId])).rows[0]?.target_id;
        originalThread = originalThreadId ? { id: originalThreadId, title: (await sql('SELECT thread_title FROM thread_targets WHERE thread_id = $1', [originalThreadId])).rows[0]?.thread_title ?? '[UNKNOWN]' } : { 'id': 0, title: '[UNKNOWN]' };
    }

    let tags: { id: number, name: string, full: boolean, warn: boolean }[] | undefined;
    let campaignThread = false;
    if (thread.forum_id === 18) {
        const userTagPref = await tagPref(ctx);
        const selectedF18Tags = (await sql<{ tag_id: number, score: number }>('SELECT tag_id, tag_score & ~3 AS score FROM tag_association WHERE thread_id = $1 AND tag_score > 0 ORDER BY tag_score DESC', [threadId])).rows;
        const tagsById = await allTagsById();
        tags = selectedF18Tags.map(row => {
            const tag = tagsById.get(row.tag_id)!;
            if ((row.score > F18_CONSTS.CONSTS.SCORE_MULTIPLIER) && (tag.tag_id === F18_CONSTS.VOTE_CAMPAIGN_TAG_ID)) {
                campaignThread = true;
            }
            return {
                id: tag.tag_id,
                name: tag.primary_name,
                full: row.score > F18_CONSTS.CONSTS.SCORE_MULTIPLIER,
                warn: userTagPref.warning.has(tag.tag_id),
                sortOrder: row.score /* integer */ - (tag.category / 16) + (tag.flags / 32768) // Use a determinstic sort order.
            } as const;
        }).sort((a, b) => b.sortOrder - a.sortOrder);
    }

    let contestResultHtml: SafeString | null = null;
    if (CAMPAIGN_REVIEW_DISPLAY_ENABLED) {
        if (threadId === CAMPAIGN_META_THREAD_ID) {
            contestResultHtml = contestResultTemplates.contestResults(ctx, { contestResults: await allContestResults(ctx) });
        } else if (tags?.some(tag => tag.id === REVIEW_CAMPAIGN_TAG_ID)) {
            contestResultHtml = contestResultTemplates.contestResult(ctx, { contestResult: (await loadContest192Json())[threadId] });
        }
    }

    return finalizePage(ctx, {
        title: ctx.state.render.contentConv(thread.thread_title),
        nav: true,
        canonicalPath: '/thread/' + threadId + ((page > 1) ? '/' + page : ''),
        deps: canAdminThread ? ['ADMIN_JS'] : undefined
    }, templates.thread(ctx, {
        isLocal: threadId > THRESHOLD_FOR_LOCAL,
        remoteForumName,
        title: thread.thread_title,
        tags,
        campaign: campaignThread && !!userInfo,
        poll: thread.poll_title || null,
        warnings,
        isSpam: thread.spam_likeness >= SPAM_THRESHOLD,
        canAdminThread,
        authorOnly: isNaN(authorId) ? null : authorInfos.get(authorId)!.user_name,
        isMyThread: userInfo ? (thread.owner_user_id === userInfo.id) : false,
        threadId,
        pagination: {
            currentPage: page,
            linkExtra: ctx.querystring ? '?' + ctx.querystring : '',
            baseLink: '/thread/' + threadId,
            maxPage,
        },
        originalLink: originalRemoteUrl(thread.forum_id, threadId),
        originalReplyLink: originalReplyUrl(thread.forum_id, threadId),
        parent: {
            name: parentForum ? parentForum.forum_name : '[UNKNOWN]',
            link: '/forum/' + thread.forum_id
        },
        iHaveReplied,
        iastSelections,
        readToDate: readStatusRequest ? await readStatusRequest.then(({ rows }) => rows.length ? rows[0].up_to_date : 0) : 0,
        starStatus: starStatusRequest ? await starStatusRequest.then(({ rows }) => rows.length ? (rows[0].star_type & 0xf) : 0) : -1, // of type StarStatusInitJson.
        posts: posts.map(row => {
            const authorInfo = ((row.user_id === 0) && row.user_name) ? { user_id: row.user_id, user_name: row.user_name, email: null, avatar: 0, label: null } : authorInfos.get(row.user_id)!;
            return {
                authorId: row.user_id,
                authorLabel: authorInfo.label,
                authorName: authorInfo.user_name,
                authorAvatar: dualAvatar(authorInfo.user_id, authorInfo.user_name, authorInfo.email, authorInfo.avatar),
                edited: (row.rev_id && (row.most_recent_update > row.posted_date)) ? row.most_recent_update : null,
                id: row.post_id,
                isLocal: row.post_id > THRESHOLD_FOR_LOCAL,
                title: row.title,
                content: row.rev_id ? processLocalContent(row.content) : processContent(row.content, ctx.state.render.renderDate),
                isHidden: !!(row.flags & 2),
                date: row.posted_date,
                replyLink: '/post/p' + row.post_id,
                originalLink: (row.post_id > THRESHOLD_FOR_LOCAL) ? null : originalRemoteUrl(thread.forum_id, threadId, row.post_id),
                authorOnlyLink: '/thread/' + threadId + '?show=' + encodeURIComponent(row.user_id)
            };
        }),
        showReplyWarning: !!(thread.flags & 0x2),
        official: !!(thread.flags & 0x4),
        originalThread,
        contestResultHtml
    }));
};

export const thread_id_heading_heading: PageImpl = async ctx => {
    const threadId = parseInt32(ctx.params.id);
    if (isNaN(threadId)) { throw error(400, '不要乱输入网址嘛┭┮﹏┭┮'); }
    const heading = ctx.params.heading;
    if (!heading) { throw error(400, '未指定跳转目标名称～'); }

    const meta = (await sql('SELECT post_id, most_recent_update FROM posts WHERE thread_id = $1 AND xrefs->\'t\' ? $2', [threadId, heading])).rows[0];
    if (!meta) { throw error(404, '嗯？没有这样的文章哟～'); }
    if (meta.most_recent_update < 0) { throw error(404, '很不幸地通知您……这篇文章已经被删除了……'); }

    const index = (await sql('WITH summary AS (SELECT post_id, (ROW_NUMBER() OVER (ORDER BY posted_date ASC, post_id ASC))::INT AS position FROM posts WHERE thread_id = $1 AND most_recent_update >= 0) SELECT position FROM summary WHERE post_id = $2', [threadId, meta.post_id])).rows[0];

    const pageNumber = Math.floor((index.position - 1) / ctx.state.userConfig.tp) + 1;
    return redirectWithoutDelay(ctx, '/thread/' + threadId + '/' + pageNumber + '#h-' + heading);
};

export const apiThread_id: HtmlSegmentImpl = async ctx => {
    const userInfo = ctx.state.mazoLoginUser;
    if (!userInfo) { throw error(403, LOGIN_REQUIRED); }

    const threadId = parseInt32(ctx.params.id);
    if (isNaN(threadId)) { throw error(400, '不要乱输入网址嘛┭┮﹏┭┮'); }
    const thread = (await sql<{ owner_user_id: number, successive_fail: number }>('SELECT owner_user_id, successive_fail FROM thread_targets WHERE thread_id = $1', [threadId])).rows[0];
    if (!thread) {
        throw error(404, '这个主题……是不存在的呢(；′⌒`)'); 
    }
    const canAdminThread = userInfo && (userInfo.admin || userInfo.confirmed);
    if ((thread.successive_fail < 0) && !canAdminThread) {
        throw error(404, '这个主题……已经被删除了！');
    }
    const limitedPermission = (thread.owner_user_id !== userInfo.id) && !canAdminThread;

    const [yearly, monthly, weekly] = (await Promise.all(
        [365, 30, 7].map(days => 
            sql<{ user_id_sgn: number, cnt: number }>('SELECT SIGN(user_id) AS user_id_sgn, COUNT(user_id)::INT AS cnt FROM unread_status WHERE tracked_at > $2 AND thread_id = $1 GROUP BY user_id_sgn', [threadId, ctx.refDate - days * 86400])
        )
    )).map(({ rows }) => {
        const map = rowsToMap(rows, 'user_id_sgn');
        const loggedIn = map.get(1);
        const visitors = map.get(-1);
        return [
            loggedIn ? loggedIn.cnt : 0,
            visitors ? visitors.cnt : 0
        ] as const;
    })

    const [cntFavs, listRecentReads, listPublicFavs, adminOpsParams] = await Promise.all([
        limitedPermission ? Promise.resolve(undefined) : sql<{ cnt: number }>('SELECT COUNT(user_id)::INT AS cnt FROM thread_stars WHERE star_type <> 0 AND star_at > 0 AND thread_id = $1', [threadId]),
        limitedPermission ? Promise.resolve(undefined) : sql<{ user_id: number, user_name: string }>('SELECT user_id, user_name FROM unread_status LEFT JOIN users USING (user_id) WHERE thread_id = $1 ORDER BY tracked_at DESC LIMIT 10', [threadId]),
        sql<{ user_name: string, title: string, fc_id: number }>('WITH stars AS (SELECT UNNEST(fc_ids) AS fc_id, user_id FROM thread_stars WHERE thread_id = $1) SELECT user_name, title, fc_id FROM stars INNER JOIN users USING (user_id) INNER JOIN fav_collections USING (fc_id) WHERE default_star_type & 16 = 0', [threadId]),
        sql<AdminLogDAO>(`SELECT admin_log_id, operation_date, admin_user_id, target_id, target_type, note, extra_information, flags FROM local_admin_log WHERE (target_type = 'thread' AND target_id = $1) OR (target_type = 'post' AND (SELECT thread_id FROM posts WHERE post_id = target_id) = $1) ORDER BY admin_log_id DESC`, [threadId]).then(({ rows }) => (rows.length ? prepareAdminLogTableRenderParams(rows) : null))
    ] as const);

    return finalizeSegment(ctx, templates.threadStats(ctx, {
        adminStats: adminOpsParams,
        readerStats: {
            limitedPermission,
            yearly, monthly, weekly,
            favs: cntFavs?.rows[0].cnt ?? 0,
            recentReads: listRecentReads?.rows,
            publicFavs: listPublicFavs.rows,
        }
    }));
};

export const post_id: PageImpl = async ctx => {
    let postId: number;

    if (ctx.params.id.charAt(0) === 't') { // Go to the first unread post in a thread.
        const threadId = parseInt32(ctx.params.id.substr(1));
        if (isNaN(threadId)) { throw error(400, '啊咧……你是怎么到这里的？真的没有乱改网址吗？'); }

        const userInfo = ctx.state.mazoLoginUser;
        if (!userInfo) {
            return redirectWithDelay(ctx, {
                message: '你未登入，所以不知道你看到了哪里～',
                targetText: '该主题的第一页',
                targetHref: '/thread/' + threadId
            });
        }
        const unreadRow = (await sql<{ up_to_date: number }, [number, number]>('SELECT up_to_date FROM unread_status WHERE thread_id = $1 AND user_id = $2', [threadId, userInfo.id])).rows[0];
        if (!unreadRow) {
            return redirectWithoutDelay(ctx, '/thread/' + threadId);
        }
        const postIdRow = (await sql<{ post_id: number }, [number, number]>('SELECT COALESCE((SELECT post_id FROM posts WHERE thread_id = $1 AND most_recent_update >= 0 AND posted_date > $2 ORDER BY posted_date ASC, post_id ASC LIMIT 1), (SELECT post_id FROM posts WHERE thread_id = $1 AND most_recent_update >= 0 ORDER BY posted_date DESC, post_id DESC LIMIT 1)) AS post_id', [threadId, unreadRow.up_to_date])).rows[0];
        if (!postIdRow) {
            throw error(404, '这个主题……是不存在的呢(；′⌒`)');
        }
        postId = postIdRow.post_id;
    } else {
        postId = parseInt32(ctx.params.id);
        if (isNaN(postId)) { throw error(400, '啊咧……你是怎么到这里的？真的没有乱改网址吗？'); }
    }

    const meta = (await sql('SELECT thread_id, most_recent_update FROM posts WHERE post_id = $1', [postId])).rows[0];
    if (!meta) { throw error(404, '嗯？没有这样的文章哟～'); }
    if (meta.most_recent_update < 0) { throw error(404, '很不幸地通知您……这篇文章已经被删除了……'); }
    const index = (await sql('WITH summary AS (SELECT post_id, (ROW_NUMBER() OVER (ORDER BY posted_date ASC, post_id ASC))::INT AS position FROM posts WHERE thread_id = $1 AND most_recent_update >= 0) SELECT position FROM summary WHERE post_id = $2', [meta.thread_id, postId])).rows[0];

    const pageNumber = Math.floor((index.position - 1) / ctx.state.userConfig.tp) + 1;
    return redirectWithoutDelay(ctx, '/thread/' + meta.thread_id + '/' + pageNumber + '#p' + postId);
};


type ApiReadResponse = { total: number, owner?: number, star?: number };

export const apiRead_id: RpcImpl = async ctx => {
    const threadId = parseInt32(ctx.params.id);
    if (isNaN(threadId)) { throw error(400, '不要乱输入网址嘛┭┮﹏┭┮'); }
    const userInfo = ctx.state.mazoLoginUser;
    if (!userInfo) { throw error(403, LOGIN_REQUIRED); }
    const [readHistory, threadRow, threadStars] = await Promise.all([
        sql(`SELECT up_to_date FROM unread_status WHERE thread_id = $1 AND user_id = $2`, [threadId, userInfo.id]),
        sql(`SELECT owner_user_id, lastpost_date FROM thread_targets WHERE thread_id = $1`, [threadId]),
        sql(`SELECT star_type FROM thread_stars WHERE thread_id = $1 AND user_id = $2`, [threadId, userInfo.id])
    ].map(p => p.then(q => q.rows[0])));
    if (!threadRow) {
        throw error(404, '主题不存在的～');
    }
    let out: ApiReadResponse;
    if (!readHistory) {
        out = {
            total: (threadRow.lastpost_date > TREAT_AS_READ_BEFORE) ? -1 : -2
        }; // -1 means "all new".
    } else {
        const countByGroup = (await sql('SELECT COUNT(post_id)::INT AS post_count, (user_id <> $1) AS is_non_owner FROM posts WHERE thread_id = $2 AND posted_date > $3 GROUP BY is_non_owner', [threadRow.owner_user_id, threadId, readHistory.up_to_date])).rows;
        out = { total: countByGroup.map(result => result.post_count).reduce((a, b) => a + b, 0) };
        const fromOp = countByGroup.find(row => row.is_non_owner === false);
        if (fromOp) { out.owner = fromOp.post_count; }
    }
    if (threadStars) {
        out.star = (threadStars.star_type) & 0xf;
    }
    ctx.body = out;
};

export const postApiInvalidate_thread_id: RpcImpl = async ctx => {
    const threadId = parseInt32(ctx.params.id);
    const meta = (await sql('SELECT last_success_crawl, successive_fail, owner_user_id FROM thread_targets WHERE thread_id = $1', [threadId])).rows[0];

    if (!meta) { ctx.throw(404, '这个主题……是不存在的呢(；′⌒`)'); }
    if (meta.successive_fail < 0) { ctx.throw(404, '这个主题已经被删除了～'); }
    if (meta.last_success_crawl === 1) { ctx.throw(409, '已经正在刷新啦～请等待…'); }

    const effectiveLastUpdateTime = meta.last_success_crawl;

    const loginUser = ctx.state.mazoLoginUser;
    if (!loginUser) { throw error(403, LOGIN_REQUIRED); }
    let hoursToWait = 0.75; // Logged-in users can refresh every 45 minutes.
    if (loginUser.admin) {
        hoursToWait = 0.05; // Admin refresh every 3 minutes.
    } else if (loginUser.confirmed || (loginUser.id === meta.owner_user_id)) {
        hoursToWait = 0.1; // Confirmed users or thread owner can refresh every 6 minutes. 
    }
    const timeToWait = (hoursToWait * 3600) - (ctx.refDate - effectiveLastUpdateTime);
    if (timeToWait > 0) {
        ctx.throw(429, `上一次刷新刚刚完成，请等待 ${Math.ceil(timeToWait / 60)} 分钟后重试～`); 
    }

    const bbcodeOut = await redoBBCodeOfThread(threadId);
    console.log(`Cache invalidation for thread ${threadId} from ${ctx.ip}: previous timestamps were ${meta.last_success_crawl}/${meta.last_full_scan_requested}`);
    await sql('UPDATE thread_targets SET last_success_crawl = 1, last_full_scan_requested = 0 WHERE thread_id = $1', [threadId]);
    logSpecialRequest(ctx, 'thread-refresh', threadId);
    ctx.body = `主题 ${threadId} 已开始刷新，可能需要几分钟（少数情况下甚至几小时）完成。调试信息：${bbcodeOut}`;
};
