import yesno = require('yesno');
import { query } from '../db';
import { executeBotForPost, getBot } from '../utils/bots';

const main = async (botId: number, minPostId: number, maxPostId: number) => {
    let i = minPostId;
    while (true) {
        const rows = (await query<{ post_id: number, rev_id: number, title: string, content: string, most_recent_update: number }>('SELECT post_id, rev_id, title, content, most_recent_update FROM posts WHERE post_id >= $1 ORDER BY post_id ASC LIMIT 100', [i])).rows;
        if (!rows.length) {
            console.log('Finished looping through posts.'); break;
        }
        i = rows[rows.length - 1].post_id;
        for (const row of rows) {
            if (row.post_id > maxPostId) { // Will never be true when maxPostId is NaN.
                return;
            }
            const revId = await executeBotForPost(botId, row);
            if (revId) {
                console.log(`Rev ${revId} inserted for post ${row.post_id}`);
            }
        }
    }
};

if (require.main === module) {
    const args = process.argv.slice(2);
    
    const botId = parseInt(args.map(single => single.match(/-+bot(?:[-_]?id)?=(\d+)/i)).find(_ => _)?.[1]!);
    const minPostId = parseInt(args.map(single => single.match(/-+min[-_]?post(?:[-_]?id)?=(\d+)/i)).find(_ => _)?.[1]!);
    const maxPostId = parseInt(args.map(single => single.match(/-+max[-_]?post(?:[-_]?id)?=(\d+)/i)).find(_ => _)?.[1]!);
    if (isNaN(botId)) { throw new Error('Bot not specified!'); }
    
    const bot = getBot(botId);
    yesno({
        question: `bot_id: ${botId} (${bot?.titleSc}); min_post_id: ${minPostId}; max_post_id: ${maxPostId}; execute?`
    }).then(confirmed => confirmed ? main(botId, minPostId, maxPostId) : undefined);
}