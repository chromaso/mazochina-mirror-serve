import * as elements from 'typed-xhtml';
import { HtmlTmpl } from '../utils/tsx_utils';

// Migrated from campaign_vote.pug.
export const campaignVote: HtmlTmpl<{
    isPanelMember: boolean,
    currentReview: { scores: number[], content: string },
    canVote: boolean,
    currentVote: 0 | 1 | 2,
    medalWinner: { thread_id: number, thread_title: string } | undefined,
    totalThumbups: number,
    totalMedals: number
}> = (ctx, self) => (
    <div class="card mb-4">
        <div class="card-header d-flex justify-content-between align-items-center">
            {self.isPanelMember ? (
                <span>
                    征文比赛投票
                    <span
                        data-title="你的投票将决定人气奖的归属，获奖作品将获得现金奖励"
                        class="mazomirror-tip"
                    >
                        <span class="material-icons std-icon">help</span>
                    </span>
                </span>
            ) : (
                <span>征文比赛评委评分</span>
            )}
            <a href="/thread/1073748496" target="_blank">
                活动详情 »
            </a>
        </div>
        {self.isPanelMember ? (
            <>
                <div class="card-body">
                    <p>您是特邀评委中的一员，请给出对此作品的点评和/或评分：</p>
                    <div id="unsaved-warn" class="alert alert-warning d-none">
                        以下为您未保存的草稿。请选择「保存」将此草稿保存，或
                        <button class="btn btn-secondary btn-sm">
                            放弃此草稿，回滚到已保存的版本
                        </button>
                        。
                    </div>
                    <div class="form-group row">
                        <label for="panel-s1" class="col-sm-2 col-form-label">
                            色情度
                        </label>
                        <div class="col-sm-5">
                            <div class="input-group">
                                <input
                                    id="panel-s1"
                                    type="number"
                                    min="0"
                                    max="100"
                                    value={String(self.currentReview.scores[0])}
                                    class="form-control"
                                />
                               <span class="input-group-text" style="width: 80px">
                                    /100
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <small class="text-muted">
                                填写 0 视作暂不评分；不会对作者或读者显示
                            </small>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="panel-s2" class="col-sm-2 col-form-label">
                            文学性
                        </label>
                        <div class="col-sm-5">
                            <div class="input-group">
                                <input
                                    id="panel-s2"
                                    type="number"
                                    min="0"
                                    max="80"
                                    value={String(self.currentReview.scores[1])}
                                    class="form-control"
                                />
                                <span class="input-group-text" style="width: 80px">
                                    /80
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <small class="text-muted">
                                填写 0 视作暂不评分；不会对作者或读者显示
                            </small>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="panel-s3" class="col-sm-2 col-form-label">
                            合题性
                        </label>
                        <div class="col-sm-5">
                            <div class="input-group">
                                <input
                                    id="panel-s3"
                                    type="number"
                                    min="0"
                                    max="80"
                                    value={String(self.currentReview.scores[2])}
                                    class="form-control"
                                />
                               <span class="input-group-text" style="width: 80px">
                                    /80
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <small class="text-muted">
                                填写 0 视作暂不评分；不会对作者或读者显示
                            </small>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="panel-s4" class="col-sm-2 col-form-label">
                            创意加分
                        </label>
                        <div class="col-sm-5">
                            <div class="input-group">
                                <input
                                    id="panel-s4"
                                    type="number"
                                    min="0"
                                    max="40"
                                    value={String(self.currentReview.scores[3])}
                                    class="form-control"
                                />
                                <span class="input-group-text" style="width: 80px">
                                    /40
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <small class="text-muted">
                                填写 0 视作暂不评分；不会对作者或读者显示
                            </small>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="panel-rc" class="col-sm-2 col-form-label">
                            评语
                        </label>
                        <div class="col-sm-10">
                            <textarea
                                id="panel-rc"
                                placeholder="（非必填，会公开显示；可使用 BBCode）"
                                class="form-control"
                            >
                                {self.currentReview.content}
                            </textarea>
                        </div>
                    </div>
                    <button id="submit-btn" class="btn btn-primary">
                        保存
                    </button>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        下方为所有用户均可参与的读者投票环节：
                    </li>
                </ul>
            </>
        ) : null}
        <div class="card-body">
            {self.canVote ? (
                <>
                    <p>
                        {(self.currentVote === 2) ? (
                            <>
                                你当前已为该作品投上 👍 和 🥇。
                                <button
                                    type="button"
                                    data-value="0"
                                    class="btn btn-small btn-outline-secondary"
                                >
                                    撤销投票
                                </button>
                            </>
                        ) : (self.currentVote === 1) ? (
                            <>
                                <button
                                    type="button"
                                    data-value="0"
                                    class="btn btn-primary"
                                >
                                    👍
                                </button>{' '}
                                <button
                                    type="button"
                                    data-value="2"
                                    class="btn btn-outline-secondary"
                                >
                                    🥇
                                </button>
                                <p>你当前已为该作品投上 👍。</p>
                            </>
                        ) : (
                            <>
                                <button
                                    type="button"
                                    data-value="1"
                                    class="btn btn-outline-secondary"
                                >
                                    👍
                                </button>
                                <p>你当前尚未给此作品投票。</p>
                            </>
                        )}
                    </p>
                    <p class="small mb-0">
                        提示：你可以给任意多篇作品投上 👍，但只能给一篇作品投上 🥇；你当前
                        {self.medalWinner ? (
                            <>
                                将 🥇 投给了
                                {' '}
                                <a href={'/thread/' + self.medalWinner.thread_id}>
                                    {ctx.state.render.contentConv(self.medalWinner.thread_title)}
                                </a>
                                。
                            </>
                        ) : (
                            '尚未将 🥇 投给任何作品。'
                        )}
                    </p>
                </>
            ) : (
                <>
                    抱歉，你的用户太新，没有投票资格。
                    <span class="mazomirror-tip" data-title="须在 2024 年 7 月 1 日前已发过至少 5 帖者才有权投票">
                        <span class="material-icons std-icon">help</span>
                    </span>
                </>
            )}
        </div>
        <div id="contest-votes-footer" class="card-footer">
            <a href="javascript:showContestVoteDetails()" class="text-reset">
                👍<small class="text-muted"> × </small>
                {self.totalThumbups}
                {' '}
                🥇<small class="text-muted"> x </small>
                {self.totalMedals}
            </a>
        </div>
    </div>
);

// Migrated from campaign_votes.pug.
export const campaignVotes: HtmlTmpl<{
    votes: { user_id: number, user_name: string, vote: number }[]
}> = (ctx, self) =>
    !self.votes.length ? (
        <span>暂无用户投票给此作品</span>
    ) : (
        <span>
            以下用户投票给此作品：
            {self.votes.map(vote => (
                <a
                    href={'/author/' + vote.user_id}
                    class="badge bg-secondary mx-1"
                >
                    {vote.user_name}
                    {vote.vote > 1 ? ' 🥇' : null}
                </a>
            ))}
        </span>
    );