import * as htmlparser2 from 'htmlparser2';
import { Element, ChildNode } from 'domhandler';
import { ElementType } from 'domelementtype';
import render from 'dom-serializer';
import { knownBBCodeTags } from './bbcode';

type Context = {
    postId: number;
    stack: string[];
    logger: Pick<Console, 'log' | 'warn'>;
    strict: boolean;
};

// https://www.phpbb.com/community/viewtopic.php?t=1721345
const INVISIBLE = '\u00ad';

const NAMED_COLORS: Record<string, string> = {
    aliceblue: '#f0f8ff',
    antiquewhite: '#faebd7',
    aqua: '#00ffff',
    aquamarine: '#7fffd4',
    azure: '#f0ffff',
    beige: '#f5f5dc',
    bisque: '#ffe4c4',
    black: '#000000',
    blanchedalmond: '#ffebcd',
    blue: '#0000ff',
    blueviolet: '#8a2be2',
    brown: '#a52a2a',
    burlywood: '#deb887',
    cadetblue: '#5f9ea0',
    chartreuse: '#7fff00',
    chocolate: '#d2691e',
    coral: '#ff7f50',
    cornflowerblue: '#6495ed',
    cornsilk: '#fff8dc',
    crimson: '#dc143c',
    cyan: '#00ffff',
    darkblue: '#00008b',
    darkcyan: '#008b8b',
    darkgoldenrod: '#b8860b',
    darkgray: '#a9a9a9',
    darkgreen: '#006400',
    darkgrey: '#a9a9a9',
    darkkhaki: '#bdb76b',
    darkmagenta: '#8b008b',
    darkolivegreen: '#556b2f',
    darkorange: '#ff8c00',
    darkorchid: '#9932cc',
    darkred: '#8b0000',
    darksalmon: '#e9967a',
    darkseagreen: '#8fbc8f',
    darkslateblue: '#483d8b',
    darkslategray: '#2f4f4f',
    darkslategrey: '#2f4f4f',
    darkturquoise: '#00ced1',
    darkviolet: '#9400d3',
    deeppink: '#ff1493',
    deepskyblue: '#00bfff',
    dimgray: '#696969',
    dimgrey: '#696969',
    dodgerblue: '#1e90ff',
    firebrick: '#b22222',
    floralwhite: '#fffaf0',
    forestgreen: '#228b22',
    fuchsia: '#ff00ff',
    gainsboro: '#dcdcdc',
    ghostwhite: '#f8f8ff',
    gold: '#ffd700',
    goldenrod: '#daa520',
    gray: '#808080',
    green: '#008000',
    greenyellow: '#adff2f',
    grey: '#808080',
    honeydew: '#f0fff0',
    hotpink: '#ff69b4',
    indianred: '#cd5c5c',
    indigo: '#4b0082',
    ivory: '#fffff0',
    khaki: '#f0e68c',
    lavender: '#e6e6fa',
    lavenderblush: '#fff0f5',
    lawngreen: '#7cfc00',
    lemonchiffon: '#fffacd',
    lightblue: '#add8e6',
    lightcoral: '#f08080',
    lightcyan: '#e0ffff',
    lightgoldenrodyellow: '#fafad2',
    lightgray: '#d3d3d3',
    lightgreen: '#90ee90',
    lightgrey: '#d3d3d3',
    lightpink: '#ffb6c1',
    lightsalmon: '#ffa07a',
    lightseagreen: '#20b2aa',
    lightskyblue: '#87cefa',
    lightslategray: '#778899',
    lightslategrey: '#778899',
    lightsteelblue: '#b0c4de',
    lightyellow: '#ffffe0',
    lime: '#00ff00',
    limegreen: '#32cd32',
    linen: '#faf0e6',
    magenta: '#ff00ff',
    maroon: '#800000',
    mediumaquamarine: '#66cdaa',
    mediumblue: '#0000cd',
    mediumorchid: '#ba55d3',
    mediumpurple: '#9370db',
    mediumseagreen: '#3cb371',
    mediumslateblue: '#7b68ee',
    mediumspringgreen: '#00fa9a',
    mediumturquoise: '#48d1cc',
    mediumvioletred: '#c71585',
    midnightblue: '#191970',
    mintcream: '#f5fffa',
    mistyrose: '#ffe4e1',
    moccasin: '#ffe4b5',
    navajowhite: '#ffdead',
    navy: '#000080',
    oldlace: '#fdf5e6',
    olive: '#808000',
    olivedrab: '#6b8e23',
    orange: '#ffa500',
    orangered: '#ff4500',
    orchid: '#da70d6',
    palegoldenrod: '#eee8aa',
    palegreen: '#98fb98',
    paleturquoise: '#afeeee',
    palevioletred: '#db7093',
    papayawhip: '#ffefd5',
    peachpuff: '#ffdab9',
    peru: '#cd853f',
    pink: '#ffc0cb',
    plum: '#dda0dd',
    powderblue: '#b0e0e6',
    purple: '#800080',
    rebeccapurple: '#663399',
    red: '#ff0000',
    rosybrown: '#bc8f8f',
    royalblue: '#4169e1',
    saddlebrown: '#8b4513',
    salmon: '#fa8072',
    sandybrown: '#f4a460',
    seagreen: '#2e8b57',
    seashell: '#fff5ee',
    sienna: '#a0522d',
    silver: '#c0c0c0',
    skyblue: '#87ceeb',
    slateblue: '#6a5acd',
    slategray: '#708090',
    slategrey: '#708090',
    snow: '#fffafa',
    springgreen: '#00ff7f',
    steelblue: '#4682b4',
    tan: '#d2b48c',
    teal: '#008080',
    thistle: '#d8bfd8',
    tomato: '#ff6347',
    turquoise: '#40e0d0',
    violet: '#ee82ee',
    wheat: '#f5deb3',
    white: '#ffffff',
    whitesmoke: '#f5f5f5',
    yellow: '#ffff00',
    yellowgreen: '#9acd32'
};

export const SMILIES: Record<string, string> = {
    'arrow': '➡️',
    'cool': '😎',
    'cry': '😢',
    'e_biggrin': '😁',
    'e_confused': '😕',
    'e_geek': '🤓',
    'e_sad': '😞',
    'e_smile': '😃',
    'e_surprised': '😲',
    'e_ugeek': '🤓',
    'e_wink': '😉',
    'eek': '😬',
    'evil': '😡',
    'exclaim': '❗',
    'idea': '💡',
    'lol': '😂',
    'mad': '😠',
    'mrgreen': '🤢',
    'neutral': '😐',
    'question': '❓',
    'razz': '🤪',
    'redface': '🥵',
    'rolleyes': '🙄',
    'twisted': '🥴'
};

const RENDER_OPTS = { xmlMode: false, selfClosingTags: true, encodeEntities: false };

const isNotWhiteSpace = (node: ChildNode): boolean => !((node.type === ElementType.Text) && /^\s*$/.test(node.data));

// Get previous/next sibling and assert it's en element (tag). Skips whitespace only, not other text nodes.
const actualSibling = (tag: Element, ctx: Context, method: 'previousSibling' | 'nextSibling'): Element => {
    let attempt: ChildNode | null = tag;
    while (true) {
        attempt = attempt[method];
        if (!attempt) { throw error(`nextActualSibling failed due to void`, ctx, tag); }
        if (attempt.type === ElementType.Tag) { return attempt; }
        if (isNotWhiteSpace(attempt)) {
            throw error(`nextActualSibling failed due to weird thing in-between`, ctx, attempt);
        }
    }
};

const sanitize = (text: string, ctx: Context): string =>
    text.replace(/\[(\/?)([\w\.*]+)([^[\]]*)]/g, (full, one, two) => {
        const tagName = two.toLowerCase();
        if (knownBBCodeTags.has(tagName)) {
            ctx.logger.log('Dangling BBCode tag ' + full + ' found in ' + ctx.postId + '; added a soft hyphen.');
            return '[' + INVISIBLE + full.substring(1, full.length);
        } else {
            // Ignore. We're probably fine.
            return full;
        }
    }).replace(/\r|\n/g, '');

const recursive = (childNodes: ChildNode[], ctx: Context): string => {
    let output = '';
    for (const cn of childNodes) {
        if (cn.type === ElementType.Text) {
            output += sanitize(cn.data, ctx);
        } else if (cn.type === ElementType.Tag) {
            for (const attempt of mappers) {
                const result = attempt(cn, ctx);
                if (result !== undefined) {
                    output += result;
                    break;
                }
            }
        } else {
            if ((cn.type !== ElementType.Comment) || (cn.data.trim().length > 1)) { // Ignore single-char comments, IDK why those just exist for some reason.
                ctx.logger.warn('Node of ' + cn.type + ' found in ' + ctx.postId + ':' + cn.startIndex + '-' + cn.endIndex);
            }
        }
    }
    return output;
};

const error = (text: string, ctx: Context, node: ChildNode): Error => new Error(text + ' at ' + ctx.postId + ':' + node.startIndex + '-' + node.endIndex);

type TextExtractor = ((container: Element) => string | undefined) & ((container: Element, ctx: Context) => string);

const text: TextExtractor = ((container: Element, ctx?: Context) => {
    if (container.childNodes.length === 0) { return ''; } // Special case: allow empty.
    if (container.childNodes.length !== 1) {
        if (ctx) { throw error('Element contains more than a text', ctx, container); }
        return;
    }
    const text = container.childNodes[0];
    if (text.type !== ElementType.Text) {
        if (ctx) { throw error('Element contains other than a text', ctx, container); }
        return;
    }
    return text.data;
}) as any;

const mappers: ((tag: Element, ctx: Context) => (string | undefined))[] = [
    /**
     * Code block.
     */
    (tag, ctx) => {
        if (tag.name !== 'div') { return; }
        if (tag.attribs['class'] !== 'codebox') { return; }
        if (tag.childNodes.length !== 2) { return; }
        if ((tag.childNodes[0] as Element).name !== 'p') { return; }
        const lastChild = tag.childNodes[1] as Element;
        if (lastChild.name !== 'pre') { return; }
        if (lastChild.childNodes.length !== 1) { return; }
        const code = lastChild.childNodes[0] as Element;
        if (code.name !== 'code') { return; }
        const textContent = text(code, ctx);
        return '[code]' + textContent.replace(/\[\/code([^[\]]*)]/gi, match => {
            ctx.logger.warn('[/code] found in code block (' + ctx.postId + ':' + code.childNodes[0].startIndex + ')'); // BBCode is fine as long it's not [/code].
            return '［' + match.substring(1, match.length - 1) + '］';
        }) + '[/code]␤';
    },
    /**
     * Code (variation - first half)
     */
    (tag, ctx) => {
        if (tag.name !== 'div') { return; }
        if (tag.attribs['class'] !== 'codetitle') { return; }

        const pairing = actualSibling(tag, ctx, 'nextSibling');
        if (pairing.type !== ElementType.Tag) { return; }
        if (pairing.name !== 'pre') { return; }
        if (pairing.attribs['class'] !== 'codecontent') { return; }

        if (tag.childNodes.length !== 1) { return; }
        const b = tag.childNodes[0];
        if ((b.type !== ElementType.Tag) || (b.name !== 'b')) { return; }
        const bText = text(b, ctx);
        if (bText.trim() !== 'Code:') { throw error('Code title b text wrong', ctx, b); }
        return ''; // Nothing. The second half will add some actual content.
    },
    /**
     * Code (variation - second half)
     */
    (tag, ctx) => {
        if (tag.name !== 'pre') { return; }
        if (tag.attribs['class'] !== 'codecontent') { return; }

        const pairing = actualSibling(tag, ctx, 'previousSibling');
        if (pairing.type !== ElementType.Tag) { return; }
        if (pairing.name !== 'div') { return; }
        if (pairing.attribs['class'] !== 'codetitle') { return; }

        const textContent = text(tag, ctx);
        return '[code]' + textContent.replace(/\[\/code([^[\]]*)]/gi, match => {
            ctx.logger.warn('[/code] found in code block (' + ctx.postId + ':' + tag.childNodes[0].startIndex + ')'); // BBCode is fine as long it's not [/code].
            return '［' + match.substring(1, match.length - 1) + '］';
        }) + '[/code]␤';
    },
    /**
     * Reply (w/ date)
     */
    (tag, ctx) => {
        if (tag.name !== 'blockquote') { return; }
        if (tag.attribs['class']) { return; }
        if (tag.childNodes.length !== 1) { return; }
        const div = tag.childNodes[0] as Element;
        if (div.name !== 'div') { return; }
        const cite = div.childNodes[0] as Element;
        if (cite.name !== 'cite') { return; }
        if (cite.childNodes.length !== 4) { return; }

        const anchor1 = cite.childNodes[0] as Element;
        if (anchor1.name !== 'a') { return; }
        const authorName = text(anchor1, ctx);
        if (authorName.match(/"|\[|\]/)) {
            throw error('Quote author name contains illegal char', ctx, anchor1);
        }
        const authorNameStr = authorName.includes(' ') ? '"' + authorName + '"' : authorName;
        const authorHrefMatch = anchor1.attribs['href'].match(/u=(\d+)$/);
        if (!authorHrefMatch) { throw error('Quote author href unexpected', ctx, anchor1); }
        const authorUserId = parseInt(authorHrefMatch[1]);

        const textNode = cite.childNodes[1];
        if (textNode.type !== ElementType.Text) { return; }
        if (textNode.data.trim() !== 'wrote:') { throw error('Quote text unexpected', ctx, textNode); }

        const anchor2 = cite.childNodes[2] as Element;
        if (anchor2.name !== 'a') { return; }
        const postHrefMatch = anchor2.attribs['href'].match(/viewtopic\.php\?p=(\d+)#p(\d+)$/);
        if (!postHrefMatch) { throw error('Quote href unexpected', ctx, anchor2); }
        if (postHrefMatch[1] !== postHrefMatch[2]) { throw error('Quote href mismatch', ctx, anchor2);  }
        const postId = parseInt(postHrefMatch[1]);
        
        const dateDiv = cite.childNodes[3] as Element;
        if (dateDiv.name !== 'div') { return; }
        if (dateDiv.attribs['class'] !== 'responsive-hide') { throw error('Quote div unexpected', ctx, dateDiv); }
        const dateInt = Date.parse(text(dateDiv, ctx));

        return `␤[quote=${authorNameStr} post_id=${postId} time=${Math.round(dateInt / 1000)} user_id=${authorUserId}]` + recursive(div.childNodes.slice(1), { ...ctx, stack: [...ctx.stack, 'quote'] }) + '[/quote]␤';
    },
    /**
     * Reply (w/o date)
     */
    (tag, ctx) => {
        if (tag.name !== 'blockquote') { return; }
        if (tag.attribs['class']) { return; }
        if (tag.childNodes.length !== 1) { return; }
        const div = tag.childNodes[0] as Element;
        if (div.name !== 'div') { return; }
        const cite = div.childNodes[0] as Element;
        if (cite.name !== 'cite') { return; }
        
        const txt = text(cite, ctx);
        const match = txt.trim().match(/^(.*)wrote:$/);
        if (!match) { throw error('Quote text unexpected', ctx, cite); }
        const authorName = match[1].trim();
        if (authorName.match(/"|\[|\]/)) {
            throw error('Quote author name contains illegal char', ctx, cite);
        }
        const authorNameStr = authorName.includes(' ') ? '"' + authorName + '"' : authorName;

        return `␤[quote=${authorNameStr}]` + recursive(div.childNodes.slice(1), { ...ctx, stack: [...ctx.stack, 'quote'] }) + '[/quote]␤';
    },
    /**
     * Reply (broken - first half)
     */
    (tag, ctx) => {
        if (tag.name !== 'div') { return; }
        if (tag.attribs['class'] !== 'quotetitle') { return; }

        const pairing = actualSibling(tag, ctx, 'nextSibling');
        if (pairing.type !== ElementType.Tag) { return; }
        if (pairing.name !== 'div') { return; }
        if (pairing.attribs['class'] !== 'quotecontent') { return; }

        if (tag.childNodes.length !== 1) { return; }
        const b = tag.childNodes[0];
        if ((b.type !== ElementType.Tag) || (b.name !== 'b')) { return; }
        const bText = text(b, ctx);
        if (bText.trim() !== 'Quote:') { throw error('Quote title b text wrong', ctx, b); }
        return ''; // Nothing. The second half will add some actual content.
    },
    /**
     * Reply (broken - second half)
     */
    (tag, ctx) => {
        if (tag.name !== 'div') { return; }
        if (tag.attribs['class'] !== 'quotecontent') { return; }

        const pairing = actualSibling(tag, ctx, 'previousSibling');
        if (pairing.type !== ElementType.Tag) { return; }
        if (pairing.name !== 'div') { return; }
        if (pairing.attribs['class'] !== 'quotetitle') { return; }

        return `␤[quote]` + recursive(tag.childNodes, { ...ctx, stack: [...ctx.stack, 'quote'] }) + '[/quote]␤';
    },
    /**
     * Reply (short)
     */
    (tag, ctx) => {
        if (tag.name !== 'blockquote') { return; }
        if (tag.attribs['class'] !== 'quotecontent') { return; }

        return `␤[quote]` + recursive(tag.childNodes, { ...ctx, stack: [...ctx.stack, 'quote'] }) + '[/quote]␤';
    },
    /**
     * Blockquote uncited
     */
    (tag, ctx) => {
        if (tag.name !== 'blockquote') { return; }
        if (tag.attribs['class'] !== 'uncited') { return; }
        if (tag.childNodes.length !== 1) { return; }
        const div = tag.childNodes[0] as Element;
        if (div.name !== 'div') { return; }

        return `␤[quote]` + recursive(div.childNodes, { ...ctx, stack: [...ctx.stack, 'quote'] }) + '[/quote]␤';
    },
    /**
     * Attachment (unsupported block - first half)
     */
    (tag, ctx) => {
        if (tag.name !== 'div') { return; }
        if (tag.attribs['class'] !== 'attachtitle') { return; }

        const pairing = actualSibling(tag, ctx, 'nextSibling');
        if (pairing.type !== ElementType.Tag) { return; }
        if (pairing.name !== 'div') { return; }
        if (pairing.attribs['class'] !== 'attachcontent') { return; }

        const titleText = text(tag, ctx);
        if (titleText !== 'Attachment:') { throw error('Attachment title text wrong', ctx, tag); }
        return ''; // Nothing. The second half will add some actual content.
    },
    /**
     * Attachment (unsupported block - second half)
     */
    (tag, ctx) => {
        if (tag.name !== 'div') { return; }
        if (tag.attribs['class'] !== 'attachcontent') { return; }

        const pairing = actualSibling(tag, ctx, 'previousSibling');
        if (pairing.type !== ElementType.Tag) { return; }
        if (pairing.name !== 'div') { return; }
        if (pairing.attribs['class'] !== 'attachtitle') { return; }

        const texts = tag.childNodes.filter(t => t.type !== ElementType.Comment);
        if (texts.length !== 1) { throw error('Block attachment content gone', ctx, tag); }
        const textNode = texts[0];
        if (textNode.type !== ElementType.Text) { throw error('Block attachment content wrong', ctx, textNode); }
        return `[code unsupported=attachment]${sanitize(textNode.data, ctx)}[/code]`;
    },
    /**
     * Attachment (unsupported, inline)
     */
    (tag, ctx) => {
        if (tag.name !== 'div') { return; }
        if (tag.attribs['class'] !== 'inline-attachment') { return; }

        const texts = tag.childNodes.filter(t => t.type !== ElementType.Comment);
        if (texts.length !== 1) { throw error('Inline attachment content gone', ctx, tag); }
        const textNode = texts[0];
        if (textNode.type !== ElementType.Text) { throw error('Inline attachment content wrong', ctx, textNode); }
        return `[code unsupported=attachment]${sanitize(textNode.data, ctx)}[/code]`;
    },
    /**
     * Spoiler
     */
    (tag, ctx) => {
        if (tag.name !== 'div') { return; }
        if (tag.attribs['class']) { return; }
        if (!(tag.attribs['style'] ?? '').replace(/\s/g, '').toLowerCase().match(/^padding:3px;background-color:#f{3,6};border:1pxsolid#d8d8d8;font-size:1em;?$/)) { return; }
        const children = tag.childNodes.filter(isNotWhiteSpace);
        if (children.length !== 2) { return; }
        const title = children[0];
        if ((title.type !== ElementType.Tag) || (title.name !== 'div')) { throw error('Spoiler title mismatch', ctx, title); }
        const span = title.childNodes[0];
        if ((span.type !== ElementType.Tag) || (span.name !== 'span')) { throw error('Spoiler span mismatch', ctx, span); }
        let b: ChildNode, a: ChildNode;
        if (title.childNodes.length === 1) { // Span is the only child.
            const spanChildren = span.childNodes.filter(isNotWhiteSpace);
            b = spanChildren[0];
            a = spanChildren[1];
        } else if (title.childNodes.length === 3) {
            b = title.childNodes[1];
            a = title.childNodes[2];
        } else { throw error('Spoiler title neither', ctx, title); }

        if ((b.type !== ElementType.Tag) || (b.name !== 'b')) { throw error('Spoiler b mismatch', ctx, b); }
        if (text(b, ctx).trim() !== 'Spoiler:') { throw error('Spoiler b content wrong', ctx, b); }
        if ((a.type !== ElementType.Tag) || (a.name !== 'a')) { throw error('Spoiler a mismatch', ctx, a); }

        const content = children[1];
        if ((content.type !== ElementType.Tag) || (content.name !== 'div')) { throw error('Content tag mismatch', ctx, content); }
        if (content.attribs['quotecontent']) { throw error('Content class mismatch', ctx, content); }
        if (content.childNodes.length !== 1) { return; }
        const contentDiv = content.firstChild!;
        if ((contentDiv.type !== ElementType.Tag) || (contentDiv.name !== 'div')) { throw error('Content inner tag mismatch', ctx, contentDiv); }
        if (!contentDiv.attribs['style']?.match(/display:\s?none;?/)) { throw error('Content inner style mismatch', ctx, contentDiv); }

        return recursive(contentDiv.childNodes, { ...ctx, stack: [...ctx.stack, 'spoiler'] });
    },
    /**
     * Stupid hide - first half
     */
    (tag, ctx) => {
        if (tag.name !== 'div') { return; }
        if (tag.attribs['class']) { return; }
        if (tag.attribs['style']?.replace(/\s/g, '') !== 'margin-bottom:2px') { return; }

        const pairing = actualSibling(tag, ctx, 'nextSibling');
        if (pairing.type !== ElementType.Tag) { return; }
        if (pairing.name !== 'div') { return; }
        if (pairing.attribs['style']?.replace(/\s/g, '') !== 'border:1pxinset;padding:6px') { return; }

        const children = tag.childNodes.filter(isNotWhiteSpace);
        if (children.length !== 2) { throw error('Stupid hide first half children', ctx, tag); }

        if ((children[0].type !== ElementType.Tag) || (children[0].name !== 'b')) { throw error('Stupid hide first half text', ctx, tag); }
        if (text(children[0], ctx) !== '本主題隱藏: ') {
            throw error('Stupid hide first half text', ctx, children[0]);
        }

        if ((children[1].type !== ElementType.Tag) || (children[1].name !== 'input')) { throw error('Stupid hide first half input', ctx, tag); }
        if (children[1].attribs['value'] !== 'Show') {
            throw error('Stupid hide first half button', ctx, children[1]);
        }
        if (!(children[1].attribs['onclick']?.includes(`display`))) {
            throw error('Stupid hide first half button', ctx, children[1]);
        }
        return ''; // Nothing. The second half will add some actual content.
    },
    /**
     * Stupid hide - second half
     */
    (tag, ctx) => {
        if (tag.name !== 'div') { return; }
        if (tag.attribs['class']) { return; }
        if (tag.attribs['style']?.replace(/\s/g, '') !== 'border:1pxinset;padding:6px') { return; }

        const pairing = actualSibling(tag, ctx, 'previousSibling');
        if (pairing.type !== ElementType.Tag) { return; }
        if (pairing.name !== 'div') { return; }
        if (pairing.attribs['style']?.replace(/\s/g, '') !== 'margin-bottom:2px') { return; }

        const children = tag.childNodes.filter(isNotWhiteSpace);
        if (children.length !== 1) { throw error('Stupid hide second half too many content', ctx, tag); }
        const div = children[0];
        if ((div.type !== ElementType.Tag) || (div.name !== 'div')) { throw error('Stupid hide first half text', ctx, tag); }
        if (div.attribs['style']?.replace(/\s/g, '') !== 'display:none') { throw error('Stupid hide not display:none', ctx, tag); }
        return recursive(div.childNodes, { ...ctx, stack: [...ctx.stack, 'stupid-hide'] });
    },
    /**
     * Image or smilies
     */
    (tag, ctx) => {
        if (tag.name !== 'img') { return; }
        if (tag.attribs['class'] === 'smilies') {
            let altMatch = (tag.attribs['alt'] ?? '').match(/^\[icon_(.+)\.gif]$/);
            if (!altMatch) {
                altMatch = (tag.attribs['alt'] ?? '').match(/^:(.*):$/);
            }
            if (!altMatch) {
                throw error('Image contains wrong alt attrib', ctx, tag);
            }
            const titleStr = altMatch[1];
            if (!(tag.attribs['src'] ?? '').endsWith(`images/smilies/icon_${titleStr}.gif`)) {
                throw error('Image contains wrong src attrib', ctx, tag);
            }
            if (!SMILIES.hasOwnProperty(titleStr)) { throw error('No such emoji', ctx, tag); }
            const mapped = SMILIES[titleStr];
            return mapped;
        } else if (!['class', 'title', 'width', 'height'].some(k => (k in tag))) {
            const src = tag.attribs['src'];
            if (src.match(/\[|\]/)) {
                throw error('Illegal string in img src', ctx, tag);
            }
            return '[img]' + src + '[/img]';
        }
    },
    /**
     * Direct (or semi-direct) conversion tags?
     */
    (tag, ctx) => {
        const tagName = tag.name.toLocaleLowerCase();
        if (['b', 'i', 'u', 's', 'li', 'ol', 'ul', 'strong', 'del', 'em'].includes(tagName)) {
            const children = recursive(tag.childNodes, { ...ctx, stack: [...ctx.stack, tagName] });
            if ((tagName === 'ol') || (tagName === 'ul')) {
                return `␤[${tagName}]␤${children}[/${tagName}]␤`;
            }
            if (tagName === 'li') {
                return `␤[${tagName}]${children}[/${tagName}]␤`;
            }
            let outTagName;
            switch (tagName) {
                case 'strong':
                    outTagName = 'b'; break;
                case 'em':
                    outTagName = 'i'; break;
                case 'del':
                    outTagName = 's'; break;
                default:
                    outTagName = tagName;
            }
            return `[${outTagName}]${children}[/${outTagName}]`;
        }
        if (tagName === 'br') {
            return '\n';
        }
        if (tagName === 'hr') {
            return '␤[hr]␤';
        }
        if (tagName === 'a') {
            const href = tag.attribs['href'].trim();
            if (href) {
                const urlContent = recursive(tag.childNodes, { ...ctx, stack: [...ctx.stack, 'url'] });
                return (urlContent === href) ? `[url]${urlContent}[/url]` : `[url=${href}]${urlContent}[/url]`;
            } else {
                return recursive(tag.childNodes, ctx);
            }
        }
    },
    /**
     * Span
     */
    (tag, ctx) => {
        if (tag.name !== 'span') { return; }
        const children = recursive(tag.childNodes, { ...ctx, stack: [...ctx.stack, 'span'] });

        const style = tag.attribs['style']?.trim();
        if (!style) { return; }

        const boldAttempt = style.match(/^font-weight:\s?bold;?$/);
        if (boldAttempt) {
            return `[b]${children}[/b]`;
        }

        const colorAttempt = style.match(/^color:\s?(.*)$/);
        if (colorAttempt) {
            let colorStr = colorAttempt[1].toLowerCase();
            if (colorStr.endsWith(';')) { colorStr = colorStr.substring(0, colorStr.length - 1); }
            
            if (!/^#[0-9a-f]{6}$/.test(colorStr)) {
                const mapped = NAMED_COLORS[colorStr];
                if (mapped) {
                    colorStr = mapped;
                } else {
                    throw error('Unrecognized color', ctx, tag);
                }
            }
            return `[color=${colorStr}]${children}[/color]`;
        }

        const uAttempt = style.match(/^text-decoration:\s?underline;?$/);
        if (uAttempt) {
            return `[u]${children}[/u]`;
        }

        const sizeAttempt = style.match(/^font-size:\s?(\d+)%;?(?:\s?line-height:\s?(?:(?:\d+%)|(?:normal));?)?$/);
        if (sizeAttempt) {
            return `[size=${sizeAttempt[1]}]${children}[/size]`;
        }
    },
    /**
     * Youtube inside <object>
     */
    (tag, ctx) => {
        if (tag.name !== 'object') { return; }
        if (!tag.childNodes.some(child => (child.type === ElementType.Tag) && (child.name === 'embed'))) { return; }
        return `[code unsupported=video]${sanitize(render(tag, RENDER_OPTS), ctx)}[/code]`;
    },
    /**
     * Youku <embed> (seems to be a singular case)
     */
    (tag, ctx) => {
        if (tag.name !== 'embed') { return; }
        const src = tag.attribs['src'] ?? '';
        if (!(src.includes('youku') || src.includes('tudou'))) { return; }
        return `[code unsupported=video]${sanitize(render(tag, RENDER_OPTS), ctx)}[/code]`;
    },
    /**
     * Catch-all mapper for unrecognized cases.
     */
    (tag, ctx) => {
        if (ctx.strict) {
            throw error('General unsupported', ctx, tag);
        }
        ctx.logger.log('General unsupported case in ' + ctx.postId + ':' + tag.startIndex + '-' + tag.endIndex);
        return '[code unsupported]' + sanitize(render(tag, RENDER_OPTS), ctx) + '[/code]';
    }
];

export const postHtmlToBBCode = (postId: number, html: string, logger: Pick<Console, 'log' | 'warn'> = console, strict: boolean = true): string => {
    if (html.includes('␤')) {
        throw new Error('␤ found in source for post ' + postId);
    }
    const preOutput = recursive(htmlparser2.parseDocument(html, { withStartIndices: true, withEndIndices: true }).childNodes, { postId, stack: [], logger, strict });
    return preOutput.replace(/␤+/g, (match: string, offset: number, str: string) => { 
        if (str.charAt(offset - 1) === '\n') { return ''; }
        if (str.charAt(offset + match.length) === '\n') { return ''; }
        return '\n';
    }).trim();
}