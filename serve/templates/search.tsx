import * as elements from 'typed-xhtml';
import { HtmlTmpl } from '../utils/tsx_utils';
import { listForums } from '../shared/forum';
import { pagination, PaginationParams } from './pagination';
import { excerpt } from '../utils/highlight';
import { BaseThreadTarget, RenderThreadRetVal } from '../shared/thread_table';
import { threadTable } from './thread_table';
import { TagMicro } from '../shared/search';

export type PanelParams = {
    searchPost: boolean,
    keywordsStr: string,
    tagsInKeywordForm: string,
    tags: {
        include: TagMicro[],
        exclude: TagMicro[]
    } | undefined,
    username: string | undefined,
    forumId: number,
    forumName: string | undefined,
    sortByOrigin: boolean,
    includeSpam: boolean,
    forums: Awaited<ReturnType<typeof listForums>>
};

export type ResultParams = {
    keywords: string[],
    resultCount: number,
    resultLimit: number,
    pagination: PaginationParams
};

// Migrated from search.inc.pug.
const searchPanel: HtmlTmpl<PanelParams> = (ctx, self) => (
    <form method="get" action="/query">
        <div class="form-group row">
            <div class="col-4 col-sm-3 col-md-2">搜索类型</div>
            <div class="col-8 col-sm-9 col-md-10">
                <div class="form-check form-check-inline">
                    <input
                        id="entity-type-thread"
                        type="radio"
                        name="type"
                        value="thread"
                        checked={!self.searchPost}
                        class="form-check-input"
                    />
                    <label for="entity-type-thread" class="form-check-label">
                        按标题搜索主题（thread）
                    </label>
                </div>
                <div class="form-check form-check-inline">
                    <input
                        id="entity-type-post"
                        type="radio"
                        name="type"
                        value="post"
                        checked={self.searchPost}
                        class="form-check-input"
                    />
                    <label for="entity-type-post" class="form-check-label">
                        按正文搜索文章（post）
                        {!ctx.state.mazoLoginUser ? (
                            <span class="badge bg-warning">登入后可用</span>
                        ) : null}
                    </label>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label for="keywords" class="col-4 col-sm-3 col-md-2 col-form-label">
                搜索关键词
            </label>
            <div class="col-8 col-sm-9 col-md-10">
                <input
                    id="keywords"
                    name="query"
                    type="text"
                    placeholder="关键词（空格分隔）"
                    value={self.keywordsStr + self.tagsInKeywordForm}
                    class="form-control"
                />
            </div>
        </div>
        <div class="form-group row">
            <label for="username" class="col-4 col-sm-3 col-md-2 col-form-label">
                作者
            </label>
            <div class="col-8 col-sm-9 col-md-10">
                <input
                    id="username"
                    name="user"
                    type="text"
                    placeholder="作者用户名"
                    value={self.username ?? ''}
                    class="form-control"
                />
            </div>
        </div>
        <div class="form-group row">
            <label for="forum-id" class="col-4 col-sm-3 col-md-2 col-form-label">
                搜索范围
            </label>
            <div class="col-8 col-sm-9 col-md-10">
                <select id="forum-id" name="forum" class="form-control">
                    <option value="0">全站</option>
                    {self.forums.map(forum => (
                        <option
                            value={String(forum.id)}
                            selected={self.forumId === forum.id}
                        >
                            {ctx.state.render.staticConv(forum.name)}
                        </option>
                    ))}
                </select>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-4 col-sm-3 col-md-2">结果显示</div>
            <div class="col-8 col-sm-9 col-md-10">
                <div id="sorting-options" class="mb-1">
                    <div class="form-check form-check-inline">
                        <input
                            id="sort-type-reply"
                            type="radio"
                            name="sort"
                            value="reply"
                            checked={!self.sortByOrigin}
                            class="form-check-input"
                        />
                        <label for="sort-type-reply" class="form-check-label">
                            按回复时间排序
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input
                            id="sort-type-origin"
                            type="radio"
                            name="sort"
                            value="origin"
                            checked={self.sortByOrigin}
                            class="form-check-input"
                        />
                        <label for="sort-type-origin" class="form-check-label">
                            按发布时间排序
                        </label>
                    </div>
                </div>
                <div class="form-check form-check-inline">
                    <input
                        id="ignore-spam"
                        type="checkbox"
                        name="all"
                        checked={self.includeSpam}
                        class="form-check-input"
                    />
                    <label for="ignore-spam" class="form-check-label">
                        包含已隐藏的违规/广告主题
                    </label>
                </div>
            </div>
        </div>
        <div class="form-group row mb-0">
            <div class="col-12 text-center">
                <button type="submit" class="btn btn-primary">
                    搜索
                </button>
            </div>
        </div>
        <script type="text/javascript">{elements.safeHtml('initSearchCheckbox();')}</script>
    </form>
);

// Migrated from search.pug.
export const search: HtmlTmpl<PanelParams> = (ctx, params) => (
    <div class="container">
        <h1 class="mb-5">搜索</h1>
        {searchPanel(ctx, params)}
    </div>
);

const tagsSections: HtmlTmpl<PanelParams['tags']> = (ctx, tags) => {
    const { render } = ctx.state;
    return (
        <>
            {tags?.include.length ? (
                <p class="lead my-2">
                    必须包含以下标签之一：
                    {tags.include.map(tag => (
                        <a class="badge rounded-pill bg-secondary me-2 fw-normal" href={'/tag/' + tag.id}>
                            {render.staticConv(tag.name)}
                        </a>
                    ))}
                </p>
            ) : null}
            {tags?.exclude.length ? (
                <p class="lead my-2">
                    不可以包含以下任何标签：
                    {tags.exclude.map(tag => (
                        <a class="badge rounded-pill bg-secondary me-2 fw-normal" href={'/tag/' + tag.id}>
                            {render.staticConv(tag.name)}
                        </a>
                    ))}
                </p>
            ) : null}
        </>
    );
};

// Migrated from search_post.pug.
export const searchPost: HtmlTmpl<PanelParams & ResultParams & {
    posts: {
        id: number,
        authorLink: string,
        authorName: string
        authorLabel: string | null,
        authorAvatar: SafeString,
        isHidden: boolean,
        isLocal: boolean
        title: string,
        content: string,
        topicLink: string,
        date: number,
    }[],
}> = (ctx, self) => {
    const { render } = ctx.state;
    return (
        <div class="container">
            <nav class="mb-4">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="/">首页</a>
                    </li>
                    {self.forumId ? (
                        <li class="breadcrumb-item">
                            <a href={'/forum/' + self.forumId}>{render.staticConv(self.forumName!)}</a>
                        </li>
                    ) : null}
                    <li class="breadcrumb-item active">搜索文章</li>
                </ol>
            </nav>
            <h1 class="mb-2">
                搜索：
                {self.username ? (
                    <>「{self.username}」发布的</>
                ) : null}
                {self.keywords.length ? (
                    <>
                        内容含{self.keywords.map(keyword => `「${keyword}」`).join('、')}
                        的
                    </>
                ) : null}
                {self.tags
                    ? '位于符合下述标签过滤的主题中的'
                    : null}
                文章
            </h1>
            {tagsSections(ctx, self.tags)}
            <div class={"alert mt-4 mb-3 d-flex justify-content-between align-items-center " + (self.resultCount ? ((self.resultCount >= self.resultLimit) ? 'alert-primary' : 'alert-secondary') : 'alert-warning')}>
                {(self.resultCount >= self.resultLimit) ? (
                    <>
                        有大量文章符合你的搜索条件，此处只显示前 {self.resultLimit} 篇。若难以找到你需要的结果，建议使用更具体的关键字。
                    </>
                ) : self.resultCount ? (
                    <>共搜索到 {self.resultCount} 篇文章。</>
                ) : '没有搜索到任何相关结果。'}
                <a href="#refine" class="small text-decoration-none text-end">
                    修改搜索条件
                </a>
            </div>
            {self.posts.length ? self.posts.map(post => (
                <div id={'p' + post.id} class="card my-4">
                    <div class="card-header d-flex justify-content-between">
                        <a class="mm-post-ava flex-shrink-0 me-2 align-self-start" href={post.authorLink}>{post.authorAvatar}</a>
                        <div class="flex-shrink-1 flex-grow-1 ms-1 align-self-start">
                            <a href={post.authorLink} class="ui-link">{post.authorName}</a>
                            {post.authorLabel ? (
                                <span class="badge bg-secondary user-select-none ms-1">
                                    {render.staticConv(post.authorLabel)}
                                </span>
                            ) : null}
                            <div class="text-muted">
                                {render.contentConv(post.title)}
                            </div>
                        </div>
                        {post.isLocal ? (
                            <span class="badge bg-secondary ms-1 mazomirror-tip align-self-center" data-title="此帖从镜像发布，仅存在于镜像站，不存在于源站。">
                                仅镜像
                            </span>
                        ) : null}
                    </div>
                    <div class="card-body">
                        {' '}
                        {post.isHidden ? (
                            <div class="alert alert-info mb-0">
                                本文章的内容回复可见。
                            </div>
                        ) : elements.safeHtml(render.contentConv(excerpt(post.content, self.keywords)))}
                    </div>
                    <div class="card-footer d-flex justify-content-between align-items-center">
                        <span class="text-muted">
                            {render.renderDate(post.date)}
                        </span>
                        <span class="flex-shrink-0">
                            <a class="btn btn-secondary btn-sm" href={post.topicLink}>
                                主题内查看
                            </a>
                        </span>
                    </div>
                </div>
            )) : null}
            {(self.pagination.maxPage > 1) ? pagination(ctx, self.pagination) : null}
            <div id="refine" class="card mt-5">
                <div class="card-header">修改搜索条件</div>
                <div class="card-body">{searchPanel(ctx, self)}</div>
            </div>
        </div>
    );
};

// Migrated from search_thread.pug.
export const searchThread: HtmlTmpl<PanelParams & ResultParams & {
    threads: RenderThreadRetVal<BaseThreadTarget>
}> = (ctx, self) => {
    const { render } = ctx.state;
    return (
        <div class="container">
            <nav class="mb-4">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="/">首页</a>
                    </li>
                    {self.forumId ? (
                        <li class="breadcrumb-item">
                            <a href={'/forum/' + self.forumId}>{render.staticConv(self.forumName!)}</a>
                        </li>
                    ) : null}
                    <li class="breadcrumb-item active">搜索主题</li>
                </ol>
            </nav>
            <h1 class="mb-2">
                搜索：
                {self.username ? (
                    <>「{self.username}」发布的</>
                ) : null}
                {self.keywords.length ? (
                    <>
                        标题含{self.keywords.map(keyword => `「${keyword}」`).join('、')}的
                    </>
                ) : null}
                {self.tags
                    ? '符合下述标签过滤的'
                    : null}
                主题
            </h1>
            {tagsSections(ctx, self.tags)}
            <div class={"alert mt-4 mb-3 d-flex justify-content-between align-items-center " + (self.resultCount ? ((self.resultCount >= self.resultLimit) ? 'alert-primary' : 'alert-secondary') : 'alert-warning')}>
                {self.resultCount >= self.resultLimit ? (
                    <>
                        有大量主题符合你的搜索条件，此处只显示前 {self.resultLimit} 个。若难以找到你需要的结果，建议使用更具体的关键字。
                    </>
                ) : self.resultCount ? (
                    <>共搜索到 {self.resultCount} 个主题。</>
                ) : '没有搜索到任何相关结果。'}
                <a href="#refine" class="small text-decoration-none text-end">
                    修改搜索条件
                </a>
            </div>
            {self.threads.length ? threadTable(ctx, { forumId: self.forumId, threads: self.threads, showReadState: true }) : null}
            {self.pagination.maxPage > 1 ? pagination(ctx, self.pagination) : null}
            <div id="refine" class="card mt-5">
                <div class="card-header">修改搜索条件</div>
                <div class="card-body">{searchPanel(ctx, self)}</div>
            </div>
        </div>
    );
};