import { query as sql, withTransaction } from '../db'; 
import { postHtmlToBBCode, SMILIES } from '../../common/html2bbcode2';

type BotMetadata = {
    titleSc: string,
    descSc: string,
    handle: (
        // The post row will be supplied as-is.
        postRow: { post_id: number, rev_id: number, content: string },
        // The local_post_log row with largest rev_id will be supplied. Caller ensures it belongs to the post.
        revRow: { rev_id: number, content: string } | undefined
    ) => Promise<
        string | // Text content for a new rev.
        undefined // No changes needed.
    >;
};

const BOTS: Map<number, BotMetadata> = new Map();

BOTS.set(1000000001, Object.freeze({
    titleSc: '源站帖子迁移',
    descSc: '将只有 HTML 版本而没有 BBCode 版本存档的源站帖子，根据 HTML 逆推出 BBCode；参见 bbcoderwk 分支代码',
    handle: async (postRow, revRow) => {
        if (revRow) { return undefined; }
        if (postRow.rev_id) { throw new Error('rev_id is set despite there being no rev rows.'); }
        const result = postHtmlToBBCode(postRow.post_id, postRow.content, undefined, false);
        await sql('INSERT INTO stats.thread_backup (post_id, content) VALUES ($1, $2)', [postRow.post_id, postRow.content]);
        return result;
    }
}));

BOTS.set(1000000002, Object.freeze({
    titleSc: '源站表情图标（smilies）替代',
    descSc: '将源站表情图标以 Unicode emoji 替代',
    handle: async (postRow, revRow) => {
        if (!revRow) { return undefined; }
        return revRow.content.replace(/\[icon_([a-z_]+)\.gif]/g, (match, group: string) => (SMILIES.hasOwnProperty(group) ? SMILIES[group] : match));
    }
}));

export const getBot = (botId: number): BotMetadata | undefined => BOTS.get(botId);

// Caller's responsbility for logging and error handling.
export const executeBotForPost = async (botId: number, postRow: { post_id: number, rev_id: number, title: string, content: string, most_recent_update: number }): Promise<number | undefined> => {
    const impl = BOTS.get(botId);
    if (!impl) { throw new Error('No such bot!'); }
    const latestRev: { rev_id: number, flags: number, content: string } | undefined = (await sql('SELECT rev_id, content FROM local_post_log WHERE post_id = $1 ORDER BY rev_id DESC LIMIT 1', [postRow.post_id])).rows[0];
    const result = await impl.handle(postRow, latestRev);
    if (latestRev && (result === latestRev.content)) {
        return; // Content not changed.
    }
    return withTransaction(async query => {
        const now = (Date.now() / 1000) | 0;
        const revId = (await query('INSERT INTO local_post_log (rev_id, post_id, updated_at, title, content, flags, user_id) VALUES (nextval(\'local_rev_id\'), $1, $2, $3, $4, $5, $6) RETURNING rev_id', [postRow.post_id, now, postRow.title, result, latestRev?.flags ?? 0, botId])).rows[0].rev_id;
        const updatedPostRow = (await query('UPDATE posts SET rev_id = $2 WHERE post_id = $1 RETURNING most_recent_update', [postRow.post_id, revId])).rows[0];
        if (updatedPostRow?.most_recent_update !== postRow.most_recent_update) {
            throw new Error('Race condition! most_recent_update changed from ' + postRow.most_recent_update + ' to ' + updatedPostRow?.most_recent_update);
        }
        return revId;
    });
};
