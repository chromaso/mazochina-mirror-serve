import * as elements from 'typed-xhtml';
import { HtmlTmpl, styled } from '../utils/tsx_utils';

// Migrated from pm_list.pug.
export const pm_list: HtmlTmpl<{
    pms: { 
        userId: number, 
        userName: string,
        outCount: number,
        inCount: number,
        unreadCount: number,
        lastIsIn: boolean,
        lastAt: number,
        lastIsFirst: boolean,
        lastTitle: string
    }[]
}> = (ctx, self) => {
    const { render } = ctx.state;
    return (
        <div class="container">
            <h1 class="mb-5">私信</h1>
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">对方用户</th>
                        <th scope="col">共发出</th>
                        <th scope="col">共收到</th>
                        <th scope="col">最新一条</th>
                    </tr>
                </thead>
                <tbody>
                    {self.pms.map(pm => (
                        <tr>
                            <th scope="row">
                                <a href={'/pm/' + pm.userId}>{pm.userName}</a>
                            </th>
                            <td>{pm.outCount}</td>
                            <td>
                                {pm.inCount}
                                {pm.unreadCount ? (<b class="d-inline-block">（{pm.unreadCount}未读）</b>) : null}
                            </td>
                            <td>
                                {pm.lastIsIn ? pm.userName : <i>你</i>} 于 {render.renderDate(pm.lastAt)} {pm.lastIsFirst ? '发出' : '回复'}
                                {pm.lastTitle ? (<>「{render.contentConv(pm.lastTitle)}」</>) : null}
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
};

// Migrated from pms.pug.
export const pms: HtmlTmpl<{
    finished: boolean,
    pms: { id: number, date: number, isFromMe: boolean, authorId: number, authorName: string, title: string, content: string }[]
}> = (ctx, self) => {
    const { render } = ctx.state;
    return (
        <>
            {!self.finished ? (
                <button
                    data-last={self.pms[0].id}
                    class="mt-4 d-block w-100 btn btn-secondary"
                >
                    加载更旧的对话
                </button>
            ) : null}
            {self.pms.map(post => (
                <div
                    id={'p' + post.id}
                    data-date={post.date.toString(36)}
                    class={"card my-4 " + (post.isFromMe ? 'ms-3 ms-sm-4' : 'me-3 me-sm-4')}
                >
                    <div class="card-header">
                        <a href={'/author/' + post.authorId} class="ui-link">{post.authorName}</a>
                        <span class="text-muted">
                            ：{render.contentConv(post.title)}
                        </span>
                    </div>
                    <div class="card-body">{elements.safeHtml(render.contentConv(post.content))}</div>
                    <div class="card-footer d-flex justify-content-between align-items-center">
                        <span class="text-muted">{render.renderDate(post.date)}</span>
                    </div>
                </div>
            ))}
        </>
    );
};

// Migrated from conversation.pug.
export const conversation: HtmlTmpl<{
    targetUserId: number,
    targetUserName: string,
    threadId: number,
    formError: string | undefined,
    justPosted: boolean,
    postTitle: string,
    postContent: string
}> = (ctx, self) => styled(`
    html, body { height: 100%; }
    .hr-button { background: none; border: 0 none; padding: 0; }
    .hr-button .material-icons { vertical-align: middle; }
    .hr-button .additional-text { display: none; }
    .hr-button.active .additional-text { display: inline; }
    blockquote > cite { display: block; }
    .card-body img { max-width: 100%; }
    .pm-conversation-list > div:first-child > *:first-child { margin-top: 0 !important; }
    .pm-conversation-list > div:last-child > *:last-child { margin-bottom: 0 !important; }`,
    <div class="container d-flex flex-column flex-grow-1" style="min-height: 0">
        <script type="text/javascript">{elements.safeHtml(`document.body.classList.add('d-flex'); document.body.classList.add('flex-column');`)}</script>
        <nav class="flex-shrink-0">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="/">首页</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="/pm">私信</a>
                </li>
                <li class="breadcrumb-item active">
                    <a href={'/author/' + self.targetUserId}>{self.targetUserName}</a>
                </li>
            </ol>
        </nav>
        {self.threadId ? (
            <>
                {/* Top half */}
                <div class="flex-grow-1 flex-shrink-1 overflow-auto">
                    <div id={'conversation-' + self.threadId} class="pm-conversation-list">
                        <div id="mm-pm-loading" class="text-center my-4">
                            <div class="spinner-border spinner-border-sm" /> 过往会话加载中
                        </div>
                    </div>
                </div>
            </>
        ) : (
            <div class="alert alert-info">
                你们还从未有过对话～请注意，每 24 小时向陌生人发起私信对话的数量有<a href="/confirmation">限制</a>。
            </div>
        )}
        <div class="d-flex align-items-center mt-2 mb-2">
            {/* Use mt-2 and mb-2 separately above so JS can toggle them separately. */}
            <button class="hr-button text-muted">
                <span class="material-icons">expand_less</span>
                <span class="additional-text">撰写回复</span>
            </button>
            <div class="border-bottom flex-grow-1 mx-3" />
            <button class="hr-button text-muted">
                <span class="additional-text">显示对话</span>
                <span class="material-icons">expand_more</span>
            </button>
        </div>
        <div class="flex-shrink-0">
            {/* Bottom half */}
            {self.formError ? (
                <div class="alert alert-danger">
                    {ctx.state.render.staticConv(self.formError)}
                </div>
            ) : null}
            {self.justPosted ? (
                <div class="alert alert-success">私信已成功发送～</div>
            ) : null}
            <form method="post">
                <div class="form-group">
                    <input
                        type="text"
                        name="title"
                        placeholder="标题（可留空）"
                        value={self.justPosted ? '' : self.postTitle}
                        class="form-control"
                    />
                </div>
                <div class="form-group">
                    <textarea
                        id="main-textarea"
                        name="content"
                        placeholder="内容"
                        rows={String(self.threadId ? 5 : 10)}
                        class="form-control"
                    >
                        {self.justPosted ? '' : self.postContent}
                    </textarea>
                </div>
                <div class="form-group text-center mb-0">
                    <input
                        type="submit"
                        name="submitpost"
                        value="发送"
                        class="btn btn-primary"
                    />
                </div>
            </form>
        </div>
        <script type="text/javascript">{self.threadId ? elements.safeHtml(`loadMmPm(${self.threadId});`) : '/*NO_CONVO*loadMmPm()*/'}</script>
        <script type="text/javascript">{elements.safeHtml(`initCodeEditor('${ctx.state.render.locale}');`)}</script>
    </div>
);
