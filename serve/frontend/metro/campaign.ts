import { getThreadIdFromUri, $xhr, $, $set, $id } from '../common-import';

const LOCAL_STORAGE_KEY = 'mm-contest-panel-draft';

const loadDrafts = (): Record<string, [string | string[], string]> => {
    try {
        const dict = JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY) || '{}');
        if (typeof dict !== 'object') { return {}; }
        return dict as any;
    } catch (e) { return {}; }
};

export const initThreadCampaign = () => {
    const placeholder = document.getElementById('campaign-vote-card-placeholder')!;
    const threadId = getThreadIdFromUri();
    const load = (f?: string, saveReview?: boolean) => {
        $xhr('/api/thread/' + threadId + '/vote', {
            f,
            t: text => {
                placeholder.innerHTML = text;
                const commentInput = placeholder.querySelector<HTMLTextAreaElement>('#panel-rc');
                if (commentInput) { // Review panel member.
                    const scoreInputs = Array.from(placeholder.querySelectorAll<HTMLInputElement>('[id^=panel-s]'));
                    const unsavedWarning = placeholder.querySelector('#unsaved-warn')!;
                    const initialDrafts = loadDrafts();
                    const initialDraft = initialDrafts[threadId];
                    if (initialDraft) {
                        if (typeof initialDraft[0] !== 'object') { initialDraft[0] = ["0", "0", "0", "0"]; }
                        if (scoreInputs.every((input, index) => input.value === initialDraft[0][index]) && (initialDraft[1] === commentInput.value)) {
                            delete initialDrafts[threadId];
                            localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(initialDrafts));
                        } else {
                            unsavedWarning.classList.remove('d-none');
                            scoreInputs.forEach((input, index) => { input.value = initialDraft[0][index]; });
                            commentInput.value = initialDraft[1];
                            unsavedWarning.querySelector<HTMLButtonElement>('button')!.addEventListener('click', () => {
                                const drafts = loadDrafts();
                                delete drafts[threadId];
                                localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(drafts));
                                load();
                            });
                        }
                    }

                    let timeout: ReturnType<typeof setTimeout> | undefined;
                    const doSaveDraft = () => {
                        timeout = undefined;
                        const drafts = loadDrafts();
                        drafts[threadId] = [scoreInputs.map(input => input.value), commentInput.value.trim()];
                        localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(drafts));
                    };
                    const saveDraft = () => {
                        if (timeout) { return; }
                        timeout = setTimeout(doSaveDraft, 500);
                    };
                    for (const scoreInput of scoreInputs) {
                        scoreInput.addEventListener('input', saveDraft);
                        scoreInput.addEventListener('change', saveDraft);
                    }
                    commentInput.addEventListener('input', saveDraft);
                    commentInput.addEventListener('change', saveDraft);

                    const submitBtn = placeholder.querySelector('#submit-btn') as HTMLButtonElement;
                    if (saveReview) {
                        const savedAlert = $('small', { className: 'ms-2 text-muted' }, '已成功保存～');
                        savedAlert.style.transition = 'opacity 0.5s linear';
                        submitBtn.parentNode!.appendChild(savedAlert);
                        savedAlert.addEventListener('transitionend', () => savedAlert.parentNode!.removeChild(savedAlert));
                        setTimeout(() => {
                            savedAlert.classList.add('opacity-0');
                        }, 1000);
                    }
                    submitBtn.addEventListener('click', () => {
                        load(scoreInputs.map(scoreInput => 'scores=' + encodeURIComponent(scoreInput.value)).join('&') + '&content=' + encodeURIComponent(commentInput.value.trim()), true);
                        doSaveDraft();
                    });
                }
                Array.from(placeholder.querySelectorAll<HTMLButtonElement>('button[data-value]')).forEach(btn => btn.addEventListener('click', submitVote));
            },
            e: error => $set(placeholder, $('div', 'alert alert-danger', [$('b', '错误：'), error, $('button', 'btn btn-secondary btn-sm', '重新加载', () => load())]))
        });
    };
    const submitVote = (e: MouseEvent) => {
        const vote = (e.currentTarget as HTMLButtonElement).getAttribute('data-value');
        if (!vote) { window.alert('Vote unknown.'); return; }
        load('vote=' + vote);
    };
    load();
};

export const showContestVoteDetails = () => {
    const footer = document.getElementById('contest-votes-footer')!;
    $xhr('/api/thread/' + getThreadIdFromUri() + '/votes', {
        t: text => {
            footer.innerHTML = text;
        },
        e: error => $set(footer, $('div', 'alert alert-danger', error))
    });
};

const contestRBPanelEditForeword = () => {
    const parent = $id('foreword-edit')!;
    $xhr('/api/contest/thread/1073748496/user', {
        e: e => alert('错误：' + e),
        j: j => {
            const old = parent.lastElementChild!;
            const obc = j.bbcode;
            const ta = $('textarea', { className: 'form-control', value: obc, name: 'foreword', placeholder: '（可使用 BBCode）', rows: Math.max(6, obc.split('\n').length) });
            parent.replaceChild($('div', 'card-body',
                $('form', { method: 'post' }, [
                    $('div', 'form-group', ta),
                    $('div', 'form-group', [
                        $('a', { className: 'btn btn-secondary' }, '取消', () => {
                            if ((obc.replace(/\s+/g, '') === ta.value.replace(/\s+/g, '')) || window.confirm('丢弃更改？')) {
                                parent.replaceChild(old, parent.lastElementChild!);
                            }
                        }),
                        $('button', { type: 'submit', className: 'btn btn-primary ms-2' }, '保存')
                    ])
                ])
            ), old);
        }
    });
};

export const contestRBPanelInit = () => {
    $id('foreword-btn')?.addEventListener('click', contestRBPanelEditForeword);
    
    const d = 'd-none';
    for (const el of Array.from(document.querySelectorAll('.mm-btnexplnk'))) {
        el.addEventListener('click', event => {
            const c = (event.currentTarget as HTMLElement).parentNode as HTMLElement;
            const e = c.nextElementSibling!;
            const b = e.querySelector('button')!;
            c.classList.add(d);
            e.classList.remove(d);
            const hide = () => {
                b.removeEventListener('click', hide);
                e.classList.add(d);
                c.classList.remove(d);
            };
            b.addEventListener('click', hide);
        }); 
    }
};
