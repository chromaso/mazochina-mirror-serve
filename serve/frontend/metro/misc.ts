import { $, $set, $xhr, getThreadIdFromUri, loadingSpinner, showModal } from '../common-import';
import { multiTabModal } from './modal';

export const openThreadStats = () => {
    const container = $('div', void 0, [loadingSpinner(false)]);
    showModal('主题记录', container);
    $xhr('/api/thread/' + getThreadIdFromUri(), {
        t: text => multiTabModal(container, text, [
            { _prop_label: '读者统计', _prop_id_in_html: 'thread-reader-logs' },
            { _prop_label: '管理操作记录', _prop_id_in_html: 'thread-admin-logs' }
        ]),
        e: () => $set(container, $('div', 'alert alert-danger', '记录加载失败啦 °(°ˊДˋ°) °'))
    });
};

export const openTriviaStats = (tabIndex: number) => {
    const container = $('div', void 0, [loadingSpinner(false)]);
    showModal('发帖排行', container);
    $xhr('/api/trivia', {
        t: text => multiTabModal(container, text, [
            { _prop_label: '本周主题', _prop_id_in_html: 'thread-weekly' },
            { _prop_label: '本周文章', _prop_id_in_html: 'post-weekly' },
            { _prop_label: '本月主题', _prop_id_in_html: 'thread-monthly' },
            { _prop_label: '本月文章', _prop_id_in_html: 'post-monthly' },
        ], void 0, tabIndex),
        e: () => $set(container, $('div', 'alert alert-danger', '排行加载失败啦 °(°ˊДˋ°) °'))
    });
};

