import { unawaited } from '../common/logic_utils';
import { query, busy } from './db';
import { updateAchievements } from './shared/achievements';
import { computeTrivia } from './shared/trivia';
import { setTimeout as setTimeoutPromise } from 'node:timers/promises';

const LOG = false;

const log = LOG ? (...params: any[]) => console.log('[bg_achi]', ...params) : () => {/* noop */};

const aborter = new AbortController();

const sleep = (ms: number) => setTimeoutPromise(ms, undefined, { ref: false, signal: aborter.signal });

const achivementBgTaskSingle = async (lastId: number): Promise<number> => {
    if (busy()) {
        log('Skipping as DB is busy.');
        return lastId;
    }
    const now = Math.floor(Date.now() / 1000);
    const nextUser = (await query<{ user_id: number }>('SELECT user_id FROM users WHERE user_id > $1 AND (SELECT COUNT(*) FROM posts WHERE posts.user_id = users.user_id) >= 5 AND password_h IS NOT NULL ORDER BY user_id ASC LIMIT 1', [lastId])).rows[0];
    if (!nextUser) {
        log('Looping to head.');
        const maxIdToDelete = (await query<{ mrid: number | null }>('SELECT MAX(rev_id) AS mrid FROM stats.achi_ppl WHERE updated_at < $1', [now - 86400 * 5])).rows[0].mrid // All logs before this would be vaccumed. Only keep logs for the last 5 days.
        if (maxIdToDelete) {
            console.log(`[bg_achi] Deleting records older than ${maxIdToDelete}.`);
            unawaited(query('DELETE FROM stats.achi_ppl WHERE rev_id <= $1', [maxIdToDelete]), 'vacuuming old stats.achi_ppl');
        }
        return 0; // Start from beginning.
    }
    const currentUser = nextUser.user_id;
    const lastProcess = (await query<{ updated_at: number }>('SELECT updated_at FROM stats.achi_ppl WHERE user_id = $1 ORDER BY rev_id DESC LIMIT 1', [currentUser])).rows[0];
    if (lastProcess?.updated_at > (now - 7200)) { // Do not repeat if last done within 2 hours.
        log('Skipping recently-done', currentUser);
        return currentUser;
    }
    await updateAchievements(currentUser, -1);
    log('Done with', currentUser);
    return currentUser;
};

const achievementBgTaskLoop = async (): Promise<void> => {
    try {
        await sleep(15000); // The first 15 seconds could be pretty busy.
        let lastProcessed = (await query<{ user_id: number }>('SELECT user_id FROM stats.achi_ppl WHERE requester < 0 ORDER BY rev_id DESC LIMIT 1')).rows[0]?.user_id ?? 0;
        while (true) {
            lastProcessed = await achivementBgTaskSingle(lastProcessed);
            await sleep(7500); // Wait for 7.5 seconds between users. Maybe change it further in the future.
        }
    } catch (error) {
        if (!aborter.signal.aborted) {
            console.error('FAILED achievementBgTaskLoop. Restarting...', error);
            setTimeout(achievementBgTaskLoop);
        }
    }
};

const triviaTaskLoop = async (): Promise<void> => {
    try {
        while (true) {
            await computeTrivia();
            await sleep(15000); // Wait for 150 seconds between recompute.
        }
    } catch (error) {
        if (!aborter.signal.aborted) {
            console.error('FAILED triviaTaskLoop. Restarting...', error);
            setTimeout(triviaTaskLoop);
        }
    }
};

export const bgTasks = () => {
    achievementBgTaskLoop();

    triviaTaskLoop();

    return aborter;
};