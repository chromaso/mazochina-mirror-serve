
import getRawBody = require('raw-body');
import { promises as fs } from 'fs';

import { query as sql } from '../db';
import { LOGIN_REQUIRED, RpcImpl, error } from '../utils/render_utils';
import { allowedFormats, attachmentIdToName } from '../shared/attachment';
import { fromRoot } from '../path';
import sharp = require('sharp');
import { imageToSignatureBuffer } from '../utils/avatar_signature';
import path = require('path');

const avatarDir = path.join('static', 'avatars');

export const postApiAvatar: RpcImpl = async ctx => {
    const userInfo = ctx.state.mazoLoginUser;
    if (!userInfo) { throw error(403, LOGIN_REQUIRED); }

    const file = await getRawBody(ctx.req, {
        length: ctx.req.headers['content-length'],
        limit: '640kb'
    });

    const source = ctx.query.src;
    let manual: boolean;
    if (source === 'auto') { // Archiving an third-party avatar.
        manual = false;

        if (file.length < 16) {
            const stringForm = file.toString('utf8');
            let skipCheckForDays;
            switch (stringForm) { // User has no third-party avatar.
                case 'NONE':
                    skipCheckForDays = 75; // 2.5 months.
                    break;
                case 'DECLINE': // User chose not to set avatar.
                    skipCheckForDays = 730; // 2 years.
                    break;
                default:
                    skipCheckForDays = 7; // a week.
                    console.warn('User avatar checking: ' + stringForm + ' ' + userInfo.email);
            }
            const skipCheckUntil = ctx.refDate + skipCheckForDays * 86400;
            await sql('UPDATE users SET avatar_skipck = $2 WHERE user_id = $1', [userInfo.id, skipCheckUntil]);
            ctx.body = stringForm + ';Next=' + skipCheckUntil;
            return;
        }
    } else if (source === 'manual') { // Uploading a first-party avatar.
        manual = true;
    } else {
        throw error(404, 'No operation specified.');
    }

    const magicNumber = file.slice(0, 3).toString('hex');
    if (!(magicNumber in allowedFormats)) {
        throw error(400, 'Image format not allowed.');
    }

    const sharpStream = sharp(file);
    const meta = await sharpStream.metadata();
    if (!((meta.width! >= 240) && (meta.width! < 1600) && (meta.width === meta.height))) {
        throw error(400, `Invalid image dimension ${meta.width}x${meta.height}`);
    }

    const signature = await imageToSignatureBuffer(sharpStream.toColorspace('rgba'));
    const baseFileName = signature.toString('base64url');

    const recents = (await sql<{ cnt: number }>('SELECT COUNT(*)::INT AS cnt FROM user_avatars WHERE user_id = $1 AND updated_at >= $2', [userInfo.id, ctx.refDate - 7200])).rows[0].cnt;
    if (recents > 4) { throw error(429, 'Too many uploads. Wait for a few hours.'); }

    const id = (await sql<{ avatar_id: number }>('INSERT INTO user_avatars (user_id, img_signature, created_at, updated_at, flags) VALUES ($1, $2, $3, $3, $4) ON CONFLICT (user_id, img_signature) DO UPDATE SET updated_at = EXCLUDED.updated_at, flags = user_avatars.flags | EXCLUDED.flags RETURNING avatar_id', [userInfo.id, signature, ctx.refDate, manual ? 1 : 0])).rows[0].avatar_id;
    const baseLinkName = attachmentIdToName(id);

    await fs.writeFile(fromRoot(avatarDir, baseFileName), file);
    if (manual) {
        await fs.writeFile(fromRoot(avatarDir, baseFileName + '.240'), await sharpStream.resize(240, 240).webp().toBuffer());
        // The above must be done first, to prevent the two `sharpStream.resize` calls from interfering w/ each other.
        await Promise.all([
            fs.writeFile(fromRoot(avatarDir, baseFileName + '.120'), await sharpStream.resize(120, 120).webp().toBuffer()),
            fs.symlink(baseFileName + '.240', fromRoot(avatarDir, baseLinkName + '-240.webp')).catch(e => { if (e.code !== 'EEXIST') { throw e; } }),
            fs.symlink(baseFileName + '.120', fromRoot(avatarDir, baseLinkName + '-120.webp')).catch(e => { if (e.code !== 'EEXIST') { throw e; } })
        ]);
        await sql(`UPDATE users SET avatar = $2, avatar_skipck = 0 WHERE user_id = $1`, [userInfo.id, -id]);
        ctx.body = baseLinkName + ';Next=0';
    } else {
        // Enable third-party avatar (if there isn't a manual one), and skip avatar rechecking for 4 months.
        const skipCheckUntil = ctx.refDate + 120 * 86400;
        await sql(`UPDATE users SET avatar = CASE avatar WHEN 0 THEN -1 ELSE avatar END, avatar_skipck = $2 WHERE user_id = $1`, [userInfo.id, skipCheckUntil]);
        ctx.body = baseLinkName + ';Next=' + skipCheckUntil;
    }

};