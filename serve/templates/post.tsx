import * as elements from 'typed-xhtml';
import { HtmlTmpl, styled } from '../utils/tsx_utils';
import { createCaptchaInfo } from '../utils/captcha';
import { allTagsByCategory, CategoryId, F18_CONSTS } from '../shared/tags';

// Migrated from f18_initial.inc.pug.
const f18Initial: HtmlTmpl<{
    f18Tags: Awaited<ReturnType<typeof allTagsByCategory>>,
    selectedF18Tags: Set<number>
}> = (ctx, { f18Tags, selectedF18Tags }) => {
    const { render } = ctx.state;

    const tagCategory = (catId: CategoryId) => {
        const radio = catId < F18_CONSTS.CATS._FIRST_ARBITRARY;
        return (
            <fieldset id={`tag-group-${catId}`} class="form-group">
                <legend class={'h6' + (radio ? ' fw-bold' : '')}>
                    {render.staticConv(F18_CONSTS.CAT_TITLES[catId])}
                    {radio ? <small class="text-muted">（必选）</small> : null}：
                </legend>
                {f18Tags.get(catId)!.map(tag => {
                    const id = 'tag-sel-' + tag.tag_id;
                    const primaryName =
                        (radio && tag.parentTags
                            ? tag.parentTags
                                .map(parent => parent.primary_name)
                                .join('，') + '—'
                            : '') + tag.primary_name;
                    const fullHint =
                        (radio && tag.parentTags
                            ? tag.parentTags
                                .map(parent => parent.hint)
                                .filter(_ => _)
                                .join('；') + '；'
                            : '') + tag.hint
                    return !(tag.flags & 0x2) ? (
                        <div class="form-check form-check-inline">
                            <input
                                type={radio ? 'radio' : 'checkbox'}
                                name={radio ? `tag-group-${catId}` : 'tag-general'}
                                value={String(tag.tag_id)}
                                id={id}
                                checked={selectedF18Tags.has(tag.tag_id)}
                                class="form-check-input"
                            />
                            {tag.hint ? (
                                <label
                                    for={id}
                                    data-title={render.staticConv(fullHint)}
                                    class="form-check-label mazomirror-tip"
                                >
                                    {render.staticConv(primaryName)}
                                    <small class="text-muted material-icons align-middle" style="font-size: 100%">
                                        info
                                    </small>
                                </label>
                            ) : (
                                <label for={id} class="form-check-label">
                                    {render.staticConv(primaryName)}
                                </label>
                            )}
                        </div>
                    ) : null;
                })}
            </fieldset>
        );
    };

    const categoryBtn = (catId: CategoryId | 0) => (
        <li class={'nav-item' + (catId ? '' : ' ms-auto')}>
            <button type="button" data-id={catId || false} class="nav-link">
                {catId ? render.staticConv(F18_CONSTS.CAT_TITLES[catId]) : '全部'}
            </button>
        </li>
    );

    return (
        <div class="card">
            <div class="card-header d-flex align-items-center justify-content-between">
                小说元信息
                <a
                    href={'/thread/' + F18_CONSTS.CONSTS.README_THREAD_ID}
                    class="material-icons text-muted std-icon"
                >
                    help
                </a>
            </div>
            <div class="card-body">
                {tagCategory(F18_CONSTS.CATS.LENGTH)}
                {tagCategory(F18_CONSTS.CATS.SOURCE)}
                <p class="h6 fw-bold">
                    内容标签<small class="text-muted">（可选）</small>：
                </p>
                <div id="interactive-tag-selector" class="d-none">
                    <div id="selected-tag-alert" class="alert alert-warning d-none" />
                    <div id="selected-tags" class="d-none mb-1">
                        已选择：
                    </div>
                    <div class="position-relative">
                        <input
                            id="tag-searcher"
                            type="text"
                            placeholder="搜索标签……"
                            data-toggle="dropdown"
                            class="form-control w-auto"
                        />
                        <div class="dropdown-menu">
                            <a href="javascript:void(0)" class="dropdown-item disabled">
                                加载中…
                            </a>
                        </div>
                    </div>
                    <div id="recommended-tags" class="mt-2 mb-1" />
                </div>
                <div class="card">
                    <div id="tag-card-header" class="card-header d-none">
                        <ul class="nav nav-tabs card-header-tabs">
                            {categoryBtn(F18_CONSTS.CATS.GENRE)}
                            {categoryBtn(F18_CONSTS.CATS.META)}
                            {categoryBtn(F18_CONSTS.CATS.CHARACTER)}
                            {categoryBtn(F18_CONSTS.CATS.ROLE)}
                            {categoryBtn(F18_CONSTS.CATS.SEX)}
                            {categoryBtn(F18_CONSTS.CATS.PODO)}
                            {categoryBtn(F18_CONSTS.CATS.TORTURE)}
                            {categoryBtn(F18_CONSTS.CATS.GENERAL)}
                            {categoryBtn(0)}
                        </ul>
                    </div>
                    <div id="tag-card-body" class="card-body">
                        <p id="tag-card-hint" class="small">
                            以下皆非必选，但建议选择 2 - 10 个标签
                        </p>
                        {tagCategory(F18_CONSTS.CATS.GENRE)}
                        {tagCategory(F18_CONSTS.CATS.META)}
                        {tagCategory(F18_CONSTS.CATS.CHARACTER)}
                        {tagCategory(F18_CONSTS.CATS.ROLE)}
                        {tagCategory(F18_CONSTS.CATS.SEX)}
                        {tagCategory(F18_CONSTS.CATS.PODO)}
                        {tagCategory(F18_CONSTS.CATS.TORTURE)}
                        {tagCategory(F18_CONSTS.CATS.GENERAL)}
                    </div>
                </div>
                <p class="small text-muted mt-2 mb-0">
                    若没有最合适的选项/标签，请先选择最接近的选项；之后可以在文章浏览页面中提议新的选项/标签。
                </p>
                <script type="text/javascript">{elements.safeHtml(`postTagSelector();`)}</script>
            </div>
        </div>
    );
};

// Migrated from f18_campaign.inc.pug.
const f18Campaign: HtmlTmpl<void> = ctx => (
    <div class="card">
        <div class="card-header">参与征文活动</div>
        <div class="card-body">
            <p>
                原创作品可参与叁孙杯「恶女」题材 M 向小说征文活动（截稿日时期是 8 月 10 号），有多个奖项可摘取；有机会获得
                {' '}
                <span
                    data-title="或等值外币；约 HKD $800 / TWD $3000 / CNY ￥700 / JPY ￥16000"
                    style="text-decoration: underline dotted"
                    class="mazomirror-tip"
                >
                    USD $100
                </span>
                {' '}
                的奖金。
                <br />
                <a href="/thread/1073748496" target="_blank">
                    查看活动详情 »
                </a>
            </p>
            <div class="form-check form-check-inline">
                <input
                    id="mm-tag-campaign-tag"
                    type="checkbox"
                    name="tag-general"
                    value={String(F18_CONSTS.OPEN_CAMPAIGN_TAG_ID)}
                    class="form-check-input"
                />
                <label for="mm-tag-campaign-tag" class="form-check-label">
                    将此作品报名参加征文比赛
                </label>
            </div>
            <script type="text/javascript">{elements.safeHtml(`initCampaignSignupInitial();`)}</script>
        </div>
    </div>
);

// Migrated from post.pug.
export const post: HtmlTmpl<{
    forum: { forum_id: number, forum_name: string },
    thread: { thread_id: number, thread_title: string } | undefined,
    pageTitle: string,
    formWarning: string | undefined,
    formError: string | null,
    replyToRemoteUrl: string | undefined,
    junkWarning: [boolean, boolean] | null,
    postHidden: boolean,
    rateLimitError: Awaited<ReturnType<typeof createCaptchaInfo>> | boolean, // true: hard-fail. false: pass. otherwise a captcha tuple.
    postTitle: string,
    postContent: string,
    canUseHidden: boolean,
    canDelete: boolean,
    f18Tags: Awaited<ReturnType<typeof allTagsByCategory>> | undefined,
    selectedF18Tags: Set<number> | undefined
}> = (ctx, self) => {
    const { render } = ctx.state;
    return styled(`
        #delete-post { display: none; }
        #delete-post:target { display: block; }
        #options-card { display: none; }
        #main-post-form.sp-options #options-card { display: block; }
        #main-post-form.sp-options #show-options-trigger { display: none; }
        #main-post-form.sp-options #hide-options-trigger { display: inline-block !important; }
        #tag-card-body.cat-selected legend { display: none; }
        #tag-card-body fieldset:last-of-type { margin-bottom: 0; }`,
        <div class="container">
            <nav>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="/">首页</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href={'/forum/' + self.forum.forum_id}>
                            {render.staticConv(self.forum.forum_name)}
                        </a>
                    </li>
                    {self.thread ? (
                        <li class="breadcrumb-item">
                            <a href={'/thread/' + self.thread.thread_id}>
                                {render.contentConv(self.thread.thread_title)}
                            </a>
                        </li>
                    ) : null}
                    <li class="breadcrumb-item active">{self.pageTitle}</li>
                </ol>
            </nav>
            {self.formWarning ? (
                <div class="alert alert-warning">
                    {elements.safeHtml(render.staticConv(self.formWarning))}
                </div>
            ) : null}
            {self.formError ? (
                <div class="alert alert-danger">
                    {render.staticConv(self.formError)}
                </div>
            ) : null}
            {self.replyToRemoteUrl ? (
                <div class="alert alert-warning">
                    你所回复的文章发布于源站，你在此发布的回复仅有镜像可见，源站并不可见。若你想确保对方看到你的回复，可以
                    <a href={self.replyToRemoteUrl}>前往源站</a>发布回复。
                </div>
            ) : null}
            {self.junkWarning ? (
                <>
                    <div class="text-center mb-2">
                        <img src="/static/warning.jpg" height="258" />
                    </div>
                    <div class="alert alert-warning">
                        {self.junkWarning[0]
                            ? '请知悉：此主题并无回复可见的内容。'
                            : null}
                        {self.junkWarning[1]
                            ? '你的回复内容似乎并没有意义。'
                            : null}
                        请确认你所要发布的内容确有意义再在下方点击发布，否则你将被永久封禁。
                    </div>
                </>
            ) : null}
            <form
                id="main-post-form"
                method="post"
                class={self.postHidden ? 'sp-options' : ''}
            >
                {self.rateLimitError === true ? (
                    <div class="alert alert-danger">
                        发帖失败：你已超过 24 小时内发帖数量上限（hard limit）。
                        <a href="/confirmation">点击此处</a>了解详情。
                    </div>
                ) : self.rateLimitError ? (
                    <div class="card border-warning mb-3">
                        <div class="card-header">请输入 CAPTCHA</div>
                        <div class="card-body">
                            你最近 24 小时内发帖数量较多，请输入 CAPTCHA 后重试。
                            <a href="/confirmation">点击此处</a>了解详情。
                            <p>
                                <img src={self.rateLimitError.dataUrl} />
                            </p>
                            <input
                                type="text"
                                name="captcha"
                                placeholder="请输入计算结果（例如「一千零四十二」）"
                                class="form-control"
                            />
                            <input type="hidden" name="ref" value={self.rateLimitError.id} />
                        </div>
                    </div>
                ) : null}
                <div class="form-group">
                    <input
                        type="text"
                        name="title"
                        placeholder="标题"
                        value={self.postTitle}
                        class="form-control"
                    />
                </div>
                <div class="form-group">
                    <textarea
                        id="main-textarea"
                        name="content"
                        placeholder="内容"
                        rows='10'
                        class="form-control"
                    >
                        {self.postContent}
                    </textarea>
                    <script type="text/javascript">{elements.safeHtml(`initCodeEditor('${ctx.state.render.locale}');`)}</script>
                </div>
                {self.f18Tags ? (
                    <>
                        <div class="form-group">
                            {f18Initial(ctx, { f18Tags: self.f18Tags, selectedF18Tags: self.selectedF18Tags! })}
                        </div>
                        {F18_CONSTS.OPEN_CAMPAIGN_TAG_ID ? (
                            <div class="form-group">{f18Campaign(ctx)}</div>
                        ) : null}
                    </>
                ) : null}
                <div class="form-group text-center">
                    <script type="text/javascript">{elements.safeHtml(`var flipOptions = function () { document.getElementById('main-post-form').classList.toggle('sp-options'); };`)}</script>
                    {self.canUseHidden ? (
                        <>
                            <a
                                id="show-options-trigger"
                                href="javascript:flipOptions()"
                                class="btn btn-outline-secondary me-2"
                            >
                                显示选项
                            </a>
                            <a
                                id="hide-options-trigger"
                                href="javascript:flipOptions()"
                                class="d-none btn btn-outline-secondary me-2"
                            >
                                隐藏选项
                            </a>
                        </>
                    ) : null}
                    {self.canDelete ? (
                        <a href="#delete-post" class="btn btn-outline-secondary me-2">
                            删除文章
                        </a>
                    ) : null}
                    {self.junkWarning ? (
                        <input type="hidden" name="junk" value="no" />
                    ) : null}
                    <input
                        type="submit"
                        name="submitpost"
                        value="提交"
                        class="btn btn-primary"
                    />
                </div>
                <div id="options-card" class="card">
                    <div class="card-header">选项</div>
                    <div class="card-body">
                        <div class="form-group form-check mb-0">
                            <input
                                id="hide-checkbox"
                                type="checkbox"
                                name="hide"
                                checked={self.postHidden}
                                class="form-check-input"
                            />
                            <label for="hide-checkbox" class="form-check-label">
                                仅回复可见
                            </label>
                            <span
                                data-title="开启回复可见可能导致主题排序偏后"
                                class="mazomirror-tip"
                            >
                                <span class="material-icons std-icon">warning</span>
                            </span>
                        </div>
                    </div>
                </div>
            </form>
            {self.canDelete ? (
                <div id="delete-post" class="card">
                    <div class="card-header">删除文章</div>
                    <form method="post" class="card-body">
                        <div class="form-group">
                            <label>若确认删除，请在下方输入「delete」</label>
                            <input name="delete" type="text" class="form-control" />
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-primary">
                                确认删除
                            </button>
                        </div>
                    </form>
                </div>
            ) : null}
        </div>
    );
};