import { SPAM_THRESHOLD, THRESHOLD_FOR_LOCAL } from '../../common/consts';
import { query as sql } from '../db';
import { tagPref } from '../shared/tags';
import { parseInt32 } from '../utils/logic_utils';
import { HtmlSegmentImpl, LOGIN_REQUIRED, PageImpl, RpcImpl, error, finalizePage, finalizeSegment, redirectWithDelay, redirectWithoutDelay } from '../utils/render_utils';
import { CONFIRMATION_ADDITIONALS, CONFIRMATION_CONSTS, pmRateLimitTest, rateLimitGroup, rateLimitTest } from '../shared/author';
import { THREAD_RENDER_COLS, renderThreads } from '../shared/thread_table';

import * as templates from '../templates/author';
import { dualAvatar } from '../utils/avatar';

export const author_id: PageImpl = async ctx => {
    const loginUser = ctx.state.mazoLoginUser;
    if (!loginUser) { throw error(403, LOGIN_REQUIRED); }
    if (ctx.params.id === 'me') {
        return redirectWithoutDelay(ctx, '/author/' + loginUser.id + (ctx.querystring ? '?' + ctx.querystring : ''));
    }
    const userId = parseInt32(ctx.params.id);
    if (isNaN(userId)) { throw error(400, '又在乱改网址了吗……'); }
    if (userId === 0) {
        throw error(404, '该作者注册于源站，但已将其在源站的用户注销，故没有用户页面可供显示～');
    }
    const userMeta = (await sql<{ user_name: string; registered_at: number; flags: number; has_password: boolean; label: string | null; email: string | null; avatar: number; }, [number]>('SELECT user_name, registered_at, flags, password_h IS NOT NULL AS has_password, label, email, avatar FROM users WHERE user_id = $1', [userId])).rows[0];
    if (!userMeta) {
        const tombstone = (await sql<{ new_user_id: number; }>('SELECT new_user_id FROM user_tombstone WHERE prev_user_id = $1', [userId])).rows[0];
        if (!tombstone) { throw error(404, '该作者不存在来着（'); }
        return redirectWithDelay(ctx, {
            message: '该用户已合并至另一用户',
            targetText: '该用户的新页面',
            targetHref: '/author/' + tombstone.new_user_id
        });
    }
    const userName = userMeta.user_name;

    const profile: { main_text: string, pinned_threads: number[] | null } | undefined = (await sql('SELECT main_text, pinned_threads FROM user_profile WHERE user_id = $1 ORDER BY rev_id DESC LIMIT 1', [userId])).rows[0];

    const pinnedThreads = profile?.pinned_threads; // Either undefined or non-empty array.
    const threads = (await sql(`SELECT ${THREAD_RENDER_COLS.join(', ')} FROM thread_targets WHERE owner_user_id = $1 AND last_page_number > 0 AND successive_fail >= 0 AND spam_likeness < ${SPAM_THRESHOLD} ORDER BY${pinnedThreads ? ' ARRAY_POSITION($2::INT[], thread_id) ASC NULLS LAST, ' : ' '}firstpost_date DESC LIMIT 10`, pinnedThreads ? [userId, pinnedThreads] : [userId])).rows;

    const [threadCount, postCount, favCount, collectionCount] = (await Promise.all([
        'SELECT COUNT(thread_targets.thread_id)::INT AS cnt FROM thread_targets WHERE owner_user_id = $1 AND last_page_number > 0 AND successive_fail >= 0',
        'SELECT COUNT(post_id)::INT AS cnt FROM posts p INNER JOIN thread_targets USING (thread_id) WHERE user_id = $1 AND p.most_recent_update >= 0 AND successive_fail >= 0',
        'SELECT COUNT(thread_id)::INT AS cnt FROM thread_stars WHERE user_id = $1 AND star_type > 0 AND fc_ids <> ARRAY[0]',
        'SELECT COUNT(fc_id)::INT AS cnt FROM fav_collections WHERE user_id = $1'
    ].map(sqlText => sql<{ cnt: number; }, [number]>(sqlText, [userId])))).map(res => res.rows[0].cnt);

    const isOriginal = userId <= THRESHOLD_FOR_LOCAL;
    const isLocal = userMeta.has_password;

    let localRegisterDate = 0;
    if (isOriginal && isLocal) {
        const tombstone = (await sql<{ registered_at: number; }>('SELECT registered_at FROM user_tombstone WHERE new_user_id = $1 AND prev_user_id >= 0', [userId])).rows[0]; // Should be at most one row. Not enforced at DB level but should be.
        if (tombstone) {
            localRegisterDate = tombstone.registered_at;
        } else {
            console.error(`Tombstone not found for user ${userId}! This should never happen.`);
        }
    }

    const violationCount = (await sql(`SELECT COUNT(admin_log_id)::INT AS cnt FROM local_admin_log WHERE flags & 3 = 0 AND ((target_type = 'thread' AND (SELECT owner_user_id FROM thread_targets WHERE thread_id = target_id) = $1) OR (target_type = 'post' AND (SELECT user_id FROM posts WHERE post_id = target_id) = $1) OR (target_type = 'user' AND target_id = $1))`, [userId])).rows[0].cnt;

    const canAdminUser = loginUser.admin;
    const isSelf = (userId === loginUser.id) && !('preview' in ctx.query);

    return finalizePage(ctx, {
        title: userName,
        canonicalPath: '/author/' + userId,
        nav: true,
        deps: canAdminUser ? ['ADMIN_JS'] : undefined
    }, templates.author(ctx, {
        userName,
        userId,
        registeredAt: userMeta.registered_at,
        profileText: profile?.main_text.split('\n') ?? null,
        avatarSmallInt: userMeta.avatar,
        lastLocalAvatar: isSelf ? ((userMeta.avatar < -1) ? -userMeta.avatar : (await sql<{ avatar_id: number }>('SELECT avatar_id FROM user_avatars WHERE user_id = $1 AND flags & 1 <> 0 ORDER BY updated_at DESC LIMIT 1', [userId])).rows[0]?.avatar_id ?? 0) : 0,
        blocked: !!(userMeta.flags & 2),
        confirmed: !!(userMeta.flags & 4),
        email: userMeta.email,
        label: userMeta.label,
        isOriginal,
        isLocal,
        localRegisterDate,
        threadCount,
        postCount,
        favCount,
        collectionCount,
        // Note that pinned threads would be rendered differently by JavaScript.
        threads: await renderThreads(ctx, threads, await tagPref(ctx), loginUser.id),
        pinnedThreadsCount: pinnedThreads?.length ?? 0,
        violationCount,
        isSelf,
        canAdminUser
    }));
};

export const apiAuthor_id: HtmlSegmentImpl = async ctx => {
    if (!ctx.state.mazoLoginUser) { throw error(403, LOGIN_REQUIRED); }
    const userId = parseInt32(ctx.params.id);
    if (isNaN(userId)) { throw error(400, '又在乱改网址了吗……'); }
    if (userId === 0) {
        throw error(404, '该作者注册于源站，但已将其在源站的用户注销，故没有用户页面可供显示～');
    }

    const [[meta], [profile], achievements, [threadCount], [postCount]] = (await Promise.all([
        sql('SELECT user_name, registered_at, flags, password_h IS NOT NULL AS has_password, label, email, avatar FROM users WHERE user_id = $1', [userId]),
        sql('SELECT main_text FROM user_profile WHERE user_id = $1 ORDER BY rev_id DESC LIMIT 1', [userId]),
        sql('SELECT name_sc, MAX(lvl) AS mlv FROM user_achievements WHERE user_id = $1 GROUP BY name_sc ORDER BY mlv DESC, MAX(granted) DESC', [userId]),
        sql('SELECT COUNT(thread_targets.thread_id)::INT AS cnt FROM thread_targets WHERE owner_user_id = $1 AND last_page_number > 0 AND successive_fail >= 0', [userId]),
        sql('SELECT COUNT(post_id)::INT AS cnt FROM posts p INNER JOIN thread_targets USING (thread_id) WHERE user_id = $1 AND p.most_recent_update >= 0 AND successive_fail >= 0', [userId]),
    ] as const)).map(res => res.rows);

    if (!meta) { throw error(404, '该作者不存在来着（'); }

    if (meta.label) {
        const labeledAchievementIndex = achievements.findIndex(row => row.name_sc === meta.label);
        if (labeledAchievementIndex < 0) {
            console.error(`Should never happen: achievement ${meta.label} not for ${userId}`);
        } else if (labeledAchievementIndex > 0) { // === 0: already the first one. do nothing.
            achievements.unshift(achievements.splice(labeledAchievementIndex, 1)[0]);
        }
    }

    return finalizeSegment(ctx, templates.authorHovercard(ctx, {
        id: userId,
        name: meta.user_name,
        isLocal: meta.has_password,
        threadCount: threadCount.cnt,
        postCount: postCount.cnt,
        profileText: profile?.main_text,
        avatar: dualAvatar(userId, meta.user_name, meta.email, meta.avatar, true),
        achievements: achievements.map(single => [single.name_sc, single.mlv])
    }));
};

export const apiAuthor_id_threads: RpcImpl = async ctx => {
    const userId = parseInt32(ctx.params.id);
    if (isNaN(userId)) { throw error(400, '又在乱改网址了吗……'); }
    const loginUser = ctx.state.mazoLoginUser;
    if (!loginUser || (loginUser.id !== userId)) {
        // Only intended to be used by the user itself, but doesn't hurt anyway.
    }

    const profile: { pinned_threads: number[] | null } | undefined = (await sql('SELECT pinned_threads FROM user_profile WHERE user_id = $1 ORDER BY rev_id DESC LIMIT 1', [userId])).rows[0];
    const pinnedThreads = profile?.pinned_threads ?? []; // Either undefined or non-empty array.

    const allThreads = (await sql<{ thread_id: number, thread_title: string, firstpost_date: number }>(`SELECT thread_id, thread_title, firstpost_date FROM thread_targets WHERE owner_user_id = $1 AND last_page_number > 0 AND successive_fail >= 0 AND spam_likeness < ${SPAM_THRESHOLD} ORDER BY firstpost_date DESC`, [userId])).rows;

    const { render } = ctx.state;
    ctx.body = [
        allThreads.map(row => [row.thread_id, render.contentConv(row.thread_title), render.renderDate(row.firstpost_date)]),
        pinnedThreads
    ];
};

export const confirmation: PageImpl = async ctx => {
    if (!ctx.state.mazoLoginUser) { throw error(403, LOGIN_REQUIRED); }

    return finalizePage(ctx, {
        title: ctx.state.render.staticConv('关于发帖频率限制和「资深用户」'),
        nav: true
    }, templates.userRules(ctx, {
        confirmed: ctx.state.mazoLoginUser.confirmed,
        preview: {
            group: rateLimitGroup(ctx.refDate, ctx.state.mazoLoginUser),
            pms: await pmRateLimitTest(ctx.refDate, ctx.state.mazoLoginUser.id),
            ...await rateLimitTest(ctx.refDate, ctx.state.mazoLoginUser.id)
        }
    }));
};

export const postConfirmation: PageImpl = async ctx => {
    if (!ctx.state.mazoLoginUser) { throw error(403, LOGIN_REQUIRED); }
    if (ctx.state.mazoLoginUser.blocked) { throw error(403, LOGIN_REQUIRED); }
    type LocalRow = { thread_id: number; thread_title: string; posted_date: number; num_repliers: number; };
    const firstMirrorPost = (await sql('SELECT post_id, title, posted_date FROM posts p INNER JOIN thread_targets USING (thread_id) WHERE user_id = $1 AND p.most_recent_update >= 0 AND successive_fail >= 0 AND p.post_id > $2 ORDER BY post_id ASC LIMIT 1', [ctx.state.mazoLoginUser.id, THRESHOLD_FOR_LOCAL])).rows[0];
    const mirrorPostCount = (await sql<{ cnt: number; }>('SELECT COUNT(post_id)::INT AS cnt FROM posts p INNER JOIN thread_targets USING (thread_id) WHERE user_id = $1 AND p.most_recent_update >= 0 AND successive_fail >= 0 AND p.post_id > $2', [ctx.state.mazoLoginUser.id, THRESHOLD_FOR_LOCAL])).rows[0].cnt;

    let validCount = 0;
    const eligibleThreads = (await sql<LocalRow>('WITH aggregated_threads AS (SELECT thread_id, thread_title, COALESCE((SELECT posted_date FROM posts WHERE posts.thread_id = thread_targets.thread_id ORDER BY post_id ASC LIMIT 1), $2) AS posted_date, (SELECT COUNT(DISTINCT user_id)::INT - 1 FROM posts WHERE posts.thread_id = thread_targets.thread_id AND posts.most_recent_update >= 0) AS num_repliers FROM thread_targets WHERE last_page_number > 0 AND successive_fail >= 0 AND owner_user_id = $1) SELECT thread_id, thread_title, posted_date, num_repliers FROM aggregated_threads ORDER BY (LEAST($3, num_repliers) + LEAST($4, ($2 - posted_date) / 86400)) DESC LIMIT 5', [ctx.state.mazoLoginUser.id, ctx.refDate, CONFIRMATION_CONSTS[0][0], CONFIRMATION_CONSTS[0][1]])).rows.map(thread => {
        const days = Math.floor(((ctx.refDate - thread.posted_date) / 86400));
        let valid = 0;
        if ((days >= CONFIRMATION_CONSTS[0][1]) && (thread.num_repliers >= CONFIRMATION_CONSTS[0][0])) {
            valid = 2;
        } else if ((days >= CONFIRMATION_CONSTS[1][1]) && (thread.num_repliers >= CONFIRMATION_CONSTS[1][0])) {
            valid = 1;
        }
        validCount += valid;
        return Object.assign(thread, { days, valid });
    });
    const threadValid = validCount >= 2;
    const firstPostValid = firstMirrorPost && (ctx.refDate - firstMirrorPost.posted_date >= 86400 * CONFIRMATION_ADDITIONALS.daysSinceMirror);
    const postCountValid = mirrorPostCount >= CONFIRMATION_ADDITIONALS.postCount;
    if (threadValid && firstPostValid && postCountValid) {
        await sql('UPDATE users SET flags = flags | 4::SMALLINT WHERE user_id = $1 RETURNING user_id, user_name', [ctx.state.mazoLoginUser.id]);
    }

    return finalizePage(ctx, {
        title: ctx.state.render.staticConv('资深用户'),
        nav: true
    }, templates.userConfirmation(ctx, {
        threads: eligibleThreads,
        firstMirrorPost,
        threadValid, firstPostValid, mirrorPostCount, postCountValid
    }));
};

export const postAuthor_id: PageImpl = async ctx => {
    const userId = parseInt32(ctx.params.id);
    if (isNaN(userId)) { throw error(400, '又在乱改网址了吗……'); }
    const loginUser = ctx.state.mazoLoginUser;
    if (!loginUser || (loginUser.id !== userId)) { throw error(403, '不可以偷偷地改别人的资料（除非你是对方的主人）。'); }
    const updated: string[] = [];

    const profileText = ctx.request.body.string('introduction');
    if (profileText || ctx.request.body.string('profile')) {
        if (profileText.startsWith('PreviousUserId=')) { throw error(400, 'PreviousUserId 是特殊值，不可设置。但你可放心删除它。'); }
        const oldContentRows = (await sql('SELECT main_text, updated_at FROM user_profile WHERE user_id = $1 ORDER BY rev_id DESC LIMIT 5', [userId])).rows;
        const oldContent = oldContentRows.length ? oldContentRows[0].main_text : '';
        if (profileText !== oldContent) {
            updated.push('个人简介');
            if ((oldContentRows.length >= 5) && (ctx.refDate - oldContentRows[4].updated_at < 86400)) {
                throw error(429, '修改过于频繁啦！24 小时内不可修改超过 5 次。');
            }
            await sql('INSERT INTO user_profile (user_id, main_text, updated_at, pinned_threads) VALUES ($1, $2, $3, (SELECT pinned_threads FROM user_profile WHERE user_id = $1 ORDER BY rev_id DESC LIMIT 1))', [userId, profileText, ctx.refDate]);
        }
    }

    // Note: the avatar settings here only applies to third-party avatar, disabled, or a reuse of a first party avatar. First party avatar uploading goes to avatar.ts.
    const avatarSetting = ctx.request.body.integer('avatar');
    switch (avatarSetting) {
        case 2: // Third-party; force refresh.
            await sql(`UPDATE users SET avatar = $2, avatar_skipck = $3 WHERE user_id = $1`, [userId, ((ctx.refDate - 1728970000) / 600) & 0x7fff, ctx.refDate + 120]);
            updated.push('头像设置');
            break;
        case 1: // Third-party; regular enable.
            await sql(`UPDATE users SET avatar = -1, avatar_skipck = $2 WHERE user_id = $1`, [userId, ctx.refDate + 120]);
            updated.push('头像设置');
            break;
        case -1: // Disable.
            await sql(`UPDATE users SET avatar = 0, avatar_skipck = $2 WHERE user_id = $1`, [userId, ctx.refDate + 120]);
            updated.push('头像设置');
            break;
        case -2: // Reuse the latest first-party.
            await sql(`UPDATE users SET avatar = COALESCE(-(SELECT avatar_id FROM user_avatars WHERE user_id = $1 AND flags & 1 <> 0 ORDER BY updated_at DESC LIMIT 1), 0), avatar_skipck = $2 WHERE user_id = $1`, [userId, 0]);
            updated.push('头像设置');
    }

    const titleSetting = ctx.request.body.string('title');
    if (titleSetting) {
        if (titleSetting === 'null') {
            await sql(`UPDATE users SET label = NULL WHERE user_id = $1`, [userId]);
        } else {
            const row = (await sql('SELECT granted FROM user_achievements WHERE user_id = $1 AND name_sc = $2 AND lvl = 3', [userId, titleSetting])).rows[0];
            if (!row) { throw error(404, `你并无「${titleSetting}」成就。`); }
            await sql(`UPDATE users SET label = $2 WHERE user_id = $1`, [userId, titleSetting]);
        }
        updated.push('头衔设置');
    }

    const pinnedThreads = ctx.request.body.integerArray('pinned');
    if (pinnedThreads && pinnedThreads.length) {
        const pinnedThreadIds = pinnedThreads.filter(_ => _); // There will be a NaN entry.
        await sql('INSERT INTO user_profile (user_id, main_text, updated_at, pinned_threads) VALUES ($1, (SELECT main_text FROM user_profile WHERE user_id = $1 ORDER BY rev_id DESC LIMIT 1), $2, $3)', [userId, ctx.refDate, pinnedThreadIds.length ? pinnedThreadIds : null]);
        updated.push('置顶主题帖');
    }

    return redirectWithDelay(ctx, {
        message: updated.length ? '你已成功修改' + updated.join('、') : '你未修改任何内容',
        targetText: '你的个人页面',
        targetHref: ctx.path,
        numberOfSeconds: 2
    });
};