import { query as sql } from '../db';
import { qnaire24Fields } from '../shared/questionnaire';
import { logSpecialRequest } from '../utils/logic_utils';
import { error, finalizePage, LOGIN_REQUIRED, PageImpl, redirectWithDelay } from '../utils/render_utils';

import * as templates from '../templates/questionnaire';
import { ROOT_USER } from '../shared/login';

export const questionnaire: PageImpl = async ctx => {
    const loginUser = ctx.state.mazoLoginUser;
    if (!loginUser) { throw error(403, LOGIN_REQUIRED); }
    const record = (await sql('SELECT request_value FROM stats.special_requests WHERE user_id = $1 AND request_key = \'qnaire24\' ORDER BY rev_id DESC LIMIT 1', [loginUser.id])).rows;
    
    return finalizePage(ctx, {
        title: '社区管理意见征询',
        nav: true
    }, templates.questionnaire(ctx, record[0]?.request_value));
};

export const adminQuestionnaire: PageImpl = async ctx => {
    const loginUser = ctx.state.mazoLoginUser;
    if (loginUser?.id !== ROOT_USER) { throw error(403, 'Not authorized.'); }
    
    const records = (await sql('SELECT user_id, request_value, user_name, at_date FROM (SELECT DISTINCT ON (user_id) user_id, request_value, at_date FROM stats.special_requests WHERE request_key = \'qnaire24\' ORDER BY user_id, rev_id DESC) AS sr LEFT JOIN users USING (user_id)')).rows;

    return finalizePage(ctx, {
        title: '社区管理意见征询结果',
        nav: false
    }, templates.adminQuestionnaire(ctx, records));
};

export const postQuestionnaire: PageImpl = async ctx => {
    const loginUser = ctx.state.mazoLoginUser;
    if (!loginUser) { throw error(403, LOGIN_REQUIRED); }

    const body = Object.fromEntries(qnaire24Fields.map(key => [key, ctx.request.body.string(key).substring(0, 10000)]).filter(([k, v]) => v));
    console.log('Qnaire input', body);
    logSpecialRequest(ctx, 'qnaire24', body);
    return redirectWithDelay(ctx, {
        message: '已提交，非常感谢',
        targetHref: '/',
        targetText: '首页'
    });
};