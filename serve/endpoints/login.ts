import crypto = require('crypto');

import { ORIGIN_ROOT_WTS, THRESHOLD_FOR_LOCAL } from '../../common/consts';
import { unawaited } from '../../common/logic_utils';
import * as conf from '../conf';
import { query as sql } from '../db';
import * as options from '../shared/options';
import * as login from '../shared/login';
import { createCaptchaInfo, validateCaptcha } from '../utils/captcha';
import { logSpecialRequest } from '../utils/logic_utils';
import { TEMPLATE_REGISTER, TEMPLATE_UPDATE, registrationAttemptCounterByEmail, sendMail as registrationEmail } from '../utils/mailer';
import { LOGIN_REQUIRED, PageImpl, error, finalizePage, redirectWithDelay, redirectWithoutDelay } from '../utils/render_utils';
import * as srcAccount from '../utils/src_account';

import * as templates from '../templates/login';

export const postLogin: PageImpl = async ctx => {
    const username = ctx.request.body.string('username');
    const password = ctx.request.body.string('password');
    if (!username || !password) { throw error(400, '请输入用户名和密码。'); }

    const userInfo = ctx.state.mazoLoginUser;
    if (userInfo) {
        return redirectWithDelay(ctx, {
            message: '你已经登入啦～',
            targetText: '你的个人页面',
            targetHref: '/author/' + userInfo.id
        });
    }

    if (!login.loginAttemptCounter.attempt(ctx.ip)) {
        throw error(429, '尝试注册或者登入的次数太多啦……（；´д｀）一小时后再来试试吧！');
    }

    // Add the ORDER BY clause so that a local user will come before a source user.
    const row = (await sql<{ user_id: number, has_password: boolean, password_match: boolean }>('SELECT user_id, password_h IS NOT NULL AS has_password, (SHA224(CONVERT_TO($2 || \'-O-\' || email, \'UTF8\')) = password_h) AS password_match FROM users WHERE user_name = $1 ORDER BY (password_h IS NULL) ASC', [username, password])).rows[0];
    if (!row) { throw error(404, '该用户不存在。'); }

    if (!row.has_password) {
        throw error(403, '你只在源站注册过，没有在镜像注册过。麻烦你重新在镜像注册一下，多谢啦～');
    }

    if (!row.password_match) {
        throw error(403, '密码错误。' + ((row.user_id <= THRESHOLD_FOR_LOCAL) ? '请使用你在镜像而非源站的密码啦！' : ''));
    }

    await login.doLogin(row.user_id, ctx);
    return redirectWithoutDelay(ctx, 'back', '/');
};

export const logout: PageImpl = async ctx => {
    if (!ctx.state.mazoLoginUser) { throw error(403, '你本来就没登入吧？'); }
    await sql('UPDATE user_auth SET session_id = -ABS(session_id) WHERE session_id = $1', [ctx.state.mazoLoginUser.sessionId]);
    ctx.cookies.set(conf.COOKIE_PREFIX + 'metro', null, options.cookieOptions);
    const referrer = ctx.get('referrer');
    return redirectWithDelay(ctx, {
        message: '已成功登出～',
        targetText: referrer ? '上个页面' : '首页',
        targetHref: referrer || '/'
    });
};

export const register: PageImpl = async ctx => {
    const captcha = await createCaptchaInfo();
    return finalizePage(ctx, {
        title: ctx.state.render.staticConv('注册'),
        nav: false
    }, templates.register(ctx, {
        username: '', password: '', retype: '', email: '', nonce: '', captcha
    }));
};

const allowedEmailHosts = new Set(['126.com', '139.com', '163.com', '188.com', 'aol.com', 'foxmail.com', 'gmail.com', 'hotmail.com', 'icloud.com', 'live.cn', 'live.com', 'msn.com', 'outlook.com', 'protonmail.com', 'qq.com', 'sina.cn', 'sina.com', 'sohu.com', 'tom.com', 'vip.qq.com', 'yahoo.com', 'yahoo.com.tw', 'yahoo.co.jp']);

const validateEmailFormat = (email: string) => {
    const emailParts = email.split('@');
    if ((emailParts.length !== 2) || !/^[a-z0-9\-_\.]+$/.test(emailParts[0])) { throw error(400, '邮箱格式错误！'); }
    if (!allowedEmailHosts.has(emailParts[1])) {
        console.log(`Rejected uncommon email: ${email}`);
        throw error(400, `${emailParts[1]} 目前不被支持……换个更常见的邮箱试试吧（例如：${Array.from(allowedEmailHosts).join('、')}）。`);
    };
};

const prepareToken = (email: string): { token: string, nonce: string } => {
    const nonce = crypto.randomBytes(6).toString('hex');
    const tokenDate = Date.now();
    return {
        token: tokenDate.toString(36) + '_' + login.generateHmacToken(email, tokenDate, nonce),
        nonce
    };
};

const validateEmailToken = (email: string, tokenPosted: string, nonce: string) => {
    const tokenSections = tokenPosted.split('_').filter(str => str);
    if (tokenSections.length !== 2) { throw error(400, '邮件验证码不太对呢……'); }
    const tokenDate = parseInt(tokenSections[0], 36);
    if (isNaN(tokenDate) || (Date.now() - tokenDate > 7200 * 1000)) { throw error(403, '可恶！邮件验证码失效啦！请在验证码发送后的两小时内输入吧～') }
    const expectedToken = login.generateHmacToken(email, tokenDate, nonce);
    if (tokenSections[1] !== expectedToken) {
        throw error(403, '邮件验证码不太对呢……');
    }
};

export const postRegister: PageImpl = async ctx => {
    const username = ctx.request.body.string('username');
    if (!username) { throw error(400, '请输入用户名。'); }
    if (username.length < 2 || username.length > 12) { throw error(400, '用户名的长度要在2-12之间。'); }
    if (!/^([A-Za-z0-9\-_~]|[\u4E00-\u9FA5]|[\u3400-\u4DB5])+$/.test(username)) { throw error(400, '用户名中含有非法字符。'); }

    const email = ctx.request.body.string('email').toLowerCase();
    if (!email) { throw error(400, '请输入邮箱。'); }
    validateEmailFormat(email);

    const password = ctx.request.body.string('password');
    if (!password) { throw error(400, '请输入密码。'); }
    if (password.length < 6 || password.length > 24) { throw error(400, '密码的长度要在6-24之间。'); }
    if (/^\d+$/.test(password)) { throw error(400, '密码不可以是纯数字哦～'); }
    if (!email) { throw error(400, '请输入邮箱。'); }
    if (ctx.request.body.string('retype') !== password) { throw error(400, '重新输入的密码与之前不符。'); }

    if (ctx.state.mazoLoginUser) { throw error(403, '你已经登入了哦～'); }
    if ((await sql('SELECT user_id FROM users WHERE LOWER(user_name) = LOWER($1)', [username])).rows.length) { throw error(400, '用户名已被占用……'); }
    if ((await sql('SELECT user_id FROM users WHERE email = $1', [email])).rows.length) { throw error(400, '这个邮箱已经被使用了来着>-<'); }

    const noncePosted = ctx.request.body.string('nonce');
    if (noncePosted) { // Actual registering after verification.
        const tokenPosted = ctx.request.body.string('token');
        if (!tokenPosted) {
            throw error(400, '你尚未填写邮箱验证码！');
        }
        validateEmailToken(email, tokenPosted, noncePosted);
        const newConfigSerialized = options.serializeConfig({ ...ctx.state.userConfig, sync: true });
        const userId = (await sql('INSERT INTO users (user_id, user_name, registered_at, email, password_h, avatar, configs) VALUES (nextval(\'local_user_id\'), $1, $2, $3, SHA224(CONVERT_TO($4 || \'-O-\' || $3, \'UTF8\')), -1, $5) RETURNING user_id', [username, ctx.refDate, email, password, newConfigSerialized])).rows[0].user_id;
        await login.doLogin(userId, ctx);
        return redirectWithDelay(ctx, {
            message: `已成功注册为 ${username}～`,
            targetText: '你的个人页面',
            targetHref: '/author/' + userId
        });
    } else { // Sending verification email only.
        if (!login.registrationAttemptCounterByIp.attempt(ctx.ip)) {
            throw error(429, '尝试注册或者登入的次数太多啦……（；´д｀）一小时后再来试试吧！');
        }
        const ref = ctx.request.body.string('ref');
        const captcha = ctx.request.body.string('captcha');

        if (!ref || !captcha) { throw error(400, '我知道三位数减法很难啦……但是也要好好填上去哦～还是说……你是机器人呢？'); }
        try {
            validateCaptcha(ref, captcha);
        } catch (e) {
            throw error(400, '验证码输入错误。详情：' + (e as Error).message);
        }

        if (!registrationAttemptCounterByEmail.attempt(email)) {
            throw error(429, '你刚刚才往这个邮箱发送过验证邮件……请等两分钟再试吧。');
        }
        const token = prepareToken(email);
        unawaited(registrationEmail({email, userName: username, code: token.token, conv: ctx.state.render.staticConv}, TEMPLATE_REGISTER), 'sending email');
        return finalizePage(ctx, {
            title: ctx.state.render.staticConv('注册'),
            nav: false
        }, templates.register(ctx, {
            username, password, retype: ctx.request.body.string('retype'), email, nonce: token.nonce, captcha: null
        }));
    }
};

export const postUserSecurity: PageImpl = async ctx => {
    const userInfo = ctx.state.mazoLoginUser;
    if (!userInfo) { throw error(403, LOGIN_REQUIRED); }

    let effectiveEmail: string | undefined;
    const emailPosted = ctx.request.body.string('email').toLowerCase();
    const oldpass = ctx.request.body.string('oldpass');
    const newpass = ctx.request.body.string('newpass');
    const retype = ctx.request.body.string('retype');
    const tokenPosted = ctx.request.body.string('token');
    let nonce = ctx.request.body.string('nonce');

    if (emailPosted || oldpass || newpass || retype) {
        if (!oldpass) {
            throw error(400, '请输入原先的密码。');
        }
        const row = (await sql<{ email: string, password_match: boolean }>('SELECT email, (SHA224(CONVERT_TO($2 || \'-O-\' || email, \'UTF8\')) = password_h) AS password_match FROM users WHERE user_id = $1', [userInfo.id, oldpass])).rows[0];
        if (!row) {
            throw error(404, '用户信息未找到。这不可能！');
        }
        if (!row.password_match) {
            throw error(403, '你输入的当前密码不正确！');
        }

        if (emailPosted && (emailPosted !== row.email)) {
            validateEmailFormat(emailPosted);
            if ((await sql('SELECT user_id FROM users WHERE email = $1', [emailPosted])).rows.length) { throw error(400, '这个邮箱已经被其它用户使用了来着>-<'); }
        }
        effectiveEmail = emailPosted || row.email;

        if (newpass) {
            if (newpass.length < 6 || newpass.length > 24) { throw error(400, '密码的长度要在6-24之间。'); }
            if (/^\d+$/.test(newpass)) { throw error(400, '密码不可以是纯数字哦～'); }
            if (retype !== newpass) { throw error(400, '两遍输入的新密码不相同。'); }
        }
        const effectivePassword = newpass || oldpass;

        const logObj: Record<string, string> = {};
        const updatedTexts: string[] = [];
        if (effectivePassword !== oldpass) {
            logObj.password = oldpass;
            updatedTexts.push('密码');
        }
        if (effectiveEmail !== row.email) {
            logObj.email = row.email;
            updatedTexts.push('邮箱');
        }
        if (!updatedTexts.length) {
            throw error(400, '你填写的和之前一样，完全没修改呀！')
        }

        if (nonce) { // Actual updating afgter verification.
            if (!tokenPosted) {
                throw error(400, '你尚未填写邮箱验证码！');
            }
            validateEmailToken(effectiveEmail, tokenPosted, nonce);
            await sql('UPDATE users SET password_h = SHA224(CONVERT_TO($1 || \'-O-\' || $2, \'UTF8\')), email = $2 WHERE user_id = $3', [effectivePassword, effectiveEmail, userInfo.id]);
            logSpecialRequest(ctx, 'usersec-update', logObj);
            return redirectWithDelay(ctx, {
                message: `你的${updatedTexts.join('和')}已经修改成功！`,
                targetHref: '/author/' + userInfo.id,
                targetText: '你的用户页'
            });
        } else { // Sending registration email only.
            if (!login.updateInfoAttemptCounterByUser.attempt(userInfo.id)) {
                throw error(429, '修改得太频繁啦（；´д｀）两小时后再来试试吧！');
            }
            // registrationAttemptCounterByEmail is skipped intentionally to improve user experience: they can use any email at this point so don't force them to use another.

            const token = prepareToken(effectiveEmail);
            nonce = token.nonce;
            unawaited(registrationEmail({email: effectiveEmail, userName: userInfo.name, code: token.token, conv: ctx.state.render.staticConv}, TEMPLATE_UPDATE), 'sending email');
        }
    }

    return finalizePage(ctx, {
        title: ctx.state.render.staticConv('修改资料'),
        nav: true
    }, templates.updateInfo(ctx, {
        oldpass, newpass, retype, effectiveEmail, email: emailPosted, nonce
    }));
};

export const merge: PageImpl = async ctx => {
    if (!ctx.state.mazoLoginUser) { throw error(403, LOGIN_REQUIRED); }
    if (ctx.state.mazoLoginUser.blocked) { throw error(403, LOGIN_REQUIRED); }
    if (ctx.state.mazoLoginUser.id <= THRESHOLD_FOR_LOCAL) {
       throw error(403, '你已经绑定过一个源站用户了，不能再次绑定～');
    }
    const profileVerificationCode = srcAccount.generateProfileToken(ctx.state.mazoLoginUser.id);
    return finalizePage(ctx, {
        title: ctx.state.render.staticConv('验证源站用户'),
        nav: true
    }, templates.mergeUser(ctx, { originRoot: ORIGIN_ROOT_WTS, profileVerificationCode }));
};

export const postMerge: PageImpl = async ctx => {
    if (!ctx.state.mazoLoginUser) { throw error(403, LOGIN_REQUIRED); }
    if (ctx.state.mazoLoginUser.blocked) { throw error(403, LOGIN_REQUIRED); }
    const username = ctx.request.body.string('username');
    if (!username) { throw error(400, '未输入源站用户名。'); }
    const password = ctx.request.body.string('password');
    const method = ctx.request.body.string('method');
    let call;
    switch (method) {
        case 'password':
            if (!password) { throw error(400, '未输入源站密码。'); }
            call = srcAccount.doPasswordMethod(ctx, ctx.state.mazoLoginUser.id, username, password);
            break;
        case 'profile':
            call = srcAccount.doProfileMethod(ctx, ctx.state.mazoLoginUser.id, username);
            break;
        default:
            throw error(400, '未指定验证方式。')
    }
    try {
        const numberOfPostsUpdated = await call;
        return redirectWithDelay(ctx, {
            message: `您的用户名已成功由「${ctx.state.mazoLoginUser.name}」更改为「${username}」～您之前以「${ctx.state.mazoLoginUser.name}」发布的 ${numberOfPostsUpdated} 个帖子现已归至「${username}」下。此后请使用「${username}」用户名登入。`,
            targetText: '首页',
            targetHref: '/',
            numberOfSeconds: 10
        });
    } catch (e) {
        throw error(400, (e as Error).message);
    }
};
