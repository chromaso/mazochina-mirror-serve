import { $, loadingSpinner, positionPop, $xhr, $set } from "../common-import";

const SPECIAL_KEY_FOR_HOVERCARD = '_skha'; // Link from anchor to card.
const SPECIAL_KEY_FOR_BACKLINK = '_skhb'; // Backlink from card to anchor.
const SPECIAL_KEY_FOR_TIMEOUT_IN = '_skoi'; // On anchor: timeout before showing.
const SPECIAL_KEY_FOR_TIMEOUT_OUT = '_skot'; // On hovercard: timeout before closing.
const SPECIAL_KEY_FOR_WINDOW_LISTENER = '_skwl'; // On hovercard: event listener to window

const isAuthorAnchor = (target: HTMLAnchorElement): boolean => 
    // Note it's intended to not show a hovercard when there is a trailing "/". We don't want a hovercard in some cases.
    (/^\/author\/\d+$/.test(target.getAttribute('href') || ''));

const findInsertPoint = (anchor: HTMLElement): HTMLElement => {
    const parentChain: HTMLElement[] = [];
    for (let i = anchor.parentNode; i instanceof HTMLElement; i = i.parentNode) {
        parentChain.push(i);
    }
    // The last element in `parentChain` must be <html> or <body>.
    let insertPoint = anchor.offsetParent as HTMLElement;
    for (let i = parentChain.length - 1; i >= 0; i--) {
        if ((parentChain[i] === document.body) || (parentChain[i] === document.documentElement)) { continue; }
        const attempt = parentChain[i];
        const overflowX = getComputedStyle(attempt)['overflow'].split(' ')[0];
        if (overflowX !== 'visible') { // A overflow-hidden one. Let's stay right outside it!
            insertPoint = attempt.offsetParent as HTMLElement;
            break;
        }
    }
    return insertPoint;
};

export const initHovercard = (level: 0 | 1 | 2): void => {
    const showHoverCard = (anchor: HTMLAnchorElement, x: number) => {
        delete (anchor as any)[SPECIAL_KEY_FOR_TIMEOUT_IN];

        const body = $('div', 'card-body', loadingSpinner(false));
        body.style.width = '320px';
        const pop = $('div', 'position-absolute card', body);
        pop.style.zIndex = '1050'; // https://getbootstrap.com/docs/4.6/layout/overview/#z-index
        pop.style.pointerEvents = 'auto'; // Otherwise it might be inserted under `.modal-dialog` which has `pointer-events: none`. This bug be reproduced by putting a hovercard deep inside a modal dialog on dekstop Chrome (Firefox is fine).

        (anchor as any)[SPECIAL_KEY_FOR_HOVERCARD] = pop;
        (pop as any)[SPECIAL_KEY_FOR_BACKLINK] = anchor;
        findInsertPoint(anchor).appendChild(pop);
        positionPop(anchor, pop, x);

        const listener = () => closeHovercard(pop);
        (pop as any)[SPECIAL_KEY_FOR_WINDOW_LISTENER] = listener;
        window.addEventListener('pagehide', listener);

        $xhr('/api' + anchor.pathname, {
            t: text => { body.innerHTML = text; },
            e: error => $set(body, $('div', 'alert alert-danger', '加载失败：' + error)),
            c: true
        });
    };

    const closeHovercard = (target: HTMLDivElement) => {
        if (!(SPECIAL_KEY_FOR_BACKLINK in target)) { return; }
        target.parentNode?.removeChild(target);
        delete (target[SPECIAL_KEY_FOR_BACKLINK] as any)[SPECIAL_KEY_FOR_HOVERCARD];

        if (SPECIAL_KEY_FOR_WINDOW_LISTENER in target) {
            window.removeEventListener('pagehide', (target as any)[SPECIAL_KEY_FOR_WINDOW_LISTENER]);
            delete target[SPECIAL_KEY_FOR_WINDOW_LISTENER];
        }

        delete target[SPECIAL_KEY_FOR_BACKLINK];
    };

    if (level === 1) {
        const openedHovercards: HTMLDivElement[] = [];

        document.addEventListener('click', (e: MouseEvent) => {
            const keepCardsOpen = openedHovercards.map(() => false);

            let target: ParentNode | null = e.target as HTMLElement;
            while (target instanceof HTMLElement) {
                if (target instanceof HTMLAnchorElement) {
                    if (isAuthorAnchor(target)) {
                        if (!(SPECIAL_KEY_FOR_HOVERCARD in target)) { // If the user clicks on the link again while the card is open, just follow the link, don't preventDefault().
                            showHoverCard(target, e.x);
                            openedHovercards.push((target as any)[SPECIAL_KEY_FOR_HOVERCARD]);
                            e.preventDefault();
                        }
                    }
                } else if (target instanceof HTMLDivElement) {
                    const index = openedHovercards.indexOf(target);
                    if (index >= 0) {
                        keepCardsOpen[index] = true;
                    }
                }
                target = target.parentNode;
            }

            for (let index = keepCardsOpen.length - 1; index >= 0; index--) {
                // Moving backwards as we'll need to delete from `openedHovercards`.
                if (!keepCardsOpen[index]) {
                    closeHovercard(openedHovercards.splice(index, 1)[0]);
                }
            }
        });
        return;
    } else if (level === 2) {
        document.addEventListener('mouseenter', ({ x, target: target }: MouseEvent) => {
            if ((target instanceof HTMLDivElement) && (SPECIAL_KEY_FOR_TIMEOUT_OUT in target)) {
                // Moving into the hovercard itself, maybe from the anchor.
                clearTimeout(target[SPECIAL_KEY_FOR_TIMEOUT_OUT] as any);
                delete target[SPECIAL_KEY_FOR_TIMEOUT_OUT];
                return;
            }
            if (!((target instanceof HTMLAnchorElement) && isAuthorAnchor(target))) {
                return;
            }
            if (SPECIAL_KEY_FOR_HOVERCARD in target) {
                // There is already a hovercard. Maybe moving from the hovercard back to the anchor.        
                const card = target[SPECIAL_KEY_FOR_HOVERCARD] as HTMLDivElement;
                if (SPECIAL_KEY_FOR_TIMEOUT_OUT in card) {
                    clearTimeout(card[SPECIAL_KEY_FOR_TIMEOUT_OUT] as any);
                    delete card[SPECIAL_KEY_FOR_TIMEOUT_OUT];
                }
                return;
            }
            if (SPECIAL_KEY_FOR_TIMEOUT_OUT in target) {
                return;
            }
            (target as any)[SPECIAL_KEY_FOR_TIMEOUT_IN] = setTimeout(() => showHoverCard(target, x), 125);
        }, true);

        document.addEventListener('mouseleave', ({ target }: MouseEvent) => {
            if ((target instanceof HTMLDivElement) && (SPECIAL_KEY_FOR_BACKLINK in target)) {
                // Moving outside the hovercard itself.
                (target as any)[SPECIAL_KEY_FOR_TIMEOUT_OUT] = setTimeout(() => closeHovercard(target), 50);
                return;
            }
            if (!(target instanceof HTMLAnchorElement)) { return; }
            if (SPECIAL_KEY_FOR_TIMEOUT_IN in target) {
                clearTimeout(target[SPECIAL_KEY_FOR_TIMEOUT_IN] as ReturnType<typeof setTimeout>);
                delete target[SPECIAL_KEY_FOR_TIMEOUT_IN];
                return;
            }
            if (!(SPECIAL_KEY_FOR_HOVERCARD in target)) { return; }
            const card = target[SPECIAL_KEY_FOR_HOVERCARD] as HTMLDivElement;
            (card as any)[SPECIAL_KEY_FOR_TIMEOUT_OUT] = setTimeout(() => closeHovercard(card), 50);
        }, true);
    }
};
