export class RateLimiter<T> {
    private rotated: number;
    private current: Map<T, number> = new Map();
    private last: Map<T, number> = new Map();

    constructor(private readonly threshold: number, private readonly windowSizeInSeconds: number) {
        this.rotated = Math.floor(Date.now() / 1000);
    }

    housekeep(): void {
        const time = Math.floor(Date.now() / 1000);
        if (time - this.rotated > this.windowSizeInSeconds * 2) {
            this.last = new Map();
            this.current = new Map();
            this.rotated = time;
        } else if (time - this.rotated > this.windowSizeInSeconds) {
            this.last = this.current;
            this.current = new Map();
            this.rotated = time;
        }
    }

    attempt(key: T): boolean {
        this.housekeep();
        const existingCount = this.current.get(key) || 0;
        const oldCount = this.last.get(key) || 0;
        if (existingCount + oldCount >= this.threshold) {
            return false;
        }
        this.current.set(key, existingCount + 1);
        return true;
    }

    blackout(key: T) { 
        this.housekeep();
        this.current.set(key, this.threshold + 1);
    }
}