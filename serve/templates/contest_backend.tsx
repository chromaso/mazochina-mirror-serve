import * as elements from 'typed-xhtml';
import { HtmlTmpl, styled } from '../utils/tsx_utils';
import { fullContestStats, SingleOut } from '../shared/contest_backend';

// Migrated from contest_admin.pug.
export const contestAdmin: HtmlTmpl<{
    TITLES_OF_COMPONENTS: Readonly<string[]>,
    panelMembers: { user_id: number, user_name: string }[],
    threadRows: {
        thread_id: number; userLink: string, userName: string; link: string; title: string;
    }[],
} & Awaited<ReturnType<typeof fullContestStats>>> = (ctx, self) => {
    const { render } = ctx.state;

    const formatJson = (k: any, v: any) => (typeof v === 'number') ? Number(v.toFixed(3)) : v;

    const renderComponents = (scores: number[], expls: (string | undefined)[], ranks: string[], metricOffset: number) => 
        self.TITLES_OF_COMPONENTS.map((component, index) => (
            <div>
                {component}：
                {expls[index] ? (
                    <span class="mazomirror-tip" style='text-decoration: underline dotted' data-title={render.staticConv(expls[index]!)}>
                        {Math.round(scores[index])}
                    </span>
                ) : (
                    <span>{Math.round(scores[index])}</span>
                )}
                <a class="s-rank" href={`javascript:mmcsDrawScat(${metricOffset + index * 2 + 1}, ${metricOffset + index * 2})`}>
                    {ranks[index]}
                </a>
            </div>
        ));

    return styled(`
        .scs-dod, .scs-std { font-size: 80%; padding: 3px; margin: 2px; display: none; }
        .scs-dod { border: 1px solid #007bff; border-radius: 2px; }
        .scs-std { border: 1px solid #28a745; border-radius: 2px; }
        .s-rank { font-size: 80%; padding: 3px; margin: 2px; border: 1px solid #dc3545; border-radius: 2px; display: none; color: inherit; }
        #main-tbody.show-dod .scs-dod { display: block; }
        #main-tbody.show-std .scs-std { display: block; }
        #main-tbody.show-rank .s-rank { display: inline-block; }`,
        <div id="root-container" class="container">
            <h1 class="mb-4">征文比赛 Dashboard</h1>
            <div class="card">
                <div class="card-header">添加新的评委</div>
                <div class="card-body">
                    <form method="post" class="d-flex align-items-center">
                        <div class="input-group w-auto">
                            <div class="input-group-text">用户名</div>
                            <input
                                type="text"
                                name="add-username"
                                placeholder="评委的完整用户名"
                                class="form-control w-auto"
                            />
                        </div>
                        <button type="submit" class="btn btn-primary ms-2">
                            添加
                        </button>
                    </form>
                </div>
            </div>
            <hr class="my-4" />
            <div class="form-check">
                <input id="tg-fluid" type="checkbox" class="form-check-input" />
                <label for="tg-fluid" class="form-check-label">
                    宽屏幕显示
                </label>
            </div>
            <div class="form-check">
                <input id="tg-ranking" type="checkbox" class="form-check-input" />
                <label for="tg-ranking" class="form-check-label">
                    显示每项数字在所有作品中的
                    <div class="s-rank d-inline">
                        排名
                    </div>
                    （= 为并列）；点击排名数字可显示该项排名的分布
                </label>
            </div>
            <table class="table table-bordered table-striped mt-3">
                <thead>
                    <tr>
                        <th scope="row">
                            <div class="text-end">评委</div>
                            <div class="text-start">作者＼</div>
                        </th>
                        {self.panelMembers.map((panelMember, memberIndex) => (
                            <th scope="col">
                                <a href={'/author/' + panelMember.user_id}>
                                    {panelMember.user_name}
                                </a>
                                <form
                                    method="post"
                                    onsubmit="return window.confirm('将此用户移除出评委列表？')"
                                >
                                    <input
                                        type="hidden"
                                        name="remove-userid"
                                        value={String(panelMember.user_id)}
                                    />
                                    <button type="submit" class="btn btn-sm btn-outline-secondary">
                                        <span class="material-icons">delete</span>
                                    </button>
                                </form>
                                <a href={`javascript:mmcsDraw("scores", ${(32 + memberIndex * 20)})`}>
                                    📊
                                </a>
                            </th>
                        ))}
                        <th>
                            平均分（原始）
                            <div class="form-check">
                                <input id="tg-steps-dod" type="checkbox" class="form-check-input" />
                                <label for="tg-steps-dod" class="fw-normal form-check-label" >
                                    显示计算过程
                                </label>
                            </div>
                            <a href='javascript:mmcsDraw("scores", 12)'>📊</a>
                        </th>
                        <th>
                            平均分（<a href="https://zh.wikipedia.org/wiki/%E6%A8%99%E6%BA%96%E5%88%86%E6%95%B8">标准化</a>）
                            <span class="mazomirror-tip" data-title="对每个评分数字，按照该评委给所有作品的该维度给出的分数为集，计算标准分数（z），然后以「(0.2z+0.5)×满分」作为校正后的该项分数">
                                <span class="material-icons std-icon">info</span>
                            </span>
                            <div class="form-check">
                                <input id="tg-steps-std" type="checkbox" class="form-check-input" />
                                <label for="tg-steps-std" class="fw-normal form-check-label">
                                    显示计算过程
                                </label>
                            </div>
                            <a href='javascript:mmcsDraw("scores", 22)'>📊</a>
                        </th>
                        <th>
                            读者投票
                            <br />
                            <a href='javascript:mmcsDraw("votes")'>📊</a>
                        </th>
                    </tr>
                </thead>
                <tbody id="main-tbody">
                    {self.threadRows.map((thread, threadIndex) => {
                        const stats = self.stats[threadIndex];
                        const ranks = self.ranks[threadIndex];
                        return [
                            <tr>
                                <td>
                                    <a href={thread.userLink}>{thread.userName}</a>
                                </td>
                                <td colspan={self.panelMembers.length}>
                                    <a href={thread.link} class="s-thread-title">
                                        {thread.title}
                                    </a>
                                </td>
                                <td rowspan="2">
                                    {renderComponents(stats.dodAgg, [], ranks.dodAgg, 12)}
                                    <div class="scs-dod">左侧各蓝框取平均数</div>
                                </td>
                                <td rowspan="2">
                                    {renderComponents(stats.stdAgg, [], ranks.stdAgg, 22)}
                                    <div class="scs-std">左侧各绿框取平均数</div>
                                </td>
                                <td rowspan="2">
                                    👍 {stats.voteMetrics[0]}
                                    <a href="javascript:mmcsDrawScat(3, 2)" class="s-rank">
                                        {ranks.voteMetrics[0]}
                                    </a>
                                    <br />
                                    + 🥇 {stats.voteMetrics[1]}
                                    <a href="javascript:mmcsDrawScat(5, 4)" class="s-rank">
                                        {ranks.voteMetrics[1]}
                                    </a>
                                    <br />
                                    = {stats.voteMetrics[2]}
                                    <a href="javascript:mmcsDrawScat(7, 6)" class="s-rank">
                                        {ranks.voteMetrics[2]}
                                    </a>
                                </td>
                            </tr>,
                            <tr>
                                <td>
                                    <small class="text-muted">色/文/合/创</small>
                                </td>
                                {self.panelMembers.map((panelMember, reviewerIndex) => {
                                    const lastestReview = stats.reviewers[reviewerIndex];
                                    return (
                                        <td>
                                            {lastestReview.rawScores ? (
                                                <span>{lastestReview.rawScores.join('／')}</span>
                                            ) : (
                                                <small class="text-muted">未评分</small>
                                            )}
                                            <br />
                                            <div class="scs-dod">
                                                {renderComponents(lastestReview.dodScores,
                                                lastestReview.dodScoresExpl,
                                                ranks.reviewers[reviewerIndex].dodScores, 32 +
                                                reviewerIndex * 20)}
                                            </div>
                                            <div class="scs-std">
                                                {renderComponents(lastestReview.stdScores,
                                                lastestReview.stdScoresExpl,
                                                ranks.reviewers[reviewerIndex].stdScores, 42 +
                                                reviewerIndex * 20)}
                                            </div>
                                            {(lastestReview.textLength > 0) ? (
                                                <span>
                                                    <a href={`javascript:mmcaShowContentModal(${thread.thread_id}, ${panelMember.user_id})`}>
                                                        评语
                                                    </a>
                                                    <small>（约 {lastestReview.textLength} 字）</small>
                                                </span>
                                            ) : (
                                                <small class="text-muted">无评语</small>
                                            )}
                                            <br />
                                            {(lastestReview.at <= 0) ? (
                                                <a
                                                    href={`javascript:mmcaShowEditModal(${thread.thread_id}, ${panelMember.user_id})`}
                                                    style='font-size: 18px'
                                                    class="material-icons"
                                                >
                                                    edit
                                                </a>
                                            ) : (
                                                <small class="text-muted">
                                                    {render.renderDate(lastestReview.at)}
                                                </small>
                                            )}
                                        </td>
                                    );
                                })}
                            </tr>,
                        ];
                    })}
                </tbody>
                <tfoot>
                    <tr>
                        <th scope="row">
                            <div class="text-start">作者／</div>
                            <div class="text-end">评委</div>
                        </th>
                        {self.panelMembers.map((panelMember, memberIndex) => (
                            <th scope="col">
                                <a href={'/author/' + panelMember.user_id}>
                                    {panelMember.user_name}
                                </a>
                                <br />
                                <a href={`javascript:mmcsDraw("scores", ${(32 + memberIndex * 20)})`}>
                                    📊
                                </a>
                            </th>
                        ))}
                        <th>
                            平均分（原始）
                            <br />
                            <a href='javascript:mmcsDraw("scores", 12)'>📊</a>
                        </th>
                        <th>
                            平均分（
                            <a href="https://zh.wikipedia.org/wiki/%E6%A8%99%E6%BA%96%E5%88%86%E6%95%B8">
                                标准化
                            </a>
                            ）
                            <span
                                data-title="对每个评分数字，按照该评委给所有作品的该维度给出的分数为集，计算标准分数（z），然后以「(0.2z+0.5)×满分」作为校正后的该项分数"
                                class="mazomirror-tip"
                            >
                                <span class="material-icons std-icon">info</span>
                            </span>
                            <br />
                            <a href='javascript:mmcsDraw("scores", 22)'>📊</a>
                        </th>
                        <th>
                            读者投票
                            <br />
                            <a href='javascript:mmcsDraw("votes")'>📊</a>
                        </th>
                    </tr>
                </tfoot>
            </table>
            <hr class="my-4" />
            <div class="card">
                <div class="card-header">数据相关性</div>
                <div class="card-body">
                    有趣的统计，看看评委之间的口味有多不同、评委和广大读者的口味是否不同；看看标准化分数是否会改变排名结果；看看早投稿会带来多大的读者投票优势…
                    <div>
                        先选横轴：
                        <select id="s-colaxh" class="form-control d-inline-block w-auto" />
                    </div>
                    <div>
                        再选纵轴：
                        <select id="s-colaxv" class="form-control d-inline-block w-auto" />
                        （r 为两项数据之间的<a href="https://zh.wikipedia.org/wiki/%E7%9A%AE%E5%B0%94%E9%80%8A%E7%A7%AF%E7%9F%A9%E7%9B%B8%E5%85%B3%E7%B3%BB%E6%95%B0">样本相关系数</a>）
                    </div>
                    <div id="scatter-target" />
                </div>
            </div>
        <script type="text/javascript">{elements.safeHtml(`mmcaInitCharts(${JSON.stringify(self.metricsForPlot, formatJson)}, ${JSON.stringify(self.correlations, formatJson)}, ${JSON.stringify(self.TITLES_OF_COMPONENTS)});`)}</script>
        </div>
    );
};
  
// Migrated from contest.pug.
export const contest: HtmlTmpl<{
    forewordAt: number,
    foreword: string | undefined,
    threadRows: {
        thread_id: number, thread_title: string
    }[],
    latestReviews: Map<number, SingleOut>
}> = (ctx, self) => {
    const { render } = ctx.state;

    const deviation = (valueNum: number) => (
        <div class="badge bg-secondary mazomirror-tip ms-1" data-title={`比你给所有作品打的此项分数的平均值${(valueNum > 0) ? '高' : '低'}出 ${Math.abs(valueNum).toFixed(2)} 个标准差`}>
            {valueNum > 0 ? '+' : null}
            {valueNum.toFixed(1)}σ
        </div>
    );

    return (
        <div class="container">
            <h1 class="mb-4">征文比赛评委点评</h1>
            <div id="foreword-edit" class="card">
                <div class="card-header">评委前言</div>
                <div class="card-body">
                    {self.forewordAt ? (
                        <>
                            {elements.safeHtml(self.foreword!)}
                            <hr />
                            <small class="text-muted">（保存于 {render.renderDate(self.forewordAt)}）
                            </small>
                            <button id="foreword-btn" class="btn btn-primary">
                                <span class="material-icons">edit</span>编辑
                            </button>
                        </>
                    ) : (
                        <button id="foreword-btn" class="btn btn-primary">
                            <span class="material-icons">edit</span>撰写
                        </button>
                    )}
                </div>
            </div>
            <hr class="my-4" />
            <div class="alert alert-secondary">
                此处列出你给
                <a href="/thread/1073748496" class="alert-link">
                    叁孙杯「恶女」题材 M 向小说征文活动
                </a>
                所有参赛作品评分/评语的进度。请前往每篇作品页面的底部填写或修改评分/评语。
            </div>
            <table class="table table-bordered table-striped mt-4">
                <thead>
                    <tr>
                        <th scope="col">作品</th>
                        <th scope="col">
                            评分<small>（色/文/合/创）</small>
                        </th>
                        <th scope="col">评语</th>
                    </tr>
                </thead>
                <tbody>
                    {self.threadRows.map(thread => {
                        const lastestReview = self.latestReviews.get(thread.thread_id)!;
                        return (
                            <tr>
                                <td>
                                    <a href={'/thread/' + thread.thread_id}>
                                        {render.contentConv(thread.thread_title)}
                                    </a>
                                </td>
                                {lastestReview ? (
                                    <>
                                        <td>
                                            {lastestReview.scores ? (
                                                <>
                                                    <div>
                                                        {lastestReview.scores.join('／')}
                                                        <button class="btn btn-sm btn-link mm-btnexplnk">
                                                            <span class="material-icons">
                                                                expand_more
                                                            </span>
                                                        </button>
                                                    </div>
                                                    <div class="d-none">
                                                        色情度：{lastestReview.scores[0]}
                                                        <small class="text-muted">/100</small>
                                                        {deviation(lastestReview.devs![0])}
                                                        <br />
                                                        文学性：{lastestReview.scores[1]}
                                                        <small class="text-muted">/80</small>
                                                        {deviation(lastestReview.devs![1])}
                                                        <br />
                                                        合题性：{lastestReview.scores[2]}
                                                        <small class="text-muted">/80</small>
                                                        {deviation(lastestReview.devs![2])}
                                                        <br />
                                                        创意加分：{lastestReview.scores[3]}
                                                        <small class="text-muted">/40</small>
                                                        {deviation(lastestReview.devs![3])}
                                                        <br />
                                                        <b>
                                                            总分：
                                                            {lastestReview.scores.reduce((a, b) => a + b)}
                                                        </b>
                                                        <small class="text-muted">/300</small>
                                                        <button class="btn btn-sm btn-link">
                                                            收起
                                                            <span class="material-icons">
                                                                expand_less
                                                            </span>
                                                        </button>
                                                    </div>
                                                </>
                                            ) : (
                                                <i class="text-muted">无评分</i>
                                            )}
                                        </td>
                                        <td>
                                            <small class="text-muted">
                                                {lastestReview.at > 0 ? (
                                                    <>
                                                        （保存于 {render.renderDate(lastestReview.at)}）
                                                    </>
                                                ) : (
                                                    <>
                                                        （由管理员保存于{' '}
                                                        {render.renderDate(-lastestReview.at)}）
                                                    </>
                                                )}
                                            </small>
                                            {lastestReview.html ? (
                                                <>
                                                    <div>
                                                        <button class="btn btn-sm btn-link mm-btnexplnk">
                                                            评语共 {lastestReview.length} 字
                                                            <span class="material-icons">
                                                                expand_more
                                                            </span>
                                                        </button>
                                                    </div>
                                                    <div class="d-none">
                                                        <div>
                                                            {elements.safeHtml(render.contentConv(lastestReview.html))}
                                                        </div>
                                                        <button class="btn btn-sm btn-link">
                                                            收起
                                                            <span class="material-icons">
                                                                expand_less
                                                            </span>
                                                        </button>
                                                    </div>
                                                </>
                                            ) : (
                                                <div>
                                                    <i class="text-muted">无评语</i>
                                                </div>
                                            )}
                                        </td>
                                    </>
                                ) : (
                                    <td colspan={2}>
                                        <i class="text-muted">从未填写</i>
                                    </td>
                                )}
                            </tr>
                        );
                    })}
                </tbody>
            </table>
            <script type="text/javascript">{elements.safeHtml(`contestRBPanelInit();`)}</script>
        </div>
    );
}
