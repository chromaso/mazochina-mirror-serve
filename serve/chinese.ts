import { OpenCC } from 'opencc';
import { LRUMap } from 'lru_map';

export const OCC_SC: Pick<OpenCC, 'convertSync' | 'convertPromise'> = new OpenCC('t2s.json');
export const OCC_TC: Pick<OpenCC, 'convertSync' | 'convertPromise'> = new OpenCC('s2t.json');

const CONV_SC_CACHE = new LRUMap<string, string>(4096);
const CONV_TC_CACHE = new LRUMap<string, string>(4096);

export const CONV_KEEP = (text: string): string => text;

export const CONV_SC = (text: string): string => {
    let val = CONV_SC_CACHE.get(text);
    if (!val) {
        val = OCC_SC.convertSync(text);
        CONV_SC_CACHE.set(text, val);
    }
    return val;
};

export const CONV_TC = (text: string): string => {
    let val = CONV_TC_CACHE.get(text);
    if (!val) {
        val = OCC_TC.convertSync(text);
        CONV_TC_CACHE.set(text, val);
    }
    return val;
};