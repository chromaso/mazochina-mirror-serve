import { LENGTH_THRESHOLD_FOR_UPDATING_SORTPOST_DATE } from '../../common/consts';
import { rowsToMap, threadNeedsSpecialSortDateCalc } from '../../common/logic_utils';
import { query as sql } from '../db';

const PAGE_SIZE = 800;
const SLEEP_BETWEEN_PAGES = 500;

// Returns true iff there's another page to process.
const auditThreadTargetsColumns = async (page: number): Promise<boolean> => {
    const data = (await sql<{ thread_id: number, firstpost_date: number, lastpost_date: number, sortpost_date: number, owner_user_id: number, post_count: number, lastpost_id: number, lastpost_user_id: number, flags: number, spam_likeness: number }>('SELECT thread_id, post_count, owner_user_id, firstpost_date, lastpost_id, lastpost_date, lastpost_user_id, flags, sortpost_date, spam_likeness FROM thread_targets ORDER BY thread_id ASC LIMIT $1 OFFSET $2', [PAGE_SIZE, page * PAGE_SIZE])).rows;
    if (!data.length) { return false; }
    const minThreadId = data[0].thread_id;
    const maxThreadId = data[data.length - 1].thread_id;
    console.log(`Threads [${minThreadId}, ${maxThreadId}]`);
    const postRows = rowsToMap((await sql('SELECT DISTINCT thread_id, COUNT(*) OVER w AS post_count, FIRST_VALUE(user_id) OVER w AS owner_user_id, FIRST_VALUE(posted_date) OVER w AS firstpost_date, LAST_VALUE(post_id) OVER w AS lastpost_id, LAST_VALUE(posted_date) OVER w AS lastpost_date, LAST_VALUE(user_id) OVER w AS lastpost_user_id, BIT_OR(flags) OVER w AS all_flags FROM posts WHERE thread_id >= $1 AND thread_id <= $2 AND most_recent_update >= 0 WINDOW w AS (PARTITION BY thread_id ORDER BY posted_date ASC, post_id ASC RANGE BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)', [minThreadId, maxThreadId])).rows, 'thread_id');
    const fakeTxn = async (sql: string, params: any[]) => {
        if (!sql.startsWith('SELECT BIT_OR(flags) AS all_flags') || (params.length !== 1)) { throw Error('Fake used wrongly!'); }
        return { rows: [postRows.get(params[0])] };
    };
    for (const tRow of data) {
        const threadId = tRow.thread_id;
        let pRow = postRows.get(threadId);
        const assertEq = (key: string & keyof typeof tRow) => {
            if (tRow[key] !== pRow[key]) {
                console.error(`Fail ${threadId}[${key}]: threads ${tRow[key]} vs ${pRow[key]} posts`);
            }
        };
        if (!pRow) {
            pRow = { post_count: 0 };
            assertEq('post_count');
            continue;
        }
        assertEq('owner_user_id');
        assertEq('firstpost_date');
        assertEq('lastpost_id');
        assertEq('lastpost_date');
        assertEq('lastpost_user_id');
        const specialSortPostDate = await threadNeedsSpecialSortDateCalc(tRow, fakeTxn);
        if (!specialSortPostDate) {
            pRow.sortpost_date = pRow.lastpost_date;
        } else {
            pRow.sortpost_date = (await sql(`SELECT posted_date FROM posts WHERE thread_id = $1 AND posts.most_recent_update >= 0 AND LENGTH(REGEXP_REPLACE(SUBSTRING(content_sc FROM POSITION('\ufffc' IN content_sc) + 1), '\\s+', '', 'g')) > $2 ORDER BY posted_date DESC, post_id DESC LIMIT 1`, [threadId, LENGTH_THRESHOLD_FOR_UPDATING_SORTPOST_DATE])).rows[0]?.posted_date ?? pRow.firstpost_date;
        }
        assertEq('sortpost_date');
    }
    return data.length >= PAGE_SIZE;
};

if (require.main === module) {
    (async (action: string) => {
        if (action !== 'threads') { console.warn('No action specified.'); return; }
        let more = true;
        for (let page = 0; more; page++) {
            more = await auditThreadTargetsColumns(page);
            await new Promise(res => setTimeout(res, SLEEP_BETWEEN_PAGES));
        }
    })(process.argv[2]);
}