import { query as sql } from '../db';

import * as login from './login';

export const RATE_LIMIT_CONSTS = [
    [5, 10, 1], [10, 20, 2], [15, 30, 3], [40, 80, 8] // Soft limit posts, hard limit posts, PM conversation initiations
] as const;

export const CONFIRMATION_CONSTS = [
    [20, 75], [15, 60] // See user_confirmation.pug for their meanings.
] as const;

export const CONFIRMATION_ADDITIONALS = {
    daysSinceMirror: 45,
    postCount: 25
} as const;

type RateLimitStat = { posts: number; threads: number; }

export const rateLimitGroup = (now: number, user: login.UserInfo) => (user.confirmed ? 3 : ((user.registered < now - 86400) ? ((user.registered < now - 3 * 86400) ? 2 : 1) : 0));

export const rateLimitTest = async (now: number, userId: number): Promise<RateLimitStat> => {
    const windowStart = now - 86400;
    const [posts, threads] = (await Promise.all([
        sql<{ cnt: number }>('SELECT COUNT(post_id)::INT as cnt FROM posts WHERE user_id = $1 AND posted_date >= $2', [userId, windowStart]), 
        sql<{ cnt: number }>('SELECT COUNT(thread_id)::INT as cnt FROM thread_targets WHERE owner_user_id = $1 AND COALESCE((SELECT posted_date FROM posts WHERE posts.thread_id = thread_targets.thread_id ORDER BY post_id ASC LIMIT 1), $2) >= $2', [userId, windowStart])
    ])).map(res => res.rows[0].cnt);
    return {
        posts, threads
    };
};

export const pmRateLimitTest = async (now: number, userId: number): Promise<number> => {
    const maxProbingCount = RATE_LIMIT_CONSTS[RATE_LIMIT_CONSTS.length - 1][2] + 1;
    const windowStart = now - 86400;
    const rows = (await sql<{ first_date: number }>('SELECT thread_id, (SELECT posted_date FROM local_pms WHERE local_pms.thread_id = local_conversations.thread_id ORDER BY post_id ASC LIMIT 1) AS first_date FROM local_conversations WHERE from_user = $1 ORDER BY thread_id DESC LIMIT $2', [userId, maxProbingCount])).rows;
    const count = rows.filter(entry => entry.first_date >= windowStart).length;
    return (count >= maxProbingCount) ? Number.POSITIVE_INFINITY : count;
};
