import * as elements from 'typed-xhtml';
import { HtmlTmpl } from '../utils/tsx_utils';

// Migrated from iast_stats.pug.
export const iastStats: HtmlTmpl<{
    outputs: { escaped: string, display: string, count: number, percentage: string }[],
    isSelect: boolean
}> = (ctx, self) => self.outputs.length ? (
    <>
        <p>以下为{self.isSelect ? '做出各选择' : '填写各内容'}的用户数：</p>
        <table class="table">
            <tbody>
                {self.outputs.map(row => [
                    <tr>
                        <td data-option={row.escaped} class="border-bottom-0">
                            {ctx.state.render.contentConv(row.display)}
                        </td>
                        <td class="border-bottom-0 text-end">
                            <b>{row.count}</b>
                        </td>
                    </tr>,
                    <tr>
                        <td colspan="2" class="border-top-0 pt-0">
                            <div class="progress">
                                <div
                                    style={`width: ${row.percentage}`}
                                    class="progress-bar"
                                />
                            </div>
                        </td>
                    </tr>
                ])}
            </tbody>
        </table>
        {!self.isSelect ? (
            <p class="small">
                *：为隐私起见，仅有当三个以上用户输入相同内容时，才会单列统计；否则将会计入「其它」。
            </p>
        ) : null}
    </>
) : (
    <div class="alert alert-info">当前尚无任何用户做出交互</div>
);
