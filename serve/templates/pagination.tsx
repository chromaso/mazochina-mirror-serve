import * as elements from 'typed-xhtml';
import { HtmlTmpl } from '../utils/tsx_utils';

export type PaginationParams = {
    currentPage: number,
    baseLink: string,
    linkExtra: string,
    maxPage: number
};

// Migrated from pagination.pug.
export const pagination: HtmlTmpl<PaginationParams> = (ctx, self) => (
    <nav class="mt-5 mb-4">
        <ul class="pagination justify-content-center d-none d-sm-flex">
            {(self.currentPage !== 1) ? (
                <li class="page-item">
                    <a href={self.baseLink + '/' + (self.currentPage - 1) + self.linkExtra} class="page-link">
                        «
                    </a>
                </li>
            ) : null}
            {(self.currentPage - 2 > 1) ? (
                <>
                    <li class="page-item">
                        <a href={self.baseLink + '/1' + self.linkExtra} class="page-link">
                            1
                        </a>
                    </li>
                    <li class={"page-item disabled" + ((self.currentPage - 2 === 2) ? ' d-none' : '')}>
                        <a href={self.baseLink + '/:page:' + self.linkExtra} class="page-link">
                            …
                        </a>
                    </li>
                </>
            ) : null}
            {(self.currentPage - 2 >= 1) ? (
                <li class="page-item">
                    <a href={self.baseLink + '/' + (self.currentPage - 2) + self.linkExtra} class="page-link">
                        {self.currentPage - 2}
                    </a>
                </li>
            ) : null}
            {(self.currentPage - 1 >= 1) ? (
                <li class="page-item">
                    <a href={self.baseLink + '/' + (self.currentPage - 1) + self.linkExtra} class="page-link">
                        {self.currentPage - 1}
                    </a>
                </li>
            ) : null}
            <li class="page-item active">
                <a href={self.baseLink + '/' + self.currentPage + self.linkExtra} class="page-link">
                    {self.currentPage}
                </a>
            </li>
            {(self.currentPage + 1 <= self.maxPage) ? (
                <li class="page-item">
                    <a href={self.baseLink + '/' + (self.currentPage + 1) + self.linkExtra} class="page-link">
                        {self.currentPage + 1}
                    </a>
                </li>
            ) : null}
            {(self.currentPage + 2 <= self.maxPage) ? (
                <li class="page-item">
                    <a href={self.baseLink + '/' + (self.currentPage + 2) + self.linkExtra} class="page-link">
                        {self.currentPage + 2}
                    </a>
                </li>
            ) : null}
            {(self.currentPage + 2 < self.maxPage) ? (
                <>
                    <li class={"page-item disabled" + ((self.currentPage + 2 === self.maxPage - 1) ? ' d-none' : '')}>
                        <a href={self.baseLink + '/:page:' + self.linkExtra} class="page-link">
                            …
                        </a>
                    </li>
                    <li class="page-item">
                        <a href={self.baseLink + '/' + self.maxPage + self.linkExtra} class="page-link">
                            {self.maxPage}
                        </a>
                    </li>
                </>
            ) : null}
            {(self.currentPage !== self.maxPage) ? (
                <li class="page-item">
                    <a href={self.baseLink + '/' + (self.currentPage + 1) + self.linkExtra} class="page-link">
                        »
                    </a>
                </li>
            ) : null}
        </ul>
        <ul class="pagination justify-content-center d-sm-none">
            {(self.currentPage - 1 > 1) ? (
                <>
                    <li class="page-item">
                        <a href={self.baseLink + '/1' + self.linkExtra} class="page-link">
                            1
                        </a>
                    </li>
                    <li class={"page-item disabled" + ((self.currentPage - 1 === 2) ? ' d-none' : '')}>
                        <a href={self.baseLink + '/:page:' + self.linkExtra} class="page-link">
                            …
                        </a>
                    </li>
                </>
            ) : null}
            {(self.currentPage - 1 >= 1) ? (
                <li class="page-item">
                    <a href={self.baseLink + '/' + (self.currentPage - 1) + self.linkExtra} class="page-link">
                        {self.currentPage - 1}
                    </a>
                </li>
            ) : null}
            <li class="page-item active">
                <a href={self.baseLink + '/' + self.currentPage + self.linkExtra} class="page-link">
                    {self.currentPage}
                </a>
            </li>
            {(self.currentPage + 1 <= self.maxPage) ? (
                <li class="page-item">
                    <a href={self.baseLink + '/' + (self.currentPage + 1) + self.linkExtra} class="page-link">
                        {self.currentPage + 1}
                    </a>
                </li>
            ) : null}
            {(self.currentPage + 1 < self.maxPage) ? (
                <>
                    <li class={"page-item disabled" + ((self.currentPage + 1 === self.maxPage - 1) ? ' d-none' : '')}>
                        <a href={self.baseLink + '/:page:' + self.linkExtra} class="page-link">
                            …
                        </a>
                    </li>
                    <li class="page-item">
                        <a href={self.baseLink + '/' + self.maxPage + self.linkExtra} class="page-link">
                            {self.maxPage}
                        </a>
                    </li>
                </>
            ) : null}
        </ul>
        <script type="text/javascript">
            {elements.safeHtml(`initPagination();`)}
        </script>
    </nav>
);
