import { Pool, QueryResult } from 'pg';
import { DB_HOST, DB_PASSWORD } from './conf';

const pool = new Pool({
    user: 'mazomirror',
    host: DB_HOST,
    database: 'mazomirror',
    password: DB_PASSWORD
});

export const query = <Out extends Record<string, any> = any, In extends any[] = any[]>(sql: string, values?: In): Promise<QueryResult<Out>> => pool.query<Out, In>(sql, values!);