import { strictEqual } from 'assert';

strictEqual((new Date()).getTimezoneOffset(), 0, 'Server not in UTC');

export const timeFormatterImpls: ((secs: number, dateObj: Date, tzSecs: number, nowSecs: number, tc: boolean) => string)[] = [
    // 长格式
    (timeStamp, dateObj, tz, now, tc) => {
        const str = dateObj.toISOString();
        return str.substr(0, 10) + ' ' + str.substr(11, 5);
    },
    // 短格式
    (timeStamp, dateObj, tz, now, tc) => {
        if (timeStamp > now) { return tc ? '未來' : '未来'; }
        const nowStr = new Date((now + tz) * 1000);
        const diffHours = Math.floor((now - timeStamp) / 3600);
        if ((diffHours < 12) || ((diffHours < 36) && (nowStr.getDate() === dateObj.getDate()))) {
            return dateObj.toTimeString().substr(0, 5);
        }
        const date = dateObj.toISOString();
        const diffDays = Math.floor((now - timeStamp) / 86400);
        if ((diffDays < 180) || (diffDays < 540) && (nowStr.getFullYear() === dateObj.getFullYear())) {
            return date.substr(5, 5);
        }
        return date.substr(0, 10);
    },
    // 相对时间
    (timeStamp, dateObj, tz, now, tc) => {
        const diff = now - timeStamp;
        if (diff < 0) { return tc ? '未來' : '未来'; }
        if (diff < 3600) { return Math.round(diff / 60) + (tc ? ' 分鐘前' : ' 分钟前'); }
        if (diff < 86400) { return Math.round(diff / 3600) + (tc ? ' 小時前' : ' 小时前'); }
        if (diff < 86400 * 7) { return Math.round(diff / 86400) + ' 天前'; }
        if (diff < 86400 * 30.436875) { return Math.round(diff / (86400 * 7)) + ' 周前'; }
        if (diff < 86400 * 365.2425) { return Math.round(diff / (86400 * 30.436875)) + (tc ? ' 個月前' : ' 个月前'); }
        return Math.round(diff / (86400 * 365.2425)) + ' 年前';
    },
    // 相对/短格式
    (timeStamp, dateObj, tz, now, tc) => {
        const diff = now - timeStamp;
        if (diff < 0) { return tc ? '未來' : '未来'; }
        if (diff < 3600) { return Math.round(diff / 60) + (tc ? ' 分鐘前' : ' 分钟前'); }
        if (diff < 86400) { return Math.round(diff / 3600) + (tc ? ' 小時前' : ' 小时前'); }
        const diffDays = Math.floor((now - timeStamp) / 86400);
        if (diffDays < 7) { return Math.round(diff / 86400) + ' 天前'; }
        const nowStr = new Date((now + tz) * 1000);
        const date = dateObj.toISOString();
        if ((diffDays < 180) || (diffDays < 540) && (nowStr.getFullYear() === dateObj.getFullYear())) {
            return date.substr(5, 5);
        }
        return date.substr(0, 10);
    }
];