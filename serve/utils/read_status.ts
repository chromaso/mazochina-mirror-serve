import { query as sql } from '../db';
import { IS_DEV_MODE as DEV_MODE } from '../conf';
import { MazoKoaContext } from './logic_utils';
import { cookieOptions } from '../shared/options';
import { createVisitorId } from '../shared/login';

const parseEntryList = (list: string[]): [number, number][] => list.map(pair => pair.split('p').map(section => parseInt(section, 25)) as [number, number]).filter(item => ((item.length === 2) && (item[0] > 0) && (item[1] > 0)));

export const processReadStatus = async (ctx: MazoKoaContext): Promise<void> => {
    const newReadStatus = ctx.cookies.get('mazomirror_ru');
    if (!newReadStatus) { return; }

    const newEntryList = parseEntryList(newReadStatus.split('q').slice(1));
    if (!newEntryList.length) { return; }

    if (DEV_MODE) {
        console.log(`MMRS: at ${ctx.path}, gets ${newReadStatus}, parsed as ${newEntryList.length} entries`);
    }

    const updatesNotAckedByClient = new Map(parseEntryList((ctx.cookies.get('mazomirror_rp') || '').split('q')));
    newEntryList.forEach(([threadId, postId]) => {
        updatesNotAckedByClient.set(threadId, postId);
    });

    const threadIds = newEntryList.map(pair => pair[0]);
    const postDates = newEntryList.map(pair => pair[1]);

    const userId = ctx.state.mazoLoginUser ? ctx.state.mazoLoginUser.id : await createVisitorId(ctx);
    if (!userId) { return; }

    await sql(`INSERT INTO unread_status (user_id, thread_id, up_to_date, tracked_at) SELECT $1, *, $4 FROM UNNEST ($2::INT[], $3::INT[]) ON CONFLICT (user_id, thread_id) DO UPDATE SET up_to_date = GREATEST(unread_status.up_to_date, EXCLUDED.up_to_date), tracked_at = EXCLUDED.tracked_at`, [userId, threadIds, postDates, ctx.refDate]);

    const rpCookie = Array.from(updatesNotAckedByClient.entries()).map(pair => pair[0].toString(25) + 'p' + pair[1].toString(25)).join('q');
    ctx.cookies.set('mazomirror_rp', rpCookie, { ...cookieOptions, httpOnly: false });
};
