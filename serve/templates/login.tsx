import * as elements from 'typed-xhtml';
import { HtmlTmpl, styled } from '../utils/tsx_utils';

// Migrated from register.pug.
export const register: HtmlTmpl<{
    captcha: { dataUrl: string, id: string } | null,
    username: string,
    password: string,
    retype: string,
    email: string,
    nonce: string
}> = (ctx, self) => (
    <div class="container">
        {self.captcha ? (
            <div class="alert alert-info">
                欢迎注册！建议你阅读本站<a href="/rules">章程与规则</a>。
            </div>
        ) : (
            <div class="alert alert-primary">
                验证邮件已发至你的邮箱；请将其中的验证码输入到下面的表单。它很可能被归为广告邮件/垃圾邮件，还请检查。
            </div>
        )}
        <form id="registration-form" method="post">
            <div class="form-group">
                <label>用户名</label>
                <input
                    type="text"
                    name="username"
                    placeholder="仅限字母、数字、汉字"
                    value={self.username}
                    readonly={!self.captcha}
                    maxlength="12"
                    class="form-control"
                />
                <small class="form-text text-muted">
                    此处不可使用与你在源站相同的用户名。若要使用你在源站使用的用户名，请先暂时以一个临时用户名注册，之后验证你在源站的身份，即可自动更名至你在源站的用户名。
                </small>
            </div>
            <div class="form-group">
                <label>密码</label>
                <input
                    type="password"
                    name="password"
                    placeholder="至少 6 位，不可全是数字"
                    value={self.password}
                    readonly={!self.captcha}
                    minlength="6"
                    class="form-control"
                />
            </div>
            <div class="form-group">
                <label>密码（确认）</label>
                <input
                    type="password"
                    name="retype"
                    placeholder="和上方输入密码相同"
                    value={self.retype}
                    readonly={!self.captcha}
                    minlength="6"
                    class="form-control"
                />
            </div>
            <div class="form-group">
                <label>邮箱地址</label>
                <input
                    type="email"
                    name="email"
                    placeholder="请使用常见邮箱服务商"
                    value={self.email}
                    readonly={!self.captcha}
                    class="form-control"
                />
            </div>
            {self.captcha ? (
                <>
                    <div class="form-group">
                        <label>
                            Captcha：
                            <img src={self.captcha.dataUrl} />
                        </label>
                        <input
                            type="text"
                            name="captcha"
                            placeholder="请以中文输入计算结果（例如「一千零四十二」）"
                            class="form-control"
                        />
                    </div>
                    <input type="hidden" name="ref" value={self.captcha.id} />
                </>
            ) : (
                <>
                    <div class="form-group">
                        <label>邮件验证码</label>
                        <input
                            type="text"
                            name="token"
                            placeholder="请在此填写邮件中收到的验证码，例如 ktx34zi0_a1074d842a926e5027ff"
                            class="form-control"
                        />
                    </div>
                    <input type="hidden" name="nonce" value={self.nonce} />
                </>
            )}
            <div class="form-group text-center">
                <button id="submit-button" type="submit" class="btn btn-primary">
                    {self.captcha ? '发送验证邮件' : '注册'}
                </button>
            </div>
        </form>
    </div>
);

// Migrated from update_info.pug.
export const updateInfo: HtmlTmpl<{
    effectiveEmail: string | undefined,
    nonce: string,
    oldpass: string,
    newpass: string,
    retype: string,
    email: string
}> = (ctx, self) => {
    const phase2 = !!self.nonce;
    return (
        <div class="container">
            {phase2 ? (
                <div class="alert alert-primary">
                    验证邮件已发至 {self.effectiveEmail}；请将其中的验证码输入到下面的表单。它很可能被归为广告邮件/垃圾邮件，还请检查。
                </div>
            ) : (
                <div class="alert alert-info">
                    你可以在这里修改密码，或者绑定邮箱；或者同时修改两者。无论哪一项操作均需要你正确输入现在的密码，并通过邮箱接收验证码。
                </div>
            )}
            <form method="post">
                <div class="form-group">
                    <label>当前密码</label>
                    <input
                        type="password"
                        name="oldpass"
                        placeholder="在此输入你当前的密码"
                        value={self.oldpass}
                        readonly={phase2}
                        class="form-control"
                    />
                </div>
                <div class="form-group">
                    <label>新密码</label>
                    <input
                        type="password"
                        name="newpass"
                        placeholder="至少 6 位，不可全是数字（若不需要修改密码，请留空）"
                        value={self.newpass}
                        readonly={phase2}
                        class="form-control"
                    />
                </div>
                <div class="form-group">
                    <label>新密码（确认）</label>
                    <input
                        type="password"
                        name="retype"
                        placeholder="和上方输入的新密码相同（若不需要修改密码，请留空）"
                        value={self.retype}
                        readonly={phase2}
                        class="form-control"
                    />
                </div>
                <div class="form-group">
                    <label>邮箱地址</label>
                    <input
                        type="email"
                        name="email"
                        placeholder="请使用常见邮箱服务商（若不需要修改邮箱，请留空）"
                        value={self.email}
                        readonly={phase2}
                        class="form-control"
                    />
                </div>
                {phase2 ? (
                    <>
                        <div class="form-group">
                            <label>邮件验证码</label>
                            <input
                                type="text"
                                name="token"
                                placeholder="请在此填写邮件中收到的验证码，例如 ktx34zi0_a1074d842a926e5027ff"
                                class="form-control"
                            />
                        </div>
                        <input type="hidden" name="nonce" value={self.nonce} />
                    </>
                ) : null}
                <div class="form-group text-center">
                    <button id="submit-button" type="submit" class="btn btn-primary">
                        {phase2 ? '确认修改' : '发送验证邮件'}
                    </button>
                </div>
            </form>
            <script type="text/javascript">{elements.safeHtml(`document.getElementById('submit-button').addEventListener('click', function (e) { var el = e.currentTarget; setTimeout(function () { el.disabled = true; }, 0); });`)}</script>
        </div>
    );
};

// Migrated from merge_user.pug.
export const mergeUser: HtmlTmpl<{
    originRoot: string,
    profileVerificationCode: string
}> = (ctx, self) => styled(
    `#by-password:target, #by-profile:target { display: block !important; }`,
    <div class="container">
        <div class="alert alert-warning">
            目前（2024 年夏），源站似乎是挂掉的状态，因此此功能无法使用。
        </div>
        <div class="alert alert-info">
            {' '}
            <h4 class="alert-heading">注意事项</h4>
            <ul class="mb-0">
                {'    '}
                <li>此操作过后，你在镜像站的用户名将会变更为你在源站的用户名；</li>
                <li>
                    你在镜像站的邮箱和密码仍然是在镜像站注册时填写的邮箱和密码，不会使用你在源站的设置；
                </li>
                <li>
                    之后，你在源站发的帖子和在镜像发的帖子就将显示在同一 profile 下。
                </li>
            </ul>
        </div>
        <form method="post">
            <div class="form-group row">
                <label class="col-sm-4 col-form-label">你在源站的用户名</label>
                <div class="col-sm-8">
                    <input
                        type="text"
                        name="username"
                        class="form-control"
                    />
                </div>
            </div>
            <fieldset class="form-group">
                {' '}
                <div class="row">
                    <legend class="col-sm-4 col-form-label pt-0">
                        任选一种验证方式
                    </legend>
                    <div class="col-sm-8">
                        <div class="form-check">
                            <input
                                id="by-method-password"
                                type="radio"
                                name="method"
                                value="password"
                                class="form-check-input"
                            />
                            <label for="by-method-password" class="form-check-label">
                                提供你在源站的密码
                            </label>
                        </div>
                        <div class="form-check">
                            <input
                                id="by-method-profile"
                                type="radio"
                                name="method"
                                value="profile"
                                class="form-check-input"
                            />
                            <label for="by-method-profile" class="form-check-label">
                                修改你在源站的个人资料
                            </label>
                        </div>
                    </div>
                </div>
            </fieldset>
            <div id="by-password" class="form-group d-none">
                <div class="row">
                    <label class="col-sm-4 col-form-label">你在源站的密码</label>
                    <div class="col-sm-8">
                        <input type="password" name="password" class="form-control" />
                    </div>
                </div>
            </div>
            <div id="by-profile" class="form-group d-none">
                <div class="card card-body">
                    请
                    <a href={self.originRoot + 'ucp.php?i=ucp_profile&mode=profile_info'} target="_blank">
                        前去修改你在源站的资料
                    </a>
                    ，将其中任意一项填写为「{self.profileVerificationCode}
                    」，保存后再点击下面的验证。
                </div>
            </div>
            <div class="form-group text-center">
                <button id="submit-button" type="submit" class="btn btn-primary">
                    提交
                </button>
            </div>
        </form>
        <script type="text/javascript">{elements.safeHtml(`document.getElementById('submit-button').addEventListener('click', function (e) { var el = e.currentTarget; window.alert('数据库更新可能需要几十秒，点击提交后请耐心等待，不要刷新或重新提交，谢谢。'); setTimeout(function () { el.disabled = true; }, 0); });`)}</script>
        <script type="text/javascript">{elements.safeHtml(`['password', 'profile'].forEach(function (key) { var el = document.getElementById('by-method-' + key); if (window.location.hash === '#by-' + key) { el.checked = true; } el.addEventListener('click', function (e) { if (e.currentTarget.checked) { window.location.hash = '#by-' + key; } }) });`)}</script>
    </div>
);