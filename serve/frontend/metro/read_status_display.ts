import { $, $id, $set, $xhr } from '../common-import';

const processJson = (threadId: number, doNotShowLink: boolean, { total, owner }: { total: number, owner?: number }): (string | HTMLElement)[] => {
    if (total === 0) {
        return [$('span', 'material-icons', 'mark_chat_read'), '全部楼层已读'];
    }
    if (total < 0) {
        const status = (total === -2) ? '此主题很久没更新' : '此主题完全未读';
        return doNotShowLink ? [status] : [
            status,
            $('br'),
            $('a', { href: '/thread/' + threadId }, [$('span', 'material-icons', 'open_in_new'), '开始阅读'])
        ];
    } else {
        const children: Parameters<typeof $>[2] = [$('span', 'material-icons', 'new_releases'), `共有 ${total} 个未读回复`];
        if (owner) {
            children.push($('br'));
            children.push(`（其中 ${owner} 个来自楼主）`);
        }
        children.push($('br'));
        if (!doNotShowLink) {
            children.push($('a', { href: '/post/t' + threadId }, [$('span', 'material-icons', 'open_in_new'), '跳至首个未读楼层']));
        }
        return children;
    }
};

const loadReadStatus = (threadIdOrNegated: number, small: HTMLElement): void => {
    const onError = (e: string): void => $set(small, '读取失败：' + e);
    const threadId = Math.abs(threadIdOrNegated);
    $xhr('/api/read/' + threadId, {
        e: onError,
        j: response => {
            const forSubs = processJson(threadId, threadIdOrNegated < 0, response as any);
            const star = (response as any).star;
            if (star & 0x8) {
                forSubs.unshift($('br'));
                forSubs.unshift(`已屏蔽`);
                forSubs.unshift($('span', 'material-icons', 'block'));
            } else if (star & 0x7) {
                forSubs.unshift($('br'));
                if (star > 1) {
                    forSubs.unshift(`已收藏并订阅`);
                } else {
                    forSubs.unshift(`已收藏`);
                }
                forSubs.unshift($('span', 'material-icons', 'favorite'));
            }
            $set(small, $('div', 'text-center', forSubs));
        }
    });
};

const onIconClick = (e: Event): void => {
    const icon = e.currentTarget as HTMLSpanElement;
    const wrap = icon.nextSibling as (HTMLDivElement | undefined);
    if (!wrap) { return; }
    const id = icon.getAttribute('data-thread-id');
    if (!id) { return; }
    icon.removeAttribute('data-thread-id');
    loadReadStatus(parseInt(id), wrap.firstChild as HTMLElement);
};

const getThreadIdFromUri = (a: HTMLAnchorElement) => a.pathname.match(/(thread\/|\/t)(\d+)$/);

export const initThreadListPage = (): void => { // Including forum page, index page, author page, tag page and potentially more.
    const table = $id('thread-table-main')!;
    const tbody = table.querySelector('tbody')!;

    for (const trigger of Array.from(tbody.getElementsByClassName('read-mark'))) {
        const icon = trigger.firstChild as HTMLSpanElement;
        const tr = (trigger.parentNode as HTMLTableCellElement).parentNode as HTMLTableRowElement;
        const threadIdMatched = getThreadIdFromUri(tr.getElementsByTagName('a')[0]);
        if (!threadIdMatched) { return; }
        // data-thread-id negated when the link is already read-status-relative.
        icon.setAttribute('data-thread-id', ((threadIdMatched[1] === '/t') ? '-' : '') + threadIdMatched[2]);
        icon.setAttribute('data-toggle', 'dropdown');
        icon.addEventListener('click', onIconClick);
        trigger.insertBefore($('div', 'dropdown-menu',
            $('small', { className: 'd-block px-2' }, icon.title)
        ), icon.nextSibling);
    }
    
    const footLink = tbody.nextElementSibling?.querySelector('a');
    if (footLink) {
        footLink.addEventListener('click', () => {
            $set(footLink.parentNode as HTMLTableSectionElement, '被屏蔽的主题现已显示在上方列表中');
            for (const tr of Array.from(tbody.children)) {
                tr.classList.remove('d-none');
            }
        });
    }
};