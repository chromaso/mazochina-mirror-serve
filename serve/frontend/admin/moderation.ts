import { $, $xhr, getThreadIdFromUri, showModal } from '../common-import';

export const mazomirrorAdmin = (type: 'user' | 'post', id: number, target?: number) => {
    let enabled = true;
    const input = $('input', { type: 'text', className: 'form-control', name: 'note', placeholder: '请输入操作理由…', required: true });
    showModal('管理操作', $('form', { method: 'post', action: '/admin' }, [
        $('div', 'alert alert-info', ['你目前可以使用部分管理员权限，维护论坛氛围。使用此功能时请保持高度克制，宁纵勿枉，谢谢。请确保你已阅读并了解', $('a', { href: '/admin/guide' }, '管理操作规范'), '再操作。']),
        $('div', 'form-group', input),
        $('input', { type: 'hidden', name: 'type', value: type }),
        $('input', { type: 'hidden', name: 'id', value: id }),
        $('input', { type: 'hidden', name: 'target', value: target, disabled: (target === void 0) }),
        $('div', 'text-center', $('button', { type: 'submit', className: 'btn btn-primary' }, '执行操作'))
    ], {
        'submit': e => {
            if (!input.value.trim()) { window.alert('理由不可为空！'); e.preventDefault(); return; }
            if (!enabled) { e.preventDefault(); return; }
            enabled = false;
        }
    }), () => enabled);
};

export const initThreadAdmin = (isSpam: boolean, canDelete: boolean) => {
    let loading: HTMLOptionElement;
    const select = $('select', { className: 'form-control', name: 'target' }, [
        $('option', { value: '', text: '（请选择管理操作）' }),
        $('option', { value: isSpam ? '-2' : '-1', text: isSpam ? '取消隐藏主题' : '（完全）隐藏主题' }),
        loading = $('option', { value: '', text: '（加载中）' })
    ]);
    if (!isSpam) {
        select.appendChild($('option', { value: '-3', text: '标注为广告并半隐藏' }));
    }
    if (canDelete) {
        select.appendChild($('option', { value: '0', text: '删除主题' }));
    }
    let splitLine: HTMLElement;
    let mergeLine: HTMLElement;
    $xhr('/api/forums', {
        j: forums => {
            select.removeChild(loading);
            for (const { id, name } of (forums as { id: number, name: string }[])) {
                select.appendChild($('option', { value: String(id), text: '移动至「' + name + '」' }));
            }
            const advSplit = $('option', { text: '拆分部分回复至新主题' });
            select.appendChild(advSplit);
            const advMerge = $('option', { value: '-4', text: '合并入另一主题' });
            select.appendChild(advMerge);
            select.addEventListener('change', () => {
                if (advSplit.selected) {
                    select.selectedIndex = 0;
                    splitLine.classList.remove('d-none');
                } else {
                    const value = parseInt(select.options[select.selectedIndex].value);
                    if (value <= 0) { // Will be false for NaN (empty string).
                        splitLine.classList.add('d-none');
                    }
                }
                if (advMerge.selected) {
                    mergeLine.classList.remove('d-none');
                } else {
                    mergeLine.classList.add('d-none');
                }
            });
        },
        e: e => console.error('无法加载论坛列表', e)
    });
    let enabled = true;
    const input = $('input', { type: 'text', className: 'form-control', name: 'note', placeholder: '请输入操作理由…', required: true });
    showModal('管理操作', [$('form', { method: 'post', action: '/admin' }, [
        $('div', 'alert alert-info', ['你是「资深用户」，因此可以处理其它用户的主题，维护论坛氛围。使用此功能时请保持谦抑，多谢。请确保你已阅读并了解', $('a', { href: '/admin/guide' }, ['管理操作规范']), '再操作。']),
        $('div', 'form-group', select),
        $('div', 'form-group', input),
        splitLine = $('div', 'form-group d-none', [
            $('input', { type: 'text', name: 'posts', className: 'form-control', placeholder: '要拆分的文章 ID 列表（以逗号分隔）' }),
            $('small', undefined, '此处留空则为对整个主题进行操作；若需进行拆分操作，必须填写且在上方选择一个「移动至」选项作为拆分出的主题帖所在的板块')
        ]),
        mergeLine = $('div', 'form-group d-none', $('input', { type: 'text', className: 'form-control', name: 'merge', placeholder: '合并至的主题 ID' })),
        $('input', { type: 'hidden', name: 'type', value: 'thread' }),
        $('input', { type: 'hidden', name: 'id', value: String(getThreadIdFromUri()) }),
        $('div', 'text-center', [$('button', { type: 'submit', className: 'btn btn-primary' }, '执行操作')])
    ], {
        'submit': e => {
            if (!select.value) { window.alert('未选择操作！'); e.preventDefault(); return; }
            if (!input.value.trim()) { window.alert('理由不可为空！'); e.preventDefault(); return; }
            if (!enabled) { e.preventDefault(); return; }
            enabled = false;
        }
    })], () => enabled);
};
