import { parseBBCode } from '../../common/bbcode';
import { CONV_SC as toSc } from '../chinese';
import { query as sql } from '../db';
import { THRESHOLD_FOR_LOCAL } from '../../common/consts';

const redoBBCodeOfPost = async (postRow: {
    post_id: number,
    title: string,
    rev_id: number,
    most_recent_update: number
}): Promise<boolean> => {
    const isLocal = postRow.rev_id > THRESHOLD_FOR_LOCAL; // No rev_id? Must be remote.
    let revId = postRow.rev_id;
    if (!revId) {
        const postLogRow = (await sql('SELECT done_as_rev FROM post_targets WHERE post_id = $1', [postRow.post_id])).rows[0];
        if (!postLogRow || !postLogRow.done_as_rev) { return false; }
        revId = postLogRow.done_as_rev;
    }
    const rev = (await sql('SELECT content FROM local_post_log WHERE post_id = $1 AND rev_id = $2', [postRow.post_id, revId])).rows[0];
    if (!rev) { throw new Error(`Revision ${revId} not found.`); }
    const parsed = parseBBCode(0, rev.content, { lax: !isLocal });

    const normalizedTextContent = toSc(postRow.title + '\n\ufffc\n' + parsed.text);
    const update = await sql('UPDATE posts SET content = $1, content_sc = $2 WHERE post_id = $3 AND rev_id = $4 AND title = $5 AND most_recent_update = $6', [parsed.html, normalizedTextContent, postRow.post_id, postRow.rev_id, postRow.title, postRow.most_recent_update]);
    if (update.rowCount !== 1) { throw new Error(`Updated ${update.rowCount} rows for post ${postRow.post_id}.`); }

    return true;
};

export const redoBBCodeOfThread = async (threadId: number): Promise<string> => {
    try {
        const batchSize = 50;
        let lastPostId = 0;
        let updated = 0;
        let skipped = 0;
        do {
            const posts = (await sql('SELECT post_id, title, rev_id, most_recent_update FROM posts WHERE thread_id = $1 AND post_id > $2 ORDER BY post_id ASC LIMIT $3', [threadId, lastPostId, batchSize])).rows;
            for (const post of posts) {
                const attempt = await redoBBCodeOfPost(post);
                if (attempt) { updated++; } else { skipped++; }
            }
            lastPostId = posts.length ? posts[posts.length - 1].post_id : 0;
        } while (lastPostId);

        return `parse=${updated},skip${skipped}`;
    } catch (e) {
        return `warn=[${(e as Error).message}]`;
    }
};
