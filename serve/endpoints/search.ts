import { query as sql } from '../db';
import { allTagsById, searchSetsToTerms, tagPref } from '../shared/tags';
import { parseInt32 } from '../utils/logic_utils';
import { forumsCache, listForums } from '../shared/forum';
import { LOGIN_REQUIRED, PageImpl, error, finalizePage, redirectWithDelay } from '../utils/render_utils';
import { THREAD_RENDER_COLS, renderThreads } from '../shared/thread_table';
import * as searchModule from '../shared/search';
import { rowsToMap } from '../../common/logic_utils';
import { THRESHOLD_FOR_LOCAL } from '../../common/consts';

import * as authorTemplates from '../templates/author';
import * as templates from '../templates/search';
import { processContentSc } from '../shared/post';
import { dualAvatar } from '../utils/avatar';

export const search_id_page: PageImpl = async ctx => {
    let searchId = 0;
    let forumId = 0;
    if (ctx.params.id) {
        searchId = parseInt32(`${ctx.params.id || ''}`);
        if (isNaN(searchId)) { throw error(400, '不要乱输入网址嘛┭┮﹏┭┮'); }
    } else if ('f' in ctx.query) {
        forumId = parseInt32(`${ctx.query.f || ''}`);
        if (isNaN(forumId)) { throw error(400, '不要乱输入网址嘛┭┮﹏┭┮'); }
    }
    let page = parseInt32(ctx.params.page);
    if (isNaN(page) || page <= 0) { page = 1; }

    let result: Awaited<ReturnType<typeof searchModule.waitForResult>> | undefined;
    let sortByOrigin = false;
    let post = false;
    let includeSpam = false;
    let keywordsStr = '';
    let username: string | undefined;
    let tags: { include: searchModule.TagMicro[], exclude: searchModule.TagMicro[] } | undefined;
    let authorInfoRows: { user_id: number, user_name: string, email: string | null, avatar: number, label: string | null }[] | undefined;
    let tagsInKeywordForm = '';

    if (searchId) {
        result = await searchModule.waitForResult(searchId, page, ctx.state.userConfig);
        post = !!(result.flags & 0x2);
        includeSpam = !!(result.flags & 0x4);
        sortByOrigin = !!(result.flags & 0x8);
        keywordsStr = result.terms;
        forumId = result.forum_id;
        if (result.tags) {
            const allF18Tags = await allTagsById();
            tags = { include: [], exclude: [] };
            for (const tag of result.tags) {
                const id = Math.abs(tag);
                ((tag < 0) ? tags.exclude : tags.include).push({
                    id, 
                    name: allF18Tags.get(id)?.primary_name ?? '[UNKNOWN]'
                });
            }
            tagsInKeywordForm = ' ' + (await searchSetsToTerms(new Set(tags.include.map(_ => _.id)), new Set(tags.exclude.map(_ => _.id)))).join(' ');
        }
        if (result.user_id) {
            authorInfoRows = (await sql('SELECT user_id, user_name, email, avatar, label FROM users WHERE user_id = $1', [result.user_id])).rows;
            username = authorInfoRows[0].user_name;
        }
    }

    const commonRenderVariables: templates.PanelParams = {
        forumName: forumId ? (await forumsCache).get(forumId)!.forum_name : undefined,
        forums: await listForums(),
        forumId,
        searchPost: post,
        includeSpam,
        sortByOrigin,
        keywordsStr,
        username,
        tags,
        tagsInKeywordForm
    };

    if (!result) {
        return finalizePage(ctx, {
            title: ctx.state.render.staticConv('搜索'),
            nav: true
        }, templates.search(ctx, commonRenderVariables));
    }

    const resultParams: templates.ResultParams = {
        resultCount: result ? result.num_results : 0,
        resultLimit: result.user_id ? searchModule.RESULT_LIMIT_WITH_USER_TERM : searchModule.RESULT_LIMIT_NO_USER_TERM,
        keywords: keywordsStr ? keywordsStr.split(' ') : [], // keywordsStr is already trimmed, won't need to trim or something.
        pagination: {
            currentPage: page,
            linkExtra: '',
            baseLink: '/search/' + searchId,
            maxPage: Math.ceil(result.num_results / (post ? ctx.state.userConfig.tp : ctx.state.userConfig.fp)),
        }
    };

    if (post) {
        const postsMap = rowsToMap((await sql<{ post_id: number, title: string, user_id: number, user_name: string, content_sc: string, flags: number, posted_date: number, thread_id: number }>('SELECT post_id, title, user_id, user_name, content_sc, flags, posted_date, thread_id FROM posts WHERE post_id = ANY($1::INT[])', [result.result_page])).rows, 'post_id');
        const posts = result.result_page.map(id => postsMap.get(id)!); // Ensure they are in the right order.
        if (!authorInfoRows) {
            authorInfoRows = (await sql('SELECT user_id, user_name, email, avatar, label FROM users WHERE user_id = ANY($1::INT[])', [Array.from(new Set(posts.map(post => post.user_id)))])).rows;
        }
        const authorInfos = rowsToMap(authorInfoRows, 'user_id');

        return finalizePage(ctx, {
            title: ctx.state.render.staticConv('搜索文章'),
            nav: true
        }, templates.searchPost(ctx, {
            ...commonRenderVariables,
            ...resultParams,
            posts: posts.map(row => {
                const authorInfo = ((row.user_id === 0) && row.user_name) ? { user_id: row.user_id, user_name: row.user_name, email: null, avatar: 0, label: null } : authorInfos.get(row.user_id)!;
                if (!authorInfo) {
                    console.log(row.user_id, row.post_id);
                }
                return {
                    authorLink: '/author/' + row.user_id,
                    authorLabel: authorInfo.label,
                    authorName: authorInfo.user_name,
                    authorAvatar: dualAvatar(authorInfo.user_id, authorInfo.user_name, authorInfo.email, authorInfo.avatar),
                    isHidden: !!(row.flags & 2),
                    id: row.post_id,
                    isLocal: row.post_id > THRESHOLD_FOR_LOCAL,
                    title: row.title,
                    content: processContentSc(row.content_sc, ctx.state.render.renderDate),
                    date: row.posted_date,
                    replyLink: '/post/p' + row.post_id,
                    topicLink: '/post/' + row.post_id,
                };
            })
        }));
    } else {
        const threadsMap = rowsToMap((await sql(`SELECT ${THREAD_RENDER_COLS.join(', ')} FROM thread_targets WHERE thread_id = ANY($1::INT[])`, [result.result_page])).rows, 'thread_id');
        const threads = result.result_page.map(id => threadsMap.get(id));  // Ensure they are in the right order.

        return finalizePage(ctx, {
            title: ctx.state.render.staticConv('搜索主题'),
            nav: true
        }, templates.searchThread(ctx, {
            ...commonRenderVariables,
            ...resultParams,
            threads: await renderThreads(ctx, threads, await tagPref(ctx), ctx.state.mazoLoginUser?.id)
        }));
    }
};

export const query: PageImpl = async ctx => {
    // Step 0: see if user is specified. convert user name to user ID.
    let uid = parseInt32(`${ctx.query.uid || ''}`) || 0;
    if (!uid) {
        const username = String(ctx.query.user || '').trim();
        if (username) {
            const userId = (await sql<{ user_id: number, user_name: string, has_password: boolean }>('SELECT user_id, user_name, password_h IS NOT NULL AS has_password FROM users WHERE LOWER(user_name) = LOWER($1)', [username])).rows;
            if (!userId.length) {
                throw error(404, '这个名字的用户不存在来着（');
            }
            if (userId.length > 1) {
                ctx.status = 300;
                return finalizePage(ctx, {
                    title: ctx.state.render.staticConv('用户 - ') + username,
                    nav: false
                }, authorTemplates.userDisambiguation(ctx, {
                    username,
                    users: userId.map(row => ({
                        id: row.user_id,
                        name: row.user_name,
                        link: ctx.path + ctx.search + '&uid=' + row.user_id, // Querystring won't be empty, we know.
                        isOriginal: row.user_id <= THRESHOLD_FOR_LOCAL,
                        isLocal: row.has_password,
                    }))
                }));
            } 
            uid = userId[0].user_id;
        }
    }
    // Step 1: see if forum is specified.
    const forumId = parseInt32(`${ctx.query.forum || ''}`) || 0;
    if (forumId) {
        if (!(await forumsCache).has(forumId)) { throw error(404, '没有这样的板块呐～') }
    }
    // Step 2: construct flags.
    let post: boolean;
    let sortByOrigin: boolean;
    let flags = 0;
    switch (ctx.query.type) {
        case 'thread':
            post = false;
            sortByOrigin = ctx.query.sort === 'origin';
            if (sortByOrigin) { flags |= 0x8; }
            break;
        case 'post':
            post = true;
            flags |= 0x2;
            break;
        default:
            throw error(400, '未指定搜索类型');
    }
    const includeSpam = 'all' in ctx.query;
    if (includeSpam) { flags |= 0x4; }
    const keywords = String(ctx.query.query || '').trim().split(' '); // Can be empty.
    // Step 3: check login user.
    let loginUser = 0;
    if (ctx.state.mazoLoginUser) {
        loginUser = ctx.state.mazoLoginUser.id;
    } else if (post) {
        throw error(403, LOGIN_REQUIRED);
    } else if (ctx.state.mazoGuest) {
        loginUser = ctx.state.mazoGuest.id;
    }
    // Step 4: start searching
    const { id, reuse, parsedTags } = await searchModule.lookupOrCreateQueryJob(keywords, uid, forumId, flags, ctx.refDate, loginUser); // This also normalizes the keywords.
    if (!reuse) {
        if (post) {
            searchModule.executeSearchOnPosts(id, keywords, uid, forumId, parsedTags, includeSpam);
        } else {
            searchModule.executeSearchOnThreads(id, keywords, uid, forumId, parsedTags, sortByOrigin!, includeSpam);
        }
    }
    return redirectWithDelay(ctx, {
        numberOfSeconds: 1,
        targetText: '搜索结果（可能尚需加载一会儿）',
        targetHref: '/search/' + id,
        message: '搜索进行中～完成后将跳转到结果页…'
    });
};