import * as elements from 'typed-xhtml';
import { HtmlTmpl } from '../utils/tsx_utils';
import { TriviaStats } from '../shared/trivia';
import { ACHIEVEMENT_COLORS } from './achievement';

const triviaSingle: HtmlTmpl<TriviaStats['postRank'][number]> = (ctx, ranking) => (
    <table class="table table-sm">
        <thead>
            <tr>
                <th>用户</th>
                <th class="text-end">发帖量</th>
            </tr>
        </thead>
        <tbody>
            {ranking.map(row => (
                <tr>
                    <td>
                        <a href={`/author/${row.user_id}`} class="ui-link">{row.user_name}</a>
                        {row.label ? (
                            <span class="badge bg-secondary user-select-none ms-1">
                                {ctx.state.render.staticConv(row.label)}
                            </span>
                        ) : null}
                    </td>
                    <td class="text-end">{row.cnt}</td>
                </tr>
            ))}
        </tbody>
    </table>
);

const triviaLiteSingle: HtmlTmpl<TriviaStats['postRank'][number]> = (ctx, ranking) => (
    <tbody>
        {ranking.map(row => (
            <tr>
                <td><a href={`/author/${row.user_id}`} class="ui-link">{row.user_name}</a></td>
                <td class="text-end">{row.cnt}</td>
            </tr>
        ))}
    </tbody>
);

export const triviaLite: HtmlTmpl<TriviaStats> = (ctx, self) => (
    <>
        <p class="pt-3 pt-sm-4 d-flex justify-content-between align-items-center">
            <span class="lead">Trivia</span>
            <button id="trivia-switch" class="btn btn-link btn-sm"></button>
        </p>
        <div class="row">
            <div class="col-12 col-sm-6 col-lg-3">
                <table class="table table-sm">
                    <thead>
                        <th>本周总帖数</th>
                        <th class="text-end">{ctx.state.mazoLoginUser ? <a href="javascript:openTriviaStats(1)">»</a> : null}</th>
                    </thead>
                    {triviaLiteSingle(ctx, self.postRank[1].slice(0, 3))}
                </table>
            </div>
            <div class="col-12 col-sm-6 col-lg-3">
                <table class="table table-sm">
                    <thead>
                        <th>本月主题帖数</th>
                        <th class="text-end">{ctx.state.mazoLoginUser ? <a href="javascript:openTriviaStats(2)">»</a> : null}</th>
                    </thead>
                    {triviaLiteSingle(ctx, self.postRank[2].slice(0, 3))}
                </table>
            </div>
            <div class="col-12 col-sm-6 col-lg-3">
                <table class="table table-sm">
                    <thead>
                        <th colspan="2">最新成就解锁</th>
                    </thead>
                    <tbody>
                        {self.achivements.map(row => (
                            <tr>
                                <td><a href={`/author/${row.user_id}`} class="ui-link">{row.user_name}</a></td>
                                <td class="text-end">
                                    <a class="text-reset ui-link" href={'/achievement?name=' + encodeURIComponent(row.name_sc)}>
                                        {ctx.state.render.staticConv(row.name_sc)}
                                        <span style={`color: #${ACHIEVEMENT_COLORS[3 - row.lvl]}`}>●</span>
                                    </a>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
            <div class="col-12 col-sm-6 col-lg-3">
                <table class="table table-sm">
                    <thead>
                        <th colspan="2">往年今日</th>
                    </thead>
                    <tbody>
                        {self.threads.map(row => (
                            <tr>
                                <td><a href={`/thread/${row.thread_id}`} class="ui-link">{ctx.state.render.contentConv(row.thread_title)}</a></td>
                                <td class="text-end text-nowrap">
                                    {row.yrs} 年前
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    </>
);

export const trivia: HtmlTmpl<TriviaStats['postRank']> = (ctx, self) => {
    const note = (<small>主题帖和回帖都计入内，但回复一个回复可见的内容时则不计。</small>);
    return (<>
        <div id="thread-weekly">
            {triviaSingle(ctx, self[0])}
        </div>
        <div id="post-weekly">
            {triviaSingle(ctx, self[1])}
            {note}
        </div>
        <div id="thread-monthly">
            {triviaSingle(ctx, self[2])}
        </div>
        <div id="post-monthly">
            {triviaSingle(ctx, self[3])}
            {note}
        </div>
    </>);
};