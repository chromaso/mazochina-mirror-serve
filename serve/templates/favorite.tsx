import * as elements from 'typed-xhtml';
import { HtmlTmpl, styled } from '../utils/tsx_utils';
import { pagination, PaginationParams } from './pagination';
import { BaseThreadTarget, RenderThreadRetVal } from '../shared/thread_table';
import { tagList } from './tag';
import { BLOCKED_THREADS_DUMMY_COLLECTION, MY_THREADS_DUMMY_COLLECTION } from '../shared/favorite';

// Migrated from favorites.pug.
export const favs: HtmlTmpl<{
    username: string,
    isSelf: boolean,
    collections: { fc_id: number, title: string, count: number, isPrivate: boolean }[]
}> = (ctx, self) => (
    <div class="container">
        <h1 class="mb-4">
            {self.username} 的 {self.isSelf ? '全部' : '公开'}收藏夹
        </h1>
        {!self.isSelf ? (
            <div class="alert alert-info">
                这里列出的只是该用户的公开收藏夹，并非全部收藏夹～
            </div>
        ) : null}
        {self.collections.length ? (
            <table class="table">
                <thead>
                    <th>收藏夹</th>
                    <th class="text-end">主题数</th>
                    <th>可见度</th>
                </thead>
                <tbody>
                    {self.collections.map(collection => (
                        <tr>
                            <td class="align-middle pe-0">
                                <a href={'/collection/' + collection.fc_id}>{collection.title}</a>
                            </td>
                            <td class="align-middle text-nowrap text-end">
                                {collection.count}
                            </td>
                            <td class="align-middle">
                                {collection.isPrivate ? (
                                    <>
                                        <span class="material-icons std-icon">visibility_off</span> 私密
                                    </>
                                ) : (
                                    <>
                                        <span class="material-icons std-icon">visibility</span> 公开
                                    </>
                                )}
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        ) : (
            <div class="alert alert-warning">
                {self.isSelf ? '你还没有收藏任何东西～' : '这个用户没有任何公开的收藏夹～'}
            </div>
        )}
    </div>
);

// Migrated from collection.pug.
export const collection: HtmlTmpl<{
    pagination: PaginationParams,
    userId: number,
    username: string,
    favId: number,
    favName: string,
    isSelf: boolean,
    isPrivate: boolean,
    isDefault: boolean,
    level: number,
    threads: RenderThreadRetVal<BaseThreadTarget & { 
        star_type: number,
        fc_ids: number[]
    }>
}> = (ctx, self) => {
    const { render } = ctx.state;
    return styled(`
        #fav-table tr.deleted td:first-child { text-decoration: line-through; }
        #fav-table td { position: relative; }
        #fav-table td .spinner-border { position: absolute; left: 50%; top: 50%; margin-left: -9px; margin-top: -9px; width: 18px; height: 18px; }`,
        <>
            <div class="container">
                <nav class="mb-4">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="/">首页</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href={'/author/' + self.userId + '/favs'}>
                                {self.username} 的收藏夹
                            </a>
                        </li>
                        <li class="breadcrumb-item active">
                            {render.contentConv(self.favName)}
                        </li>
                    </ol>
                </nav>
                <h1 class="mb-4">{render.contentConv(self.favName)}</h1>
                {self.isSelf ? (
                    (self.favId === MY_THREADS_DUMMY_COLLECTION.fc_id) ? (
                        <>
                            <p class="lead">
                                系统自动创建的收藏夹，用于列出你在镜像发布的所有主题
                            </p>
                            <div class="alert alert-info">
                                你在源站发布的主题并不会自动列入此收藏夹，但你仍可手动加入
                            </div>
                        </>
                    ) : (self.favId === BLOCKED_THREADS_DUMMY_COLLECTION.fc_id) ? (
                        <p class="lead">
                            这是所有被你屏蔽的主题。被你屏蔽的主题不会出现在主题列表页中
                        </p> 
                    ) : self.isPrivate ? (
                        <p class="lead">私密收藏夹</p>
                    ) : (
                        <p class="lead">公开收藏夹</p>
                    )
                ) : (
                    <p class="lead">
                        由 <a href={'/author/' + self.userId} class="ui-link">{self.username}</a> 创建的公开收藏夹
                    </p>
                )}
                {self.threads.length ? (
                    <>
                        <table id="fav-table" class="table my-5">
                            <thead>
                                <th>主题</th>
                                <th class="text-nowrap text-end">回复</th>
                                {(self.isSelf && (self.favId !== BLOCKED_THREADS_DUMMY_COLLECTION.fc_id)) ? (
                                    <th class="text-center">订阅提醒？</th>
                                ) : null}
                                <th />
                            </thead>
                            <tbody id="favs-list">
                                {self.threads.map(thread => (
                                    <tr data-thread-id={String(thread.thread_id)}>
                                        <td class="align-middle">
                                            <a href={thread.firstLink} class="ui-link">
                                                {render.contentConv(thread.thread_title)}
                                            </a>
                                            {thread.tags
                                                ? tagList(ctx, { list: thread.tags, tagStyle: self.threads.tagStyle })
                                                : null}
                                            <small class="d-none d-sm-block mt-1">
                                                <span class="badge bg-secondary">
                                                    {thread.forumName}
                                                </span> {thread.firstUser} @ {render.renderDate(thread.firstTime)}
                                            </small>
                                        </td>
                                        <td class="align-middle text-nowrap text-end">
                                            {thread.count - 1}
                                        </td>
                                        {self.isSelf ? (
                                            <>
                                                {(self.favId !== BLOCKED_THREADS_DUMMY_COLLECTION.fc_id) ? (
                                                    <td class="align-middle text-center">
                                                        <select class="form-control form-control-sm" disabled>
                                                            <option
                                                                value="1"
                                                                selected={((thread.star_type & 0x7) === 1) || !!(thread.star_type & 0x8)}
                                                            >
                                                                仅收藏不订阅
                                                            </option>
                                                            {((self.favId !== MY_THREADS_DUMMY_COLLECTION.fc_id) && !(thread.star_type & 0x8)) ? (
                                                                <option
                                                                    value="2"
                                                                    selected={(thread.star_type & 0x7) === 2}
                                                                >
                                                                    订阅楼主的回帖 †
                                                                </option>
                                                            ) : null}
                                                            {(!(thread.star_type & 0x8)) ? (
                                                                <option
                                                                    value="6"
                                                                    selected={(thread.star_type & 0x7) === 6}
                                                                >
                                                                    订阅所有的回帖
                                                                </option>
                                                            ) : null}
                                                        </select>
                                                        {thread.fc_ids.length > 1 ? (
                                                            <>
                                                                {' '}
                                                                <span class="mazomirror-tip" data-title={(thread.star_type & 0x8) ? '此主题已被你屏蔽' : `此主题同时被收藏于你的其它 ${thread.fc_ids.length - 1} 个收藏夹中`}>
                                                                    <span class="material-icons std-icon">info</span>
                                                                </span>
                                                            </>
                                                        ) : null}
                                                    </td>
                                                ) : null}
                                                <td class="align-middle text-end">
                                                    <button disabled class="btn btn-sm btn-outline-secondary">
                                                        <span class="material-icons">delete</span>
                                                    </button>
                                                </td>
                                            </>
                                        ) : null}
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                        {pagination(ctx, self.pagination)}
                    </>
                ) : (
                    <div class="alert alert-warning">
                        这个收藏夹里没有收藏任何东西～
                    </div>
                )}
                {((self.favId > 0) && self.isSelf) ? (
                    <div class="card">
                        <div class="card-header">收藏夹设置</div>
                        <div class="card-body">
                            <form method="post" action={self.pagination.baseLink}>
                                <div class="form-group row">
                                    <label for="fav-form-title" class="col-form-label col-4">
                                        标题
                                    </label>
                                    <div class="col-8">
                                        <input
                                            id="fav-form-title"
                                            name="title"
                                            value={self.favName}
                                            class="form-control"
                                        />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-form-label col-4">可见度</label>
                                    <div class="col-8">
                                        <div class="form-check form-check-inline">
                                            <input
                                                id="fav-form-visibility0"
                                                type="radio"
                                                name="visibility"
                                                value="0"
                                                checked={!self.isPrivate}
                                                class="form-check-input"
                                            />
                                            <label for="fav-form-visibility0" class="form-check-label">
                                                公开
                                            </label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input
                                                id="fav-form-visibility1"
                                                type="radio"
                                                name="visibility"
                                                value="1"
                                                checked={self.isPrivate}
                                                class="form-check-input"
                                            />
                                            <label for="fav-form-visibility1" class="form-check-label">
                                                私密
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-form-label col-4">
                                        默认订阅选项
                                        {' '}
                                        <span class="mazomirror-tip" data-title="主题被添加进此收藏夹时，若先前未被订阅，将自动应用此收藏设置；仅影响未来添加进此收藏夹的主题，不会改变已在收藏夹内的主题的设定">
                                            <span class="material-icons std-icon">help</span>
                                        </span>
                                    </label>
                                    <div class="col-8">
                                        <div class="form-check form-check-inline">
                                            <input
                                                id="fav-form-level1"
                                                type="radio"
                                                name="level"
                                                value="1"
                                                checked={self.level === 1}
                                                class="form-check-input"
                                            />
                                            <label for="fav-form-level1" class="form-check-label">
                                                不订阅
                                            </label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input
                                                id="fav-form-level2"
                                                type="radio"
                                                name="level"
                                                value="2"
                                                checked={self.level === 2}
                                                class="form-check-input"
                                            />
                                            <label for="fav-form-level2" class="form-check-label">
                                                订阅楼主的回帖 †
                                            </label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input
                                                id="fav-form-level6"
                                                type="radio"
                                                name="level"
                                                value="6"
                                                checked={self.level === 6}
                                                class="form-check-input"
                                            />
                                            <label for="fav-form-level6" class="form-check-label">
                                                订阅所有的回帖
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-form-label col-4" />
                                    <div class="col-8">
                                        <div class="form-check form-check-inline">
                                            <input
                                                id="fav-form-promote"
                                                type="checkbox"
                                                name="promote"
                                                checked={self.isDefault}
                                                class="form-check-input"
                                            />
                                            <label for="fav-form-promote" class="form-check-label">
                                                设为默认收藏夹
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-12 text-center">
                                        <input
                                            id="fav-form-delete"
                                            type="submit"
                                            name="delete"
                                            value="删除收藏夹"
                                            class="btn btn-danger mx-2 opacity-50"
                                        />
                                        <input
                                            type="submit"
                                            value="保存"
                                            class="btn btn-primary"
                                        />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                ) : null}
                {(self.isSelf && (self.favId > 0)) ? (
                    <div class="alert alert-info mt-4">
                        †：选择「订阅楼主的回帖」时，楼主对他人的简短回复将不计入其中。仅有楼主未引用任何人或者发布长回复时才会触发提醒。
                    </div>
                ) : null}
            </div>
            {self.isSelf ? <script type="text/javascript">{elements.safeHtml(`initCollectionPage(${self.favId}, ${!self.threads.length});`)}</script> : null}
        </>
    );
};

// Migrated from collections.pug.
export const collections: HtmlTmpl<{
    collections: { fc_id: number, title: string, count: number, isPrivate: boolean }[]
}> = (ctx, self) => (
    <ul class="list-group">
        {self.collections.map(collection => (
            <a class="list-group-item d-flex justify-content-between align-items-center" href={'/collection/' + collection.fc_id}>
                <span>
                    {collection.isPrivate ? (
                        <span title="私密" class="material-icons align-middle me-1">
                            visibility_off
                        </span>
                    ) : (
                        <span title="公开" class="material-icons align-middle me-1">
                            visibility
                        </span>
                    )}
                    {collection.title}
                </span>
                <small class="ms-2">{collection.count}</small>
            </a>
        ))}
    </ul>
);
