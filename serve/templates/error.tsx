import * as elements from 'typed-xhtml';
import { HtmlTmpl } from '../utils/tsx_utils';

// Migrated from error_login.pug.
export const errorLogin: HtmlTmpl<void> = ctx => (
    <div class="container">
        <div class="card" style='max-width: 480px; margin: 0 auto;'>
            <div class="card-header">登入</div>
            <div class="card-body">
                <div class="alert alert-primary">此页面需要登入后才可查看</div>
                <form method="post" action="/login">
                    <div class="form-group">
                        <input
                            id="login-form-username"
                            name="username"
                            type="text"
                            placeholder="用户名"
                            class="form-control"
                        />
                    </div>
                    <div class="form-group">
                        <input
                            id="login-form-password"
                            name="password"
                            type="password"
                            placeholder="密码"
                            class="form-control"
                        />
                    </div>
                    <div class="row justify-content-between">
                        <div class="col-auto">
                            <a href="/register" class="btn btn-secondary">
                                注册
                            </a>
                        </div>
                        <div class="col-auto">
                            <button type="submit" class="btn btn-primary">
                                登入
                            </button>
                        </div>
                    </div>
                </form>
                <p class="mt-2 mb-0 small">
                    若忘记密码或需要其它帮助，请<a href="/modmail">联系镜像管理员</a>。
                </p>
            </div>
        </div>
    </div>
);


// Migrated from error.pug.
export const error: HtmlTmpl<{ errorMessage: string }> = (ctx, { errorMessage }) => (
    <div class="container">
        <div class="card" style='max-width: 480px; margin: 0 auto;'>
            <div class="card-header">出错啦～</div>
                <div class="card-body">
                <p class="card-text">{ctx.state.render.staticConv(errorMessage)}</p>
                <a href="javascript: history.back()" class="btn btn-primary">
                    返回上一页
                </a>
            </div>
        </div>
    </div>
);

// Migrated from redirect.pug.
export const redirect: HtmlTmpl<{
    title?: string,
    message: string,
    targetHref: string,
    targetText: string,
    numberOfSeconds: number
}> = (ctx, self) => (
    <div class="container">
        <div class="card" style='max-width: 480px; margin: 0 auto;'>
            <div class="card-header">{self.title || '操作完成'}</div>
            <div class="card-body">
                <p class="card-text">{ctx.state.render.staticConv(self.message)}</p>
                <a
                    id="primary-link"
                    href={self.targetHref}
                    class="btn btn-primary"
                >
                    跳转至{ctx.state.render.staticConv(self.targetText)}
                </a>
            </div>
            <div class="card-footer text-muted">
                {'页面将在 '}
                <span id="countdown-seconds">{self.numberOfSeconds}</span>
                {' 秒后自动跳转'}
            </div>
        </div>
        <script type="text/javascript">{elements.safeHtml(`!function(n,e,i){function t(v){return window.location.replace(e.href),(n.parentNode)&&(n.parentNode.innerHTML='正在跳转中…页面加载请稍等～'),(clearInterval(i)),v&&v.preventDefault(),!1}e.addEventListener("click",t);var r=parseInt(n.innerHTML.trim());i=setInterval(function(){n.innerHTML=--r,r<=0&&t()},1e3)}(document.getElementById("countdown-seconds"),document.getElementById("primary-link"));`)}</script>
    </div>
);

// Migrated from redirect_post.pug.
export const redirectPost: HtmlTmpl<{
    path: string,
    body: Record<string, string>
}> = (ctx, self) => (
    <div class="container">
        <div class="card" style='max-width: 480px; margin: 0 auto;'>
            <div class="card-header">跳转中</div>
            <div class="card-body">
                <form action={self.path} method="post">
                    {Object.entries(self.body).map(([key, val]) => <input type="hidden" name={key} value={val} />)}
                    <button id="primary-btn" type="submit" class="btn btn-primary">
                        完成跳转
                    </button>
                </form>
            </div>
        </div>
        <script type="text/javascript">{elements.safeHtml(`(function (btn) { var pr = btn.parentNode; pr.replaceChild(document.createTextNode('正在跳转，请稍候…'), btn); pr.submit(); })(document.getElementById('primary-btn'));`)}</script>
    </div>
);
