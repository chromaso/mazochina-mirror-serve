export const initCodeEditor = (locale: 'cn' | 'tw') =>
    window.sceditor.create(document.getElementById('main-textarea') as HTMLTextAreaElement, {
        format: 'bbcode',
        style: 'https://cdn.jsdelivr.net/npm/sceditor@3/minified/themes/content/default.min.css',
        width: '100%',
        resizeWidth: false,
        resizeMinHeight: null,
        icons: 'material',
        emoticonsEnabled: false,
        locale,
        toolbarExclude: 'email,youtube,table,cut,copy,paste,pastetext,indent,outdent,code,ltr,rtl,date,time,print,font,justify,emoticon,subscript,superscript,left,center,right',
        startInSourceMode: true
    });