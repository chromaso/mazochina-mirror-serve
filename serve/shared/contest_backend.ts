import assert = require('assert');

import * as sStats from 'simple-statistics';

import { parseBBCode } from '../../common/bbcode';
import { escapeHtml } from '../../common/dependencies';
import { rowsToMap, rowsToMultiMap } from '../../common/logic_utils';
import { query } from '../db';

export const TEMP_EXTERNAL_WORKS: Map<number, [string, string]> = new Map([
    [-22440846, ['诱惑公馆~被恶女篡位的王国（1）', '2024-06-26T12:55:29Z']],
    [-22499718, ['堕天特区的冒险书—「彷徨一刻」', '2024-07-05T09:27:20Z']],
    [-22527901, ['一次被占卜师玩到漏液的实践经历', '2024-07-09T06:45:36Z']],
    [-22633964, ['喜欢玩弄乒乓球也喜欢被漂亮姐姐玩弄', '2024-07-24T14:23:56Z']],
    [-22650829, ['可恶的仇人女同性恋不可能夺走我的一切', '2024-07-27T08:01:03Z']],
    [-22650874, ['送死', '2024-07-27T08:08:59Z']],
    [-22651259, ['恶意', '2024-07-27T09:20:43Z']],
    [-22720502, ['诱导', '2024-08-06T13:05:37Z']],
    [-22731771, ['沦为邪恶女公爵玩物的伪娘王子与贵族公子', '2024-08-08T08:57:42Z']],
    [-22731884, ['路边', '2024-08-08T09:15:32Z']],
    [-22731952, ['神圣的榨精洗礼', '2024-08-08T09:27:49Z']],
    [-22752218, ['木漏れ日', '2024-08-11T04:54:52Z']],
    [-22752950, ['久留城', '2024-08-11T07:09:19Z']],
    [-22753351, ['Aschan的记录', '2024-08-11T08:15:04Z']]
]);

const numberNotNull = (_: number | null | undefined): _ is number => !!_;

export type ExtraInformation = {
    subscores1?: number[];
    post?: number;
};

export type ExtraInformationLegacy = ExtraInformation | number[] | null;

// Use the following SQL to revert all rows to tbe legacy format: `UPDATE contest_panel_reviews SET extra_information = extra_information -> 'subscores1' WHERE jsonb_typeof(extra_information) = 'object'`.
export const legacyRowCompat = <T extends { extra_information: ExtraInformationLegacy }>(row: T): (Omit<T, 'extra_information'> & { scores: number[] | undefined, extra_information: ExtraInformation }) => {
    let scores = row.extra_information ? (Array.isArray(row.extra_information) ? row.extra_information : row.extra_information.subscores1) : undefined;
    if (scores?.every(score => !score)) { scores = undefined; }
    return { 
        ...(row as Omit<T, 'extra_information'>),
        scores, // Can be undefined.
        extra_information: row.extra_information ? ({ ...(Array.isArray(row.extra_information) ? {} : row.extra_information), subscores1: scores }) : {}
    };
};

export const SCORE_COMPONENTS = [100, 80, 80, 40] as const;
export const TITLES_OF_COMPONENTS = ['色情', '文学', '合题', '创意', '总分'] as const;

export type SingleOut = { html: string, length: number, scores?: number[], devs?: number[], at: number };

export const parseBBCodeOneOff = (bbcode: string): Readonly<{ html: string, length: number }> => {
    try {
        const parsed = parseBBCode(0, bbcode, { lax: true });
        return {
            html: parsed.html,
            length: parsed.text.replace(/\s+/g, '').length
        };
    } catch (e) {
        return {
            html: bbcode.split('\n').map(row => escapeHtml(row)).join('<br />'),
            length: bbcode.replace(/\s+/g, '').length
        };
    }
};

const SORTORDER_EPSILON = 0.001;

type NumberToString<Type> = Type extends number ? string : {
    [Property in keyof Type]: Type[Property] extends number ? string : (Type[Property] extends object ? NumberToString<Type[Property] > : unknown);
};

/** Return sort order of numbers. Example: sortOrder([2, 1, 4, 5, 2, 5]) gives ['4=', '6', '3', '1=', '4=', '1='] */
const sortOrder = (input: number[]): string[] => {
    const sorted = input.map((number, index) => [number, index]).sort((a, b) => b[0] - a[0]);
    const retVal: string[] = new Array(input.length);
    let refIndexPlus1 = 0;
    let refValue = Number.POSITIVE_INFINITY;
    for (let i = 0; i < sorted.length; i++) {
        const [value, originalIndex] = sorted[i];
        if (refValue - value < SORTORDER_EPSILON) { // Too close to the last one.
            retVal[originalIndex] = String(refIndexPlus1) + '=';   
            if (i === refIndexPlus1) { retVal[sorted[refIndexPlus1 - 1][1]] += '='; }
            continue;
        }
        refIndexPlus1 = i + 1;
        retVal[originalIndex] = String(refIndexPlus1);
        refValue = value;
    }
    return retVal;
};

/**
 * Take a list of objects. Each of elements must be of a type specified by a "shape" parameter, otherwise behavior will be undefined.
 * 
 * All NUMERIC elements present in the "shape" refers to an element whose rank (among the list) needs to be found.
 * 
 * Returns a list of elements, same as the "input", but indicating the rank of each of its field.
 * 
 * Example: 
 * shape = { foo: [0, { bar: [0, 0] }] }.
 * input = [{ foo: [2, { bar: [4, 7] }] }, { foo: [9, { bar: [5, 3] }] }, { foo: [6, { bar: [1, 3] }] }]
 * 
 * returns [{ foo: ['3', { bar: ['2', '1'] }] }, { foo: ['1', { bar: ['1', '2='] }] }, { foo: ['2', { bar: ['3', '2='] }] }]
 */
export const renderSortOrderForFields = <ST, SI extends ST>(subShape: ST, subInputs: SI[]): NumberToString<ST>[] => {
    if (typeof subShape === 'number') {
        return sortOrder(subInputs as number[]) as any[];
    }
    if ((typeof subShape !== 'object') || (subShape === null)) { return subInputs.map(_ => subShape) as any; }
    if (Array.isArray(subShape)) {
        const eachRanked = subShape.map((v, i) => 
            renderSortOrderForFields(v, subInputs.map(subInput => (subInput as any)[i]))
        );
        return subInputs.map((v, i) => eachRanked.map(ranks => ranks[i])) as any;
    }
    const eachRanked = Object.entries(subShape).map(([k, v]) => [k, renderSortOrderForFields(v, subInputs.map(subInput => (subInput as any)[k]))]);
    return subInputs.map((v, i) => Object.fromEntries(eachRanked.map(([rk, rv]) => [rk, rv[i]])));
};

export const singleUserStats = async (userId: number, threadIds: number[]): Promise<Map<number, SingleOut>> => {
    const rows = (await query<{ thread_id: number, score: number, content: string, review_at: number, extra_information: ExtraInformation | number[] | null }>('SELECT DISTINCT ON (thread_id) thread_id, score, content, review_at, extra_information FROM contest_panel_reviews WHERE user_id = $1 AND thread_id = ANY($2::INT[]) ORDER BY thread_id, rev_id DESC', [userId, threadIds])).rows.map(legacyRowCompat);

    const normalizers = SCORE_COMPONENTS.map((_, index) => {
        const scores = rows.map(row => row.scores?.[index]).filter(numberNotNull);
        if (!scores.length) { return (() => 0); }
        const mean = sStats.mean(scores);
        const sd = sStats.standardDeviation(scores); // Can be zero.
        return sd ? ((score: number) => (score - mean) / sd) : (() => 0);
    });
    
    const output = new Map<number, SingleOut>();
    for (const row of rows) {
        if (row.scores && (row.scores.length !== SCORE_COMPONENTS.length)) {
            throw new Error('分项分数长度不对劲。');
        }
    
        output.set(row.thread_id, {
            ...parseBBCodeOneOff(row.content),
            ...(row.scores ? {
                scores: row.scores,
                devs: row.scores.map((score, index) => normalizers[index](score)),
            } : undefined),
            at: row.review_at
        });
    }

    return output;
};

export const appendSumInPlace = (input: number[]): number[] => {
    input.push(sStats.sumSimple(input));
    return input;
};

type Individual = {
    rawScores: number[] | null | undefined,
    dodScores: number[],
    dodScoresExpl: (string | undefined)[],
    stdScores: number[],
    stdScoresExpl: string[],
    textLength: number,
    at: number
};

// Return value to be in the same order as `threadIds` input.
const contestStats = async (threadIds: number[], panelMembers: number[]): Promise<{
    reviewers: Individual[], // To be in the same order as panelMembers.
    dodAgg: number[],
    stdAgg: number[],
    voteMetrics: [number, number, number]
}[]> => {
    const reviewsByUserId = rowsToMultiMap((await query<{ thread_id: number, user_id: number, rev_len: number, review_at: number, extra_information: ExtraInformation | number[] | null }>('SELECT DISTINCT ON (thread_id, user_id) thread_id, user_id, LENGTH(content) AS rev_len, review_at, extra_information FROM contest_panel_reviews WHERE thread_id = ANY($1::INT[]) ORDER BY thread_id, user_id, rev_id DESC', [threadIds])).rows.map(legacyRowCompat), 'user_id');

    const voteCounts = rowsToMultiMap((await query<{ thread_id: number, vote: number, cnt: number }>('SELECT thread_id, vote, COUNT(user_id)::INT AS cnt FROM contest_votes WHERE vote > 0 GROUP BY thread_id, vote')).rows, 'thread_id');

    // Outer array indexed by thread, to be in the same order as `threadIds`; inner array indexed by reviewer, to be in the same order as `panelMembers`
    const reviewersPerThread: Individual[][] = threadIds.map(_ => []);

    for (const userId of panelMembers) {
        const rows = reviewsByUserId.get(userId) ?? [];
        for (const row of rows) {
            if (row.scores && (row.scores.length !== SCORE_COMPONENTS.length)) {
                throw new Error('分项分数长度不对劲。');
            }
        }
        const helpersByComponent: ({ defaultDodValue: [number, string], normalizer: (score: number) => [number, string] })[] = SCORE_COMPONENTS.map((max, index) => {
            const scores = rows.map(row => row.scores?.[index]).filter(numberNotNull);
            if (!scores.length) {
                const defaultDodValue: [number, string] = [max * 0.5, '该评委未给任何作品评分，暂默认为满分的一半'];
                return {
                    defaultDodValue,
                    normalizer: score => {
                        assert.ok(!score, '这里不该有分数！');
                        return defaultDodValue;
                    }
                };
            }
            const mean = sStats.mean(scores);
            const sd = sStats.standardDeviation(scores); // Can be zero.
            return {
                defaultDodValue: [mean, '该评委未给此作品评分，按照此评委给其它作品给出的此维度平均分计算'],
                normalizer: score => {
                    if (!score) { return [max * 0.5, '该评委未给此作品评分，按照标准分数 z=0 计算']  }
                    const dev = (score === mean) ? 0 : ((score - mean) / sd);
                    const normalized = Math.min(Math.max(0.5 + dev * 0.2, 0), 1) * max;
                    return [
                        normalized,
                        `原始分数（${score}）换算为标准分数 z= ${dev.toFixed(2)}，故校正后分数为 (${dev.toFixed(2)} × 0.2 + 0.5) × ${max} = ${normalized.toFixed(1)}`
                    ];
                }
            };
        });

        const rowsByThreadId = rowsToMap(rows, 'thread_id');
        threadIds.forEach((threadId, index) => {
            const row = rowsByThreadId.get(threadId);
            const dodScores = row?.scores?.map((score, index) => score ? [score] as [number] : helpersByComponent[index].defaultDodValue) ?? helpersByComponent.map(pair => pair.defaultDodValue);
            const stdScores = (row?.scores ?? [0, 0, 0, 0]).map((score, index) => helpersByComponent[index].normalizer(score));

            reviewersPerThread[index].push({
                rawScores: row?.scores,
                dodScores: appendSumInPlace(dodScores.map(_ => _[0])),
                dodScoresExpl: dodScores.map(_ => _[1]),
                stdScores: appendSumInPlace(stdScores.map(_ => _[0])),
                stdScoresExpl: stdScores.map(_ => _[1]),
                textLength: row?.rev_len ?? 0,
                at: row?.review_at ?? 0
            });
        });
    }

    return reviewersPerThread.map((reviewers, threadIndex) => {
        const threadId = threadIds[threadIndex];
        let totalThumbups = 0;
        let totalMedals = 0;
        for (const vc of (voteCounts.get(threadId) ?? [])) {
            totalThumbups += vc.cnt;
            if (vc.vote === 2) { totalMedals += vc.cnt; }
        }
        const voteMetrics: [number, number, number] = [totalThumbups, totalMedals, totalThumbups + totalMedals];
    
        return {
            reviewers,
            dodAgg: appendSumInPlace(SCORE_COMPONENTS.map((_, componentIndex) => sStats.mean(reviewers.map(individual => individual.dodScores[componentIndex])))),
            stdAgg: appendSumInPlace(SCORE_COMPONENTS.map((_, componentIndex) => sStats.mean(reviewers.map(individual => individual.stdScores[componentIndex])))),
            voteMetrics
        };
    });
};

const DEADLINE = Date.parse('2024-08-10T23:59:59-1100') / 1000;

const parseIntDec = (v: string): number => parseInt(v);

export const fullContestStats = async (threads: { thread_id: number, firstpost_date: number }[], panelMembers: { user_id: number, user_name: string }[]): Promise<{
    // A list to be in the same order as `threads`.
    stats: Awaited<ReturnType<typeof contestStats>>,
    // A list to be in the same order as `threads`, each entry mirroring the shape of an entry in `stats`.
    ranks: { reviewers: { dodScores: string[], stdScores: string[] }[], dodAgg: string[], stdAgg: string[], voteMetrics: string[] }[],
    // A list of [metric name, metric values] pairs. Where each "metric values" is a list to be in the same order as `threads`.
    metricsForPlot: Readonly<[string, number[]] | null>[],
    // Each tuple is a (metricsForPlot index, metricsForPlot index, sample Pearson correlation coefficient between those two metrics).
    correlations: [number, number, number][];
}> => {
    const stats = await contestStats(threads.map(thread => thread.thread_id), panelMembers.map(member => member.user_id));
    const ranks = renderSortOrderForFields({
        reviewers: panelMembers.map(_ => ({ dodScores: [0, 0, 0, 0, 0], stdScores: [0, 0, 0, 0, 0] })),
        dodAgg: [0, 0, 0, 0, 0],
        stdAgg: [0, 0, 0, 0, 0],
        voteMetrics: [0, 0, 0]
    }, stats);

    const metricsForPlot: Readonly<[string, number[]] | null>[] = [];
    const correlations: [number, number, number][] = [];
    const addCorrelation = (index1: number, index2: number) => {
        assert.ok(index1 < index2);
        assert.ok(!correlations.some(([i1, i2]) => (i1 === index1) && (i2 === index2)), `${index1}, ${index2} already exists!`);
        correlations.push([index1, index2, sStats.sampleCorrelation(metricsForPlot[index1]![1], metricsForPlot[index2]![1])]);
    };
    // 0: post date (format: (deadline - post date) in hours), accompanied by ranking.
    metricsForPlot[0] = ['交稿时间（截稿前多少小时）', threads.map(thread => (DEADLINE - thread.firstpost_date) / 3600)];
    metricsForPlot[1] = ['交稿时间排名', sortOrder(metricsForPlot[0][1]).map(parseIntDec)];
    addCorrelation(0, 1);
    assert.strictEqual(metricsForPlot.length, 2);
    // [2, 7]: voteMetrics, each accompanied by ranking;
    ['👍', '🥇', '👍+🥇'].forEach((display, index) => {
        metricsForPlot.push([display + '数量', stats.map(thread => thread.voteMetrics[index])]);
        addCorrelation(0, metricsForPlot.length - 1);
        addCorrelation(1, metricsForPlot.length - 1);
        metricsForPlot.push([display + '排名', ranks.map(thread => thread.voteMetrics[index]).map(parseIntDec)]);
        addCorrelation(0, metricsForPlot.length - 1);
        addCorrelation(1, metricsForPlot.length - 1);
        addCorrelation(metricsForPlot.length - 2, metricsForPlot.length - 1);
    });
    addCorrelation(2, 4); addCorrelation(2, 6); addCorrelation(4, 6); // Between numbers.
    addCorrelation(3, 5); addCorrelation(3, 7); addCorrelation(5, 7); // Between rankings.
    assert.strictEqual(metricsForPlot.length, 8);
    // (empty slots filled with null)
    metricsForPlot[11] = null;
    // [12, 21]: rank for dodAgg, each accompanied by ranking;
    TITLES_OF_COMPONENTS.forEach((title, index) => {
        const baseIndex = metricsForPlot.length;
        metricsForPlot.push([title + ' - 平均分（原始）', stats.map(thread => thread.dodAgg[index])]); // at baseIndex.
        metricsForPlot.push([title + ' - 平均分（原始）排名', ranks.map(thread => thread.dodAgg[index]).map(parseIntDec)]); // at baseIndex + 1.
        addCorrelation(baseIndex, baseIndex + 1);
        addCorrelation(0, baseIndex);
        addCorrelation(1, baseIndex + 1);
        for (let i = 1; i <= index; i++) { // between components.
            addCorrelation(baseIndex - 2 * i, baseIndex);
            addCorrelation(baseIndex - 2 * i + 1, baseIndex + 1);
        }
    });
    assert.strictEqual(metricsForPlot.length, 22);
    // [22, 31]: rank for stdAgg, each accompanied by ranking;
    TITLES_OF_COMPONENTS.forEach((title, index) => {
        const baseIndex = metricsForPlot.length;
        metricsForPlot.push([title + ' - 平均分（标准化）', stats.map(thread => thread.stdAgg[index])]); // at baseIndex.
        metricsForPlot.push([title + ' - 平均分（标准化）排名', ranks.map(thread => thread.stdAgg[index]).map(parseIntDec)]); // at baseIndex + 1.
        addCorrelation(baseIndex, baseIndex + 1);
        addCorrelation(0, baseIndex);
        addCorrelation(1, baseIndex + 1);
        addCorrelation(baseIndex - 10, baseIndex); // dod vs std, score.
        addCorrelation(baseIndex - 10 + 1, baseIndex + 1); // dod vs std, ranking.
        for (let i = 1; i <= index; i++) { // between components.
            addCorrelation(baseIndex - 2 * i, baseIndex);
            addCorrelation(baseIndex - 2 * i + 1, baseIndex + 1);
        }
    });
    panelMembers.forEach((panelMember, memberIndex) => {
        assert.strictEqual(metricsForPlot.length, 32 + 20 * memberIndex);
        // [32 + (20 * i), 41 + (20 * i)]: reviewers[i].dodScores, each accompanied by ranking.
        TITLES_OF_COMPONENTS.forEach((title, index) => {
            const baseIndex = metricsForPlot.length;
            metricsForPlot.push([`${title} - ${panelMember.user_name}（原始）`, stats.map(thread => thread.reviewers[memberIndex].dodScores[index])]); // at baseIndex.
            metricsForPlot.push([`${title} - ${panelMember.user_name}（原始）排名`, ranks.map(thread => thread.reviewers[memberIndex].dodScores[index]).map(parseIntDec)]); // at baseIndex + 1.
            addCorrelation(baseIndex, baseIndex + 1);
            for (let i = 1; i <= index; i++) { // between components.
                addCorrelation(baseIndex - 2 * i, baseIndex);
                addCorrelation(baseIndex - 2 * i + 1, baseIndex + 1);
            }
            for (let i = 0; i <= memberIndex; i++) { // between agg and reviewers.
                addCorrelation(baseIndex - 20 * (i + 1), baseIndex);
                addCorrelation(baseIndex - 20 * (i + 1) + 1, baseIndex + 1);
            }
            if (index === TITLES_OF_COMPONENTS.length - 1) {
                addCorrelation(0, baseIndex);
                addCorrelation(1, baseIndex + 1);
            }
        });
        assert.strictEqual(metricsForPlot.length, 42 + 20 * memberIndex);
        // [42 + (20 * i), 51 + (20 * i)]: reviewers[i].stdScores, each accompanied by ranking.
        TITLES_OF_COMPONENTS.forEach((title, index) => {
            const baseIndex = metricsForPlot.length;
            metricsForPlot.push([`${title} - ${panelMember.user_name}（标准化）`, stats.map(thread => thread.reviewers[memberIndex].stdScores[index])]); // at baseIndex.
            metricsForPlot.push([`${title} - ${panelMember.user_name}（标准化）排名`, ranks.map(thread => thread.reviewers[memberIndex].stdScores[index]).map(parseIntDec)]); // at baseIndex + 1.
            addCorrelation(baseIndex, baseIndex + 1);
            addCorrelation(baseIndex - 10, baseIndex); // dod vs std, score.
            addCorrelation(baseIndex - 10 + 1, baseIndex + 1); // dod vs std, ranking.
            for (let i = 1; i <= index; i++) { // between components.
                addCorrelation(baseIndex - 2 * i, baseIndex);
                addCorrelation(baseIndex - 2 * i + 1, baseIndex + 1);
            }
            for (let i = 0; i <= memberIndex; i++) { // between agg and reviewers.
                addCorrelation(baseIndex - 20 * (i + 1), baseIndex);
                addCorrelation(baseIndex - 20 * (i + 1) + 1, baseIndex + 1);
            }
            if (index === TITLES_OF_COMPONENTS.length - 1) {
                addCorrelation(0, baseIndex);
                addCorrelation(1, baseIndex + 1);
            }
        });
    });
    return {
        stats,
        ranks,
        metricsForPlot,
        correlations
    };
}