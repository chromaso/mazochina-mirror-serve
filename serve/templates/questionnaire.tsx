import * as elements from 'typed-xhtml';
import { HtmlTmpl } from '../utils/tsx_utils';
import { qnaire24Fields } from '../shared/questionnaire';

export const questionnaire: HtmlTmpl<Record<(typeof qnaire24Fields)[number], string> | undefined> = (ctx, result) => (
    <div class="container">
        <h3>社区管理意见征询</h3>
        <p>此问卷入口只对资深用户及随机一部分活跃用户显示，但其它用户亦可通过此页面链接参与。以下为我（<a href="/author/25646">@chromaso</a>）暂难以决定的事项，故会基本按照此征询的结果来作出调整。</p>
        {result ? <div class="alert alert-info">你已提交过此问卷。你仍可修改后重新保存。</div> : <p>填写此问卷（及阅读其中的参考链接）或需要超过十五分钟时间。建议在有稍长的空闲时间时填写。非常感谢。你提交后此问卷后仍可回到此页面修改回答。</p>}
        <hr />
        <form method="post">
            <fieldset class="form-group row">
                <legend class="col-form-label col-sm-2 pt-0">同一作品不同章节分开发布的处理</legend>
                <div class="col-sm-10">
                    <p>将不同章节拆分在多个主题帖里发布的行为多次引起过注意。尤其是有一些广告性质的收费文，也利用每一预览章节单独发布来占据主题列表页，影响普通读者体验：</p>
                    <ul>
                        <li>先前<a href="/thread/1073748302">引起注意</a>的<a href="/thread/1073748298">例子</a></li>
                        <li><a href="/thread/1073751077">其它的例子</a></li>
                    </ul>
                    <p>目前我的计划是，<b>管理员可以直接将同一作品的不同章节合并</b>至同一主题帖内，无须征求作者同意。对此：</p>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="q1" id="q1t" value="t" checked={(result?.q1 === 't')} />
                        <label class="form-check-label" for="q1t">
                            赞成
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="q1" id="q1f" value="f" checked={(result?.q1 === 'f')} />
                        <label class="form-check-label" for="q1f">
                            反对
                        </label>
                    </div>
                    <textarea class="form-control" placeholder="补充说明（选填）" name="q1x">{result?.q1x}</textarea>
                </div>
            </fieldset>
            <hr />
            <fieldset class="form-group row">
                <legend class="col-form-label col-sm-2 pt-0">不使用「预览」标签的处理</legend>
                <div class="col-sm-10">
                    <p>有一些贩卖小说的作者，贴出部分章节作预览。读者需要联系作者才能购买全文。先前姑且决定对此类帖子暂不隐藏处理，但板规里有规定，这种情况必须使用「预览」标签。现在依然常有贩卖收费文章，只贴出预览部分却不使用「预览」标签的情况：</p>
                    <ul>
                        <li><a href="/thread/1073751499">违规帖子举例</a></li>
                        <li><a href="/thread/1073750365">另一个违规帖子举例</a></li>
                    </ul>
                    <p>对于这种情况，管理员每次去手动添加「预览」标签浪费时间且难免遗漏。因此，现在考虑选择：</p>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="q2" id="q2m" value="m" checked={(result?.q2 === 'm')} />
                        <label class="form-check-label" for="q2m">
                            管理员私信警告发布者「请修正相应标签，再犯将被封禁」
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="q2" id="q2r" value="r" checked={(result?.q2 === 'r')} />
                        <label class="form-check-label" for="q2r">
                            管理员在其帖子下公开回复警告「请修正相应标签，再犯将被封禁」
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="q2" id="q2n" value="n" checked={(result?.q2 === 'n')} />
                        <label class="form-check-label" for="q2n">
                            不作警告或封禁处理
                        </label>
                    </div>
                    <textarea class="form-control" placeholder="补充说明（选填）" name="q2x">{result?.q2x}</textarea>
                </div>
            </fieldset>
            <hr />
            <fieldset class="form-group row">
                <legend class="col-form-label col-sm-2 pt-0">标签列表的扩充</legend>
                <div class="col-sm-10">
                    <p>我拒绝过很多添加新标签的建议，很多时候是因为新标签<i>过于狭窄</i>（<a href="/post/1073837618">例子甲</a>、<a href="/post/1073805622">例子乙</a>），亦即某标签可能只会适配于太少的作品。我不希望候选标签列表变得过大、过于杂乱。过于狭窄、过于 specific 的内容或许使用关键字搜索即可。</p>
                    <p>但我依然不确定这是正确的决定，因此希望征询意见：</p>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="q3" id="q3c" value="c" checked={(result?.q3 === 'c')} />
                        <label class="form-check-label" for="q3c">
                            应当继续将标签总数控制在不太多的程度，仅当有较多(&gt; 5)某内容的作品时再创建标签
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="q3" id="q3e" value="e" checked={(result?.q3 === 'e')} />
                        <label class="form-check-label" for="q3e">
                            应当适当降低标准，只要存在某种主题/性癖，就有理由创建对应标签
                        </label>
                    </div>
                    <textarea class="form-control" placeholder="补充说明（选填）" name="q3x">{result?.q3x}</textarea>
                </div>
            </fieldset>
            <hr />
            <fieldset class="form-group row">
                <legend class="col-form-label col-sm-2 pt-0">「小说文字区」以外的板块使用标签</legend>
                <div class="col-sm-10">
                    <p>目前，标签功能仅限于「小说文字区」使用。我考虑将其扩展到所有板块（例如「游戏与截图」、「插画、同人及漫画」）使用，毕竟大多数内容标签对于除了小说以外的内容也是适用的。但在我目前还要花费时间开发其它功能的情况下，我不确定<i>将标签功能适用范围扩展至其它板块</i>这一工程是否值得占据更高优先级。</p>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="q4" id="q4t" value="t" checked={(result?.q4 === 't')} />
                        <label class="form-check-label" for="q4t">
                            应当尽快将标签功能扩展至所有板块
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="q4" id="q4f" value="f" checked={(result?.q4 === 'f')} />
                        <label class="form-check-label" for="q4f">
                            意义没那么大，暂时不必急于做此事
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="q4" id="q4d" value="d" checked={(result?.q4 === 'd')} />
                        <label class="form-check-label" for="q4d">
                            完全不必将标签功能扩展至其它板块
                        </label>
                    </div>
                    <textarea class="form-control" placeholder="补充说明（选填）" name="q4x">{result?.q4x}</textarea>
                </div>
            </fieldset>
            <hr />
            <fieldset class="form-group row">
                <legend class="col-form-label col-sm-2 pt-0">成就列表的扩充</legend>
                <div class="col-sm-10">
                    <p>关于成就功能，有<a href="/post/1073860063">提议</a>创建更多基于标签的成就。我暂时不想添加新的成就（原因包括不希望页面太杂乱、不希望成就变得毫无稀缺性，且一旦添加新的成就就难以删除，故不想草率为之），但并不确定这是正确的选择。</p>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="q5" id="q5t" value="t" checked={(result?.q5 === 't')} />
                        <label class="form-check-label" for="q5t">
                            应该添加大量<i>发布带有某标签的作品</i>这一类的成就
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="q5" id="q5m" value="m" checked={(result?.q5 === 'm')} />
                        <label class="form-check-label" for="q5m">
                            可以少量添加新的成就
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="q5" id="q5f" value="f" checked={(result?.q5 === 'f')} />
                        <label class="form-check-label" for="q5f">
                            在进一步观察成就机制的影响前，暂不添加新的成就
                        </label>
                    </div>
                    <textarea class="form-control" placeholder="补充说明（选填）" name="q5x">{result?.q5x}</textarea>
                </div>
            </fieldset>
            <hr />
            <fieldset class="form-group row">
                <legend class="col-form-label col-sm-2 pt-0">板规的定案</legend>
                <div class="col-sm-10">
                    <p><a href="/rules">板规的草案</a>已放置超过一年半。鉴于一直没收到什么修改的提议，我倾向于将其定案，并直接发帖将其贴到板务区内（毕竟跳转至 Google Docs 浏览还是不方便）。若就此计划有任何提议，请指出。请拨冗检读板规内容，若对其有任何提议，也请指出。</p>
                    <textarea class="form-control" placeholder="建议（选填）" name="q6x">{result?.q6x}</textarea>
                </div>
            </fieldset>
            <hr />
            <p>若有以上话题之外的意见建议，请直接<a href="/modmail">联系我</a>。</p>
            <button class="btn btn-primary" type="submit">提交/保存</button>
        </form>
    </div>
);

export const adminQuestionnaire: HtmlTmpl<{
    user_id: number, request_value: Record<(typeof qnaire24Fields)[number], string>, user_name: string, at_date: number
}[]> = (ctx, records) => {
    const textFieldRender = (text?: string) => text ? <>{text.split('\n').map(t => (<div>{t}</div>))}</> : null;
    return (
        <div class="container-fluid">
            <table class="table table-sm">
                <thead>
                    <tr>
                        <th scope="col">用户</th>
                        <th scope="col">章节拆分</th>
                        <th scope="col">预览标签</th>
                        <th scope="col">标签扩充</th>
                        <th scope="col">全局标签</th>
                        <th scope="col">成就扩充</th>
                        <th scope="col">板规定案</th>
                    </tr>
                </thead>
                <tbody>
                    {records.map(rec => (
                        <tr>
                            <th scope="row"><a href={'/author/' + rec.user_id}>{rec.user_name}</a></th>
                            <td>
                                {rec.request_value.q1}
                                {textFieldRender(rec.request_value.q1x)}
                            </td>
                            <td>
                                {rec.request_value.q2}
                                {textFieldRender(rec.request_value.q2x)}
                            </td>
                            <td>
                                {rec.request_value.q3}
                                {textFieldRender(rec.request_value.q3x)}
                            </td>
                            <td>
                                {rec.request_value.q4}
                                {textFieldRender(rec.request_value.q4x)}
                            </td>
                            <td>
                                {rec.request_value.q5}
                                {textFieldRender(rec.request_value.q5x)}
                            </td>
                            <td>
                                {textFieldRender(rec.request_value.q6x)}
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}