import createError = require('http-errors');
import { RouterContext } from 'koa-router';
import * as elements from 'typed-xhtml';

import { MazoContext, MazoKoaContext, MazoState } from './logic_utils';
import { nav } from '../templates/globals';
import assert = require('assert');
import { cssOf, SafeStringPossiblyWithStyle, staticHashes } from './tsx_utils';
import { RenderHelpers } from '../shared/options';
import * as errorTemplates from '../templates/error';

export const LOGIN_REQUIRED = 'LOGIN_REQUIRED';

const DUMMY_TAG_PAGE = Symbol('HtmlPageRenderred');
const DUMMY_TAG_SEGMENT = Symbol('HtmlSegmentRenderred');
type PageRendered = typeof DUMMY_TAG_PAGE;
type HtmlSegmentRendered = typeof DUMMY_TAG_SEGMENT;

export type PageImpl = (ctx: RouterContext<MazoState, MazoContext>) => Promise<PageRendered>;
export type HtmlSegmentImpl = (ctx: RouterContext<MazoState, MazoContext>) => Promise<HtmlSegmentRendered>;
export type RpcImpl = (ctx: RouterContext<MazoState, MazoContext>) => Promise<void>;

export const finalizeSegment = (ctx: MazoKoaContext, html: SafeString): HtmlSegmentRendered => {
    assert.ok(elements.isSafeHtml(html));
    assert.ok(cssOf(html) === undefined);
    ctx.body = html.valueOf();
    return DUMMY_TAG_SEGMENT;
};

export const ADDITIONAL_HDRS = {
    ADMIN_JS: (render: RenderHelpers) => <script type='text/javascript' src={'/static/admin-' + render.locale + '.js?v=' + staticHashes[3]} />,
    SCEDITOR: (render: RenderHelpers) => <>
        <link rel='stylesheet' type='text/css' href='https://cdn.jsdelivr.net/npm/sceditor@3/minified/themes/default.min.css' />
        <script type='text/javascript' src='/static/editor.js?updated=211027' />
        <script type='text/javascript' src={'/static/editor-' + render.locale + '.js'} />
    </>,
    CHARTS: (render: RenderHelpers) => <script type='text/javascript' src='https://www.gstatic.com/charts/loader.js' />
} as const;

export const finalizePage = (
    ctx: MazoKoaContext,
    options: {
        title: string,
        canonicalPath?: string,
        nav: boolean,
        deps?: (keyof typeof ADDITIONAL_HDRS)[]
    },
    safeBody: SafeStringPossiblyWithStyle
): PageRendered => {
    assert.ok(elements.isSafeHtml(safeBody));
    const { render } = ctx.state;
    const css = cssOf(safeBody);
    ctx.body = `<!DOCTYPE html><html lang="zh">${
        <head>
            <title>M系镜像 - {options.title}</title>
            <meta name='viewport' content='width=device-width, initial-scale=1' />
            {options.canonicalPath ? <link rel='canonical' href={'https://mirror.chromaso.net' + options.canonicalPath} /> : <meta name='robots' content='noindex' />}
            <link rel='icon' type='image/png' href='/static/favicon/192x192.png' />
            <link rel='apple-touch-icon' type='image/png' href='/static/favicon/180x180.png' />
            <link rel='stylesheet' type='text/css' href={render.themeCss} />
            <link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/icon?family=Material+Icons' />
            <link rel='stylesheet' type='text/css' href={'/static/styles.css?v=' + staticHashes[2]} />
            <script type='text/javascript' src={'/static/bundle-' + render.locale + '.js?v=' + staticHashes[0]} />
            {css ? <style type="text/css">{css}</style> : null}
            {options.deps ? options.deps.map(key => ADDITIONAL_HDRS[key](render)) : null}
        </head>
    }<body>${options.nav ? nav(ctx) : ''}${safeBody}</body></html>`;
    return DUMMY_TAG_PAGE;
};

export const renderErrorToCtxBody = (ctx: MazoKoaContext, errorMessage: string): PageRendered => {
    const { staticConv } = ctx.state.render;
    if (errorMessage === LOGIN_REQUIRED) {
        if (ctx.state.mazoLoginUser) {
            if (!ctx.state.mazoLoginUser.blocked) {
                console.error('[Should never happen] Regular user sent to login-required page!');
            }
            return finalizePage(ctx, {
                title: staticConv('你已被封禁'),
                nav: false
            }, errorTemplates.redirect(ctx, {
                title: staticConv('你已被封禁'),
                message: '你已被管理员封禁，因此无法执行此操作。请先联系管理员解封。', // Tempates already calls staticConv() on message.
                numberOfSeconds: 15,
                targetHref: '/moderation?u=' + ctx.state.mazoLoginUser.id,
                targetText: '封禁操作理由' // Tempates already calls staticConv() on targetText.
            }));
        } else {
            return finalizePage(ctx, { title: staticConv('此页面需登入后才可查看'), nav: false }, errorTemplates.errorLogin(ctx))
        }
    } else {
        return finalizePage(ctx, { title: staticConv('错误'), nav: false }, errorTemplates.error(ctx, { errorMessage }));
    }
};

export const redirectWithDelay = (ctx: MazoKoaContext, params: { message: string, targetText: string, targetHref: string, numberOfSeconds?: number }): PageRendered =>
    finalizePage(ctx, {
        title: ctx.state.render.staticConv('操作完成'),
        nav: false
    }, errorTemplates.redirect(ctx, {
        numberOfSeconds: 5, ...params
    }));

export const redirectWithoutDelay = (ctx: MazoKoaContext, path: string, fallback?: string): PageRendered => { 
    ctx.redirect(path, fallback);
    return DUMMY_TAG_PAGE;
};

export const redirectWithHttpPost = (ctx: MazoKoaContext, path: string, body: Record<string, string>): PageRendered =>    
    finalizePage(ctx, {
        title: ctx.state.render.staticConv('跳转中'),
        nav: false
    }, errorTemplates.redirectPost(ctx, {
        path, body
    }));

// Always to be used with "throw".
export const error = (httpCode: number, errorMessage: string): createError.HttpError => {
    const error = createError(httpCode, errorMessage);
    error.shouldBeRendered = true;
    return error;
};

export const processLocalContent = (content: string): string => content.replace(/<blockquote>/g, '<blockquote class="card p-3 d-block">');
