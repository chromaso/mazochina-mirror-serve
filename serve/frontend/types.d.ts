// This file is used just for imports between global/metro/admin.

// Whether a modal is allowed to be closed.
type ModalShouldClose = () => boolean;

// A method to close the modal. Note that `beforeClose` guard will be applied even when closing through this.
type ModalCloseMethod = () => void;

export interface Types {
    $: <K extends keyof HTMLElementTagNameMap>(
        // HTML tag name.
        tagName: K,
        // Class name if a string; properties (not attributes) map otherwise.
        props?: string | Partial<Record<keyof HTMLElementTagNameMap[K], any>>,
        // Child element(s).
        children?: HTMLElement | string | (HTMLElement | string)[],
        // Click event listener if a function; event listeners map otherwise.
        eventListeners?: ((e: MouseEvent) => void) | Partial<{ [P in keyof HTMLElementEventMap]: ((event: HTMLElementEventMap[P]) => void) }>
    ) => HTMLElementTagNameMap[K],
    $set: (parent: HTMLElement, children: HTMLElement | string | (HTMLElement | string)[]) => void;
    $xhr: (path: string, params: {
        /** On error callback. Must be present on call requests. */
        e: (error: string) => void;
    } & ({
        /** Encoded form data, to POST or NULL to post w/ an empty body. For GET request, don't include the field. */
        f?: XMLHttpRequestBodyInit | null;
        /** Caching can never be used on POST request. */
        c?: never;
    } | {
        /** GET request. */
        f?: never;
        /** Enable caching (based on path only). */
        c?: boolean;
    }) & ({
        /** On text response callback. JSON callback cannot be used at the same time. */
        t: (text: string) => void;
        j?: never;
    } | {
        t?: never;
        /** On JSON response callback. Text callback cannot be used at the same time. */
        j: (json: any) => void;
    })) => void,
    showModal: (title: string, body: Parameters<Types['$']>[2], beforeClose?: ModalShouldClose, size?: 'sm' | 'lg' | 'xl') => ModalCloseMethod,
    debugLog: (text: string) => void,
    getThreadIdFromUri: () => number,
    reportIfNeeded: (threadId?: number, postDate?: number) => void,
    closeDropdownMenuIfSourceInside: (source: HTMLElement) => boolean,
    tipper: (text: string) => HTMLElement,
    loadingSpinner: (inline: boolean, text?: string) => HTMLElement,
    positionPop: (anchor: HTMLElement, popup: HTMLElement, initialMouseEventX?: number) => void
}

// https://www.sceditor.com/api/sceditor/
type SCEditorInstanceLite = {
    bind(events: string, handler: Function): void;
    getSourceEditorValue(): string;
};

declare global {
    interface Window {
        // Exports from global/common-export to metro/common-import.
        '肉便器': [
            Types['$xhr'], // 0
            Types['$'], // 1
            Types['showModal'], // 2
            Types['debugLog'], // 3
            Types['$set'], // 4
            Types['getThreadIdFromUri'], // 5
            Types['reportIfNeeded'], // 6
            Types['closeDropdownMenuIfSourceInside'], // 7
            Types['tipper'], // 8
            Types['loadingSpinner'], // 9
            Types['positionPop'] // 10
        ],
        // Exports from metro/index to global/iast_runtime.
        postIast: (inputId: string, value: string, callback: (returnValue: [string, string?]) => void) => void,
        // SCEditor
        sceditor: {
            instance(textarea: HTMLElement): SCEditorInstanceLite | undefined,
            create(textarea: HTMLTextAreaElement, options: { format: 'bbcode' } & Partial<{
                toolbar: string,
                toolbarExclude: string | null,
                style: string,
                fonts: string,
                colors: string | null,
                locale: string,
                charset: string,
                startInSourceMode: boolean,
                emoticonsEnabled: boolean,
                emoticonsCompat: boolean,
                emoticonsRoot: string,
                emoticons: unknown,
                icons: string | null,
                width: string | number | null,
                height: string | number | null,
                resizeEnabled: boolean,
                resizeMinWidth: number | null,
                resizeMinHeight: number | null,
                resizeMaxHeight: number | null,
                resizeMaxWidth: number | null,
                resizeHeight: boolean,
                resizeWidth: boolean,
                dateFormat: string,
                toolbarContainer: HTMLElement | null,
                enablePasteFiltering: boolean,
                readOnly: boolean,
                rtl: boolean,
                autofocus: boolean,
                autofocusEnd: boolean,
                autoExpand: boolean,
                autoUpdate: boolean,
                runWithoutWysiwygSupport: false,
                id: string | null,
                plugins: string,
                spellcheck: boolean,
                disableBlockRemove: boolean,
                parserOptions: unknown,
                bbcodeTrim: boolean,
                dropDownCss: unknown,
                allowedTags: string[],
                allowedAttributes: string[]
            }>): SCEditorInstanceLite
        }
    }
    interface HTMLElement {
        also<T>(this: T | null | undefined, block: (it: T) => void): T;
    }
}
