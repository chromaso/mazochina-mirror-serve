import { $set } from './dollar';

// https://getbootstrap.com/docs/5.3/customize/color-modes/#javascript
const colorModeSwitch = () => {
    type ColorOption = 'auto' | 'dark' | 'light';
    const DESC_OF_OPTIONS: Record<ColorOption, [string, string]> = {
        'auto': ['自动切换深浅', 'brightness_medium'], 'dark': ['深色模式', 'dark_mode'], 'light': ['浅色模式', 'light_mode']
    };
    
    const setStoredTheme = (theme: ColorOption) => localStorage.setItem('mazomirror-color', theme)
    const getPreferredTheme = (): ColorOption => (localStorage.getItem('mazomirror-color') as any) || 'auto';
    
    const setTheme = (theme: ColorOption): void => document.documentElement.setAttribute('data-bs-theme', (theme === 'auto') ? (window.matchMedia('(prefers-color-scheme: dark)').matches ? 'dark' : 'light') : theme);

    setTheme(getPreferredTheme());

    window.matchMedia('(prefers-color-scheme: dark)').addEventListener('change', () => {
        setTheme(getPreferredTheme());
    });

    window.addEventListener('DOMContentLoaded', () => {    
        const target = document.getElementById('light-dark-mode-switch');
        if (!target) { return; } // Page without nav.
        const [icon, desc] = Array.from(target.children).filter((e): e is HTMLSpanElement => (e.tagName === 'SPAN'));
        const updateDisplay = (theme: ColorOption) => {
            const option = DESC_OF_OPTIONS[theme];
            $set(desc, option[0]);
            $set(icon, option[1]);
        };

        updateDisplay(getPreferredTheme());

        target.addEventListener('click', () => {
            const list = Object.keys(DESC_OF_OPTIONS) as ColorOption[];
            const next = list[(list.indexOf(getPreferredTheme()) + 1) % 3];
            setStoredTheme(next);
            setTheme(next);
            updateDisplay(next);
        });
    });
};

export const sideEffect = () => {
    // Nav-related: close popups when clicking outside.
    document.addEventListener('click', e => {
        const currentHash = location.hash.substr(1);
        if (!['search', 'login', 'delete-post', 'favorites', 'collections', 'notifications', 'pmessages' ,'menu'].includes(currentHash)) {
            return;
        }
        for (let node = e.target as HTMLElement; (node instanceof HTMLElement); node = node.parentNode as HTMLElement) {
            if (node.id === currentHash) { return; }
        }
        location.hash = '_';
    });

    try { colorModeSwitch(); } catch (e) {}
};