
import getRawBody = require('raw-body');

import { query as sql } from '../db';
import { RpcImpl, error, LOGIN_REQUIRED } from '../utils/render_utils';
import * as attachments from '../shared/attachment';
import { imagePreprocess } from '../shared/attachment';

export const putApiAttachment: RpcImpl = async ctx => {
    const userInfo = ctx.state.mazoLoginUser;
    if (!userInfo) { throw error(403, LOGIN_REQUIRED); }

    const file = await getRawBody(ctx.req, {
        length: ctx.req.headers['content-length'],
        limit: '6mb'
    });

    const magicNumber = file.slice(0, 3).toString('hex');
    const format = attachments.allowedFormats[magicNumber as keyof typeof attachments.allowedFormats];
    if (!format) {
        throw error(400, '当前只支持 JPEG、PNG、GIF 和 WebP 格式的图片～');
    }

    const attachmentIn24hours = (await sql('SELECT COUNT(attachment_id)::INT AS file_count, SUM(file_size) AS file_size_count FROM local_attachments WHERE user_id = $1 AND uploaded_date > $2', [userInfo.id, ctx.refDate - 86400])).rows[0];
    const numLimit = userInfo.confirmed ? 32 : 12;
    if (attachmentIn24hours.file_count >= numLimit) {
        throw error(429, `24 小时内不可上传超过 ${numLimit} 个附件。`);
    }
    const sizeLimit = userInfo.confirmed ? 16 : 6;
    if (attachmentIn24hours.file_size_count >= sizeLimit * 1024 * 1024) {
        throw error(429, `24 小时内不可上传超过 ${sizeLimit} MB 的附件。`);
    }

    const sizeLimitInKb = userInfo.confirmed ? 2560 : 960;
    const compressed = await imagePreprocess(file, sizeLimitInKb);

    ctx.body = '/' + await attachments.uploadAttachment(compressed, compressed !== file, userInfo.id, ctx.refDate, format, sql);
};