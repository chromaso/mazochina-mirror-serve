import { promises as fs } from 'fs';
import { tagPref } from './tags';
import { query } from '../db';
import { BaseThreadTarget, RenderThreadRetVal, renderThreads, THREAD_RENDER_COLS, ThreadRendering } from './thread_table';
import { MazoKoaContext } from '../utils/logic_utils';
import { shuffleInPlace } from 'simple-statistics';
import { error } from '../utils/render_utils';
import { SCORE_COMPONENTS } from './contest_backend';
import { fromRoot } from '../path';

export const CAMPAIGN_REVIEW_DISPLAY_ENABLED = true;

 // Campaign ID for which thread pages would include special UI for contest panel review.
export const REVIEW_CAMPAIGN_TAG_ID = 192;
export const META_THREAD_ID = 1073748496;

export type Campaign192Json = { 
    [threadId: string]: {
        scores: {
            avg: number,
            z: number,
            sd: number,
            rank: string | null
        }[],
        reviews: number,
        scorers: number[]
    } & ({
        votes: { count: number, rank: string | null }[],
        voters: Readonly<[number[], number[]]>
    } | {
        url: string, date: number, title: string 
    })
};

let timeoutForVacate: ReturnType<typeof setTimeout>;
let cached: Promise<Campaign192Json> | undefined;

// Supporting only 192.json is too specific, I know. But YAGNI until another campaign.
export const loadContest192Json = (): Promise<Campaign192Json> => {
    clearTimeout(timeoutForVacate);
    timeoutForVacate = setTimeout(() => {
        cached = undefined;
    }, 60000);
    if (cached) { return cached; }
    return cached = fs.readFile(fromRoot('static', 'campaign', '192.json'), 'utf8').then(json => Object.freeze(JSON.parse(json))).catch(e => { cached = undefined; throw e; });
};

type ExtThread = { thread_id: number, firstLink: string, thread_title: string,  firstTime: number };

export const allContestResults = async (ctx: MazoKoaContext): Promise<{ 
    threads: RenderThreadRetVal<BaseThreadTarget>,
    extThreads: ExtThread[],
    results: Campaign192Json,
    forewords: { user_id: number, user_name: string, post_id: number, content: string }[]
}> => {
    const results = await loadContest192Json();
    const threadIdResults = await query(`SELECT ${THREAD_RENDER_COLS.join(', ')} FROM thread_targets WHERE thread_id = ANY($1::INT[]) ORDER BY RANDOM()`, [Object.keys(results)]);
    const threads: RenderThreadRetVal<BaseThreadTarget> = (await renderThreads(ctx, threadIdResults.rows, {
        ...await tagPref(ctx),
        visible: true,
        catOrder: 3 // Do not include source tag.
    }));
    for (const thread of threads) {
        if (!thread.tags) { continue; }
        thread.tags = thread.tags.filter(tag => !tag.link.endsWith('/' + REVIEW_CAMPAIGN_TAG_ID));
    }
    const negativeIds = Object.keys(results).map(threadId => parseInt(threadId)).filter(single => single < 0);
    const forewords = (await query('SELECT user_id, users.user_name, post_id, content FROM posts LEFT JOIN users USING (user_id) WHERE thread_id = $1 AND posts.flags = 16 ORDER BY RANDOM()', [META_THREAD_ID])).rows;

    return {
        threads,
        results,
        forewords,
        extThreads: shuffleInPlace(negativeIds).map(negativeId => {
            const entry = results[negativeId];
            if (!('url' in entry)) { throw new Error(`Unexpected JSON[${negativeId}]`); }
            return {
                thread_id: negativeId,
                firstLink: entry.url,
                thread_title: entry.title,
                firstTime: entry.date
            };
        })
    };
};

export const totalScoreAfterValidation = (components: number[]): number => {
    if (components.length !== SCORE_COMPONENTS.length) { throw error(400, '分项分数未指定。'); }
    if (components.every(entry => entry === 0)) { return 0; }
    SCORE_COMPONENTS.map((max, index) => {
        if (!(components[index] <= max)) {
            throw error(400, `第 ${index + 1} 项分数不可以高于 ${max}。`);
        }
        if (!(components[index] > 0)) {
            throw error(400, '不可以只填写部分分项的分数 - 只可以全部填写却全部留 0。');
        }
    });
    return components.reduce((a, b) => a + b);
};
