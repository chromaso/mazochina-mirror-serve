import { Middleware, ParameterizedContext } from 'koa';
import getRawBody = require('raw-body');
import querystring = require('querystring');

import { query as sql } from '../db';
import { GuestInfo, UserInfo } from '../shared/login';
import { RawConfig, FullConfig, RenderHelpers } from '../shared/options';
import { unawaited } from '../../common/logic_utils';
import { IastInput, randomName } from '../../common/iast';
import { OCC_SC } from '../chinese';

export type MazoContext = {
    refDate: number; // Date in seconds.
    tc: boolean; // Use zh-TW instead of zh-CN variant of templates.
};

export type MazoState = {
    mazoLoginUser?: UserInfo;
    mazoGuest?: GuestInfo;
    userConfigRaw: RawConfig;
    userConfig: FullConfig;
    render: RenderHelpers;
};

export type MazoKoaContext = ParameterizedContext<MazoState, MazoContext>;

export const parseInt32 = (input: string): number => {
    let val = parseInt(input);
    if ((val > 0x7fffffff) || (val < -0x80000000)) { // Will fail postgres anyway.
        val = NaN;
    }
    return val;
};

class FormBody {
    constructor(private readonly body: NodeJS.Dict<string | string[]>) {}

    // String field in post body. Defaults to "".
    string(key: string): string {
        const raw = this.body[key] ?? '';
        if (typeof raw !== 'string') { return ''; }
        return raw.trim();
    }

    // Integer field in post body. Defaults to NaN.
    integer(key: string): number {
        const raw = this.body[key] ?? '';
        if (typeof raw !== 'string') { return NaN; }
        return parseInt32(raw);
    }

    // Integer array field. May include NaN.
    integerArray(key: string): number[] {
        const raw = this.body[key];
        if (raw === undefined) { return []; }
        return (typeof raw === 'string') ? [parseInt32(raw)] : raw.map(parseInt32);
    }
}

declare module "koa" {
    interface Request {
        body: FormBody;
    }
}

export const bodyParser: Middleware = async (ctx, next) => {
    if (ctx.is('application/x-www-form-urlencoded')) {
        // Should there be any errors, no need to catch. `getRawBody` throws error created by createError.
        const rawBody = await getRawBody(ctx.req, {
            length: ctx.req.headers['content-length'],
            limit: '768kb',
            encoding: true
        });
        ctx.request.body = new FormBody(querystring.parse(rawBody));
    } else {
        ctx.request.body = new FormBody({});
    }

    await next();
};

export const updateForumTimestamp = async (forumId: number, seconds: number, indirect?: boolean) => {
    const returned = (await sql('UPDATE forum_targets SET most_recent_update_include_children = GREATEST($1, most_recent_update_include_children)' + (indirect ? '' : ', most_recent_update = GREATEST($1, most_recent_update)') + ' WHERE forum_id = $2 RETURNING parent_forum_id', [seconds, forumId])).rows[0];
    if (!returned) {
        throw new RangeError(`No forum ${forumId} exists.`);
    }
    if (returned.parent_forum_id) {
        await updateForumTimestamp(returned.parent_forum_id, seconds, true);
    }
};

// Returns type-3 notifications that has been added/deleted.
export const sendReplyNotifications = async (postId: number, replyToUsers: number[], date: number): Promise<{ addedUsers: number[], removedUsers: number[] }> => {
    const removalJob = sql<{ user_id: number }, [number, number[]]>('DELETE FROM notifications WHERE message_type = 3 AND primary_target_id = $1 AND NOT (user_id = ANY($2::INT[])) AND dismissed = 0 RETURNING user_id', [postId, replyToUsers]);
    const additionJob = sql<{ user_id: number }, [number, number, number[]]>('INSERT INTO notifications (user_id, notification_date, message_type, primary_target_id) SELECT user_ids, $1, 3, $2 FROM UNNEST($3::INT[]) user_ids ON CONFLICT (user_id, message_type, primary_target_id) DO NOTHING RETURNING user_id', [date, postId, replyToUsers]); // It will only return those actually added, not those on conflict.

    return {
        addedUsers: (await additionJob).rows.map(row => row.user_id),
        removedUsers: (await removalJob).rows.map(row => row.user_id)
    };
};

export const myFirstPostInThread = async (userInfo: UserInfo | undefined, threadId: number): Promise<number> => {
    if (!userInfo) { return 0; }
    const myPosts = await sql('SELECT post_id FROM posts WHERE most_recent_update >= 0 AND thread_id = $1 AND user_id = $2 ORDER BY posted_date ASC, post_id ASC LIMIT 1', [threadId, userInfo.id]);
    return (myPosts.rows.length > 0) ? myPosts.rows[0].post_id : 0;
};

// Very fast and naive hash function returning a number in [0, 1).
const rapidHash = (input: string): number => {
    // https://stackoverflow.com/a/7616484
    let hash = input.length % 8;
    for (let i = 0; i < input.length; i++) {
        hash = ((hash << 5) - hash) + input.charCodeAt(i);
        hash |= 0;
    }
    hash &= 0xffff;
    // LCG https://en.wikipedia.org/wiki/Linear_congruential_generator#Parameters_in_common_use
    hash = (75 * hash + 74) % 65537;
    return hash / 65537;
};

type IastDefaultEntry = [
    string, // Default value. Escaped for type=select, unescaped otherwise.
    1 | 0, // 1 means explicit default, 0 means implicit default.
    IastInput['type']
];

// Note: the returned object is subject to modification by callers, thus should NEVER be reused.
export const getIastDefaultOptions = async (threadId: number, currentUserName?: string): Promise<{ [key: string]: IastDefaultEntry }> => {
    const query = await sql<{ key: string, value: IastInput }, [number]>('SELECT vs.* FROM iast_config, jsonb_each(iast_config.iast_inputs) AS vs WHERE thread_id = $1', [threadId]);
    const output: { [key: string]: IastDefaultEntry } = {};
    let nameCount = 0;
    for (const row of query.rows) {
        if (row.value.default) { // Explicit default value set.
            output[row.key] = [row.value.default, 1, row.value.type];
        } else {
            let defaultValue = '';
            // Those are "implicit defaults", contrary to "explicit defaults" specified by the author.
            switch (row.value.type) {
                case 'cnm':
                    defaultValue = randomName(rapidHash(row.key), ++nameCount);
                    break;
                case 'any':
                    defaultValue = '？？？';
                    break;
                default:
                    if (!Array.isArray(row.value.type)) {
                        console.error(`Should never happen: iast[${threadId}][${row.key}] bad default`);
                    }
            }
            // An array wrapping indicates it's never actually explicit.
            output[row.key] = [defaultValue, 0, row.value.type];
        }
        if (row.value.uname && currentUserName) {
            output[row.key] = [currentUserName, 0, row.value.type];
        }
    }
    return output;
};

// For threads whose lastTime is before this (in seconds), treat them as read.
export const TREAT_AS_READ_BEFORE = 1633000000;

export const logSpecialRequest = (ctx: MazoKoaContext, requestKey: string, requestValue: number | String | { [key: string]: string | number }) => unawaited(
    sql('INSERT INTO stats.special_requests (user_id, request_key, request_value, ua, ip, at_date) VALUES ($1, $2, $3, $4, $5, $6)', [
        ctx.state.mazoLoginUser?.id ?? ctx.state.mazoGuest?.id ?? 0,
        requestKey,
        (typeof requestValue === 'object') ? JSON.stringify(requestValue) : requestValue,
        ctx.headers['user-agent'] || '',
        ctx.ip,
        ctx.refDate
    ]),
`logging special_requests.${requestKey}`);

export const normalize = (val: string): string => OCC_SC.convertSync(val.toLowerCase());// IaSt uses this to normalize identifiers.

export const escapeForIdentifier = (text: string): string => {
    const filtered = normalize(text).replace(/[^a-z0-9\u4E00-\u9FA5\u3400-\u4DB5_\-]/g, '');

    if (!filtered.length) { return '~'; }
    return filtered.replace(/[\u007F-\uFFFF]/g, chr => '\\u' + ("0000" + chr.charCodeAt(0).toString(16)).substr(-4));
};
