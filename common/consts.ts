
export const THRESHOLD_FOR_LOCAL = 1073741823;

// spam_likeness value for soft-spam that's manually marked.
export const SPAM_THRESHOLD = 40;
// spam_likeness value for hard-spam that's manually marked, or other threads manually hidden).
export const SPAM_MANUAL = 1001;
// All other spam_likeness are auto.

// For a "special" thread (defined by `threadNeedsSpecialSortDateCalc`), sortpost_date will only be updated if the post length is larger than this.
export const LENGTH_THRESHOLD_FOR_UPDATING_SORTPOST_DATE = 25;

export const ADMIN_DASH_TIMESERIES_CHUNK_LENGTH = 3600 * 8;
export const ADMIN_DASH_TIMESERIES_CHUNK_COUNT = 45;

export const ORIGIN_ROOT_WTS = 'http://mazochina.com/forum/';