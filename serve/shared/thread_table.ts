import { SPAM_THRESHOLD } from '../../common/consts';
import { rowsToMap } from '../../common/logic_utils';
import { query as sql } from '../db';
import { F18_CONSTS, LIMIT_MEANINGS, TagDisplayStyle, TagInfoInThreadTable, allTagsById, displayInThreadsTable, tagPref } from './tags';
import { MazoKoaContext, TREAT_AS_READ_BEFORE } from '../utils/logic_utils';
import { forumsCache } from './forum';

export type TagStyleTuple = Readonly<[TagDisplayStyle, /* noWrap */ boolean]>;

type StarsRow = { thread_id: number, star: boolean };

export type BaseThreadTarget = {
    thread_id: number,
    thread_title: string,
    spam_likeness: number,
    forum_id: number,
    flags: number,
    firstpost_date: number,
    lastpost_date: number,
    lastpost_id: number,
    owner_user_id: number,
    lastpost_user_id: number,
    post_count: number
};

export type ThreadRendering<T extends BaseThreadTarget> = T & {
    thread_title: string;
    firstLink: string;
    isSpam: boolean;
    firstTime: number;
    firstUser: string;
    ownerId: number;
    lastLink: string;
    lastTime: number;
    lastUser: string;
    count: number;
    official: boolean;
    unread?: number;
    tags?: TagInfoInThreadTable[];
    star: boolean | undefined; // true for starred, false for blocked, undefined otherwise
    forumName: string;
};

export const THREAD_RENDER_COLS: (keyof BaseThreadTarget)[] = ['thread_id', 'thread_title', 'spam_likeness', 'forum_id', 'flags', 'firstpost_date', 'lastpost_date', 'lastpost_id', 'owner_user_id', 'lastpost_user_id', 'post_count'];

export type RenderThreadRetVal<T extends BaseThreadTarget> = (ThreadRendering<T>[]) & { tagStyle: TagStyleTuple };

// Returns a list which can be used in `thread_table.threadTable`, but also used in `thread_table.threadTableLite` and `favorite.collection`, even `contest_result.contestResults`.
// `activeUserId` is soly used to show read/unread state and star status. Omit this parameter if caller does not need that feature.
export const renderThreads = async <T extends BaseThreadTarget>(ctx: MazoKoaContext, threadRows: T[], f18TagsConf: Awaited<ReturnType<typeof tagPref>>, activeUserId?: number): Promise<RenderThreadRetVal<T>> => {
    const forumMeta = await forumsCache;

    const threadIds = threadRows.map(row => row.thread_id);
    const starredOrBlockedThreadsRequest = activeUserId ? sql<StarsRow>('SELECT thread_id, (star_type & 8) = 0 AS star FROM thread_stars WHERE user_id = $1 AND thread_id = ANY($2::INT[]) AND star_type > 0', [activeUserId, threadIds]) : null;
    const readStatusRequest = activeUserId ? sql<{ thread_id: number, up_to_date: number }>('SELECT thread_id, up_to_date FROM unread_status WHERE user_id = $1 AND thread_id = ANY($2::INT[])', [activeUserId, threadIds]) : null;

    const userIds = new Set<number>();
    for (const { owner_user_id, lastpost_user_id } of threadRows) {
        userIds.add(owner_user_id);
        userIds.add(lastpost_user_id);
    }
    const authorInfoRequest = sql('SELECT user_id, user_name FROM users WHERE user_id = ANY($1::INT[])', [Array.from(userIds)]);

    const includesF18 = threadRows.some(thread => thread.forum_id === 18) && f18TagsConf.visible;
    const f18TagsRequest = includesF18 ? Promise.all([
        sql<{ thread_id: number, tags: [number, number][] }>(`SELECT thread_id, ARRAY_AGG(ARRAY[tag_id, tag_score & ~3]) AS tags FROM tag_association WHERE thread_id = ANY($1::INT[]) AND tag_score > ${F18_CONSTS.CONSTS.SCORE_MULTIPLIER} GROUP BY thread_id`, [threadIds]),
        allTagsById()
    ] as const).then(([{ rows }, tags]) =>
        new Map(rows.map(row => [row.thread_id, displayInThreadsTable(row.tags, f18TagsConf, tags, LIMIT_MEANINGS[f18TagsConf.limit])]))
    ) : null;

    const starredOrBlockedThreads = starredOrBlockedThreadsRequest ? rowsToMap((await starredOrBlockedThreadsRequest).rows, 'thread_id') : undefined;

    const unreadBadges = new Map<number, number>();
    if (readStatusRequest) {
        const threadsWithReadStatus = rowsToMap((await readStatusRequest).rows, 'thread_id');
        for (const { thread_id, lastpost_date } of threadRows) {
            const readStatusRow = threadsWithReadStatus.get(thread_id);
            if (readStatusRow) {
                if (readStatusRow.up_to_date >= lastpost_date) {
                    unreadBadges.set(thread_id, -4); // Might be a bit weird here but -4 is for "all already read".
                } else {
                    unreadBadges.set(thread_id, -3); // -3 for "new replies, not sure many".
                }
            } else if (lastpost_date > TREAT_AS_READ_BEFORE) {
                unreadBadges.set(thread_id, -1); // For those threads that the entire thread is never opened, set to -1.
            } else {
                unreadBadges.set(thread_id, -2); // Too old. We don't have any information.
            }
        }
    }

    const authorInfos = rowsToMap((await authorInfoRequest).rows, 'user_id');

    const f18Tags = f18TagsRequest ? await f18TagsRequest : new Map<number, TagInfoInThreadTable[]>();

    const tagStyle = [f18TagsConf.style, f18TagsConf.nowrap] as const;

    return Object.assign(threadRows.map(row => {
        const threadId = row.thread_id;
        const forumRow = forumMeta.get(row.forum_id);
        return {
            ...row,
            firstLink: '/thread/' + threadId,
            isSpam: row.spam_likeness >= SPAM_THRESHOLD,
            firstTime: row.firstpost_date,
            ownerId: row.owner_user_id,
            firstUser: authorInfos.get(row.owner_user_id).user_name,
            lastLink: '/thread/' + threadId + '/' + Math.ceil(row.post_count / ctx.state.userConfig.tp) + '#p' + row.lastpost_id,
            lastTime: row.lastpost_date,
            lastUser: authorInfos.get(row.lastpost_user_id).user_name,
            count: row.post_count,
            unread: unreadBadges.get(threadId),
            star: starredOrBlockedThreads?.get(threadId)?.star,
            forumName: forumRow ? forumRow.forum_name : '[UNKNOWN]',
            official: !!(row.flags & 0x4),
            tags: f18Tags.get(threadId)
        };
    }), { tagStyle });
};
