import { getThreadIdFromUri, $, $id, $xhr, tipper, $set, loadingSpinner } from '../common-import';

type Status = 0 | 1 | 2 | 6 | 8;
type StarStatusInitJson = Status;
type StatusSet = {
    _prop_level: Status,
    _prop_pending: boolean,
    _prop_favlists: number[],
    _prop_blockButton: HTMLButtonElement,
    _prop_buttons: [HTMLButtonElement] | [HTMLButtonElement, HTMLButtonElement] // First entry must be with text; second button, if present, must be w/o text
};

const updateButtonDisplay = (buttonSource: StatusSet, status: Status, anim?: boolean): void => {
    $set(buttonSource._prop_blockButton, (status === 8) ? '解除屏蔽此主题' : '屏蔽此主题');
    buttonSource._prop_buttons.forEach(button => {
        const icon = button.firstChild as HTMLSpanElement;
        if (status === 8) {
            $set(icon, 'block');
        } else if (status > 0) {
            $set(icon, 'favorite');
        } else if (anim) {
            $set(icon, 'heart_broken');
            const child = icon.firstChild;
            setTimeout(() => {
                if (icon.firstChild === child) { $set(icon, 'favorite_border'); }
            }, 750);
        } else {
            $set(icon, 'favorite_border');
        }
        const newText = document.createTextNode((status <= 0) ? '收藏/订阅' : ((status > 1) ? ((status === 8) ? '已屏蔽' : '已收藏并订阅') : '已收藏'));
        const oldText = (button.lastChild!.nodeType === Node.TEXT_NODE) ? button.lastChild! : (button.lastChild as HTMLElement).firstChild!;
        oldText.parentNode!.replaceChild(newText, oldText);
    });
};

type FavList = Readonly<[number, string, number]>;
type ApiResponse = { favs: number[], level: Status, options?: FavList[], option?: FavList };

const updateStarStatus = (
    statusRef: StatusSet,
    requestString: string,
    onSuccess: (response: ApiResponse) => void,
    onError: (errorString?: string) => void
) => {
    if (statusRef._prop_pending) {
        onError('已经在操作中，请勿重复操作～'); return;
    }
    const threadId = getThreadIdFromUri();
    if (!threadId) {
        onError('当前页面未知'); return; 
    }
    statusRef._prop_pending = true;
    $xhr('/api/star/' + threadId, {
        f: 'action=' + requestString,
        j: (response: ApiResponse) => {
            try {
                statusRef._prop_level = response.level;
                statusRef._prop_favlists = response.favs;
                onSuccess(response);
            } catch (e) {
                onError((e as Error).message || String(e));
            }
            statusRef._prop_pending = false;
            updateButtonDisplay(statusRef, response.level, true);
        },
        e: err => { onError(err || '未知错误'); statusRef._prop_pending = false; }
    });
};

const raceConditionMove = (subCase: number, response: ApiResponse, extra: number) => new Error(`更新失败，请刷新试试吧 [${subCase}-${extra}-${response.level}-${getThreadIdFromUri()}-${response.favs.join('.')}]～`);

// LINT.IfChange
const MY_THREADS_DUMMY_COLLECTION = [0, '我发布的主题', 0x10 | 0x6] as const;
// LINT.ThenChange(../../shared/favorite.ts)

const constructDropdownMenu = (container: HTMLDivElement, statusRef: StatusSet, isMyThread: boolean, isLocal: boolean, options: NonNullable<ApiResponse['options']>) => {
    // Special treatment: when isMyThread, level 2 radio box will be hidden, so we check 1 instead, as they're essentially equivalent.
    const effectiveLevel = (level: Status) => ((isMyThread && (level === 2)) || (level === 0) || (level === 8)) ? 1 : level;

    $set(container, (statusRef._prop_level === 8) ? '你已屏蔽此主题' : '你已收藏此主题');

    const disableAll = (disabled: boolean) => {
        const each = (el: HTMLButtonElement | HTMLInputElement) => {
            el.disabled = disabled
        };
        Array.from(container.getElementsByTagName('button')).forEach(each);
        Array.from(container.getElementsByTagName('input')).forEach(each);
    };

    console.log('11');
    const uniqId = Math.random().toString(16).substr(2, 4);

    const radioClick = (e: MouseEvent) => {
        e.preventDefault();
        if (statusRef._prop_level === 0) {
            alert('你需要将主题添加至至少一个收藏夹，方可订阅。'); return;
        }
        if (statusRef._prop_level === 8) {
            alert('无法订阅一个已被你屏蔽的主题。'); return;
        }
        const radio = e.currentTarget as HTMLInputElement;
        const value = parseInt(radio.value) as Status;
        disableAll(true);
        updateStarStatus(statusRef, 'level&level=' + value, response => {
            if (response.level !== value) { throw raceConditionMove(1, response, value); }
            radio.checked = true;
            disableAll(false);
        }, error => { alert('操作失败：' + error); disableAll(false); });
    };
    const levelRadioButtons = [1, 2, 6].map(num => 
        $('input', { className: 'me-1', type: 'radio', name: 'fav-' + uniqId, value: String(num), checked: num === effectiveLevel(statusRef._prop_level) }, undefined, radioClick)
    );
    const subscriptionOptions = $('div', 'small mt-1', [
        $('label', undefined, '订阅选项'),
        $('label', 'form-check-label d-block', [levelRadioButtons[0], '不订阅回帖提醒']),
        $('label', 'form-check-label ' + (isMyThread ? 'd-none' : 'd-block'), [levelRadioButtons[1], '订阅楼主的回帖 ', tipper('楼主对他人的简短回复将不计入其中')]),
        $('label', 'form-check-label d-block', [levelRadioButtons[2], '订阅所有的回帖']),
    ]);

    console.log('12');
    const makeOption = (entry: FavList) => {
        const selected = statusRef._prop_favlists.includes(entry[0]);
        const isPrivate = (entry[2] & 0x10) !== 0;
        return $(
            'button',
            'list-group-item list-group-item-action d-flex align-items-center' + (selected ? ' list-group-item-primary' : ''), 
            [
                $('span', 'material-icons me-1', [selected ? 'check_box' : 'check_box_outline_blank']),
                entry[1],
                $('span', {
                    className: 'material-icons ms-1',
                    title: isPrivate ? '私密' : '公开'
                }, isPrivate ? 'visibility_off' : 'visibility')
            ], e => {
                if ((entry[0] === 0) && selected && isLocal) { alert('你在镜像发布的主题均会自动进入此收藏夹，无法取消～'); return; }
                if (selected && (statusRef._prop_favlists.length === 1) && (statusRef._prop_level > 1)) {
                    if (!confirm('将主题从收藏夹中移出后，将会取消你对新回复的订阅，确定吗？')) { return; }
                }
                const self = e.currentTarget as HTMLButtonElement;
                disableAll(true);
                updateStarStatus(statusRef, (selected ? 'remove' : 'append') + '&fav=' + entry[0], response => {
                    if (response.favs.includes(entry[0]) === selected) {
                        throw raceConditionMove(2, response, selected ? 1 : 0);
                    }
                    self.parentNode!.replaceChild(makeOption(entry), self);
                    if (response.level === 0) {
                        levelRadioButtons[0].checked = true;
                        container.replaceChild(document.createTextNode('你已取消收藏'), container.firstChild!);
                        subscriptionOptions.style.opacity = '0.75';
                    } else if (response.level === 8) {
                        levelRadioButtons[0].checked = true;
                        container.replaceChild(document.createTextNode('你已屏蔽此主题'), container.firstChild!);
                        subscriptionOptions.style.opacity = '0.75';
                    } else {
                        let level = effectiveLevel(response.level);
                        levelRadioButtons[(level === 6) ? 2 : (level - 1)].checked = true;
                        container.replaceChild(document.createTextNode('你已更新收藏'), container.firstChild!);
                        subscriptionOptions.style.opacity = '1';
                    }
                    disableAll(false);
                }, error => { alert('操作失败：' + error); disableAll(false); });
            }
        );
    };

    console.log('13');
    container.appendChild($('div', undefined, [
        $('div', 'list-group small text-start my-2', [
            ...isMyThread ? [makeOption(MY_THREADS_DUMMY_COLLECTION)] : [],
            ...options.map(makeOption),
            $('button', 'list-group-item list-group-item-action d-flex align-items-center list-group-item-secondary', [
                $('span', 'material-icons me-1', 'add'),
                '新建收藏夹'
            ], e => {
                const name = (prompt('新收藏夹的名字：') || '').trim();
                if (!name) { return; }
                const self = e.currentTarget as HTMLButtonElement;
                disableAll(true);
                updateStarStatus(statusRef, 'create&title=' + encodeURIComponent(name), response => {
                    if ((!response.level) || (!response.favs.length)) { throw raceConditionMove(3, response, response.option?.[0] ?? -1); }
                    self.parentNode!.insertBefore(makeOption(response.option!), self);
                    disableAll(false);
                    // Always enable.
                    subscriptionOptions.style.opacity = '1';
                }, error => { alert('操作失败：' + error); disableAll(false); });
            })
        ]),
        subscriptionOptions
    ]));
};

export const initThreadFav = (starStatus: StarStatusInitJson, isMyThread: boolean, isLocal: boolean): void => {
    const handle = $id('fav-button') as HTMLButtonElement;
    const fastHandle = $id('fav-quick') as HTMLButtonElement | null;

    const container = (handle.nextElementSibling as HTMLDivElement).firstElementChild! as HTMLDivElement; // the .px-2 div

    const currentStatus: StatusSet = {
        _prop_level: starStatus,
        _prop_favlists: [], // Can be wrong, but it's just a placeholder, won't be actually used.
        _prop_pending: false,
        _prop_blockButton: $id('block-thread-btn') as HTMLButtonElement,
        _prop_buttons: fastHandle ? [handle, fastHandle] : [handle]
    };

    updateButtonDisplay(currentStatus, starStatus);

    currentStatus._prop_buttons.forEach(btn => btn.addEventListener('click', e => {
        const dropdown = (e.currentTarget as HTMLElement).nextElementSibling as HTMLDivElement;
        dropdown.appendChild(container);

        if (container.querySelector('input') || currentStatus._prop_pending) { return; } // A menu is already constructed.

        const alreadyStarred = currentStatus._prop_level > 0;
        updateStarStatus(
            currentStatus,
            alreadyStarred ? 'view' : 'auto',
            response => {
                if ((!alreadyStarred) && !((response.level > 0) && response.favs.length)) {
                    throw raceConditionMove(4, response, alreadyStarred ? 1 : 0);
                }
                constructDropdownMenu(container, currentStatus, isMyThread, isLocal, response.options!);
            },
            error => alert(error)
        );
    }));

    currentStatus._prop_blockButton.addEventListener('click', () => {
        const removing = currentStatus._prop_level === 8;
        updateStarStatus(currentStatus, (removing ? 'remove' : 'append') + '&fav=-1', () => {
            $set(container, loadingSpinner(false, '屏蔽状态已更新…'));
        }, error => {
            console.log(error);
        });
    });
};