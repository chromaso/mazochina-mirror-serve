import { $xhr } from './dollar';
import { debugLog, getThreadIdFromUri } from './utils';

const cookieKeyForRead = 'mazomirror_ru';
const cookieKeyForFeedback = 'mazomirror_rp';

let timeout: ReturnType<typeof setTimeout> | undefined = void 0;

const getCookie = (key: string): string | undefined => {
    const magicLength = key.length + 1;
    return document.cookie.split(';').map(str => str.trim()).find(str => str.substr(0, magicLength) === key + '=')?.substr(magicLength);
};

const setCookie = (key: string, newValue: string): void => {
    document.cookie = key + '=' + newValue + ';samesite=lax' + ((location.protocol === 'https:') ? ';secure' : '') + ';max-age=63072000;path=/';
};

const onSuccessUpdate = (rp: string): void => {
    const reValues = getCookie(cookieKeyForRead);
    const reEncodedPairs = reValues ? reValues.split('q') : [''];
    const reDateEntry = reEncodedPairs.shift();
    const encodedPairs = rp.split('q');
    const filtered = reEncodedPairs.filter(entry => !encodedPairs.includes(entry));
    if (!filtered.length) {
        setCookie(cookieKeyForRead, '');
        debugLog('Cache removed as all entries are flushed');
    } else {
        setCookie(cookieKeyForRead, reDateEntry + 'q' + filtered.join('q'));
        debugLog(`Writing ${filtered.length} out of ${reEncodedPairs.length} back as update is not fully done`);
    }
};

// Returns true iff reported.
// https://stackoverflow.com/a/58192440 tells us we don't need to worry about race conditions.
export const reportIfNeeded = (threadId?: number, postDate?: number): boolean => {
    const oldValues = getCookie(cookieKeyForRead);

    // Format: "pair"s separated by `q`.
    // First entry is not a pair, but timestamp_sec.toString(26);
    // Following entries are pairs separated by `p`, each an integer toString(25).
    const encodedPairs = oldValues ? oldValues.split('q') : [];

    // Now the timestamp is shifted out. Can be undefined!
    const dateEntry = encodedPairs.shift();

    if (threadId && postDate) {
        const serializedStart = threadId.toString(25) + 'p';
        let found = false;
        for (let i = 0; i < encodedPairs.length; i++) {
            if (encodedPairs[i].substr(0, serializedStart.length) === serializedStart) {
                const oldValue = encodedPairs[i].substr(serializedStart.length);
                const oldPostDate = parseInt(oldValue, 25);
                if (postDate > oldPostDate) {
                    encodedPairs[i] = serializedStart + postDate.toString(25);
                    debugLog(`Updating ${threadId} to ${postDate} (prev. ${oldPostDate})`);
                } else {
                    debugLog(`Skipping ${threadId} to ${postDate} (prev. ${oldPostDate})`);
                }
                found = true;
                break;
            }
        }
        if (!found) {
            debugLog(`Adding cached ${threadId} to ${postDate}`);
            encodedPairs.push(serializedStart + postDate.toString(25));
        }
    } else if (!encodedPairs.length) {
        debugLog(`Empty on empty`);
        return false;
    }

    const newTrackDate = Math.round(Date.now() / 750);
    let oldTrackDate = parseInt(dateEntry!, 26);
    if (isNaN(oldTrackDate)) { oldTrackDate = newTrackDate; }
    const timeSinceLastRefresh = newTrackDate - oldTrackDate; // Unit would be 0.75 s. Will NOT be NaN.
    if (timeSinceLastRefresh > 8) { // Every 6 seconds, flush.
        debugLog(`Flushing since outdated for ${(newTrackDate - oldTrackDate) * 0.75} secs`);
    } else if (encodedPairs.length > 10) {
        debugLog(`Flushing since there are ${encodedPairs.length} pairs`);
    } else {
        const delay = Math.random() * 1500 + ((8 - timeSinceLastRefresh) * 750);
        if (timeout === void 0) {
            timeout = setTimeout(() => {
                debugLog(`Waked up from timeout ${timeout}.`);
                timeout = void 0;
                reportIfNeeded();
            }, delay);
            debugLog(`Not flushing yet, but scheduled ${Math.round(delay / 1000)} secs later @ ${timeout}`);
        } else {
            debugLog(`Not flushing yet, but waiting for existing ${timeout}`);
        }
        // Still update localStorage since there might be updated entries.
        const newSerialized = oldTrackDate.toString(26) + 'q' + encodedPairs.join('q');
        if (newSerialized !== oldValues) {
            setCookie(cookieKeyForRead, newSerialized);
        }
        return false;
    }
    if (timeout !== void 0) {
        debugLog(`Clearing previous timeout ${timeout}`);
        clearTimeout(timeout);
    }
    // Now we're sending an actual XHR.
    const versionSent = newTrackDate.toString(26) + 'q' + encodedPairs.join('q');
    setCookie(cookieKeyForRead, versionSent);
    const onError = (): void => console.error(`MMRS failed to update!`);
    $xhr('/api/read', {
        f: null,
        e: onError,
        t: responseText => {
            debugLog('Calling onSuccessUpdate from XHR: ' + responseText);
            onSuccessUpdate(responseText);
        }
    });
    return true;
};

const markThreadImpl = (threadId: number, oldPostDate: number, firstPostDate: number): void => {
    let previousDate = oldPostDate;

    const track = (date: number): void => {
        if (date <= previousDate) {
            debugLog(`Early-skipping ${threadId} to ${date} (prev. ${previousDate})`);
            return;
        }
        debugLog(`Marking ${threadId} to ${date}`);
        reportIfNeeded(threadId, date);
        previousDate = date;
    };

    let currentPostDate = firstPostDate;
    const initialPInHash = location.hash.match(/^#p(\d+)$/);
    if (initialPInHash) {
        const element = document.getElementById('p' + initialPInHash);
        if (element) {
            currentPostDate = parseInt(element.getAttribute('data-date') || '', 36);
        }
    }
    debugLog(`Init in thread page (${oldPostDate} -> ${currentPostDate})`);
    track(currentPostDate);

    const posts = Array.from(document.getElementsByClassName('mm-post'));
    const onScroll = (): void => {
        const windowHeight = document.documentElement.clientHeight;
        let lastPostInside: Element | undefined;
        for (const post of posts) {
            const rect = post.getBoundingClientRect();
            if (rect.bottom + rect.top > windowHeight * 2) {
                break;
            }
            lastPostInside = post;
        }
        if (!lastPostInside) { return; }
        const date = lastPostInside.getAttribute('data-date');
        if (!date) { console.error('MMRS failed to identify post'); return; }
        track(parseInt(date, 36));
    };
    document.addEventListener('scroll', onScroll);
    setTimeout(onScroll);
};

export const markThread = (oldPostDate: number, firstPostDate: number): void => {
    const threadId = getThreadIdFromUri();
    if (!threadId) { return; }
    if (document.hidden) {
        debugLog(`markThread postponed as page is hidden.`);
        const listener = () => {
            if (document.hidden) { return; }
            debugLog(`markThread finally executed.`);
            window.removeEventListener('visibilitychange', listener);
            markThreadImpl(threadId, oldPostDate, firstPostDate);
        };
        window.addEventListener('visibilitychange', listener);
    } else { // Include the case where `docuemnt.hidden` is not supported.
        debugLog(`markThread directly executed.`);
        markThreadImpl(threadId, oldPostDate, firstPostDate);
    }
};

export const sideEffect = () => {
    setTimeout(() => {
        const feedback = getCookie(cookieKeyForFeedback);
        if (feedback) { 
            debugLog('Calling onSuccessUpdate from init: ' + feedback);
            onSuccessUpdate(feedback);
            setCookie(cookieKeyForFeedback, ''); 
        } else {
            debugLog('No feedback');
        }
    });
};