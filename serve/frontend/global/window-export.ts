import * as campaign from './campaign';
import * as commonExport from './common-export';
import * as configure from './configure';
import * as dollar from './dollar';
import * as homepage from './homepage';
import * as iastRuntime from './iast_runtime';
import * as misc from './misc';
import * as nav from './nav';
import * as pagination from './pagination';
import * as pop from './pop';
import * as readStatusTrack from './read_status_track';
import * as tag from './tag';
import * as thread from './thread';

dollar.sideEffect(); // This must be run first!

commonExport.sideEffect();
nav.sideEffect();
pop.sideEffect();
readStatusTrack.sideEffect();

const global = window as any;

// Go to a post of specified ID.
// Usages: post content.
global['goToPost'] = misc.goToPost;

// In search page, show/hide a checkbox.
// Usages: search.tsx (search_id_page).
global['initSearchCheckbox'] = misc.initSearchCheckbox;

// Opens the settings modal.
// Usages: navbar, frontend homepage.ts.
global['openMazoSettings'] = configure.openMazoSettings;

// Initializes IaSt runtime in a thread page.
// Usages: thread.tsx (thread_id_page).
global['iastInitilize'] = iastRuntime.iastInitilize;

// Initializes IaSt runtime in a thread page.
// Usages: pagination.tsx (many endpoints).
global['initPagination'] = pagination.initPagination;

// Initializes all misc tweaks and features for a thread page.
// Usages: thread.tsx (thread_id_page).
global['threadPageInit'] = thread.threadPageInit;

// Shows vote counts (and if current user has voted) for each thread, in the campaign tag page. Only used when there is a VOTE_CAMPAIGN_TAG_ID.
// TODO(chromaso): comment this out when there is no active campaign.
// Usages: tag.tsx (tag_id).
global['tagPageCampaignVoteOverlay'] = campaign.tagPageCampaignVoteOverlay;

// Shows the list of users votes for a thread, when it's of REVIEW_CAMPAIGN_TAG_ID.
// Usages: contest_result.tsx (thread_id_page).
global['loadContestVoters'] = campaign.loadContestVoters;

// Shows the panel reviews of a thread, when it's of REVIEW_CAMPAIGN_TAG_ID.
// Usages: contest_result.tsx (thread_id_page).
global['loadReviewThread'] = campaign.loadReviewThread;

// Shows the panel reviews of a thread, when it's of REVIEW_CAMPAIGN_TAG_ID.
// Usages: contest_result.tsx (thread_id_page).
global['initResultsThreadList'] = campaign.initResultsThreadList;

// Initialized the home page.
// Usages: index.tsx (home).
global['initHomePage'] = homepage.initHomePage;

// Tracks read status of a thread.
// Usages: thread.tsx (thread_id_page).
global['markThread'] = readStatusTrack.markThread;

// In tag page, handle the switch button click.
// Usages: tag.tsx (tag_id).
global['initTagPageSwitch'] = tag.initTagPageSwitch;
