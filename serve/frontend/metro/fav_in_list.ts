import { $, $id, $xhr } from '../common-import';

// LINT.IfChange
const MY_THREADS_DUMMY_COLLECTION_ID = 0;
const BLOCKED_THREADS_DUMMY_COLLECTION_ID = -1;
// LINT.ThenChange(../../shared/favorite.ts)

const sendRequest = (threadId: number, requestString: string, onSuccess: (response: { favs: number[], level: 0 | 1 | 2 | 3 }) => void, onError: (error: string) => void) => {
    $xhr('/api/star/' + threadId, {
        f: 'action=' + requestString,
        j: response => {
            try {
                onSuccess(response as any);
            } catch (e) {
                onError((e as Error).message || '操作失败，请刷新重试～');
            }
        },
        e: err => { onError('操作失败：' + (err || '未知错误')); }
    });
};

export const initCollectionPage = (favId: number, empty: boolean) => {
    const deleteButton = $id('fav-form-delete') as HTMLInputElement | undefined;
    if (deleteButton && !empty) {
        deleteButton.addEventListener('click', e => {
            if (!window.confirm('你确定删除此收藏夹吗？此操作将无法撤销，且所有并未同时被其它收藏夹所收藏的主题将会被取消订阅。')) {
                e.preventDefault();
            }
        });
    }

    const isBlockingList = favId === BLOCKED_THREADS_DUMMY_COLLECTION_ID;

    const tbody = $id('favs-list')!;
    for (const tr of Array.from(tbody.children)) {
        const button = tr.querySelector('button')!;
        const sels: (HTMLSelectElement | HTMLButtonElement)[] = [button];

        const lockRow = (e: Event) => {
            sels.forEach(item => { item.disabled = true; });
            const td = (e.currentTarget as HTMLElement).parentNode as HTMLTableCellElement;
            const spinner = $('span', 'spinner-border');
            td.appendChild(spinner);
            return () => {
                sels.forEach(item => { item.disabled = false; });
                td.removeChild(spinner);
            };;
        };
        const errorOut = (text: string) => {
            tr.removeChild(tr.lastElementChild!);
            tr.replaceChild($('td', { colSpan: 2, className: 'align-middle text-center text-danger' }, text), tr.lastElementChild!);
        };
        const threadId = parseInt(tr.getAttribute('data-thread-id')!);

        let typeSel: HTMLSelectElement | undefined;
        if (!isBlockingList) {
            typeSel = tr.querySelector('select')!;
            const onSelect = (e: Event) => {
                const unlocker = lockRow(e);
                const level = parseInt(typeSel!.value);
                sendRequest(threadId, 'level&level=' + level, out => {
                    if (out.level !== level) { throw new Error(); }
                    unlocker();
                }, errorOut);
            };
            typeSel!.addEventListener('change', onSelect);
            sels.unshift(typeSel!);
        }

        const recover = (oldCells: HTMLTableCellElement[], e: Event) => {
            const btn = (e.currentTarget as HTMLElement) as HTMLButtonElement;
            btn.disabled = true;
            const thisCell = btn.parentNode as HTMLTableCellElement;
            const unlocker = lockRow(e);
            sendRequest(threadId, 'append&fav=' + favId, out => {
                if (typeSel) {
                    typeSel.value = String(out.level);
                }
                if (!out.favs.includes(favId)) { throw new Error(); }
                tr.removeChild(thisCell);
                while (oldCells.length) {
                    tr.appendChild(oldCells.pop()!);
                }
                tr.className = '';
                btn.disabled = false;
                unlocker();
            }, error => {
                // The user needs another chance to recover.
                alert('恢复' + error);
                btn.disabled = false;
                unlocker();
            });
        };
        button.addEventListener('click', (e: Event) => {
            if (favId === MY_THREADS_DUMMY_COLLECTION_ID) { alert('你发布的主题均会自动进入此收藏夹，无法取消～'); return; }
            const unlocker = lockRow(e);
            sendRequest(threadId, 'remove&fav=' + favId, out => {
                if (out.favs.includes(favId)) { throw new Error(); }
                tr.className = 'opacity-50 deleted';
                const backup: HTMLTableCellElement[] = [];
                const colCount = (isBlockingList ? 2 : 3);
                for (let i = 0; i < colCount; i++) {
                    backup.push(tr.removeChild(tr.lastElementChild as HTMLTableCellElement));
                }
                tr.appendChild($('td', { colSpan: colCount, className: 'align-middle text-center' }, [
                    isBlockingList ? '已解除屏蔽' : (out.favs.length ? '已从此收藏夹中移除' : '已取消收藏和订阅'),
                    $('button', 'btn btn-sm btn-secondary ms-1', '撤销操作', btnEvent => recover(backup, btnEvent))
                ]));
                unlocker();
            }, errorOut);
        });
        sels.forEach(item => { item.disabled = false; });
    }
};