-- Use most basic types so will be compatible with SQLite
-- All dates stored as timestamp in seconds, with INT

CREATE TABLE forum_targets (
    forum_id INT NOT NULL,
    forum_name TEXT NOT NULL DEFAULT '',
    last_success_crawl INT NOT NULL DEFAULT 0,
    most_recent_update INT NOT NULL DEFAULT 0,
    dirty_until_page INT NOT NULL DEFAULT 0,
    dirty_most_recent_update INT NOT NULL DEFAULT 0,
    successive_fail INT NOT NULL DEFAULT 0,
    parent_forum_id INT NOT NULL DEFAULT 0,
    display_order INT NOT NULL DEFAULT 0,
    refresh_interval INT NOT NULL DEFAULT 600,
    most_recent_update_include_children INT NOT NULL DEFAULT 0,
    PRIMARY KEY(forum_id)
);

CREATE TABLE thread_targets (
    thread_id INT NOT NULL, -- remote/local
    forum_id INT NOT NULL DEFAULT 0, -- remote/local
    thread_title TEXT NOT NULL DEFAULT '', -- remote/local
    last_success_crawl INT NOT NULL DEFAULT 0, -- remote only. local set to fixed 0
    last_page_number INT NOT NULL DEFAULT 0, -- remote only. local set to fixed 1073741824
    dirty_until_page INT NOT NULL DEFAULT 0, -- remote only. local set to fixed 0
    firstpost_date INT NOT NULL DEFAULT 0, -- remote/local, more like a cache for posts table
    lastpost_date INT NOT NULL DEFAULT 0, -- remote/local, more like a cache for posts table
    sortpost_date INT NOT NULL DEFAULT 0, -- remote/local, more like a cache for posts table. similar to lastpost_date but with special handling to lower spams
    lastpost_id INT NOT NULL DEFAULT 0, -- remote/local, only serves as a cache for posts table
    lastpost_user_id INT NOT NULL DEFAULT 0, -- remote/local, only serves as a cache for posts table
    post_count INT NOT NULL DEFAULT 0, -- remote/local, only serves as a cache for posts table
    remote_most_recent_update INT NOT NULL DEFAULT 0, -- remote only
    dirty_most_recent_update INT NOT NULL DEFAULT 0, -- remote only. local set to fixed 0
    successive_fail INT NOT NULL DEFAULT 0, -- remote only. local set to fixed 0 (unless for deleted which will be -1000)
    poll_title TEXT NOT NULL DEFAULT '', -- remote only. local set to fixed ''
    thread_title_sc TEXT NOT NULL DEFAULT '', -- remote/local
    spam_likeness INT NOT NULL DEFAULT 0, -- remote: auto. remote/local: 40 (soft-spam) / 1001 (hard-spam) / -1000 (manually clear)
    last_full_scan_requested INT NOT NULL DEFAULT 0, -- remote only. local set to fixed 0
    owner_user_id INT NOT NULL DEFAULT 0, -- remote/local
    remote_forum_id INT NOT NULL DEFAULT 0, -- remote only. local set to fixed 0
    flags SMALLINT NOT NULL DEFAULT 0 -- second-LSB 2 for reply warning, third-LSB 4 for official posts; fourth-LSB 8 for split post; fifth-LSB 16 for merged into another (this thread must be in a deleted state)
    PRIMARY KEY(thread_id)
);

CREATE INDEX thread_targets_forum_id ON thread_targets USING btree (forum_id);
CREATE INDEX thread_targets_owner_user_id ON thread_targets USING hash (owner_user_id);

CREATE TABLE post_targets ( --- For crawler use only
    post_id INT NOT NULL,
    crawled_at INT NOT NULL, -- date crawled, obviously
    src_html TEXT NOT NULL, -- subslv theme
    done_as_rev INT NOT NULL,
    most_recent_recheck INT NOT NULL,
    PRIMARY KEY(post_id)
);

CREATE TABLE posts (
    post_id INT NOT NULL, -- remote/local
    thread_id INT NOT NULL DEFAULT 0, -- remote/local
    title TEXT NOT NULL DEFAULT '', -- remote/local
    user_id INT NOT NULL DEFAULT 0,
    user_name TEXT NOT NULL DEFAULT '', --remote
    content TEXT NOT NULL DEFAULT '', -- remote/local
    posted_date INT NOT NULL DEFAULT 0, -- remote/local
    most_recent_update INT NOT NULL DEFAULT 0, -- remote/local. local set to actual last edit time, remote is set to claimed last edit time, or a the post_date if not claiming any. negate for deleted (both remote and local)
    content_sc TEXT NOT NULL DEFAULT '', -- remote/local
    rev_id INT NOT NULL DEFAULT 0, -- local only. 0 for remote
    flags INT NOT NULL DEFAULT 0, -- local only for now. second-least-significant bit (2) used for rep2see. third-least-significant bit (4) used for iast. fourth LSB (8) not used (reserved by PM feature). fifth-LSB (16) on for contest panel review
    xrefs JSONB NULL, -- raw LinksParsedFromBBCode - if null: this column is not populated (legacy posts); if empty object {}: empty
    PRIMARY KEY(post_id)
);

CREATE INDEX posts_thread_id ON posts USING btree (thread_id);
CREATE INDEX posts_user_id ON posts USING hash (user_id);

CREATE TABLE search_queries (
    query_id INT NOT NULL PRIMARY KEY, -- current timestamp in seconds; in case of conflict, use (current_max) + 1
    flags SMALLINT NOT NULL DEFAULT 0, -- LSB 1 for error, 0 for OK; second-least-significant bit (2) 0 for thread, 1 for post; third-LSB 1 to include spam, 0 to not include them; fourth-LSB (thread only) 1 for sort-by-origin, 0 for sort-by-reply. 
    terms TEXT NOT NULL, -- words, normalized, sorted, joined by space
    tags INT[] NULL, -- tags, after descedent expansion, sorted. negate if excluding
    user_id INT NOT NULL DEFAULT 0,
    forum_id INT NOT NULL DEFAULT 0,
    results INT[] NULL, -- null before the results are in
    initiator INT NOT NULL -- user ID to start the search
);

CREATE INDEX search_queries_query ON search_queries USING btree (flags, terms, user_id, forum_id);

CREATE TABLE users (
    user_id INT NOT NULL, -- remote/local
    user_name TEXT NOT NULL DEFAULT '', -- remote/local
    registered_at INT NOT NULL DEFAULT 0, -- remote/local
    label TEXT NULL,
    email TEXT NULL,
    password_h BYTEA NULL,
    flags SMALLINT NOT NULL DEFAULT 0, -- second-least-significant bit (2) 1 for blacklisted, 0 for not-blacklisted; third-least-significant bit (4) 1 for confirmed, 0 for not-confirmed, fifth-LSB (16) for campaign (contest) panel members
    avatar SMALLINT NOT NULL DEFAULT 0, -- 0 for disabled; -1 for inherently enabled third-party avatar; if positive: (refresh_timestamp - 1728970000) / 600 for third-party avatar; if negative (and < -1): avatar_id for first-party avatar
    configs BIGINT NOT NULL DEFAULT 0,
    avatar_skipck INT NOT NULL DEFAULT 0
    PRIMARY KEY(user_id)
);

CREATE TABLE user_tombstone ( -- local-only users that no longer exists since data has been transferred to a remote user
    prev_user_id INT NOT NULL, -- can be zero if directly bound to a remote user w/o creating a local user first
    new_user_id INT NOT NULL,
    registered_at INT NOT NULL,
    merged_at INT NOT NULL -- can be zero if manually merged, or that bound to a remote user w/o creating a local user first
);

CREATE INDEX user_tombstone_prev_user_id ON user_tombstone USING hash (prev_user_id) WHERE prev_user_id <> 0;
CREATE INDEX user_tombstone_new_user_id ON user_tombstone USING hash (new_user_id);

CREATE INDEX users_user_name ON users USING btree (user_name);
CREATE UNIQUE INDEX users_user_email ON users USING btree (email);
CREATE INDEX users_lower_user_name ON users USING hash (LOWER(user_name));

INSERT INTO users (user_id, user_name) VALUES (0, '[UNKNOWN]');

-- Local users/threads/posts have IDs starting at 2^30.

CREATE SEQUENCE local_user_id START 1073741824;
CREATE SEQUENCE local_thread_id START 1073741824;
CREATE SEQUENCE local_post_id START 1073741824;
CREATE SEQUENCE local_rev_id START 1073741824;
CREATE SEQUENCE remote_rev_id START 1;
CREATE SEQUENCE visitor_id START 1;

CREATE TABLE user_auth (
    session_id SERIAL NOT NULL PRIMARY KEY, -- negated if logged out.
    token BYTEA NOT NULL,
        -- 16-bytes binary.
        -- user_id can be extracted by using `user_auth_token_user_id` function below.
        -- login_date can be extracted by using `user_auth_token_date_ms` function below, which is in millisecs from epoch.
    replacing INT NOT NULL DEFAULT 0, -- the session ID this session is replacing. Negated iff just set, not actually voided the old one - positive if the old one is actually voided.
    ua TEXT NOT NULL,
    ips INET[] NOT NULL,
    domain TEXT NULL -- ctx.host instead of ctx.origin
);

CREATE UNIQUE INDEX user_auth_active ON user_auth USING btree (token) WHERE session_id > 0;

CREATE FUNCTION user_auth_token_user_id (token BYTEA) RETURNS INT LANGUAGE SQL IMMUTABLE LEAKPROOF STRICT PARALLEL SAFE AS $$ SELECT (GET_BYTE(token, 3) << 8 | GET_BYTE(token, 2) << 8 | GET_BYTE(token, 1) << 8 | GET_BYTE(token, 0)) $$;
CREATE FUNCTION user_auth_token_date_ms (token BYTEA) RETURNS BIGINT LANGUAGE SQL IMMUTABLE LEAKPROOF STRICT PARALLEL SAFE AS $$ SELECT (((GET_BYTE(token, 11) << 8 | GET_BYTE(token, 12) << 8 | GET_BYTE(token, 13))::BIGINT << 8 | GET_BYTE(token, 14) << 8 | GET_BYTE(token, 15)) + 1099511627776) $$;

CREATE TABLE local_post_log ( -- actual not local-only anymore, and applies also to PMs
    rev_id INT NOT NULL PRIMARY KEY,
    post_id INT NOT NULL,
    updated_at INT NOT NULL, -- for remote, not reliable
    title TEXT NOT NULL, -- for remote, not reliable
    content TEXT NOT NULL, -- in BBcode
    flags INT NOT NULL -- same as posts.flags or local_pms.flags (and those two are actually compatible with each other)
    user_id INT NOT NULL DEFAULT 0 -- if 0: post owner for local; there is a hole between 1073800124 and 1073802000.
);

CREATE INDEX local_post_log_post_id ON local_post_log USING hash (post_id);

CREATE TABLE local_conversations (
    thread_id INT NOT NULL PRIMARY KEY, -- uses sequence local_thread_id,
    from_user INT NOT NULL,
    to_user INT NOT NULL
);

CREATE INDEX local_conversations_from_user ON local_conversations USING btree (from_user);
CREATE UNIQUE INDEX local_conversations_users ON local_conversations USING btree (LEAST(from_user, to_user), GREATEST(from_user, to_user));

CREATE TABLE local_pms (
    post_id INT NOT NULL PRIMARY KEY, -- uses sequence local_post_id,
    thread_id INT NOT NULL, -- MUST be a local_conversations entry
    title TEXT NOT NULL DEFAULT '',
    content TEXT NOT NULL,
    rev_id INT NOT NULL,
    posted_date INT NOT NULL,
    flags SMALLINT NOT NULL, -- second-least-significant bit (2) and third-LSB (4) reserved for posts. fourth LSB (8) 1 if from conversation's to_user to from_user, 0 if from conversation's from_user to to_user.
    xrefs JSONB NULL -- raw LinksParsedFromBBCode - if null: this column is not populated (legacy posts); if empty object {}: empty
);

CREATE INDEX local_pms_thread_id ON local_pms USING btree (thread_id);

CREATE TABLE local_attachments (
    attachment_id SERIAL NOT NULL PRIMARY KEY,
    post_id INT NOT NULL DEFAULT 0, -- might be 0 before publishing,
    user_id INT NOT NULL,
    uploaded_date INT NOT NULL,
    file_size INT NOT NULL,
    file_hash BYTEA NOT NULL,
    flags SMALLINT NOT NULL DEFAULT 0 -- LSB (1) iff compressed at server-side
);

CREATE INDEX local_attachments_user_id ON local_attachments USING hash (user_id);

CREATE TABLE local_admin_log (
    admin_log_id SERIAL NOT NULL PRIMARY KEY,
    operation_date INT NOT NULL,
    admin_user_id INT NOT NULL,
    target_id INT NOT NULL,
    target_type VARCHAR(6) NOT NULL, -- user, post or thread
    note TEXT NOT NULL,
    extra_information JSONB NULL,
        -- post: always null
        -- user: can be { do: "free" } for unblocking; otherwise blocking
        -- thread: either
        --   - { from: number, to: number } where both values are forum IDs or a special value 0/-1/-2
        --   - { thread: number, posts: number[] } where an new thread ID and a list of post IDs are provided
    flags SMALLINT NOT NULL DEFAULT 0 -- LSB 1 to omit it in the list, 2nd-LSB to disassociate from owner
);

CREATE TABLE notifications (
    notification_id SERIAL NOT NULL PRIMARY KEY,
    user_id INT NOT NULL,
    notification_date INT NOT NULL,
    message_type SMALLINT NOT NULL, -- 2 for admin, 3 for post being replied, 4 for new post in subscribed thread, 5 for PM
    primary_target_id INT NOT NULL, -- (when message_type == 2) admin log id; (when message_type == 3) the reply post itself (not the post being replied); (when message_type = 4) thread id; (when message_type = 5) conversation thread id.
    dismissed INT NOT NULL DEFAULT 0 -- timestamp of the dismissal
);

CREATE INDEX notifications_user_id ON notifications USING btree (user_id, notification_date DESC); -- used when querying notifications for a user.
CREATE UNIQUE INDEX notifications_tuple ON notifications USING btree (message_type, primary_target_id, user_id); -- used when updating a notification of a post/thread.

CREATE TABLE unread_status (
    user_id INT NOT NULL, -- negative values possible for visitors
    thread_id INT NOT NULL,
    up_to_date INT NOT NULL,
    tracked_at INT NOT NULL,
    PRIMARY KEY (user_id, thread_id)
);

CREATE INDEX unread_status_thread_id ON unread_status USING hash (thread_id);

CREATE TABLE user_profile (
    rev_id SERIAL NOT NULL PRIMARY KEY,
    user_id INT NOT NULL,
    main_text TEXT NOT NULL,
    updated_at INT NOT NULL,
    pinned_threads INT[] NULL
);

CREATE INDEX user_profile_user_id ON user_profile USING btree (user_id, rev_id);

CREATE TABLE user_avatars (
    avatar_id SERIAL NOT NULL PRIMARY KEY,
    user_id INT NOT NULL,
    img_signature BYTEA NOT NULL,
    created_at INT NOT NULL,
    updated_at INT NOT NULL,
    flags SMALLINT NOT NULL DEFAULT 0 -- 0 if auto-uploaded from third party, 1 if manual
);

CREATE UNIQUE INDEX user_avatars_user_id ON user_avatars USING btree (user_id, img_signature);

CREATE TABLE user_achievements (
    user_id INTEGER NOT NULL,
    name_sc VARCHAR(6) NOT NULL,
    lvl SMALLINT NOT NULL, -- 3: gold (can be used as title), 2: silver, 1: bronze, cannot be anything else
    granted INTEGER NOT NULL,
    post_id INTEGER NOT NULL,
    PRIMARY KEY (user_id, name_sc, lvl)
);

CREATE TABLE iast_choices (
    user_id INT NOT NULL, 
    thread_id INT NOT NULL,
    choices JSONB NOT NULL, -- key - value string dict
    updated_at INT NOT NULL,
    PRIMARY KEY (user_id, thread_id)
);

CREATE TABLE iast_config (
    thread_id INT NOT NULL,
    post_id INT NOT NULL,
    iast_inputs JSONB NOT NULL, -- key - IastInput dict
    iast_outputs JSONB NOT NULL, -- key - IastDependencyRequirement dict,
    PRIMARY KEY (post_id)
);

CREATE TABLE thread_stars (
    user_id INT NOT NULL, 
    thread_id INT NOT NULL,
    star_type SMALLINT NOT NULL, -- [LOWER 4 BITS] 1: just star, do nothing; 2: subscribe op (i.e. owner_user_id) only; 3: subscribe OP regardless if it's an actual update (unused); 6: subscribe all posters; [5TH BIT] ignored; if entire field is -1: blocked
    star_at INT NOT NULL,
    fc_ids INT[] NOT NULL,
    PRIMARY KEY (user_id, thread_id)
);

CREATE TABLE fav_collections (
    fc_id SERIAL NOT NULL PRIMARY KEY,
    user_id INT NOT NULL,
    default_star_type SMALLINT NOT NULL DEFAULT 2, -- [LOWER 3 BITS] 1: just star, do nothing; 2: subscribe op (i.e. owner_user_id) only; 3: subscribe OP regardless if it's an actual update (unused); 6: subscribe all posters. [4TH BIT] 1 (0x8) to kill switch to BLOCKED. [5TH BIT] 1 if private; 0 if public; NOTICE THAT the 5TH bit is not a "default" type, but applies to the collection itself
    title TEXT NOT NULL,
    display_order SMALLINT NOT NULL DEFAULT 1000
);

CREATE INDEX fav_collections_user_id ON fav_collections USING hash (user_id);

-- 记录所有可以选择的标签
CREATE TABLE tags (
    -- 自动生成的标签 ID
    tag_id SERIAL NOT NULL PRIMARY KEY,
    -- category 字段：1 - 作品长度/状态，2 - 来源等
    category SMALLINT NOT NULL,
    primary_name TEXT NOT NULL,
    aliases TEXT[] NULL,
    -- 描述
    desc_text TEXT NOT NULL DEFAULT '', -- deprecated. use desc_html instead.
    desc_html TEXT NOT NULL DEFAULT '',
    -- LSB：1 为默认警告，0 为默认不警告
    -- 2nd LSB: 0 为可以选择，1 为不可直接选择；但 3rd LSB 为 1 时此项 undefined
    -- 3rd LSB: 1 为完全不可选择（隐藏）；此项为 1 时 2nd LSB 无意义
    -- 4th LSB - 13th LSB：出现在候选列表中的顺序（数值大者优先）
    flags SMALLINT NOT NULL DEFAULT 0
);

-- 记录标签的层级关系
CREATE TABLE tags_taxonomy (
    parent_tag INT NOT NULL,
    child_tag INT NOT NULL,
    PRIMARY KEY (parent_tag, child_tag)
);

-- 记录一个主题是否有某个标签
CREATE TABLE tag_association (
    -- 自动生成的 tag_association ID
    tagass_id SERIAL NOT NULL PRIMARY KEY,
    -- 对应的主题 ID
    thread_id INT NOT NULL,
    -- 对应的标签 ID
    tag_id INT NOT NULL,
    -- 分数（除了 LSB 与 2nd LSB）；LSB 1 为作者本人选择，0 为作者本人未选择
    -- 这可能作为主题列表页显示时的优先级（数值大者优先）（subject to runtime adjustments）
    tag_score INT NOT NULL
);

CREATE UNIQUE INDEX tag_association_primary ON tag_association USING btree (thread_id, tag_id);

-- 记录对「在某个主题上添加某个标签」的投票/提议
CREATE TABLE tag_votes (
    -- 对应的 tag_association 行
    tagass_id INT NOT NULL,
    -- 对应的用户 ID
    user_id INT NOT NULL,
    -- 作出投票的时间戳，若 -1 则为作者发布时便选择了标签
    voted_at INT NOT NULL, -- named "voted_at", actually never updated, it's just the date of original vote, even if `vote` changed later.
    -- 投票（3 为赞成，-3 为反对），资深用户则为（8 或 -8）
    vote SMALLINT NOT NULL,
    updated_at INT NOT NULL DEFAULT 0, -- the lastest update time. same as voted_at if never updated. can be 0 if the field is not tracked (for older entries, updated_at is never tracked).
    PRIMARY KEY (tagass_id, user_id)
);

CREATE TABLE tags_user_pref (
    user_id INT NOT NULL PRIMARY KEY,
    -- Tags to show warning symbol on. Position for override-on, negative for override-off.
    warning INT[] NULL,
    flags SMALLINT NOT NULL DEFAULT 0 -- LSB 1 to enable, 0 to disable; 2nd, 3rd and 4th LSB for style, 5th and 6th LSB for cats; 9th LSB for nowrap; 10th and llth LSB for limit
);

CREATE TABLE contest_votes (
    user_id INT NOT NULL, 
    thread_id INT NOT NULL,
    vote_at INT NOT NULL,
    vote INT NOT NULL, -- 2 for 🥇+👍, 1 for 👍
    PRIMARY KEY (user_id, thread_id)
);

CREATE INDEX contest_votes_thread_id ON contest_votes USING hash (thread_id);

CREATE TABLE contest_panel_reviews (
    rev_id SERIAL NOT NULL,
    thread_id INT NOT NULL,
    user_id INT NOT NULL,
    content TEXT NOT NULL DEFAULT '',
    score SMALLINT NOT NULL DEFAULT 0,
    review_at INT NOT NULL, -- negate if filled by admin
    extra_information JSONB NULL
);

CREATE INDEX contest_panel_reviews_tuple ON contest_panel_reviews USING btree (thread_id, user_id);

--- Logs

CREATE TABLE stats.special_requests (
    rev_id SERIAL NOT NULL PRIMARY KEY,
    user_id INT NOT NULL, -- can be zero
    request_key VARCHAR(14) NOT NULL,
    request_value JSONB NOT NULL,
    ua TEXT NOT NULL,
    ip INET NOT NULL,
    at_date INT NOT NULL
);

CREATE INDEX special_requests_user_id_key ON stats.special_requests USING btree (user_id, request_key) WHERE user_id > 0;

CREATE TABLE stats.achi_ppl (
    rev_id SERIAL NOT NULL PRIMARY KEY,
    updated_at INT NOT NULL,
    user_id INT NOT NULL,
    requester INT NOT NULL, -- user ID if online; -1 if offline
    count SMALLINT NOT NULL,
    duration INT NOT NULL -- milliseconds
);

CREATE INDEX achi_ppl_user_id ON stats.achi_ppl USING hash (user_id);

CREATE TABLE stats.browser_stat ( -- Collection removed @ commit 268716d.
    rev_id SERIAL NOT NULL PRIMARY KEY,
    date_report INT NOT NULL,
    results INT NOT NULL,
    js_ua TEXT NOT NULL,
    http_ua TEXT NULL, -- null iff the same as above.
    js_session INT NOT NULL,
    http_session INT NULL, -- null iff the same as above.
    js_tz TEXT NOT NULL,
    ips INET[] NOT NULL,
    domain TEXT NULL
);

--- Temporary table.

CREATE TABLE stats.thread_backup (
    post_id INT NOT NULL PRIMARY KEY,
    content TEXT NOT NULL
);