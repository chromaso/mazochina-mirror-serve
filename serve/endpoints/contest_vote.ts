import { query as sql } from '../db';
import { F18_CONSTS } from '../shared/tags';
import { MazoKoaContext, parseInt32 } from '../utils/logic_utils';
import { HtmlSegmentImpl, error, LOGIN_REQUIRED, finalizeSegment, RpcImpl } from '../utils/render_utils';
import { totalScoreAfterValidation } from '../shared/contest_result';

import * as templates from '../templates/contest_vote';

const renderCampaignVote = async (ctx: MazoKoaContext, threadId: number, canVote: boolean, currentVote: number): Promise<ReturnType<typeof finalizeSegment>> => {
    const userInfo = ctx.state.mazoLoginUser;
    if (!userInfo) { throw error(403, LOGIN_REQUIRED); }
    const isPanelMember = !!(userInfo.rawFlags & 0x10);

    const medalWinnerInfo = await sql('SELECT thread_id, thread_title FROM thread_targets WHERE thread_id IN (SELECT thread_id FROM contest_votes WHERE user_id = $1 AND vote = 2)', [userInfo.id]);
    const voteCounts = (await sql<{ cnt: number, vote: number }>('SELECT COUNT(user_id)::INT AS cnt, vote FROM contest_votes WHERE thread_id = $1 AND vote > 0 GROUP BY vote', [threadId])).rows;
    let totalThumbups = 0;
    let totalMedals = 0;
    for (const vc of voteCounts) {
        totalThumbups += vc.cnt;
        if (vc.vote === 2) { totalMedals += vc.cnt; }
    }

    return finalizeSegment(ctx, templates.campaignVote(ctx, {
        canVote,
        currentVote: (currentVote as 0 | 1 | 2),
        medalWinner: medalWinnerInfo.rows[0],
        totalThumbups,
        totalMedals,
        isPanelMember,
        currentReview: isPanelMember ? ((await sql('SELECT content, COALESCE(extra_information, \'[0,0,0,0]\'::JSONB) AS scores FROM contest_panel_reviews WHERE thread_id = $1 AND user_id = $2 ORDER BY rev_id DESC LIMIT 1', [threadId, userInfo.id])).rows[0] ?? { content: '', scores: [0, 0, 0, 0] }) : undefined
    }));
};

const canVoteForCampaign = async (userId: number): Promise<boolean> => (await sql('SELECT post_id FROM posts WHERE user_id = $1 AND most_recent_update > 0 AND posted_date < 1719792000 LIMIT 5', [userId])).rows.length >= 5;

export const apiThread_id_vote: HtmlSegmentImpl = async ctx => {
    if (!F18_CONSTS.VOTE_CAMPAIGN_TAG_ID) { throw error(404, '当前无征文活动'); }

    const threadId = parseInt32(ctx.params.id);
    const userInfo = ctx.state.mazoLoginUser;
    if (!userInfo) { throw error(403, LOGIN_REQUIRED); }
    return renderCampaignVote(ctx, threadId, await canVoteForCampaign(userInfo.id), (await sql<{ vote: number }>('SELECT vote FROM contest_votes WHERE thread_id = $1 AND user_id = $2', [threadId, userInfo.id])).rows[0]?.vote ?? 0);
};

export const postApiThread_id_vote: HtmlSegmentImpl = async ctx => {
    if (!F18_CONSTS.VOTE_CAMPAIGN_TAG_ID) { throw error(404, '当前无征文活动'); }

    const userInfo = ctx.state.mazoLoginUser;
    if (!userInfo) { throw error(403, LOGIN_REQUIRED); }
    if (userInfo.blocked) { throw error(403, LOGIN_REQUIRED); }
    const threadId = parseInt32(ctx.params.id);
    const thread = (await sql<{ thread_id: number, owner_user_id: number, forum_id: number }>('SELECT thread_id, owner_user_id, forum_id FROM thread_targets WHERE thread_id = $1 AND last_page_number > 0 AND successive_fail >= 0', [threadId])).rows[0];
    if (!thread) { throw error(404, '主题不存在！'); }
    if (thread.forum_id !== 18) { throw error(404, '主题不在小说文字区中！'); }

    let vote = ctx.request.body.integer('vote');
    const canVote = await canVoteForCampaign(userInfo.id);
    if (!isNaN(vote)) {
        if (!canVote) { throw error(400, '抱歉，你的用户太新，没有投票资格。'); }
        if (thread.owner_user_id === userInfo.id) { throw error(400, '不可以给自己的作品投票！'); }
        switch (vote) {
            case 2:
                await Promise.all([
                    sql(`UPDATE contest_votes SET vote = 1 WHERE thread_id <> $1 AND vote = 2 AND user_id = $2`, [threadId, userInfo.id]),
                    sql(`INSERT INTO contest_votes (thread_id, user_id, vote_at, vote) VALUES ($1, $2, $3, 2) ON CONFLICT (user_id, thread_id) DO UPDATE SET vote = EXCLUDED.vote, vote_at = EXCLUDED.vote_at`, [threadId, userInfo.id, ctx.refDate])
                ]);
                break;
            case 1:
                await sql(`INSERT INTO contest_votes (thread_id, user_id, vote_at, vote) VALUES ($1, $2, $3, 1) ON CONFLICT (user_id, thread_id) DO UPDATE SET vote = EXCLUDED.vote, vote_at = EXCLUDED.vote_at`, [threadId, userInfo.id, ctx.refDate]);
                break;
            case 0:
                await sql(`UPDATE contest_votes SET vote = 0, vote_at = $3 WHERE thread_id = $1 AND user_id = $2`, [threadId, userInfo.id, ctx.refDate]);
                break;
            default:
                throw error(400, '投票选项不合法！');
        }
    } else {
        vote = (await sql('SELECT vote FROM contest_votes WHERE thread_id = $1 AND user_id = $2', [threadId, userInfo.id])).rows[0]?.vote ?? 0;
    }

    const scores = ctx.request.body.integerArray('scores');
    const commentContent = ctx.request.body.string('content');
    if (commentContent || scores.length) {
        if (!(userInfo.rawFlags & 0x10)) { throw error(403, '你不是评委，没有打分权限。'); }
        const score = totalScoreAfterValidation(scores);
        await sql('INSERT INTO contest_panel_reviews (thread_id, user_id, content, score, review_at, extra_information) VALUES ($1, $2, $3, $4, $5, $6)', [threadId, userInfo.id, commentContent, score, ctx.refDate, score ? JSON.stringify(scores) : null]);
    }

    return renderCampaignVote(ctx, threadId, canVote, vote);
};

export const apiThread_id_votes: HtmlSegmentImpl = async ctx => {
    if (!F18_CONSTS.VOTE_CAMPAIGN_TAG_ID) { throw error(404, '当前无征文活动'); }

    const threadId = parseInt32(ctx.params.id);
    const userInfo = ctx.state.mazoLoginUser;
    if (!userInfo) { throw error(403, LOGIN_REQUIRED); }
    const votes = (await sql<{ user_id: number, user_name: string, vote: number }>('SELECT user_id, user_name, vote FROM contest_votes LEFT JOIN users USING (user_id) WHERE thread_id = $1 AND vote > 0 ORDER BY vote DESC, vote_at ASC', [threadId])).rows;
    return finalizeSegment(ctx, templates.campaignVotes(ctx, { votes }));
};

export const apiVotes: RpcImpl = async ctx => {
    if (!F18_CONSTS.VOTE_CAMPAIGN_TAG_ID) { throw error(404, '当前无征文活动'); }

    const counts: Record<string, [number?, number?, number?]> = {};
    const voteCounts = (await sql<{ thread_id: number, cnt: number, vote: number }>('SELECT thread_id, COUNT(user_id)::INT AS cnt, vote FROM contest_votes WHERE vote > 0 GROUP BY thread_id, vote')).rows;
    for (const vc of voteCounts) {
        if (!counts[vc.thread_id]) { counts[vc.thread_id] = []; }
        counts[vc.thread_id][vc.vote] = vc.cnt;
    }
    if (ctx.state.mazoLoginUser) {
        const myVotes = (await sql<{ thread_id: number, vote: number }>('SELECT thread_id, vote FROM contest_votes WHERE vote > 0 AND user_id = $1', [ctx.state.mazoLoginUser.id])).rows;
        for (const vc of myVotes) {
            if (!counts[vc.thread_id]) { counts[vc.thread_id] = []; }
            counts[vc.thread_id][0] = vc.vote;
        }
    }
    ctx.body = counts;
};
