import * as author from './author';
import * as hovercard from './hovercard';
import * as campaign from './campaign';
import * as collectionsPopup from './collections_popup';
import * as editor from './editor';
import * as favInList from './fav_in_list';
import * as favInThread from './fav_in_thread';
import * as iast from './iast';
import * as misc from './misc';
import * as notifications from './notifications';
import * as pm from './pm';
import * as readStatusDisplay from './read_status_display';
import * as tagsCampaign from './tags_campaign';
import * as tagsF18Initial from './tags_f18_initial';
import * as tagsInThreads from './tags_in_threads';
import * as tagsSettings from './tags_settings';

collectionsPopup.sideEffect();
iast.sideEffect();
notifications.sideEffect();

const global = window as any;

// Open a modal showing stats of a thread.
// Usages: thread.tsx (thread_id_page).
global['openThreadStats'] = misc.openThreadStats;

// Open a modal showing stats of a thread.
// Usages: trivia.tsx.
global['openTriviaStats'] = misc.openTriviaStats;

// Favorite-related features in a thread page.
// Usages: thread.tsx (thread_id_page).
global['initThreadFav'] = favInThread.initThreadFav;

// Thread management in a collection page.
// Usages: favorite.tsx (collection_id_page).
global['initCollectionPage'] = favInList.initCollectionPage;

// Show the vote counts of a thread in a thread page. Only used when there is a VOTE_CAMPAIGN_TAG_ID.
// TODO(chromaso): comment this out when there is no active campaign.
// Usages: thread.tsx (thread_id_page).
global['initThreadCampaign'] = campaign.initThreadCampaign;

// Show who voted for a thread. Only used when there is a VOTE_CAMPAIGN_TAG_ID.
// TODO(chromaso): comment this out when there is no active campaign.
// Usages: contest_vote.tsx (apiThread_id_vote, postApiThread_id_vote).
global['showContestVoteDetails'] = campaign.showContestVoteDetails;

// Initialize a PM conversation page.
// Usages: pm.tsx (pm_id, postPm_id).
global['loadMmPm'] = pm.loadMmPm;

// In a thread list page, for all threads, show read/unread status popups when clicked.
// Usages: forum.tsx (forum_id_page), thread_table.tsx (home), author.tsx (author_id).
global['initThreadListPage'] = readStatusDisplay.initThreadListPage;

// In new post editor page, show UI for campaign signup when there is a OPEN_CAMPAIGN_TAG_ID.
// TODO(chromaso): comment this out when there is no active campaign.
// Usages: post.tsx (postPost_parent).
global['initCampaignSignupInitial'] = tagsCampaign.initCampaignSignupInitial;

// In new post editor page, initialize the tag selector.
// Usages: post.tsx (postPost_parent).
global['postTagSelector'] = tagsF18Initial.postTagSelector;

// In a thread page, initialize tag management features.
// Usages: thread.tsx (thread_id_page).
global['initThreadTags'] = tagsInThreads.initThreadTags;

// Initialize the tag settings page.
// Usages: tag.tsx (tagSettings).
global['initTagSettingsPage'] = tagsSettings.initTagSettingsPage;

// In a editor, intialize BBCode editor (SCEditor).
// Usages: pm.tsx (pm_id, postPm_id), post.tsx (postPost_parent, postEdit_id).
global['initCodeEditor'] = editor.initCodeEditor;

// In the hidden (internal) page for review members, initialize the page.
// TODO(chromaso): maybe comment this out when there is no active campaign?
// Usages: content_backend.tsx (contestPanel).
global['contestRBPanelInit'] = campaign.contestRBPanelInit;

// In the author profile page, if the active user is the author, initialize editors.
// Usages: author.tsx (author).
global['initProfileEdit'] = author.initProfileEdit;

// In the author profile page, initialize pinned threads.
// Usages: author.tsx (author).
global['initPinnedThreads'] = author.initPinnedThreads;

// In the author profile page, if the active user is the author, manage pinned threads.
// Usages: author.tsx (author).
global['pinnedThreadsMgmt'] = author.pinnedThreadsMgmt;

// In the author profile page, load the list of achievements.
// Usages: author.tsx (author).
global['loadAchisInAuthor'] = author.loadAchisInAuthor;

// In the author profile page, load the list of qualification entries for a singel achievement.
// Usages: author.tsx (author).
global['showArcli'] = author.showArcLi;

// In the header, with some possiblity of trigger an third-party avatar check.
// Usages: globals.tsx (via utils/avatar.ts).
global['avatarChecker'] = author.avatarChecker;

// In the header, set up hovercard listeners
// Usages: globals.tsx.
global['initHovercard'] = hovercard.initHovercard;