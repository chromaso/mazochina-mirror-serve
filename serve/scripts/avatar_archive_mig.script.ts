import { promises as fs } from 'fs';

import { query as sql } from '../db';
import { error } from '../utils/render_utils';
import { allowedFormats } from '../shared/attachment';
import { fromRoot } from '../path';
import sharp = require('sharp');
import { imageToSignatureBuffer } from '../utils/avatar_signature';
import path = require('path');

const avatarDir = path.join('static', 'avatars');

const single = async (userId: number, file: Buffer, dateInS: number) => {
    const magicNumber = file.slice(0, 3).toString('hex');
    if (!(magicNumber in allowedFormats)) {
        throw error(400, 'Image format not allowed.');
    }

    const sharpStream = sharp(file);
    const meta = await sharpStream.metadata();
    if (!((meta.width! >= 240) && (meta.width! < 1600) && !(meta.width! & 1) && (meta.width === meta.height))) {
        throw error(400, `Invalid image dimension ${meta.width}x${meta.height}`);
    }

    const signature = await imageToSignatureBuffer(sharpStream.toColorspace('rgba'));
    const baseFileName = signature.toString('base64url');

    const id = (await sql<{ avatar_id: number }>('INSERT INTO user_avatars (user_id, img_signature, created_at, updated_at, flags) VALUES ($1, $2, $3, $3, $4) ON CONFLICT (user_id, img_signature) DO UPDATE SET updated_at = EXCLUDED.updated_at, flags = user_avatars.flags | EXCLUDED.flags RETURNING avatar_id', [userId, signature, dateInS, 0])).rows[0].avatar_id;

    await fs.writeFile(fromRoot(avatarDir, baseFileName), file);
    return id;
};

const files = async () => {
    const files = (await fs.readdir(fromRoot(avatarDir))).filter(f => /^\d+-24-11/.test(f));
    const allStats = await Promise.all(files.map(f => fs.stat(fromRoot(avatarDir, f))));
    const entries = allStats.map((v, i) => ({
        fn: files[i],
        date: v.mtimeMs
    })).sort((a, b) => a.date - b.date);
    for (const entry of entries) {
        const userId = parseInt(entry.fn.match(/^(\d+)-/)![1]);
        try {
            console.log(await single(userId, await fs.readFile(fromRoot(avatarDir, entry.fn)), Math.round(entry.date / 1000)));
        } catch (e) {
            console.error(e);
        }
    }
};

if (require.main === module) {
    files();
}