import { $, $set } from '../common-import';

export const multiTabModal = (containerUnderModalBody: HTMLElement, innerHTML: string, buttons: { _prop_id_in_html: string; _prop_label: string; }[], onTabSwitch?: (element: HTMLElement) => void, selectedTabIndex?: number) => {
    const wrap = $('div', { innerHTML });
    const children: { [name: string]: HTMLElement; } = {};

    const navButtons: HTMLElement[] = [];
    let activeNavButton: HTMLButtonElement | undefined;
    const onClickButton = (e: MouseEvent) => {
        const target = e.currentTarget as HTMLButtonElement;
        if (target === activeNavButton) { return; }
        if (activeNavButton) { activeNavButton.classList.remove('active'); }
        activeNavButton = target;
        target.classList.add('active');
        const element = children[target.name];
        $set(containerUnderModalBody, children[target.name]);
        onTabSwitch?.(element);
    };
    const addButton = (title: string, id: string) => {
        const body = wrap.querySelector('#' + id);
        if (!body) { return; }
        children[id] = body as HTMLElement;
        navButtons.push($('li', 'nav-item', $('button', { type: 'button', className: 'nav-link', name: id }, title, onClickButton)));
    };
    for (const button of buttons) {
        addButton(button._prop_label, button._prop_id_in_html);
    }

    // Warning: super hacky and nasty here.
    const modalBody = containerUnderModalBody.parentNode as HTMLElement;
    const modalHeader = modalBody.previousElementSibling!;
    modalHeader.classList.add('border-bottom-0');

    const tabs = $('ul', 'nav nav-tabs px-3', navButtons);
    modalBody.parentNode!.insertBefore(tabs, modalBody);
    navButtons[selectedTabIndex || 0].querySelector('button')!.click();
};
