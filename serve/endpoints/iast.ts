import { ESCAPED_IDENTIFIER_PATTERN, IastInput, legalChineseName, MAX_LENGTH_PER_INPUT, unescapeForIast } from '../../common/iast';
import { rowsToMap } from '../../common/logic_utils';
import { query as sql } from '../db';
import { escapeForIast } from '../shared/iast';
import { parseInt32 } from '../utils/logic_utils';
import { error, finalizeSegment, HtmlSegmentImpl, LOGIN_REQUIRED, RpcImpl } from '../utils/render_utils';

import * as templates from '../templates/iast';

export const apiIast_id_key: HtmlSegmentImpl = async ctx => {
    const threadId = parseInt32(ctx.params.id);
    if (isNaN(threadId)) { throw error(400, '不要乱输入网址嘛┭┮﹏┭┮'); }

    const key = ctx.params.key;
    if (!key || !ESCAPED_IDENTIFIER_PATTERN.test(key)) {
        throw error(400, '似乎没有这个问题…');
    }

    const typeQuery = (await sql<{ v: IastInput, post_id: number }, [string, number]>('SELECT * FROM (SELECT post_id, JSONB_EXTRACT_PATH(iast_inputs, $1) as v FROM iast_config WHERE thread_id = $2) AS vs WHERE v IS NOT NULL', [key, threadId])).rows;
    if (typeQuery.length !== 1) {
        throw error(404, '似乎没有这个问题…');
    }

    const inputInfo = typeQuery[0].v;
    if (inputInfo.priv) {
        const ownerLookup = (await sql<{ owner_user_id: number }>('SELECT owner_user_id FROM thread_targets WHERE thread_id = $1', [threadId])).rows[0];
        if (!ownerLookup) {
            throw error(404, '似乎没有这个主题…');
        }
        if (ownerLookup.owner_user_id !== ctx.state.mazoLoginUser?.id) {
            throw error(403, '作者禁止了查看统计数据！');
        }
    }
    if (inputInfo.type === 'cnm') {
        throw error(403, '姓名类型不提供统计数据！');
    }

    type Row = {
        escaped: string, count: number, display: string, percentage?: string
    };
    const rawRows = (await sql<{ cnt: number, ic: string }, [string, number]>('SELECT COUNT(user_id)::INT AS cnt, ic FROM iast_choices, JSONB_EXTRACT_PATH(iast_choices.choices, $1) AS ic WHERE thread_id = $2 AND ic IS NOT NULL GROUP BY ic ORDER BY cnt DESC', [key, threadId])).rows;
    const outRows: Row[] = [];
    if (typeof inputInfo.type === 'string') {
        const outputs = new Map<string, Row>();
        for (const row of rawRows) {
            const normalizedForm = escapeForIast(row.ic);
            const existing = outputs.get(normalizedForm);
            if (!existing) {
                outputs.set(normalizedForm, { escaped: normalizedForm, display: row.ic, count: row.cnt }); // Use the most-used variation as the display.
            } else {
                existing.count += row.cnt;
            }
        }
        let othersCnt = 0;
        outputs.forEach(v => {
            if (v.count < 3) {
                othersCnt += v.count;
            } else {
                outRows.push(v);
            }
        });
        outRows.sort((a, b) => b.count - a.count);
        if (othersCnt > 0) {
            outRows.push({ escaped: '', display: '其它*', count: othersCnt });
        }
    } else {
        // In this case we know the values stored are already escaped.
        const stats = rowsToMap(rawRows, 'ic');
        inputInfo.type.map(text => {
            outRows.push({ escaped: text, display: unescapeForIast(text), count: stats.get(text)?.cnt ?? 0 });
            stats.delete(text);
        });
        stats.forEach(v => outRows.push({ escaped: v.ic, display: unescapeForIast(v.ic), count: v.cnt }));
    }

    const max = Math.max(...outRows.map(_ => _.count));
    return finalizeSegment(ctx, templates.iastStats(ctx, {
        outputs: outRows.map(row => Object.assign(row, { percentage: `${Math.round(row.count / max * 1000) / 10}%` })),
        isSelect: typeof inputInfo.type !== 'string'
    }));
};

export const postApiIast_id: RpcImpl = async ctx => {
    const threadId = parseInt32(ctx.params.id);
    if (isNaN(threadId)) { throw error(400, '这个网址是错的。'); }
    const userInfo = ctx.state.mazoLoginUser;
    if (!userInfo) { throw error(403, LOGIN_REQUIRED); }

    const key = ctx.request.body.string('key');
    const rawValue = ctx.request.body.string('value');
    if (!key || !rawValue || !ESCAPED_IDENTIFIER_PATTERN.test(key)) {
        throw error(400, '你没有指定合法的问题或回答…');
    }

    const typeQuery = (await sql<{ v: IastInput }, [string, number]>('SELECT v FROM (SELECT JSONB_EXTRACT_PATH(iast_inputs, $1) as v FROM iast_config WHERE thread_id = $2) AS vs WHERE v IS NOT NULL', [key, threadId])).rows;
    if (typeQuery.length !== 1) {
        throw error(404, '似乎没有这个问题…');
    }

    const iastType = typeQuery[0].v;
    if (iastType.type === 'cnm') {
        try {
            legalChineseName(rawValue);
        } catch (e) {
            throw error(400, '这个姓名不行：' + (e as Error).message);
        }
    } else if (Array.isArray(iastType.type)) {
        // For array type, the posted value should already be the escaped one.
        if (!iastType.type.includes(rawValue)) {
            throw error(400, '这不是其中一个选项。');
        }
    } else { // type=text
        if (rawValue.length > MAX_LENGTH_PER_INPUT) {
            throw error(400, '你输入的回答太长啦～搞个短点的好不好…');
        }
    }

    await sql<{ choices: { [key: string]: string } }, [number, number, string, string, number]>(`INSERT INTO iast_choices (user_id, thread_id, choices, updated_at) VALUES ($1, $2, JSONB_BUILD_OBJECT($3::TEXT, $4::TEXT), $5) ON CONFLICT (user_id, thread_id) DO UPDATE SET choices = iast_choices.choices || EXCLUDED.choices, updated_at = EXCLUDED.updated_at`, [userInfo.id, threadId, key, rawValue, ctx.refDate]);

    ctx.body = Array.isArray(iastType.type) ? [rawValue] : [escapeForIast(rawValue), rawValue];
};
