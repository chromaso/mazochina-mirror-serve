import crypto = require('crypto');
import { query as sql } from '../db';
import { Middleware, ParameterizedContext } from 'koa';
import { RateLimiter } from '../utils/rate_limit';
import { COOKIE_PREFIX, hmacKeyEmail } from '../conf';
import { MazoKoaContext, MazoState } from '../utils/logic_utils';
import { cookieOptions } from './options';
import { unawaited } from '../../common/logic_utils';

export const ROOT_USER = 25646;

const adminUsers = new Set([
    ROOT_USER, // chromaso
    176607, // 士师志望
    1073741824 // Pompom
]);

const tokenFormat = /^[0-9a-f]{32}$/;

export type GuestInfo = {
    id: number; // Always negative.
    sessionId: number;
};

export type UserInfo = {
    id: number;
    name: string;
    email: string;
    avatarSmallInt: number;
    avatarSkipck: number;
    blocked: boolean;
    confirmed: boolean;
    sessionId: number;
    admin: boolean;
    registered: number;
    rawFlags: number;
    oldConfigs: bigint;
};

export const doLogin = async (userId: number, ctx: ParameterizedContext<MazoState>): Promise<number> => {
    const token = Buffer.alloc(16);
    const date = Date.now();
    token.writeBigUInt64BE(BigInt(date), 8); // Date at bytes 8 - 15, but really, only last 5 bytes matters as the first three are 00 00 01. [][][][][][][][][D][D][D][D!][D!][D!][D!][D!]. valid from 2004-11 to 2039-09.
    token.writeDoubleBE(Math.random(), 2); // Random at bytes 2 - 10. [][][R][R][R][R][R][R][R][R][R][D!][D!][D!][D!][D!]
    token.writeInt32LE(userId, 0); // User ID at beginning. [U][U][U][U][R][R][R][R][R][R][R][D!][D!][D!][D!][D!]
    const tokenString = token.toString('hex');
    const ips = ctx.ips ?? [];
    if (ips[0] !== ctx.ip) { ips.unshift(ctx.ip); }
    const oldSessionId = ctx.state.mazoLoginUser?.sessionId ?? ctx.state.mazoGuest?.sessionId ?? 0;
    const sessionId = (await sql<{ session_id: number }>('INSERT INTO user_auth (token, replacing, ua, ips, domain) VALUES (DECODE($1, \'hex\'), $2, $3, $4, $5) RETURNING session_id', [
        tokenString, -oldSessionId, ctx.headers['user-agent'] || '', ips, ctx.host
    ])).rows[0].session_id;
    if (ctx.state.mazoGuest && (userId > 0)) {
        await cleanUpGuestBrowse(ctx.state.mazoGuest, userId);
    }
    ctx.cookies.set(COOKIE_PREFIX + 'metro', tokenString, cookieOptions);
    return sessionId;
};

export const attachCurrentLogin: Middleware<MazoState> = async (ctx, next) => {
    do {
        const token = String(ctx.cookies.get(COOKIE_PREFIX + 'metro'));
        if (!tokenFormat.test(token)) { break; }
        const result = await sql<{ session_id: number, user_id: number, login_date: number, replacing: number }>(`SELECT session_id, user_auth_token_user_id(token) AS user_id, (user_auth_token_date_ms(token) / 1000)::INT AS login_date, replacing FROM user_auth WHERE token = DECODE($1, \'hex\') AND session_id > 0`, [token]);
        const session = result.rows[0];
        if (!session) { break; }

        if (session.replacing < 0) {
            // The first actual use of a new session token (that was issued to replace the old one). Now we know the new token is stored in browser, we can kill the previous session.
            unawaited(Promise.all([
                sql('UPDATE user_auth SET replacing = ABS(replacing) WHERE session_id = $1', [session.session_id]),
                sql('UPDATE user_auth SET session_id = -ABS(session_id) WHERE session_id = $1', [-session.replacing])
            ]), `actually voiding session ${-session.replacing}`);
        }

        let forceReplaceSession: number | undefined;
        if (session.user_id < 0) { // A visitor.
            ctx.state.mazoGuest = {
                id: session.user_id,
                sessionId: session.session_id
            };
        } else {
            let userRow = (await sql('SELECT user_id, user_name, flags, email, registered_at, configs, avatar, avatar_skipck FROM users WHERE user_id = $1', [session.user_id])).rows[0];
            if (!userRow) {
                const tombstone = (await sql<{ new_user_id: number; }>('SELECT new_user_id FROM user_tombstone WHERE prev_user_id = $1', [session.user_id])).rows[0];
                if (!tombstone) { console.error(`User row ${session.user_id} not found.`); break; }
                userRow = (await sql('SELECT user_id, user_name, flags, email, registered_at, configs, avatar, avatar_skipck FROM users WHERE user_id = $1', [tombstone.new_user_id])).rows[0];
                forceReplaceSession = userRow.user_id;
            }
            const blocked = !!(userRow.flags & 2);
            ctx.state.mazoLoginUser = {
                id: userRow.user_id,
                name: userRow.user_name,
                email: userRow.email,
                avatarSmallInt: userRow.avatar,
                avatarSkipck: userRow.avatar_skipck,
                blocked,
                confirmed: (!!(userRow.flags & 4)),
                registered: userRow.registered_at,
                rawFlags: userRow.flags,
                sessionId: session.session_id,
                admin: adminUsers.has(userRow.user_id),
                oldConfigs: userRow.configs
            } as UserInfo;
        }

        const now = (Date.now() / 1000) | 0;
        const oldness = Math.min(Math.max((now - session.login_date - 17280000) / 17280000, 0), 1) / 4; // 0 if < 200 days old; linear increase to 0.25 when 400 days old. This is the initial setting. It will go down.
        const maybeRenewSession = Math.random() < oldness;
        if ((forceReplaceSession || maybeRenewSession) && !ctx.path.includes('/api/')) {
            // Must be called after `mazoLoginUser` was set.
            console.log(`Replacing ${forceReplaceSession ? 'tombstone' : 'old'} session ${session.session_id}.`);
            await doLogin(forceReplaceSession || session.user_id, ctx);
        }
    } while (false);

    await next();
};

export const generateHmacToken = (key: string, dateInMs: number, nonce: string): string => {
    const hmac = crypto.createHmac('sha1', hmacKeyEmail);
    hmac.update(key);
    hmac.update(nonce);
    hmac.update(dateInMs.toString(36));
    return hmac.digest('hex').substr(8, 20);
};

export const cleanUpGuestBrowse = async (visitor: GuestInfo, userId: number) => {
    await sql('INSERT INTO unread_status (user_id, thread_id, up_to_date, tracked_at) SELECT $1, thread_id, up_to_date, tracked_at FROM unread_status WHERE user_id = $2 ON CONFLICT (user_id, thread_id) DO UPDATE SET up_to_date = GREATEST(unread_status.up_to_date, EXCLUDED.up_to_date), tracked_at = GREATEST(unread_status.tracked_at, EXCLUDED.tracked_at)', [userId, visitor.id]);
    await sql('DELETE FROM unread_status WHERE user_id = $1', [visitor.id]);
};

export const createVisitorId = async (ctx: MazoKoaContext): Promise<number> => {
    if (ctx.state.mazoGuest) { return ctx.state.mazoGuest.id; }
    const ua = (ctx.headers['user-agent'] || '').toLowerCase();
    if (!ua) { return 0; }
    if (
        (ua.includes('http://') || ua.includes('https://')) &&
        (ua.includes('crawl') || ua.includes('bot') || ua.includes('spider'))
    ) {
        return 0;
    }
    const id = 0 - (await sql('SELECT nextval(\'visitor_id\')::INT AS vid')).rows[0].vid;
    ctx.state.mazoGuest = { id, sessionId: (await doLogin(id, ctx)) };
    return id;
};

export const formatVisitorId = (id: number | undefined) => {
    if (!id) { return '新访客'; }
    const buffer = Buffer.alloc(4);
    buffer.writeInt32BE(id ^ 0x8a30d591);
    return '访客-' + buffer.toString('hex');
};

export const loginAttemptCounter = new RateLimiter<string>(10, 1800);
export const registrationAttemptCounterByIp = new RateLimiter<string>(5, 1800);
export const updateInfoAttemptCounterByUser = new RateLimiter<number>(2, 3600);
