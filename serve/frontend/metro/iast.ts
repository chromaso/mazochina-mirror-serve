import { $xhr, getThreadIdFromUri } from '../common-import';

export const sideEffect = () => {
    window.postIast = (inputId, value, callback) => {
        const onError = (error: string): void => alert('没能成功保存你的选择：' + error);
        const threadId = getThreadIdFromUri();
        if (!threadId) {
            onError('当前页面未知'); return;
        }
        $xhr('/api/iast/' + threadId, {
            f: 'key=' + encodeURIComponent(inputId) + '&value=' + encodeURIComponent(value),
            e: onError,
            j: callback
        });
    };    
};