const fetch = require('node-fetch');
const http = require('http');
const { JSDOM } = require('jsdom');
const { read } = require('read-last-lines');
const { UA, SS_COOKIES } = require('../crawl/conf');

const LOG_FILE_PREFIX = '/var/www/.pm2/logs/';

const makeLoginAttempt = async (username, password) => {
    if (!username) { throw new Error('用户名未提供'); }
    if (!password) { throw new Error('密码未提供'); }
    const params = new URLSearchParams();
    params.append('username', username);
    params.append('password', password);
    params.append('viewonline', 'on');
    params.append('redirect', 'foobar.php');
    params.append('login', 'Login');
    const response = await fetch('http://mazochina.com/forum/ucp.php?mode=login', {
        method: 'POST',
        body: params,
        redirect: 'manual',
        headers: {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36'
        }
    });
    if (response.status === 302) {
        const setCookies = response.headers.raw()['set-cookie'];
        if (setCookies.length < 2) {
            throw new Error('Cookie 错误，建议换用另一种验证方式');
        }
        const uids = setCookies.map(item => item.match(/^phpbb3_[a-z0-9]{2,8}_u=(\d+);/)).filter(_ => _);
        const sids = setCookies.map(item => item.match(/^phpbb3_[a-z0-9]{2,8}_sid=([a-z0-9]{32});/)).filter(_ => _);
        if (!uids.length || !sids.length) {
            throw new Error('Cookie 错误，建议换用另一种验证方式');
        }
        return {
            uid: parseInt(uids.pop()[1]), sid: sids.pop()[1]
        };
    } else if ((response.headers.get('content-type') || '').startsWith('text/html')) {
        let errorText;
        try {
            const responseText = await response.text();
            const dom = new JSDOM(responseText);
            const errorEl = dom.window.document.querySelector('div.error');
            errorText = errorEl.textContent || 'Login failed';
        } catch (e) {
            errorText = `正文读取失败`;
        }
        throw new Error('验证失败：' + errorText);
    } else {
        throw new Error(`HTTP 错误 ${response.status}: ${response.statusText}，建议择日重试或换用另一种验证方式`);
    }
};

const attemptVerification = async (username, password) => {
    if (!username) { throw new Error('用户名未提供'); }
    const response = await fetch('http://mazochina.com/forum/memberlist.php?mode=viewprofile&un=' + encodeURIComponent(username), {
        method: 'GET',
        headers: {
            'User-Agent': UA,
            'Cookie': SS_COOKIES
        }
    });
    const text = await response.text();
    if (!response.ok || !text.includes('Viewing profile')) {
        throw Error('无法读取源站页面，建议稍后重试');
    }
    const canonicalMatch = text.match(/<link rel="canonical" href="http:\/\/mazochina\.com\/forum\/memberlist\.php\?mode=viewprofile&amp;u=(\d+)"/);
    if (!canonicalMatch) {
        console.log(text);
        throw Error('读取到的源站页面是乱码，请确认用户名输入无误并稍后重试');
    }
    const id = parseInt(canonicalMatch[1]);
    const code = text.match(/bmv[b-ks-x]{18}uq/);
    if (!code) {
        throw Error('未从源站个人页面中找到验证码');
    }
    return {
        id, code: code[0]
    };
};

const handleLogRequest = fileName => {
    if (!/^\w+-\w+$/.test(fileName)) {
        throw Error('文件名不合法');
    }
    return read(LOG_FILE_PREFIX + fileName + '.log', 250);
};

http.createServer((request, response) => {
    try {
        const requestUrl = new URL(request.url, `http://${request.headers.host}`);
        const method = request.method.toUpperCase();
        if ((method === 'GET') && requestUrl.pathname.endsWith('/pap-log')) {
            handleLogRequest(requestUrl.searchParams.get('file') || '').then(result => {
                response.writeHead(200, { 'Content-Type': 'text/plain' });
                response.end(result);
            }).catch(error => {
                response.writeHead(500);
                response.end(error.message);
            });
            return;
        }
        if (method !== 'POST') {
            response.writeHead(405); response.end(); return;
        }
        let handler;
        if (requestUrl.pathname.endsWith('/pap-login')) {
            handler = makeLoginAttempt;
        } else if (requestUrl.pathname.endsWith('/pap-profile')) {
            handler = attemptVerification;
        } else {
            response.writeHead(404); response.end(); return;
        }
        handler(requestUrl.searchParams.get('username'), requestUrl.searchParams.get('password')).then(result => {
            response.writeHead(200, { 'Content-Type': 'application/json' });
            response.end(JSON.stringify({ result }));
            console.log('OK w/ ' + request.url);
        }).catch(error => {
            response.writeHead(200, { 'Content-Type': 'application/json' });
            response.end(JSON.stringify({ error: error.message }));
            console.error(error, request.url);
        });
    } catch (e) {
        response.writeHead(500);
        response.end();
    }
}).listen(8989, '10.124.0.4');
