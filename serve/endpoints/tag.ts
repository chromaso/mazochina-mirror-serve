import { rowsToMap } from '../../common/logic_utils';
import { query as sql } from '../db';
import { allDescedentTags, allTagsByCategory, allTagsById, F18_CONSTS, TagLite, tagPref, tagPrefUpdateFlags, tagPrefUpdateWarn, voteForThread } from '../shared/tags';
import { renderThreads, THREAD_RENDER_COLS } from '../shared/thread_table';
import { normalize, parseInt32 } from '../utils/logic_utils';
import { error, finalizePage, finalizeSegment, HtmlSegmentImpl, LOGIN_REQUIRED, PageImpl, redirectWithDelay, redirectWithHttpPost, RpcImpl } from '../utils/render_utils';

import * as templates from '../templates/tag';

export const tags: PageImpl = async ctx =>
    finalizePage(ctx, {
        title: ctx.state.render.staticConv('标签列表'),
        canonicalPath: '/tags',
        nav: true
    }, templates.tags(ctx, {
        f18Tags: await allTagsByCategory(),
        warns: (await tagPref(ctx)).warning,
        grandCounts: rowsToMap((await sql<{ tag_id: number, cnt: number }>(`SELECT tag_id, COUNT(thread_id)::INT AS cnt FROM thread_targets INNER JOIN tag_association USING (thread_id) WHERE forum_id = 18 AND last_page_number > 0 AND successive_fail >= 0 AND tag_score > ${F18_CONSTS.CONSTS.SCORE_MULTIPLIER} GROUP BY tag_id`)).rows, 'tag_id')
    }));

export const tag_id: PageImpl = async ctx => {
    const [conf, tags] = await Promise.all([tagPref(ctx), allTagsById()]);
    const tag = tags.get(parseInt32(ctx.params.id));
    if (!tag) { throw error(404, '没有这个标签哦！'); }
    const tagTree = Array.from(allDescedentTags(tag));
    const isActiveCampaign = tag.tag_id === F18_CONSTS.VOTE_CAMPAIGN_TAG_ID;
    const threadRows = (await sql(`SELECT ${THREAD_RENDER_COLS.join(', ')} FROM thread_targets WHERE forum_id = 18 AND last_page_number > 0 AND successive_fail >= 0 AND EXISTS (SELECT tagass_id FROM tag_association WHERE tag_association.thread_id = thread_targets.thread_id AND tag_id = ANY($1::INT[]) AND tag_score > ${F18_CONSTS.CONSTS.SCORE_MULTIPLIER}) ORDER BY thread_id DESC LIMIT $2`, [tagTree, isActiveCampaign ? 0x7fff : 20])).rows;
    const threadCount = (await sql(`SELECT COUNT(thread_id)::INT AS cnt FROM thread_targets WHERE forum_id = 18 AND last_page_number > 0 AND successive_fail >= 0 AND EXISTS (SELECT tagass_id FROM tag_association WHERE tag_association.thread_id = thread_targets.thread_id AND tag_id = ANY($1::INT[]) AND tag_score > ${F18_CONSTS.CONSTS.SCORE_MULTIPLIER})`, [tagTree])).rows[0].cnt;
    return finalizePage(ctx, {
        title: `标签 ${ctx.state.render.staticConv(tag.primary_name)}`,
        nav: true,
        canonicalPath: '/tag/' + tag.tag_id
    }, templates.tag(ctx, {
        tag,
        warn: conf.warning.has(tag.tag_id),
        threads: await renderThreads(ctx, threadRows, conf, ctx.state.mazoLoginUser?.id),
        threadCount
    }));
};

export const postTag_id: PageImpl = async (ctx) => {
    if (!ctx.state.mazoLoginUser) { throw error(403, LOGIN_REQUIRED); }

    const tags = await allTagsById();
    const tagId = parseInt32(ctx.params.id);
    const tag = tags.get(tagId);
    if (!tag) { throw error(404, '没有这个标签哦！'); }
    const suggestion = ctx.request.body.string('suggestion');
    if (!(suggestion.length > 2)) { throw error(400, '填写得也太短了点吧！'); }

    const { staticConv } = ctx.state.render;
    return redirectWithHttpPost(ctx, `/post/t${F18_CONSTS.CONSTS.README_THREAD_ID}`, {
        title: staticConv(`对标签「${tag.primary_name}」的修改建议`),
        content: `标签：[url=/tag/${tag.tag_id}]${staticConv(tag.primary_name)}[/url]` + '\n\n' + suggestion,
        submitpost: 'auto'
    });
};

export const postThread_id_tag: PageImpl = async (ctx) => {
    if (!ctx.state.mazoLoginUser) { throw error(403, LOGIN_REQUIRED); }

    const threadId = parseInt32(ctx.params.id);
    const thread = await sql<{ forum_id: Number; thread_title: string; }>('SELECT forum_id, thread_title FROM thread_targets WHERE thread_id = $1', [threadId]);
    const threadRow = thread.rows[0];
    if (!threadRow) { throw error(404, '无该主题！'); }
    if (threadRow.forum_id !== 18) { throw error(404, '该主题不在小说文字区！'); }

    const name = ctx.request.body.string('name');
    if (name.length < 1) { throw error(400, '你未填写标签名称！'); }
    if (name.length > 10) { throw error(400, '标签的主要名称过长啦！不要超过十个字。'); }
    const normalizedName = normalize(name);
    const duplicate = Array.from((await allTagsById()).values()).find(tag => (normalize(tag.primary_name) === normalizedName) || tag.aliases?.some(alias => normalize(alias) === normalizedName)
    );
    if (duplicate) {
        return redirectWithDelay(ctx, {
            message: `标签「${name}」已经存在`,
            targetHref: '/tag/' + duplicate.tag_id,
            targetText: '已存在的标签'
        });
    }

    const cat = ctx.request.body.integer('cat');
    const title = F18_CONSTS.CAT_TITLES[cat];
    if (!title) {
        throw error(400, '你未选择标签的类别！');
    }

    const suggestion = ctx.request.body.string('suggestion');

    const { staticConv, contentConv } = ctx.state.render;
    const safeThreadTitle = contentConv(threadRow.thread_title).replace(/\[/g, '［').replace(/]/g, '］');
    return redirectWithHttpPost(ctx, `/post/t${F18_CONSTS.CONSTS.README_THREAD_ID}`, {
        title: staticConv(`建议添加「${name}」标签`),
        content: staticConv(`提议在「${title}」分类下添加「${name}」标签，以适用于主题`) + ` [url=/thread/${threadId}]${safeThreadTitle}[/url]` + (suggestion ? '：\n\n' + suggestion : ''),
        submitpost: 'auto'
    });
};

export const apiTags: RpcImpl = async (ctx) => {
    ctx.set('Cache-Control', `public, max-age=${F18_CONSTS.CONSTS.CACHE_PERIOD / 1000}`);
    const { staticConv } = ctx.state.render;
    const filterFunction: (tag: TagLite) => boolean = ctx.state.mazoLoginUser?.admin ? (_ => true) : (tag => (F18_CONSTS.OPEN_CAMPAIGN_TAG_ID === tag.tag_id) || !(tag.flags & 0x4));
    ctx.body = Array.from((await allTagsById()).values()).filter(filterFunction).map(
        tag => [tag.tag_id, (tag.aliases ? [tag.primary_name, ...tag.aliases] : [tag.primary_name]).map(staticConv), (tag.category << 3) | (tag.flags & 0x7), staticConv(tag.desc_html), tag.childTags?.map(tag => tag.tag_id)]
    );
};

export const tagsSettings: PageImpl = async ctx => {
    const userInfo = ctx.state.mazoLoginUser;
    if (!userInfo) { throw error(403, LOGIN_REQUIRED); }
    const [conf, tags] = await Promise.all([tagPref(ctx), allTagsById()]);

    return finalizePage(ctx, {
        title: ctx.state.render.staticConv('标签设置'),
        nav: true
    }, templates.tagSettings(ctx, {
        tagsOn: conf.visible,
        style: conf.style,
        catOrder: conf.catOrder,
        nowrap: conf.nowrap,
        limit: conf.limit,
        warnings: Array.from(conf.warning).map(warn => tags.get(warn)!)
    }));
};

export const postApiTagsSettings: RpcImpl = async (ctx) => {
    const userInfo = ctx.state.mazoLoginUser;
    if (!userInfo) { throw error(403, LOGIN_REQUIRED); }

    const responses: string[] = [];

    const feature = ctx.request.body.integer('feature');
    const style = ctx.request.body.integer('style');
    const order = ctx.request.body.integer('order');
    const limit = ctx.request.body.integer('limit');
    const nowrap = ctx.request.body.integer('nowrap');
    const warn = ctx.request.body.integer('warn');

    if (feature) {
        const on = feature > 0;
        await tagPrefUpdateFlags((1 << 0), on ? 0x1 : 0x0, userInfo.id);
        responses.push(`已${on ? '开启' : '关闭'}在主题列表页显示标签`);
    }
    if (style && F18_CONSTS.CONSTS.STYLES.includes(style as any)) {
        await tagPrefUpdateFlags((1 << 1) | (1 << 2) | (1 << 3), style << 1, userInfo.id);
        responses.push(`已保存显示样式`);
    }
    if (order && (order >= 1) && (order <= 3)) {
        await tagPrefUpdateFlags((1 << 4) | (1 << 5), order << 4, userInfo.id);
        responses.push(`已保存显示顺序`);
    }
    if (limit && (limit >= 1) && (limit <= 3)) {
        await tagPrefUpdateFlags((1 << 9) | (1 << 10), limit << 9, userInfo.id);
        responses.push(`已保存显示数量`);
    }
    if (nowrap) {
        const on = nowrap > 0;
        await tagPrefUpdateFlags((1 << 8), on ? 0x1 << 8 : 0x0, userInfo.id);
        responses.push(`已${on ? '开启' : '完全关闭'}单行显示`);
    }
    if (warn) { // Non-zero and non-NaN.
        const tagId = Math.abs(warn);
        const allTags = await allTagsById();
        const tag = allTags.get(tagId);
        if (!tag) { throw error(404, '标签不存在！'); }
        const toWarn = tagId === warn;
        if (!(await tagPrefUpdateWarn(userInfo.id, tagId, toWarn))) {
            throw error(412, '你尚未启用标签功能！');
        }
        responses.push(`标签「${tag.primary_name}」已设置为${toWarn ? '敏感' : '非敏感'}`);
        const descendants = allDescedentTags(tag);
        if (descendants.size > 1) {
            const updatedSettings = await tagPref(ctx);
            const warns = Array.from(descendants).filter(tagId => updatedSettings.warning.has(tagId) !== toWarn).map(tagId => `「${allTags.get(tagId)!.primary_name}」`);
            if (warns.length) {
                responses.push(`请注意子孙标签${warns.join('、')}并未自动应用同样的设置`);
            }
        }
    }

    if (!responses.length) { throw error(400, '未指定操作！'); }
    ctx.body = responses.join('，') + '。';
};

export const apiThread_id_tags: HtmlSegmentImpl = async ctx => {
    const userInfo = ctx.state.mazoLoginUser;
    if (!userInfo) { throw error(403, LOGIN_REQUIRED); }

    const threadId = parseInt32(ctx.params.id);
    if (!threadId) { throw error(400, '未指定主题！'); }

    const [thread, allTagVotes, allTags] = await Promise.all([
        sql<{ forum_id: Number, owner_user_id: number, thread_title: string }>('SELECT forum_id, owner_user_id, thread_title FROM thread_targets WHERE thread_id = $1', [threadId]),
        // TODO: those rows are not needed when forum_id is not 18. Maybe remove them for better performance?
        sql<{ tag_id: number, score: number, votes: [number, number][] }>('SELECT tag_id, tag_score & ~3 AS score, ARRAY(SELECT ARRAY[user_id, vote] FROM tag_votes WHERE tag_votes.tagass_id = tag_association.tagass_id ORDER BY voted_at ASC) AS votes FROM tag_association WHERE thread_id = $1 ORDER BY tag_score DESC', [threadId]),
        allTagsById()
    ]);

    const threadRow = thread.rows[0];
    if (!threadRow) { throw error(404, '无该主题！'); }
    if (threadRow.forum_id !== 18) { throw error(404, '该主题不在小说文字区！'); }

    const voteUserIds = new Set<number>();
    const tagSelections = new Map<number, { full: boolean, some: boolean, voted: boolean }>(); // True if full, false if some, nonexistent otherwise.
    const tags = allTagVotes.rows.map(row => {
        tagSelections.set(row.tag_id, {
            full: row.score > F18_CONSTS.CONSTS.SCORE_MULTIPLIER,
            some: row.score > 0,
            voted: !!(row.votes.find(([userId]) => userId === userInfo.id)?.[1]) // Voted and nonzero.
        });
        const normalizedScore = row.score / F18_CONSTS.CONSTS.SCORE_MULTIPLIER;
        for (const [userId] of row.votes) {
            voteUserIds.add(userId);
        }
        return {
            tag: allTags.get(row.tag_id)!,
            score: normalizedScore,
            owner: row.votes.find(([userId]) => threadRow.owner_user_id === userId)?.[1] ?? 0,
            votes: row.votes // [userId, vote]
        };
    });

    return finalizeSegment(ctx, templates.threadTags(ctx, {
        users: rowsToMap((await sql<{ user_id: number, user_name: string }>('SELECT user_id, user_name FROM users WHERE user_id = ANY($1::INT[])', [Array.from(voteUserIds)])).rows, 'user_id'),
        tags,
        f18Tags: await allTagsByCategory(),
        tagSelections,
        thread: { id: threadId, title: threadRow.thread_title }
    }));
};

export const apiThread_id_tag: HtmlSegmentImpl = async ctx => {
    const threadId = parseInt32(ctx.params.id);
    const tagId = parseInt32(ctx.params.tagId);
    if (!threadId || !tagId) { throw error(400, '未指定对象！'); }
    // Not checking thread existence here. If caller specifies a bogus ID, fool on them.
    const tag = (await allTagsById()).get(tagId);
    if (!tag) { throw error(404, '标签不存在！'); }

    const tagass = (await sql<{ tagass_id: number, tag_score: number }>('SELECT tagass_id, tag_score FROM tag_association WHERE thread_id = $1 AND tag_id = $2', [threadId, tagId])).rows[0];
    const voteCounts = tagass ? rowsToMap((await sql<{ vd: number, cnt: number }>('SELECT SIGN(vote) AS vd, COUNT(user_id)::INT AS cnt FROM tag_votes WHERE tagass_id = $1 GROUP BY vd', [tagass.tagass_id])).rows, 'vd') : new Map<number, { vd: number, cnt: number }>();

    const loginUser = ctx.state.mazoLoginUser;
    const myVote = (loginUser && tagass) ? ((await sql('SELECT vote FROM tag_votes WHERE tagass_id = $1 AND user_id = $2', [tagass.tagass_id, loginUser.id])).rows[0]?.vote ?? 0) : 0;

    return finalizeSegment(ctx, templates.threadTag(ctx, {
        tag,
        owner: tagass ? !!(tagass.tag_score & 0x1) : false,
        upvotes: voteCounts.get(1)?.cnt ?? 0,
        downvotes: voteCounts.get(-1)?.cnt ?? 0,
        myVote
    }));
};

export const postApiThread_id_tag: RpcImpl = async ctx => {
    const userInfo = ctx.state.mazoLoginUser;
    if (!userInfo) { throw error(403, LOGIN_REQUIRED); }
    const conf = await tagPref(ctx);

    const threadId = parseInt32(ctx.params.id);
    const tagId = parseInt32(ctx.params.tagId);
    if (!threadId || !tagId) { throw error(400, '未指定对象！'); }
    const vote = ctx.request.body.integer('vote');
    if (!([-1, 0, 1].includes(vote))) { throw error(400, '未做出投票！'); }

    const tag = (await allTagsById()).get(tagId);
    if (!tag) { throw error(404, '标签不存在！'); }
    if (tag.flags & 0x2) { throw error(400, '标签不可选！'); }
    const thread = (await sql<{ thread_id: number, owner_user_id: number, forum_id: number }>('SELECT thread_id, owner_user_id, forum_id FROM thread_targets WHERE thread_id = $1 AND last_page_number > 0 AND successive_fail >= 0', [threadId])).rows[0];
    if (!thread) { throw error(404, '主题不存在！'); }
    if (thread.forum_id !== 18) { throw error(404, '主题不在小说文字区中！'); }
    if (tag.flags & 0x4) {
        if (!(userInfo.admin || ((tagId === F18_CONSTS.OPEN_CAMPAIGN_TAG_ID) && (thread.owner_user_id === userInfo.id)))) {
            throw error(400, '只有管理员/作者本人可设置此标签！');
        }
    }

    const tagass = await voteForThread(thread, tagId, userInfo, ctx.refDate, vote as (-1 | 0 | 1), !!(tag.flags & 0x4));
    ctx.body = {
        full: tagass.tag_score > F18_CONSTS.CONSTS.SCORE_MULTIPLIER,
        some: tagass.tag_score > 0,
        warn: conf.warning.has(tagId),
        name: tag.primary_name
    };
};