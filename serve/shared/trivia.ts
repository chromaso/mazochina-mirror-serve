import { SPAM_THRESHOLD } from '../../common/consts';
import { rowsToMap } from '../../common/logic_utils';
import { query } from '../db';

type UserEntry = { user_id: number; user_name: string; label: string | null, cnt: number; };

export type TriviaStats = {
    postRank: UserEntry[][]; // Weekly thread, weekly post, monthly thread, monthly post.
    achivements: { user_id: number; user_name: string; name_sc: string; lvl: number; }[];
    threads: { thread_id: number, thread_title: string, yrs: number }[];
}

let triviaStats: TriviaStats | undefined;

const SEC_PER_YEAR = 31556952;

export const computeTrivia = async () => {
    const refDate = (Date.now() / 1000) | 0;
    const dateGroups = [refDate - 7 * 86400, refDate - 30 * 86400];
    const rawData = (await Promise.all(dateGroups.flatMap(dateThreshold => [
        query<{ user_id: number, cnt: number }>(`SELECT owner_user_id AS user_id, COUNT(thread_id)::INT AS cnt FROM thread_targets WHERE last_page_number > 0 AND successive_fail >= 0 AND spam_likeness < ${SPAM_THRESHOLD} AND firstpost_date > $1 GROUP BY user_id ORDER BY cnt DESC LIMIT 15`, [dateThreshold]),
        query<{ user_id: number, cnt: number }>(`SELECT user_id, COUNT(post_id)::INT AS cnt FROM posts LEFT JOIN thread_targets USING (thread_id) WHERE posts.most_recent_update >= 0 AND posts.posted_date > $1 AND thread_targets.last_page_number > 0 AND thread_targets.successive_fail >= 0 AND thread_targets.spam_likeness < ${SPAM_THRESHOLD} AND ((thread_targets.owner_user_id = posts.user_id) OR NOT EXISTS (SELECT * FROM posts p_ref WHERE p_ref.thread_id = thread_targets.thread_id AND p_ref.flags & 2 <> 0)) GROUP BY user_id ORDER BY cnt DESC LIMIT 15`, [dateThreshold])
    ]))).map(qr => qr.rows);
    const achivementGranted = await query<{ user_id: number, name_sc: string, lvl: number }>('SELECT user_id, name_sc, lvl FROM user_achievements ORDER BY granted DESC LIMIT 3');
    const anniversaryThreads = await query(`SELECT thread_id, thread_title, ($1 - firstpost_date) / ${SEC_PER_YEAR} AS yrs FROM thread_targets WHERE post_count > 5 AND thread_targets.last_page_number > 0 AND thread_targets.successive_fail >= 0 AND thread_targets.spam_likeness < ${SPAM_THRESHOLD} AND firstpost_date < $1 - ${SEC_PER_YEAR / 2} AND MOD($1 - firstpost_date, ${SEC_PER_YEAR}) < 86400 ORDER BY (post_count + firstpost_date / ${SEC_PER_YEAR / 8}) DESC LIMIT 3`, [refDate + 1800]); // Prefer newer threads by effective -8 post_count every year.
    const userIds = new Set(rawData.flatMap(ranking => ranking.map(row => row.user_id)));
    for (const row of achivementGranted.rows) { userIds.add(row.user_id); }
    const userInfo = rowsToMap((await query<{ user_id: number, user_name: string, label: string | null }>('SELECT user_id, user_name, label FROM users WHERE user_id = ANY($1::INT[])', [Array.from(userIds)])).rows, 'user_id');
    triviaStats = {
        postRank: rawData.map(group => group.map(single => Object.assign(single, userInfo.get(single.user_id)))),
        achivements: achivementGranted.rows.map(single => Object.assign(single, userInfo.get(single.user_id))),
        threads: anniversaryThreads.rows
    };
};

export const getTrivia = (): TriviaStats | undefined => triviaStats;