// Verifies the following properties of tags are observed:
// - ☑ No cyclic taxonomy structure.
// - ☑ No duplicate names/alias.
// - ☑ Names/alias contains certain characters only.
// - ☑ All non-selectable tags must have children.
//
// Additionally:
// - All existing tagasses has a valid tag.

import { rowsToMap, rowsToMultiMap } from '../../common/logic_utils';
import { query } from '../db';
import { CONV_SC as CONV_SC } from '../chinese';

const main = async (full: boolean) => {
    const tags = rowsToMap((await query<{ tag_id: number, primary_name: string, aliases: string[] | null, category: number, desc_html: string, flags: number }>('SELECT tag_id, primary_name, aliases, category, desc_html, flags FROM tags')).rows, 'tag_id');
    const relationships = rowsToMultiMap((await query<{ parent_tag: number, child_tag: number }>('SELECT parent_tag, child_tag FROM tags_taxonomy')).rows, 'parent_tag');

    const names = new Set<string>();
    const validateName = (name: string, id: number) => {
        if (!/^([A-Za-z0-9\-\/]|[\u4E00-\u9FA5]|[\u3400-\u4DB5])+$/.test(name)) { throw new Error(`非法标签名（${id}）：${name}`); }
        if (CONV_SC(name) !== name) { throw new Error(`非简体标签名（${id}）：${name}`); }
        const normalized = name.toLowerCase();
        if (names.has(normalized)) {
            throw new Error(`重复标签名（${id}）：${name}`);
        }
        names.add(normalized);
    };

    for (const tag of tags.values()) {
        validateName(tag.primary_name, tag.tag_id);
        tag.aliases?.forEach(alias => validateName(alias, tag.tag_id));

        if (tag.flags & 0x2) { // Non-selectable.
            if (!relationships.get(tag.tag_id)) {
                throw new Error(`标签不可选（${tag.tag_id}）：${tag.primary_name}`); 
            }
        }
        const goDescedents = (descedentChain: number[]) => {
            if ((descedentChain.length > 1) && (descedentChain[0] === tag.tag_id)) { throw new Error(`环状关系（${tag.tag_id}）：${descedentChain.join(' > ')}`); }
            const children = relationships.get(descedentChain[0]);
            children?.forEach(child => {
                goDescedents([child.child_tag, ...descedentChain]);
            });
        };
        goDescedents([tag.tag_id]);
    }

    console.log(`Validated ${tags.size} tags, ${names.size} names, ${relationships.size} of tags with children.`);

    if (full) {
        const orphans = (await query('SELECT tag_id, COUNT(thread_id)::INT AS cnt FROM tag_association WHERE NOT EXISTS (SELECT primary_name FROM tags WHERE tags.tag_id = tag_association.tag_id) GROUP BY tag_id')).rows;
        if (orphans.length) {
            throw new Error(`标签不存在：${orphans.map(row => row.tag_id).join(', ')}`);
        }
    }
};

if (require.main === module) {
    main(true);
}