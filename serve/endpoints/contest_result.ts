import { rowsToMap } from '../../common/logic_utils';
import { query as sql } from '../db';
import { parseInt32 } from '../utils/logic_utils';
import { HtmlSegmentImpl, error, LOGIN_REQUIRED, finalizeSegment } from '../utils/render_utils';
import { loadContest192Json } from '../shared/contest_result';

import * as templates from '../templates/contest_result';

export const apiVoters: HtmlSegmentImpl = async ctx => {
    const threadId = parseInt32(ctx.params.id);
    const userInfo = ctx.state.mazoLoginUser;
    if (!userInfo) { throw error(403, LOGIN_REQUIRED); }
    const result = (await loadContest192Json())[threadId];
    if (!result) { throw error(404, '此主题未参与活动。'); }
    if (!('voters' in result)) { throw error(404, '该主题无投票数据。'); }
    const voters = result.voters;
    const userInfos = rowsToMap((await sql<{ user_id: number; user_name: string; }>('SELECT user_id, user_name FROM users WHERE user_id = ANY($1::INT[])', [voters[0].concat(voters[1])])).rows, 'user_id');
    return finalizeSegment(ctx, templates.campaignVotesArchive(ctx, {
        voters1: voters[0].map(v => userInfos.get(v)!),
        voters2: voters[1].map(v => userInfos.get(v)!)
    }));
};

export const apiReviewThread_id: HtmlSegmentImpl = async ctx => {
    const threadId = parseInt32(ctx.params.id);
    if (isNaN(threadId)) { throw error(400, '不要乱输入网址嘛┭┮﹏┭┮'); }

    const scorers = (await loadContest192Json())[threadId]?.scorers;
    if (!scorers) { throw error(404, '此作品没有评分信息。'); }

    const reviews = (threadId > 0) ? (await sql<{ user_id: number; user_name: string; post_id: number; content: string; }>('SELECT user_id, users.user_name, post_id, content FROM posts LEFT JOIN users USING (user_id) WHERE thread_id = $1 AND posts.flags = 16 ORDER BY RANDOM()', [threadId])).rows : [];
    const scoreUsers = (await sql<{ user_id: number; user_name: string; }>('SELECT user_id, user_name FROM users WHERE user_id = ANY($1::INT[])', [scorers])).rows;

    return finalizeSegment(ctx, templates.reviewThread(ctx, { reviews, scoreUsers }));
};

