const assert = require('assert');
const bbcode = require('./bbcode');

const errorOf = (str, lineno, colno) => err => (err instanceof bbcode.BBCodeError) && (err.chineseText === str) && ((lineno === undefined) || (err.lineno === lineno)) && ((colno === undefined) || (err.colno === colno));

const parseBBCode = (...args) => {
    const result = bbcode.parseBBCode(0, ...args);
    delete result.lines; // Lines are trivial, don't test them.
    if (!result.quotes.length) { delete result.quotes; } // To make goldens more concise, drop empty.
    if (!result.replyToUsers.length) { delete result.replyToUsers; } // To make goldens more concise, drop empty.
    if (result.iastItems === undefined) { delete result.iastItems; } // To make goldens more concise, drop empty.
    if (!result.links.attachments.length) { delete result.links.attachments; } // To make goldens more concise, drop empty.
    if (!result.links.toLinks.size) { delete result.links.toLinks; } // To make goldens more concise, drop empty.
    if (!Object.keys(result.links).length) { delete result.links; } // To make goldens more concise, drop empty.
    return result;
};

// Group of basic tests.

// HTML escaping.
assert.deepStrictEqual(parseBBCode('test <b>测试'), {
    html: 'test &lt;b&gt;测试',
    text: 'test <b>测试'
});

// Basic direct-convert tags.
assert.deepStrictEqual(parseBBCode('test [b]测[i]试[/i][/b]不对[i]没办法[/i]\n\n不妙了\n\n[h1]测试[/h1]不对劲\n[h3]好吧[/h3]'), {
    html: 'test <b>测<i>试</i></b>不对<i>没办法</i><br /><br />不妙了<br /><h1>测试</h1>不对劲<h3>好吧</h3>',
    text: 'test 测试不对没办法\n不妙了\n测试\n不对劲\n好吧'
});

// Bad tags (x3).
assert.throws(() => parseBBCode('高级\n[i]测试 [/i]不可以[hr]没了[/hr]'), errorOf('出现了 [/hr]，但你并没有未关闭的 [hr] 标签', 1, 19));
assert.throws(() => parseBBCode('[quote]\n [br]测试[/quote]'), errorOf('不支持的标签：[br]', 1, 1));
assert.throws(() => parseBBCode('大新闻[h3]对接'), errorOf('[h3] 标签未关闭', -1, -1));

// Headings as link targets.
assert.deepStrictEqual(parseBBCode('[h1=test1]测试[/h1]不对劲\n[h3=测试2g&内容]好吧[/h3]'), {
    html: '<h1 id="h-test1">测试</h1>不对劲<h3 id="h-u6d4bu8bd52gu5185u5bb9">好吧</h3>',
    text: '测试\n不对劲\n好吧',
    links: { toLinks: new Set(['test1', 'u6d4bu8bd52gu5185u5bb9']) }
});

// Links (both to headings and others).
assert.deepStrictEqual(parseBBCode('[url=/post/11]T1[/url]\n[url=https://google.com/]T2[/url]\n[url=//example.org/?a=1]T3[/url]'), {
    html: '<a href="/post/11">T1</a><br /><a href="https://google.com/">T2</a><br /><a href="//example.org/?a=1">T3</a>',
    text: 'T1\nT2\nT3'
});
assert.deepStrictEqual(parseBBCode('[url=post/11]T1[/url]\n[url=测试2g&内容]T2[/url]\n[url]what?[/url]'), {
    html: '<a href="post/11">T1</a><br /><a href="#h-u6d4bu8bd52gu5185u5bb9">T2</a><br /><a class="btn btn-link btn-sm" href="#h-what"><span class="material-icons">link</span></a>',
    text: 'T1\nT2\nwhat?'
});

// Font size compats.
assert.deepStrictEqual(parseBBCode('[size=3]测试[/size]\n[size=150]不对[/size]'), {
    html: '<span style="font-size: 16px; font-size: 1rem">测试</span><br /><span style="font-size: 21.69px; font-size: 1.36rem">不对</span>',
    text: '测试\n不对'
});

// Rich (but not so rich) text.
assert.deepStrictEqual(parseBBCode('[b]test foo[/b]METRO\n\n高级\n[i]测试 [/i]不可以[hr][size=2]\n[u]不妙[/u][url]http://jandan.net[/url]\n\n\n测试[url=http://g.cn/]fun[i]ny\n完[/i]蛋[/url][/size]\n<'), {
    html: '<b>test foo</b>METRO<br /><br />高级<br /><i>测试 </i>不可以<hr /><span style="font-size: 13px; font-size: 0.81rem"><br /><u>不妙</u><a href="http://jandan.net">http://jandan.net</a><br /><br /><br />测试<a href="http://g.cn/">fun<i>ny<br />完</i>蛋</a></span><br />&lt;',
    text: 'test fooMETRO\n高级\n测试 不可以\n不妙http://jandan.net\n测试funny\n完蛋\n<'
});

// Rich text done wrong (x3).
assert.throws(() => parseBBCode('[b]test foo[/b]METRO\n\n高级\n[i]测试 [/i]不可以[hr][size=2][/i]'), errorOf('出现了 [/i]，但你此时应该先关闭的是 size 标签', 3, 25));
assert.throws(() => parseBBCode('[b]test foo[/b]METRO\n\n高级\n[i]测试 [/i]不可以[hr][size=2]\n[u]不妙[/u][url]http://jandan.net[/url][url]'), errorOf('[url] 标签未关闭', -1, -1));
assert.throws(() => parseBBCode('不妙\n[i][color]test[/color][/i]'), errorOf('未设置标签的值', 1, 3));

// Standard-style list.
assert.deepStrictEqual(parseBBCode('[ul]\n[li]1[/li]\n[li]2[/li][li][ol]\n[li]3.a[/li]\n[li]3.b[/li]\n[/ol][/li]\n[/ul]'), {
    html: '<ul><li>1</li><li>2</li><li><ol><li>3.a</li><li>3.b</li></ol></li></ul>',
    text: '1\n2\n3.a\n3.b'
});

// Standard-style list done wrong.
assert.throws(() => parseBBCode('[ul]\n[li]1[/li]\n[li]2[/li][li][ol]\n[li]3.a[/li]\n[li]3.b[/li]\n[/ol]\n[/ul]'), errorOf('出现了 [/ul]，但你此时应该先关闭的是 li 标签'));

// Legacy-style list.
assert.deepStrictEqual(parseBBCode('[list]\n[*]1[*]2\n[*][list=1][*]3.a[/list]\n[*]4\n[/list]'), {
    html: '<ul><li>1</li><li>2</li><li><ol><li>3.a</li></ol></li><li>4</li></ul>',
    text: '1\n2\n3.a\n4'
});

// Legacy-style list, but with some closing [/*] and some dangling content.
assert.deepStrictEqual(parseBBCode('[list=1][*]BB[/*][*]cc[/*]X[*]ZZ[/list]'), {
    html: '<ol><li>BB</li><li>cc</li>X<li>ZZ</li></ol>',
    text: 'BB\ncc\nX\nZZ'
});

// Legacy-style list done wrong.
assert.throws(() => parseBBCode('[list][*]1\n[*]2[/list][*]test'), errorOf('[*] 标签必须直接置于 [list] 标签之内', 1, 11));

// Simple quoting.
assert.deepStrictEqual(parseBBCode('[quote]\n[quote=foo]\nbar\n[/quote]\n[/quote]\n\ntest done'), {
    html: '<blockquote><blockquote><cite>foo：</cite>bar</blockquote></blockquote>test done',
    text: '[引用]\ntest done',
    quotes: [{ authorName: 'foo', start: [1, 0, 11], end: [3, 0, 8]}]
});

// Complex quoting.
assert.deepStrictEqual(parseBBCode('foo[b]bar[/b]baz[quote=bar author=1125 post=4433][quote=foo post=1127]bar[/quote][/quote]test not done[quote=bar author=1125]duplicate ID[/quote][quote="baz bom" author=1126][i][b]test[/b]\n\nfunny[/i][/quote]'), {
    html: 'foo<b>bar</b>baz<blockquote><cite><a class="text-reset" href="/author/1125">bar</a>：<a href="/post/4433">↑</a></cite><blockquote><cite>foo：<a href="/post/1127">↑</a></cite>bar</blockquote></blockquote>test not done<blockquote><cite><a class="text-reset" href="/author/1125">bar</a>：</cite>duplicate ID</blockquote><blockquote><cite><a class="text-reset" href="/author/1126">baz bom</a>：</cite><i><b>test</b><br /><br />funny</i></blockquote>',
    text: 'foobarbaz\n[引用]\ntest not done\n[引用]\n[引用]',
    quotes: [{
        authorName: 'bar',
        start: [0, 16, 49],
        authorId: 1125,
        postId: 4433,
        end: [0, 81, 89]
    }, {
        authorName: 'bar',
        start: [0, 102, 125],
        authorId: 1125,
        end: [0, 137, 145]
    }, {
        authorName: 'baz bom',
        start: [0, 145, 174],
        authorId: 1126,
        end: [2, 9, 17]
    }],
    replyToUsers: [1125, 1125, 1126]
});

// Additional quoting: ensure nested quotings are not included; phpBB prop alias are supported.
assert.deepStrictEqual(parseBBCode('[quote=毛毛虫 user_id=42 post_id=105 time=337845818]\n[quote=毛毛虫 user_id=42 post_id=105 time=337845818]\n滑稽\n[/quote]\n失败了[/quote]\n不对劲\n[quote=大毛毛虫 user_id=43 post_id=106 time=337845819]再次测试[/quote]\n还是不对劲'), {
    html: '<blockquote><cite><a class="text-reset" href="/author/42">毛毛虫</a>：<a href="/post/105">↑</a></cite><blockquote><cite><a class="text-reset" href="/author/42">毛毛虫</a>：<a href="/post/105">↑</a></cite>滑稽</blockquote>失败了</blockquote>不对劲<blockquote><cite><a class="text-reset" href="/author/43">大毛毛虫</a>：<a href="/post/106">↑</a></cite>再次测试</blockquote>还是不对劲',
    text: '[引用]\n不对劲\n[引用]\n还是不对劲',
    replyToUsers: [42, 43],
    quotes: [{
        authorName: '毛毛虫',
        start: [0, 0, 49],
        authorId: 42,
        postId: 105,
        end: [4, 3, 11]
    }, {
        authorName: '大毛毛虫',
        start: [6, 0, 50],
        authorId: 43,
        postId: 106,
        end: [6, 54, 62]
      }
    ]
});

// Image, with some (but not all) attchments.
assert.deepStrictEqual(parseBBCode('[b][img]http://example.com/foo/a.jpg[/img][/b][img]/static/attachments/aabb0123.png[/img]\ndenote[img]http://example.com/static/attachments/deadbeef.jpg[/img]test'), {
    html: '<b><img src="http://example.com/foo/a.jpg" /></b><img src="/static/attachments/aabb0123.png" /><br />denote<img src="http://example.com/static/attachments/deadbeef.jpg" />test',
    text: '[图片][图片]\ndenote[图片]test',
    links: { attachments: ['aabb0123', 'deadbeef'] }
});

// Image, some with width/height specs.
assert.deepStrictEqual(parseBBCode('[img width=350]http://example.com/a.jpg[/img][img height=240]/static/attachments/aabb0123.png[/img]\n[img width="120" height="12"]http://example.com/static/attachments/deadbeef.jpg[/img]'), {
    html: '<img src="http://example.com/a.jpg" width="350" /><img src="/static/attachments/aabb0123.png" height="240" /><br /><img src="http://example.com/static/attachments/deadbeef.jpg" width="120" height="12" />',
    text: '[图片][图片]\n[图片]',
    links: { attachments: ['aabb0123', 'deadbeef'] }
});
assert.deepStrictEqual(parseBBCode('[img width="350" height=nnxx]http://example.com/a.jpg[/img][url=/test][img width="javascript:alert(0)" height="36" onerror="alert(0)"]https://example.com/aabb0123.png[/img][/url]'), {
    html: '<img src="http://example.com/a.jpg" width="350" /><a href="/test"><img src="https://example.com/aabb0123.png" height="36" /></a>',
    text: '[图片][图片]'
});

// Special-close nesting checks (x2).
assert.throws(() => parseBBCode('[img]a[hr]b[/img]'), errorOf('不可在 [img] 中出现其它标签', 0, 6));
assert.throws(() => parseBBCode('[url=/merge]merge[/url]\n[url]http://[b]example.com/[/b][/url]'), errorOf('不可在 [url] 中出现其它标签', 1, 12));

// Code block.
assert.deepStrictEqual(parseBBCode('test[code]alpha[b]dsfds\n\n[gamma]  [i][/quote][/code]delta'), {
    html: 'test<div class="card"><pre class="card-body mb-0"><code>alpha[b]dsfds\n\n[gamma]  [i][/quote]</code></pre></div>delta',
    text: 'test\nalpha[b]dsfds\n[gamma]  [i][/quote]\ndelta'
});
assert.deepStrictEqual(parseBBCode('test\n[code]informative[code][/code]\ndelta'), {
    html: 'test<div class="card"><pre class="card-body mb-0"><code>informative[code]</code></pre></div>delta',
    text: 'test\ninformative[code]\ndelta'
});
assert.deepStrictEqual(parseBBCode('A\n[code]XXX[b]xx[/b]<a href="/tag/1" onclick="what.if()">5</a>[code][/code]\nB'), {
    html: 'A<div class="card"><pre class="card-body mb-0"><code>XXX[b]xx[/b]&lt;a href=&quot;/tag/1&quot; onclick=&quot;what.if()&quot;&gt;5&lt;/a&gt;[code]</code></pre></div>B',
    text: 'A\nXXX[b]xx[/b]<a href="/tag/1" onclick="what.if()">5</a>[code]\nB'
});
assert.throws(() => parseBBCode('[code][b]alpha[i]beta'), errorOf('[code] 标签未关闭', -1, -1));

// Unsupported.
assert.deepStrictEqual(parseBBCode('[code unsupported=attachment]c672232sample7.jpg[/code]PS : 7月期待的RPG作品\nPS2 : 6月的天の光は恋の星終於完成.... 🤢 \n\n[code unsupported=video]<object width="480" height="380"><param name="movie" value="http://www.youtube.com/v/761ZOkt4bMg&rel=en&fs=1&color1=0x234900&color2=0xd4d4d4" /><param name="allowFullScreen" value="true" /><param name="allowscriptaccess" value="always" /><embed src="http://www.youtube.com/v/761ZOkt4bMg&rel=en&fs=1&color1=0x234900&color2=0xd4d4d4" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="550" height="380" /></object>[/code]', { allowUnsupported: true }), {
    html: '<div class="card"><div class="card-header">已失效或語法錯誤的源站附件</div><pre class="card-body mb-0"><code>c672232sample7.jpg</code></pre></div>PS : 7月期待的RPG作品<br />PS2 : 6月的天の光は恋の星終於完成.... 🤢 <br /><div class="card"><div class="card-header">已失效或語法錯誤的源站視頻</div><pre class="card-body mb-0"><code>&lt;object width=&quot;480&quot; height=&quot;380&quot;&gt;&lt;param name=&quot;movie&quot; value=&quot;http://www.youtube.com/v/761ZOkt4bMg&amp;rel=en&amp;fs=1&amp;color1=0x234900&amp;color2=0xd4d4d4&quot; /&gt;&lt;param name=&quot;allowFullScreen&quot; value=&quot;true&quot; /&gt;&lt;param name=&quot;allowscriptaccess&quot; value=&quot;always&quot; /&gt;&lt;embed src=&quot;http://www.youtube.com/v/761ZOkt4bMg&amp;rel=en&amp;fs=1&amp;color1=0x234900&amp;color2=0xd4d4d4&quot; type=&quot;application/x-shockwave-flash&quot; allowscriptaccess=&quot;always&quot; allowfullscreen=&quot;true&quot; width=&quot;550&quot; height=&quot;380&quot; /&gt;&lt;/object&gt;</code></pre></div>',
    text: 'c672232sample7.jpg\nPS : 7月期待的RPG作品\nPS2 : 6月的天の光は恋の星終於完成.... 🤢 \n<object width="480" height="380"><param name="movie" value="http://www.youtube.com/v/761ZOkt4bMg&rel=en&fs=1&color1=0x234900&color2=0xd4d4d4" /><param name="allowFullScreen" value="true" /><param name="allowscriptaccess" value="always" /><embed src="http://www.youtube.com/v/761ZOkt4bMg&rel=en&fs=1&color1=0x234900&color2=0xd4d4d4" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="550" height="380" /></object>'
});

// Unknown BBCode.

assert.deepStrictEqual(parseBBCode('test [b]测[i]试[smile][/i][/b][quote]测试数据[test][/quote]'), {
    html: 'test <b>测<i>试[smile]</i></b><blockquote>测试数据[test]</blockquote>',
    text: 'test 测试[smile]\n测试数据[test]'
});

// Group of IaSt tests.

const iastOptions = { iast: { escapeIdentifier: str => str } };

// Wrong cases.
assert.throws(() => parseBBCode('oops [input=test type=name]测试[/input]'), errorOf('此文章中不可以使用交互 tag（[input]）', 0, 5));
assert.throws(() => parseBBCode('\n[input=test]测试[/input]', iastOptions), errorOf('问题「test」未指定类型', 1, 0));
assert.throws(() => parseBBCode('[input type=name]测试[/input]', iastOptions), errorOf('[input] 标签未设置 ID', 0, 0));
assert.throws(() => parseBBCode('[input=test type=goof]测试[/input]', iastOptions), errorOf('问题「test」未指定类型', 0, 0));
assert.throws(() => parseBBCode('[input=test type=text default=测试][quote]test[/quote][/input]', iastOptions), errorOf('不可在 [input] 中出现 [quote]', 0, 33));
assert.throws(() => parseBBCode('[input=test type=text default=测试]测试[/input]\nfoo\n[input=test type=name]测试[/input]', iastOptions), errorOf('问题「test」已被设置过，不能重复设置', 2, 0));
assert.throws(() => parseBBCode('[input=test type=select]测试[option=1]1[/option][option=2]2[/option][/input]\n[option]厕所[/option]', iastOptions), errorOf('[option] 标签必须直接置于 [input] 标签之内', 1, 0));
assert.throws(() => parseBBCode('[input=test type=select]测试[option=bar]厨房[/option][b][option=baz]厕所[/option][/b][/input]', iastOptions), errorOf('[option] 标签必须直接置于 [input] 标签之内', 0, 52));
assert.throws(() => parseBBCode('[input=test type=name default=张无忌]测试[option=2]厕所[/option][/input]', iastOptions), errorOf('[input=test] 不支持设置选项', 0, 36));
assert.throws(() => parseBBCode('[input=test type=select]测试[option=foo]厕所[/option][option=foo]马桶[/option][/input]', iastOptions), errorOf('[input=test] 已经包含了另一个「foo」的选项', 0, 49));
assert.throws(() => parseBBCode('[input=test type=select]测试[option=foo]厕所[option=bar]马桶', iastOptions), errorOf('[option] 标签必须直接置于 [input] 标签之内', 0, 40));
assert.throws(() => parseBBCode('[input=test type=select]测试[option=foo]厕所', iastOptions), errorOf('[option] 标签未关闭', -1, -1));
assert.throws(() => parseBBCode('[input=test type=select]测试[option=foo]厕所[/option]', iastOptions), errorOf('[input] 标签未关闭', -1, -1));
assert.throws(() => parseBBCode('[input=test type=name default=迷你羊][/input]', iastOptions), errorOf('问题「test」的默认姓名「迷你羊」无法被识别为汉语姓名：迷并非已知的汉语姓氏，请尝试使用更常见的姓氏', 0, 0));
assert.throws(() => parseBBCode('\n[input=baz type=text default=ok][var]baz[/var][/input]\n', iastOptions), errorOf('[input=baz] 内部使用 [var] 调用自己的值', 1, 40));
assert.throws(() => parseBBCode('[input=foo type=select][option=o1]1[/option][option=o2]2[/option][/input][input=bar type=select default=o1][option=o2]test[/option][option=o3]test[/option][/input]', iastOptions), errorOf('[input=bar] 的默认值「o1」并不存在于选项列表中', 0, 155));
assert.throws(() => parseBBCode('\n[input=foo type=select]\n[/input]\n', iastOptions), errorOf('[input=foo] 的选项过少：至少要有两个选项', 2, 0));
assert.throws(() => parseBBCode('\n[input=baz type=select][option=a]a[/option][/input]\n', iastOptions), errorOf('[input=baz] 的选项过少：至少要有两个选项', 1, 43));
assert.throws(() => parseBBCode('\n[if=baz=a]测试[/if][input=baz type=select][option=a]a[/option][/input]\n', iastOptions), errorOf('问题「baz」必须设置在使用其答案之前', 1, 17));
assert.throws(() => parseBBCode('\n[if=a][/if]', iastOptions), errorOf('[if] 标签的表达式错误：表达式「a」不是 key=value 的形式', 1, 0));
assert.throws(() => parseBBCode('[if=a=1][if=b=2||][/if][/if]', iastOptions), errorOf('[if] 标签的表达式错误：表达式「」不是 key=value 的形式', 0, 8));

// Simple correct cases.
// TODO(chromaso): test the new syntax cases in https://gitlab.com/chromaso/mazochina-mirror-serve/-/merge_requests/13.
assert.deepStrictEqual(parseBBCode('[input=funny type=name default=张三]名字[/input]没救了', iastOptions), {
    html: '<span class="position-relative"><a href="javascript:void(0)" data-iast="input;funny;cnm"></a><div class="dropdown-menu p-3">名字</div></span>没救了',
    text: '张三没救了',
    iastItems: { inputs: { funny: { type: 'cnm', default: '张三' } }, usages: {} }
});
assert.deepStrictEqual(parseBBCode('测试：[input=funny type=name default=张三]\n名字\n[/input]没救了\n\n可不是吗', iastOptions), {
    html: '测试：<span class="position-relative"><a href="javascript:void(0)" data-iast="input;funny;cnm"></a><div class="dropdown-menu p-3">名字</div></span>没救了<br /><br />可不是吗',
    text: '测试：张三没救了\n可不是吗',
    iastItems: { inputs: { funny: { type: 'cnm', default: '张三' } }, usages: {} }
});
assert.deepStrictEqual(parseBBCode('[input=parent type=name default=张三][/input][input=child type=select][var]parent[/var][option=o1][var]FN(parent)[/var][/option][option=o2]T[/option][/input]', iastOptions), {
    html: '<span class="position-relative"><a href="javascript:void(0)" data-iast="input;parent;cnm"></a><div class="dropdown-menu p-3"></div></span><span class="position-relative"><a href="javascript:void(0)" data-iast="input;child"></a><div class="dropdown-menu p-3"><span data-iast="var;parent"></span><button class="btn btn-primary btn-sm m-1 d-block" data-iast="option;child;o1"><span data-iast="var;FN(parent)"></span></button><button class="btn btn-primary btn-sm m-1 d-block" data-iast="option;child;o2">T</button></div></span>',
    text: '张三',
    iastItems: {
        inputs: {
            parent: { type: 'cnm', default: '张三' },
            child: { type: [ 'o1', 'o2' ] }
        },
        usages: { parent: { text: [ 'any', 'FN' ] } }
    }
});
assert.deepStrictEqual(parseBBCode('测试：[input=funny type=text default=没什么]她说：[/input]\n啥[var]FN(tim)[/var]没了', iastOptions), {
    html: '测试：<span class="position-relative"><a href="javascript:void(0)" data-iast="input;funny;any"></a><div class="dropdown-menu p-3">她说：</div></span><br />啥<span data-iast="var;FN(tim)"></span>没了',
    text: '测试：没什么\n啥\n没了',
    iastItems: {
        inputs: { funny: { type: 'any', default: '没什么' } },
        usages: { tim: { text: [ 'FN' ] } }
    }
});
assert.deepStrictEqual(parseBBCode('完蛋\n选择项目：[input=sport type=select][option=swim]游泳[/option]\n或者[option=skate]滑冰[/option][/input]都行。', iastOptions), {
    html: '完蛋<br />选择项目：<span class="position-relative"><a href="javascript:void(0)" data-iast="input;sport"></a><div class="dropdown-menu p-3"><button class="btn btn-primary btn-sm m-1 d-block" data-iast="option;sport;swim">游泳</button>或者<button class="btn btn-primary btn-sm m-1 d-block" data-iast="option;sport;skate">滑冰</button></div></span>都行。',
    text: '完蛋\n选择项目：\n都行。',
    iastItems: { inputs: { sport: { type: [ 'swim', 'skate' ] } }, usages: {} }
});
assert.deepStrictEqual(parseBBCode('[if=sport=foo||sport=bar]内容环节～[/if]大体就是这样[if=sport=baz]内容[var]LC(boo)[/var]没有[/if]\n\n[var]sport[/var]', iastOptions), {
    html: '<div class="d-none" data-iast="if;sport=foo|sport=bar">内容环节～</div>大体就是这样<div class="d-none" data-iast="if;sport=baz">内容<span data-iast="var;LC(boo)"></span>没有</div><br /><span data-iast="var;sport"></span>',
    text: '内容环节～\n大体就是这样\n内容\n没有',
    iastItems: {
        inputs: {},
        usages: {
            sport: { options: [ 'foo', 'bar', 'baz' ], text: [ 'any' ] },
            boo: { text: [ 'LC' ] }
        }
    }
});
assert.deepStrictEqual(parseBBCode('[if=a=b&&b=c||c=d&&a=11&&dam=wtf||dam=bar][if=b=gg]test[/if][/if]test[if=c=1][/if]', iastOptions), {
    html: '<div class="d-none" data-iast="if;a=b&amp;b=c|c=d&amp;a=11&amp;dam=wtf|dam=bar"><div class="d-none" data-iast="if;b=gg">test</div></div>test<div class="d-none" data-iast="if;c=1"></div>',
    text: 'test\ntest',
    iastItems: {
        inputs: {},
        usages: {
            a: { options: [ 'b', '11' ] },
            b: { options: [ 'c', 'gg' ] },
            c: { options: [ 'd', '1' ] },
            dam: { options: [ 'wtf', 'bar' ] }
        }
    }
});