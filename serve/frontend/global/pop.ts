import { Types } from "../types";

let opened: HTMLElement | undefined;

const closeOpened = () => {
    opened!.classList.remove('show');
    opened = void 0;
};

export const findRoot = (source: HTMLElement): HTMLElement | null => {
    let target: ParentNode | null = source;
    while (target instanceof HTMLElement) {
        if (target.classList.contains('dropdown-menu')) {
            return target;
        }
        target = target.parentNode;
    }
    return null;
};

export const closeDropdownMenuIfSourceInside = (source: HTMLElement): boolean => {
    if (!opened) { return false; }
    const root = findRoot(source);
    if (root === opened) {
        closeOpened();
        return true;
    }
    return false;
};

export const positionPop: Types['positionPop'] = (anchor, popup, initialMouseEventX) => {
    const anchorPosition = anchor.getBoundingClientRect();
    const windowHeight = document.documentElement.clientHeight;
    const left = Math.round(anchorPosition.width / 2);
    popup.style.left = left + 'px';
    const mayPointUp = anchor.tagName.toLowerCase() !== 'input';
    const pointingUp = mayPointUp && ((anchorPosition.top + anchorPosition.bottom) / 2 > windowHeight * 2 / 3);
    // In Firefox, `.style.top = 100%` for an inline element that wraps across lines, would only put the element below the first line, instead of all inlines. It might be a mistake to use position-relative on inline elements (IaSt), but it's too late to fix.
    // Instead, we just set 0 here, knowing it would be wrong. `reposition` will fix it.
    if (pointingUp) {
        popup.style.top = 'auto';
        popup.style.bottom = '0';
    } else {
        popup.style.bottom = 'auto';
        popup.style.top = '0';
    }
    let retries = 60;
    const reposition = () => {
        const bcr = popup.getBoundingClientRect();
        if ((!bcr.width) && (!bcr.height)) { retries--; if (retries > 0) { setTimeout(reposition); } return; }
        let targetCenterClientX = initialMouseEventX || ((anchorPosition.left + anchorPosition.right) / 2);
        const windowWidth = document.documentElement.clientWidth;
        const rightOverflow = targetCenterClientX + bcr.width / 2 - windowWidth;
        if (rightOverflow > 0) {
            targetCenterClientX -= rightOverflow;
        }
        const leftUnderflow = targetCenterClientX - bcr.width / 2;
        if (leftUnderflow < 0) {
            targetCenterClientX -= leftUnderflow;
        }
        const centerClientX = (bcr.left + bcr.right) / 2; // When .style.left is `left`;
        popup.style.left = (left + targetCenterClientX - centerClientX) + 'px';
        if (pointingUp) {
            popup.style.bottom = (bcr.bottom - anchorPosition.top) + 'px';
        } else {
            popup.style.top = (anchorPosition.bottom - bcr.top) + 'px';
        }
    };
    reposition();
};

export const sideEffect = () => {
    // Close a dropdown when clicking outside.
    document.addEventListener('click', (e: MouseEvent) => {
        let target: ParentNode | null = e.target as HTMLElement;
        let anchor: HTMLElement | undefined;
        while (target instanceof HTMLElement) {
            // Keep consistent with Bootstrap's builtin. Also used by IaSt.
            if (target === opened) {
                return; // Clicking within a popup.
            } else if (target.getAttribute('data-toggle') === 'dropdown') {
                anchor = target;
                break;
            }
            target = target.parentNode;
        }
        if (opened) { closeOpened(); }
        if (!anchor) { return; }
        const dropdown = anchor.nextElementSibling;
        if (!(dropdown instanceof HTMLElement)) { return; }
        dropdown.classList.add('show');
        opened = dropdown;
        positionPop(anchor, dropdown, e.x);
    });
};
