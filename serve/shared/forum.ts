import { rowsToMap } from '../../common/logic_utils';
import { query as sql } from '../db';

type ListForumReturnValue = { id: number; name: string; };

export const forumsCache = sql<{ forum_id: number; forum_name: string; parent_forum_id: number; }>('SELECT forum_id, forum_name, parent_forum_id FROM forum_targets ORDER BY forum_targets.display_order ASC').then(forums => rowsToMap(forums.rows, 'forum_id'));

export const listForums = async (): Promise<ListForumReturnValue[]> => {
    const dbResults = Array.from((await forumsCache).values());
    const output: ListForumReturnValue[] = [];
    const recursiveAdd = (parentId: number, prefix: string): void => dbResults.filter(forum => forum.parent_forum_id === parentId).forEach(row => {
        output.push({ id: row.forum_id, name: prefix + row.forum_name });
        recursiveAdd(row.forum_id, prefix + '　　');
    });
    recursiveAdd(0, '');
    return output;
};
