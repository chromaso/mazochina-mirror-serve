import { query as sql } from '../db';
import { sendReplyNotifications } from './logic_utils';

import { IS_DEV_MODE } from '../conf';

type Requiring<T, K extends keyof T> = T & Required<Pick<T, K>>;

type UpdateNotificationParams = {
    isFromOp: boolean; // If the post is from thread's owner_user_id.
    isActualReply?: boolean; // If the post is considered an "update".
    thread: number;
    post: number;
    skipUsers?: number[]; // If set, skip sending to this set of users.
    postDate?: number; // Seconds-since-epoch when posting a new post. Leave undefined when and ONLY when deleting a post.
};

export const onNewPost = async (params: Requiring<UpdateNotificationParams, 'skipUsers' | 'postDate' | 'isActualReply'>) => {
    const targetStarType = params.isFromOp ? (params.isActualReply ? 2 : 3) : 6;
    const subscriptions = (await sql<{ user_id: number }>('SELECT user_id FROM thread_stars WHERE thread_id = $1 AND (star_type & 7::SMALLINT) >= $2 AND NOT (user_id = ANY($3::INT[]))', [params.thread, targetStarType, params.skipUsers])).rows;
    if (!subscriptions.length) { return; } // No subscription at all.
    await sql<{}, [number, number, number[]]>('INSERT INTO notifications (user_id, notification_date, message_type, primary_target_id, dismissed) SELECT subscriber_user_id, $1, 4, $2, 0 FROM UNNEST($3::int[]) subscriber_user_id ON CONFLICT (user_id, message_type, primary_target_id) DO UPDATE SET notification_date = EXCLUDED.notification_date, dismissed = EXCLUDED.dismissed', [params.postDate, params.thread, subscriptions.map(row => row.user_id)]);
};

// Returns:
// undefined when it's no longer subscribed (should be dismissed, otherwise will be deleted upon unsubscribing). Format as "你曾订阅的主题「...」曾有新回复".
// [-1, 4] when (star_type === 2), number indicating the num of replies of OP.
// [-1, 0] when (star_type === 2), no replies from OP. It MIGHT be a false notification in the case where a subsequence post gets deleted. Format as "你订阅的主题「...」楼主曾有新的回复（你现已阅读）".
// [4, 0] or [0, 0] or [4, 2] when (star_type === 3 or 6), number indicating [total new posts, new posts from OP]. [0, 0] MIGHT be a false notification in the case where a subsequence post gets deleted.
export const fetchType4Notifications = async (userId: number, threads: number[], threadInfo: Map<number, { owner_user_id: number }>): Promise<Map<number, [number, number] | undefined>> => {
    // star_type here will be 0 if it's 8 in the DB (i.e. blocked).
    const threadSubRows = (await sql<{ thread_id: number, star_type: 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7, up_to_date: number }, [number, number[]]>('SELECT thread_id, (star_type & 7::SMALLINT) AS star_type, up_to_date FROM thread_stars INNER JOIN unread_status USING (user_id, thread_id) WHERE user_id = $1 AND thread_id = ANY($2::int[])', [userId, threads])).rows;
    const output = new Map<number, [number, number] | undefined>();
    for (const row of threadSubRows) {
        const opOnly = row.star_type === 2;
        if (!(opOnly || (row.star_type === 6))) {
            output.set(row.thread_id, undefined);
            continue; // They have already unsubscribed.
        }
        const posts = (await sql<{ post_id: number, user_id: number, type3: number | null }, [number, number, number]>(`SELECT post_id, posts.user_id, (SELECT notification_id FROM notifications WHERE message_type = 3 AND primary_target_id = posts.post_id AND notifications.user_id = $1) AS type3 FROM posts WHERE thread_id = $2 AND most_recent_update >= 0 AND posted_date > $3`, [userId, row.thread_id, row.up_to_date])).rows;
        const owner = threadInfo.get(row.thread_id)!.owner_user_id;
        const ownerPosts = posts.filter(post => post.user_id === owner);
        const predicate = (opOnly ? ownerPosts : posts).filter(item => !item.type3);
        if (!predicate) {
            output.set(row.thread_id, [opOnly ? -1 : 0, 0]);
            continue;
        }
        output.set(row.thread_id, [opOnly ? -1 : posts.length, ownerPosts.length]);
    }
    return output;
};

export const onDeletingPost = async (params: Pick<UpdateNotificationParams, 'post' | 'thread'>) => {
    // TODO: check each user to see if a type-4 notification is still warranted.
    const existingNotifications = (await sql<{ user_id: number }>('SELECT user_id FROM notifications WHERE message_type = 4 AND primary_target_id = $1 AND dismissed = 0', [params.thread])).rows;
    if (existingNotifications.length) { 
        console.warn(`[NF] Users ${JSON.stringify(existingNotifications.map(user => user.user_id))} may subject to false type-4 notification due to a deletion at post ${params.post}`);
    }
};

export const onUpdatingPostConvertingBetweenType3AndType4 = async (params: Requiring<UpdateNotificationParams, 'postDate' | 'isActualReply'>, type3: ReturnType<typeof sendReplyNotifications>) => {
    const type3Actions = await type3;
    if (type3Actions.removedUsers.length) {
        const targetStarType = params.isFromOp ? (params.isActualReply ? 2 : 3) : 6;
        const subscriptions = (await sql<{ user_id: number, up_to_date: number }, [number, number, number[]]>('SELECT user_id, up_to_date FROM thread_stars LEFT JOIN unread_status USING (user_id, thread_id) WHERE thread_id = $1 AND (star_type & 7::SMALLINT) >= $2 AND user_id = ANY($3::INT[])', [params.thread, targetStarType, type3Actions.removedUsers])).rows;
        const notificationsToAdd = subscriptions.filter(subscription => (params.postDate > (subscription.up_to_date || 0))).map(row => row.user_id);
        if (IS_DEV_MODE) { console.log(`[NF] Doing type 3->4 for ${JSON.stringify(type3Actions.removedUsers)}, actually matching ${JSON.stringify(notificationsToAdd)}`); }
        if (notificationsToAdd.length) {
            await sql<{}, [number, number, number[]]>('INSERT INTO notifications (user_id, notification_date, message_type, primary_target_id, dismissed) SELECT subscriber_user_id, $1, 4, $2, 0 FROM UNNEST($3::INT[]) subscriber_user_id ON CONFLICT (user_id, message_type, primary_target_id) DO UPDATE SET notification_date = GREATEST(notifications.notification_date, EXCLUDED.notification_date), dismissed = EXCLUDED.dismissed', [params.postDate, params.thread, notificationsToAdd]);
        }
    }
    if (type3Actions.addedUsers.length) {
        console.warn(`[NF] Users ${JSON.stringify(type3Actions.addedUsers)} may subject to false type-4 notification due to a 4->3 conversion at post ${params.post}`);
    }
};
