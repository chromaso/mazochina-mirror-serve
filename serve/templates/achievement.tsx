import * as elements from 'typed-xhtml';
import { HtmlTmpl } from '../utils/tsx_utils';
import { updateAchievements, ACHIEVEMENTS, PSingle, MSingle, AchievementConf, achivementNameIconLookUp } from '../shared/achievements';

export const ACHIEVEMENT_COLORS = ['af9500', 'b4b4b4', '6a3805', 'd3d3d3'];

const achivementImg: HtmlTmpl<{ level: 0 | 1 | 2 | 3, matIcon?: string, locked?: boolean, size: number }> = (ctx, { level, matIcon, locked, size }) => (
    <div style={`width: ${size}px; height: ${size}px; background: url('/static/a240/medal-${level}.png') center / contain no-repeat`} class={((level && !locked) ? '' : 'opacity-75') + ' user-select-none text-center'}>
        <span class="material-icons text-white" style={`font-size: ${0.4 * size}px; line-height: ${0.75 * size}px`}>{matIcon || 'star'}</span>
    </div>
);

export const achievementInHovercard: HtmlTmpl<[string, 1 | 2 | 3]> = (ctx, self) => (
    <span class="badge rounded-pill text-white me-2" style={`background: #${ACHIEVEMENT_COLORS[3 - self[1]]}`}>
        <span class="material-icons align-middle me-1" style={`font-size: 100%`}>{achivementNameIconLookUp(self[0]) || 'star'}</span>
        {ctx.state.render.staticConv(self[0])}
    </span>
);

const renderGuild: HtmlTmpl<{
    maxLevel: number
}> = (ctx, {
    maxLevel
}) => {
    const { staticConv } = ctx.state.render;
    const matIcon = 'volunteer_activism';
    const texts = ['向喵喵橙大人贡上 ￥1000', '向喵喵橙大人贡上所有的作品', '向喵喵橙大人贡上自己的生命'];
    const singleLevel = (level: 1 | 2 | 3) => (
        <div class="d-flex flex-row align-items-center">
            {achivementImg(ctx, { level, matIcon, locked: maxLevel < level, size: 40 })}
            <div class={'ps-2' + ((maxLevel < level) ? ' opacity-75' : '')}>
                <div>{staticConv(texts[level - 1])}</div>
                <small>来自喵喵橙大人的奖励</small>
            </div>
        </div>
    );
    return (
        <div class="card position-relative">
            <div class="d-flex flex-row align-items-center" data-toggle="dropdown" style="cursor: pointer">
                <div class="py-1 ps-2">{achivementImg(ctx, { level: maxLevel as 1 | 2 | 3, matIcon, size: 54 })}</div>
                <div class="px-2 overflow-hidden">
                    <div class="fw-bold">喵喵橙粉丝骑士团</div>
                    <div class="small text-truncate">把自己奉献给喵喵橙大人</div>
                </div>
            </div>
            <div class="dropdown-menu px-2 w-100">
                <div>
                    这是一个只有喵喵橙粉丝同好会成员能看到的成就，不会公开显示。
                </div>
                {([3, 2, 1] as const).map(i => (<>
                    <div class="dropdown-divider" />
                    {singleLevel(i)}
                </>))}
            </div>
        </div>
    );
};

const renderProgramatic: HtmlTmpl<{
    single: PSingle,
    conf: AchievementConf,
    maxLevel: number
}> = (ctx, {
    single,
    conf: { name, descPrefix, l3CountThreshold, descSuffix, postId, epoch, matIcon },
    maxLevel
}) => {
    const { staticConv, renderDate } = ctx.state.render;
    const renderGranted = (granted: number) => (
        granted ? ((granted > epoch) ? (<span>于 {renderDate(granted)} 达标</span>) : '设立此成就前便已达标') : '尚未达标'
    );
    const singleLevel = (level: 1 | 2 | 3, threshold: number) => (
        <div class="d-flex flex-row align-items-center">
            {achivementImg(ctx, { level, matIcon, locked: maxLevel < level, size: 40 })}
            <div class={'ps-2' + ((maxLevel < level) ? ' opacity-75' : '')}>
                <div>{staticConv(descPrefix)} {threshold} {staticConv(descSuffix)}</div>
                <small>{renderGranted(single.granted[level - 1])}</small>
            </div>
        </div>
    );
    return (
        <div class="card position-relative">
            <div class="d-flex flex-row align-items-center" data-toggle="dropdown" style="cursor: pointer">
                <div class="py-1 ps-2">{achivementImg(ctx, { level: maxLevel as 1 | 2 | 3, matIcon, size: 54 })}</div>
                <div class="px-2 overflow-hidden">
                    <div class="fw-bold">{staticConv(name)}</div>
                    <div class="small text-truncate">{staticConv(descPrefix)} {single.count} {staticConv(descSuffix)}</div>
                </div>
            </div>
            <div class="dropdown-menu px-2 w-100">
                <div>
                    {staticConv(single.desc)}
                    <div class="mt-1">
                        <a class="btn btn-secondary btn-sm" href={`javascript:showArcli(${JSON.stringify(name)}, ${JSON.stringify(staticConv(name))})`}>列出所有具体条目</a>
                        {' '}
                        <a class="btn btn-secondary btn-sm" href={'/post/' + postId}>关于</a>
                    </div>
                </div>
                {([3, 2, 1] as const).map(i => (<>
                    <div class="dropdown-divider" />
                    {singleLevel(i, Math.ceil(l3CountThreshold * i / 3))}
                </>))}
            </div>
        </div>
    );
};

const renderManual: HtmlTmpl<MSingle> = (ctx, single) => (
    <div class="card position-relative">
        <div class="d-flex flex-row align-items-center" data-toggle="dropdown" style="cursor: pointer">
            <div class="py-1 ps-2">{achivementImg(ctx, { level: single.level as 1 | 2 | 3, size: 54 })}</div>
            <div class="px-2 overflow-hidden">
                <div class="fw-bold">{ctx.state.render.staticConv(single.name)}</div>
                <div class="small text-truncate">由管理员<a href={`/post/` + single.postId}>手动授予</a>于 {ctx.state.render.renderDate(single.granted, true)[0]}</div>
            </div>
        </div>
        <div class="dropdown-menu px-2 w-100">
            此成就由管理员手动授予，详情请参见<a href={`/post/` + single.postId}>对应帖子</a>。
        </div>
    </div>
);

const guildMembers = new Map<number, 0 | 1 | 2 | 3>([[25646, 1], [1073741824, 0], [176607, 1], [1073787088, 0], [97815, 1], [1073791488, 2], [1073779435, 2]]);

export const achievements: HtmlTmpl<{ 
    entries: Awaited<ReturnType<typeof updateAchievements>>,
    isSelf: boolean,
    userId: number
}> = (ctx, { entries, isSelf, userId }) => {
    const showGuild = guildMembers.has(ctx.state.mazoLoginUser?.id ?? 0) && (isSelf || (userId !== ctx.state.mazoLoginUser?.id)); // Don't show it in preview mode.
    const guildLevel = guildMembers.get(userId ?? 0);
    const allEntries: Readonly<{ programatic: boolean, index: number, level: number, granted: number }>[] = [
        ...entries.m.map((single, index) => ({ programatic: false, index, level: single.level, granted: single.granted } as const)),
        ...((showGuild && (guildLevel !== undefined)) ? [{ programatic: true, index: -1, level: guildLevel, granted: 0 }]: []),
        ...entries.p.map((single, index) => {
            for (let level = 3; level > 0; level--) {
                const granted = single.granted[level - 1];
                if (granted) {
                    return { programatic: true, index, level, granted } as const;
                }
            }
            return { programatic: true, index, level: 0, granted: 0 } as const;
        }).filter(isSelf ? (_ => _) : (single => single.level))
    ].sort((a, b) => ((b.level - a.level) || (b.granted - a.granted)));
    return allEntries.length ? (
        <div>
            {isSelf ? (
                <form class="mb-2" method="post">
                    你可以从你达成的<span style={`color: #${ACHIEVEMENT_COLORS[0]}`}>⬤</span>级别成就中选取一个，作为你的默认头衔：
                    <select class="form-control form-control-sm d-inline-block w-auto" id="title-selector" name="title">
                        <option value="null">（无头衔）</option>
                        {allEntries.filter(entry => entry.level >= 3).map(indexer => {
                            const name = indexer.programatic ? ACHIEVEMENTS[indexer.index].name : entries.m[indexer.index].name;
                            return (<option selected={(entries.currentTitle === name)} value={name}>{ctx.state.render.staticConv(name)}</option>);
                        })}
                    </select>
                </form>
            ) : null}
            <div class="row">
                {allEntries.map(indexer => (    
                    <div class={`col-sm-6 col-lg-4 col-xl-3 mt-2` + (indexer.level ? '' : ' d-none')}>
                        {indexer.programatic ? ((indexer.index < 0) ? renderGuild(ctx, { maxLevel: indexer.level }) : renderProgramatic(ctx, {
                            single: entries.p[indexer.index],
                            conf: ACHIEVEMENTS[indexer.index],
                            maxLevel: indexer.level
                        })) : renderManual(ctx, entries.m[indexer.index])}
                    </div>            
                ))}
            </div>
        </div>
    ) : (<div>暂未解锁任何成就</div>);
};

export const achievementQualificationList: HtmlTmpl<{ 
    desc: string,
    items: SafeString[],
    footnotes: string[]
}> = (ctx, { desc, items, footnotes }) => (
    <div>
        <p>{ctx.state.render.staticConv(desc)}。</p>
        {items.length ? (<>
            <div>以下为全列表：</div>
            <ul>
                {items.map(item => (
                    <li>{item}</li>
                ))}
            </ul>
        </>) : (
            <p>无符合条件的条目可供显示。</p>
        )}
        {footnotes.map(footnote => <div class="small">注：{ctx.state.render.staticConv(footnote)}</div>)}
    </div>
);

const introduction: HtmlTmpl<AchievementConf> = (ctx, { descPrefix, l3CountThreshold, descSuffix, matIcon }) => {
    const { staticConv } = ctx.state.render;
    const levelNames = ['金', '银', '铜'];
    const singleLevel = (level: 1 | 2 | 3, threshold: number) => (
        <div class="d-flex flex-row align-items-center">
            {achivementImg(ctx, { level, matIcon, size: 54 })}
            <div class="ps-2">
                <div>{levelNames[3 - level]}等级要求：</div>
                <div>{staticConv(descPrefix)} {threshold} {staticConv(descSuffix)}</div>
            </div>
        </div>
    );
    return (
        <div class="card mb-4">
            <ul class="list-group list-group-flush">
                {([3, 2, 1] as const).map(i => (
                    <li class="list-group-item">{singleLevel(i, Math.ceil(l3CountThreshold * i / 3))}</li>
                ))}
            </ul>
        </div>
    );
};

export const achievementGranteeList: HtmlTmpl<{
    name: string,
    list: { user_id: number, max_lvl: number, user_name: string }[],
    conf: AchievementConf | undefined
}> = (ctx, { name, list, conf }) => (
    <div class="container">
        <h1 class="mb-4">成就「{ctx.state.render.staticConv(name)}」的所有获得者</h1>
        {conf ? introduction(ctx, conf) : null}
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">用户名</th>
                    <th scope="col">成就等级</th>
                </tr>
            </thead>
            <tbody>
                {list.map(single => (
                    <tr>
                        <td><a href={'/author/' + single.user_id}>{single.user_name}</a></td>
                        <td><span style={`color: #${ACHIEVEMENT_COLORS[3 - single.max_lvl]}`}>⬤</span></td>
                    </tr>
                ))}
            </tbody>
        </table>
    </div>
);