import { $xhr, $, $set } from "./dollar";
import { showModal } from "./modal";
import { getThreadIdFromUri, loadingSpinner } from "./utils";

export const tagPageCampaignVoteOverlay = (table: HTMLTableSectionElement) => {
    $xhr('/api/votes', {
        e: msg => window.alert('投票加载失败！' + msg),
        j: (json: Record<string, [number, number, number]>) => {
            const headTr = (table.previousElementSibling as HTMLTableSectionElement).firstElementChild as HTMLTableRowElement;
            headTr.insertBefore($('th', void 0, '投票'), headTr.children[2]);
            const trs = table.children;
            [].forEach.call(trs, (tr: HTMLTableRowElement) => {
                const anchor = tr.querySelector('a')!;
                const threadIdMatch = anchor.pathname.match(/^\/thread\/(\d+)(\/\d+)?$/);
                if (!threadIdMatch) { return; }
                const rawRow = json[threadIdMatch[1]] || [];
                const medalCount = (rawRow[2] || 0);
                const thumbCount = (rawRow[1] || 0) + medalCount;
                const myVote = rawRow[0] || 0;
                const hint = `共 ${thumbCount} 人为此作品投上了 👍，${medalCount} 人为此作品投上了 🥇；你` + (myVote ? '为此作品投上了 👍' + ((myVote >= 2) ? ' 和 🥇' : '') : '未为此作品投票');
                tr.insertBefore($('td', 'align-middle', $('span', 'mazomirror-tip', [
                    $('span', (myVote >= 1) ? '' : 'opacity-50', '👍'),
                    $('span', 'small text-muted', String(thumbCount)),
                    ' ',
                    $('span', (myVote >= 2) ? '' : 'opacity-50', '🥇'),
                    $('span', 'small text-muted', String(medalCount))
                ]).also(tip => tip.setAttribute('data-title', hint))), tr.children[2]);
            });
        }
    });
};

export const loadContestVoters = (threadId?: number) => {
    if (!threadId) { threadId = getThreadIdFromUri(); }

    const container = $('div', undefined, loadingSpinner(false));
    showModal('给该作品投票的读者', container);
    $xhr('/api/voters/' + threadId, {
        e: error => $set(container, $('div', 'alert alert-danger', '加载失败：' + error)),
        t: html => { container.innerHTML = html; }
    });
};

export const loadReviewThread = (placeholder: HTMLElement, threadId?: number | string) => {
    if (!threadId) { threadId = getThreadIdFromUri(); }
    $set(placeholder, '评语加载中…');
    $xhr('/api/review/thread/' + threadId, {
        e: error => $set(placeholder, $('div', 'alert alert-danger', '加载失败：' + error)),
        t: innerHTML => {
            const div = $('div', { innerHTML });
            placeholder.parentNode!.replaceChild(div.firstElementChild!, placeholder);
        }
    });
};

export const initResultsThreadList = () => {
    const header = document.getElementById('contest-results-hdr')!;
    const drop = document.getElementById('contest-results-ddm')!;

    const tbd = (header.parentNode as HTMLTableSectionElement).nextElementSibling!;
    const trPairs: [HTMLTableRowElement, HTMLTableRowElement][] = [];
    for (var i = 1; i < tbd.children.length; i += 2) {
        trPairs.push([
            tbd.children[i - 1] as HTMLTableRowElement,
            tbd.children[i] as HTMLTableRowElement
        ]);
    }
    const getSortValue = (td: Element) => parseFloat(td.getAttribute('data-sort-v') || td.textContent!) || 0;
    const sortTable = (colIdx: number) => {
        for (const [row1, row2] of trPairs.sort((a, b) => getSortValue(b[0].children[colIdx]) - getSortValue(a[0].children[colIdx]))) {
            tbd.appendChild(row1);
            tbd.appendChild(row2);
        }
        for (var i = 1; i < header.children.length; i++) {
            (header.children[i].lastChild as HTMLButtonElement).classList[(i === colIdx) ? 'remove' : 'add']('opacity-50');
        }
    };
    const onSortButton = (e: MouseEvent) =>
        sortTable(parseInt((e.currentTarget as HTMLButtonElement).name.substr(3)));
    const showColumn = (index: number, show: boolean) =>
        [[header]].concat(trPairs).forEach(pair => {
            (pair[0].children[index] as HTMLTableCellElement).style.setProperty('display', show ? 'table-cell' : 'none', 'important');
        });
    const makeCb = (checked: boolean, name: string, className: string | null, onChange: (e: MouseEvent) => void) => {
        var randId = 'rand-' + Math.random().toString(36).substring(2, 8);
        return $('div', 'form-check form-check-inline' + (className ? ' ' + className : ''), [
            $('input', { type: 'checkbox', classList: 'form-check-input', checked: checked, id: randId }, [], onChange),
            $('label', { classList: 'form-check-label', htmlFor: randId }, name)
        ]);
    };
    ([] as HTMLTableCellElement[]).forEach.call(header.children, (th, index) => {
        if (!index) { return; }
        th.appendChild($('button', { className: 'btn btn-sm btn-link opacity-50', name: 'sc-' + index, title: '按此分数排序' }, $('span', 'material-icons', 'sort').also(function (el) { el.style.fontSize = '100%'; }), onSortButton));
        const titleDiv = th.querySelector('div')!;
        const name = titleDiv.textContent as string;
        const listDiv = $('div');
        if (th.classList.contains('d-md-table-cell')) {
            const onSubClk = (e: MouseEvent) => {
                showColumn(index, (e.currentTarget as HTMLInputElement).checked);
                const div = (e.currentTarget as HTMLInputElement).parentNode as HTMLDivElement;
                div.classList.remove('d-md-none');
                div.classList.remove('d-none');
                div.classList.remove('d-md-inline');
                $set(listDiv, div);
            };
            $set(listDiv, [
                makeCb(false, name, 'd-md-none', onSubClk),
                makeCb(true, name, 'd-none d-md-inline', onSubClk)
            ]);
        } else {
            $set(
                listDiv,
                makeCb(
                    !th.classList.contains('d-none'),
                    name,
                    titleDiv.classList.contains('small') ? 'small' : null,
                    e => showColumn(index, (e.currentTarget as HTMLInputElement).checked)
                )
            );
        }
        drop.appendChild(listDiv);
    });
    const clickExpRev = (e: MouseEvent) => {
        const a = e.currentTarget as HTMLElement;
        const tid = a.getAttribute('data-rev-tgr-thread');
        const td = a.parentNode!.parentNode!.parentNode!;
        const plcHdr = $('div');
        td.appendChild($('div', 'px-2', [
            plcHdr,
            $('div', 'text-center', $('button', 'btn btn-secondary btm-sm', '收起评语', () => {
                td.removeChild(td.lastChild!);
                a.style.visibility = 'visible';
            }))
        ]));
        loadReviewThread(plcHdr, tid!);
        a.style.visibility = 'hidden';
    };

    ([] as HTMLElement[]).forEach.call(tbd.querySelectorAll('[data-rev-tgr-thread]'), a => a.addEventListener('click', clickExpRev));
};