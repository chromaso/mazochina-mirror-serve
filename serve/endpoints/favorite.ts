import { THRESHOLD_FOR_LOCAL } from '../../common/consts';
import { rowsToMap, unawaited } from '../../common/logic_utils';
import { query as sql, withTransaction } from '../db';
import { tagPref } from '../shared/tags';
import { parseInt32 } from '../utils/logic_utils';
import { PageImpl, error, LOGIN_REQUIRED, redirectWithDelay, finalizePage, finalizeSegment, HtmlSegmentImpl, RpcImpl } from '../utils/render_utils';
import { BaseThreadTarget, THREAD_RENDER_COLS, renderThreads } from '../shared/thread_table';

import * as templates from '../templates/favorite';
import { BLOCKED_THREADS_DUMMY_COLLECTION, MY_THREADS_DUMMY_COLLECTION } from '../shared/favorite';

export const prepareFavListForAuthor = async (userId: number, isSelf: boolean): Promise<{
    fc_id: number, title: string, count: number, isPrivate: boolean
}[]> => {
    const rawCollections = (await sql('SELECT fc_id, title, default_star_type FROM fav_collections WHERE user_id = $1 ORDER BY display_order ASC', [userId])).rows;
    const allFavs = rowsToMap((await sql<{ cnt: number, fc_id: number }, [number]>('SELECT COUNT(thread_id)::INT AS cnt, fc_id FROM (SELECT thread_id, UNNEST(fc_ids) AS fc_id FROM thread_stars WHERE user_id = $1) t GROUP BY fc_id', [userId])).rows, 'fc_id');

    return (
        isSelf ? (
            [
                ...(allFavs.has(MY_THREADS_DUMMY_COLLECTION.fc_id) ? [MY_THREADS_DUMMY_COLLECTION]: []),
                ...(allFavs.has(BLOCKED_THREADS_DUMMY_COLLECTION.fc_id) ? [BLOCKED_THREADS_DUMMY_COLLECTION]: []),
                ...rawCollections
            ]
        ) : rawCollections.filter(item => !(item.default_star_type & 0x10))
    ).map(item => {
        const countRow = allFavs.get(item.fc_id);
        return {
            ...item,
            count: countRow ? countRow.cnt : 0,
            isPrivate: !!(item.default_star_type & 0x10)
        };
    });
};

export const author_id_favs: PageImpl = async ctx => {
    const userId = parseInt32(ctx.params.id);
    if (isNaN(userId)) { throw error(400, '又在乱改网址了吗……'); }
    const loginUser = ctx.state.mazoLoginUser;
    if (!loginUser) { throw error(403, LOGIN_REQUIRED); }
    const userMeta = (await sql<{ user_name: string; has_password: boolean; label: string | null; }, [number]>('SELECT user_name, password_h IS NOT NULL AS has_password FROM users WHERE user_id = $1', [userId])).rows[0];
    if (!userMeta) {
        const tombstone = (await sql<{ new_user_id: number; }>('SELECT new_user_id FROM user_tombstone WHERE prev_user_id = $1', [userId])).rows[0];
        if (!tombstone) { throw error(404, '该作者不存在来着（'); }
        return redirectWithDelay(ctx, {
            message: '该用户已合并至另一用户',
            targetText: '该用户的新页面',
            targetHref: '/author/' + tombstone.new_user_id + '/favs'
        });
    }
    if (!userMeta.has_password) {
        throw error(400, '该用户未在镜像注册，因此无法使用收藏功能～');
    }
    const isSelf = loginUser.id === userId;

    return finalizePage(ctx, {
        title: userMeta.user_name + ctx.state.render.staticConv(' 的收藏'),
        nav: true,
        canonicalPath: '/author/' + userId + '/favs',
    }, templates.favs(ctx, {
        username: userMeta.user_name,
        isSelf,
        collections: await prepareFavListForAuthor(userId, isSelf),
    }));
};

export const collection_id_page: PageImpl = async ctx => {
    const favId = parseInt32(ctx.params.id);
    if (isNaN(favId)) { throw error(400, '又在乱改网址了吗……'); }
    let page = parseInt32(ctx.params.page);
    if (isNaN(page) || page <= 0) { page = 1; }

    const loginUser = ctx.state.mazoLoginUser;
    if (!loginUser) { throw error(403, LOGIN_REQUIRED); }

    let meta: { default_star_type: number; title: string; user_id: number; user_name: string; display_order: number; };
    switch (favId) {
        case MY_THREADS_DUMMY_COLLECTION.fc_id:
            meta = { ...MY_THREADS_DUMMY_COLLECTION, user_id: loginUser.id, user_name: loginUser.name, display_order: 0 };
            break;
        case BLOCKED_THREADS_DUMMY_COLLECTION.fc_id:
            meta = { ...BLOCKED_THREADS_DUMMY_COLLECTION, user_id: loginUser.id, user_name: loginUser.name, display_order: 0 };
            break;
        default:
            meta = (await sql<{ default_star_type: number; title: string; user_id: number; user_name: string; display_order: number; }, [number]>('SELECT default_star_type, title, user_id, user_name, display_order FROM fav_collections LEFT JOIN users USING (user_id) WHERE fc_id = $1', [favId])).rows[0];
    }
    if (!meta) { throw error(404, '该收藏夹不存在来着（'); }

    const isDefault = (favId > 0) ? ((await sql('SELECT COUNT(*)::INT AS cnt FROM fav_collections WHERE user_id = $1 AND display_order <= $2 AND fc_id <> $3', [loginUser.id, meta.display_order, favId])).rows[0].cnt === 0) : false;
    const isSelf = loginUser.id === meta.user_id;
    const isPrivate = !!(meta.default_star_type & 0x10);

    if (isPrivate && !isSelf) { throw error(403, `这个收藏夹是私密的，只有 ${meta.user_name} 自己可以查看哦～`); }

    const total = (await sql<{ cnt: number; }>('SELECT COUNT(thread_id)::INT AS cnt FROM thread_stars WHERE user_id = $1 AND star_type > 0 AND $2 = ANY (fc_ids)', [meta.user_id, favId])).rows[0].cnt;
    const allFavs = (await sql<{ star_type: number; fc_ids: number[]; } & BaseThreadTarget>(`SELECT ${THREAD_RENDER_COLS.join(', ')}, star_type, fc_ids FROM thread_stars INNER JOIN thread_targets USING (thread_id) WHERE user_id = $1 AND star_type > 0 AND $2 = ANY (fc_ids) ORDER BY star_at & 7::SMALLINT DESC LIMIT $3 OFFSET $4`, [meta.user_id, favId, ctx.state.userConfig.fp, (page - 1) * ctx.state.userConfig.fp])).rows; // TODO: support different order-by options

    const maxPage = Math.ceil(total / ctx.state.userConfig.fp);
    if ((page > 1) && (page > maxPage)) {
        throw error(400, '页码不对哦～');
    }

    return finalizePage(ctx, {
        title: ctx.state.render.contentConv(meta.title),
        nav: true,
        canonicalPath: '/collection/' + favId + ((page > 1) ? '/' + page : '')
    }, templates.collection(ctx, {
        pagination: {
            baseLink: '/collection/' + favId,
            currentPage: page,
            maxPage,
            linkExtra: ''
        },
        userId: meta.user_id,
        username: meta.user_name,
        favId,
        favName: meta.title,
        level: meta.default_star_type & 0xf,
        isSelf,
        isPrivate,
        isDefault,
        threads: await renderThreads(ctx, allFavs, await tagPref(ctx)),
    }));
};

export const postCollection_id: PageImpl = async ctx => {
    const loginUser = ctx.state.mazoLoginUser;
    if (!loginUser) { throw error(403, LOGIN_REQUIRED); }
    const favId = parseInt32(ctx.params.id);
    if (!(favId > 0)) { throw error(400, '未指定收藏夹～'); } // Reject dummy collections as well as NaN.

    if (ctx.request.body.string('delete')) {
        const success = await withTransaction(async query => {
            await query('UPDATE thread_stars SET fc_ids = ARRAY_REMOVE(fc_ids, $1), star_type = CASE WHEN CARDINALITY(ARRAY_REMOVE(fc_ids, $1)) > 0 THEN star_type ELSE 0 END WHERE user_id = $2', [favId, loginUser.id]);
            return (await query('DELETE FROM fav_collections WHERE user_id = $1 AND fc_id = $2 RETURNING fc_id', [loginUser.id, favId])).rows[0];
        });
        if (!success) { throw error(404, '未找到收藏夹～'); }
        return redirectWithDelay(ctx, {
            message: '收藏夹已删除',
            targetText: '你的收藏列表',
            targetHref: '/author/' + loginUser.id + '/favs'
        });
    } else {
        // Default subscription level.
        const level = ctx.request.body.integer('level');
        if (!(level >= 0 && level <= 7)) { throw error(400, '没有这种选项！'); }
        // Visibility (0 or non-0).
        const visibility = ctx.request.body.integer('visibility');
        if (isNaN(visibility)) { throw error(400, '没有这种选项！'); }
        // Title.
        const title = ctx.request.body.string('title');
        if (title.length < 1 || title.length > 40) { throw error(400, '标题不能为空也不能长于 40 个字符哦～'); }
        // Default.
        const makeDefault = !!(ctx.request.body.string('promote'));

        const row = (await sql(`UPDATE fav_collections SET default_star_type = ($1::SMALLINT | $2::SMALLINT), title = $3 ${makeDefault ? ', display_order = COALESCE((SELECT MIN(display_order) FROM fav_collections WHERE user_id = $5 AND fc_id <> $4) - 1, display_order) ' : ''}WHERE fc_id = $4 AND user_id = $5 RETURNING display_order`, [visibility ? 0x10 : 0, level, title, favId, loginUser.id])).rows[0];
        if (row.display_order < 100) { // Display orders < 100 or > 2000 are reserved.
            unawaited(sql('UPDATE fav_collections SET display_order = LEAST(display_order + 10, 2000) WHERE user_id = $1', [loginUser.id]), 'normalizing display orders of fav collections');
        }

        if (!row) { throw error(404, '未找到收藏夹～'); }
        return redirectWithDelay(ctx, {
            message: '已成功修改收藏夹',
            targetText: '修改后的收藏夹',
            targetHref: ctx.path
        });
    }
};

export const apiCollections: HtmlSegmentImpl = async (ctx) => {
    const userInfo = ctx.state.mazoLoginUser;
    if (!userInfo) { throw error(403, LOGIN_REQUIRED); }
    return finalizeSegment(ctx, templates.collections(ctx, {
        collections: await prepareFavListForAuthor(userInfo.id, true)
    }));
};

export const postApiStar_id: RpcImpl = async (ctx) => {
    const threadId = parseInt32(ctx.params.id);
    if (isNaN(threadId)) { throw error(400, '这个网址是错的。'); }
    const userInfo = ctx.state.mazoLoginUser;
    if (!userInfo) { throw error(403, LOGIN_REQUIRED); }
    const thread = (await sql('SELECT owner_user_id FROM thread_targets WHERE thread_id = $1', [threadId])).rows[0];
    if (!thread) { throw error(404, '这个主题不存在啦！'); }

    const action = ctx.request.body.string('action');
    let rowAfterwards: { fc_ids: number[]; star_type: number; } | undefined = undefined;

    const out: { favs: number[]; level: number; options?: [number, string, number][]; option?: [number, string, number]; } = {} as any;

    const idAndDefaultType = 'fc_id, default_star_type'; // Fields needed for the var above.
    const doAdd = async (fcMeta: { fc_id: number; default_star_type: number; }) => {
        rowAfterwards = (await sql('INSERT INTO thread_stars (user_id, thread_id, star_type, star_at, fc_ids) VALUES ($1, $2, $3, $4, ARRAY[$5]::INT[]) ON CONFLICT (user_id, thread_id) DO UPDATE SET star_type = GREATEST(EXCLUDED.star_type, thread_stars.star_type), star_at = CASE WHEN CARDINALITY(thread_stars.fc_ids) > 0 THEN thread_stars.star_at ELSE EXCLUDED.star_at END, fc_ids = CASE WHEN EXCLUDED.fc_ids[1] = ANY (thread_stars.fc_ids) THEN thread_stars.fc_ids ELSE thread_stars.fc_ids || EXCLUDED.fc_ids END RETURNING fc_ids, star_type, star_at', [userInfo.id, threadId, fcMeta.default_star_type & 0xf, ctx.refDate, fcMeta.fc_id])).rows[0]!; // The GREATEST operator here also ensures blocking (0x8) take precedence over subscribing.

        if (((rowAfterwards as any).star_at === ctx.refDate) && ((rowAfterwards!.star_type & 0x7) > 1)) {
            unawaited(sql<{}, [number, number, number]>('INSERT INTO unread_status (user_id, thread_id, up_to_date, tracked_at) VALUES ($1, $2, (SELECT MAX(posted_date) FROM posts WHERE thread_id = $2 AND most_recent_update >= 0), $3) ON CONFLICT (user_id, thread_id) DO UPDATE SET up_to_date = GREATEST(unread_status.up_to_date, EXCLUDED.up_to_date), tracked_at = EXCLUDED.tracked_at', [userInfo.id, threadId, ctx.refDate]), 'read_status upon starring');
        }
    };
    const doUnsubscribe = () => unawaited(sql('DELETE FROM notifications WHERE message_type = 4 AND primary_target_id = $1 AND user_id = $2', [threadId, userInfo.id]), 'deleting type-4 notifications'); // Delete them all, even if they're read. This makes our life easier.

    switch (action) {
        case 'create': { // Add the thread to a new fav-list with a specific name.
            const userSpecifiedName = ctx.request.body.string('title'); // Provided when the user explicitly adds it to a new fav. Not provided when the user initially clicks on the fav button (which may or may not means not creating a new favlist).
            if (!userSpecifiedName || userSpecifiedName.length > 20) { throw error(400, '名字太长啦！'); }
            const count = (await sql('SELECT COUNT(fc_id)::INT AS cnt FROM fav_collections WHERE user_id = $1', [userInfo.id])).rows[0].cnt;
            if (count > 50) { throw error(403, '不可创建那么多个收藏夹啦！你已经创建了太多了～'); }
            const row = (await sql(`INSERT INTO fav_collections (user_id, title) VALUES ($1, $2) RETURNING ${idAndDefaultType}, title`, [userInfo.id, userSpecifiedName])).rows[0];
            await doAdd(row);
            out.option = [row.fc_id, row.title, row.default_star_type];
            break;
        }
        case 'auto': { // Add the thread to the default fav-list, or create one if there isn't one.
            await doAdd((await sql(`WITH existing_fc AS (SELECT ${idAndDefaultType} FROM fav_collections WHERE user_id = $1 ORDER BY display_order ASC LIMIT 1), new_fc AS (INSERT INTO fav_collections (user_id, title) SELECT $1, $2 WHERE NOT EXISTS (SELECT fc_id FROM existing_fc) RETURNING ${idAndDefaultType}) SELECT ${idAndDefaultType} FROM existing_fc UNION SELECT ${idAndDefaultType} FROM new_fc`, [userInfo.id, '默认收藏夹'])).rows[0]);
            out.options = (await sql('SELECT fc_id, title, default_star_type FROM fav_collections WHERE user_id = $1 ORDER BY display_order ASC', [userInfo.id])).rows.map(row => [row.fc_id, row.title, row.default_star_type]);
            break;
        }
        case 'append': { // Add the thread to a explicitly-specified existing fav-list.
            const rawFavId = ctx.request.body.integer('fav');
            if (isNaN(rawFavId)) { throw error(400, '未指定收藏夹。'); }
            switch (rawFavId) {
                case MY_THREADS_DUMMY_COLLECTION.fc_id:
                    if (thread.owner_user_id !== userInfo.id) { throw error(403, '不可以把其它人的主题加到这个收藏夹里～'); }
                    await doAdd(MY_THREADS_DUMMY_COLLECTION);
                    break;
                case BLOCKED_THREADS_DUMMY_COLLECTION.fc_id:
                    await doAdd(BLOCKED_THREADS_DUMMY_COLLECTION);
                    break;
                default: {
                    const fcMeta = (await sql(`SELECT ${idAndDefaultType}, user_id FROM fav_collections WHERE fc_id = $1`, [rawFavId])).rows[0];
                    if (!fcMeta) { throw error(404, '收藏夹不存在～'); }
                    if (fcMeta.user_id !== userInfo.id) { throw error(403, '不可以插到别人的收藏夹里面！'); }
                    await doAdd(fcMeta);
                }
            }
            break;
        }
        case 'remove': { // Remove the item from a fav-list.
            const rawFavId = ctx.request.body.integer('fav');
            if (isNaN(rawFavId)) { throw error(400, '未指定收藏夹。'); }
            if (rawFavId === MY_THREADS_DUMMY_COLLECTION.fc_id) {
                if (threadId > THRESHOLD_FOR_LOCAL) { throw error(403, '无法将主题从默认收藏夹中移出…'); }
            } else if (rawFavId !== BLOCKED_THREADS_DUMMY_COLLECTION.fc_id) {
                const fcMeta = (await sql(`SELECT user_id FROM fav_collections WHERE fc_id = $1`, [rawFavId])).rows[0];
                if (!fcMeta) { throw error(404, '收藏夹不存在～'); }
                if (fcMeta.user_id !== userInfo.id) { throw error(403, '不可以改别人的收藏夹！'); }
            }
            rowAfterwards = (await sql(`UPDATE thread_stars SET fc_ids = ARRAY_REMOVE(fc_ids, $1), star_type = CASE WHEN (CARDINALITY(ARRAY_REMOVE(fc_ids, $1)) > 0) THEN CASE WHEN $1 <> ${BLOCKED_THREADS_DUMMY_COLLECTION.fc_id} THEN star_type ELSE 1 END ELSE 0 END WHERE user_id = $2 AND thread_id = $3 RETURNING fc_ids, star_type`, [rawFavId, userInfo.id, threadId])).rows[0]; // This row can technically be nonexistent!
            if (!rowAfterwards || !rowAfterwards.fc_ids.length) {
                doUnsubscribe();
            }
            break;
        }
        case 'level': { // Change the subscription level of the item.
            const destLevel = ctx.request.body.integer('level');
            if (isNaN(destLevel)) { throw error(400, '未指定订阅选项～'); }
            if (!((destLevel >= 0) && (destLevel <= 7))) { throw error(400, '订阅级别不存在～'); }
            rowAfterwards = (await sql('UPDATE thread_stars SET star_type = $3 WHERE user_id = $1 AND thread_id = $2 AND star_type <> 0 AND CARDINALITY(fc_ids) > 0 RETURNING fc_ids, star_type', [userInfo.id, threadId, destLevel])).rows[0]; // This row can technically be nonexistent!
            if (destLevel === 1) { doUnsubscribe(); }
            break;
        }
        case 'view': // Do nothing: list the current status.
            rowAfterwards = (await sql('SELECT fc_ids, star_type FROM thread_stars WHERE user_id = $1 AND thread_id = $2', [userInfo.id, threadId])).rows[0];
            out.options = (await sql('SELECT fc_id, title, default_star_type FROM fav_collections WHERE user_id = $1 ORDER BY display_order ASC', [userInfo.id])).rows.map(row => [row.fc_id, row.title, row.default_star_type]);
            break;
        default:
            throw error(400, '鬼知道你要做什么！');
    }

    out.favs = rowAfterwards ? rowAfterwards.fc_ids : [];
    out.level = rowAfterwards ? rowAfterwards.star_type & 0xf : 0;

    ctx.body = out;
};
