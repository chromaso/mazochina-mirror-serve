export const CRAWLER_403 = 'TOPIC_NOT_AUTHORISED';
export const CRAWLER_404 = 'TOPIC_NOT_EXIST';

export type ThreadPageRawResult = {
    forumId: number,
    posts: {
        id: number,
        title: string,
        userId: number,
        username: string,
        date: number,
        lastEdit: number,
        contentText: string,
        content: string // HTML
    }[],
    title: string,
    page: number,
    poll: string | null
};

export type PostPageRawResult = {
    userName: string,
    postTime: number,
    userId: number,
    content:string
};