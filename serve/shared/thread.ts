import { LENGTH_THRESHOLD_FOR_UPDATING_SORTPOST_DATE } from '../../common/consts';
import { threadNeedsSpecialSortDateCalc } from '../../common/logic_utils';
import { query as sql } from '../db';

// Update `lastpost_date`, `sortpost_date`, `lastpost_id`, `lastpost_user_id` and `post_count` of a thread row.
// Usually called after a post deletion in the thread.
export const updateThreadTargetsRow = async (txn: typeof sql, threadId: number) => {
    const threadRow = (await txn('UPDATE thread_targets SET post_count = (SELECT COUNT(post_id) FROM posts WHERE thread_id = $1 AND posts.most_recent_update >= 0), lastpost_date = lastquery.posted_date, lastpost_id = lastquery.post_id, lastpost_user_id = lastquery.user_id FROM (SELECT post_id, posted_date, user_id FROM posts WHERE thread_id = $1 AND posts.most_recent_update >= 0 ORDER BY posted_date DESC, post_id DESC LIMIT 1) AS lastquery WHERE thread_id = $1 RETURNING thread_id, flags, spam_likeness', [threadId])).rows[0];
    if (await threadNeedsSpecialSortDateCalc(threadRow, txn)) {
        await txn(`UPDATE thread_targets SET sortpost_date = (SELECT posted_date FROM posts WHERE thread_id = $1 AND posts.most_recent_update >= 0 AND LENGTH(REGEXP_REPLACE(SUBSTRING(content_sc FROM POSITION('\ufffc' IN content_sc) + 1), '\\s+', '', 'g')) > $2 ORDER BY posted_date DESC, post_id DESC LIMIT 1) WHERE thread_id = $1`, [threadId, LENGTH_THRESHOLD_FOR_UPDATING_SORTPOST_DATE]);
    } else {
        await txn('UPDATE thread_targets SET sortpost_date = lastpost_date WHERE thread_id = $1', [threadId]);
    }
};