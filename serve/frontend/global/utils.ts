import { Types } from '../types';
import { $ } from './dollar';

const printDebugLog = !!localStorage.getItem('mazomirror-debug');

export const formatTz = (tz: number) => 'UTC' + (tz ? ((tz > 0) ? '+' : '') + tz : '');

export const debugLog: Types['debugLog'] = text => { if (printDebugLog) { console.log('MM [' + Math.floor(performance.now()) + ']: ' + text); } };

export const getThreadIdFromUri: Types['getThreadIdFromUri'] = () => {
    const threadIdMatch = location.pathname.match(/^\/thread\/(\d+)(\/\d+)?$/);
    if (!threadIdMatch) { debugLog('Unknown thread ID'); return 0; }
    return parseInt(threadIdMatch[1]);
};

export const loadingSpinner: Types['loadingSpinner'] = (inline, text = '加载中') =>
    inline ? $('span', undefined, [$('div', 'spinner-border spinner-border-sm'), $('span', 'ms-1', text)]) : $('div', 'text-center', [
        $('div', 'spinner-border'),
        $('div', 'mt-1', text)
    ]);