import { query as sql, parameterizedQuery as parameterizedSql } from '../db';
import { SPAM_THRESHOLD } from '../../common/consts';
import { CONV_SC as toSc } from '../chinese';
import { unawaited } from '../../common/logic_utils';
import { FullConfig as Config } from './options';
import { error } from '../utils/render_utils';
import { F18_CONSTS, TagLite, allDescedentTags, allTagsById } from './tags';

// Time duration constants, in seconds.
const STOP_WAITING_AFTER = 150; // Abondon a search if no results become available after 2.5 minutes.
const QUERY_VALID_FOR = 900; // Do not search again (and use the cache) within 900 seconds.
const KEEP_RESULTS_FOR = 172800; // 48 hours. Delete afterwards to keep the table thin.

export const RESULT_LIMIT_NO_USER_TERM = 400;
export const RESULT_LIMIT_WITH_USER_TERM = 2500;

type Flags = {
    // Error bit not included.
    post: boolean; // Instead of thread.
    sortByOrigin: boolean; // Only applies to threads.
    includeSpam: boolean;
};

export type TagMicro = { id: number, name: string };

const normalize = (val: string): string => toSc(val.toLowerCase().trim());
const sleep = (ms: number) => new Promise(res => setTimeout(res, ms));

export const waitForResult = async (
    id: number, // query_id
    pageNumber: number, // 1-indexed
    userConfig: Config
): Promise<{
    terms: string,
    user_id: number,
    forum_id: number,
    tags: number[] | null,
    flags: number,
    num_results: number,
    result_page: number[]
}> => {
    let sleepTime = 50; // Milliseconds.
    for (let sleptCount = 0; sleptCount < 14; sleptCount++) { // Total sleep: ~29 seconds
        const nullCheckResult = (await sql<{ flags: number, has_results: boolean }>('SELECT flags, results IS NOT NULL AS has_results FROM search_queries WHERE query_id = $1', [id])).rows[0];
        if (!nullCheckResult) { throw error(410, '搜索结果过期喇，请重新搜索～'); }
        if (nullCheckResult.has_results) {
            if (nullCheckResult.flags & 0x1) {
                throw error(500, '搜索失败了…非常抱歉！若持续出现此错误请联系管理员。');
            }
            const isPost = !!(nullCheckResult.flags & 0x2);
            const pageSize = isPost ? userConfig.tp : userConfig.fp;
            return (await sql('SELECT terms, user_id, forum_id, tags, flags, ARRAY_LENGTH(results, 1) AS num_results, results[$1:$2] AS result_page FROM search_queries WHERE query_id = $3', [(pageNumber - 1) * pageSize + 1, pageNumber * pageSize, id])).rows[0];
        }
        await sleep(sleepTime);
        sleepTime *= 1.5;
    }
    throw error(504, '搜索还在进行中…请刷新试试看有没有完成～');
};

const lookupTag = async (tagNamePhrase: string): Promise<number[]> => {
    const expand = tagNamePhrase.charAt(0) !== '#';
    const tagName = expand ? tagNamePhrase : tagNamePhrase.substring(1);
    const allTags = await allTagsById();
    let tagFound: TagLite | undefined;
    for (const tag of allTags.values()) {
        if (normalize(tag.primary_name) === tagName) { tagFound = tag; break; }
        if (tag.aliases?.some(alias => normalize(alias) === tagName)) { tagFound = tag; break; }
    }
    if (!tagFound) { throw error(404, `没有「${tagName}」这个标签～`); }
    return expand ? Array.from(allDescedentTags(tagFound)) : [tagFound.tag_id];
};

type TagCondition = {
    include: number[] | undefined;
    exclude: number[] | undefined;
};

export const lookupOrCreateQueryJob = async (
    keywords: string[], // not normalized. it WILL be altered in-place for normalization and tag removal!
    userId: number, // may be 0
    forumId: number, // may be 0
    flags: number, // pre-sanitized, see schema.sql for definition
    now: number,
    initiator: number
): Promise<{ id: number, parsedTags: TagCondition, reuse: boolean }> => {
    // Step 0: normalize keywords and extract tags.
    const normalizedKeywordsIncludingTags = Array.from(new Set(keywords.map(normalize).filter(_ => _)));
    const actualKeywords: string[] = [];
    let excludeTags: Set<number> | undefined;
    let includeTags: Set<number> | undefined;
    for (const keyword of normalizedKeywordsIncludingTags) {
        if (keyword.startsWith('-#')) {
            if (!excludeTags) { excludeTags = new Set(); }
            (await lookupTag(keyword.substring(2))).forEach(tag => excludeTags!.add(tag));
        } else if (keyword.startsWith('#')) {
            if (!includeTags) { includeTags = new Set(); }
            (await lookupTag(keyword.substring(1))).forEach(tag => includeTags!.add(tag));
        } else {
            actualKeywords.push(keyword);
        }
    }
    keywords.splice(0, keywords.length, ...actualKeywords.sort());
    const keywordsString = keywords.join(' ');
    // Step 0.5: normalize tags.
    let tags: number[] | null = null;
    if (excludeTags || includeTags) {
        tags = excludeTags ? Array.from(excludeTags).map(tag => -tag) : [];
        excludeTags?.forEach(tag => includeTags?.delete(tag));
        includeTags?.forEach(tag => tags!.push(tag));
        tags.sort((a, b) => a - b);
    }
    const parsedTags = {
        include: includeTags?.size ? Array.from(includeTags) : undefined,
        exclude: excludeTags ? Array.from(excludeTags) : undefined
    };
    // Step 1: look for exact match.
    const existing = (await sql('SELECT query_id, results IS NOT NULL AS has_results FROM search_queries WHERE flags = $1 AND terms = $2 AND user_id = $3 AND forum_id = $4 AND tags = $5 ORDER BY query_id DESC LIMIT 1', [flags, keywordsString, userId, forumId, tags])).rows[0];
    if (existing && ((existing.query_id >= now - STOP_WAITING_AFTER) || ((existing.query_id >= now - QUERY_VALID_FOR) && existing.has_results))) {
        return { id: existing.query_id, reuse: true, parsedTags };
    }
    // Step 2: check the current "queue".
    const current = (await sql('SELECT MAX(query_id) AS max_id FROM search_queries')).rows[0];
    if (current.max_id > now + 3) { // Allow a offset (between ID and timestamp) of < 5 seconds.
        throw new Error('现在使用搜索功能的人数过多，请稍后再试～');
    }
    const inserted = (await sql('INSERT INTO search_queries (query_id, flags, terms, user_id, forum_id, tags, initiator) VALUES (GREATEST($1, (SELECT MAX(query_id) FROM search_queries) + 1), $2, $3, $4, $5, $6, $7) RETURNING query_id', [now, flags, keywordsString, userId, forumId, tags, initiator])).rows[0]; // So we did have a `duplicate key value violates unique constraint "search_queries_pkey"` error thrown from this line. Might be a race condition. Such cases are very rare so we leave it to be thrown into a HTTP 500.
    unawaited(sql('DELETE FROM search_queries WHERE query_id < $1', [now - KEEP_RESULTS_FOR]), 'deleting outdated searches');
    return { id: inserted.query_id, reuse: false, parsedTags };
};

const executeWithHandling = (id: number, promise: Promise<unknown>): void => {
    console.time(`Search ${id}`);
    unawaited(promise.then(() => console.timeEnd(`Search ${id}`), err => {
        console.error(`Search ${id} failed`, err);
        return sql('UPDATE search_queries SET flags = flags | 1 WHERE query_id = $1', [id]);
    }), `marking search ${id} failed`);
};

const addParams = (forumId: number, includeSpam: boolean, parsedTags: TagCondition, conditions: string[], param: (value: any) => string) => {
    if (forumId) {
        conditions.push(`forum_id = ${param(forumId)}`);
    }
    if (!includeSpam) {
        conditions.push(`spam_likeness < ${SPAM_THRESHOLD}`);
    }
    if (parsedTags.include) {
        conditions.push(`(forum_id = 18 AND EXISTS (SELECT tagass_id FROM tag_association WHERE tag_association.thread_id = thread_targets.thread_id AND tag_id = ANY(${param(parsedTags.include)}::INT[]) AND tag_score > ${F18_CONSTS.CONSTS.SCORE_MULTIPLIER}))`);
    }
    if (parsedTags.exclude) {
        conditions.push(`NOT (forum_id = 18 AND EXISTS (SELECT tagass_id FROM tag_association WHERE tag_association.thread_id = thread_targets.thread_id AND tag_id = ANY(${param(parsedTags.exclude)}::INT[]) AND tag_score > ${F18_CONSTS.CONSTS.SCORE_MULTIPLIER}))`);
    }
    return conditions;
};

export const executeSearchOnThreads = (
    id: number,
    keywords: string[], // not normalized, but excluded empty strings. it WILL be altered in-place for normalization!
    userId: number, // may be 0
    forumId: number, // may be 0
    tags: TagCondition,
    sortByOrigin: boolean,
    includeSpam: boolean
): void =>
    executeWithHandling(id, parameterizedSql(param => {
        const conditions = keywords.map(keyword => `POSITION(${param(keyword)} IN thread_title_sc) != 0`);
        if (userId) {
            conditions.push(`owner_user_id = ${param(userId)}`);
        }
        addParams(forumId, includeSpam, tags, conditions, param);
        return `UPDATE search_queries SET results = ARRAY(SELECT thread_id FROM thread_targets WHERE last_page_number > 0 AND successive_fail >= 0${conditions.length ? ' AND ' + conditions.join(' AND ') : ''} ORDER BY ${sortByOrigin ? 'firstpost_date' : 'lastpost_date'} DESC LIMIT ${userId ? RESULT_LIMIT_WITH_USER_TERM : RESULT_LIMIT_NO_USER_TERM}) WHERE query_id = ${param(id)}`;
    }));

export const executeSearchOnPosts = (
    id: number,
    keywords: string[], // not normalized, but excluded empty strings. it WILL be altered in-place for normalization!
    userId: number, // may be 0
    forumId: number, // may be 0
    tags: TagCondition,
    includeSpam: boolean
): void =>
    executeWithHandling(id, parameterizedSql(param => {
        const conditions = keywords.map(keyword => `POSITION(${param(keyword)} IN content_sc) != 0`);
        if (userId) {
            conditions.push(`user_id = ${param(userId)}`);
        }
        addParams(forumId, includeSpam, tags, conditions, param);
        return `UPDATE search_queries SET results = ARRAY(SELECT post_id FROM posts INNER JOIN thread_targets USING (thread_id) WHERE last_page_number > 0 AND successive_fail >= 0 AND most_recent_update >= 0${conditions.length ? ' AND ' + conditions.join(' AND ') : ''} ORDER BY posted_date DESC, post_id DESC LIMIT ${userId ? RESULT_LIMIT_WITH_USER_TERM : RESULT_LIMIT_NO_USER_TERM}) WHERE query_id = ${param(id)}`;
    }));