import { stripIastOff, parseBBCode } from "../../common/bbcode";
import { query as sql } from "../db";
import { processContentScForQuote } from "../shared/post";
import { MazoState } from "./logic_utils";

export const getReplyToBBCode = async (state: MazoState, post: { rev_id: number; flags: number; content_sc: string; }): Promise<string> => {
    if (post.flags & 0x2) {
        return state.render.staticConv('[回复可见]');
    } else if (post.rev_id) {
        const sourceBBCode = await sql('SELECT content FROM local_post_log WHERE rev_id = $1', [post.rev_id]).then(({ rows }) => rows[0].content).catch(e => (console.error(e), '[读取失败]'));
        return (post.flags & 0x4) ? stripIastOff(sourceBBCode, `rev_id ${post.rev_id}`) : sourceBBCode;
    } else { // Remote post, surely won't have any flags.
        return processContentScForQuote(post.content_sc);
    }
};

const getAlternativeReplyToBBCodes = async (state: MazoState, postId: number, exclRevId: number): Promise<string[]> => {
    const { rows } = (await sql<{ rev_id: number, flags: number, content: string }>('SELECT rev_id, flags, content FROM local_post_log WHERE post_id = $1 AND rev_id <> $2', [postId, exclRevId]));
    return rows.map(row => {
        if (row.flags & 0x2) {
            return state.render.staticConv('[回复可见]');
        }
        return (row.flags & 0x4) ? stripIastOff(row.content, `rev_id ${row.rev_id}`) : row.content;
    });
};

// Returns the length of text except for quoted sections.
export const checkQuoteItemValidity = async (state: MazoState, { quotes, lines }: ReturnType<typeof parseBBCode>, replyToPost?: { post_id: number; title: string; content_sc: string; user_id: number; user_name: string; thread_id: number; rev_id: number; flags: number; }): Promise<number> => {
    let outsideQuote = '';
    let lastLineNo = 0;
    let lastColNo = 0;
    for (const quote of quotes) {
        const lineNoOfStartingTag = quote.start[0];
        if (lineNoOfStartingTag > lastLineNo) {
            // Finish the partly-done line.
            outsideQuote += lines[lastLineNo].substring(lastColNo);
            for (let i = lastLineNo + 1; i < lineNoOfStartingTag; i++) {
                // Complete lines between them.
                outsideQuote += lines[i];
            }
        }
        outsideQuote += lines[lineNoOfStartingTag].substring((lastLineNo < lineNoOfStartingTag) ? 0 : lastColNo, quote.start[1]); // The start of this line, or the in-bet'ween thing if we're on the same line.

        if (quote.postId) { // Check content only when a post ID is available.
            const lineNoOfEndingTag = quote.end[0];
            let insideQuote = lines[lineNoOfStartingTag].substring(quote.start[2], (lineNoOfStartingTag < lineNoOfEndingTag) ? undefined : quote.end[1]);
            if (lineNoOfEndingTag > lineNoOfStartingTag) {
                insideQuote = lines[lineNoOfStartingTag].substring(quote.start[2]);
                for (let i = lineNoOfStartingTag + 1; i < lineNoOfEndingTag; i++) {
                    insideQuote += lines[i];
                }
                insideQuote += lines[lineNoOfEndingTag].substring(0, quote.end[1]);
            }
            const post = (replyToPost && (quote.postId === replyToPost.post_id)) ? replyToPost : (await sql<{ post_id: number; title: string; content_sc: string; user_id: number; user_name: string; thread_id: number; rev_id: number; flags: number; }, [number]>('SELECT post_id, title, content_sc, posts.user_id, users.user_name, thread_id, rev_id, posts.flags FROM posts LEFT JOIN users USING (user_id) WHERE post_id = $1 AND (posts.most_recent_update >= 0 OR posts.flags & 16 <> 0)', [quote.postId])).rows[0];
            if (!post) { throw new RangeError(`试图回复不存在的文章 ${quote.postId}`); }
            if (post.user_id !== quote.authorId) { throw new RangeError(`你要回复的文章并非该作者所发布`); }
            if (post.user_name !== quote.authorName) { throw new RangeError(`你要回复的文章并非该「${quote.authorName}」所发布`); }

            const replyTo = await getReplyToBBCode(state, post);

            const isSubstr = replyTo.replace(/\s/g, '').includes(insideQuote.replace(/\s/g, ''));
            if (!isSubstr) {
                const alternatives = await getAlternativeReplyToBBCodes(state, post.post_id, post.rev_id);
                if (!alternatives.some(alternative => alternative.replace(/\s/g, '').includes(insideQuote.replace(/\s/g, '')))) {
                    throw new RangeError(`你引用的内容并非原文章的一部分（引用时你只可以不加修改地引用原文章或其一部分）`);
                }
            }
        } else {
            throw new RangeError('未提供引用的文章 ID'); // maybe we should tolerate this?
        }

        lastLineNo = quote.end[0];
        lastColNo = quote.end[2];
    }
    outsideQuote += lines[lastLineNo].substring(lastColNo);
    for (let i = lastLineNo + 1; i < lines.length; i++) {
        outsideQuote += lines[i];
    }
    return outsideQuote.replace(/\s/g, '').length;
};
