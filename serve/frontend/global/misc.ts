export const goToPost = (postId: number) => location.assign((document.getElementById('p' + postId) ? '#p' : '/post/') + postId);

export const initSearchCheckbox = () => {
    const [radioThread, radioPost, sortingOptions] = ['entity-type-thread', 'entity-type-post', 'sorting-options'].map(t => document.getElementById(t)!);
    radioThread.addEventListener('click', () => {
        sortingOptions.style.display = 'block'
    });
    radioPost.addEventListener('click', () => {
        sortingOptions.style.display = 'none'
    });
    if ((radioPost as HTMLInputElement).checked) {
        sortingOptions.style.display = 'none'
    };
};
