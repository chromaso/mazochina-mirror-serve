import { QueryResult } from 'pg';
import { rowsToMap, rowsToMultiMap } from '../../common/logic_utils';
import { parameterizedQuery, query as sql } from '../db';
import { MazoKoaContext } from '../utils/logic_utils';
import { UserInfo } from './login';

// Campaign ID that's still open for new thread submission.
const OPEN_CAMPAIGN_TAG_ID = 0;

// Campaign ID that's still open for voting/panel-commenting.
const VOTE_CAMPAIGN_TAG_ID = 0;

const CATS = Object.freeze({
    // Mandatory fields.
    LENGTH: 1,
    SOURCE: 2,
    // Other non-fetish.
    _FIRST_ARBITRARY: 4,
    GENRE: 4,
    META: 5,
    CHARACTER: 6,
    ROLE: 7,
    CAMPAIGN: 8,
    // Fetish.
    _FIRST_FETISH: 12,
    SEX: 12,
    PODO: 13,
    TORTURE: 14,
    GENERAL: 15,
});

export type CategoryId = (typeof CATS)[(keyof typeof CATS)];

export const CAT_TITLES = Object.freeze(((init: string[]) => {
    init[CATS.LENGTH] = '篇幅';
    init[CATS.SOURCE] = '来源';
    init[CATS.GENRE] = '题材';
    init[CATS.META] = '形式';
    init[CATS.CHARACTER] = '人设';
    init[CATS.ROLE] = '关系';
    init[CATS.CAMPAIGN] = '活动';
    init[CATS.SEX] = '性癖：性';
    init[CATS.PODO] = '性癖：足';
    init[CATS.TORTURE] = '性癖：刑';
    init[CATS.GENERAL] = '性癖：其它';
    return init;
})([]));

const ARBITRARY_CAT_PRIORITY = Object.freeze(((init: number[]) => {
    init[CATS.GENRE] = 0.5;
    init[CATS.META] = 1;
    init[CATS.CHARACTER] = 0;
    init[CATS.ROLE] = 0.5;
    init[CATS.CAMPAIGN] = 10;
    init[CATS.SEX] = 0;
    init[CATS.PODO] = 0;
    init[CATS.TORTURE] = 0;
    init[CATS.GENERAL] = 0;
    return init;
})([]));

const CONSTS = Object.freeze({
    SCORE_MULTIPLIER: (1 << 20) - 1,
    CACHE_PERIOD: 1800e3, // 30 minutes
    STYLES: [1, 2, 3, 4, 5, 6] as TagDisplayStyle[],
    README_THREAD_ID: 1073746588
});

export const F18_CONSTS = Object.freeze({
    CATS,
    CAT_TITLES,
    CONSTS,
    OPEN_CAMPAIGN_TAG_ID,
    VOTE_CAMPAIGN_TAG_ID
});

export type TagDisplayStyle = 1 | 2 | 3 | 4 | 5 | 6;

export type TagCatOrder = 1 | 2 | 3;

export type TagsDisplayLimit = 1 | 2 | 3; // each means [inf, 10, 5] (see below)

export const LIMIT_MEANINGS = [0, 0xff, 10, 5];

type UserTagsPrefDAO = {
    visible: boolean,
    warning: Set<number>,
    style: TagDisplayStyle, 
    catOrder: TagCatOrder,
    nowrap: boolean,
    limit: TagsDisplayLimit 
};

const defaultWarningTags: Promise<Set<number>> = sql<{ tag_id: number }>('SELECT tag_id FROM tags WHERE (flags & 1) <> 0').then(({ rows }) => new Set(rows.map(({ tag_id }) => tag_id)));

const tagConfigForUser = async (userId: number): Promise<UserTagsPrefDAO | undefined> => {
    const row = (await sql<{ warning: number[], flags: number }>('SELECT warning, flags FROM tags_user_pref WHERE user_id = $1', [userId])).rows[0];
    if (!row) { return undefined; }
    let warning = (await defaultWarningTags);
    if (row.warning) {
        warning = new Set(warning);
        for (const tagId of row.warning) {
            if (tagId > 0) { warning.add(tagId); } else { warning.delete(-tagId); }
        }
    }
    const style = ((row.flags >> 1) & 0x7) as TagDisplayStyle;
    const catOrder = (row.flags >> 4) & 0x3;
    const limit = (row.flags >> 9) & 0x3;
    return {
        visible: !!(row.flags & 0x1),
        warning,
        style: CONSTS.STYLES.includes(style) ? style : 1,
        catOrder: ((catOrder >= 1) && (catOrder <= 3)) ? (catOrder as 1 | 2 | 3) : 1,
        nowrap: !!((row.flags >> 8) & 0x1),
        limit: ((limit >= 1) && (limit <= 3)) ? (limit as 1 | 2 | 3) : 2
    };
};

export type TagLite = {
    tag_id: number;
    primary_name: string;
    aliases: string[] | null;
    parentTags: [TagLite, ...TagLite[]] | null;
    childTags: [TagLite, ...TagLite[]] | null;
    category: CategoryId;
    desc_html: string;
    hint?: string;
    flags: number;
    specificity: number; // Normalized between 0 - 1, with branch (non-leaf non-root) nodes only in 0.3 - 0.8.
};

let tagCacheExpire = 0;
let tagMapPromise: Promise<Map<number, TagLite>>;

export const allTagsById = (): (typeof tagMapPromise) => {
    const refTime = Date.now();
    if (refTime > tagCacheExpire) {
        tagCacheExpire = refTime + CONSTS.CACHE_PERIOD; // 20 minutes cache.
        tagMapPromise = Promise.all([
            sql<TagLite>('SELECT tag_id, primary_name, aliases, category, desc_html, flags FROM tags ORDER BY flags DESC'),
            sql<{ parent_tag: number, child_tag: number }>('SELECT parent_tag, child_tag FROM tags_taxonomy')
        ]).then(([{ rows }, { rows: taxoRows }]) => {
            const maxOrderFlag = Math.max(...rows.map(row => row.flags & 0x1ff8));
            const maxMinusMinOrderFlag = maxOrderFlag - Math.min(...rows.map(row => row.flags & 0x1ff8));
            const out = rowsToMap(rows.map(tag => {
                let hint = tag.desc_html;
                if (tag.aliases) {
                    hint += '（别名：' + tag.aliases.join('、') + '）';
                }
                tag.hint = hint;
                const orderFlag = tag.flags & 0x1ff8;
                tag.specificity = 0.5 * (maxOrderFlag - orderFlag) / maxMinusMinOrderFlag + 0.3;
                return tag;
            }), 'tag_id');
            for (const { parent_tag, child_tag } of taxoRows) {
                const parent = out.get(parent_tag)!;
                const child = out.get(child_tag)!;
                if (parent.childTags) { parent.childTags.push(child); } else { parent.childTags = [child]; }
                if (child.parentTags) { child.parentTags.push(parent); } else { child.parentTags = [parent]; }
            }
            for (const tag of rows) {
                if (!tag.childTags) { // Leaf or standalone node.
                    tag.specificity += 0.2;
                } else {
                    tag.specificity -= tag.parentTags ? 0.15 : 0.3;
                }
            }
            return out;
        });
    }
    return tagMapPromise;
};

export const allTagsByCategory = async () => rowsToMultiMap((await allTagsById()).values(), 'category');

type RawVote = 1 | 0 | -1;

const SCORE_PARAM_AUTHOR_POSITIVE = 1.75;
const SCORE_PARAM_AUTHOR_NEGATIVE = -0.375;
const SCORE_PARAM_POSITIVE_OFFSET = 1;
const SCORE_PARAM_NEGATIVE_OFFSET = 1.5;

// Caller must ensure tag exists.
// refTime must be -1 when creating a thread.
export const voteForThread = async (threadRow: { thread_id: number, owner_user_id: number }, tagId: number, user: UserInfo, refTime: number, vote: RawVote, superVote: boolean): Promise<{ tagass_id: number, tag_score: number }> => {
    const isAuthor = user.id === threadRow.owner_user_id;
    const markRatio = (superVote && user.admin) ? 1000 : (user.confirmed ? 8 : 3);
    let attemptScore = Math.log(((vote > 0) ? markRatio : 0) + SCORE_PARAM_POSITIVE_OFFSET) - Math.log(((vote < 0) ? markRatio : 0) + SCORE_PARAM_NEGATIVE_OFFSET);
    let authorScore = 0;
    if (isAuthor && (vote !== 0)) {
        authorScore = (vote > 0) ? SCORE_PARAM_AUTHOR_POSITIVE : SCORE_PARAM_AUTHOR_NEGATIVE;
        attemptScore += authorScore;
    }
    const effectiveVote = vote * markRatio;

    const returnRow = (await parameterizedQuery<{ tagass_id: number, tag_score: number }>(addParam => 
        `INSERT INTO tag_association (thread_id, tag_id, tag_score) VALUES (
            ${addParam(threadRow.thread_id)},
            ${addParam(tagId)},
            ROUND(${addParam(attemptScore)}::REAL * ${CONSTS.SCORE_MULTIPLIER})::INT & ~3 | ${addParam((isAuthor && vote > 0) ? 1 : 0)}
        ) ON CONFLICT (thread_id, tag_id) DO UPDATE SET tag_score = (
            ROUND((
                ${isAuthor ? addParam(authorScore) : `(CASE (SELECT SIGN(vote) FROM tag_votes WHERE tag_votes.tagass_id = tag_association.tagass_id AND user_id = ${addParam(threadRow.owner_user_id)}) WHEN 1 THEN ${SCORE_PARAM_AUTHOR_POSITIVE} WHEN -1 THEN ${SCORE_PARAM_AUTHOR_NEGATIVE} ELSE 0 END)`} +
                LN(${addParam((vote > 0) ? markRatio : 0)} + COALESCE((SELECT SUM(vote) FROM tag_votes WHERE tag_votes.tagass_id = tag_association.tagass_id AND user_id <> ${addParam(user.id)} AND vote > 0), 0) + ${SCORE_PARAM_POSITIVE_OFFSET}) -
                LN(${addParam((vote < 0) ? markRatio : 0)} - COALESCE((SELECT SUM(vote) FROM tag_votes WHERE tag_votes.tagass_id = tag_association.tagass_id AND user_id <> ${addParam(user.id)} AND vote < 0), 0) + ${SCORE_PARAM_NEGATIVE_OFFSET})
            ) * ${CONSTS.SCORE_MULTIPLIER})::INT & ~3 | (${isAuthor ? addParam((vote > 0) ? 1 : 0) : 'tag_association.tag_score & 3'})
        ) RETURNING tagass_id, tag_score`
    )).rows[0];
    await sql('INSERT INTO tag_votes (tagass_id, user_id, voted_at, vote, updated_at) VALUES ($1, $2, $3, $4, $3) ON CONFLICT (tagass_id, user_id) DO UPDATE SET vote = EXCLUDED.vote, updated_at = EXCLUDED.updated_at', [returnRow.tagass_id, user.id, refTime, effectiveVote]);
    return returnRow;
};

export const tagPref = async (ctx: MazoKoaContext): Promise<UserTagsPrefDAO> => {
    const userInfo = ctx.state.mazoLoginUser;
    const userPref = userInfo ? (await tagConfigForUser(userInfo.id)) : undefined;
    return userPref ?? {
        visible: true,
        warning: await defaultWarningTags,
        style: 1,
        catOrder: 1,
        limit: 2,
        nowrap: false
    };
};

export const tagPrefUpdateFlags = async (mask: number, value: number, userId: number) =>
    await sql('INSERT INTO tags_user_pref (user_id, flags) VALUES ($3, $2) ON CONFLICT (user_id) DO UPDATE SET flags = tags_user_pref.flags & ~$1::SMALLINT | EXCLUDED.flags', [mask, value, userId]);

// Caller's responsibility to validate parameters.
// Returns true on success, false on failure.
export const tagPrefUpdateWarn = async (userId: number, tagId: number, warnOn: boolean): Promise<boolean> => {
    const defaultSettings = await defaultWarningTags;
    let update: QueryResult;
    if (defaultSettings.has(tagId) === warnOn) { // Same as default conf.
        update = await sql('UPDATE tags_user_pref SET warning = NULLIF(ARRAY(SELECT UNNEST(COALESCE(warning, ARRAY[]::INT[])) EXCEPT SELECT $1 EXCEPT SELECT 0 - $1), ARRAY[]::INTEGER[]) WHERE user_id = $2', [tagId, userId]);
    } else if (warnOn) { // Override to on.
        update = await sql('UPDATE tags_user_pref SET warning = NULLIF(ARRAY(SELECT UNNEST(COALESCE(warning, ARRAY[]::INT[])) EXCEPT SELECT 0 - $1 UNION SELECT $1), ARRAY[]::INTEGER[]) WHERE user_id = $2', [tagId, userId]);
    } else { // Override to off.
        update = await sql('UPDATE tags_user_pref SET warning = NULLIF(ARRAY(SELECT UNNEST(COALESCE(warning, ARRAY[]::INT[])) EXCEPT SELECT $1 UNION SELECT 0 - $1), ARRAY[]::INTEGER[]) WHERE user_id = $2', [tagId, userId]);
    }
    return !!update.rowCount;
}

export const allDescedentTags = (tag: TagLite): Set<number> => {
    const retVal = new Set([tag.tag_id]);
    if (tag.childTags) {
        const doChild = (item: TagLite) => {
            if (retVal.has(item.tag_id)) { return; }
            retVal.add(item.tag_id);
            item!.childTags?.forEach(doChild);
        };
        tag.childTags.forEach(doChild);
    }
    return retVal;
};

export const searchSetsToTerms = async (include: Set<number>, exclude: Set<number>): Promise<string[]> => {
    const outputs = [];

    // Handle exclude first.
    // For each tag, add flag (all my descedents are here). If it's the case, all descedents can be removed.
    const tagList = await allTagsById();

    const skipThoseExcludes = new Set<number>();
    for (const id of exclude) {
        const tag = tagList.get(id)!;
        if (skipThoseExcludes.has(id)) { continue; }
        if (tag.childTags) {
            let allDescedentsAlreadyThere = true;
            const descedents = allDescedentTags(tag);
            for (const descedent of descedents) {
                if (!exclude.has(descedent)) {
                    allDescedentsAlreadyThere = false;
                    break;
                }
            }
            if (!allDescedentsAlreadyThere) {
                // If not all descedents are excluded: it must be directly excluded; it cannot be transitively excluded by its parent.
                outputs.push('-##' + tag.primary_name);
                skipThoseExcludes.add(id); // No need to consider again.
            } else {
                // All descedents are there! Those descedents can be deleted.
                for (const descedent of descedents) {
                    if (descedent !== id) { skipThoseExcludes.add(descedent); }
                }
            }
        }
    }
    for (const id of exclude) {
        if (!skipThoseExcludes.has(id)) { outputs.push('-#' + tagList.get(id)!.primary_name); }
    }

    const skipThoseIncludes = new Set<number>();
    for (const id of include) {
        const tag = tagList.get(id)!;
        if (skipThoseIncludes.has(id)) { continue; }
        if (tag.childTags) {
            let allDescedentsAlreadyThere = true;
            const descedents = allDescedentTags(tag);
            for (const descedent of descedents) {
                if (!include.has(descedent) && !exclude.has(descedent)) {
                    allDescedentsAlreadyThere = false;
                    break;
                }
            }
            if (!allDescedentsAlreadyThere) {
                // If not all descedents are included: it must be directly included; it cannot be transitively includes by its parent.
                outputs.push('##' + tag.primary_name);
                skipThoseIncludes.add(id); // No need to consider again.
            } else {
                // All descedents are there! Those descedents can be deleted.
                for (const descedent of descedents) {
                    if (descedent !== id) { skipThoseIncludes.add(descedent); }
                }
            }
        }
    }
    for (const id of include) {
        if (!skipThoseIncludes.has(id)) { outputs.push('#' + tagList.get(id)!.primary_name); }
    }

    return outputs.sort();
};

export type TagInfoInThreadTable = {
    link: string,
    name: string,
    warn: boolean,
    debug?: string
};

// Caller is responsible for ensuring perf.enabled is true.
export const displayInThreadsTable = (tagIdTagScorePairs: [number, number][], pref: UserTagsPrefDAO, tagList: Awaited<ReturnType<typeof allTagsById>>, count: number): TagInfoInThreadTable[] => {
    let topSourcePair: [TagLite, number] | undefined;
    let topLengthPair: [TagLite, number] | undefined;

    const arbitraryTags: [TagLite, number, string][] = []; // Tag, score, score-debug-info.

    for (const [tagId, dbScore] of tagIdTagScorePairs) {
        const tag = tagList.get(tagId);
        if (!tag) { console.error('Should never happen: tag ' + tagId + ' unknown.'); continue; }
        if (tag.category === CATS.SOURCE) {
            if (!topSourcePair || (dbScore > topSourcePair[1])) {
                topSourcePair = [tag, dbScore];
            }
        } else if (tag.category === CATS.LENGTH) {
            if (!topLengthPair || (dbScore > topLengthPair[1])) {
                topLengthPair = [tag, dbScore];
            }
        } else if (tag.category >= CATS._FIRST_ARBITRARY) {
            const priorityScore = (ARBITRARY_CAT_PRIORITY[tag.category] ?? 0) * 0.5; // (1, 0.5 or 0) * 0.5
            const normalizedScore = dbScore / CONSTS.SCORE_MULTIPLIER; // Should be > 1 for all, likely between 1 and 4.
            const specificityScore = tag.specificity * 0.25;
            const warn = pref.warning.has(tag.tag_id);
            const initialScore = normalizedScore + priorityScore + tag.specificity + (warn ? 0.5 : 0);
            arbitraryTags.push([
                tag,
                initialScore,
                `CP(${priorityScore.toFixed(2)})+TS(${normalizedScore.toFixed(3)})+SP(${specificityScore.toFixed(3)})` + (warn ? `+WN(0.5)` : '')
            ]);
        } // All situations should have been covered.
    }

    const retVal: TagInfoInThreadTable[] = [];
    const addRetVal = (tag: TagLite, debug: string) => retVal.push({
        link: '/tag/' + tag.tag_id,
        name: tag.primary_name,
        warn: pref.warning.has(tag.tag_id),
        debug
    });

    switch (pref.catOrder) {
        case 1: {
            if (topSourcePair) { addRetVal(topSourcePair[0], `CS:TS(${(topSourcePair[1] / CONSTS.SCORE_MULTIPLIER).toFixed(3)})`); }
            if (topLengthPair) { addRetVal(topLengthPair[0], `CL:TS(${(topLengthPair[1] / CONSTS.SCORE_MULTIPLIER).toFixed(3)})`); }
            break;
        }
        case 2: {
            if (topLengthPair) { addRetVal(topLengthPair[0], `CL:TS(${(topLengthPair[1] / CONSTS.SCORE_MULTIPLIER).toFixed(3)})`); }
            if (topSourcePair) { addRetVal(topSourcePair[0], `CS:TS(${(topSourcePair[1] / CONSTS.SCORE_MULTIPLIER).toFixed(3)})`); }
            break;
        }
        case 3: {
            if (topLengthPair) { addRetVal(topLengthPair[0], `CL:TS(${(topLengthPair[1] / CONSTS.SCORE_MULTIPLIER).toFixed(3)})`); }
        }
    }

    while ((retVal.length < count) && arbitraryTags.length) {
        let candidateIndex = -1;
        let candidateScoreSoFar = Number.NEGATIVE_INFINITY;
        for (let i = 0; i < arbitraryTags.length; i++) {
            const score = arbitraryTags[i][1];
            if (score > candidateScoreSoFar) {
                candidateScoreSoFar = score;
                candidateIndex = i;
            }
        }
        const candidateTag = arbitraryTags.splice(candidateIndex, 1)[0];
        addRetVal(candidateTag[0], candidateTag[2]);
        for (const entry of arbitraryTags) {
            if (entry[0].childTags?.includes(candidateTag[0])) {
                entry[1] -= 0.5;
                entry[2] += '-CT(0.5)';
            }
        }
    }

    return retVal;
};