import { parseRefExpression, FunctionName, IastInput, legalChineseName, expressionRuntime, parseIfExpression, unescapeForIast } from '../../../common/iast';
import { closeDropdownMenuIfSourceInside, findRoot as findDropdownRoot } from './pop';
import { $, $set, $xhr, updateChildAt } from './dollar';
import { getThreadIdFromUri, loadingSpinner } from './utils';
import { showModal } from './modal';

type InputItem = [
    HTMLAnchorElement,
    // Display mapped from option IDs.
    { [option: string]: HTMLButtonElement } | undefined
];
type VarItem = [
    HTMLElement,
    // Function operating on the answer, or undefined when showing the answer directly would be preferred 
    FunctionName | undefined
];
type IfItem = [HTMLElement, ReturnType<typeof parseIfExpression>];

const error = (content: string): HTMLElement =>
    $('span', 'badge bg-danger', [content]);

const editButton = (warning: boolean): HTMLElement => $('span', 'material-icons align-middle text-primary rounded-circle border border-primary p-1 mx-1', warning ? 'priority_high' : 'edit').also(icon => { icon.style.fontSize = '90%'; });

const getAllIastVars = (wrap: HTMLElement): [HTMLElement, ReturnType<typeof parseRefExpression>][] => Array.prototype.map.call(wrap.querySelectorAll('[data-iast]'), el => { 
    const dataIastSections = el.getAttribute('data-iast')!.split(';');
    if (dataIastSections[0] !== 'var') { return null; }
    const value = parseRefExpression(dataIastSections[1]);
    return [el, value];
}).filter(_ => _) as any[];

type CurrentValue = [string, string?]; // For select, it's always [escaped]; otherwise it's [escaped, display]. Escaped is empty when and only when unspecified (in which case, display will be the implicit default).

export const iastInitilize = (currentValues: Record<string, CurrentValue>, isMyThread: boolean): void => {
    const inputsById: { [key: string]: InputItem } = {};
    const ifsById: { [key: string]: IfItem[] } = {};
    const varsById: { [key: string]: VarItem[] } = {};

    const updateInputDisplay = (inputId: string): void => {
        const [a, map] = inputsById[inputId];

        const valueOrDefault = currentValues[inputId];
        let escapedValue = valueOrDefault[0];
        let displayValue = valueOrDefault[1] || unescapeForIast(valueOrDefault[0]); // The latter case should never happen. Just here to be safer.
        if (map) { // type=select
            if (escapedValue) {
                const origin = map[escapedValue];
                if (origin) {
                    const newWrap = $('span');
                    Array.prototype.forEach.call(origin.childNodes, item => newWrap.appendChild(item.cloneNode(true)));
                    // Add new items to var list.
                    getAllIastVars(newWrap).forEach(newVar => {
                        varsById[newVar[1][0]].push([newVar[0], newVar[1][1]]);
                    });
                    const oldWrap = updateChildAt(a, 0, newWrap);
                    updateChildAt(a, 1, editButton(false));
                    if (oldWrap instanceof HTMLElement) {
                        // Remove old items from var list.
                        getAllIastVars(oldWrap).forEach(oldVar => {
                            const key = oldVar[1][0];
                            varsById[key] = varsById[key].filter(pair => pair[0] !== oldVar[0]);
                        });
                    }
                    return;
                } else {
                    displayValue = `（未知：${unescapeForIast(escapedValue)}）`;
                }
            } else {
                displayValue = '（你尚未做出选择）';
            }
        }
        updateChildAt(a, 0, document.createTextNode(displayValue)); // The latter case of "||" should never happen. Just here to be safer.
        updateChildAt(a, 1, editButton(!escapedValue));
    };

    const showStatsModal = (inputId: string): void => {
        const container = $('div', void 0, loadingSpinner(false));
        const selections = inputsById[inputId][1];
        showModal(`${selections ? '选项' : '输入'}统计`, container);
        $xhr('/api/iast/' + getThreadIdFromUri() + '/' + encodeURI(inputId), {
            t: text => {
                container.innerHTML = text;
                if (selections) {
                    const tds = container.querySelectorAll('[data-option]');
                    Array.prototype.forEach.call(tds, (td: HTMLTableCellElement) => {
                        const rawOption = td.getAttribute('data-option');
                        if (!rawOption) { return; }
                        const newWrap = $('div');
                        const origin = selections[rawOption];
                        if (!origin) { return; }
                        Array.prototype.forEach.call(origin.childNodes, item => newWrap.appendChild(item.cloneNode(true)));
                        getAllIastVars(newWrap).forEach(newVar => {
                            newVar[0].style.textDecoration = 'underline dotted';
                        });
                        $set(td, newWrap);
                    });
                }
            },
            e: () => $set(container, $('div', 'alert alert-danger', '统计数据加载失败啦 °(°ˊДˋ°) °'))
        });
    };

    const evaluateIf = (parsed: ReturnType<typeof parseIfExpression>): boolean => parsed.some(
        // conjunctTerm[1] should always be unescaped at the time of BBcode -> HTML.
        disjunctTerm => disjunctTerm.every(conjunctTerm => (currentValues[conjunctTerm[0]]?.[0] === conjunctTerm[1]) !== conjunctTerm[2])
    );

    const updateIfs = (inputId: string) => {
        const list = ifsById[inputId];
        if (!list) { return; }
        list.forEach(item => {
            if (evaluateIf(item[1])) {
                item[0].style.setProperty('display', 'block', 'important');
            } else {
                item[0].style.setProperty('display', 'none');
            }
        });
    };

    const updateVars = (inputId: string) => {
        const current = currentValues[inputId];
        const latestValue = current[1] || unescapeForIast(current[0]);
        const list = varsById[inputId];
        if (!list) { return; }
        list.forEach(item => {
            const newText = document.createTextNode(item[1] ? expressionRuntime[item[1]](latestValue) : latestValue);
            item[0].replaceChild(newText, item[0].firstChild!);
        });
    };

    const update = (inputId: string, value: string, eventSource: HTMLElement): void => {
        if (!('postIast' in window)) {
            alert('登入用户才可以进行输入选择哦～');
            return;
        }
        const root = findDropdownRoot(eventSource)!;
        const loadModal = $('div', 'position-absolute text-dark text-center d-flex justify-content-center align-items-center', '正在保存中…').also(el => {
            el.style.left = '0';
            el.style.top = '0';
            el.style.right = '0';
            el.style.bottom = '0';
            el.style.background = 'rgba(255, 255, 255, 0.75)';
            el.style.zIndex = '1080';
        });
        root.appendChild(loadModal);
        window.postIast(inputId, value, res => {
            currentValues[inputId] = res;
            updateInputDisplay(inputId);
            updateIfs(inputId);
            updateVars(inputId);
            root.removeChild(loadModal);
            closeDropdownMenuIfSourceInside(root);
        }); // TODO: handle the error case: hidding the loadModal and reverting the input to initial value?
    };

    const inputForm = (value: CurrentValue, isCnm: boolean, callback: (text: string) => boolean): HTMLFormElement => {
        const inputText = $('input', {
            type: 'text',
            className: 'form-control',
            maxLength: 30,
            minLength: isCnm ? 2 : 1,
            size: isCnm ? 4 : 10
        });
        const display = value[1] || unescapeForIast(value[0]);
        if (value[0]) {
            inputText.value = display;
        } else {
            inputText.placeholder = display;
        }
        return $('form', undefined,
            $('div', 'input-group', [
                inputText,
                $('button', {
                    type: 'submit',
                    className: 'btn btn-primary'
                }, [
                    $('span', 'material-icons', 'done')
                ])
            ]), {
            'submit': e => {
                e.preventDefault();
                callback(inputText.value.trim());
            }
        });
    };

    [].forEach.call(document.querySelectorAll('[data-iast]'), (el: HTMLElement) => {
        const dataIastSections = el.getAttribute('data-iast')!.split(';');
        switch (dataIastSections[0]) {
            case 'if': {
                const disjuncts = parseIfExpression(dataIastSections[1]);
                const allPairs = ([] as [string, string, boolean][]).concat(...disjuncts);
                allPairs.forEach(([k, v]) => {
                    if (!ifsById.hasOwnProperty(k)) { ifsById[k] = []; }
                    ifsById[k].push([el, disjuncts]);
                });
                if (evaluateIf(disjuncts)) {
                    el.style.setProperty('display', 'block', 'important');
                }
                break;
            }
            case 'option': {
                const [_, inputId, optionId] = dataIastSections;
                inputsById[inputId][1]![optionId] = el as HTMLButtonElement;
                el.addEventListener('click', e => {
                    update(inputId, optionId, e.currentTarget as HTMLElement);
                });
                break;
            }
            case 'var': {
                try {
                    const value = parseRefExpression(dataIastSections[1]);
                    if (!varsById.hasOwnProperty(value[0])) { varsById[value[0]] = []; }
                    varsById[value[0]].push([el, value[1]]);
                    const current = currentValues[value[0]];
                    const latestValue = current[1] || unescapeForIast(current[0]);
                    el.appendChild(document.createTextNode(value[1] ? expressionRuntime[value[1]](latestValue) : latestValue));
                } catch (e) {
                    el.appendChild(error('错误：' + (e as Error).message));
                }
                break;
            }
            case 'input': {
                try {
                    let isPrivate = false;
                    if (dataIastSections[1] === '') { // An additional empty section means it's private.
                        isPrivate = true;
                        dataIastSections.splice(1, 1);
                    }
                    const id = dataIastSections[1];
                    const nonSelectType = dataIastSections[2] as (IastInput['type'] & string) | undefined;
                    inputsById[id] = [el as HTMLAnchorElement, nonSelectType ? undefined : {}];
                    el.style.textDecoration = 'underline dotted';
                    el.setAttribute('data-toggle', 'dropdown');
                    el.appendChild(document.createTextNode('（加载中）'));
                    el.title = '点击修改你的选择';
                    const popUp = el.nextElementSibling as HTMLDivElement;
                    if (nonSelectType) {
                        popUp.appendChild(inputForm(currentValues[id], nonSelectType === 'cnm', text => {
                            if (!text) { alert('内容不能为空哦～'); return false; }
                            if (nonSelectType === 'cnm') {
                                try {
                                    legalChineseName(text);
                                } catch (e) {
                                    alert('错误：' + (e as Error).message);
                                    return false;
                                }
                            }
                            update(id, text, popUp);
                            return true;
                        }));
                    }
                    if ((isMyThread || !isPrivate) && (nonSelectType !== 'cnm')) {
                        popUp.appendChild($('button', 'btn btn-sm btn-secondary mt-2 mx-auto mb-1 d-block', [$('span', 'material-icons me-1', 'leaderboard'), '统计'], () => {
                            if (!currentValues[id][0]) {
                                alert('请先做出你的选择，再查看别人的选择啦！');
                                return;
                            }
                            showStatsModal(id);
                        }));
                    }
                } catch (e) {
                    el.appendChild(error('错误：' + (e as Error).message));
                }
                break;
            }
            default:
                el.appendChild(error('错误：运行时不支持「' + dataIastSections[0] + '」'));
        }
    });

    Object.keys(inputsById).forEach(updateInputDisplay);
};