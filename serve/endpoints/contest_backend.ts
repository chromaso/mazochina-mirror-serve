import { META_THREAD_ID, REVIEW_CAMPAIGN_TAG_ID } from '../shared/contest_result';
import { fullContestStats, parseBBCodeOneOff, singleUserStats, TEMP_EXTERNAL_WORKS, TITLES_OF_COMPONENTS } from '../shared/contest_backend';
import { query as sql } from '../db';
import { F18_CONSTS } from '../shared/tags';
import { parseInt32 } from '../utils/logic_utils';
import { error, finalizePage, LOGIN_REQUIRED, PageImpl, redirectWithDelay, RpcImpl } from '../utils/render_utils';

import * as templates from '../templates/contest_backend';
import { totalScoreAfterValidation } from '../shared/contest_result';

type UserLite = { user_id: number; user_name: string; };

export const adminContest: PageImpl = async (ctx) => {
    const panelMembers = (await sql<UserLite>('SELECT user_id, user_name FROM users WHERE flags & 16 <> 0')).rows;
    const threadRowsRaw = (await sql<UserLite & { thread_id: number; thread_title: string; firstpost_date: number; }>(`SELECT thread_id, thread_title, firstpost_date, user_id, user_name FROM thread_targets LEFT JOIN users ON thread_targets.owner_user_id = users.user_id WHERE forum_id = 18 AND last_page_number > 0 AND successive_fail >= 0 AND EXISTS (SELECT tagass_id FROM tag_association WHERE tag_association.thread_id = thread_targets.thread_id AND tag_id = $1 AND tag_score > ${F18_CONSTS.CONSTS.SCORE_MULTIPLIER}) ORDER BY thread_id ASC`, [REVIEW_CAMPAIGN_TAG_ID || F18_CONSTS.VOTE_CAMPAIGN_TAG_ID])).rows;

    const threadRows = threadRowsRaw.map(thread => ({
        thread_id: thread.thread_id,
        userLink: '/author/' + thread.user_id,
        userName: thread.user_name,
        link: '/thread/' + thread.thread_id,
        title: thread.thread_title,
        firstpost_date: thread.firstpost_date
    })).concat(Array.from(TEMP_EXTERNAL_WORKS).map(([k, v]) => ({
        thread_id: k,
        userLink: 'https://www.pixiv.net/users/107369766',
        userName: 'Pixiv',
        link: `https://www.pixiv.net/novel/show.php?id=${-k}`,
        title: v[0],
        firstpost_date: Math.round(Date.parse(v[1]) / 1000)
    })));

    return finalizePage(ctx, {
        title: ctx.state.render.staticConv('征文比赛 Dashboard'),
        nav: false,
        deps: ['ADMIN_JS', 'CHARTS']
    }, templates.contestAdmin(ctx, {
        panelMembers,
        threadRows,
        ...(await fullContestStats(threadRows, panelMembers)),
        TITLES_OF_COMPONENTS
    }));
};

export const postAdminContest: PageImpl = async (ctx) => {
    const loginUser = ctx.state.mazoLoginUser;
    if (!loginUser) { throw error(403, LOGIN_REQUIRED); }
    if (!loginUser.admin) {
        throw error(403, '你没有管理权限。');
    }

    let alertText = '';
    const addUsername = ctx.request.body.string('add-username');
    if (addUsername) {
        const updated = (await sql<UserLite>('UPDATE users SET flags = flags | 16 WHERE user_name = $1 AND password_h IS NOT NULL RETURNING user_id, user_name', [addUsername])).rows[0];
        if (updated) {
            alertText += `已将 ${updated.user_name} 添加为评委。`;
        } else {
            throw error(403, '没有这个用户。请确定大小写/繁简体必须完全符合，且该用户必须在镜像注册。');
        }
    }
    const removeUserId = ctx.request.body.integer('remove-userid');
    if (removeUserId) {
        const updated = (await sql<UserLite>('UPDATE users SET flags = flags & (~ 16::SMALLINT) WHERE user_id = $1 RETURNING user_id, user_name', [removeUserId])).rows[0];
        if (updated) {
            alertText += `已将 ${updated.user_name} 从评委列表移除。`;
        }
    }
    const overrideUser = ctx.request.body.integer('override-user');
    const overrideThread = ctx.request.body.integer('override-thread');
    if (!isNaN(overrideUser) && !isNaN(overrideThread)) {
        const row = (await sql('SELECT review_at FROM contest_panel_reviews WHERE thread_id = $1 AND user_id = $2 ORDER BY rev_id DESC LIMIT 1', [overrideThread, overrideUser])).rows[0];
        if (row && (row.review_at > 0)) {
            throw error(403, '该评委已亲自填写评分/评语。管理员不可覆盖已亲自填写的评分/评语。');
        }

        const scores = ctx.request.body.integerArray('override-score');
        const commentContent = ctx.request.body.string('override-content');
        const score = totalScoreAfterValidation(scores);
        await sql('INSERT INTO contest_panel_reviews (thread_id, user_id, content, score, review_at, extra_information) VALUES ($1, $2, $3, $4, $5, $6)', [overrideThread, overrideUser, commentContent, score, -ctx.refDate, score ? JSON.stringify(scores) : null]);
        alertText += '已代评委填入。';
    }
    if (alertText === '') { alertText = '未做出任何变更。'; }

    return redirectWithDelay(ctx, { message: alertText, targetText: '活动管理', targetHref: ctx.path });
};

export const contestPanel: PageImpl = async (ctx) => {
    const loginUser = ctx.state.mazoLoginUser;
    if (!loginUser) { throw error(403, LOGIN_REQUIRED); }
    if (!(loginUser.rawFlags & 0x10)) {
        throw error(403, '你不是评委，没有打分权限。');
    }

    const threadRows = (await sql<{ thread_id: number; thread_title: string; }>(`SELECT thread_id, thread_title FROM thread_targets WHERE forum_id = 18 AND last_page_number > 0 AND successive_fail >= 0 AND EXISTS (SELECT tagass_id FROM tag_association WHERE tag_association.thread_id = thread_targets.thread_id AND tag_id = $1 AND tag_score > ${F18_CONSTS.CONSTS.SCORE_MULTIPLIER}) ORDER BY thread_id ASC`, [REVIEW_CAMPAIGN_TAG_ID || F18_CONSTS.VOTE_CAMPAIGN_TAG_ID])).rows;
    const foreword = (await sql<{ content: string; review_at: number; }>('SELECT content, review_at FROM contest_panel_reviews WHERE thread_id = $1 AND user_id = $2 ORDER BY rev_id DESC LIMIT 1', [META_THREAD_ID, loginUser.id])).rows[0];

    return finalizePage(ctx, {
        title: ctx.state.render.staticConv('征文比赛评委点评'),
        nav: true
    }, templates.contest(ctx, {
        latestReviews: await singleUserStats(loginUser.id, threadRows.map(thread => thread.thread_id)),
        threadRows,
        foreword: foreword ? parseBBCodeOneOff(foreword.content).html : undefined,
        forewordAt: foreword?.review_at ?? 0
    }));
};

export const postContestPanel: PageImpl = async (ctx) => {
    const loginUser = ctx.state.mazoLoginUser;
    if (!loginUser) { throw error(403, LOGIN_REQUIRED); }
    if (!(loginUser.rawFlags & 0x10)) {
        throw error(403, '你不是评委，没有操作权限。');
    }
    const foreword = ctx.request.body.string('foreword');
    await sql('INSERT INTO contest_panel_reviews (thread_id, user_id, content, score, review_at, extra_information) VALUES ($1, $2, $3, 0, $4, NULL)', [META_THREAD_ID, loginUser.id, foreword, ctx.refDate]);
    return redirectWithDelay(ctx, { message: '已保存前言～', targetText: '评委点评进度', targetHref: ctx.path });
};

export const apiContestThread_id_user_id: RpcImpl = async (ctx) => {
    const threadId = parseInt32(ctx.params.id);
    if (!threadId) { throw error(400, '未指定主题！'); }

    const userInfo = ctx.state.mazoLoginUser;
    if (!userInfo) { throw error(403, LOGIN_REQUIRED); }

    let userId = parseInt32(ctx.params.userId);
    if (isNaN(userId)) {
        userId = userInfo.id; // Viewing for oneself.
    } else if (!userInfo.admin) {
        throw error(403, '你没有权限。');
    }

    const row = (await sql('SELECT content FROM contest_panel_reviews WHERE thread_id = $1 AND user_id = $2 ORDER BY rev_id DESC LIMIT 1', [threadId, userId])).rows[0];

    ctx.body = row ? { bbcode: row.content, html: parseBBCodeOneOff(row.content).html } : { bbcode: '', html: '（从未填写）' };
};
