import crypto = require('crypto');

import { RateLimiter } from './rate_limit';
import { hmacKeyCaptcha } from '../conf';
import { fromRoot } from '../path';
import sharp = require('sharp');

const toChn = (num: number) => {
    // Input must be an integer in [1, 9999].
    let rem = num;
    const chars = '零一二三四五六七八九';
    let out = '';
    const thousand = (rem / 1000) | 0;
    if (thousand > 0) { out += chars.charAt(thousand) + '千'; }
    rem %= 1000;
    const hundred = (rem / 100) | 0;
    if (hundred > 0) { out += chars.charAt(hundred) + '百'; }
    rem %= 100;
    const decade = (rem / 10) | 0;
    if (decade > 0) {
        if (out && (out.charAt(out.length - 1) !== '百')) { out += '零'; }
        out += chars.charAt(decade) + '十';
    }
    rem %= 10;
    if (rem > 0) {
        if (out && (out.charAt(out.length - 1) !== '十')) { out += '零'; }
        out += chars.charAt(rem);
    }
    return out;
};

const randomColor = (bg: boolean): string => {
    const colors = [];
    for (let i = 0; i < 3; i++) {
        colors[i] = Math.random() * 64 | 0;
        if (bg) { colors[i] = 255 - colors[i]; colors[i + 3] = 255 - colors[i + 3]; }
    }
    return '#' + (0x1000000 | (colors[0] << 16) | (colors[1] << 8) | colors[2]).toString(16).substring(1);
};

const createCaptcha = async (): Promise<{
    dataUrl: string,
    expectedSolution: string
}> => {
    const number1 = Math.floor(Math.random() * 400) + 600; // [600, 1000)
    const number2 = Math.floor(Math.random() * 400) + 100; // [100, 500)

    let chalk = Math.random() < 0.5;
    const picSegments: sharp.Sharp[] = [];
    for (const chn of [toChn(number1), '减' + toChn(number2)]) {
        let pangoStr = '';
        for (let i = 0; i < chn.length; i++) {
            const baseFontSize = (Math.random() * 6 + 26) | 0;
            pangoStr += `<span foreground="${randomColor(false)}" size="${baseFontSize * 1024}" rise="${(Math.random() * 8 * 1024) | 0}" letter_spacing="${(Math.random() * 6 * 1024) | 0}">${chn.charAt(i)}</span>`;
        }
        chalk = !chalk;
        picSegments.push(sharp({
            text: {
                font: chalk ? 'SentyChalk' : 'SentyPaperCut',
                fontfile: chalk ? fromRoot('chalk.ttf') : fromRoot('papercut.ttf'),
                rgba: true,
                text: pangoStr
            }
        }));
    }
    const meta = await Promise.all(picSegments.map(single => single.metadata()));
    const grossWidth = 10 + meta[0].width! + 5 + meta[1].width! + 10;
    const grossHeight = Math.max(meta[0].height!, meta[1].height!) + 10;
    const bgBuffer = Buffer.allocUnsafe(300);
    const rl = (Math.random() * 80 + 175) | 0; const rr = (Math.random() * 80 + 175) | 0;
    const gt = (Math.random() * 80 + 175) | 0; const gb = (Math.random() * 80 + 175) | 0;
    const bl = (Math.random() * 80 + 175) | 0; const br = (Math.random() * 80 + 175) | 0;
    const ot = (Math.random() * 40 + 200) | 0; const ob = (Math.random() * 40 + 200) | 0;
    for (let i = 0; i < 300; i += 4) {
        const x = (i % 60) / 60;
        const y = (i / 60) / 5;
        bgBuffer[i] = rr * x + rl * (1 - x);
        bgBuffer[i + 1] = gb * y + gt * (1 - y);
        bgBuffer[i + 2] = br * x + bl * (1 - x);
        bgBuffer[i + 3] = ot * y + ob * (1 - y) - ((Math.random() * 30) | 0);
    }
    const baseGradient = sharp(bgBuffer, { raw: { width: 15, height: 5, channels: 4 } }).resize(grossWidth, grossHeight, { fit: 'fill', 'kernel': 'cubic' });
    const toComposite = (input: sharp.Sharp): Promise<sharp.OverlayOptions> => input.raw().toBuffer({ resolveWithObject: true }).then(({ data: input, info: raw }) => ({ input, raw }));
    const picSegment = sharp({
        create: {
            width: grossWidth,
            height: grossHeight,
            channels: 3,
            noise: {
                type: 'gaussian',
                mean: 180,
                sigma: 60
            },
            background: {}
        }
    }).composite([
        await toComposite(baseGradient),
        { top: ((grossHeight - meta[0].height!) / 2) | 0, left: 10, ...await toComposite(picSegments[0]) },
        { top: ((grossHeight - meta[1].height!) / 2) | 0, left: meta[0].width! + 15, ...await toComposite(picSegments[1]) }
    ]);

    return {
        dataUrl: 'data:image/webp;base64,' + (await picSegment.webp({ quality: 80 }).toBuffer()).toString('base64'),
        expectedSolution: toChn(number1 - number2)
    };
};

export const createCaptchaInfo = async (): Promise<{ id: string, dataUrl: string }> => {
    const captcha = await createCaptcha();

    const hashedExpectedSolution = crypto.createHash('sha1');
    hashedExpectedSolution.update(captcha.expectedSolution);
    hashedExpectedSolution.update(hmacKeyCaptcha);

    const tupleBuffer = Buffer.from(JSON.stringify([
        Date.now().toString(16) + '-' + Math.random().toString(16).substr(2),
        hashedExpectedSolution.digest('base64'),
        Date.now()
    ]), 'utf8');
    const hmac = crypto.createHmac('sha1', hmacKeyCaptcha);
    hmac.update(tupleBuffer);

    return {
        id: tupleBuffer.toString('hex') + '_' + hmac.digest('hex'),
        dataUrl: captcha.dataUrl
    };
};

const invalidCaptchas = new RateLimiter(3, 900);

// Throws when fails. Otherwise do nothing.
export const validateCaptcha = (id: string, solution: string): void => {
    const idSections = id.split('_');
    if ((idSections.length !== 2) || (idSections.some(section => !/^[0-9a-f]+$/.test(section)))) {
        throw new RangeError('ID cannot be parsed.');
    }
    const tupleBuffer = Buffer.from(idSections[0], 'hex');
    const hmac = crypto.createHmac('sha1', hmacKeyCaptcha);
    hmac.update(tupleBuffer);
    if (hmac.digest('hex') !== idSections[1]) { throw new RangeError('HMAC does not match.'); }
    const tuple = JSON.parse(tupleBuffer.toString('utf8'));
    if (tuple.length !== 3) { throw new RangeError('ID tuple invlid.'); }
    const [serverSideId, hashedExpectedSolution, date] = tuple;
    if (!(Date.now() - date < 600e3)) {
        throw new RangeError('Captcha expired.');
    }
    if (!invalidCaptchas.attempt(serverSideId)) {
        throw new RangeError('Too many attempts on a single Captcha.');
    }
    
    const normalizedSolution = solution.trim().replace(/兩|两/g, '二').replace(/〇/g, '零');
    const hashedActualSolution = crypto.createHash('sha1');
    hashedActualSolution.update(normalizedSolution);
    hashedActualSolution.update(hmacKeyCaptcha);
    if (hashedActualSolution.digest('base64') !== hashedExpectedSolution) {
        throw new RangeError('Captcha was wrong.');
    }
    invalidCaptchas.blackout(serverSideId);
};