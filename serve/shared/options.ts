import { COOKIE_PREFIX } from '../conf';
import { CONV_KEEP, CONV_TC, CONV_SC } from '../chinese';
import { highlight } from '../utils/highlight';
import { query } from '../db';
import { Context } from 'koa';
import { MazoKoaContext, MazoState } from '../utils/logic_utils';
import { unawaited } from '../../common/logic_utils';
import { timeFormatterImpls } from '../utils/time';
import { safeHtml } from 'typed-xhtml';

type CookieOptions = NonNullable<Parameters<Context['cookies']['set']>[2]>;

export const cookieOptions: CookieOptions = {
    maxAge: 2 * 365 * 86400 * 1000
};

// ['cyborg', 'darkly', 'quartz', 'slate', 'solar'] are dark-only; others are light/dark
export const ALL_THEMES = Object.freeze([
    'default', // 0
    'cosmo', // 1
    'cyborg', // 2
    'darkly', // 3
    'litera', // 4
    'lumen', // 5
    'lux', // 6
    'materia', // 7
    'minty', // 8
    'sketchy', // 9
    'slate', // 10
    'solar', // 11
    'spacelab', // 12
    'united', // 13
    'yeti', // 14
    'cerulean', // 15
    'flatly', // 16
    'journal', // 17
    'morph', // 18
    'pulse', // 19
    'quartz', // 20
    'sandstone', // 21
    'simplex', // 22
    'zephyr' // 23
    // superhero and vapor are excluded
] as const);

export const DEFAULT_THEMES: Readonly<ThemeName[]> = Object.freeze(['cerulean', 'cosmo', 'default', 'flatly', 'journal', 'lumen', 'pulse', 'sandstone', 'spacelab', 'united', 'yeti']);

export type ThemeName = typeof ALL_THEMES[number];

export const themeToUrl = (themeName: ThemeName) => (themeName === 'default') ? 'https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css' : `https://cdn.jsdelivr.net/npm/bootswatch@5.3.3/dist/${themeName}/bootstrap.min.css`;

const tzs: string[] = []; // tzs[0]: UTC-12; tzs[12]: UTC; tzs[24]: UTC+12; so index - 12 === offset.
for (let i = -12; i <= 12; i++) {
    tzs.push('UTC' + ((i > 0) ? '+' : '') + ((i === 0) ? '' : i));
}

const forumPc = [20, 30, 50, 100];
const threadPc = [10, 20, 30, 50];

const ccOptions = {
    'scnc': '简体界面，内容不作转换',
    'sccc': '简体界面，内容转为简体',
    'tcnc': '正體界面，內容不作轉換',
    'tccc': '正體界面，內容轉为正體'
} as const;

const ccOptionsList = Object.keys(ccOptions) as (keyof typeof ccOptions)[];

export type FullConfig = {
    sync: boolean,
    tz: number, tfmt: 0 | 1 | 2 | 3, theme: ThemeName, fp: number, tp: number, cc: keyof typeof ccOptions,
    lf: 0 | 1 | -1,
    hc: 0 | 1 | 2
};

export type RawConfig = Partial<FullConfig>;

// Unused now, but kept for reference.
const serializeConfigV0 = (config: RawConfig): bigint => {
    let outBasic = 0;
    if (config.theme !== undefined) {
        const index = ALL_THEMES.indexOf(config.theme);
        if (index >= 0) {
            outBasic |= index; // [0, 4)
            outBasic |= 1 << 4; // [4, 5)
        }
    }
    if (config.cc !== undefined) {
        const index = ccOptionsList.indexOf(config.cc);
        if (index >= 0) {
            outBasic |= index << 5; // [5, 7)
            outBasic |= 1 << 7; // [7, 8)
        }
    }
    if (config.fp !== undefined) {
        const index = forumPc.indexOf(config.fp);
        if (index >= 0) {
            outBasic |= index << 8; // [8, 10)
            outBasic |= 1 << 10; // [10, 11)
        }
    }
    if (config.tp !== undefined) {
        const index = threadPc.indexOf(config.tp);
        if (index >= 0) {
            outBasic |= index << 11; // [11, 13)
            outBasic |= 1 << 13; // [13, 14)
        }
    }
    if (config.tz !== undefined) {
        if ((typeof config.tz === 'number') && (config.tz >= -12) && (config.tz <= 12)) {
            outBasic |= (config.tz + 12) << 14; // [14, 19)
            outBasic |= 1 << 19; // [19, 20)
        }
    }
    if (config.tfmt !== undefined) {
        if ((typeof config.tfmt === 'number') && (config.tfmt >= 0) && (config.tfmt <= 3)) {
            outBasic |= config.tfmt << 22; // [22, 24)
            outBasic |= 1 << 24; // [24, 25)
        }
    }
    if (config.lf !== undefined) {
        if ((typeof config.lf === 'number') && (config.lf >= -1) && (config.lf <= 1)) {
            outBasic |= (
                config.lf + 2 // 1, 2 or 3.
            ) << 25; // [25, 27), non-zero.
        }
    } // Else: // [25, 27) all be zero.
    if (config.hc !== undefined) {
        if ((typeof config.hc === 'number') && (config.hc >= 0) && (config.hc <= 2)) {
            outBasic |= (
                config.hc + 1 // 1, 2 or 3.
            ) << 28; // [28, 30), non-zero.
        }
    } // Else: // [28, 30) all be zero.
    if (config.sync) {
        outBasic |= 1 << 30;
    }
    return BigInt(outBasic);
};

// V1.
export const serializeConfig = (config: RawConfig): bigint => {
    let out = 0;

    if ((typeof config.tfmt === 'number') && (config.tfmt >= 0) && (config.tfmt <= 3)) {
        out |= config.tfmt + 1;
    } // Bits [0, 3): all 0 if undefined.

    if ((typeof config.lf === 'number') && (config.lf >= -1) && (config.lf <= 1)) {
        out |= (
            config.lf + 2 // 1, 2 or 3.
        ) << 3;
    } // [3, 5): all 0 if undefined.

    if (config.cc !== undefined) {
        const index = ccOptionsList.indexOf(config.cc);
        out |= (index + 1) << 5; // [5, 8); if index is -1, these are all zero.
    } // [5, 8): all 0 if undefined.

    if (config.fp !== undefined) {
        const index = forumPc.indexOf(config.fp);
        out |= (index + 1) << 8; // [8, 11); if index is -1, these are all zero.
    } // [8, 11): all 0 if undefined.

    if (config.tp !== undefined) {
        const index = threadPc.indexOf(config.tp);
        out |= (index + 1) << 11; // [11, 14); if index is -1, these are all zero.
    } // [11, 14): all 0 if undefined.

    if (config.tz !== undefined) {
        if ((typeof config.tz === 'number') && (config.tz >= -12) && (config.tz <= 12)) {
            out |= (config.tz + 13) << 14; // [14, 19) to be between 1 and 25, inclusive.
        }
    } // [14, 19): all 0 if undefined.

    if (config.theme !== undefined) {
        const index = ALL_THEMES.indexOf(config.theme); // Including the case of -1.
        if (index >= 0) {
            out |= (index + 1) << 20; // [20, 25); if index is -1, these are all zero.
        }
    } // [20, 25): all 0 if undefined.

    if ((typeof config.hc === 'number') && (config.hc >= 0) && (config.hc <= 2)) {
        out |= (
            config.hc + 1 // 1, 2 or 3.
        ) << 25; // [25, 27), non-zero.
    } // [25, 27): all 0 if undefined.

    if (config.sync) {
        out |= 1 << 30;
    }

    return BigInt(out) | (BigInt(1) << BigInt(49));    
};

const deserializeConfigV0 = (num: number): RawConfig => {
    const out: RawConfig = {};
    if (!((num < 0x7fffffff) && (num > 0))) {
        // Including the NaN case.
        return out;
    }
    if (num & (1 << 4)) {
        const candidate = ALL_THEMES[num & 0xf];
        if (candidate !== undefined) { out.theme = candidate; }
    }
    if (num & (1 << 7)) {
        const candidate = ccOptionsList[(num >> 5) & 0x3];
        if (candidate !== undefined) { out.cc = candidate; }
    }
    if (num & (1 << 10)) {
        const candidate = forumPc[(num >> 8) & 0x3];
        if (candidate !== undefined) { out.fp = candidate; }
    }
    if (num & (1 << 13)) {
        const candidate = threadPc[(num >> 11) & 0x3];
        if (candidate !== undefined) { out.tp = candidate; }
    }
    if (num & (1 << 19)) {
        const candidate = ((num >> 14) & (0x1f)) - 12;
        if ((candidate >= -12) && (candidate <= 12)) { out.tz = candidate; }
    }
    if (num & (1 << 24)) {
        out.tfmt = ((num >> 22) & 0x3) as (0 | 1 | 2 | 3);
    }
    const lfCandidate = ((num >> 25) & 0x3) as (0 | 1 | 2 | 3);
    if (lfCandidate !== 0) {
        out.lf = (lfCandidate - 2) as (-1 | 0 | 1);
    }
    const hcCandicate = ((num >> 28) & 0x3) as (0 | 1 | 2 | 3);
    if (hcCandicate !== 0) {
        out.hc = (hcCandicate - 1) as (0 | 1 | 2);
    }
    if (num & (1 << 30)) {
        out.sync = true;
    }
    return out;
};

const deserializeConfigV1 = (num: number): RawConfig => {
    const out: RawConfig = {};
    if (!((num < 0x7fffffff) && (num > 0))) {
        // Including the NaN case.
        return out;
    }

    const tfmtGroup = num & 0x7;
    if (tfmtGroup !== 0) {
        out.tfmt = (tfmtGroup - 1) as (0 | 1 | 2 | 3);
    }

    const lfGroup = (num >> 3) & 0x3;
    if (lfGroup !== 0) {
        out.lf = (lfGroup - 2) as (-1 | 0 | 1);
    }

    const ccGroup = (num >> 5) & 0x7;
    if (ccGroup !== 0) {
        out.cc = ccOptionsList[ccGroup - 1];
    }

    const fpGroup = (num >> 8) & 0x7;
    if (fpGroup !== 0) {
        out.fp = forumPc[fpGroup - 1];
    }

    const tpGroup = (num >> 11) & 0x7;
    if (tpGroup !== 0) {
        out.tp = threadPc[tpGroup - 1];
    }

    const tzGroup = (num >> 14) & 0x1f;
    if (tzGroup !== 0) {
        out.tz = tzGroup - 13;
    }

    const themeGroup = (num >> 20) & 0x1f;
    if (themeGroup !== 0) {
        out.theme = ALL_THEMES[themeGroup - 1];
    }

    const hcGroup = (num >> 25) & 0x3;
    if (hcGroup !== 0) {
        out.hc = (hcGroup - 1) as (0 | 1 | 2);
    }

    if (num & (1 << 30)) {
        out.sync = true;
    }
    
    return out;
};

const deserializeConfig = (rawNum: bigint): RawConfig => {
    const version = Number(rawNum >> BigInt(49));
    if (version === 1) {
        return deserializeConfigV1(Number(rawNum & BigInt(562949953421311))); // For v1, for now it's guaranteed that only lower 31 bits would be used.
    } else if (version === 0) {
        return deserializeConfigV0(Number(rawNum)); // For v0, it's guaranteed that only lower 31 bits would be used.
    }
    return {};
};

const isBot = (ctx: Context): boolean => {
    const ua = (ctx.headers['user-agent'] || '').toLowerCase();
    return (!ua) || ua.includes('bot') || ua.includes('crawl') || ua.includes('spider');
};

export const currentConfiguration = (ctx: MazoKoaContext): Pick<MazoState, 'userConfig' | 'userConfigRaw'> => {
    const ssConfig = (ctx.state.mazoLoginUser?.oldConfigs) ? deserializeConfig(ctx.state.mazoLoginUser.oldConfigs) : undefined;
    const newConfigRaw = ctx.cookies.get(COOKIE_PREFIX + 'conf');
    let isABot = false;

    let baseConf: RawConfig;
    if (ssConfig && ssConfig.sync) { // Attempt server-side config first.
        baseConf = ssConfig;
    } else if (newConfigRaw) { // Fall back to the new-style cookie.
        try {
            baseConf = deserializeConfig(BigInt(parseInt(newConfigRaw, 32))); // WARNING: by using parseInt here, only lower 52 bits can be used!
        } catch (e) {
            // Seen some weird "NaN cannot be converted to a BigInt because it is not an integer" error in prod. Let's log the details.
            console.error(`Cannot understand the conf string ${JSON.stringify(newConfigRaw)}`);
            baseConf = {};
        }
    } else { // No existing conf.
        isABot = isBot(ctx);

        baseConf = {};
    }
    let changed = false;
    if (baseConf.theme === undefined) {
        baseConf.theme = isABot ? 'default' : DEFAULT_THEMES[Math.floor(Math.random() * DEFAULT_THEMES.length)];
        changed = true;
    }
    if (baseConf.cc === undefined) {
        baseConf.cc = isABot ? 'scnc' : (Math.random() > 0.5 ? 'scnc' : 'tcnc');
        changed = true;
    }
    if (baseConf.tfmt === undefined) {
        if (isABot) {
            baseConf.tfmt = 0;
        } else {
            // Never use the long format. Lean towards the last format.
            const randomTfmt = Math.floor(Math.random() * 4);
            baseConf.tfmt = (randomTfmt || 3) as (1 | 2 | 3);
        }
        changed = true;
    }
    if (baseConf.tz === undefined) {
        baseConf.tz = isABot ? 8 : ((Math.random() > 0.25) ? 8 : 0);
        changed = true;
    }
    if (changed) { // Don't serialize the default values being assigned below.
        const snapshotForPersistence = serializeConfig(baseConf);
        const cookieString = snapshotForPersistence.toString(32);
        if (newConfigRaw !== cookieString) {
            ctx.cookies.set(COOKIE_PREFIX + 'conf', snapshotForPersistence.toString(32), cookieOptions);
        }
        if (ctx.state.mazoLoginUser && baseConf.sync) {
            if (ctx.state.mazoLoginUser.oldConfigs !== snapshotForPersistence) {
                unawaited(query<{}, [bigint, number]>('UPDATE users SET configs = $1 WHERE user_id = $2', [snapshotForPersistence, ctx.state.mazoLoginUser.id]), 'logging user_options');
            }
        }
    }
    return {
        userConfig: {
            fp: 30,
            tp: 20,
            lf: 0,
            hc: (ctx.headers['user-agent'] || '').includes('Mobile') ? 1 : 2,
            sync: false,
            ...(baseConf as (RawConfig & Pick<FullConfig, 'theme' | 'cc' | 'tfmt' | 'tz'>))
        },
        userConfigRaw: baseConf
    };
};

interface DateRenderrer {
    (dateInSeconds: number): SafeString;
    (dateInSeconds: number, raw: true): [string, string];
}

export type RenderHelpers = {
    themeCss: string,
    staticConv: (text: string) => string,
    contentConv: (text: string) => string,
    locale: 'cn' | 'tw',
    highlight: (text: string, keywordsInScLowerCase: string[]) => string,
    renderDate: DateRenderrer
}

export const renderHelpers = (ctx: MazoKoaContext): RenderHelpers => {
    const { theme, cc, tz, tfmt: tFormat } = ctx.state.userConfig;
    const contentConv = (cc === 'sccc') ? CONV_SC : ((cc === 'tccc') ? CONV_TC : CONV_KEEP);
    const useTc = cc.startsWith('tc');
    const fullDateAlready = tFormat === 0;
    const timeFormatter = timeFormatterImpls[tFormat];
    const tzInSecs = tz * 3600;
    return {
        themeCss: themeToUrl(theme),
        // Part of the UI, so assume source is SC.
        staticConv: useTc ? CONV_TC : CONV_KEEP,
        // Depending on user settings to decide whether to convert.
        contentConv,
        // Used for JS file suffixes.
        locale: useTc ? 'tw' : 'cn',
        highlight: (text, keywordsInScLowerCase) => contentConv(highlight(CONV_SC(text.toLowerCase()), keywordsInScLowerCase)),
        renderDate: ((timeStamp: number, raw?: true) => {
            const dateObj = new Date((timeStamp + tzInSecs) * 1000);
            const formatted = timeFormatter(timeStamp, dateObj, tzInSecs, ctx.refDate, useTc);
            if (fullDateAlready) { return raw ? [formatted, formatted] : safeHtml(`<time>${formatted}</time>`); }
            const structured = timeFormatterImpls[0](timeStamp, dateObj, tzInSecs, ctx.refDate, useTc);
            return raw ? [formatted, structured] : safeHtml(`<time datetime="${structured}">${formatted}</time>`); // We're sure those strings won't need to escaped.
        }) as DateRenderrer
    };
};

export type ConfigPanel = {
    selectedTz: FullConfig['tz']; tzs: typeof tzs;
    selectedTheme: FullConfig['theme'];
    selectedForumPc: FullConfig['fp']; forumPc: FullConfig['fp'][];
    selectedThreadPc: FullConfig['tp']; threadPc: FullConfig['tp'][];
    selectedCc: FullConfig['cc']; ccs: Record<FullConfig['cc'], string>;
    selectedTfmt: FullConfig['tfmt']; tfmtPreviews: string[][];
    lf: FullConfig['lf'];
    hc: FullConfig['hc'];
    sync: FullConfig['sync'];
};

export const footer = async (ctx: MazoKoaContext): Promise<ConfigPanel> => {
    const { tz, theme, fp, tp, cc, tfmt, lf, hc, sync } = ctx.state.userConfig;
    const now = Math.floor(Date.now() / 1000);
    const tests = [now - 3600 * 6, now - 86400 * 6, now - 86400 * 120, now - 86400 * 1200];
    const tzInSecs = tz * 3600;
    return {
        selectedTz: tz, tzs,
        selectedTheme: theme,
        selectedForumPc: fp, forumPc,
        selectedThreadPc: tp, threadPc,
        selectedCc: cc, ccs: ccOptions,
        selectedTfmt: tfmt,
        tfmtPreviews: timeFormatterImpls.map(impl => tests.map(testCase => impl(testCase, new Date((testCase + tzInSecs) * 1000), tzInSecs, ctx.refDate, cc.startsWith('tc')))),
        lf,
        hc,
        sync
    };
}