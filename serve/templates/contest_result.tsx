import * as elements from 'typed-xhtml';
import { HtmlTmpl } from '../utils/tsx_utils';
import { allContestResults } from '../shared/contest_result';
import { tagList } from './tag';

// Migrated from contest_result.inc.pug.
export const contestResult: HtmlTmpl<{
    contestResult: {
        votes?: { count: number, rank: string | null }[],
        scores: { avg: number, rank: string | null, sd: number }[],
        reviews: number,
        scorers: number[]
    }
}> = (ctx, self) => {
    const { render } = ctx.state;

    const renderRanking = (rankStr: string | null, isVote: boolean) => (
        rankStr ? (
            <span
                data-title={`${isVote ? '该种读者投票' : '该项评委打分'}在所有参赛作品中位列` + render.staticConv(rankStr)}
                class="badge bg-primary mazomirror-tip"
            >
                {render.staticConv(rankStr)}
            </span>
        ) : (
            <span
                data-title={`${isVote ? '该种读者投票' : '该项评委打分'}在所有参赛作品中不幸未能进入前 50%，但本作品依然是非常优秀的小说`}
                class="badge bg-secondary mazomirror-tip"
            >
                后 50%
            </span>
        )
    );

    const renderSd = (sd: number, count: number) => (
        <span
            data-title={count + ' 名评委给此作品打出的分数的标准差（SD）为' + sd + '；标准差越小则说明评委意见越一致'}
            class="badge bg-secondary mazomirror-tip"
        >
            σ={sd.toFixed(1)}
        </span>
    );

    return (
        <div class="card">
            <div class="card-header">征文比赛赛果</div>
            <div class="card-body">
                <p class="pb-2 mb-4">
                    此作品参加了 2024 年夏季举办的<a href="/thread/1073748496">叁孙杯「恶女」题材 M 向小说征文比赛</a>，以下为其获得的评委评分和读者投票：
                </p>
                <div class="row text-center">
                    <div class="col-3 col-md mb-2">
                        <span class="align-middle material-icons" style='font-size: 150%'>
                            switch_access_shortcut
                        </span>
                        <span class="align-middle">色情</span>
                        <div style='font-size: 120%'>
                            {Math.round(self.contestResult.scores[0].avg)}
                            <small class="text-muted">/100</small>
                        </div>
                        <div class="small">
                            {renderRanking(self.contestResult.scores[0].rank, false)}
                            {' '}
                            {renderSd(self.contestResult.scores[0].sd, self.contestResult.scorers.length)}
                        </div>
                    </div>
                    <div class="col-3 col-md mb-2">
                        <span class="align-middle material-icons" style='font-size: 150%'>
                            auto_stories
                        </span>
                        <span class="align-middle"> 文学</span>
                        <div style='font-size: 120%'>
                            {Math.round(self.contestResult.scores[1].avg)}
                            <small class="text-muted">/80</small>
                        </div>
                        <div class="small">
                            {renderRanking(self.contestResult.scores[1].rank, false)}
                            {' '}
                            {renderSd(self.contestResult.scores[1].sd, self.contestResult.scorers.length)}
                        </div>
                    </div>
                    <div class="col-3 col-md mb-2">
                        <span class="align-middle material-icons" style='font-size: 150%'>
                            join_inner
                        </span>
                        <span class="align-middle"> 合题</span>
                        <div style='font-size: 120%'>
                            {Math.round(self.contestResult.scores[2].avg)}
                            <small class="text-muted">/80</small>
                        </div>
                        <div class="small">
                            {renderRanking(self.contestResult.scores[2].rank, false)}
                            {' '}
                            {renderSd(self.contestResult.scores[2].sd, self.contestResult.scorers.length)}
                        </div>
                    </div>
                    <div class="col-3 col-md mb-2">
                        <span class="align-middle material-icons" style='font-size: 150%'>
                            tips_and_updates
                        </span>
                        <span class="align-middle"> 创意</span>
                        <div style='font-size: 120%'>
                            {Math.round(self.contestResult.scores[3].avg)}
                            <small class="text-muted">/40</small>
                        </div>
                        <div class="small">
                            {renderRanking(self.contestResult.scores[3].rank, false)}
                            {' '}
                            {renderSd(self.contestResult.scores[3].sd, self.contestResult.scorers.length)}
                        </div>
                    </div>
                    <div class="col-12 col-md-4 mb-2">
                        <span class="align-middle material-icons" style='font-size: 150%'>
                            functions
                        </span>
                        <span class="align-middle"> 评委总分</span>
                        <div style='font-size: 120%'>
                            {Math.round(self.contestResult.scores[4].avg)}
                            <small class="text-muted">/300</small>
                        </div>
                        <div class="small">
                            {renderRanking(self.contestResult.scores[4].rank, false)}
                            {' '}
                            {renderSd(self.contestResult.scores[4].sd, self.contestResult.scorers.length)}
                        </div>
                    </div>
                </div>
                <div class="row text-center">
                    <div class="col-6">
                        <a style='font-size: 150%' href="javascript:loadContestVoters()">
                            👍<small class="text-muted"> × </small>
                            {self.contestResult.votes![0].count}
                        </a>
                        <div class="small">
                            {renderRanking(self.contestResult.votes![1].rank, true)}
                            {' '}
                            <span
                                data-title="所有在 2024 年 7 月前发过 5 帖的读者均可投票；每名读者可给任意多篇作品投 👍"
                                class="mazomirror-tip"
                            >
                                <span class="material-icons std-icon">info</span>
                            </span>
                        </div>
                    </div>
                    <div class="col-6">
                        <a style='font-size: 150%' href="javascript:loadContestVoters()">
                            🥇<small class="text-muted"> × </small>
                            {self.contestResult.votes![1].count}
                        </a>
                        <div class="small">
                            {renderRanking(self.contestResult.votes![2].rank, true)}
                            {' '}
                            <span
                                data-title="所有在 2024 年 7 月前发过 5 帖的读者均可投票；每名读者只可以给一篇作品投 🥇"
                                class="mazomirror-tip"
                            >
                                <span class="material-icons std-icon">info</span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div id="mm-btn-v-thread-rev" class="card-body text-center">
                <a class="card-link" href='javascript:loadReviewThread(document.getElementById("mm-btn-v-thread-rev"))'>
                    查看评语（共 {self.contestResult.reviews} 份）
                </a>
            </div>
        </div>
    );
};

// Migrated from contest_results.inc.pug.
export const contestResults: HtmlTmpl<{
    contestResults: Awaited<ReturnType<typeof allContestResults>>
}> = (ctx, self) => {
    const { render } = ctx.state;

    const renderZ = (z: number) => (z > 0 ? '+' : '') + z.toFixed(2);
    
    type LocalThread = (typeof self.contestResults)['threads'][number];
    type ExtThread = (typeof self.contestResults)['extThreads'][number];
    const isLocalThread = (thread: LocalThread | ExtThread): thread is LocalThread => thread.thread_id > 0;

    return (
        <div class="card">
            <div class="card-header">征文比赛赛果公布</div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item">
                    <div class="my-3">
                        <h4>评委前言</h4>
                        <ul class="list-group list-group-flush">
                            {self.contestResults.forewords.map(review => (
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-auto me-auto">
                                            <a href={'/author/' + review.user_id}>
                                                <span class="material-icons std-icon">
                                                    person
                                                </span>{' '}
                                                {review.user_name}
                                            </a>
                                            ：
                                        </div>
                                        <div class="col-auto">
                                            <form
                                                action={'/post/p' + review.post_id}
                                                method="post"
                                                class="d-inline"
                                            >
                                                <button
                                                    type="submit"
                                                    style="transform: scale(0.8); transform-origin: top right;"
                                                    class="btn btn-secondary btn-sm"
                                                >
                                                    <span class="material-icons me-1">
                                                        format_quote
                                                    </span>
                                                    回复
                                                </button>
                                            </form>
                                        </div>
                                    </div>
                                    <div>{elements.safeHtml(render.contentConv(review.content))}</div>
                                </li>
                            ))}
                        </ul>
                    </div>
                </li>
                <li class="list-group-item">
                    <div class="my-3">
                        <h4>作品分数列表</h4>
                        <div class="row mb-2">
                            <div class="col-auto me-auto text-muted">
                                默认随机排序；点击每栏中的
                                <button class="btn btn-sm btn-link">
                                    <span class="material-icons" style='font-size: 100%'>
                                        sort
                                    </span>
                                </button>
                                按钮以按该栏数值排序
                            </div>
                            <div class="col-auto position-relative">
                                <a href="javascript:void(0)" data-toggle="dropdown">
                                    <span class="material-icons std-icon">view_column</span> 显示/隐藏栏目
                                </a>
                                <div class="dropdown-menu" style='min-width: 280px'>
                                    <div id="contest-results-ddm" class="px-3" />
                                    <div class="px-3 small">
                                        各项标准分计算方式：先以每个评委给所有作品打的该项分数为母体，将每个评委打的分数换算为<a href="https://zh.wikipedia.org/zh-hans/%E6%A8%99%E6%BA%96%E5%88%86%E6%95%B8">标准分数</a>；再对各个评委的标准分数取平均。因此，可减少评委习惯不同（例如给各作品评分的接近程度不同）带来的影响。
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="overflow-auto">
                            <table class="table">
                                <thead>
                                    <tr id="contest-results-hdr">
                                        <th scope="col">
                                            作品
                                            <div class="small text-muted">标签</div>
                                            <div class="small text-muted">作者</div>
                                        </th>
                                        <th scope="col" class="text-end d-none d-md-table-cell">
                                            <span class="material-icons std-icon">
                                                switch_access_shortcut
                                            </span>
                                            <div>色情</div>
                                            <div class="small text-muted">/100</div>
                                        </th>
                                        <th scope="col" class="text-end d-none">
                                            <div class="small">
                                                色情<div class="text-muted">标准分</div>
                                            </div>
                                        </th>
                                        <th scope="col" class="text-end d-none d-md-table-cell">
                                            <span class="material-icons std-icon">
                                                auto_stories
                                            </span>
                                            <div>文学</div>
                                            <div class="small text-muted">/80</div>
                                        </th>
                                        <th scope="col" class="text-end d-none">
                                            <div class="small">
                                                文学<div class="text-muted">标准分</div>
                                            </div>
                                        </th>
                                        <th scope="col" class="text-end d-none d-md-table-cell">
                                            <span class="material-icons std-icon">
                                                join_inner
                                            </span>
                                            <div>合题</div>
                                            <div class="small text-muted">/80</div>
                                        </th>
                                        <th scope="col" class="text-end d-none">
                                            <div class="small">
                                                合题<div class="text-muted">标准分</div>
                                            </div>
                                        </th>
                                        <th scope="col" class="text-end d-none d-md-table-cell">
                                            <span class="material-icons std-icon">
                                                tips_and_updates
                                            </span>
                                            <div>创意</div>
                                            <div class="small text-muted">/40</div>
                                        </th>
                                        <th scope="col" class="text-end d-none">
                                            <div class="small">
                                                创意<div class="text-muted">标准分</div>
                                            </div>
                                        </th>
                                        <th scope="col" class="text-end">
                                            <span class="material-icons std-icon">functions</span>
                                            <div>总分</div>
                                            <div class="small text-muted">/300</div>
                                        </th>
                                        <th scope="col" class="text-end d-none">
                                            <div class="small">
                                                总分<div class="text-muted">标准分</div>
                                            </div>
                                            <span class="mazomirror-tip" data-title="各单项的标准分数，按照各项的满分加权后取平均">
                                                <span class="material-icons align-middle" style='font-size: 100%'>
                                                    info
                                                </span>
                                            </span>
                                        </th>
                                        <th scope="col" class="text-end">
                                            <div style='font-size: 120%'>
                                                👍
                                            </div>
                                            <div>
                                                <span
                                                    data-title="所有在 2024 年 7 月前发过 5 帖的读者均可投票；每名读者可给任意多篇作品投 👍"
                                                    class="mazomirror-tip"
                                                >
                                                    <span class="material-icons align-middle" style='font-size: 100%'>
                                                        info
                                                    </span>
                                                </span>
                                            </div>
                                        </th>
                                        <th scope="col" class="text-end">
                                            <div style='font-size: 120%'>
                                                🥇
                                            </div>
                                            <div>
                                                <span
                                                    data-title="所有在 2024 年 7 月前发过 5 帖的读者均可投票；每名读者只可以给一篇作品投 🥇"
                                                    class="mazomirror-tip"
                                                >
                                                    <span class="material-icons align-middle" style='font-size: 100%'>
                                                        info
                                                    </span>
                                                </span>
                                            </div>
                                        </th>
                                        <th scope="col" class="text-end d-none">
                                            <div style='font-size: 120%'>
                                                👍+🥇
                                            </div>
                                        </th>
                                        <th scope="col" class="text-end d-none">
                                            <div>投稿时间</div>
                                            <div>
                                                <span
                                                    data-title="格式为月-日；年份均为 2024 年；以 UTC（协调世界时）而非你的时区计算"
                                                    class="mazomirror-tip"
                                                >
                                                    <span class="material-icons align-middle" style='font-size: 100%'>
                                                        info
                                                    </span>
                                                </span>
                                            </div>
                                        </th>
                                        <th scope="col" class="text-end d-none">
                                            <div>回复数</div>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {[...self.contestResults.threads, ...self.contestResults.extThreads].map(thread => {
                                        let threadRtg = self.contestResults.results[thread.thread_id];
                                        return [
                                            <tr>
                                                <td class="pb-0">
                                                    <a href={thread.firstLink}>
                                                        {render.contentConv(thread.thread_title)}
                                                    </a>
                                                </td>
                                                <td
                                                    data-sort-v={threadRtg.scores[0].avg}
                                                    class="pb-0 text-end d-none d-md-table-cell"
                                                >
                                                    {Math.round(threadRtg.scores[0].avg)}
                                                </td>
                                                <td
                                                    data-sort-v={threadRtg.scores[0].z}
                                                    class="pb-0 text-end d-none"
                                                >
                                                    {renderZ(threadRtg.scores[0].z)}
                                                </td>
                                                <td
                                                    data-sort-v={threadRtg.scores[1].avg}
                                                    class="pb-0 text-end d-none d-md-table-cell"
                                                >
                                                    {Math.round(threadRtg.scores[1].avg)}
                                                </td>
                                                <td
                                                    data-sort-v={threadRtg.scores[1].z}
                                                    class="pb-0 text-end d-none"
                                                >
                                                    {renderZ(threadRtg.scores[1].z)}
                                                </td>
                                                <td
                                                    data-sort-v={threadRtg.scores[2].avg}
                                                    class="pb-0 text-end d-none d-md-table-cell"
                                                >
                                                    {Math.round(threadRtg.scores[2].avg)}
                                                </td>
                                                <td
                                                    data-sort-v={threadRtg.scores[2].z}
                                                    class="pb-0 text-end d-none"
                                                >
                                                    {renderZ(threadRtg.scores[2].z)}
                                                </td>
                                                <td
                                                    data-sort-v={threadRtg.scores[3].avg}
                                                    class="pb-0 text-end d-none d-md-table-cell"
                                                >
                                                    {Math.round(threadRtg.scores[3].avg)}
                                                </td>
                                                <td
                                                    data-sort-v={threadRtg.scores[3].z}
                                                    class="pb-0 text-end d-none"
                                                >
                                                    {renderZ(threadRtg.scores[3].z)}
                                                </td>
                                                <td
                                                    data-sort-v={threadRtg.scores[4].avg}
                                                    class="pb-0 text-end"
                                                >
                                                    {Math.round(threadRtg.scores[4].avg)}
                                                </td>
                                                <td
                                                    data-sort-v={threadRtg.scores[4].z}
                                                    class="pb-0 text-end d-none"
                                                >
                                                    {renderZ(threadRtg.scores[4].z)}
                                                </td>
                                                {!('votes' in threadRtg) ? (
                                                    <>
                                                        <td class="pb-0 text-end">-</td>
                                                        <td class="pb-0 text-end">-</td>
                                                        <td class="pb-0 text-end d-none">-</td>
                                                    </>
                                                ) : (
                                                    <>
                                                        <td class="pb-0 text-end">
                                                            <a
                                                                href={`javascript:loadContestVoters(${thread.thread_id})`}
                                                                class="text-reset"
                                                            >
                                                                {Math.round(threadRtg.votes[0].count)}
                                                            </a>
                                                        </td>
                                                        <td class="pb-0 text-end">
                                                            <a
                                                                href={`javascript:loadContestVoters(${thread.thread_id})`}
                                                                class="text-reset"
                                                            >
                                                                {Math.round(threadRtg.votes[1].count)}
                                                            </a>
                                                        </td>
                                                        <td class="pb-0 text-end d-none">
                                                            <a
                                                                href={`javascript:loadContestVoters(${thread.thread_id})`}
                                                                class="text-reset"
                                                            >
                                                                {Math.round(threadRtg.votes[2].count)}
                                                            </a>
                                                        </td>
                                                    </>
                                                )}
                                                <td
                                                    data-sort-v={1724000000 - thread.firstTime}
                                                    class="pb-0 d-none"
                                                >
                                                    {new Date(thread.firstTime * 1000)
                                                        .toISOString()
                                                        .substring(5, 10)}
                                                </td>
                                                {!isLocalThread(thread) ? (
                                                    <td class="pb-0 text-end d-none">-</td>
                                                ) : (
                                                    <td class="pb-0 text-end d-none">
                                                        {thread.count - 1}
                                                    </td>
                                                )}
                                            </tr>,
                                            <tr>
                                                <td colspan={42} class="border-top-0 pt-2">
                                                    {!isLocalThread(thread) ? (
                                                        <div class="small text-muted">
                                                            （未在本站发布）
                                                        </div>
                                                    ) : tagList(ctx, { list: thread.tags ?? [], tagStyle: self.contestResults.threads.tagStyle })}
                                                    <div class="row">
                                                        <div class="col-auto me-auto">
                                                            {!isLocalThread(thread) ? (
                                                                <small class="text-muted">
                                                                    <span class="material-icons std-icon">
                                                                        person
                                                                    </span>
                                                                    （非本站用户）
                                                                </small>
                                                            ) : (
                                                                <a
                                                                    href={'/author/' + thread.ownerId}
                                                                    class="small"
                                                                >
                                                                    <span class="material-icons std-icon">
                                                                        person
                                                                    </span>
                                                                    {' '}
                                                                    {thread.firstUser}
                                                                </a>
                                                            )}
                                                        </div>
                                                        <div class="col-auto">
                                                            <a
                                                                href="javascript:void(0)"
                                                                data-rev-tgr-thread={thread.thread_id}
                                                                class="small text-reset"
                                                            >
                                                                评语（{threadRtg.reviews} 份）
                                                            </a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>,
                                        ];
                                    })}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </li>
            </ul>
            <script type="text/javascript">{elements.safeHtml(`initResultsThreadList();`)}</script>
        </div>
    );
};

// Migrated from review_thread.pug.
export const reviewThread: HtmlTmpl<{
    scoreUsers: { user_id: number, user_name: string }[],
    reviews: {
        user_id: number, user_name: string, post_id: number, content: string
    }[]
}> = (ctx, self) => (
    <ul class="list-group list-group-flush">
        <li class="list-group-item small">
            {'上述评分为评委 '}
            {self.scoreUsers.map((user, index) => [
                index ? '、' : null,
                <a href={'/author/' + user.user_id}>{user.user_name}</a>,
            ])}
            {' 打分的平均值。'}
        </li>
        {self.reviews.map(review => (
            <li class="list-group-item">
                <div class="row">
                    <div class="col-auto me-auto">
                        评委 <a href={'/author/' + review.user_id}>{review.user_name}</a> 的评语：
                    </div>
                    <div class="col-auto">
                        <form
                            action={'/post/p' + review.post_id}
                            method="post"
                            class="d-inline"
                        >
                            <button
                                type="submit"
                                style="transform: scale(0.8); transform-origin: top right"
                                class="btn btn-secondary btn-sm"
                            >
                                <span class="material-icons me-1">format_quote</span>
                                回复
                            </button>
                        </form>
                    </div>
                </div>
                <div>{elements.safeHtml(ctx.state.render.contentConv(review.content))}</div>
            </li>
        ))}
    </ul>
);

// Migrated from campaign_votes_archive.pug.
export const campaignVotesArchive: HtmlTmpl<{
    voters2: { user_id: number, user_name: string }[],
    voters1: { user_id: number, user_name: string }[]
}> = (ctx, self) => (
    <>
        <h5>以下用户为此作品投出了 👍 及 🥇：</h5>
        <p>
            {self.voters2.length
                ? self.voters2.map((v, i) => [
                        i ? '、' : null,
                        <a href={'/author/' + v.user_id}>{v.user_name}</a>,
                    ])
                : '无'}
        </p>
        <h5>以下用户为此作品投出了 👍：</h5>
        <p>
            {self.voters1.length
                ? self.voters1.map((v, i) => [
                        i ? '、' : null,
                        <a href={'/author/' + v.user_id}>{v.user_name}</a>,
                    ])
                : '无'}
        </p>
    </>
);
