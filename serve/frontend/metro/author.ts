import { $, $id, $set, $xhr, loadingSpinner, showModal } from '../common-import';

const getAuthorIdFromUri = (): number => {
    const path = location.pathname.match(/^\/*author\/(\d+)/);
    if (!path) { throw new Error('URL not recognized.'); }
    return parseInt(path[1]);
};

const style = 'border-radius: 50% 50% 0 50%; width: 120px; height: 120px; position: relative; overflow: hidden';

export const initPinnedThreads = (pinnedThreadCount: number) => {
    const tbody = $id('author-page-thread-table')!.querySelector('tbody')!;
    const trs = tbody.children;
    for (let i = 0; i < pinnedThreadCount; i++) {
        const td = trs[i].firstElementChild!;
        td.appendChild($('div', 'text-center mazomirror-tip', $('span', { className: 'material-icons', style: 'font-size: 100%' }, 'push_pin'))).also(tip => tip.setAttribute('data-title', '此主题由作者选择置顶展示'));
    }
};

type ApiAuthorIdThreadsResponse = [
    [number, string, string][],
    number[]
];

export const pinnedThreadsMgmt = () => {
    const container = $('div', void 0, [loadingSpinner(false)]);
    showModal('设置此页面置顶主题帖', container);

    $xhr('/api/author/' + getAuthorIdFromUri() + '/threads', {
        j: ([threads, pinned]: ApiAuthorIdThreadsResponse) => {
            const pinnedG = $('ul', 'list-group list-group-flush');
            const othersG = $('ul', 'list-group list-group-flush');

            const threadsMap: { [threadIdStr: string]: [[number, string, string], HTMLLIElement] } = {};

            const createSingle = (threadId: number, threadTitle: string, posted: string, children: HTMLElement[]) =>
                $('li', 'list-group-item', [
                    $('div', 'float-start', [
                        $('a', { href: '/thread/' + threadId, className: 'text-reset' }, threadTitle, e => e.preventDefault()),
                        $('a', { href: '/thread/' + threadId, target: '_blank' }, $('span', { className: 'material-icons align-middle ms-1', style: 'font-size: 100%' }, 'open_in_new')),
                        $('br'),
                        $('span', { className: 'text-muted', innerHTML: posted })
                    ]),
                    $('div', 'float-end', children)
                ]);

            const enableAndDisableCorrespondingly = () => {
                for (const single of Array.from(pinnedG.children)) {
                    const previousNextButtons = Array.from(single.querySelectorAll('button.btn-link')) as HTMLButtonElement[];
                    previousNextButtons[0].disabled = !single.previousElementSibling;
                    previousNextButtons[1].disabled = !single.nextElementSibling;
                }
            };

            const addToPinned = (threadId: string | number) => {
                const entry = threadsMap[threadId];
                entry[1].style.display = 'none';
                const single = createSingle(entry[0][0], entry[0][1], entry[0][2], [
                    $('button', { className: 'btn btn-link', type: 'button', title: '上移' }, $('span', 'material-icons', 'keyboard_arrow_up'), () => {
                        pinnedG.insertBefore(single, single.previousElementSibling);
                        enableAndDisableCorrespondingly();
                    }),
                    $('button', { className: 'btn btn-link', type: 'button', title: '下移' }, $('span', 'material-icons', 'keyboard_arrow_down'), () => {
                        pinnedG.insertBefore(single.nextElementSibling!, single);
                        enableAndDisableCorrespondingly();
                    }),
                    $('button', { className: 'btn btn-link', type: 'button', title: '移出置顶' }, $('span', 'material-icons', 'move_down'), () => {
                        entry[1].style.display = 'block';
                        pinnedG.removeChild(single);
                        enableAndDisableCorrespondingly();
                    }),
                    $('input', { type: 'hidden', name: 'pinned', value: threadId })
                ]);
                pinnedG.appendChild(single);
                enableAndDisableCorrespondingly();
            };

            const clickAddToPinned = (e: MouseEvent) => {
                if (pinnedG.children.length >= 5) {
                    alert('不可置顶超过五个主题。');
                    return;
                }
                const button = e.currentTarget as HTMLButtonElement;
                const li = button.parentNode!.parentNode as HTMLLIElement;
                addToPinned(li.querySelector('a')!.href.match(/thread\/(\d+)$/)![1]);
            };

            for (const thread of threads) {
                const single = createSingle(thread[0], thread[1], thread[2], [
                    $('button', { className: 'btn btn-link', type: 'button', title: '移入置顶' }, $('span', 'material-icons', 'move_up'), clickAddToPinned)
                ]);
                othersG.appendChild(single);
                threadsMap[thread[0]] = [thread, single];
            }

            for (const pinnedRow of pinned) {
                addToPinned(pinnedRow);
            }

            $set(container, $('form', { method: 'post' }, [
                $('div', 'card', [
                    $('div', 'card-header', '置顶主题帖（不超过五个）'),
                    pinnedG
                ]),
                $('div', 'text-center my-3', $('input', { className: 'btn btn-primary', type: 'submit', name: 'pinned', value: '保存' })),
                $('div', 'card', [
                    $('div', 'card-header', '其它主题帖（按发帖时间排序）'),
                    othersG
                ])
            ]));
        },
        e: () => $set(container, $('div', 'alert alert-danger', '加载失败啦 °(°ˊДˋ°) °'))
    });
};

export const initProfileEdit = () => {
    const editorWrapper = $id('edit-introduction') as HTMLDivElement;
    
    (window as any)['editIntro'] = () => {
        editorWrapper.style.setProperty('display', 'block', 'important');
        (editorWrapper.nextSibling as HTMLDivElement).style.setProperty('display', 'none');
    };

    (window as any)['cancelEditIntro'] = () => {
        editorWrapper.style.removeProperty('display');
        (editorWrapper.nextSibling as HTMLDivElement).style.removeProperty('display');
    };

    (window as any)['editAvatarPopup'] = (avatarSmallInt: number, email: string, thirdPartySrc: string, lastLocalAvatar: string) => {
        const defaultAvatar = $id('profile-avatar-container')!.querySelector('.mm-def-ava') as HTMLDivElement;

        let form: HTMLFormElement;
        let firstPartyContainer: HTMLElement;
        let radioToEnableThirdParty: HTMLInputElement;
        let radioToEnableFirstParty: HTMLInputElement;

        let blobToUpload: Blob | null = null;
        let locked = false;
        showModal('头像设置', form = $('form', { method: 'post' }, [
            $('ul', 'list-group list-group-flush', [
                $('li', 'list-group-item d-flex align-items-center', [
                    $('div', {
                        'className': 'flex-shrink-0',
                        'style': style
                    }, (defaultAvatar.cloneNode(true) as HTMLElement).also(tag => { tag.style.visibility = 'visible'; })),
                    $('div', 'flex-grow-1 ms-2', [
                        $('label', undefined, [
                            $('input', { 'type': 'radio', 'name': 'avatar', 'value': '-1', 'checked': !avatarSmallInt }),
                            ' 使用生成的固定图案'
                        ])
                    ])
                ]),
                $('li', 'list-group-item d-flex align-items-center', [
                    $('img', {
                        'src': thirdPartySrc,
                        'className': 'flex-shrink-0',
                        'style': style,
                        'referrerPolicy': 'no-referrer',
                    }).also(img => img.addEventListener('error', ({ currentTarget }) => {
                        (currentTarget as HTMLElement).parentNode?.replaceChild($('div', { 'className': 'flex-shrink-0 text-center', 'style': style + '; line-height: 120px' }, '无'), (currentTarget as HTMLElement));
                    })),
                    $('div', 'flex-grow-1 ms-2', [
                        $('label', undefined, [
                            radioToEnableThirdParty = $('input', { 'type': 'radio', 'name': 'avatar', 'value': '1', 'checked': avatarSmallInt && !(avatarSmallInt < -1) }),
                            ` 使用与 ${email} 关联的头像`
                        ]),
                        $('small', 'd-block', [
                            '可在 ',
                            $('a', { href: 'https://cravatar.com/', rel: 'noreferrer' }, 'Cravatar.com'),
                            ' 或 ',
                            $('a', { href: 'https://gravatar.com/', rel: 'noreferrer' }, 'Gravator.com'),
                            ` 为 ${email} 设置统一头像；因缓存，设置后需很久才会生效。若长时间未生效，你也可`,
                            $('a', { href: 'javascript:void(0)' }, '试着刷新缓存', () => {
                                alert('现在将启用第三方头像并试图刷新缓存。请注意在十分钟内再试图刷新不会有任何效果。');
                                radioToEnableThirdParty.checked = true;
                                radioToEnableThirdParty.value = '2';
                                form.submit();
                            }),
                            '。'
                        ]),
                    ])
                ]),
                $('li', 'list-group-item d-flex align-items-center', [
                    firstPartyContainer = $('div', {
                        'className': 'flex-shrink-0',
                        'style': style
                    }, lastLocalAvatar ? $('img', { 'width': '120', 'height': '120', 'src': lastLocalAvatar }) : $('div', { 'className': 'text-center', 'style': 'line-height: 120px' }, '无')),
                    $('div', 'flex-grow-1 ms-2 overflow-hidden', [
                        $('label', undefined, [
                            radioToEnableFirstParty = $('input', { 'type': 'radio', 'name': 'avatar', 'value': '-2', 'checked': (avatarSmallInt < -1), 'disabled': !lastLocalAvatar }),
                            ` 使用直接上传的头像`
                        ]),
                        $('input', { 'type': 'file', 'accept': 'image/*' }, undefined, { 'change': e => {
                            const file = (e.target as HTMLInputElement).files?.[0]; // get the file
                            if (!file) { return; }
                            const url = URL.createObjectURL(file);
                            const image = new Image();
                            image.onload = () => {
                                if (!((image.width >= 240) && (image.height >= 240))) { alert('失败：图片太小了，请确认长宽均不低于 240 像素。'); return; }
                                const outDim = Math.min(image.width, image.height, 480);
                                const canvas = $('canvas');
                                canvas.style.width = '120px';
                                canvas.style.height = '120px';
                                canvas.width = outDim;
                                canvas.height = outDim;
                                const ctx = canvas.getContext('2d')!;
                                const srcDim = Math.min(image.width, image.height);
                                ctx.drawImage(image, ((image.width - srcDim) / 2) | 0, ((image.height - srcDim) / 2) | 0, srcDim, srcDim, 0, 0, outDim, outDim);
                                URL.revokeObjectURL(url);
                                $set(firstPartyContainer, canvas);
                                canvas.toBlob(blob => { 
                                    blobToUpload = blob;
                                    radioToEnableFirstParty.disabled = !blob;
                                    radioToEnableFirstParty.checked = !!blob;
                                }, 'image/webp', 0.8);
                            };
                            image.onerror = () => alert('失败：无法识别图片文件格式。');
                            image.src = url;
                        }})
                    ])
                ]),
            ]),
            $('div', 'text-center', $('button', { 'className': 'btn btn-primary', 'type': 'submit' }, '保存'))
        ], { submit: e => {
            if (!blobToUpload) { return; }
            if (!radioToEnableFirstParty.checked) { return; }
            e.preventDefault();
            if (locked) { return; }
            locked = true;
            $xhr('/api/avatar?src=manual', {
                f: blobToUpload,
                e: error => { alert('头像上传失败：' + error); locked = false; },
                t: () => location.reload()
            });
        }}));
    };
};

export const loadAchisInAuthor = () => {
    const targetContainer = $id('per-user-achivement-tgt')!;
    $xhr('/api/author/' + getAuthorIdFromUri() + '/achievements' + location.search, {
        f: null,
        e: error => $set(targetContainer, $('div', 'alert alert-danger', '加载失败：' + error)),
        t: html => {
            targetContainer.innerHTML = html;
            const selector = targetContainer.querySelector('#title-selector');
            if (selector) {
                selector.addEventListener('change', () => {
                    (selector.parentNode as HTMLFormElement).submit();
                });
            }
            const row = targetContainer.querySelector('.row');
            if (!row) { console.warn('未找到成就列表～'); return; }
            const singles = Array.from(row.children);
            if (!singles.some(single => single.classList.contains('d-none'))) { return; }
            targetContainer.appendChild($('button', 'btn btn-secondary mt-2', '显示尚未解锁的成就', e => {
                for (const single of singles) {
                    single.classList.remove('d-none');
                }
                const self = e.currentTarget as HTMLElement;
                self.parentNode!.removeChild(self);
            }));
        }
    });
};

// `identifierName` is always SC, but `displayName` can be SC/TC. For SC users the two would be the same.
export const showArcLi = (identifierName: string, displayName: string) => {
    const container = $('div', void 0, [loadingSpinner(false)]);
    showModal('「' + displayName + '」的进度详情', container);
    $xhr('/api/author/' + getAuthorIdFromUri() + '/achievements?name=' + identifierName, {
        e: error => $set(container, $('div', 'alert alert-danger', '加载失败：' + error)),
        t: html => { container.innerHTML = html; }
    });
};

// Check if a third-party avatar is available, and record the result.
export const avatarChecker = (avatarUrl: string, enabled: boolean) => {
    const image = new Image();
    if (!('crossOrigin' in image)) { return; }

    const canvas = $('canvas');
    if (!canvas.toBlob) { return; }

    image.crossOrigin = 'anonymous';

    setTimeout(() => {
        const report = (content: string | Blob) => {
            const wrapped = (typeof content === 'string') ? new Blob([content]) : content;
            $xhr('/api/avatar?src=auto', {
                f: wrapped,
                e: console.error,
                t: console.log
            });
        };

        const onSuccess = (imgBlob: Blob) => { // We have the blob! Show it to users then.
            if (enabled) {
                report(imgBlob);
            } else {
                canvas.style.width = '120px';
                canvas.style.height = '120px';
                const closeModal = showModal('设置头像？', [
                    $('p', undefined, '你尚未设置头像。是否将下面的图片设置为你的头像？'),
                    $('p', 'text-center', $('img', {
                        'src': avatarUrl,
                        'style': style
                    })),
                    $('p', 'small text-center', '日后你随时可在个人页面修改你的选择。'),
                    $('div', 'text-center', [
                        $('button', 'btn btn-primary', '使用此头像', () => {
                            report(imgBlob);
                            closeModal();
                        }),
                        ' ',
                        $('button', 'btn btn-secondary', '不设置头像', () => {
                            report('DECLINE');
                            closeModal();
                        })
                    ])
                ]);
            }
        };

        image.onload = () => {
            if ((image.width !== image.height) || !(image.width >= 240)) { report('FAIL-F'); return; }

            canvas.width = image.width;
            canvas.height = image.height;
            const context = canvas.getContext('2d')!;
            context.drawImage(image, 0, 0);

            const imageData = context.getImageData(0, 0, image.width, image.height);
            let isEmpty = true;
            for (let i = 3; i < imageData.data.length; i += 4) {
                if (imageData.data[i] !== 0) { isEmpty = false; break; }
            }

            if (isEmpty) { report('NONE'); return; }

            canvas.toBlob(blob => {
                if (!blob) { report('FAIL-C'); } else { onSuccess(blob); }
            }, 'image/webp', 0.8);
        };

        image.onerror = () => {
            report('FAIL-N');
        };

        image.src = avatarUrl;
    });
};
