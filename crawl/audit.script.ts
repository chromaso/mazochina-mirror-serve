/*
DB schema

CREATE TABLE stats.thread_audit (
    thread_id INTEGER NOT NULL PRIMARY KEY,
    forum_id INTEGER NOT NULL DEFAULT 0,
    title TEXT NULL,
    error TEXT NULL,
    verified_at INTEGER NOT NULL
);
*/

import { crawlThreadPage } from './crawl.subs';
import { query } from './db';

const sleep = (ms: number) => new Promise(res => setTimeout(res, ms));

const main = async () => {
    for (let thread = 1; thread < 55655; thread++) {
        const now = Math.round(Date.now() / 1000);
        try {
            const result = await crawlThreadPage(22, thread, 1);
            await query('INSERT INTO stats.thread_audit (thread_id, forum_id, title, verified_at) VALUES ($1, $2, $3, $4)', [thread, result.forumId, result.title, now]);
            console.log(`Thread ${thread} found: ${result.forumId} ${result.title.substring(0, 20)}.`);
        } catch (e) {
            const message = (e as Error)?.message || String(e);
            await query('INSERT INTO stats.thread_audit (thread_id, error, verified_at) VALUES ($1, $2, $3)', [thread, message, now]);
            console.log(`Thread ${thread} Bad: ${message.split('\n').shift()!.substring(0, 20)}.`);
        }
        await sleep(Math.random() * 500 + 750);
    }
};

main().then(() => {
    console.log('Done!');
}, e => {
    console.error('FATAL', e);
});