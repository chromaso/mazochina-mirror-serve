import { $id, $set, $xhr } from '../common-import';

const init = () => {
    if (location.hash === '#collections') {
        const inner = $id('coll-card-inner')!;
        $xhr('/api/collections', {
            e: e => $set(inner, e),
            t: response => {
                inner.innerHTML = response;
            }
        });
    }
};

export const sideEffect = () => {
    window.addEventListener('hashchange', init);
    init();
};