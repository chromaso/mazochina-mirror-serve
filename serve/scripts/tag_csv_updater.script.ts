import * as phin from 'phin';
import * as yesno from 'yesno';

import { withTransaction } from '../db';
import { CAT_TITLES, CategoryId, F18_CONSTS, allTagsById } from '../shared/tags';

const CAT_IDS: Readonly<Record<string, CategoryId>> = Object.fromEntries(
    CAT_TITLES
        .map((title, index) => [title, index as CategoryId])
        .filter(_ => _) // Needed as `CAT_TITLES` is sparse.
    );

type DAO = {
    category: number,
    primary_name: string,
    aliases: string[] | undefined,
    desc_html: string,
    flags: number,
    preExists: boolean
};

const NAME_REGEX = /^([A-Za-z0-9\-\/]|[\u4E00-\u9FA5]|[\u3400-\u4DB5])+$/

const main = async () => {
    const resp = await phin({ url: 'https://docs.google.com/spreadsheets/d/1JQ7T1glEOFAc3hifdZrV95OQUHcfNEbSeQmb-MYBZKQ/gviz/tq?tqx=out:csv&sheet=Main', timeout: 15000, parse: 'string' });
    if (resp.statusCode !== 200) { throw new Error(`Response ${resp.statusMessage} + ${resp.body}`); }
    const csv = resp.body;

    const outSqls: [string[], string[]] = [[], []];
    const addSql = (sql: string, priority: 1 | 2) => outSqls[priority - 1].push(sql);

    const tagsOut = new Map<string, DAO>();
    const taxonomyOut: [string, string][] = [];

    const cnts = new Map<number, number>();
    const nextCnt = (category: number, group: 256 | 512 | 768 | 1024): number => {
        const index = category + group;
        const newValue = (cnts.get(index) ?? 0) + 1;
        cnts.set(index, newValue);
        return newValue;
    };

    let currentGroupId: number | undefined;
    const rows = csv.split('\n').map(row => row.trim());
    const index = rows.findIndex(row => row.replace(/"/g, '') === '名称,别名（alias）,常见度,父级标签,描述,备注');
    if (index < 0) { throw new Error('Header not found'); }
    for (const row of rows.slice(index + 1)) {
        if (!row) { continue; }
        const cells = row.split(',').map(cell => {
            const QUOTE = '~DQUOTE~';
            // Might be wrapped with `"`.
            const content = cell.trim().replace(/^"(.*)"$/, '$1').replace(/""/g, QUOTE); // https://stackoverflow.com/a/17808731
            if (content.includes('"')) { throw new Error(`Unexpect " in ${cell} (row ${row})`); }
            return content.replace(new RegExp(QUOTE, 'g'), '"');
        });
        if (cells.length !== 6) { throw new Error('Cell number weird for row ' + row); }
        if (cells.slice(1).every(c => c === '')) {
            // This is a group header.
            currentGroupId = CAT_IDS[cells[0]];
            if (!currentGroupId) { throw new Error('Group ' + cells[0] + ' not recognized'); }
        } else {
            if (!currentGroupId) { throw new Error('Not in a group'); }
            let flags = 0x0;
            let name = cells[0];
            if (name.endsWith('⚠')) {
                flags |= 0x1;
                name = name.substring(0, name.length - 1).trim();
            }
            if (!NAME_REGEX.test(name)) { throw new Error(`Illegal name ${name}`); }
            const aliases = cells[1].split('、').filter(a => a);
            for (const alias of aliases) {
                if (!NAME_REGEX.test(alias)) { throw new Error(`Illegal alias ${alias}`); }
            }
            if (cells[2].startsWith('不可选')) {
                flags |= 0x2;
                flags |= ((256 - nextCnt(currentGroupId, 256)) << 3) & 0x1ff8;
            } else if (cells[2].startsWith('罕见')) {
                flags |= ((512 - nextCnt(currentGroupId, 512)) << 3) & 0x1ff8;
            } else if (cells[2].startsWith('一般')) {
                flags |= ((768 - nextCnt(currentGroupId, 768)) << 3) & 0x1ff8;
            } else if (cells[2].startsWith('常见')) {
                flags |= ((1024 - nextCnt(currentGroupId, 1024)) << 3) & 0x1ff8;
            } else {
                throw new Error('Rarity not recognized'); 
            }
            const parents = cells[3].split('、').filter(a => a);
            for (const parent of parents) {
                taxonomyOut.push([parent, name]);
            }
            tagsOut.set(name, {
                aliases: aliases.length ? aliases : undefined,
                desc_html: cells[4],
                primary_name: name,
                flags,
                category: currentGroupId,
                preExists: false
            });
        }
    }

    for (const [tagName, tag] of tagsOut) {
        if ((tag.flags & 0x2) && (!taxonomyOut.some(([parent, _]) => parent === tagName))) {
            throw new Error(`Tag is not selectable but not a parent either!`);
        }
    }
    for (const [parent, child] of taxonomyOut) {
        if (!tagsOut.has(parent)) {
            throw new Error(`${child}'s parent ${parent} not found!`);
        }
    }

    const allExistingTags = await allTagsById();
    for (const tag of allExistingTags.values()) {
        if (tag.category < F18_CONSTS.CATS._FIRST_ARBITRARY) { continue; }
        const newVersion = tagsOut.get(tag.primary_name);
        if (!newVersion) {
            console.error(`[FATAL] Tag ${tag.primary_name}（${tag.tag_id}） gets deleted!`); continue;
        }
        const updates: string[] = [];
        if (newVersion.category !== tag.category) { 
            console.warn(`> Tag ${tag.primary_name}（${tag.tag_id}）category changed from ${tag.category} to ${newVersion.category}!`);
            updates.push(`category = ${newVersion.category}`);
        }
        if (JSON.stringify(newVersion.aliases ?? null) !== JSON.stringify(tag.aliases ?? null)) {
            updates.push(`aliases = ${newVersion.aliases ? `ARRAY['${newVersion.aliases.join(`', '`)}']` : 'NULL'}`);
        }
        if (newVersion.desc_html !== tag.desc_html) {
            updates.push(`desc_html = '${newVersion.desc_html.replace(/'/g, '\\\'')}'`);
        }
        if (newVersion.flags !== tag.flags) {
            updates.push(`flags = ${newVersion.flags}`);
        }
        if (updates.length) {
            addSql(`UPDATE tags SET ${updates.join(', ')} WHERE tag_id = ${tag.tag_id}`, 1);
        }
        const newParents = taxonomyOut.filter(([parent, child]) => child === tag.primary_name).map(([parent]) => parent);
        if (tag.parentTags) {
            for (const parentTag of tag.parentTags) {
                const index = newParents.indexOf(parentTag.primary_name);
                if (index < 0) {
                    console.warn(`> Relationship [${parentTag.primary_name}, ${tag.primary_name}] gone!`);
                    addSql(`DELETE FROM tags_taxonomy WHERE parent_tag = ${parentTag.tag_id} AND child_tag = ${tag.tag_id}`, 2);
                } else {
                    newParents.splice(index, 1);
                }
            }
        }
        for (const newParent of newParents) {
            console.info(`> Relationship [${newParent}, ${tag.primary_name}] to be added.`);
            addSql(`INSERT INTO tags_taxonomy (parent_tag, child_tag) VALUES ((SELECT tag_id FROM tags WHERE primary_name = '${newParent}'), ${tag.tag_id})`, 2);
        }
        newVersion.preExists = true;
    }

    for (const newTag of tagsOut.values()) {
        if (newTag.preExists) { continue; }
        console.log(`> Adding a new tag ${newTag.primary_name}.`);
        addSql(`INSERT INTO tags (category, primary_name, aliases, desc_html, flags) VALUES (${newTag.category}, '${newTag.primary_name}', ${newTag.aliases ? `ARRAY['${newTag.aliases.join(`', '`)}']` : 'NULL'}, '${newTag.desc_html.replace(/'/g, '\\\'')}', ${newTag.flags})`, 1);
        const parents = taxonomyOut.filter(([parent, child]) => child === newTag.primary_name).map(([parent]) => parent);
        if (parents.length) {
            console.log(`> Adding ${parents.length} parents for ${newTag.primary_name}`);
            for (const parent of parents) {
                addSql(`INSERT INTO tags_taxonomy (parent_tag, child_tag) VALUES ((SELECT tag_id FROM tags WHERE primary_name = '${parent}'), (SELECT tag_id FROM tags WHERE primary_name = '${newTag.primary_name}'))`, 2);
            }
        }
    }

    console.log('#### Output SQL ####');
    for (const group of outSqls) {
        for (const sql of group) {
            console.log(sql + ';');
        }
    }

    if (await yesno({ question: 'Do those operations?', defaultValue: false })) {
        const maxTagId = await withTransaction(async txn => {
            for (const group of outSqls) {
                await Promise.all(group.map(sql => txn(sql)));
            }
            return (await txn('SELECT MAX(tag_id) AS mtid FROM tags')).rows[0].mtid;
        });
        console.log(`SQL executed. Max tag ID is ${maxTagId} now.`);
    } else {
        console.log('SQL not executed.');
    }
};

if (require.main === module) {
    main();
}