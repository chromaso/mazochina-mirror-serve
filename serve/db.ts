import { Pool, PoolClient, QueryConfigValues, QueryResult, types } from 'pg';
import { dbPassword } from './conf';

types.setTypeParser(20, BigInt);

const pool = new Pool({
    user: 'mazomirror',
    host: '127.0.0.1',
    database: 'mazomirror',
    password: dbPassword
});

export const busy = (): boolean => pool.idleCount < pool.totalCount;

// async
export const query = <Out extends Record<string, any> = any, In extends any[] = any[]>(sql: string, values?: QueryConfigValues<In>): Promise<QueryResult<Out>> => pool.query<Out, In>(sql, values);

export const parameterizedQuery = <Out extends Record<string, any> = any>(sqlMaker: (addParam: (value: any) => string) => string): ReturnType<typeof query<Out>> => {
    const params: any[] = [];
    return query(sqlMaker(value => {
        params.push(value);
        return '$' + params.length;
    }), params);
};

export const withTransaction = async <T>(withClient: (query: PoolClient['query']) => (Promise<T> | T)): Promise<T> => {
    const client = await pool.connect();
    try {
        await client.query('BEGIN');
        const res = await withClient(client.query.bind(client));
        await client.query('COMMIT');
        return res;
    } catch (e) {
        await client.query('ROLLBACK');
        throw e;
    } finally {
        client.release();
    }
};