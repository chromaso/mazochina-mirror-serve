import { $, $id, $set, $xhr, debugLog } from '../common-import';

const localStorageUpdatedKey = 'mazomirror-notifications-updated';
const localStorageDataKey = 'mazomirror-notifications-data';

type IdPrefix = 'fav' | 'noti' | 'pm';

const elementSet = (prefix: IdPrefix) => ({
    _prop_noneBtn: $id(prefix + '-none')!,
    _prop_activeBtn: $id(prefix + '-active')!,
    _prop_inner: $id(prefix + '-card-inner')!,
    _prop_dismissAllBtn: $id(prefix + '-dism-b') // This is intentially nullable as `pm` does not have one.
});

const TYPES: IdPrefix[] = ['fav', 'noti', 'pm'];

const updateSingleWithResponse = (fragment: HTMLElement, prefix: IdPrefix): void => {
    const els = elementSet(prefix);
    const content = fragment.querySelector(`#${prefix}-card-inner`) as HTMLElement;
    const items = Array.from(content.getElementsByTagName('li'));
    const unreadIds: number[] = [];
    const doDismiss = (ids: number[]) => {
        // Invalidate the cache.
        localStorage.removeItem(localStorageUpdatedKey);
        localStorage.removeItem(localStorageDataKey);
        $xhr('/api/notifications', {
            f: ids.map(id => 'id=' + id).join('&'),
            e: console.warn,
            t: console.log
        });
        if (!unreadIds.length) {
            els._prop_noneBtn.classList.remove('d-none');
            els._prop_activeBtn.classList.add('d-none');
            els._prop_dismissAllBtn?.classList.add('d-none');
        }
    };
    const listener = function (this: HTMLLIElement) {
        const id = parseInt(this.id.substr(3));
        const index = unreadIds.indexOf(id);
        if (index < 0) { return; }        
        doDismiss(unreadIds.splice(index, 1));
        this.removeEventListener('click', listener);
        this.classList.add('dismissed');
    };
    for (const entry of items) {
        if (entry.classList.contains('dismissed')) { continue; }
        unreadIds.push(parseInt(entry.id.substr(3)));
        entry.addEventListener('click', listener);
    }
    els._prop_dismissAllBtn?.addEventListener('click', () => {
        for (const entry of items) {
            if (entry.classList.contains('dismissed')) { continue; }
            entry.removeEventListener('click', listener);
            entry.classList.add('dismissed');
        }
        doDismiss(unreadIds.splice(0, unreadIds.length));
    });
    debugLog(`${unreadIds.length} for ${prefix}`)
    if (unreadIds.length) {
        els._prop_noneBtn.classList.add('d-none');
        els._prop_activeBtn.classList.remove('d-none');
        els._prop_dismissAllBtn?.classList.remove('d-none');
    } else {
        els._prop_noneBtn.classList.remove('d-none');
        els._prop_activeBtn.classList.add('d-none');
        els._prop_dismissAllBtn?.classList.add('d-none');
    }
    els._prop_inner.parentNode!.replaceChild(content, els._prop_inner);
};

const updateWithResponse = (xhrResponse: string): void => {
    const fragment = $('div', { innerHTML: xhrResponse });
    TYPES.forEach(t => {
        try {
            updateSingleWithResponse(fragment, t);
        } catch (e) {
            console.error(e);
        }
    });
};

const initWithResponse = (xhrResponse: string): void => {
    updateWithResponse(xhrResponse);
    window.addEventListener('storage', e => {
        if (e.key !== localStorageDataKey) { return; }
        const data = localStorage.getItem(localStorageDataKey);
        if (data) {
            updateWithResponse(data);
        }
    });
};

const error = (response?: string): void => {
    localStorage.removeItem(localStorageUpdatedKey);
    TYPES.map(elementSet).forEach(({ _prop_inner }) =>
        $set(_prop_inner, $('div', 'alert alert-warning mb-0', 'Error: ' + response))
    );
};

const attemptInit = (): void => {
    const now = (Date.now() / 1000) | 0;
    const lastUpdateRequested = parseInt(localStorage.getItem(localStorageUpdatedKey)!, 36);
    if (now - lastUpdateRequested < 20) { // With 20 seconds, the last one can be used.
        const data = localStorage.getItem(localStorageDataKey);
        if (!data) {
            // Perhaps there is another request in progress? Let's wait.
            setTimeout(attemptInit, 2000);
        } else {
            initWithResponse(data);
        }
    } else {
        localStorage.removeItem(localStorageDataKey);
        localStorage.setItem(localStorageUpdatedKey, now.toString(36));
        $xhr('/api/notifications/v2', {
            e: error,
            t: response => {
                localStorage.setItem(localStorageDataKey, response);
                initWithResponse(response);
            }
        });
    }
}

export const sideEffect = () => {
    (elementSet(TYPES[0])._prop_noneBtn.parentNode as HTMLAnchorElement).addEventListener('click', e => {
        location.hash = '#favorites';
        e.preventDefault();
        e.stopPropagation();
    });

    attemptInit();
};