export const COOKIE_PREFIX: string;
export const hmacKeyEmail: string;
export const hmacKeyCaptcha: string;
export const mailjetKey: string;
export const sendInBlueKey: string;
export const dbPassword: string;
export const IS_DEV_MODE: boolean;
export const BACKOFFICE_SERVER: string;