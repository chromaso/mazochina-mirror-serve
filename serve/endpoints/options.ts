import * as conf from '../conf';
import { query as sql } from '../db';
import * as options from '../shared/options';
import { logSpecialRequest, MazoKoaContext } from '../utils/logic_utils';
import { error, finalizeSegment, HtmlSegmentImpl, PageImpl, redirectWithoutDelay } from '../utils/render_utils';

import * as templates from '../templates/options';

const CONFIG_KEYS: (keyof options.FullConfig)[] = ['theme', 'tz', 'fp', 'tp', 'cc', 'tfmt', 'lf', 'hc', 'sync'];

export const apiConfigure: HtmlSegmentImpl = async ctx => finalizeSegment(ctx, templates.configPanel(ctx, await options.footer(ctx)));

// Caller must update both userConfigRaw and userConfig first.
const saveOptionsImpl = async (ctx: MazoKoaContext): Promise<void> => {
    const loginUser = ctx.state.mazoLoginUser;
    const newConfigSerialized = options.serializeConfig(ctx.state.userConfigRaw);
    ctx.cookies.set(conf.COOKIE_PREFIX + 'conf', newConfigSerialized.toString(32), options.cookieOptions);
    if ((ctx.state.userConfigRaw.sync || ctx.request.body.string('sync')) && loginUser) { // 'sync' must be enabled, otherwise the action of turning off sync will not be recorded.
        await sql('UPDATE users SET configs = $1 WHERE user_id = $2', [newConfigSerialized, loginUser.id]);
    }
    // Both userConfigRaw and userConfig has been updated so far. Re-generate RenderHelpers based on the new ones, so the returned HTML reflects the updates.
    // Note that this is NOT absolutely safe, as the updated value can be a out-of-range one. Only the serialized version will rule out those illegal values. Doesn't really matter, as malicious users will only affect themselves.
    ctx.state.render = options.renderHelpers(ctx);
};

export const postApiConfigure: HtmlSegmentImpl = async ctx => {
    for (const key of CONFIG_KEYS) {
        const rawValue = ctx.request.body.string(key);
        if (!rawValue) { continue; }
        if (rawValue.length > 20) { throw error(413, '选项长度太长啦！'); }
        let value;
        try { value = JSON.parse(rawValue); } catch (e) { throw error(400, '选项内容有误～'); }
        if ((typeof ctx.state.userConfig[key]) !== (typeof value)) { throw error(400, '选项内容不对劲～'); }
        ctx.state.userConfigRaw[key] = value;
        (ctx.state.userConfig[key] as any) = value;
        logSpecialRequest(ctx, 'option-' + key, rawValue);
    }
    await saveOptionsImpl(ctx);

    return finalizeSegment(ctx, templates.configPanel(ctx, await options.footer(ctx)));
};

export const apiThemes: HtmlSegmentImpl = async ctx => {
    return finalizeSegment(ctx, templates.preview(ctx, await options.footer(ctx)));
};

export const postTheme: PageImpl = async ctx => {
    const theme = ctx.request.body.string('themeaction');
    const themeName = theme.match(/([a-z]+)$/);
    if (!themeName) { throw error(400, '未指定风格。'); }
    ctx.state.userConfigRaw.theme = themeName[1] as any;
    ctx.state.userConfigRaw.theme = themeName[1] as any;
    await saveOptionsImpl(ctx);

    return redirectWithoutDelay(ctx, 'back', '/');
};

export const theme_theme: HtmlSegmentImpl = async ctx => {
    const theme = ctx.params.theme as any;
    if (!theme || !/^[a-z]+$/.test(theme)) { throw error(400, '未指定风格名称！'); }
    return finalizeSegment(ctx, templates.previewTheme(ctx, { theme }));
};