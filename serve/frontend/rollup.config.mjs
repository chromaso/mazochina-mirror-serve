import typescript from '@rollup/plugin-typescript';
import { minify } from 'uglify-js';

const noMinify = process.env['NO_MONIFY'];

if (noMinify) {
    console.warn('Frontend code NOT minified!');
}

// A rollup plugin to uglify with `enclose` option.
const minifierEnclosing = variablesToEnclose => ({
    name: 'uglify',
    renderChunk: code => noMinify ? code : minify(code, {
        enclose: variablesToEnclose.join(',') + ':' + variablesToEnclose.join(','),
        mangle: {
            properties: { regex: /^_prop_/ }
        }
    })
});

export default [
    {
        input: 'global/window-export.ts',
        output: {
            file: '../static/bundle-cn.js',
            format: 'iife'
        },
        plugins: [typescript(), minifierEnclosing(['document', 'HTMLElement'])]
    },
    {
        input: 'metro/window-export.ts',
        output: {
            file: '../static/metro-cn.js',
            format: 'iife'
        },
        plugins: [typescript(), minifierEnclosing(['localStorage', 'document', 'setTimeout', 'alert'])]
    },
    {
        input: 'admin/window-export.ts',
        output: {
            file: '../static/admin-cn.js',
            format: 'iife'
        },
        plugins: [typescript(), minifierEnclosing(['document', 'String'])]
    }
];
