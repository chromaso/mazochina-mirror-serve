import { $ } from './dollar';

export const initPagination = () => Array.prototype.forEach.call(document.querySelectorAll('a.page-link[href*=":page:"]'), (anchor: HTMLAnchorElement) => {
    const void0 = 'javascript:void(0)';
    if (anchor.href === void0) { return; }
    const template = anchor.href.split(':page:');
    anchor.href = void0;
    anchor.style.pointerEvents = 'auto';
    anchor.setAttribute('data-toggle', 'dropdown');
    const input = $('input', { type: 'text', inputMode: 'numeric', pattern: '\\d+', className: 'form-control form-control-sm flex-shrink-1 mx-1', placeholder: '1', required: true });
    input.style.width = '64px';
    const dropdown = $('div', 'dropdown-menu', [
        $('form', 'd-flex flex-nowrap text-nowrap justify-content-center align-items-center px-2', [
            '前往第', input, '页',
            $('button', { className: 'btn btn-sm btn-primary ms-1', type: 'submit' }, '>')
        ], {
            'submit': e => {
                e.preventDefault();
                const page = parseInt(input.value);
                if (!((page > 0) && (page < 0x7fff))) { return; }
                location.assign(template.join(input.value));
            }
        })
    ]);
    const parent = anchor.parentNode as HTMLLIElement;
    parent.style.position = 'relative';
    parent.appendChild(dropdown);
});
