import { rowsToMap, unawaited } from '../../common/logic_utils';
import { query as sql, withTransaction } from '../db';
import { pmRateLimitTest, RATE_LIMIT_CONSTS, rateLimitGroup } from '../shared/author';
import { MazoKoaContext, parseInt32 } from '../utils/logic_utils';
import { HtmlSegmentImpl, LOGIN_REQUIRED, PageImpl, error, finalizePage, finalizeSegment, processLocalContent, redirectWithoutDelay } from '../utils/render_utils';
import * as attachments from '../shared/attachment';
import { parseBBCode } from '../../common/bbcode';
import { RateLimiter } from '../utils/rate_limit';
import { checkQuoteItemValidity } from '../utils/quote_reply';

import * as templates from '../templates/pm';
import { xrefsToWireFormat } from '../shared/post';

// Checks if PM can be sent.
const checkUsersAndPreparePm = async (ctx: MazoKoaContext, toUser: number): Promise<{
    threadId: number; // 0 if conversation not there yet.
    userName: string;
    flagToUse: number; // 0 or 8
}> => {
    if (isNaN(toUser)) { throw error(400, '又在乱改网址了吗……'); }

    const loginUser = ctx.state.mazoLoginUser;
    if (!loginUser) { throw error(403, LOGIN_REQUIRED); }
    const fromUserId = loginUser.id;
    if (fromUserId === toUser) { throw error(400, '不可以向自己发送私信啦！'); }
    const targetUser = (await sql<{ user_name: string; has_password: boolean; }>('SELECT user_name, password_h IS NOT NULL AS has_password FROM users WHERE user_id = $1', [toUser])).rows[0];
    if (!targetUser) { throw error(404, '该用户不存在～'); }
    if (!targetUser.has_password) { throw error(400, '该用户仅在源站注册过，尚未在镜像注册过，你无法在镜像向其发送私信 :('); }
    const conversation = (await sql<{ thread_id: number; from_user: number; }>('SELECT thread_id, from_user FROM local_conversations WHERE LEAST(from_user, to_user) = $1 AND GREATEST(from_user, to_user) = $2', [Math.min(fromUserId, toUser), Math.max(fromUserId, toUser)])).rows[0];
    if (!conversation) {
        const pmsInWindow = await pmRateLimitTest(ctx.refDate, fromUserId);
        const myLimit = RATE_LIMIT_CONSTS[rateLimitGroup(ctx.refDate, loginUser)][2];
        if (pmsInWindow + 1 > myLimit) {
            throw error(429, '你已超过 24 小时内创建新私信对话的上限～');
        }
    }
    return {
        threadId: conversation ? conversation.thread_id : 0,
        userName: targetUser.user_name,
        flagToUse: conversation ? ((conversation.from_user == fromUserId) ? 0 : 8) : 0
    };
};

export const pm: PageImpl = async (ctx) => {
    const loginUser = ctx.state.mazoLoginUser;
    if (!loginUser) { throw error(403, LOGIN_REQUIRED); }
    const allPmThreads = rowsToMap((await sql<{ thread_id: number; from_me: boolean; other_user: number; }, [number]>('SELECT thread_id, from_user = $1 AS from_me, COALESCE(NULLIF(from_user, $1), to_user) AS other_user FROM local_conversations WHERE from_user = $1 OR to_user = $1', [loginUser.id])).rows, 'thread_id');
    if (!allPmThreads.size) {
        throw error(404, '你从未发出或收到过任何私信哦！你可以在其它作者的资料页对其发起私信。');
    }
    const threadIds = Array.from(allPmThreads.keys());
    const eachThreadPromise = sql<{ thread_id: number; cnt_reply: number; cnt: number; first_post: number; last_post: number; }, [number[]]>('SELECT thread_id, (COUNT(post_id) FILTER (WHERE (flags & 8) <> 0))::INT AS cnt_reply, COUNT(post_id)::INT AS cnt, MIN(post_id) AS first_post, MAX(post_id) AS last_post FROM local_pms WHERE thread_id = ANY($1::INT[]) GROUP BY thread_id ORDER BY last_post DESC', [threadIds]).then(result => result.rows);
    const [users, lastPosts, unreadCounts] = await Promise.all([
        // users: user name of each user the active user is talking to, indexed by user id.
        sql<{ user_id: number; user_name: string; }, [number[]]>('SELECT user_id, user_name FROM users WHERE user_id = ANY($1::INT[])', [Array.from(allPmThreads.values()).map(thread => thread.other_user)]).then(result => rowsToMap(result.rows, 'user_id')),
        // lastPosts: last PM post of each PM thread, indexed by thread id.
        eachThreadPromise.then(eachThread => sql<{ thread_id: number; title: string; posted_date: number; is_reply: boolean; }, [number[]]>('SELECT thread_id, title, posted_date, ((flags & 8) <> 0) AS is_reply FROM local_pms WHERE post_id = ANY($1::INT[])', [eachThread.map(thread => thread.last_post)])).then(result => rowsToMap(result.rows, 'thread_id')),
        // unreadCounts: unread post counts of each PM thread, indexed by thread id.
        sql<{ thread_id: number; cnt_reply: number; cnt: number; }, [number[], number]>('WITH rs AS (SELECT thread_id, up_to_date FROM unread_status WHERE user_id = $2 AND thread_id = ANY($1::int[])) SELECT thread_id, (COUNT(post_id) FILTER (WHERE (flags & 8) <> 0))::INT AS cnt_reply, COUNT(post_id)::INT AS cnt FROM local_pms WHERE thread_id = ANY($1::int[]) AND posted_date > COALESCE((SELECT up_to_date FROM rs WHERE rs.thread_id = local_pms.thread_id), 0) GROUP BY thread_id', [threadIds, loginUser.id]).then(result => rowsToMap(result.rows, 'thread_id'))
    ]);

    return finalizePage(ctx, {
        title: ctx.state.render.staticConv('私信列表'),
        nav: true,
    }, templates.pm_list(ctx, {
        pms: (await eachThreadPromise).map(thread => {
            const threadRow = allPmThreads.get(thread.thread_id)!;
            const last = lastPosts.get(thread.thread_id)!;
            const outCount = threadRow.from_me ? thread.cnt - thread.cnt_reply : thread.cnt_reply;
            const unreadCountRow = unreadCounts.get(thread.thread_id);
            const unreadCount = unreadCountRow ? (threadRow.from_me ? unreadCountRow.cnt_reply : (unreadCountRow.cnt - unreadCountRow.cnt_reply)) : 0;
            return {
                userId: threadRow.other_user,
                userName: users.get(threadRow.other_user)?.user_name ?? '[UNKNOWN]',
                outCount,
                inCount: thread.cnt - outCount,
                lastAt: last.posted_date,
                lastTitle: last.title,
                lastIsIn: threadRow.from_me === last.is_reply,
                lastIsFirst: thread.first_post === thread.last_post,
                unreadCount
            };
        })
    }));
};

export const pm_id: PageImpl = async (ctx) => {
    const targetUserId = parseInt32(ctx.params.id);
    let { threadId, userName } = await checkUsersAndPreparePm(ctx, targetUserId);

    return finalizePage(ctx, {
        title: `${ctx.state.render.staticConv('与')} ${userName} ${ctx.state.render.staticConv('私信')}`,
        nav: true,
        deps: ['SCEDITOR']
    }, templates.conversation(ctx, {
        threadId,
        targetUserId,
        targetUserName: userName,
        formError: undefined,
        postTitle: '',
        postContent: '',
        justPosted: false
    }));
};

// For enforcing the 2-second windowed rate limit.
const DEDUP_RATE_LIMITER = new RateLimiter<number>(1, 2);

export const apiPm_id: HtmlSegmentImpl = async (ctx) => {
    const threadId = parseInt32(ctx.params.id);
    if (isNaN(threadId)) { throw error(400, '又在乱改网址了吗……'); }
    const loginUser = ctx.state.mazoLoginUser;
    if (!loginUser) { throw error(403, LOGIN_REQUIRED); }
    const conversation = (await sql<{ from_user: number; to_user: number; }>('SELECT from_user, to_user FROM local_conversations WHERE thread_id = $1', [threadId])).rows[0];
    if (!conversation) { throw error(404, '没有这个私信主题哦！'); }
    let iAmFromUser: boolean;
    if (conversation.from_user === loginUser.id) {
        iAmFromUser = true;
    } else if (conversation.to_user === loginUser.id) {
        iAmFromUser = false;
    } else {
        throw error(403, '这不是你的私信啦，不能偷看别人的！');
    }
    const lastPostId = parseInt32(`${ctx.query.last || ''}`);
    const history = (await sql<{ post_id: number; title: string; content: string; flags: number; posted_date: number; }>('SELECT post_id, title, content, flags, posted_date FROM local_pms WHERE thread_id = $1 AND post_id < $2 ORDER BY post_id DESC LIMIT $3', [threadId, isNaN(lastPostId) ? 0x7fffffff : lastPostId, ctx.state.userConfig.tp + 1])).rows;
    const finished = history.length <= ctx.state.userConfig.tp;
    if (!finished) {
        history.splice(ctx.state.userConfig.tp);
    }
    const targetUser = (await sql<{ user_id: number; user_name: string; }>('SELECT user_id, user_name FROM users WHERE user_id = $1', [iAmFromUser ? conversation.to_user : conversation.from_user])).rows[0];
    if (!targetUser) {
        throw error(500, '用户信息没找到～请将此错误报告至管理员！');
    }

    return finalizeSegment(ctx, templates.pms(ctx, {
        pms: history.map(row => {
            const toUserToFromUser = !!(row.flags & 0x8);
            const isFromMe = toUserToFromUser !== iAmFromUser;
            return {
                id: row.post_id,
                isFromMe,
                date: row.posted_date,
                content: processLocalContent(row.content),
                title: row.title,
                authorId: isFromMe ? loginUser.id : targetUser.user_id,
                authorName: isFromMe ? loginUser.name : targetUser.user_name
            };
        }).reverse(),
        finished
    }));
};

export const postPm_id: PageImpl = async ctx => {
    const targetUserId = parseInt32(ctx.params.id);
    let { threadId, userName, flagToUse } = await checkUsersAndPreparePm(ctx, targetUserId);
    const loginUserId = ctx.state.mazoLoginUser!.id;

    const postTitle = ctx.request.body.string('title');
    const postContent = ctx.request.body.string('content');
    const errors = [];
    let justPosted = false;

    if (ctx.request.body.string('submitpost')) {
        if (!DEDUP_RATE_LIMITER.attempt(loginUserId)) {
            errors.push('你操作得太频繁啦～请勿在五秒内连续点击发布～');
        } else {
            const postId = (await sql('SELECT nextval(\'local_post_id\') AS id')).rows[0].id;
            if (postTitle.length > 55) { errors.push('标题不可超过 55 个字'); }
            let parsed: ReturnType<typeof parseBBCode>;
            try {
                parsed = parseBBCode(postId, postContent);
                let properTextLength = parsed.text.length;
                if (parsed.quotes.length) {
                    properTextLength = await checkQuoteItemValidity(ctx.state, parsed);
                }
                if (properTextLength < 2) {
                    errors.push('正文长度太短');
                }
                if (parsed.links.toLinks.size) {
                    errors.push('私信中不可设置链接跳转目标');
                }
            } catch (e) {
                errors.push((e as Error).message);
            }

            if (!errors.length) {
                if (threadId) {
                    const myLastPost = (await sql<{ post_id: number, thread_id: number, content: string }>('SELECT post_id, content FROM local_pms WHERE thread_id = $1 AND flags & 8 = $2 & 8 ORDER BY post_id DESC LIMIT 1', [threadId, flagToUse])).rows[0];
                    if (myLastPost && (myLastPost.content === parsed!.html)) {
                        // Posting the same thing. Instead of showing an error, just silently nagivate back.
                        return redirectWithoutDelay(ctx, '/pm/' + targetUserId);
                    }
                }
    
                await withTransaction(async query => {
                    const insertedLog = (await query('INSERT INTO local_post_log (rev_id, post_id, updated_at, title, content, flags) VALUES (nextval(\'local_rev_id\'), $1, $2, $3, $4, $5) RETURNING rev_id', [postId, ctx.refDate, postTitle, postContent, flagToUse])).rows[0];
                    threadId ||= (await query<{ thread_id: number }>('INSERT INTO local_conversations (thread_id, from_user, to_user) VALUES (nextval(\'local_thread_id\'), $1, $2) RETURNING thread_id', [loginUserId, targetUserId])).rows[0].thread_id;
                    await query('INSERT INTO local_pms (post_id, thread_id, title, content, rev_id, posted_date, flags, xrefs) VALUES ($1, $2, $3, $4, $5, $6, $7, $8)', [postId, threadId, postTitle, parsed.html, insertedLog.rev_id, ctx.refDate, flagToUse, xrefsToWireFormat(parsed)]);
                    await attachments.updateAttachmentsToPosts(parsed.links.attachments, postId, query);
                });
    
                unawaited(sql<{}, [number, number, number]>('INSERT INTO notifications (user_id, notification_date, message_type, primary_target_id, dismissed) VALUES ($3, $1, 5, $2, 0) ON CONFLICT (user_id, message_type, primary_target_id) DO UPDATE SET notification_date = EXCLUDED.notification_date, dismissed = EXCLUDED.dismissed', [ctx.refDate, threadId, targetUserId]), 'type-5 notification');
    
                justPosted = true;
            }

        }
    }

    return finalizePage(ctx, {
        title: `${ctx.state.render.staticConv('与')} ${userName} ${ctx.state.render.staticConv('私信')}`,
        nav: true,
        deps: ['SCEDITOR']
    }, templates.conversation(ctx, {
        threadId,
        targetUserId,
        targetUserName: userName,
        formError: errors.length ? errors.join('；') : undefined,
        postTitle,
        postContent,
        justPosted
    }));
};