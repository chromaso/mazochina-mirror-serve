import * as logger from './logger';

class TaskQueue<T> {

    private standardTasks = new Set<T>();
    private lowPriorityTasks = new Set<T>();

    constructor(public readonly name: string) {
        this.name = name;
    };

    pop(highOnly?: boolean): T | undefined {
        let val = this.standardTasks.values().next().value;
        if (val) {
            logger.log('pop-high-' + this.name, val);
            this.standardTasks.delete(val);
            const values = Array.from(this.standardTasks.values());
            logger.log('remaining-high-' + this.name, values.length + ' (' + values.slice(0, 5).join(', ') + (values.length > 5 ? ' ...' + values.slice(values.length - 5).join(', ') : '') + ')');
        } else if (!highOnly) {
            val = this.lowPriorityTasks.values().next().value;
            if (val) {
                logger.log('pop-low-' + this.name, val);
                this.lowPriorityTasks.delete(val);
            }
        }
        return val;
    };

    push(el: T): void {
        logger.log('push-high-' + this.name, el);
        if (this.lowPriorityTasks.has(el)) { this.lowPriorityTasks.delete(el); }
        this.standardTasks.add(el);
    };

    pushLow(el: T): void {
        logger.log('push-low-' + this.name, el);
        if (this.standardTasks.has(el)) { return; }
        this.lowPriorityTasks.add(el);
    };

}

export const forumQueue = new TaskQueue<number>('forum');
export const threadQueue = new TaskQueue<number>('thread');
