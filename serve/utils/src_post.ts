import { ORIGIN_ROOT_WTS } from '../../common/consts';

export const originalRemoteUrl = (forum: number, thread: number, post?: number): string => ORIGIN_ROOT_WTS + 'viewtopic.php?f=' + forum + '&t=' + thread + (post ? '&p=' + post + '#p' + post : '');

export const originalReplyUrl = (forum: number, thread: number, post?: number): string => ORIGIN_ROOT_WTS + `posting.php?mode=${post ? 'quote' : 'reply'}&f=${forum}&` + (post ? `p=${post}` : `t=${thread}`);