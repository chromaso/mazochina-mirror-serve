import { $, $set } from './dollar';
import { formatTz } from './utils';

const DISMISSAL_LS_KEY = 'mazomirror-alert-dismissals';
const PREFERRED_TAB_KEY = 'mazomirror-index-tab';

const parsePreferredTagLs = () => {
    const data = (localStorage.getItem(PREFERRED_TAB_KEY) || '').split(';');
    return {
        _prop_tab: parseInt(data[0]) || 1, // 0 or 1
        _prop_trivia: parseInt(data[1]) || 0 // -1, 0 or 1 (disabled, not set, enabled)
    };
};

const initTabAndTriviaSwitch = () => {
    const selector = document.getElementById('index-tab-switch')!;
    const triviaSwitchContainer = document.getElementById('trivia-switch');
    const btns = Array.from(selector.querySelectorAll('.nav-link'));
    const panels = selector.nextElementSibling!.children;

    const activateOne = (index: number, on: boolean) => {
        btns[index].classList[on ? 'add' : 'remove']('active');
        panels[index].classList[on ? 'remove' : 'add']('d-none');
    };
    const activateTabs = (index: number) => {
        if (index !== 1) { index = 0; }
        activateOne(index, true);
        activateOne(1 - index, false);
    };
    btns.forEach((btn, idx) =>
        btn.addEventListener('click', e => {
            e.preventDefault();
            activateTabs(idx);
            localStorage.setItem(PREFERRED_TAB_KEY, String(idx + 1) + ';' + parsePreferredTagLs()._prop_trivia);
        })
    );
    selector.classList.remove('d-none');

    const parsed = parsePreferredTagLs();
    activateTabs(parsed._prop_tab - 1);

    if (triviaSwitchContainer) {
        const icon = $('span', 'material-icons');
        $set(triviaSwitchContainer, icon);

        const actualTrivia = (triviaSwitchContainer.parentNode as HTMLElement).nextElementSibling as HTMLElement;

        const setTo = (show: boolean) => {
            actualTrivia.classList[show ? 'remove' : 'add']('d-none');
            $set(icon, show ? 'expand_less' : 'expand_more');
            triviaSwitchContainer.title = (show ? '隐藏' : '显示') + ' Trivia';
        };
        triviaSwitchContainer.addEventListener('click', () => {
            const turningOn = actualTrivia.classList.contains('d-none');
            setTo(turningOn);
            localStorage.setItem(PREFERRED_TAB_KEY, parsePreferredTagLs()._prop_tab + ';' + (turningOn ? 1 : -1));
        });

        if (parsed._prop_trivia < 0) { // Hide trivia.
            setTo(false);
        } else {
            setTo(true);
        }
    }
};

export const initHomePage = (currentTz: number, showMergeNoticeFor: number, hostname: string) => {
    initTabAndTriviaSwitch();

    // All code below are for dynamic alerts.

    let dismissSettings: Record<string, string>;
    try {
        dismissSettings = JSON.parse(localStorage.getItem(DISMISSAL_LS_KEY) || '{}');
    } catch (e) {}
    if ((typeof dismissSettings! !== 'object')) { dismissSettings = {}; }

    const createAlert = (classNameSuffix: string, nodes: Parameters<typeof $>[2], k: string, v: string) => {
        const container = document.getElementById('dynamic-alert-container')!;
        const alert = $('div', 'alert d-flex justify-content-between align-items-center' + classNameSuffix, [
            $('p', 'mb-0', nodes),
            $('a', { href: '#_', className: 'small text-decoration-none text-end' }, '不再提醒', () => {
                dismissSettings[k] = v;
                localStorage.setItem(DISMISSAL_LS_KEY, JSON.stringify(dismissSettings));
                container.removeChild(alert);
            })
        ]);
        container.appendChild(alert);
    };

    const deviceTz = Math.round(-(new Date).getTimezoneOffset() / 60);
    if (deviceTz !== currentTz) {
        const key = `${deviceTz}:${currentTz}`;
        if (dismissSettings['tz'] !== key) {
            createAlert(' alert-warning', [`目前显示的时间使用的时区（${formatTz(currentTz)}）和你设备所在的时区（${formatTz(deviceTz)}）不同，你可以在`, $('a', { href: 'javascript:openMazoSettings("tz")' }, '设置选项'), '中进行修改。'], 'tz', key);
        }
    }

    // TODO(chromaso): restore this once mazochina.com is back online.
    //
    // if (showMergeNoticeFor) {
    //     if (dismissSettings['src'] !== String(showMergeNoticeFor)) {
    //         createAlert(' alert-secondary', ['若你在源站注册过，现可', $('a', { href: '/merge' }, '点击自助验证你在源站的身份'), '，并在镜像使用你在源站使用的用户名～'], 'src', String(showMergeNoticeFor));
    //     }
    // }

    if (hostname && (hostname !== 'mirror.chromaso.net')) {
        if (dismissSettings['host'] !== hostname) {
            createAlert(' alert-secondary', ['你正在使用的并非主域名；请知悉本站主域名是 ', $('a', { href: 'https://mirror.chromaso.net/' }, 'mirror.chromaso.net'), `。建议尽量使用主域名访问本站。`], 'host', hostname);
        }
    }
};

