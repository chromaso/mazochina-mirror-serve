import { rowsToMap } from '../../common/logic_utils';
import { query as sql } from '../db';
import { processContentSc } from './post';
import { forumsCache } from './forum';

export const prepareAdminLogTableRenderParams = async (adminOps: AdminLogDAO[]) => {
    const threadIdsToFetch = new Set([
        ...adminOps.filter(row => row.target_type === 'thread').map(row => row.target_id),
        ...adminOps.map(row => row.extra_information?.thread).filter(_ => _)
    ]);
    const threads = threadIdsToFetch.size ? (await sql<{ thread_title: string; thread_id: number; forum_id: number; owner_user_id: number; flags: number; }>('SELECT thread_title, thread_id, forum_id, owner_user_id, flags FROM thread_targets WHERE thread_id = ANY($1::INT[])', [Array.from(threadIdsToFetch)])).rows : [];
    const postIds = adminOps.filter(row => row.target_type === 'post').map(row => row.target_id);
    const posts = postIds.length ? (await sql<{ title: string; thread_id: number; post_id: number; user_id: number; content_sc: string; }>('SELECT title, thread_id, post_id, user_id, content_sc FROM posts WHERE post_id = ANY($1::INT[])', [postIds])).rows.map(row => ({ ...row, content: processContentSc(row.content_sc).trim() })) : [];
    const userIds = Array.from(new Set([
        ...adminOps.filter(row => row.target_type === 'user').map(row => row.target_id),
        ...adminOps.map(row => row.admin_user_id),
        ...posts.map(row => row.user_id),
        ...threads.map(row => row.owner_user_id)
    ]));
    const users = rowsToMap((await sql<{ user_id: number; user_name: string; }>('SELECT user_id, user_name FROM users WHERE user_id = ANY($1::INT[])', [userIds])).rows, 'user_id');
    if (users.size !== userIds.length) {
        console.error('[Should never happen] Users ' + userIds.filter(id => !users.has(id)).join(', ') + ' not found!');
    }

    return {
        logs: adminOps,
        users,
        posts: rowsToMap(posts, 'post_id'),
        threads: rowsToMap(threads, 'thread_id'),
        forums: await forumsCache
    };
};export type AdminLogDAO = {
    admin_log_id: number;
    target_type: 'user' | 'post' | 'thread';
    extra_information: any;
    target_id: number;
    flags: number;
    admin_user_id: number;
    note: string;
    operation_date: number;
};

