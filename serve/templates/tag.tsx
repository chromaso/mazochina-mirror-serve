import * as elements from 'typed-xhtml';
import { HtmlTmpl, styled } from '../utils/tsx_utils';
import { BaseThreadTarget, RenderThreadRetVal, TagStyleTuple } from '../shared/thread_table';
import { threadTable } from './thread_table';
import { allTagsByCategory, CategoryId, F18_CONSTS, TagCatOrder, TagDisplayStyle, TagInfoInThreadTable, TagLite, TagsDisplayLimit } from '../shared/tags';

const tagStyleToCssClasses = [
    'bg-secondary', // 2
    'bg-transparent border text-reset', // 3 & 4
    'bg-transparent border border-secondary text-reset' // 5 & 6
];

// Migrated from tags_in_thread_table.inc.pug.
const tagListImpl: HtmlTmpl<{
    list: Readonly<TagInfoInThreadTable[]>, style: TagDisplayStyle
}> = (ctx, { list, style }) => (
    <>
        {list.map(tag => [
            ' ',
            (style === 1) ? (
                <a class="d-inline-block text-reset ui-link" href={tag.link} data-debug={tag.debug || false}>
                    <span class="text-muted">#</span>
                    {ctx.state.render.staticConv(tag.name)}
                    {tag.warn ? (
                        <span class="material-icons align-middle" style='font-size: 85%'>report_problem</span>
                    ) : null}
                </a>
            ) : (
                <a class={'badge rounded-pill position-relative ' + tagStyleToCssClasses[Math.floor((style - 1) / 2)]} href={tag.link} data-debug={tag.debug || false}>
                    {ctx.state.render.staticConv(tag.name)}
                    {tag.warn ? (
                        <>
                            <span class="material-icons position-absolute align-middle" style='font-size: 100%'>report_problem</span>
                            <span class="d-inline-block ps-2"></span>
                        </>
                    ) : null}
                </a>
            )
        ])}
    </>
);

const DUMMY = [{ name: 'Lorem Ipsum', link: '#', warn: false }] as const;

// Migrated from tags_in_thread_table.inc.pug.
export const tagList: HtmlTmpl<{
    list: TagInfoInThreadTable[], tagStyle: TagStyleTuple
}> = (ctx, { list, tagStyle: [style, noWrap] }) => (
    <div class={(((style !== 4) && (style !== 6)) ? 'small' : '') + ' ' + (noWrap ? 'position-relative' : '')}>
        {noWrap ? (
            <>
                <div class="text-truncate position-absolute w-100 overflow-hidden">
                    {tagListImpl(ctx, { list, style })}
                </div>
                <div class={`invisible${((style !== 4) && (style !== 6)) ? ' small' : ''}`}>
                    {tagListImpl(ctx, { list: DUMMY, style })}
                </div>
            </>
        ) : tagListImpl(ctx, { list, style })}
    </div>
);

// Migrated from tag.pug.
export const tag: HtmlTmpl<{
    tag: TagLite,
    warn: boolean,
    threads: RenderThreadRetVal<BaseThreadTarget>,
    threadCount: number
}> = (ctx, self) => {
    const { render, mazoLoginUser } = ctx.state;
    return styled('#suggestion-card:not(.d-none) + div { display: none; }', (
        <div class="container">
            <nav class="mb-4">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="/">首页</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="/tags">标签</a>
                    </li>
                    <li class="breadcrumb-item active">
                        {render.staticConv(self.tag.primary_name)}
                    </li>
                </ol>
            </nav>
            <h1 class="mb-3">{render.staticConv(self.tag.primary_name)}</h1>
            <div class="custom-control custom-switch">
                <input
                    id="tag-warning-switch"
                    type="checkbox"
                    checked={self.warn}
                    disabled={true}
                    class="custom-control-input"
                />
                <label for="tag-warning-switch" class="custom-control-label">
                    以<span class="material-icons std-icon">report_problem</span>标识此标签
                    <span class="ms-1 mazomirror-tip"  data-title="对于你可能不喜欢的标签，你可以将其以警告符号显示，以方便你避开相关小说">
                        <span class="material-icons std-icon">help</span>
                    </span>
                </label>
            </div>
            <table class="table mt-3 mb-1">
                <tr>
                    <td>类别</td>
                    <td>
                        {render.staticConv(F18_CONSTS.CAT_TITLES[self.tag.category])}
                    </td>
                </tr>
                <tr>
                    <td>描述</td>
                    <td>
                        {self.tag.desc_html ? (
                            elements.safeHtml(render.staticConv(self.tag.desc_html))
                        ) : (
                            <div class="text-muted">（暂无描述，你可在下方点击提议）</div>
                        )}
                    </td>
                </tr>
                <tr>
                    <td>别名</td>
                    <td>
                        {self.tag.aliases ? (
                            self.tag.aliases.map(render.staticConv).join('、')
                        ) : (
                            <div class="text-muted">（暂无别名，你可在下方点击提议）</div>
                        )}
                    </td>
                </tr>
                {self.tag.parentTags ? (
                    <tr>
                        <td>上级标签</td>
                        <td class="lead py-0 align-middle">
                            {self.tag.parentTags.map(otherTag => (
                                <a class="badge rounded-pill bg-secondary me-2 fw-normal" href={'/tag/' + otherTag.tag_id}>
                                    {render.staticConv(otherTag.primary_name)}
                                </a>
                            ))}
                        </td>
                    </tr>
                ) : null}
                {self.tag.childTags ? (
                    <tr>
                        <td>子标签</td>
                        <td class="lead py-0 align-middle">
                            {self.tag.childTags.map(otherTag => (
                                <a class="badge rounded-pill bg-secondary me-2 fw-normal" href={'/tag/' + otherTag.tag_id}>
                                    {render.staticConv(otherTag.primary_name)}
                                </a>
                            ))}
                        </td>
                    </tr>
                ) : null}
            </table>
            <div id="suggestion-card" class="card d-none">
                <div class="card-body">
                    <form method="post">
                        <p>
                            你可以提议添加或修改此标签的描述、别名、上级标签等。你提交的内容将公开显示。
                        </p>
                        <div class="form-group">
                            <textarea name="suggestion" class="form-control" />
                        </div>
                        <div class="text-center">
                            <a class="btn btn-secondary btn-sm" href="javascript:showSCard(false)">
                                取消
                            </a>
                            {' '}
                            <button type="submit" class="btn btn-primary btn-sm">
                                提交
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            <div>
                <a class="btn btn-primary btn-sm" href={mazoLoginUser ? 'javascript:showSCard(true)' : `javascript:window.alert('登入后才可进行此操作哦！')`}>
                    提议更改/补充上述信息
                </a>
            </div>
            {self.threads.length ? (
                <>
                    <h3 class="mt-4 mb-3">
                        含该标签{self.tag.childTags ? '（或其子孙标签）' : ''}
                        的小说（共 {self.threadCount} 个主题）
                    </h3>
                    <div id="tag-page-main-threads-table">
                        {threadTable(ctx, { threads: self.threads, showReadState: true })}
                    </div>
                    {(self.threadCount > self.threads.length) ? (
                        <div class="text-center my-4">
                            <a class="btn btn-primary m-1" href={'/query?type=thread&query=' + encodeURIComponent('#' + self.tag.primary_name)}>
                                全部主题（{self.threadCount}）
                            </a>
                        </div>
                    ) : null}
                </>
            ) : (
                <div class="alert mt-4 alert-info">该标签下暂未有任何小说</div>
            )}
            <script type="text/javascript">{elements.safeHtml(`initTagPageSwitch(${self.tag.tag_id});`)}</script>
            <script type="text/javascript">{elements.safeHtml(`var showSCard = function (on) { document.getElementById('suggestion-card').classList[on ? 'remove' : 'add']('d-none'); };`)}</script>
            {(self.tag.tag_id === F18_CONSTS.VOTE_CAMPAIGN_TAG_ID) ? <script type="text/javascript">{elements.safeHtml(`tagPageCampaignVoteOverlay(document.querySelector('#tag-page-main-threads-table tbody'));`)}</script> : null}
        </div>
    ));
};

// Migrated from tags.pug.
export const tags: HtmlTmpl<{
    f18Tags: Awaited<ReturnType<typeof allTagsByCategory>>,
    warns: Set<number>,
    grandCounts: Map<number, { cnt: number }>
}> = (ctx, self) => (
    <div class="container">
        <div class="d-flex justify-content-between align-items-center mb-4">
            <h1 class="my-0">所有小说标签</h1>
            <a href="/tags/settings" class="btn btn-primary">
                <span class="material-icons">settings</span>
            </a>
        </div>
        <div class="alert alert-info">
            下方列表中每个标签的小说数量皆只包含直接添加了该标签的小说数量，而不包含添加了其子标签的小说数量。
        </div>
        {F18_CONSTS.CAT_TITLES.map((title, catId) => (
            <>
                <h4>{ctx.state.render.staticConv(title)}</h4>
                <p class="lead">
                    {self.f18Tags.get(catId as CategoryId)?.map(tag => {
                        const countRow = self.grandCounts.get(tag.tag_id);
                        return (
                            <a class="badge rounded-pill bg-secondary me-2 fw-normal" href={'/tag/' + tag.tag_id}>
                                {ctx.state.render.staticConv(tag.primary_name)}
                                {self.warns.has(tag.tag_id) ? (
                                    <span class="material-icons align-middle" style='font-size: 85%'>
                                        report_problem
                                    </span>
                                ) : null}
                                {countRow ? (
                                    <small class="ms-1">{countRow.cnt}</small>
                                ) : null}
                            </a>
                        );
                    })}
                </p>
            </>
        ))}
    </div>
);

// Migrated from tag_settings.pug.
export const tagSettings: HtmlTmpl<{
    tagsOn: boolean,
    style: TagDisplayStyle,
    catOrder: TagCatOrder,
    limit: TagsDisplayLimit,
    nowrap: boolean,
    warnings: TagLite[]
}> = (ctx, self) => {
    const demoTags = [
        { link: 'javascript:void(0)', name: '示例', warn: false },
        { link: 'javascript:void(0)', name: '例子', warn: false },
        { link: 'javascript:void(0)', name: 'M的例子', warn: true },
        { link: 'javascript:void(0)', name: 'Lorem', warn: false },
        { link: 'javascript:void(0)', name: 'Ipsum', warn: false }
    ];
    const { render } = ctx.state;
    return styled(`#warning-tag-list:empty::before { content: '当前没有任何敏感标签'; }`,
        <>
            <div class="container">
                <nav class="mb-4">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="/">首页</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="/tags">标签</a>
                        </li>
                        <li class="breadcrumb-item active">设置</li>
                    </ol>
                </nav>
                <h1 class="mb-4">标签相关设置</h1>
                <div id="settings-update-alert" class="alert alert-info">
                    对于标签功能，可以前往
                    <a href={'/thread/' + F18_CONSTS.CONSTS.README_THREAD_ID}>
                        这里
                    </a>
                    了解或提出建议。
                </div>
                <form>
                    <fieldset class="form-group row pt-2">
                        <legend class="text-bold col-form-label col-sm-3 pt-0 fw-bold">
                            列表页中显示标签
                        </legend>
                        <div class="col-sm-9">
                            <div class="form-check form-switch">
                                <input
                                    id="tf-feature"
                                    type="checkbox"
                                    checked={self.tagsOn}
                                    name="feature"
                                    class="form-check-input"
                                />
                                <label for="tf-feature" class="form-check-label">
                                    在列表页中显示标签
                                </label>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset class="form-group row pt-2">
                        <legend class="col-form-label col-sm-3 pt-0 fw-bold">
                            列表页中显示标签的样式
                        </legend>
                        <div class="col-sm-9">
                            {F18_CONSTS.CONSTS.STYLES.map(styleId => (
                                <div class="form-check">
                                    <input
                                        type="radio"
                                        name="style"
                                        id={`tf-style-${styleId}`}
                                        value={String(styleId)}
                                        checked={self.style === styleId}
                                        class="form-check-input"
                                    />
                                    <label  class="form-check-label" for={`tf-style-${styleId}`}>
                                        {tagList(ctx, {
                                            list: demoTags,
                                            tagStyle: [styleId, false]
                                        })}
                                    </label>
                                </div>
                            ))}
                        </div>
                    </fieldset>
                    <fieldset class="form-group row pt-2">
                        <legend class="col-form-label col-sm-3 pt-0 fw-bold">
                            列表页中显示标签的顺序
                        </legend>
                        <div class="col-sm-9">
                            <div class="form-check">
                                <input
                                    type="radio"
                                    name="order"
                                    id="tf-order-1"
                                    value="1"
                                    checked={self.catOrder === 1}
                                    class="form-check-input"
                                />
                                <label for="tf-order-1" class="form-check-label">
                                    {render.staticConv(
                                        F18_CONSTS.CAT_TITLES[F18_CONSTS.CATS.SOURCE],
                                    )} - {render.staticConv(
                                        F18_CONSTS.CAT_TITLES[F18_CONSTS.CATS.LENGTH],
                                    )} - 内容标签
                                </label>
                            </div>
                            <div class="form-check">
                                <input
                                    type="radio"
                                    name="order"
                                    id="tf-order-2"
                                    value="2"
                                    checked={self.catOrder === 2}
                                    class="form-check-input"
                                />
                                <label for="tf-order-2" class="form-check-label">
                                    {render.staticConv(
                                        F18_CONSTS.CAT_TITLES[F18_CONSTS.CATS.LENGTH],
                                    )} - {render.staticConv(
                                        F18_CONSTS.CAT_TITLES[F18_CONSTS.CATS.SOURCE],
                                    )} - 内容标签
                                </label>
                            </div>
                            <div class="form-check">
                                <input
                                    type="radio"
                                    name="order"
                                    id="tf-order-3"
                                    value="3"
                                    checked={self.catOrder === 3}
                                    class="form-check-input"
                                />
                                <label for="tf-order-3" class="form-check-label">
                                    {render.staticConv(
                                        F18_CONSTS.CAT_TITLES[F18_CONSTS.CATS.LENGTH],
                                    )} - 内容标签
                                </label>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset class="form-group row pt-2">
                        <legend class="col-form-label col-sm-3 pt-0 fw-bold">
                            列表页中显示标签的数量
                        </legend>
                        <div class="col-sm-9">
                            <div class="form-check">
                                <input
                                    type="radio"
                                    name="limit"
                                    id="tf-limit-1"
                                    value="1"
                                    checked={self.limit === 1}
                                    class="form-check-input"
                                />
                                <label for="tf-limit-1" class="form-check-label">
                                    无限制（全部有效标签）
                                </label>
                            </div>
                            <div class="form-check">
                                <input
                                    type="radio"
                                    name="limit"
                                    id="tf-limit-2"
                                    value="2"
                                    checked={self.limit === 2}
                                    class="form-check-input"
                                />
                                <label for="tf-limit-2" class="form-check-label">
                                    不超过十个有效标签
                                </label>
                            </div>
                            <div class="form-check">
                                <input
                                    type="radio"
                                    name="limit"
                                    id="tf-limit-3"
                                    value="3"
                                    checked={self.limit === 3}
                                    class="form-check-input"
                                />
                                <label for="tf-limit-3" class="form-check-label">
                                    不超过五个有效标签
                                </label>
                            </div>
                            <div class="mt-1">当一行内不足以显示以上数量的标签时：</div>
                            <div class="form-check">
                                <input
                                    type="radio"
                                    name="nowrap"
                                    id="tf-nowrap--1"
                                    value="-1"
                                    checked={!self.nowrap}
                                    class="form-check-input"
                                />
                                <label for="tf-nowrap--1" class="form-check-label">
                                    换行显示剩余的标签
                                </label>
                            </div>
                            <div class="form-check">
                                <input
                                    type="radio"
                                    name="nowrap"
                                    id="tf-nowrap-1"
                                    value="1"
                                    checked={self.nowrap}
                                    class="form-check-input"
                                />
                                <label for="tf-nowrap-1" class="form-check-label">
                                    放弃显示一部分标签
                                </label>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset class="form-group row pt-2">
                        <legend class="col-form-label col-sm-3 pt-0 fw-bold">
                            敏感标签
                        </legend>
                        <div class="col-sm-9">
                            <div class="card">
                                <div class="card-body">
                                    <div id="warning-tag-list" class="m-n2">
                                        {self.warnings.map(tag => (
                                            // LINT.IfChange(tag_chip)
                                            <div class="btn-group btn-group-sm m-1">
                                                <button type="button" class="btn btn-secondary">
                                                    {render.staticConv(tag.primary_name)}
                                                </button>
                                                <button type="button" class="btn btn-secondary mm-tag-remove" name={String(tag.tag_id)}>
                                                    ×
                                                </button>
                                            </div>
                                            // LINT.ThenChange(../frontend/metro/tags_settings.ts:tag_chip)
                                        ))}
                                    </div>
                                </div>
                                <div class="card-body pt-0">
                                    <div class="m-n1 position-relative">
                                        <input
                                            type="text"
                                            placeholder="搜索标签以加入…"
                                            data-toggle="dropdown"
                                            class="form-control form-control-sm w-auto"
                                        />
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item disabled">
                                                加载中…
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <small class="form-text text-muted">
                                对于你可能不喜欢的标签，你可以加入这个列表，将其以
                                <span class="material-icons std-icon">report_problem</span>
                                标识，以方便避开
                            </small>
                        </div>
                    </fieldset>
                </form>
                <script type="text/javascript">{elements.safeHtml(`initTagSettingsPage('warning-tag-list', 'settings-update-alert');`)}</script>
            </div>
        </>
    );
};

// Migrated from thread_tag.pug
export const threadTag: HtmlTmpl<{
    owner: boolean,
    tag: TagLite,
    myVote: number,
    upvotes: number,
    downvotes: number
}> = (ctx, self) => (
    <div class="dropdown-menu">
        <div class="m-2 mt-0">
            {self.owner ? (
                <div class="text-center mb-2">
                    <span class="badge bg-secondary">此标签由作者添加</span>
                </div>
            ) : null}
            {self.tag.hint ? <p class="small mb-2">{elements.safeHtml(ctx.state.render.staticConv(self.tag.hint))}</p> : null}
            <div class="text-center">
                <a class="btn btn-secondary btn-sm" href={'/tag/' + self.tag.tag_id}>
                    标签详情
                </a>
            </div>
        </div>
        <div class="dropdown-divider" />
        <div class="m-2 mb-0 text-center">
            <p class="mb-2">此标签适用于此小说吗？</p>
            <div class="btn-group">
                <button
                    type="button"
                    name={self.myVote > 0 ? 'vote-0' : 'vote-1'}
                    class={'btn ' + ((self.myVote > 0) ? 'btn-primary' : 'btn-secondary')}
                >
                    <span class="material-icons">
                        {self.myVote > 0 ? 'thumb_up_alt' : 'thumb_up_off_alt'}
                    </span>
                    <small> {self.upvotes}</small>
                </button>
                <button
                    type="button"
                    name={self.myVote < 0 ? 'vote-0' : 'vote--1'}
                    class={'btn ' + ((self.myVote < 0) ? 'btn-primary' : 'btn-secondary')}
                >
                    <span class="material-icons">
                        {self.myVote > 0 ? 'thumb_down_alt' : 'thumb_down_off_alt'}
                    </span>
                    <small> {self.downvotes}</small>
                </button>
            </div>
        </div>
    </div>
);

type VoteTuple = [number /* user */, number /* vote */];

const voteTd: HtmlTmpl<{
    vote: VoteTuple,
    users: Map<number, { user_name: string }>
}> = (ctx, { vote, users }) => (
    <>
        <td>
            <a href={'/author/' + vote[0]}>{users.get(vote[0])?.user_name ?? '[UNKNOWN]'}</a>
        </td>
        <td data-debug={vote[1]}>
            {(vote[1] > 0) ? (
                <span class="material-icons align-middle">thumb_up_alt</span>
            ) : (vote[1] < 0) ? (
                <span class="material-icons align-middle">thumb_down_alt</span>
            ) : '已撤回'}
        </td>
    </>
);

// LINT.IfChange(tab_ids)
const thread_only_LOG_TAB = 'thread-tag-logs';
const thread_only_BROWSE_TAB = 'thread-add-tags';
// LINT.ThenChange(../frontend/metro/tags_in_threads.ts:tab_ids)

// Migrated from thread_tags.pug
export const threadTags: HtmlTmpl<{
    tags: { votes: VoteTuple[], tag: TagLite, score: number, owner: number }[],
    tagSelections: Map<number, {
        full: boolean,
        some: boolean,
        voted: boolean
    }>,
    thread: { id: number, title: string },
    f18Tags: Awaited<ReturnType<typeof allTagsByCategory>>,
    users: Map<number, { user_id: number, user_name: string }>
}> = (ctx, self) => {
    const { render } = ctx.state;
    return (
        <>
            <div id={thread_only_LOG_TAB}>
                {self.tags.length ? (
                    <>
                        <div class="alert alert-info">
                            关于标签操作的解释，请参见
                            <a href={'/thread/' + F18_CONSTS.CONSTS.README_THREAD_ID}>
                                标签功能的介绍帖
                            </a>
                            。
                        </div>
                        <table class="table table-sm mb-0">
                            <thead>
                                <th>标签</th>
                                <th>用户</th>
                                <th>投票</th>
                            </thead>
                            <tbody>
                                {self.tags.map(tag => [
                                    <tr>
                                        <td rowspan={tag.votes.length}>
                                            <p class="lead mb-2">
                                                <a
                                                    href={'/tag/' + tag.tag.tag_id}
                                                    data-title={tag.tag.hint ? render.staticConv(tag.tag.hint) : false}
                                                    class={"badge rounded-pill bg-secondary me-2 fw-normal" + (tag.tag.hint ? ' mazomirror-tip' : '')}
                                                >
                                                    {render.staticConv(tag.tag.primary_name)}
                                                    {tag.tag.hint ? (
                                                        <small class="material-icons align-middle" style="font-size: 85%">
                                                            info
                                                        </small>
                                                    ) : null}
                                                </a>
                                            </p>
                                            <div class="small">
                                                计分：{Math.round(tag.score * 1000) / 1000}
                                                <span class="mazomirror-tip" data-title="计分高的标签将优先显示；计分大于 1 的标签才会生效；详细说明请参见链接">
                                                    <span class="material-icons align-middle" style="font-size: 100%">
                                                        help
                                                    </span>
                                                </span>
                                            </div>
                                            {tag.owner ? (
                                                <span class="badge bg-secondary">
                                                    此标签由作者{tag.owner > 0 ? '添加' : '反对'}
                                                </span>
                                            ) : null}
                                        </td>
                                        {voteTd(ctx, { vote: tag.votes[0], users: self.users })}
                                    </tr>,
                                    tag.votes.slice(1).map(vote => (<tr>{voteTd(ctx, { vote: vote, users: self.users })}</tr>))
                                ])}
                            </tbody>
                        </table>
                    </>
                ) : (
                    <div class="alert alert-info">此主题从未有任何标签操作记录。</div>
                )}
            </div>
            <div id={thread_only_BROWSE_TAB} class="pt-1">
                {F18_CONSTS.CAT_TITLES.map((title, catId) => {
                    const radio = catId < F18_CONSTS.CATS._FIRST_ARBITRARY;
                    return title ? (
                        <>
                            <fieldset class="mb-3">
                                <legend class="h6 fw-bold">
                                    {render.staticConv(title)}
                                </legend>
                                {self.f18Tags.get(catId as CategoryId)!.map(tag => {
                                    const primaryName = ((radio && tag.parentTags)
                                        ? tag.parentTags
                                                .map(parent => parent.primary_name)
                                                .join('，') + '—'
                                        : '') + tag.primary_name;
                                    const selection = self.tagSelections.get(tag.tag_id);
                                    // .text-reset below is needed for dark themes.
                                    return !(tag.flags & 0x2) ? (
                                        <>
                                            <div class="d-inline-block position-relative mb-1 mx-1">
                                                <button class="mm-tag-fcbox text-reset" data-id={String(tag.tag_id)}>
                                                    <span class={"material-icons std-icon" + (selection?.voted ? ' text-primary' : '')}>
                                                        {/* LINT.IfChange(material_cb) */}
                                                        {selection?.full ? 'check_box' : (selection?.some ? 'indeterminate_check_box' : 'check_box_outline_blank')}
                                                        {/* LINT.ThenChange(../frontend/metro/tags_in_threads.ts:material_cb) */}
                                                    </span>
                                                    <span>{render.staticConv(primaryName)}</span>
                                                </button>
                                            </div>
                                        </>
                                    ) : null;
                                })}
                            </fieldset>
                        </>
                    ) : null;
                })}
                <div id="ic-suggestion-card" class="card d-none">
                    <div class="card-body">
                        <form method="post" action={`/thread/${self.thread.id}/tag`}>
                            <p>
                                若你认为有适用于此主题（{render.contentConv(self.thread.title)}）的标签并未包含在上述列表中，可以提议新的标签。
                            </p>
                            <input type="hidden" name="title" value="创建新标签的建议" />
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label" for="ic-suggestion-name" >
                                    名称
                                </label>
                                <div class="col-sm-10">
                                    <input
                                        id="ic-suggestion-name"
                                        name="name"
                                        type="text"
                                        placeholder="标签的主要名称"
                                        class="form-control"
                                    />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label" for="ic-suggestion-cat">
                                    类别
                                </label>
                                <div class="col-sm-10">
                                    <select id="ic-suggestion-cat" class="custom-select" name="cat">
                                        <option value="0">（请选择）</option>
                                        {F18_CONSTS.CAT_TITLES.map((title, catId) => 
                                            title ? (
                                                <option value={String(catId)}>{render.staticConv(title)}</option>
                                            ) : null
                                        )}
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <textarea
                                    name="suggestion"
                                    placeholder="标签描述/别名等其它信息（非必填）"
                                    class="form-control"
                                />
                            </div>
                            <div class="text-center">
                                <a
                                    href="javascript:document.getElementById('ic-suggestion-card').classList.add('d-none')"
                                    class="btn btn-secondary btn-sm"
                                >
                                    取消
                                </a>
                                {' '}
                                <button type="submit" class="btn btn-primary btn-sm">
                                    提交
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="py-2">
                    <a
                        href="javascript:document.getElementById('ic-suggestion-card').classList.remove('d-none')"
                        class="btn btn-secondary btn-sm"
                    >
                        没有合适的标签？提议新的标签
                    </a>
                </div>
            </div>
        </>
    );
};