const date = () => new Date().toISOString();

export const fail = (target: any, errorType: string, detail?: any) => {
    console.warn('Failure - ' + errorType + ' - ' + date());
    console.warn(JSON.stringify(target), detail);
};

export const info = (target: any, eventType: string, detail?: any) => {
    console.info('Info - ' + eventType + ' - ' + date());
    console.info(JSON.stringify(target), detail);
};

export const error = (errorType: string, detail?: any) => {
    console.error('Error - ' + errorType + ' - ' + date());
    console.error(detail);
};

export const fatalAndExit = (detail?: any) => {
    console.error('FATAL - ' + date());
    console.error(detail);
    process.exit(1);
};

export const log = (eventType: string, detail?: any) => {
    console.log('Log - ' + eventType + ' - ' + date());
    console.log(detail);
};