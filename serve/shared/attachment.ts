import crypto = require('crypto');
import path = require('path');
import { promises as fs } from 'fs';

import { query } from '../db';
import { fromRoot } from '../path';
import sharp = require('sharp');
import { error } from '../utils/render_utils';

export const allowedFormats = Object.freeze({
    'ffd8ff': 'jpeg',
    '89504e': 'png',
    '474946': 'gif',
    '524946': 'webp'
});

const attachmentDir = path.join('static', 'attachments');

export const imagePreprocess = async (file: Buffer, sizeLimitInKb: number): Promise<Buffer> => {
    const sharpStream = sharp(file);
    const meta = await sharpStream.metadata();
    if (!(meta.width! > 2) || !(meta.height! > 2)) {
        throw error(400, '图片长宽无法被识别。');
    }
    const animated = (meta.pages ?? 1) > 1;
    if (animated) {
        if (file.length / 1024 > sizeLimitInKb) {
            throw error(429, `单个图片文件大小不可超过 ${sizeLimitInKb} KB；而动画图片无法被自动压缩`);
        }
        return file;
    }
    if (file.length / 1024 <= 128) { // Less 128 KB? Don't bother to compress.
        return file;
    }

    let zoomRatio = 1;
    const minSize = Math.min(meta.width!, meta.height!);
    if (minSize > 2160) { zoomRatio = 2160 / minSize; }

    const pixelCount = meta.width! * meta.height!;
    if (pixelCount > 9600000) { zoomRatio = Math.min(zoomRatio, Math.sqrt(9600000 / pixelCount)); }

    return sharpStream.resize(Math.round(meta.width! * zoomRatio), Math.round(meta.height! * zoomRatio)).webp(/* Default quality 80 is used */).toBuffer();
};

export const attachmentIdToName = (id: number): string => {
    const buffer = Buffer.alloc(4);
    buffer.writeInt32LE(id ^ 19941124);
    return buffer.toString('hex');
};

export const attachmentNameToId = (name: string): number => {
    if (!/^[0-9a-f]{8}$/.test(name)) { return 0; }
    const buffer = Buffer.from(name, 'hex');
    return buffer.readInt32LE() ^ 19941124;
};

export const uploadAttachment = async (file: Buffer, compressed: boolean, userId: number, dateInSeconds: number, format: string, txn: typeof query): Promise<string> => {
    const fileHasher = crypto.createHash('md5');
    fileHasher.update(file);
    const fileHash = fileHasher.digest('hex');
    const attachmentId = (await txn('INSERT INTO local_attachments (user_id, uploaded_date, file_size, file_hash, flags) VALUES ($1, $2, $3, DECODE($4, \'hex\'), $5) RETURNING attachment_id', [userId, dateInSeconds, file.length, fileHash, compressed ? 0x1 : 0x0])).rows[0].attachment_id;
    await fs.writeFile(fromRoot(attachmentDir, fileHash), file);
    const logicalFileName = path.join(attachmentDir, attachmentIdToName(attachmentId) + '.' + format);
    await fs.symlink(fileHash, fromRoot(logicalFileName));
    return logicalFileName;
} 

// async
export const updateAttachmentsToPosts = (names: string[], postId: number, txn: typeof query): Promise<unknown> => txn('UPDATE local_attachments SET post_id = $1 WHERE post_id = 0 AND attachment_id = ANY($2::int[])', [postId, names.map(attachmentNameToId)]);