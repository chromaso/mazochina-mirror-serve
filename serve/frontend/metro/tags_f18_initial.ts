import { $, $id, $set } from '../common-import';
import { TagListModel, displayTag, initializeSearcher, loadTagListModel, requireChildren } from './tags_common';

const hide = (el: HTMLElement) => {
    // https://stackoverflow.com/a/8318442.
    el.style.position = 'absolute';
    el.style.visibility = 'hidden';
}

const show = (el: HTMLElement) => {
    el.style.removeProperty('position');
    el.style.removeProperty('visibility');
}

// Works like debounce, but will always be called once intervalUpperBoundMs is exceeded.
const debounceOrThrottle = (method: () => void, intervalUpperBoundMs: number, waitMs: number) => {
    let firstUnfulfilled: number | undefined;
    let timeout: ReturnType<typeof setTimeout> | undefined;
    return () => {
        const now = Date.now();
        if (firstUnfulfilled) { // A timeout pending.
            // Don't extend beyond the maximum interval.
            if (now + waitMs > firstUnfulfilled + intervalUpperBoundMs) {
                return;
            }
            clearTimeout(timeout);
        } else { // No timeout pending.
            firstUnfulfilled = now;
        }
        timeout = setTimeout(() => {
            firstUnfulfilled = undefined;
            method();
        }, waitMs);
    };
};

const initializeFullList = (tagBody: HTMLElement) => {
    const head = $id('tag-card-header')!;
    const cardHint = $id('tag-card-hint')!;
    const fieldSets = Array.from(tagBody.getElementsByTagName('fieldset'));

    let lastClickedButton: HTMLElement | undefined;
    const activateTab = (button: HTMLElement) => {
        const catId = button.getAttribute('data-id');
        if (catId) {
            fieldSets.forEach(hide);
            const fieldSet = $id('tag-group-' + catId)!;
            show(fieldSet);
            fieldSet.classList.add('mb-0');
            tagBody.classList.add('cat-selected');
        } else {
            fieldSets.forEach(f => {
                show(f);
                f.classList.remove('mb-0');
            });
            tagBody.classList.remove('cat-selected');
        }
        if (lastClickedButton) { lastClickedButton.classList.remove('active'); }
        button.classList.add('active');
        lastClickedButton = button;
        cardHint.style.display = 'none';
    };

    head.style.setProperty('display', 'block', 'important');
    head.addEventListener('click', e => {
        let target: ParentNode | null = e.target as HTMLElement;
        while (target instanceof HTMLElement) {
            if (target === head) { return; }
            if (target.tagName.toUpperCase() === 'BUTTON') {
                activateTab(target);
            }
            target = target.parentNode;
        }
        return null;
    });
    fieldSets.forEach(hide);
    cardHint.classList.add('mb-0');
    $set(cardHint, '你也可点击上方分类，查看所有可选择标签列表');

    return Array.from(tagBody.getElementsByTagName('checkbox'));
};

const initializeInteractiveSelector = (
    model: TagListModel,
    checkboxes: HTMLInputElement[],
    titleInput: HTMLInputElement,
    contentTextArea: HTMLTextAreaElement
) => {
    $id('interactive-tag-selector')!.style.setProperty('display', 'block', 'important');

    const selectedTagsContainer = $id('selected-tags')!;
    const selectedTags: Record<string, HTMLElement> = {}; // Tag IDs, but in stringified.

    const selectedTagsAlert = $id('selected-tag-alert')!;
    const updateAlertVisiblity = () => {
        const count = Object.keys(selectedTags).length;
        selectedTagsContainer.classList[count ? 'remove' : 'add']('d-none');
        if (count <= 0) {
            selectedTagsAlert.classList.remove('d-none');
            $set(selectedTagsAlert, '你暂未添加任何内容标签。内容标签皆非必选，但建议添加 2 - 10 个。');
        } else if (count > 10) {
            selectedTagsAlert.classList.remove('d-none');
            $set(selectedTagsAlert, '你添加的内容标签似乎太多了，建议只保留最相关的标签。');
        } else {
            selectedTagsAlert.classList.add('d-none');
        }
    };

    const onAddTag = (e: Event) => addTag((e.currentTarget as HTMLButtonElement).name);
    const onRemoveTag = (e: Event) => removeTag((e.currentTarget as HTMLButtonElement).name);

    const removeTag = (tagId: string) => {
        ($id('tag-sel-' + tagId) as HTMLInputElement).checked = false;
        const child = selectedTags[tagId];
        if (!child) { return; }
        selectedTagsContainer.removeChild(child);
        delete selectedTags[tagId];
        updateSuggestions();
        updateAlertVisiblity();
    };
    const addTag = (tagId: string, duringInit?: boolean) => { // Except for from checkbox; in that case this shouldn't be called.
        const tag = model.tagMapById[tagId];
        if (tag._prop_flags & 0x2) {
            requireChildren(tag, addTag);
            return;
        }
        ($id('tag-sel-' + tagId) as HTMLInputElement).checked = true;
        if (selectedTags.hasOwnProperty(tagId)) { return; }
        const btn = $('div', 'btn-group btn-group-sm me-1 mb-1', [
            $('button', { type: 'button', className: 'btn btn-primary' }).also(displayTag(tag)),
            $('button', { type: 'button', className: 'btn btn-primary', name: tagId }, '×', onRemoveTag)
        ]);
        selectedTags[tagId] = btn;
        selectedTagsContainer.appendChild(btn);
        if (!duringInit) { updateSuggestions(); updateAlertVisiblity(); }
    };
    const onCheckboxClick = (e: Event) => {
        const checkbox = e.currentTarget as HTMLInputElement;
        const tagId = checkbox.value;
        if (checkbox.checked) {
            if (!selectedTags.hasOwnProperty(tagId)) {
                addTag(tagId);
            }
        } else {
            removeTag(tagId);
        }
    };
    for (const checkbox of checkboxes) {
        if (checkbox.checked) {
            addTag(checkbox.value, true);
        }
        checkbox.addEventListener('click', onCheckboxClick);
    }

    const suggestions: string[] = [];
    const suggestionsContainer = $id('recommended-tags')!;
    const renderedSuggestions: Record<string, HTMLElement> = {};
    const renderSuggestions = () => {
        const newSuggestions = suggestions.slice(0, 15);
        Object.keys(renderedSuggestions).forEach(k => {
            suggestionsContainer.removeChild(renderedSuggestions[k]);
            if (!newSuggestions.includes(k)) { delete renderedSuggestions[k]; }
        });
        for (const suggestion of newSuggestions) {
            let element = renderedSuggestions[suggestion];
            if (!element) {
                const props = { type: 'button', className: 'btn btn-secondary', name: suggestion };
                element = $('div', 'btn-group btn-group-sm me-1 mb-1', [
                    $('button', props, undefined, onAddTag).also(displayTag(model.tagMapById[suggestion])),
                    $('button', props, '+', onAddTag)
                ]);
                renderedSuggestions[suggestion] = element;
            }
            suggestionsContainer.appendChild(element);
        }
    };

    const sceditorInstance = window.sceditor?.instance(contentTextArea);
    const updateSuggestions = () => {
        suggestions.splice(0, suggestions.length, ...model.recommend(selectedTags, titleInput.value, sceditorInstance?.getSourceEditorValue() || contentTextArea.value, {
            _prop_defaultWarningBonus: 0,
            _prop_nonArbitraryPenalty: 1
        }));
        renderSuggestions();
    };
    updateSuggestions();
    updateAlertVisiblity();

    titleInput.addEventListener('change', updateSuggestions);
    contentTextArea.addEventListener('change', updateSuggestions);
    const debouncedUpdateSuggestions = debounceOrThrottle(updateSuggestions, 5000, 1500);
    titleInput.addEventListener('input', debouncedUpdateSuggestions);
    contentTextArea.addEventListener('input', debouncedUpdateSuggestions);
    sceditorInstance?.bind('blur keypress valuechanged pasteraw paste', debouncedUpdateSuggestions);

    initializeSearcher($id('tag-searcher') as HTMLInputElement, model, suggestions, addTag, false);
};

export const postTagSelector = () => {
    const thb = $id('tag-card-body')!;
    initializeFullList(thb);

    loadTagListModel(true, res => {
        if (typeof res === 'string') { console.error(res); return; }
        initializeInteractiveSelector(
            res,
            Array.from(thb.getElementsByTagName('input')),
            document.querySelector('input[name=title]')!,
            document.getElementById('main-textarea') as HTMLTextAreaElement
        );
    });
};
