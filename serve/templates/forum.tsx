import * as elements from 'typed-xhtml';
import { HtmlTmpl } from '../utils/tsx_utils';
import { threadTable } from './thread_table';
import { BaseThreadTarget, RenderThreadRetVal } from '../shared/thread_table';
import { pagination, PaginationParams } from './pagination';

// Migrated from forum.pug.
export const forum: HtmlTmpl<{
    forumId: number,
    title: string,
    postLink: string | undefined,
    childForums: { link: string, name: string, count: number, lastPost: number }[],
    showReadState: boolean,
    threads: RenderThreadRetVal<BaseThreadTarget>,
    pagination: PaginationParams,
    sorting: 'sort' | 'first' | 'last',
    includeSpam: number
}> = (ctx, self) => {
    const { render } = ctx.state;
    return (
        <div class="container">
            <nav class="mb-4">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="/">首页</a>
                    </li>
                    <li class="breadcrumb-item active">
                        {render.staticConv(self.title)}
                    </li>
                </ol>
            </nav>
            <div class="d-flex justify-content-between align-items-center mb-4">
                <h1 class="my-0">{render.staticConv(self.title)}</h1>
                {self.postLink ? (
                    <form action={self.postLink} method="post" class="d-inline">
                        <button type="submit" class="btn btn-primary">
                            <span class="material-icons me-0">post_add</span>
                        </button>
                    </form>
                ) : null}
            </div>
            {self.childForums.length
                ? self.childForums.map(forum => (
                        <div class="card my-4">
                            <div class="card-body">
                                <h5 class="card-title">
                                    子板面：
                                    <a href={forum.link} class="ui-link">{render.staticConv(forum.name)}</a>
                                </h5>
                                <p class="card-text">
                                    {forum.count}个主题
                                    <span class="text-muted ms-3">
                                        最新更新：{render.renderDate(forum.lastPost)}
                                    </span>
                                </p>
                            </div>
                        </div>
                    ))
                : null}
            {threadTable(ctx, { forumId: self.forumId, threads: self.threads, showReadState: self.showReadState })}
            {pagination(ctx, self.pagination)}
            <div class="text-center my-4">
                {self.postLink ? (
                    <form action={self.postLink} method="post" class="d-inline">
                        <button type="submit" class="btn btn-primary">
                            <span class="material-icons me-1">post_add</span>
                            {(self.forumId === 18) ? '发表小说' : '发表主题'}
                        </button>
                    </form>
                ) : null}
                <span class="position-relative">
                    <button data-toggle="dropdown" class="btn btn-secondary ms-3">
                        <span class="material-icons">filter_list</span>
                    </button>
                    <div class="dropdown-menu text-center">
                        <form id="thread-list-form" method="get" class="px-2">
                            <div class="small mt-1">
                                <label class="d-block form-check-label">
                                    <input
                                        type="radio"
                                        name="sort"
                                        value="sort"
                                        checked={self.sorting === 'sort'}
                                        class="me-1"
                                    />
                                    按回复时间排序（优化）
                                </label>
                                <label class="d-block form-check-label">
                                    <input
                                        type="radio"
                                        name="sort"
                                        value="last"
                                        checked={self.sorting === 'last'}
                                        class="me-1"
                                    />
                                    按回复时间排序（传统）
                                </label>
                                <label class="d-block form-check-label">
                                    <input
                                        type="radio"
                                        name="sort"
                                        value="first"
                                        checked={self.sorting === 'first'}
                                        class="me-1"
                                    />
                                    按发布时间排序
                                </label>
                            </div>
                            <div class="dropdown-divider" />
                            <div class="small mt-1">
                                <label class="d-block form-check-label">
                                    <input
                                        type="radio"
                                        name="all"
                                        value="-1"
                                        checked={self.includeSpam < 0}
                                        class="me-1"
                                    />
                                    完全不显示广告帖
                                </label>
                                <label class="d-block form-check-label">
                                    <input
                                        type="radio"
                                        name="all"
                                        value="0"
                                        checked={!self.includeSpam}
                                        class="me-1"
                                    />
                                    显示半隐藏广告帖
                                </label>
                                <label class="d-block form-check-label">
                                    <input
                                        type="radio"
                                        name="all"
                                        value="1"
                                        checked={self.includeSpam > 0}
                                        class="me-1"
                                    />
                                    显示全部主题
                                </label>
                            </div>
                        </form>
                    </div>
                </span>
            </div>
            <div class="row">
                <div class="col-auto mx-auto">
                    <div class="card">
                        <div class="card-header">根据标题搜索</div>
                        <div class="card-body">
                            <form
                                method="get"
                                action="/query"
                                class="d-flex justify-content-center flex-nowrap"
                            >
                                {self.forumId ? (
                                    <input type="hidden" name="forum" value={String(self.forumId)} />
                                ) : null}
                                <input type="hidden" name="type" value="thread" />
                                <div class="input-group">
                                    <input
                                        name="query"
                                        placeholder="关键词"
                                        class="form-control"
                                    />
                                    <button class="btn btn-primary">
                                        {self.forumId ? '搜索本板' : '搜索'}
                                    </button>
                                </div>
                                <a
                                    href={'/search/?f=' + self.forumId}
                                    class="btn btn-secondary ms-2 ms-sm-3 flex-shrink-0"
                                >
                                    更多
                                </a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                {elements.safeHtml(`(function(a,b){[].forEach.call(a.getElementsByTagName("input"),function(c){c.addEventListener("change",b(a))})})(document.getElementById("thread-list-form"),function(a){return function(){a.submit()}});`)}
            </script>
        </div>
    );
}
    