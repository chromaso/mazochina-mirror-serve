
import { IastItemsParsedFromBBCode } from '../../common/bbcode';
import { THRESHOLD_FOR_LOCAL } from '../../common/consts';
import { IastDependencyRequirement, IastInput, unescapeForIast } from '../../common/iast';
import { normalize } from '../utils/logic_utils';
import { query as sql } from '../db';

// IaSt uses this to normalize identifiers.
export const escapeForIast = (text: string) => {
    const filtered = normalize(text).replace(/[^a-z0-9\u4E00-\u9FA5\u3400-\u4DB5_\-]/g, '');

    if (!filtered.length) { return '~'; }
    return filtered;
};

const crossCheckError = (text: string): Error => new Error('交互选项设计错误：' + text);

export const checkIastItemsValidity = async (
    items: IastItemsParsedFromBBCode,
    threadId: number, // Can be zero.
    postId: number, // Can be zero.
): Promise<void> => {
    const iastAll = (await sql<{ post_id: number, ins: { [key: string]: IastInput }, outs: { [key: string]: IastDependencyRequirement } }>('SELECT post_id, iast_inputs AS ins, iast_outputs AS outs FROM iast_config WHERE thread_id = $1 AND post_id <> $2', [threadId, postId])).rows;

    const effectivePostId = postId || (THRESHOLD_FOR_LOCAL * 2);
    iastAll.push({
        post_id: effectivePostId,
        ins: items.inputs,
        outs: items.usages
    });
    iastAll.sort((a, b) => a.post_id - b.post_id); // Now effectively ORDER BY post_id ASC.

    const printId = (id: number) => (id === effectivePostId) ? '此文章' : `文章（#${id}）`;

    const defs = new Map<string, Readonly<[number /* post ID */, IastInput['type']]>>();

    for (const row of iastAll) {
        for (const [key, value] of Object.entries(row.ins)) {
            const existing = defs.get(key);
            if (existing) {
                throw crossCheckError(`${printId(existing[0])}已经定义过 ${key}，不可在${printId(row.post_id)}重复定义`);
            }
            defs.set(key, [row.post_id, value.type]);
        }
        if (defs.size > 200) {
            throw crossCheckError('此主题中定义的 [input] 数量超过了上限（200），请联系管理员');
        }
        for (const [key, value] of Object.entries(row.outs)) {
            const def = defs.get(key);
            // Since we sorted the array, we can detect uses before definitions across posts. For use-before-definition within a post, bbcode parser will handle it. 
            if (!def) { throw crossCheckError(`${printId(row.post_id)}所使用的变量「${unescapeForIast(key)}」在使用前尚未被定义过`); }
            const sourceType = def[1];
            // The parts below take care of errors even when definition and usage are within the same post. Bbcode parse will not handle these.
            if (Array.isArray(sourceType)) {
                if (value.text) { throw crossCheckError(`${printId(def[0])}所定义的变量「${unescapeForIast(key)}」是选择题，在${printId(row.post_id)}中不可以当作文字输入来使用`); }
                if (value.options) {
                    const missing = value.options.filter(item => item && !sourceType.includes(item)); // Allow empty string as it means nothing selected yet
                    if (missing.length) {
                        throw crossCheckError(`在${printId(row.post_id)}中试图匹配变量「${unescapeForIast(key)}」的值为不存在的选项：${missing.map(unescapeForIast).join('、')}`);
                    }
                }
            } else if (sourceType === 'any') {
                if (value.text) {
                    if (value.text.includes('FN') || value.text.includes('GN')) {
                        throw crossCheckError(`在${printId(row.post_id)}中试图将变量「${unescapeForIast(key)}」作为姓名使用，但其在${printId(def[0])}中并未被定义为姓名类型`);
                    }
                }
            }
        }
    }
};