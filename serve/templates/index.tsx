import * as elements from 'typed-xhtml';

import { HtmlTmpl } from '../utils/tsx_utils';
import { FullConfig as Config } from '../shared/options';
import { BaseThreadTarget, RenderThreadRetVal } from '../shared/thread_table';
import { threadTableLite } from './thread_table';
import { triviaLite } from './trivia';
import { TriviaStats } from '../shared/trivia';
import { formatVisitorId } from '../shared/login';
import { homeGreet } from '../utils/rand_greet';

export type ForumRendering = { count: number, name: string, link: string, childForums: { name: string; link: string }[], lastPost: number };

export const index: HtmlTmpl<{
    localOnlyUser: boolean,
    forumGroups: ForumRendering[][],
    currentTz: Config['tz'],
    hostname: string,
    threads: RenderThreadRetVal<BaseThreadTarget>,
    triviaStats: TriviaStats | undefined
}> = (ctx, self) => {
    const loginUser = ctx.state.mazoLoginUser;
    const render = ctx.state.render;
    return (
        <>
            <div class="container">
                <div class="alert alert-secondary">
                    {loginUser ? (
                        <>
                            {homeGreet(ctx)}，{loginUser.name}！
                            有任何问题请参见 <a href="/thread/1073741842">FAQ</a>。
                        </>
                    ) : (
                        <>
                            你好，{formatVisitorId(ctx.state.mazoGuest?.id)}！<a href="#login">注册或登入</a>来使用全部功能吧。有任何问题请参见 <a href="/thread/1073741842">FAQ</a>。
                        </>
                    )}
                </div>
                <div id="dynamic-alert-container" />
                <ul id="index-tab-switch" class="nav nav-tabs nav-fill d-none d-md-none mb-3">
                    <li class="nav-item">
                        <a href="#" class="nav-link">板面</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">动态</a>
                    </li>
                </ul>
                <div class="row mx-md-n4">
                    <div class="d-md-block col-md-6 col-lg-7 col-xl-8 pe-md-4">
                        {self.forumGroups.map(forumGroup => [
                            <div class="row">
                                {forumGroup.map(forum => (
                                    <div class="col-sm-6 col-md-12 col-lg-6 col-xl-4">
                                        <div class="card my-2">
                                            <div class="card-body">
                                                <h5 class="card-title">
                                                    <a href={forum.link} class="ui-link">
                                                        {render.staticConv(forum.name)}
                                                    </a>
                                                </h5>
                                                <p class="card-text">
                                                    <span class="d-inline-block me-3">{forum.count} 个主题</span>
                                                    <span class="d-inline-block text-muted">
                                                        最新更新：{render.renderDate(forum.lastPost)}
                                                    </span>
                                                    {forum.childForums.length ? (
                                                        <>
                                                            <br />
                                                            子板面：
                                                            {forum.childForums.map(child => (
                                                                <a href={child.link} class="ui-link">
                                                                    {render.staticConv(child.name)}
                                                                </a>
                                                            ))}
                                                        </>
                                                    ) : null}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                ))}
                            </div>,
                            <hr />
                        ])}
                    </div>
                    <div class="d-md-flex col-md-6 col-lg-5 col-xl-4 flex-row">
                        <div class="vr me-md-4 d-none d-md-block" />
                        <div>
                            <p class="lead pt-4 pt-md-0 d-none d-md-block">动态</p>
                            {threadTableLite(ctx, { threads: self.threads })}
                            <div class="text-center">
                                <a href="/forum/0" class="btn btn-lg btn-link">
                                    更多
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                {self.triviaStats ? triviaLite(ctx, self.triviaStats) : null}
                <p class="text-center mt-4">
                    <a href="/rules">章程与规则</a> · <a href="/thread/1073741842">关于</a> · <a href="/moderation">管理操作记录</a>
                </p>
            </div>
            <script type="text/javascript">
                {elements.safeHtml(`initHomePage(${self.currentTz}, ${self.localOnlyUser ? loginUser!.id : 0}, ${JSON.stringify(self.hostname)});`)}
            </script>
        </>
    );
};