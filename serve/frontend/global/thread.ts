import { $ } from "./dollar";
import { getThreadIdFromUri } from "./utils";

export const threadPageInit = () => {
    // Delegating side reply button to bottom reply button.
    const repButton = document.getElementById('rep-button')!;
    const repQuick = document.getElementById('rep-quick')!;
    repQuick.addEventListener('click', () => repButton.click());

    // Hide the side buttons when already at bottom.
    const onScroll = (): void => {
        const windowHeight = document.documentElement.clientHeight;
        const bottomButton = repButton.getBoundingClientRect();
        document.body.classList[(bottomButton.bottom + bottomButton.top < windowHeight * 2) ? 'add' : 'remove']('no-side-buttons');
    };
    document.addEventListener('scroll', onScroll);
    window.addEventListener('resize', onScroll);
    setTimeout(onScroll);

    const links = Array.from(document.querySelectorAll<HTMLAnchorElement>('.mm-post a'));
    // Don't set HTTP referrer for certain links.
    for (const el of links.filter(el => el.hostname === 'pan.baidu.com')) {
        el.rel = 'noreferrer';
    }

    // Height limit for blockquotes.
    const removeHeightLimit = function (this: HTMLElement, e: MouseEvent) {
        this.style.maxHeight = 'none';
        this.style.opacity = '1';
        e.stopPropagation();
        this.removeEventListener('click', removeHeightLimit, true);
    };
    try {
        Array.prototype.forEach.call(document.querySelectorAll('blockquote:has(> cite)'), (el: HTMLElement) => el.addEventListener('click', removeHeightLimit, true));
    } catch (e) {
        // Do nothing. Some browsers not supporting `:has` selector will throw an error. Safe to ignore.
    }

    // Click to show full create/edit date of a post.
    const dateDisplayStyleFlip = function (this: HTMLElement) {
        const first = this.firstElementChild as HTMLSpanElement;
        first.className = first.className ? '' : 'd-none';
    };
    for (const el of Array.from(document.getElementsByClassName('post-date-wrap'))) {
        el.addEventListener('click', dateDisplayStyleFlip);
    }

    // If there is a submit button (should only be there when logged in), prevent it from being clicked multiple times.
    const button = document.getElementById('qrp-form-btn');
    if (button) {
        let lastClick = 0;
        button.addEventListener('click', evt => {
            const time = Date.now();
            if (time - lastClick < 5000) {
                evt.preventDefault();
            } else {
                lastClick = time;
            }
        });
    }

    // Links to other posts or headings.
    for (const link of links) {
        const href = link.getAttribute('href');
        if (!href) { continue; }

        const postLink = href.match(/^\/post\/(\d+)$/);
        if (postLink) {
            const elementId = 'p' + postLink[1];
            if (document.getElementById(elementId)) { // Post found in this page.
                link.href = '#' + elementId;
            }
        } else if (href.substring(0, 3) === '#h-') {
            if (!document.getElementById(href.substring(1))) { // Heading not found in this page. Maybe in another page.            
                link.href = '/thread/' + getThreadIdFromUri()  + '/heading/' + href.substring(3);
            }
        }
    }

    // Headings.
    const headings = Array.from(document.querySelectorAll<HTMLHeadingElement>(['1', '2', '3', '4', '5', '6'].map(l => '.mm-post h' + l).join(', ')));
    if (headings.length) {
        const mute = (e: Event) => e.preventDefault();
        const threadId = getThreadIdFromUri();
        for (const heading of headings) {
            const hasId = heading.id.substring(0, 2) === 'h-';
            const flare = $(hasId ? 'a' : 'span', 'mm-flare text-muted small d-inline-block text-decoration-none opacity-50 user-select-none me-1', '§');
            if (hasId) {
                (flare as HTMLAnchorElement).href = '/thread/' + threadId + '/heading/' + heading.id.substring(2);
                flare.addEventListener('click', mute);
            }
            heading.insertBefore(flare, heading.firstChild);
        }
    }
};
